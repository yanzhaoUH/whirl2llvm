; ModuleID = 'llvm.bc'

@.str = private constant [4 x i8] c"%d\0A\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10 = alloca i32, align 4
  %_temp_ehpit0 = alloca i32, align 4
  %a = alloca [40 x i32], align 64
  %i = alloca i32, align 4
  %sum = alloca i32, align 4
  %i_50 = alloca i32, align 4
  %"<preg>_55" = alloca <4 x i32>, align 8
  store <4 x i32> <i32 2, i32 2, i32 2, i32 2>, <4 x i32>* %"<preg>_55", align 8
  %"<preg>_58" = alloca i64, align 8
  %0 = bitcast [40 x i32]* %a to i32*
  %1 = ptrtoint i32* %0 to i64
  store i64 %1, i64* %"<preg>_58", align 8
  br label %LABEL_2306

LABEL_2306:                                       ; preds = %LABEL_2306, %entry
  %i_50_PHI = phi i32 [ 0, %entry ], [ %add, %LABEL_2306 ]
  %2 = load <4 x i32>, <4 x i32>* %"<preg>_55", align 8
  %3 = load i64, i64* %"<preg>_58", align 8
  %4 = inttoptr i64 %3 to i32*
  %5 = bitcast i32* %4 to <4 x i32>*
  store <4 x i32> %2, <4 x i32>* %5
  %add = add i32 %i_50_PHI, 4
  %6 = load i64, i64* %"<preg>_58", align 8
  %add.1 = add i64 %6, 16
  store i64 %add.1, i64* %"<preg>_58", align 8
  %cmp1 = icmp sle i32 %i_50_PHI, 39
  br i1 %cmp1, label %LABEL_2306, label %label_end

label_end:                                        ; preds = %LABEL_2306
  %7 = bitcast [40 x i32]* %a to i32*
  %8 = ptrtoint i32* %7 to i64
  %9 = inttoptr i64 %8 to i32*
  %call1 = call i32 @_Z5test1Pii(i32* %9, i32 40)
  %__comma_49 = alloca i32, align 4
  store i32 %call1, i32* %__comma_49, align 8
  %10 = load i32, i32* %__comma_49, align 4
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0), i32 %10)
  ret i32 0
}

define i32 @_Z5test1Pii(i32* %aList, i32 %Length) {
entry:
  %Length.addr = alloca i32, align 4
  %aList.addr = alloca i32*, align 8
  %i = alloca i32, align 4
  %sum = alloca i32, align 4
  store i32* %aList, i32** %aList.addr, align 8
  store i32 %Length, i32* %Length.addr, align 4
  %sum_1_56 = alloca <4 x i32>, align 8
  store <4 x i32> zeroinitializer, <4 x i32>* %sum_1_56, align 8
  %i_49 = alloca i32, align 4
  store i32 0, i32* %i_49, align 8
  %"<preg>_60" = alloca i64, align 8
  %0 = ptrtoint i32* %aList to i64
  store i64 %0, i64* %"<preg>_60", align 8
  br label %LABEL_2306

LABEL_2306:                                       ; preds = %LABEL_2306, %entry
  %1 = load <4 x i32>, <4 x i32>* %sum_1_56, align 8
  %2 = load i64, i64* %"<preg>_60", align 8
  %3 = inttoptr i64 %2 to i32*
  %4 = bitcast i32* %3 to <4 x i32>*
  %wide.load = load <4 x i32>, <4 x i32>* %4, align 4
  %add = add <4 x i32> %1, %wide.load
  store <4 x i32> %add, <4 x i32>* %sum_1_56, align 8
  %5 = load i32, i32* %i_49, align 4
  %add.1 = add i32 %5, 4
  store i32 %add.1, i32* %i_49, align 8
  %6 = load i64, i64* %"<preg>_60", align 8
  %add.2 = add i64 %6, 16
  store i64 %add.2, i64* %"<preg>_60", align 8
  %7 = load i32, i32* %i_49, align 4
  %cmp2 = icmp sle i32 %7, 39
  br i1 %cmp2, label %LABEL_2306, label %label_end

label_end:                                        ; preds = %LABEL_2306
  %8 = load <4 x i32>, <4 x i32>* %sum_1_56, align 8
  %rdx.shuf = shufflevector <4 x i32> %8, <4 x i32> undef, <4 x i32> <i32 2, i32 3, i32 undef, i32 undef>
  %bin.rdx1 = add <4 x i32> %8, %rdx.shuf
  %rdx.shuf1 = shufflevector <4 x i32> %bin.rdx1, <4 x i32> undef, <4 x i32> <i32 1, i32 undef, i32 undef, i32 undef>
  %bin.rdx2 = add <4 x i32> %bin.rdx1, %rdx.shuf1
  %9 = extractelement <4 x i32> %bin.rdx2, i32 0
  ret i32 %9
}

declare i32 @printf(i8*, ...)
