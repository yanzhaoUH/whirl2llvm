LLVM_CONFIG?=llvm-config
  
   CXX=g++
   C=gcc
   #CXX=g++wrapper
   #C=gccwrapper
   SRC_DIR?=$(PWD)

   LDFLAGS+=$(shell $(LLVM_CONFIG) --ldflags)
   COMMON_FLAGS=-Wall -Wextra

   CXXFLAGS=$(COMMON_FLAGS) $(shell $(LLVM_CONFIG) --cxxflags) -Dlinux 
   CPPFLAGS=$(shell $(LLVM_CONFIG) --cppflags) -I$(SRC_DIR)/src

   LLVMLIBS=$(shell $(LLVM_CONFIG) --libs bitwriter support)
   SYSTEMLIBS=$(shell $(LLVM_CONFIG) --system-libs)

#  

CPPFLAGS += -m64 -mfpmath=sse -msse2 -D_NOTHREADS -D_SGI_SOURCE -D_LANGUAGE_C_PLUS_PLUS -Wformat -funsigned-char -D__GNU_BUG_WORKAROUND   


CXXFLAGS += -DVENDOR_OSP    -DMONGOOSE_BE   -DInsist_On -DDEBUG_IR -DIR_TOOLS -D_NEW_SYMTAB -D__GNU_BUG_WORKAROUND -D__MIPS_AND_IA64_ELF_H    -I../../../../openuh/osprey/ir_tools -I/home/yan/openuh-build/osprey/../../openuh/osprey/common/com -I/home/yan/openuh-build/osprey/../../openuh/osprey/common/com/x8664 -I/home/yan/openuh-build/osprey/../../openuh/osprey/common/util -I/home/yan/openuh-build/osprey/../../openuh/osprey/be/com -I./ -I/home/yan/openuh/osprey/ir_tools/  -I/home/yan/openuh-build/osprey/../../openuh/osprey/be/opt  -I/home/yan/openuh-build/osprey/../../openuh/osprey/include -I/home/yan/openuh-build/osprey/.././osprey/targdir/include/libelf -DCROSS_COMPILATION -fPIC -DTARG_X8664 -D__STDC_LIMIT_MACROS -DKEY -DOSP_OPT -DPATHSCALE_MERGE -DPSC_TO_OPEN64 -DSHARED_BUILD -D_GNU_SOURCE -DIR_TOOLS -D_NEW_SYMTAB -D__GNU_BUG_WORKAROUND -D__MIPS_AND_IA64_ELF_H    -I/home/yan/openuh-build/osprey/.././osprey/targdir/include -I/home/yan/openuh-build/osprey/../. -I/include -I/home/yan/openuh-build/osprey/../../openuh/osprey/include   -g -O0 -D_MIPSEL -D_LONGLONG -D_MIPS_SZINT=32 -D_MIPS_SZPTR=32 -D_MIPS_SZLONG=32 -MMD



CFLAGS = -DVENDOR_OSP    -DInsist_On -DDEBUG_IR -DIR_TOOLS -D_NEW_SYMTAB -D__GNU_BUG_WORKAROUND -D__MIPS_AND_IA64_ELF_H -I../../../../openuh/osprey/ir_tools -I/home/yan/openuh-build/osprey/../../openuh/osprey/common/com -I/home/yan/openuh-build/osprey/../../openuh/osprey/common/com/x8664 -I/home/yan/openuh-build/osprey/../../openuh/osprey/common/util -I/home/yan/openuh-build/osprey/../../openuh/osprey/be/com -I/home/yan/openuh-build/osprey/../../openuh/osprey/be/opt  -I/home/yan/openuh-build/osprey/../../openuh/osprey/include -I/home/yan/openuh-build/osprey/.././osprey/targdir/include/libelf -DCROSS_COMPILATION  -fPIC -DTARG_X8664 -D__STDC_LIMIT_MACROS -DKEY -DOSP_OPT -DPATHSCALE_MERGE -DPSC_TO_OPEN64   -DSHARED_BUILD -std=c++11 -D_GNU_SOURCE  -I/home/yan/openuh-build/osprey/.././osprey/targdir/include -I/home/yan/openuh-build/osprey/../. -I/include -I/home/yan/openuh-build/osprey/../../openuh/osprey/include  -g -O0 -D_MIPSEL -D_LONGLONG -D_MIPS_SZINT=32 -D_MIPS_SZPTR=32 -D_MIPS_SZLONG=32 -MMD



CPPFILE := $(wildcard src/*.cpp)

CFILE := $(wildcard src/*.c)

CXXFILE := $(wildcard src/*.cxx)

OBJSFILE := $(addprefix obj/, $(notdir $(CXXFILE:.cxx=.o)))

#OBJSFILE := $(OBJSFILE)  $(addprefix obj/,$(notdir $(CPPFILE:.cpp=.o)))

OBJSFILE := $(OBJSFILE)  $(addprefix obj/,$(notdir $(CFILE:.c=.o)))







obj/%.o : src/%.cpp
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) $< -o $@

obj/%.o : src/%.cxx
	@echo $(CXXFILE)
	@echo $(OBJSFILE)
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) $< -o $@

obj/%.o : src/%.c
	$(C) -c $(CPPFLAGS) $(CFLAGS) $< -o $@



all: whirl2llvm

whirl2llvm : $(OBJSFILE)
	$(CXX) $(CPPFLAGS)  -o whirl2llvm   $(CXXFLAGS) $(LDFLAGS) $(OBJSFILE) /home/yan/openuh64/osprey/targdir/libcomutil/libcomutil.a  -lm    $(LLVMLIBS) $(SYSTEMLIBS) 


clean:
	rm $(OBJSFILE)



