#ifndef W2L_GLOBAL
#define W2L_GLOBAL

/*
#include <llvm/ADT/SmallVector.h>
#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/CallingConv.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>


#include <llvm/IR/Constants.h>*/


#include <llvm/Pass.h>
#include <llvm/ADT/SmallVector.h>
//#include <llvm/Analysis/Verifier.h>
#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/CallingConv.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/GlobalVariable.h>
#include <llvm/IR/IRPrintingPasses.h>
#include <llvm/IR/InlineAsm.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/Bitcode/ReaderWriter.h>
#include <llvm/Support/FormattedStream.h>
#include <llvm/Support/MathExtras.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/Support/ToolOutputFile.h>

#include "mtypes.h"
#include <set>
#include <string>
#include <stdio.h>

#include "Field_llvm.h"


using namespace std;

//Yan I made my onw type, maybe I should change it later.
typedef enum
{
  w2lB = 1 ,w2lI, w2lU, w2lA, w2lF, w2lFQ, w2lC, w2lCQ, w2lV, w2lM, w2lBS, w2lAR,  
} W2LTYPE;

typedef struct
{
  char symName[128];
  char typeName[128];
  W2LTYPE w2ltype;
  
  const TY* pTY;
  const TY* pPointerTY; // if pTY a pointer, what type does this pointer point to? 
  
 
  
  bool pointer;
  int numPointer;
  enum ST_SCLASS w2lStore;
  
  ST_IDX st_idx;
  
  //MTYPE_t w2ltype;
  int typeLen;
  int pointerTypeLen;
  int align;
} W2LSTRUCT;





typedef struct
{
	char symName[128];
		
	int w2lLevel;
	int w2lIndex;
	
} W2LSYM;

extern W2LSTRUCT w2lStr;
extern llvm::Module *w2lMod;

extern llvm::Value *g_FunReturn;

extern string g_symTabName;

extern char LLVM_BC_FILENAME[255];
extern map<string, W2LSTRUCT> symMap;
extern map<string, W2LSTRUCT> globalSymMap;
extern map<string, llvm::AllocaInst *> w2lAllocMap; 
extern map<string, llvm::AllocaInst *> w2lPregAllocMap; 

//extern map<string, W2LSTRUCT> globalSymMap;

extern map<string, llvm::Value* > globalConstMap;

extern map<string, llvm::Function *> w2lFunMap;

extern llvm::GlobalVariable* gvar_array__str;
extern map<string,vector<W2LFIELD> > w2lstructMap;
extern map<string, llvm::StructType * > w2lstructTyMap;

extern map<string, llvm::Value *> w2lpregMap;
extern map<string, llvm::PHINode *> w2lPHIMap;

extern map<string, llvm::ArrayType * > w2larrayTyMap;
extern map<string, llvm::Value* > w2lInitMap;
extern map<string, llvm::Value*> w2lGlobalValueMap;

extern map<string, llvm::BasicBlock*> g_tfBranchMap;

class WN;
//void IR_put_func_llvm(WN * wn, FILE *f /*, bool createFun = true*/);
void Print_local_symtab_llvm (FILE *f, const SCOPE& scope); 
string buildSymName_llvm(const ST* wn_st);
void Print_global_symtab_llvm (FILE *f);
llvm::Type* createLLVMType_llvm(const TY& ty);
llvm::Type* createScalarLLVMType_llvm(const TYPE_ID type_id);
extern void IR_put_func_llvm(WN * wn, FILE *f /*, bool createFun*/);
int getAlignmentFromTy_llvm(const TY& ty);
#endif
