/*
 * Copyright (C) 2014 University of Houston. All Rights Reserved.
 */

/*
 * Copyright (C) 2009-2010 Advanced Micro Devices, Inc.  All Rights Reserved.
 */

/*
 * Copyright 2003, 2004, 2005, 2006 PathScale, Inc.  All Rights Reserved.
 */

/*

  Copyright (C) 2000, 2001 Silicon Graphics, Inc.  All Rights Reserved.

  This program is free software; you can redistribute it and/or modify it
  under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it would be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

  Further, this software is distributed without any warranty that it is
  free of the rightful claim of any third person regarding infringement 
  or the like.  Any license provided herein, whether implied or 
  otherwise, applies only to this software file.  Patent licenses, if 
  any, provided herein do not apply to combinations of this program with 
  other software, or any other product whatsoever.  

  You should have received a copy of the GNU General Public License along
  with this program; if not, write the Free Software Foundation, Inc., 59
  Temple Place - Suite 330, Boston MA 02111-1307, USA.

  Contact information:  Silicon Graphics, Inc., 1600 Amphitheatre Pky,
  Mountain View, CA 94043, or:

  http://www.sgi.com

  For further information regarding this notice, see:

  http://oss.sgi.com/projects/GenInfo/NoticeExplan

*/


//-*-c++-*-
/* ====================================================================
 * ====================================================================
 *
 * Module: ir_reader.cxx
 *
 * Revision history:
 *  1-SEP-94 - Original Version
 *
 * Description:
 *
 * ====================================================================
 * ====================================================================
 */

/*************************************************************************
   IR Ascii/Binary Reader/Write

   Interface routines:
      IR_init
      IR_open
      IR_close
      IR_finish
      IR_get_func
      IR_output

 *************************************************************************/

/// \file

#ifdef DRAGON
#include <string>
using namespace std;
#endif
#ifdef USE_PCH
#include "common_com_pch.h"
#endif /* USE_PCH */
#pragma hdrstop
#include <stdio.h>
#include <string>
using namespace::std;
#include <string.h>
#include <sys/types.h>
#ifndef __MINGW32__
#include <sys/mman.h>   
#endif /* __MINGW32__ */
#include <sys/stat.h>
#include <fcntl.h>
#include <stdarg.h>
#include <ctype.h>
#include <unistd.h>
#include <cassert> 
#include <cmplrs/rcodes.h>

#ifndef USE_STANDARD_TYPES
#define USE_STANDARD_TYPES // to allow wn_tree_op.h to include ... vector.h
                           // which uses standard types.
#endif

#include "wn_tree_util.h"
#include "defs.h"
#include "errors.h"
#include "srcpos.h"
#include "opcode.h"
#include "stab.h"
#include "const.h"
#include "targ_const.h"
extern char * Targ_Print (const char *fmt, TCON c );
#include "targ_sim.h"
#include "strtab.h"
#include "irbdata.h"
#include "wn.h"		/* Whirl Node descriptor */
#include "wn_simp.h"    /* Whirl simplifier (can be stubs) */
#include "dwarf_DST.h"
#include "dwarf_DST_mem.h"
#include "ir_reader.h"
#include "tracing.h"
#include "config_opt.h"

#ifdef BACK_END
#include "opt_alias_mgr.h"
#endif /* BACK_END */

#include "wio.h"
#include "wintrinsic.h"
#include "wn_pragmas.h"
#include "wutil.h"
#if defined(TARG_NVISA)
#include "intrn_info.h"
#endif
#ifdef BACK_END
#include "intrn_info.h"
#include "region_util.h"
#include "dvector.h"
#endif /* BACK_END */

#if defined(BACK_END) || defined(IR_TOOLS)
/*for whirl ssa*/
#include "wssa_mgr.h"
#include "wssa_wn.h"
#include "pu_info.h"
#endif

#include <sstream> 


#include <w2l_global.h>


using namespace std; 

using namespace llvm;
/* Extended Ascii-WHIRL */

enum OPR_EXTENDED {
    OPR_END_BLOCK	= OPERATOR_LAST+1,
    OPR_BODY		= OPERATOR_LAST+2,
    OPR_NOOP		= OPERATOR_LAST+3,
    OPR_THEN		= OPERATOR_LAST+4,
    OPR_ELSE		= OPERATOR_LAST+5,
    OPR_END_IF		= OPERATOR_LAST+6,
    OPR_INIT		= OPERATOR_LAST+7,
    OPR_COMP		= OPERATOR_LAST+8,
    OPR_INCR		= OPERATOR_LAST+9,
    OPR_END_COMPGOTO	= OPERATOR_LAST+10,
    OPR_END_XGOTO 	= OPERATOR_LAST+11,
    OPR_LOC		= OPERATOR_LAST+12,
    OPR_END_LINFO	= OPERATOR_LAST+13,
    OPR_END_SWITCH	= OPERATOR_LAST+14,
};

enum OPC_EXTENDED {
    OPC_END_BLOCK	= OPCODE_LAST+1,
    OPC_BODY		= OPCODE_LAST+2,
    OPC_NOOP		= OPCODE_LAST+3,
    OPC_THEN		= OPCODE_LAST+4,
    OPC_ELSE		= OPCODE_LAST+5,
    OPC_END_IF		= OPCODE_LAST+6,
    OPC_INIT		= OPCODE_LAST+7,
    OPC_COMP		= OPCODE_LAST+8,
    OPC_INCR		= OPCODE_LAST+9,
    OPC_END_COMPGOTO	= OPCODE_LAST+10,
    OPC_END_XGOTO 	= OPCODE_LAST+11,
    OPC_LOC		= OPCODE_LAST+12,
    OPC_END_LINFO	= OPCODE_LAST+13,
    OPC_END_SWITCH	= OPCODE_LAST+14,
    MAX_OPCODE		= OPCODE_LAST+15,
};

typedef struct {
  const char *pr_name;
  INT  n_kids;
  INT  flags;
} IR_OPCODE_TABLE;


IR_OPCODE_TABLE ir_opcode_table[1];         /* index by opcode */


#define HASH_LEN	2413	       /* prime number */
#define IR_MAX_ARGS     3              /* IR opcode has at most 3 arguments */

IR_OPCODE_TABLE *opcode_hash[HASH_LEN]; 

#define LINE_LEN  1024  /* maximum length of line */

typedef struct {
  OPCODE opcode;
  INT _operator;
  INT n_kids;
  INT flags;
  ST *st;
  TY_IDX ty;
  TY_IDX load_addr_ty;
  INT id;
  INT num_dim;
  INT num_entries;
  INT last_label;
  WN_OFFSET offset;
  INT16 offset1_of_2;
  INT16 offset2_of_2;
  WN_ESIZE  esize;
  INT16 cvtl_bits;
  INT64 const_val;
  INT32 label_number;
  UINT32 flags_val;
  INT cgoto;
  INT intrinsic;
  char *intrinsic_name;
  char *args[IR_MAX_ARGS+1];
} TOKEN;

static void ir_error(const char *s);
static INT ir_get_expr_list(void);
static WN * ir_get_expr(void);
static WN * ir_get_stmt(void);
static void ir_skip_token(void);
static char *ir_read_line(char *buf, INT size, FILE *ir_file);
static void ir_match_token(OPERATOR opr);
static void ir_expect_token(OPERATOR opc);
static TOKEN *ir_next_token(void);
static void ir_get_token(TOKEN *token);
static BOOL ir_insert_hash(const char *s, IR_OPCODE_TABLE *irt);
static INT ir_lookup(char *s);
static void ir_build_hashtable(void);
static void ir_put_wn(WN * wn, INT indent);
static void ir_put_expr(WN * wn, INT indent);
static void ir_put_marker(const char *str, INT indent);
static void ir_put_stmt(WN * wn, INT indent);
static void WN_TREE_put_stmt(WN *, INT); // fwd declaration

/* Should we dump the tree in prefix order or postfix order? */
/* The default is postfix */

static BOOL dump_parent_before_children = FALSE;

#ifdef BACK_END
/*
**  declaration is problematic, as the ALIAS_MANAGER is not exposed
**  to consumers of ir_reader.h
*/
extern void fdump_dep_tree(FILE *, const WN *, struct ALIAS_MANAGER *);

/*  Suppress warning if not resolved at link-time. */
/* CG_Dump_Region is defined in cg.so, only call if cg.so is loaded */
#if defined(__linux__) || defined(BUILD_OS_DARWIN) || !defined(SHARED_BUILD)
extern void (*CG_Dump_Region_p) (FILE*, WN*);
#define CG_Dump_Region (*CG_Dump_Region_p)
#else
#pragma weak CG_Dump_Region
#endif // __linux__

#endif /* BACK_END */

BOOL IR_dump_map_info = FALSE;
BOOL IR_dump_region = FALSE;
BOOL IR_DUMPDEP_info = FALSE;
BOOL IR_dump_line_numbers = TRUE;

WN_MAP IR_alias_map = WN_MAP_UNDEFINED;
const struct ALIAS_MANAGER *IR_alias_mgr = NULL;
WN_MAP IR_freq_map = WN_MAP_UNDEFINED;

#define OPCODE_has_alias_info(opc)	(OPCODE_is_load(opc) ||\
                                  	 OPCODE_is_store(opc) ||\
                                  	 OPCODE_operator(opc) == OPR_PARM)

#define IR_dump_alias_info(opc)		(IR_alias_map != WN_MAP_UNDEFINED && OPCODE_has_alias_info(opc))

typedef struct DUMPDEP
{
  WN			*node;
  INT32			id;
  struct DUMPDEP	*next;
} DUMPDEP, *DUMPDEPp;

#define	DUMPDEP_node(x)		(x)->node
#define	DUMPDEP_id(x)		(x)->id
#define	DUMPDEP_next(x)		((x)->next)


DUMPDEPp IR_DUMPDEP_head= NULL;
static INT32 AddToDUMPDEP(WN *node);



/*========================================================================
	
    Initialization

 ========================================================================*/

#ifndef MONGOOSE_BE
static FILE *ir_file;
#endif
static FILE *ir_ofile;
static INT  ir_line;
#ifdef IR_TOOLS	
static char *line;
static char *errmsg;
#endif
BOOL follow_st;
static USRCPOS last_srcpos;
static BOOL is_initialized = FALSE;
static WN_MAP ir_put_map = WN_MAP_UNDEFINED;

/// Yan's New function are added here. 
/// You can use gg command to jump here
// this operator need to know the currentFun inforamtion. 
Function* currentFun = NULL;


Value *g_FunReturn;

int g_endLabel = -1;
bool O3 = false;

typedef map<string, FLD*> OFFType;



typedef map<string, Value*> OFVALUE;


map<string, OFVALUE*> struct_value_map;

map<string, OFFType*> struct_field_map;

map<int, BasicBlock*> g_switchCaseMap;

map<int, BasicBlock*> g_caseMap;

map<string, BasicBlock*> g_tfBranchMap;


stack<BasicBlock*> g_loopEndStack;
bool g_gotoFlag = false;

map<string, AllocaInst *> w2lAllocMap; 
map<string, AllocaInst *> w2lPregAllocMap; 

map<string, BasicBlock*> w2lEntryMap;
map<string, Function *> w2lFunMap;
map<string, Value*> w2lGlobalValueMap;

map<string, Value* > w2lInitMap;

map<string, llvm::Value *> w2lpregMap;

pair<string, Value* > w2lJustCall; 

map<string, llvm::PHINode *> w2lPHIMap;

set<BasicBlock* > w2lSetReturn;

bool IsLongInt  = false;

BasicBlock* g_entry_labelEntry = nullptr;
Instruction* g_entry_insert = nullptr;


static void ir_put_stmt_llvm(WN * wn, INT indent, BasicBlock *&labelEntry);


string buildSymName_llvm(const ST* wn_st){	
	char name_whole[256];

	strcpy(name_whole, ST_name(wn_st) );
	strcat(name_whole, "_");

	int st_order_whole = ST_IDX_index (wn_st->st_idx);
	string st_order_str = to_string(st_order_whole);
	strcat(name_whole, st_order_str.c_str() );
	string result(name_whole);
	return result;
}

Function* g_llvm_memcpy = nullptr;

Function* getIntrinsicMemcpy_llvm(){

	if(w2lFunMap.find("memcpy")!=w2lFunMap.end()){
		return w2lFunMap["memcpy"];
	}
	
	//if(g_llvm_memcpy != nullptr)
	//	return g_llvm_memcpy;
	
	PointerType* PointerTy_7 = PointerType::get(
								   IntegerType::get(w2lMod->getContext(), 8), 0);
	std::vector<Type*>FuncTy_17_args;
	FuncTy_17_args.push_back(PointerTy_7);
	FuncTy_17_args.push_back(PointerTy_7);
	FuncTy_17_args.push_back(IntegerType::get(w2lMod->getContext(), 64));
	FuncTy_17_args.push_back(IntegerType::get(w2lMod->getContext(), 32));
	FuncTy_17_args.push_back(IntegerType::get(w2lMod->getContext(), 1));
	FunctionType* FuncTy_17 = FunctionType::get(
								  /*Result=*/Type::getVoidTy(w2lMod->getContext()),
								  /*Params=*/FuncTy_17_args,
								  /*isVarArg=*/false);
	
	
	
	Function* func_llvm_memcpy_p0i8_p0i8_i64 = 
		w2lMod->getFunction("llvm.memcpy.p0i8.p0i8.i64");
	if (!func_llvm_memcpy_p0i8_p0i8_i64) {
		func_llvm_memcpy_p0i8_p0i8_i64 = Function::Create(
											 /*Type=*/FuncTy_17,
											 /*Linkage=*/GlobalValue::ExternalLinkage,
											 /*Name=*/"llvm.memcpy.p0i8.p0i8.i64", w2lMod); // (external, no body)
		func_llvm_memcpy_p0i8_p0i8_i64->setCallingConv(CallingConv::C);
	}
	AttributeSet func_llvm_memcpy_p0i8_p0i8_i64_PAL;
	{
		SmallVector<AttributeSet, 4> Attrs;
		AttributeSet PAS;
		{
			AttrBuilder B;
			B.addAttribute(Attribute::NoCapture);
			PAS = AttributeSet::get(w2lMod->getContext(), 1U, B);
		}
		
		Attrs.push_back(PAS);
		{
			AttrBuilder B;
			B.addAttribute(Attribute::ReadOnly);
			B.addAttribute(Attribute::NoCapture);
			PAS = AttributeSet::get(w2lMod->getContext(), 2U, B);
		}
		
		Attrs.push_back(PAS);
		{
			AttrBuilder B;
			B.addAttribute(Attribute::NoUnwind);
			PAS = AttributeSet::get(w2lMod->getContext(), ~0U, B);
		}

		Attrs.push_back(PAS);
		func_llvm_memcpy_p0i8_p0i8_i64_PAL = 
			AttributeSet::get(w2lMod->getContext(), Attrs);

	}
	func_llvm_memcpy_p0i8_p0i8_i64->setAttributes(
		func_llvm_memcpy_p0i8_p0i8_i64_PAL);

	w2lFunMap.insert(pair<string,  Function* > ("memcpy" ,func_llvm_memcpy_p0i8_p0i8_i64) );
	
	return func_llvm_memcpy_p0i8_p0i8_i64;
	//g_llvm_memcpy = func_llvm_memcpy_p0i8_p0i8_i64;
	//return g_llvm_memcpy;
}


Function* getIntrinsicStrcpy_llvm(){

	if(w2lFunMap.find("strcpy")!=w2lFunMap.end()){
		return w2lFunMap["strcpy"];
	}
	
	PointerType* PointerTy_14 = PointerType::get(
									IntegerType::get(w2lMod->getContext(), 8), 0);
	
	std::vector<Type*>FuncTy_18_args;
	FuncTy_18_args.push_back(PointerTy_14);
	FuncTy_18_args.push_back(PointerTy_14);
	FunctionType* FuncTy_18 = FunctionType::get(
								  /*Result=*/PointerTy_14,
								  /*Params=*/FuncTy_18_args,
								  /*isVarArg=*/false);

	Function* func_strcpy = w2lMod->getFunction("strcpy");
	if (!func_strcpy) {
		func_strcpy = Function::Create(
						  /*Type=*/FuncTy_18,
						  /*Linkage=*/GlobalValue::ExternalLinkage,
						  /*Name=*/"strcpy", w2lMod); // (external, no body)
		func_strcpy->setCallingConv(CallingConv::C);
	}
	AttributeSet func_strcpy_PAL;
	{
		SmallVector<AttributeSet, 4> Attrs;
		AttributeSet PAS;
		{
			AttrBuilder B;
			B.addAttribute(Attribute::NoUnwind);
			PAS = AttributeSet::get(w2lMod->getContext(), ~0U, B);
		}
		
		Attrs.push_back(PAS);
		func_strcpy_PAL = AttributeSet::get(w2lMod->getContext(), Attrs);
		
	}
	func_strcpy->setAttributes(func_strcpy_PAL);
	

	w2lFunMap.insert(pair<string,  Function* > ("strcpy" ,func_strcpy) );
	return func_strcpy;
	//g_llvm_memcpy = func_llvm_memcpy_p0i8_p0i8_i64;
	//return g_llvm_memcpy;
}


Function* getIntrinsicMaxnum_f32_llvm(){  

	if(w2lFunMap.find("maxnum_f32")!=w2lFunMap.end()){
		return w2lFunMap["maxnum_f32"];
	}
	
	std::vector<Type*>FuncTy_2_args;
	FuncTy_2_args.push_back(Type::getFloatTy(w2lMod->getContext()));
	FuncTy_2_args.push_back(Type::getFloatTy(w2lMod->getContext()));
	FunctionType* FuncTy_2 = FunctionType::get(
								 /*Result=*/Type::getFloatTy(w2lMod->getContext()),
								 /*Params=*/FuncTy_2_args,
								 /*isVarArg=*/false);


	Function* func_llvm_maxnum_f32 = w2lMod->getFunction("llvm.maxnum.f32");
	if (!func_llvm_maxnum_f32) {
		func_llvm_maxnum_f32 = Function::Create(
								   /*Type=*/FuncTy_2,
								   /*Linkage=*/GlobalValue::ExternalLinkage,
								   /*Name=*/"llvm.maxnum.f32", w2lMod); // (external, no body)
		func_llvm_maxnum_f32->setCallingConv(CallingConv::C);
	}
	AttributeSet func_llvm_maxnum_f32_PAL;
	{
		SmallVector<AttributeSet, 4> Attrs;
		AttributeSet PAS;
		{
			AttrBuilder B;
			B.addAttribute(Attribute::NoUnwind);
			B.addAttribute(Attribute::ReadNone);
			PAS = AttributeSet::get(w2lMod->getContext(), ~0U, B);
		}
		
		Attrs.push_back(PAS);
		func_llvm_maxnum_f32_PAL = AttributeSet::get(w2lMod->getContext(), Attrs);
		
	}
	func_llvm_maxnum_f32->setAttributes(func_llvm_maxnum_f32_PAL);
	
	
	
	w2lFunMap.insert(pair<string,  Function* > ("maxnum_f32" ,func_llvm_maxnum_f32) );
	return func_llvm_maxnum_f32;
	
	//g_llvm_memcpy = func_llvm_memcpy_p0i8_p0i8_i64;
	//return g_llvm_memcpy;
}


Function* getIntrinsicMaxnum_v4f32_llvm(){

	if(w2lFunMap.find("maxnum_v4f32")!=w2lFunMap.end()){
		return w2lFunMap["maxnum_v4f32"];
	}
	
	VectorType* VectorTy_3 = VectorType::get(Type::getFloatTy(w2lMod->getContext()), 4);
	
	std::vector<Type*>FuncTy_4_args;
	FuncTy_4_args.push_back(VectorTy_3);
	FuncTy_4_args.push_back(VectorTy_3);
	FunctionType* FuncTy_4 = FunctionType::get(
								 /*Result=*/VectorTy_3,
								 /*Params=*/FuncTy_4_args,
								 /*isVarArg=*/false);


	Function* func_llvm_maxnum_v4f32 = w2lMod->getFunction("llvm.maxnum.v4f32");
	if (!func_llvm_maxnum_v4f32) {
		func_llvm_maxnum_v4f32 = Function::Create(
									 /*Type=*/FuncTy_4,
									 /*Linkage=*/GlobalValue::ExternalLinkage,
									 /*Name=*/"llvm.maxnum.v4f32", w2lMod); // (external, no body)
		func_llvm_maxnum_v4f32->setCallingConv(CallingConv::C);
	}
	AttributeSet func_llvm_maxnum_v4f32_PAL;
	{
		SmallVector<AttributeSet, 4> Attrs;
		AttributeSet PAS;
		{
			AttrBuilder B;
			B.addAttribute(Attribute::NoUnwind);
			B.addAttribute(Attribute::ReadNone);
			PAS = AttributeSet::get(w2lMod->getContext(), ~0U, B);
		}
		
		Attrs.push_back(PAS);
		func_llvm_maxnum_v4f32_PAL = AttributeSet::get(w2lMod->getContext(), Attrs);
		
	}
	func_llvm_maxnum_v4f32->setAttributes(func_llvm_maxnum_v4f32_PAL);
	
	
	w2lFunMap.insert(pair<string,  Function* > ("maxnum_v4f32" ,func_llvm_maxnum_v4f32) );
	return func_llvm_maxnum_v4f32;
	
	//g_llvm_memcpy = func_llvm_memcpy_p0i8_p0i8_i64;
	//return g_llvm_memcpy;
}

Function* getIntrinsicStrcmp_llvm(){

	if(w2lFunMap.find("strcmp")!=w2lFunMap.end()){
		return w2lFunMap["strcmp"];
	}
	
	PointerType* PointerTy_14 = PointerType::get(
									IntegerType::get(w2lMod->getContext(), 8), 0);


	std::vector<Type*>FuncTy_20_args;
	FuncTy_20_args.push_back(PointerTy_14);  
	FuncTy_20_args.push_back(PointerTy_14);
	FunctionType* FuncTy_20 = FunctionType::get(
								  /*Result=*/IntegerType::get(w2lMod->getContext(), 32),
								  /*Params=*/FuncTy_20_args,
								  /*isVarArg=*/false);


	Function* func_strcmp = w2lMod->getFunction("strcmp");
	if (!func_strcmp) {
		func_strcmp = Function::Create(
						  /*Type=*/FuncTy_20,
						  /*Linkage=*/GlobalValue::ExternalLinkage,
						  /*Name=*/"strcmp", w2lMod); // (external, no body)
		func_strcmp->setCallingConv(CallingConv::C);
	}
	AttributeSet func_strcmp_PAL;
	{
		SmallVector<AttributeSet, 4> Attrs;
		AttributeSet PAS;
		{
			AttrBuilder B;
			B.addAttribute(Attribute::NoUnwind);
			B.addAttribute(Attribute::ReadOnly);
			PAS = AttributeSet::get(w2lMod->getContext(), ~0U, B);
		}
		
		Attrs.push_back(PAS);
		func_strcmp_PAL = AttributeSet::get(w2lMod->getContext(), Attrs);
		
	}
	func_strcmp->setAttributes(func_strcmp_PAL);
	
	w2lFunMap.insert(pair<string,  Function* > ("strcmp" ,func_strcmp) );
	return func_strcmp;
	
	//g_llvm_memcpy = func_llvm_memcpy_p0i8_p0i8_i64;
	//return g_llvm_memcpy;
}




Function* getIntrinsicFun_llvm(string funName){

	if(funName=="memcpy")
		return getIntrinsicMemcpy_llvm();
	else if(funName=="strcpy")
		return getIntrinsicStrcpy_llvm();
	else if(funName=="strcmp")
		return getIntrinsicStrcmp_llvm();
	else if(funName=="maxnum_f32")
		return getIntrinsicMaxnum_f32_llvm();
	else if(funName=="maxnum_v4f32")
		return getIntrinsicMaxnum_v4f32_llvm();
	else{
		assert(false && "missing intrinsic fun defination here");
	}
}




Type* createScalarLLVMTypeFromTypeID_llvm(const TYPE_ID ty_id){
	Type* resultType = nullptr;
	
	if(ty_id == MTYPE_I1 || ty_id == MTYPE_U1){
		resultType = IntegerType::get(w2lMod->getContext(), 8);
	}
	else if(ty_id == MTYPE_I1 || ty_id == MTYPE_U1){
		resultType = IntegerType::get(w2lMod->getContext(), 8);
	}
	else if(ty_id == MTYPE_I2 || ty_id == MTYPE_U2 ){
		resultType = IntegerType::get(w2lMod->getContext(), 16);
	}
	else if(ty_id == MTYPE_I4 || ty_id == MTYPE_U4){
		resultType = IntegerType::get(w2lMod->getContext(), 32);
	}
	else if(ty_id == MTYPE_I8 || ty_id == MTYPE_U8){
		resultType = IntegerType::get(w2lMod->getContext(), 64);
	}
	else if(ty_id == MTYPE_F4){
	    resultType = Type::getFloatTy(w2lMod->getContext());
	}
	else if(ty_id == MTYPE_F8){
	    resultType = Type::getDoubleTy(w2lMod->getContext());
	}
	else if(ty_id == MTYPE_F10){
	    resultType = Type::getX86_FP80Ty(w2lMod->getContext());
	}
	else if(ty_id == MTYPE_V16I4){
		//VectorType* VectorTy_yan_4 = 
		resultType = VectorType::get(IntegerType::get(w2lMod->getContext(), 32), 4);
	}
	else if(ty_id == MTYPE_V16F4){
		//VectorType* VectorTy_yan_4 = 
		resultType = VectorType::get(Type::getFloatTy(w2lMod->getContext()), 4);
	}
    else{
		assert(false &&"missing type mapping information here");
	}
	
    assert(resultType != nullptr &&" resultType is nullptr here");
	return resultType;
}




bool getValueType_llvm(TYPE_ID desc, TYPE_ID &vTY_ID, Value* val_orig){

	
	vTY_ID = desc;
	if( (val_orig->getType())->isIntegerTy() ){
	   IntegerType* iTy = (IntegerType*)val_orig->getType();
	   int bitWidth = iTy->getBitWidth();
	   if(bitWidth == 1){
		   vTY_ID = MTYPE_B;
	   }
	   /*
	   else if(bitWidth == 8){
		   vTY_ID = MTYPE_I1;
	   }
	   else if(bitWidth == 16){
		   vTY_ID = MTYPE_I2;
	   }
	   else if(bitWidth == 32){
		   vTY_ID = MTYPE_I4;
	   }
	   else if(bitWidth == 64){
		   vTY_ID = MTYPE_I8;
	   }  */ 
	}
	if(vTY_ID != desc)
		return true;
	return false;
}


Value* convert_llvm(TYPE_ID rtype, TYPE_ID desc, Value* val_orig, BasicBlock *labelEntry ){

	//1) 
	
	
	if(rtype == desc)
		return val_orig;
	
	Value* val = nullptr;
	int bitsize = MTYPE_bit_size(rtype);
	
	//1) This is to deal with CIOR statement, detail can be seen test10.c file
	//another example can be seen in test21.c
	if(dyn_cast<PHINode>(val_orig) != nullptr && (val_orig->getType())->isIntegerTy()){		
		llvm::Type* llvm_ty=val_orig->getType();
		//if(llvm_ty->isIntegerTy()){		
			if(dyn_cast<IntegerType>(llvm_ty)->getBitWidth() == 1 && rtype == MTYPE_I4){
			    val = new ZExtInst(val_orig, 
					IntegerType::get(w2lMod->getContext(), bitsize), "conv", labelEntry);
			}
			else{
				val = val_orig;
			}
		//}
	}
	else if( (rtype == MTYPE_U4 ) &&
			 (desc == MTYPE_B)){			
		val = new ZExtInst(val_orig, 
						   IntegerType::get(w2lMod->getContext(), bitsize), "conv3", labelEntry);				
	} 
	else if( (rtype == MTYPE_I4 ) &&
			 (desc == MTYPE_B)){			
		val = new SExtInst(val_orig, 
						   IntegerType::get(w2lMod->getContext(), bitsize), "conv3", labelEntry);				
	} 
	else if( (rtype == MTYPE_I4 ) &&
			 (desc == MTYPE_I8)){			
		val = new TruncInst(val_orig, 
							IntegerType::get(w2lMod->getContext(), bitsize), "conv", labelEntry);					
	}
	else if( (rtype == MTYPE_I1 || rtype == MTYPE_I2 || rtype == MTYPE_U2 ) &&
       (desc == MTYPE_I4 || desc == MTYPE_I8 || desc == MTYPE_U4 || desc == MTYPE_U8)){			
		
		val = new TruncInst(val_orig, 
				IntegerType::get(w2lMod->getContext(), bitsize), "conv", labelEntry);					
	}	
	else if( (desc == MTYPE_I2 || desc == MTYPE_I1) && rtype == MTYPE_I4){
		val = new SExtInst(val_orig, 
						   IntegerType::get(w2lMod->getContext(), bitsize), "conv3", labelEntry);
		
	}
	else if( (desc == MTYPE_I4 ) && rtype == MTYPE_I8){
		val = new SExtInst(val_orig, 
						   IntegerType::get(w2lMod->getContext(), bitsize), "conv3", labelEntry);
		
	}
	else if( desc == MTYPE_U4 && (rtype == MTYPE_I8 || rtype == MTYPE_U8) ){
		val = new ZExtInst(val_orig, 
					   IntegerType::get(w2lMod->getContext(), 64), "conv3", labelEntry);
		
	}
	else if( (desc == MTYPE_U2 || desc == MTYPE_U1) && rtype == MTYPE_U4){
		val = new ZExtInst(val_orig, 
					   IntegerType::get(w2lMod->getContext(), 32), "conv3", labelEntry);
		
	}
	else	{
		val = val_orig;
	}
	
	assert(val != nullptr && "val is null ");
	return val;
}




string getSymNameFromWN(const WN* wn)
{
	ST_IDX st_idx = WN_st_idx(wn);
	assert(st_idx != (ST_IDX) 0 && "st_idx is 0 error");
	const ST* st = &St_Table[st_idx];
	string symName = ST_name (st);
	return symName;
}
string getTypeName(const WN* wn, BasicBlock * labelEntry)
{
	    string funName = labelEntry->getParent()->getName();
		
		ST_IDX st_idx= WN_st_idx(wn);
		if( st_idx == (ST_IDX) 0){
			fprintf(stderr, "error getTypeName \n");
			exit(1);
		}
		const ST* st = &St_Table[st_idx];
		char name[256];
		char *p;	
		strcpy(name, ST_name(st) );
		int st_order = ST_IDX_index (st_idx);
		string st_order_str = to_string(st_order);
		strcat(name, st_order_str.c_str() );
		
		//string keyName = "";
		//keyName = funName;
		//keyName+="_";
		//keyName += name;

		assert(symMap.find(name)!= symMap.end() && "error symMap in getTypeName");
		
		W2LSTRUCT w2l = symMap[name];
		string temp(w2l.typeName);
	  	return temp;
}

static W2LSYM
Get_Name_Level_Num_llvm (ST_IDX st_idx)
{
	W2LSYM w2lsym;
	
	char *name;
	char *p;
	
	assert(st_idx != (ST_IDX) 0 &&"error in Get_Name_Level_Num_llvm");

	
	if (!follow_st) {
		/* Do not follow ST *, so that it can dump useful information
		   even when ST * is not valid */
		fprintf(ir_ofile, " <st %d>", (INT32) st_idx);

	} else {
		const ST* st = &St_Table[st_idx];
		if (ST_class(st) == CLASS_CONST) {
		name = "<constant>";	
	//		name = Targ_Print(NULL, STC_val(st));
			/* new lines and spaces in constant strings 
			 * will mess up the ascii reader,
			 * so replace with underlines */
	//		for (p = name; *p != '\0'; p++)
	//			switch (*p) {
	//				case ' ':
	//				case '\t':
	//				case '\n':
	//					*p = '_';
	//			}
		} else 
			name = ST_name(st);
		
		strcpy(w2lsym.symName , name);
		w2lsym.w2lLevel = ST_level (st);
		w2lsym.w2lIndex = ST_index (st);
		//fprintf (ir_ofile, " <%d,%d,%s>", ST_level (st), ST_index (st), name);
		return w2lsym;
	}
}


bool previousIsReturn(BasicBlock *labelEntry){
	if(!labelEntry->empty() && dyn_cast<ReturnInst>(&(labelEntry->back())) != nullptr)
		return true;
	return false;
}


//Yan this function is not used any more, just keep it. 
void createFuncsTyargs_llvm(SmallVectorImpl<Type*> &FuncTyArgs, WN* wn)
{
	// below is to get function name. 
	W2LSYM w2lsym0 = Get_Name_Level_Num_llvm (WN_st_idx(wn));
	string funName = w2lsym0.symName;
	
	unsigned kidNum = unsigned( WN_kid_count(wn)-3 );
	
	
	for (int i = 0; i < kidNum; i++) {
		
		WN* wn2 = WN_kid(wn,i);
		if (wn2) {
			OPCODE opc2 = WN_opcode(wn2);
			if(opc2!=OPC_IDNAME) {  
				printf("### error: WN opcode is not OPC_IDNAME");
				return ;
			}
			
			W2LSYM w2lsym1 = Get_Name_Level_Num_llvm (WN_st_idx(wn2));
			
			string idName(w2lsym1.symName);
			//idName+="_";
			//idName += ;
			//strcpy(w2lsym1.symName,idName.c_str());
			
			assert(symMap.find(idName)!= symMap.end() && 
				   " error of symMap in createFuncsTyargs_llvm");
			
			if(symMap.find(idName)!= symMap.end())
			{
				fprintf(stdout, "IDNAME from symMap %s %d %d\n", symMap[idName].symName, symMap[idName].w2ltype, symMap[idName].typeLen );
				W2LSTRUCT w2lTemp = symMap[idName];
				
				if(w2lTemp.w2ltype == w2lI )
				{
					FuncTyArgs.push_back(IntegerType::get(w2lMod->getContext(),w2lTemp.typeLen));
				}
				else if(w2lTemp.w2ltype == w2lU)
				{
					
				}
				
			}				 
			
		}
	}
	
}

Function* createFunc_llvm(WN * wn, SmallVector<Type*,32> &FuncTyArgs )
{
	
	W2LSYM w2lsym1 = Get_Name_Level_Num_llvm (WN_st_idx(wn));
	W2LSTRUCT w2lTemp; 
	
	string str(w2lsym1.symName);
	assert(globalSymMap.find(str) != globalSymMap.end() &&" error in createFunc_llvm");
	
	
		fprintf(stdout, "from global sysmap %d %d \n", globalSymMap[str].typeLen, globalSymMap[str].w2ltype); 
		w2lTemp = globalSymMap[str];
	
	
	Function *funcSum;
	
	if(w2lTemp.w2ltype == w2lI )
	{
		FunctionType *FuncTy = FunctionType::get(IntegerType::get(w2lMod->getContext(),
							   w2lTemp.typeLen), FuncTyArgs, false);
		funcSum = 
			Function::Create(FuncTy, GlobalValue::ExternalLinkage, w2lTemp.symName , w2lMod);
		funcSum->setCallingConv(CallingConv::C);
	}	
	
	return funcSum;
}

//Yan, in this function, just add IDNAME to args
//args's type has been decided by createExternFun_llvm() in symtab.cxx file
//Here just give aList and Length to each argu
/*FUNC_ENTRY <1,50,_Z5test1Pii> {line: 1/1}
  IDNAME 0 <2,4,aList>
  IDNAME 0 <2,5,Length>*/
void createFuncArgs_llvm(WN * wn,Function *funcSum)
{
	unsigned kidNum = unsigned( WN_kid_count(wn)-3 );
	
	Function::arg_iterator args = funcSum->arg_begin();
	
	for (int i = 0; i < kidNum; i++)
	{
		WN* wn2 = WN_kid(wn,i);
		if (wn2) {
			OPCODE opc2 = WN_opcode(wn2);
			assert(opc2==OPC_IDNAME &&"error: WN opcode is not OPC_IDNAME\n");
			//string symName = getSymNameFromWN(wn2);
			//W2LSYM w2lsym1 = Get_Name_Level_Num_llvm (WN_st_idx(wn2));
			
			ST_IDX st_idx = WN_st_idx(wn2);
			assert(st_idx != (ST_IDX) 0 && "st_idx error ");
			const ST* wn_st = &St_Table[st_idx];
			
			string symName = buildSymName_llvm(wn_st);
			Value * fun_arg = args++;
			fun_arg->setName(symName);
		}
	}
}

static Value* ir_put_expr_llvm(WN * wn, INT indent, BasicBlock ** labelEntry);

Value* createICMP_llvm(WN* wn, int indent, BasicBlock **labelEntry)
{
	
/*	
eq: equal
ne: not equal
ugt: unsigned greater than
uge: unsigned greater or equal
ult: unsigned less than
ule: unsigned less or equal
sgt: signed greater than
sge: signed greater or equal
slt: signed less than
sle: signed less or equal

*/
	
/*
false: no comparison, always returns false
oeq: ordered and equal
ogt: ordered and greater than
oge: ordered and greater than or equal
olt: ordered and less than
ole: ordered and less than or equal
one: ordered and not equal
ord: ordered (no nans)
ueq: unordered or equal
ugt: unordered or greater than
uge: unordered or greater than or equal
ult: unordered or less than
ule: unordered or less than or equal
une: unordered or not equal
uno: unordered (either nans)
true: no comparison, always returns true
*/
	
	TYPE_ID  rtype = OPCODE_rtype(WN_opcode(wn));
	
	static int icmp_num = 1;
	char icmp_name[256];
	sprintf(icmp_name,"%s%d", "cmp", icmp_num);
	
	WN* wnkid0 = WN_kid(wn,0);
	WN* wnkid1 = WN_kid(wn,1);
	Value *val1 = ir_put_expr_llvm(wnkid0,indent+1, labelEntry);
	Value *val2 = ir_put_expr_llvm(wnkid1,indent+1, labelEntry);
	
	IntegerType* intType = IntegerType::get(w2lMod->getContext(), 64);
	
	
	
	if(val1->getType()->isPointerTy()){
		val1 = new PtrToIntInst(val1, intType,  "", *labelEntry);
	}
	
	if(val2->getType()->isPointerTy()){
		val2 = new PtrToIntInst(val2, intType,  "", *labelEntry);
	}
	
	OPCODE opc = WN_opcode(wn);
	TYPE_ID tyid = OPCODE_desc(opc);
	TYPE_ID tyrid = OPCODE_rtype(opc);
	
	
	TYPE_ID ty_child0 = OPCODE_rtype(WN_opcode(wnkid0));
	TYPE_ID ty_child1 = OPCODE_rtype(WN_opcode(wnkid1));
	
	if(tyid != ty_child0){
		val1 = convert_llvm( tyid, ty_child0, val1, *labelEntry);
	}
	
	if(tyid != ty_child1){
		val2 = convert_llvm( tyid, ty_child1, val2, *labelEntry);
	}
	
	CmpInst* cmp = nullptr;
	if(WN_operator(wn) == OPR_LT){
		
		if(tyid == MTYPE_U8 || tyid == MTYPE_U4){ 
		   cmp = new ICmpInst(**labelEntry, ICmpInst::ICMP_ULT, 
										  val1, val2, icmp_name);
		}
		else if(tyid == MTYPE_I4 || tyid == MTYPE_I8){ 
			cmp = new ICmpInst(**labelEntry, ICmpInst::ICMP_SLT, 
							   val1, val2, icmp_name);
		}
		else if(tyid == MTYPE_F4 || tyid == MTYPE_F8){
			cmp = new FCmpInst(**labelEntry, FCmpInst::FCMP_OLT, 
							   val1, val2, icmp_name);
		}
		else{
			assert(false &&" missing mapping in OPR_LT");
		}
		
	}
	else if(WN_operator(wn) == OPR_LE){
	    if(tyid == MTYPE_U8 || tyid == MTYPE_U4){ 
			cmp = new ICmpInst(**labelEntry, ICmpInst::ICMP_ULE, 
							   val1, val2, icmp_name);
		}
		else if(tyid == MTYPE_I4 || tyid == MTYPE_I8){ 
			cmp = new ICmpInst(**labelEntry, ICmpInst::ICMP_SLE, 
							   val1, val2, icmp_name);
		}
		else if(tyid == MTYPE_F4 || tyid == MTYPE_F8){
			cmp = new FCmpInst(**labelEntry, FCmpInst::FCMP_OLE, 
							   val1, val2, icmp_name);
		}
		else{
			assert(false &&" missing mapping in here");
		}
	}
	else if( WN_operator(wn) == OPR_GE ){
			
		/*U4F8GE
		  F8F8LDID 0 <2,4,n1> T<11,.predef_F8,8>
		  F8F8LDID 0 <2,5,n2> T<11,.predef_F8,8> */
		if(tyid == MTYPE_U8 || tyid == MTYPE_U4){ 
			cmp = new ICmpInst(**labelEntry, ICmpInst::ICMP_UGE, 
							   val1, val2, icmp_name);
		}
		else if(tyid == MTYPE_I4 || tyid == MTYPE_I8){ 
			cmp = new ICmpInst(**labelEntry, ICmpInst::ICMP_SGE, 
							   val1, val2, icmp_name);
		}
		else if(tyid == MTYPE_F4 || tyid == MTYPE_F8){
			cmp = new FCmpInst(**labelEntry, FCmpInst::FCMP_OGE, 
							   val1, val2, icmp_name);
		}
		else{
			assert(false &&" missing mapping ");
		}
	}
	
	else if( WN_operator(wn) == OPR_GT ){
		/*U4F8GT
		 F8F8LDID 0 <2,7,determinant> T<11,.predef_F8,8>
		 F8CONST <1,57,____0.000000000000000>*/
		
		if(tyid == MTYPE_U8 || tyid == MTYPE_U4){ 
			cmp = new ICmpInst(**labelEntry, ICmpInst::ICMP_UGT, 
							   val1, val2, icmp_name);
		}
		else if(tyid == MTYPE_I4 || tyid == MTYPE_I8){ 
			cmp = new ICmpInst(**labelEntry, ICmpInst::ICMP_SGT, 
							   val1, val2, icmp_name);
		}
		else if(tyid == MTYPE_F4 || tyid == MTYPE_F8){
			cmp = new FCmpInst(**labelEntry, FCmpInst::FCMP_OGT, 
							   val1, val2, icmp_name);
		}
		else{
			assert(false &&" missing mapping in here");
		}
	}
	else if(WN_operator(wn) == OPR_EQ){
	   		   
	   /*U4I4EQ
	     I4I1LDID 0 <2,4,c> T<2,.predef_I1,1>
	     I4INTCONST 105 (0x69) */
	   if(tyid == MTYPE_F4 || tyid == MTYPE_F8){
		   cmp = new FCmpInst(**labelEntry, FCmpInst::FCMP_OEQ, 
							  val1,val2, icmp_name);
		   //cmp = new ICmpInst(*labelEntry, ICmpInst::ICMP_EQ, val1,val2, icmp_name);
	       //CastInst* int32_lor_ext = new ZExtInst(cmp,
		//		IntegerType::get(w2lMod->getContext(), 32), "lor.ext", labelEntry);
		   
	      //  return int32_lor_ext;
	   } 
	   else if(tyid == MTYPE_I1 ||tyid == MTYPE_I2||tyid == MTYPE_I4 ||tyid == MTYPE_I8
			  ||tyid == MTYPE_U2||tyid == MTYPE_U4 ||tyid == MTYPE_U8){
		   cmp = new ICmpInst(**labelEntry, ICmpInst::ICMP_EQ, val1,val2, icmp_name);
	   
	   }   
	   else{
		   assert(false && "missing mapping");
	   }
	}
	else if(WN_operator(wn) == OPR_NE){
		
		/*I4F8NE
		  F8F8LDID 0 <2,14,_temp___save_sqrt2> T<11,.predef_F8,8>
		  F8F8LDID 0 <2,14,_temp___save_sqrt2> T<11,.predef_F8,8> */
	    if(tyid == MTYPE_F4 || tyid == MTYPE_F8){
			cmp = new FCmpInst(**labelEntry, FCmpInst::FCMP_ONE, 
							   val1,val2, icmp_name);
			//cmp = new ICmpInst(*labelEntry, ICmpInst::ICMP_EQ, val1,val2, icmp_name);
			//CastInst* int32_lor_ext = new ZExtInst(cmp,
			//		IntegerType::get(w2lMod->getContext(), 32), "lor.ext", labelEntry);
			
			//  return int32_lor_ext;
		} 
		else if(tyid == MTYPE_I1 ||tyid == MTYPE_I2||tyid == MTYPE_I4 ||tyid == MTYPE_I8
				||tyid == MTYPE_U2||tyid == MTYPE_U4 ||tyid == MTYPE_U8){
			cmp = new ICmpInst(**labelEntry, ICmpInst::ICMP_NE, val1,val2, icmp_name);
			
		}   
		else{
			assert(false && "missing mapping");
		}
		
		
	}
	icmp_num++;
	
	assert(cmp!=nullptr && "Error in function createICMP_llvm \n ");
	
	
	return cmp;
}


void getSwitchConst_Lable_llvm(const WN* wn, vector<int>& vectConst,vector<int>& vectLable)
{
	WN* wnkids = WN_kid(wn,1);
	OPCODE opcode = WN_opcode(wnkids);
	if (OPCODE_operator( opcode) != OPR_BLOCK) {
		fprintf(stderr, " error in switch in ir_put_stmt_llvm");
		exit(1);
	}
	
	//WN is BLOCK now;
	
	WN* casewn = WN_first(wnkids);
	while (casewn) {
		opcode = WN_opcode(casewn);
		assert(OPCODE_operator(opcode) == OPR_CASEGOTO && "not a CASEGOTO");
		
		int switchCase = WN_const_val(casewn);
		int lable = WN_label_number(casewn);
		vectConst.push_back(switchCase);
		vectLable.push_back(lable);
		casewn = WN_next(casewn);
	}
}

Function *create_fun_from_wn(WN* wn)
{
		//1) get st
	    Function * funPtr = nullptr;	
	
		ST_IDX st_idx = WN_st_idx(wn);
		assert(st_idx != (ST_IDX) 0 && "st_idx error in createCall_llvm");
		const ST* st = &St_Table[st_idx];
		
		//2) get pu type
		
		TY_IDX ty_idx = 0;
		ty_idx = PU_prototype (Pu_Table[(st->u2).pu]);
		const TY & ty = Ty_Table[ty_idx];
		
	
		
		//3) create llvm function object
		
		//fprintf (f, "FUNCTION (f: 0x%04x)\n", Pu_flags());
		
		SmallVector<Type*,32> FuncTyArgs; 
		 {
			TYLIST_IDX list_idx = ty.Tylist ();
			//fprintf (f, "\treturns ");
			//Print_TY_IDX_verbose (f, Tylist_Table[idx]);
			
			TY_IDX idx = Tylist_Table[list_idx];
			
			const TY& ty = Ty_Table[idx];
			const char *name = TY_name_idx (ty) == 0 ? "(anon)" : TY_name (ty);
			fprintf (stdout, "%s (#%d) align %d", name, TY_IDX_index (idx), TY_align (idx));
		 
			
			if (Tylist_Table[list_idx] == 0) {
				
				SmallVector<Type*,32> FuncTyArgs; 
			
				Function *func = createFunc_llvm(wn,FuncTyArgs);
				return func;
				
			}
			
			++list_idx;
			while (Tylist_Table[list_idx] != 0) {
				
				TY_IDX idx = Tylist_Table[list_idx];
							 
				const TY& ty = Ty_Table[idx];
				const char *name = TY_name_idx (ty) == 0 ? "(anon)" : TY_name (ty);
				fprintf (stdout, "%s (#%d) align %d", name, TY_IDX_index (idx), TY_align (idx));				
				++list_idx;
				
				if(strcmp(name,".predef_I4") == 0 )
				{
					FuncTyArgs.push_back(IntegerType::get(w2lMod->getContext(),/*w2lTemp.typeLen*/ 4));
				}
				else if(strcmp(name,".predef_I4") == 0 )
				{
					
				}
				
			}			
		}
		
		Function *func = createFunc_llvm(wn,FuncTyArgs);
		return funPtr;
}

Value* createCall_llvm(WN* wn, vector<Value*> call_parm, BasicBlock *labelEntry)
{
	//int num = call_parm.size();
	static int call_num = 1;
	
	Function *func = nullptr;
	string funName = "" ;
	if(WN_operator(wn) == OPR_CALL){
		
		W2LSYM w2lsym = Get_Name_Level_Num_llvm (WN_st_idx(wn));
		
		if(strcmp(w2lsym.symName, "__pathscale_malloc_alg") == 0)
			return nullptr; 
		
		assert(w2lFunMap.find(w2lsym.symName)!= w2lFunMap.end() 
			   && "error in createCall_llvm function");
		func = w2lFunMap[w2lsym.symName];
	}
	else if(WN_operator(wn) == OPR_INTRINSIC_CALL){
		funName = INTRINSIC_name((INTRINSIC) WN_intrinsic(wn));
		transform(funName.begin(), funName.end(), funName.begin(), ::tolower);
		func = getIntrinsicFun_llvm(funName);
	}
	else{
		assert(false && "missing type here");
	}
	
	assert(func != nullptr && "func is nullptr here");
	
	/*
	if(w2lFunMap.find(w2lsym.symName) == w2lFunMap.end()){
		//function has not been created, so I need to creat this function and add it to the
		// basicly, all the function should be inserted into w2lFunMap in ST::Print_llvm funciton,
		//through function createExternFun_llvm.  
		Function * func =  create_fun_from_wn( wn);
		w2lFunMap.insert( pair<string, Function*>(w2lsym.symName, func) );
		
		BasicBlock *labelEntry   = NULL;  
		labelEntry = BasicBlock::Create(w2lMod->getContext(), "entry",func, 0);
		w2lEntryMap.insert(pair<string, BasicBlock*>(w2lsym.symName,labelEntry) );	
	}
	*/
	
	
	
	char Call_name[256];
	sprintf(Call_name,"%s%d", "call", call_num);
	CallInst* call = nullptr;
	
	if(call_parm.size()>0){
		if(func->getReturnType()->isVoidTy()){
			//assert(false && "here");
			call = CallInst::Create(func , call_parm, "", labelEntry);
		}else{
			call = CallInst::Create(func , call_parm, Call_name, labelEntry);
		}
		call->setCallingConv(CallingConv::C);
		//w2lJustCall.first= string(w2lsym.symName);
		if(funName == "memcpy"){
			w2lJustCall.second = call_parm[0];
		}
		else{
			w2lJustCall.second = call;
		}
	}
	else{
		call = CallInst::Create(func , "", labelEntry);
		call->setCallingConv(CallingConv::C);
		//w2lJustCall.first= string(w2lsym.symName);
		w2lJustCall.second = call;
	}
	
	call_num++;
	return call;							
}

Value* getElementValue_llvm(int field_order, Value* value_input, const FLD* pfld, const TY& ty, BasicBlock *labelEntry)
{

	//1) get offset in getElementPtr;
	std::vector<Value*> ptr_arrayidx1_indices;
	
	ConstantInt* const_int32_0 = ConstantInt::get(w2lMod->getContext(),
								 APInt(32, StringRef("0"), 10));
	ptr_arrayidx1_indices.push_back(const_int32_0);	

	char num[256];
	sprintf(num, "%ld" , field_order);
	ConstantInt* const_index_2 = ConstantInt::get(w2lMod->getContext(),
								 APInt(32, num, 10));
	ptr_arrayidx1_indices.push_back(const_index_2);
	
	
	//2) get fld name and type.
	const TY& ty_fld = Ty_Table[pfld->type];
	Type* type_llvm = createLLVMType_llvm(ty);
	string fld_name = &Str_Table[pfld->name_idx];
	
	
	//3) create GetElementPtrInst and return;
	Instruction* ptr_offset = GetElementPtrInst::Create(
								  type_llvm, 
								  value_input, ptr_arrayidx1_indices, fld_name.c_str(), labelEntry);
	return ptr_offset;

}

int buildOffType_llvm(OFFType* off_type1, OFVALUE* of_value1, Value* value_input, const TY& ty, 
					  int &offset, int &field_order, BasicBlock *labelEntry){
	
	field_order+=1;
	
	int inside_offset = offset;
	
	FLD_IDX  fld_idx = ty.Fld ();
	
	int inside_field = 0;
	if (fld_idx != 0) {
		FLD_ITER iter = Make_fld_iter (FLD_HANDLE (fld_idx));
		do{
			FLD fld_temp = (*iter);
			int fld_offset = fld_temp.ofst+inside_offset;
			
			const TY& fld_ty = Ty_Table[fld_temp.type];
			
			string a1 = to_string(fld_offset);
			string a2 = to_string(field_order);
			a1 = a1+"+"+a2;
			
			Value* value_get = getElementValue_llvm(inside_field, value_input, &(*iter), ty, labelEntry);
			
			off_type1->insert(pair<string, FLD*>(a1, &(*iter)));
			of_value1->insert(pair<string, Value*>(a1, value_get));
			
			if(TY_kind(fld_ty) ==TY_KIND::KIND_STRUCT){	
				buildOffType_llvm(off_type1, of_value1, value_get, fld_ty, fld_offset, field_order, labelEntry);
			}
			inside_field++;
			field_order+=1;
		} while (! FLD_last_field (iter++));
	} 
	field_order -=1;
	
	return off_type1->size();
}

FLD* getFieldType_llvm(const TY& ty, int &offset, int &field_order)
{
	
	string ty_name = TY_name(ty);
	OFFType* off_type1;
	assert(struct_field_map.find(ty_name) != struct_field_map.end());
	off_type1 = struct_field_map[ty_name];
	
	
	string a1 = to_string(offset);
	string a2 = to_string(field_order);
	a1 = a1+"+"+a2;
	
	assert(off_type1->find(a1) != off_type1->end() &&" faile here");
	return off_type1->at(a1);
	
}


Value* getField_llvm(const TY& ty, int offset, int field_order, Value* val_result, BasicBlock *labelEntry)
{
	
	string ty_name = TY_name(ty);
	
	OFFType* off_type1;
	OFVALUE* of_value1;
		
	if(struct_field_map.find(ty_name) != struct_field_map.end()
	  && struct_value_map.find(ty_name) != struct_value_map.end() ){
		
		off_type1 = struct_field_map[ty_name];
		of_value1 = struct_value_map[ty_name];
		
	}
	else{
		off_type1 = new OFFType();
		of_value1 = new OFVALUE();
		int offset_inside = 0;
		int field_order_inside = 0;
		
		buildOffType_llvm(off_type1, of_value1, val_result, ty, offset_inside, field_order_inside, labelEntry);
		struct_field_map.insert(pair<string, OFFType*>(ty_name, off_type1));
		//struct_value_map.insert(pair<string, OFVALUE*>(ty_name, of_value1));
	}
	
	
	
	string a1 = to_string(offset);
	string a2 = to_string(field_order);
	a1 = a1+"+"+a2;
	//off_type1->insert(pair<string, FLD*>(a1, &(*iter)));
	assert(off_type1->find(a1) != off_type1->end() &&" faile here");
	assert(of_value1->find(a1) != of_value1->end() &&" faile here");
	
	return of_value1->at(a1);
	//return off_type1->at(a1);	
	//typedef map<string, FLD*> OFFType;
	//map<string, OFFTYPE*> struct_field_map;
}

void createIStore_V16F4_llvm(WN* wn,BasicBlock *labelEntry) {

	WN* wnkid = WN_kid(wn,0);
	Value * val1 = ir_put_expr_llvm(wnkid, 1,&labelEntry);

	WN* wnkid1 = WN_kid(wn,1);
	Value * val2 = ir_put_expr_llvm(wnkid1, 1,&labelEntry);

	VectorType* VectorTy = VectorType::get(Type::getFloatTy(w2lMod->getContext()), 4);
	PointerType* PointerTy = PointerType::get(VectorTy, 0);

	PointerType* IntPtrTy = PointerType::get(IntegerType::get(w2lMod->getContext(), 32), 0);

	IntToPtrInst* ptr = new IntToPtrInst(val2, 
										 IntPtrTy,  "", labelEntry);			
	CastInst* castPtr = new BitCastInst(ptr, PointerTy, "", labelEntry);


	StoreInst* store = new StoreInst(val1, castPtr, false, labelEntry);
	//store->setAlignment(4);

}



void createIStore_V16I4_llvm(WN* wn,BasicBlock *labelEntry) {

	WN* wnkid = WN_kid(wn,0);
	Value * val1 = ir_put_expr_llvm(wnkid, 1,&labelEntry);

	WN* wnkid1 = WN_kid(wn,1);
	Value * val2 = ir_put_expr_llvm(wnkid1, 1,&labelEntry);

	VectorType* VectorTy = VectorType::get(IntegerType::get(w2lMod->getContext(), 32), 4);
	PointerType* PointerTy = PointerType::get(VectorTy, 0);

	PointerType* IntPtrTy = PointerType::get(IntegerType::get(w2lMod->getContext(), 32), 0);

	IntToPtrInst* ptr = new IntToPtrInst(val2, 
										 IntPtrTy,  "", labelEntry);			
	CastInst* castPtr = new BitCastInst(ptr, PointerTy, "", labelEntry);


	StoreInst* store = new StoreInst(val1, castPtr, false, labelEntry);
	//store->setAlignment(4);

}


void createIStore_llvm(WN* wn,BasicBlock *labelEntry)
{
	/*I4ISTORE 0 T<57,anon_ptr.,8> {line: 1/19}
	   I4I4LDID 0 <2,7,b> T<4,.predef_I4,4>
	   U8U8LDID 0 <2,12,pi> T<57,anon_ptr.,8>
	   
	    %1 = load i32, i32* %b, align 4
       %2 = load i32*, i32** %pi, align 8
       store i32 %1, i32* %2, align 4   
   */
	
	OPCODE opc = WN_opcode(wn);  	
	TYPE_ID  dtype = OPCODE_desc(opc);
	
	assert(OPCODE_has_1ty(opc) && WN_ty(wn) != (TY_IDX) 0);

	TY_IDX tyidx = WN_ty(wn);
	TY& ty = Ty_Table[tyidx];
    assert(ty.kind == TY_KIND::KIND_POINTER);
	
	/*
		INT pcount = 0;
		const TY *pty = &ty;
		while (TY_kind (*pty) == KIND_POINTER) {
			pty = &Ty_Table[TY_pointed (*pty)];
			++pcount;
		}
		
		PointerType* PointerTy = nullptr;	
		Type* pointedType = nullptr; 
		
		pointedType = createLLVMType_llvm(*pty);

		CastInst* ptr_40 = new BitCastInst(
			ptrA, PointerTy_7, "", *labelEntry);*/

	WN* wnkid1 = WN_kid(wn,0);
	assert(OPCODE_is_expression( WN_opcode(wnkid1)) && 
		   "error in Ir_put_stmt_llvm -> OPR_ISTORE");
	//2) change type;
	TYPE_ID  rtype_child = OPCODE_rtype(WN_opcode(wnkid1));	
	Value *val1 = ir_put_expr_llvm(wnkid1,1,&labelEntry);
	val1 = convert_llvm(dtype, rtype_child, val1, labelEntry );
	
	
	WN* wnkid2 = WN_kid(wn,1);
	Value *val2 = ir_put_expr_llvm(wnkid2,1,&labelEntry);
	
	int offset = 0;
	offset= WN_offset(wn);
	
	int byte_size = MTYPE_byte_size(dtype);
	
	char num[256];
	assert(offset % byte_size == 0 && "offset value is wrong");
	offset= offset/byte_size;
	sprintf(num, "%ld" , offset);
	ConstantInt* const_int64 = ConstantInt::get(w2lMod->getContext(),
							   APInt(64, num, 10));
	Type* pointee = createScalarLLVMTypeFromTypeID_llvm(dtype);;
	
	if(pointee != cast<PointerType>(val2->getType())->getElementType()){	
			
		PointerType* IntPtrTy = PointerType::get(pointee, 0);
		
		CastInst* castPtr = new BitCastInst(val2, IntPtrTy, "", labelEntry);
		
		Instruction* ptr_offset = GetElementPtrInst::Create(
										 pointee, 
										 castPtr, const_int64, "castPtr", labelEntry);
	   
		StoreInst* store = new StoreInst(val1, ptr_offset, false, labelEntry);
		//store->setAlignment(pointee->getAlignment());
		return ;
	}
	
	
	StoreInst* store = new StoreInst(val1, val2, false, labelEntry);
	return ;
	//store->setAlignment(4);

}

AllocaInst * createPointerAlloc_llvm(W2LSTRUCT & w2lStr,BasicBlock *labelEntry){
	
	
	
	/*const TY& rty =  *(w2lStr.pTY);
	string symName = w2lStr.symName;
	//ptrA = createAllocByTypeID_llvm(ty_ID, symName, labelEntry);
	ptrA = createAllocByType_llvm(rty, symName, labelEntry);*/
	
	
	AllocaInst *ptrA = nullptr; 
	PointerType* PT; 
	//if(w2lStr.w2ltype==w2lI){
	
	if(TY_kind(*(w2lStr.pPointerTY)) == KIND_SCALAR){
		TYPE_ID ty_ID = TY_mtype(*(w2lStr.pPointerTY));
		if(ty_ID==MTYPE_I4){	
			
			int pointerTypeLen = MTYPE_bit_size(ty_ID);		
			 PT = PointerType::get(IntegerType::get(w2lMod->getContext(),
											   pointerTypeLen), 0);
			
		}else if(ty_ID==MTYPE_F8){ //deal with float * type;
			
			 PT = PointerType::get(Type::getFloatTy(w2lMod->getContext()), 0);
			
		} else if (ty_ID==MTYPE_F16){//deal with double* type
			
			 PT = PointerType::get(Type::getDoubleTy(w2lMod->getContext()), 0);
		}
		else{
			
			assert(false&&"missing a type here");
		}
		ptrA = new AllocaInst(PT, w2lStr.symName, labelEntry);
	}
	
	if(TY_kind(*(w2lStr.pPointerTY)) == KIND_STRUCT){
		const TY & ty = *(w2lStr.pPointerTY);
		string structName = TY_name(ty);
        assert(w2lstructTyMap.find(structName) != w2lstructTyMap.end() && "no struct defined");
		StructType *StructTy_struct_as  = w2lstructTyMap[structName];
		 PT = PointerType::get(StructTy_struct_as, 0);
		ptrA = new AllocaInst(PT, w2lStr.symName, labelEntry);
	}

	//ArrayType* ArrayTy_0 = ArrayType::get(IntegerType::get(mod->getContext(), 32), 4);
	//PointerType* PointerTy_1 = PointerType::get(ArrayTy_0, 0);
		
	assert(ptrA!=nullptr &&"error in createPointerAlloc_llvm\n");
	
	ptrA->setAlignment(8);
	
	return ptrA;
}
	
AllocaInst * createStructAlloc_llvm(W2LSTRUCT & w2lStr,BasicBlock *labelEntry){
		AllocaInst *ptrA = nullptr; 
		string typeName = w2lStr.typeName;
		assert(w2lstructTyMap.find(typeName) != w2lstructTyMap.end() &&"no struct defined\n");
		
		/*if(w2lstructTyMap.find(typeName) == w2lstructTyMap.end()){
			fprintf(stderr,"error in createAlloc_llvm->w2lM\n");
			exit(1);
		}*/
		
		StructType *sty = w2lstructTyMap[typeName];
		ptrA = new AllocaInst(sty, w2lStr.symName , labelEntry);
		ptrA->setAlignment(w2lStr.align);
		
		assert(ptrA != nullptr &&"createStructAlloc_llvm ");
		return ptrA;
	
}

AllocaInst * createArrayAlloc_llvm(W2LSTRUCT & w2lStr,BasicBlock *labelEntry){
		AllocaInst *ptrA; 
		string typeName = w2lStr.typeName;
		
		assert(w2larrayTyMap.find(typeName) != w2larrayTyMap.end() &&" no array defined\n");
		
		
		ArrayType *aty = w2larrayTyMap[typeName];
		
		ptrA = new AllocaInst(aty, w2lStr.symName,
							  labelEntry);
		
		ptrA->setAlignment(w2lStr.align*4);
		assert(ptrA != nullptr &&" error in createArrayAlloc_llvm");
		return ptrA;
}



int getAlignmentFromTy_llvm(const TY& ty)
{
	int alignment= 4;
	if(ty.kind == TY_KIND::KIND_SCALAR){ 
		TYPE_ID ty_ID = TY_mtype(ty);
		alignment = MTYPE_alignment(ty_ID);
		//TY_IDX ty_idx = TY_Id 
		//alignment = TY_align(ty_idx);
	}
	else if(ty.kind == TY_KIND::KIND_POINTER){		
		TY_IDX ty_idx = TY_pointed(ty);
		alignment =TY_align(ty_idx);
	}
	else if(ty.kind == TY_KIND::KIND_ARRAY  ){
		TY_IDX ty_idx = TY_etype(ty);
		alignment =TY_align(ty_idx);
	}
	//else if(ty.kind == TY_KIND::KIND_STRUCT  ){
	//	resultType =  createStructLLVMType_llvm(ty);
	//}
	//else if(ty.kind == TY_KIND::KIND_VOID){
	//	resultType = Type::getVoidTy(w2lMod->getContext());
	//}
	//else{
	//	assert(false &&" missing type here");
	//}
	return alignment;
		
}
AllocaInst* createAllocByType_llvm(  const TY& ty, string & symName, BasicBlock *labelEntry){
	
	AllocaInst* alloc_yan = nullptr;	
	
	int alignment = getAlignmentFromTy_llvm(ty);
	
	Type* type = createLLVMType_llvm(ty);
	
	alloc_yan = new AllocaInst(type, symName.c_str(), labelEntry);
	assert(alloc_yan != nullptr && "fail here");
	
	alloc_yan->setAlignment(alignment);
	return alloc_yan;
}


AllocaInst* createAllocByTypeID_llvm( TYPE_ID dtype , string & symName, BasicBlock *labelEntry){
	
	AllocaInst* ptr_yan = nullptr;	
	
#if 0
	if(ty_ID==MTYPE_I1 || ty_ID==MTYPE_I2 || ty_ID==MTYPE_I4 || ty_ID==MTYPE_I8   )
	{
		// this is for function parameter, so you need to add .addr
		
		ptrA = new AllocaInst(IntegerType::get(w2lMod->getContext(), w2lStr.typeLen), w2lStr.symName, labelEntry);
		ptrA->setAlignment(w2lStr.align);
	}
	else if(ty_ID==MTYPE_F8){
		
		ptrA = new AllocaInst(Type::getFloatTy(w2lMod->getContext()), w2lStr.symName, labelEntry);
		ptrA->setAlignment(w2lStr.align);
		
	}
	// deal with other data type.
	else if(w2lStr.w2ltype==MTYPE_F16){
		
		ptrA = new AllocaInst(Type::getDoubleTy(w2lMod->getContext()), w2lStr.symName, labelEntry);
		ptrA->setAlignment(w2lStr.align);
		
	}
	//else if(w2lStr.w2ltype==w2lA){
	//	
	//}
	
	assert(ptrA != nullptr &&"error in createBuildInAlloc_llvm");
	return ptrA; 
	
#endif 	
	
	
	
	if(dtype == MTYPE_V16I4){
		VectorType* VectorTy_yan_4 = VectorType::get(IntegerType::get(w2lMod->getContext(), 32), 4);
		ptr_yan = new AllocaInst(VectorTy_yan_4, symName.c_str(), labelEntry);
		ptr_yan->setAlignment(8);		
	}
	else if(dtype==MTYPE_I1 || dtype==MTYPE_I2 || dtype==MTYPE_I4 || dtype==MTYPE_I8   )
	{
		int bitsize = MTYPE_bit_size(dtype);
		int alignment = MTYPE_alignment(dtype);
		ptr_yan = new AllocaInst(IntegerType::get(w2lMod->getContext(),bitsize ), symName.c_str(), labelEntry);
		ptr_yan->setAlignment(alignment);
		
	}
	else if(dtype==MTYPE_F4){
		int alignment = MTYPE_alignment(dtype);
		ptr_yan = new AllocaInst(Type::getFloatTy(w2lMod->getContext()), symName.c_str(),
								 labelEntry);
		ptr_yan->setAlignment(alignment);
		
	}
	else if(dtype==MTYPE_F8){
		int alignment = MTYPE_alignment(dtype);
		ptr_yan = new AllocaInst(Type::getDoubleTy(w2lMod->getContext()), symName.c_str(),
								 labelEntry);
		ptr_yan->setAlignment(alignment);
		
	}
	// deal with other data type.
	else if(dtype==MTYPE_F16 || dtype==MTYPE_F10){
		int alignment = MTYPE_alignment(dtype);
		ptr_yan = new AllocaInst(Type::getDoubleTy(w2lMod->getContext()), symName.c_str(), labelEntry);
		ptr_yan->setAlignment(alignment);
		
	}
	else if(dtype == MTYPE_U8){
		int alignment = MTYPE_alignment(dtype);
		ptr_yan = new AllocaInst(IntegerType::get(w2lMod->getContext(), 64), symName.c_str(), labelEntry);
		ptr_yan->setAlignment(alignment);
		
	}else if(dtype == MTYPE_U4){
		int alignment = MTYPE_alignment(dtype);
		ptr_yan = new AllocaInst(IntegerType::get(w2lMod->getContext(), 32), symName.c_str(), labelEntry);
		ptr_yan->setAlignment(alignment);
		
	}
	else{
		
		assert(ptr_yan!=nullptr &&" missing a type here");
	}
	
	return ptr_yan;
}



AllocaInst * createBuildInAlloc_llvm(W2LSTRUCT & w2lStr,BasicBlock *labelEntry){
	
	AllocaInst *ptrA=nullptr; 
	
	//TYPE_ID ty_ID = TY_mtype(*(w2lStr.pTY));
	const TY& rty =  *(w2lStr.pTY);
	string symName = w2lStr.symName;
	//ptrA = createAllocByTypeID_llvm(ty_ID, symName, labelEntry);
	ptrA = createAllocByType_llvm(rty, symName, labelEntry);
	assert(ptrA != nullptr &&" null ptrA ");
	return ptrA;
}


void createAlloc_llvm(BasicBlock *labelEntry)
{
	string funcName = labelEntry->getParent()->getName();
	
	map<string, W2LSTRUCT>::iterator it;
	map<string, W2LSTRUCT>::iterator end = symMap.end();
	
	for(it = symMap.begin(); it!=end ;it++)
	{
		W2LSTRUCT w2lStrTemp = it->second;
		AllocaInst *ptrA = nullptr;
		
		char oldName[256];
		//below is function arguments
		if(w2lStrTemp.w2lStore == SCLASS_FORMAL || 
				w2lStrTemp.w2lStore == SCLASS_FORMAL_REF)
		{
			char nameTem[256];
			strcpy(oldName, w2lStrTemp.symName);
			strcpy(nameTem, w2lStrTemp.symName);
			strcat(nameTem, ".addr");	
			strcpy(w2lStrTemp.symName, nameTem);
		}
		
		const TY& ty = *(w2lStrTemp.pTY);
		string strSynName = w2lStrTemp.symName;
		ptrA = createAllocByType_llvm(ty, strSynName ,labelEntry);
		assert(ptrA != nullptr &&"ptrA should not be nullptr");
		
		//if(w2lStrTemp.pointer){ // deal with pointer
	/*	if(TY_kind (*(w2lStrTemp.pTY)) == KIND_POINTER){
			ptrA = createPointerAlloc_llvm(w2lStrTemp,labelEntry);
		}
		//deal with structure
		//else if(w2lStrTemp.w2ltype==w2lM) { 
		else if (TY_kind (*(w2lStrTemp.pTY)) == KIND_STRUCT){
			ptrA = createStructAlloc_llvm(w2lStrTemp,labelEntry);
		}
		//deal with array
		//else if(w2lStrTemp.w2ltype==w2lAR){
		else if (TY_kind (*(w2lStrTemp.pTY)) == KIND_ARRAY){
			
			ptrA = createArrayAlloc_llvm(w2lStrTemp,labelEntry);
			
		}else if(TY_kind (*(w2lStrTemp.pTY)) == KIND_SCALAR){
			
			ptrA = createBuildInAlloc_llvm(w2lStrTemp,labelEntry);	
			
		}else{
			
           assert(false &&"should not be here any way");
		}*/
		
		
		
		// return to old symTableName
		if(w2lStrTemp.w2lStore == SCLASS_FORMAL ||
				w2lStrTemp.w2lStore == SCLASS_FORMAL_REF)
		{
		   strcpy(w2lStrTemp.symName, oldName);
		}
		
		assert(ptrA != nullptr && "error createAlloc_llvm function");
		string strSymName(w2lStrTemp.symName);
		w2lAllocMap.insert(std::pair<string, AllocaInst *>(strSymName, ptrA));	
	}	 
}


void  createArgsStore_llvm(WN* wn,Function *funcSum, BasicBlock *labelEntry)
{
	unsigned kidNum = unsigned( WN_kid_count(wn)-3 );
	Function::arg_iterator args = funcSum->arg_begin();
	
	
	for(int i = 0;i<kidNum;i++)
	{
		Value * v = args;
		string name = v->getName();
		if(w2lAllocMap.find(v->getName()) != w2lAllocMap.end())
		{
			AllocaInst *ptrA = w2lAllocMap[v->getName()];
			StoreInst *st0 = new StoreInst(args, ptrA, false, labelEntry);
			st0->setAlignment(ptrA->getAlignment());
		}
	    args++;
	}
	
}



void createReturn_llvm(WN* wn,  BasicBlock *labelEntry)
{
	//if(w2lSetReturn.find(labelEntry) != w2lSetReturn.end())
	
	
	if(previousIsReturn(labelEntry))
		return;
	
	w2lSetReturn.insert(labelEntry);
	
	WN* wnkid = WN_kid(wn,0);
	assert(OPCODE_is_expression( WN_opcode(wnkid)) &&" error in OPR_RETURN_VAL\n");
	
	Value *val = ir_put_expr_llvm(wnkid,1,&labelEntry);
	
	OPCODE opc = WN_opcode(wn);  	
	TYPE_ID  dtype = OPCODE_desc(opc);
	TYPE_ID  child_type = OPCODE_rtype(WN_opcode(wnkid));
	
	
	TYPE_ID vTY_ID = child_type; 
	if(getValueType_llvm(child_type, vTY_ID , val) ){
		child_type = vTY_ID;
	}
	if(dtype != child_type){
		val = convert_llvm( dtype,child_type, val, labelEntry);
	}
	
	ReturnInst::Create(w2lMod->getContext(), val, labelEntry);
	return ;	
}



bool pregMapArgu_llvm(WN_OFFSET offset, const char* name, BasicBlock *labelEntry)
{
	//1) get function
	string strName = name;
	Function *fun = labelEntry->getParent();
	Function::arg_iterator args = fun->arg_begin();
	Function::arg_iterator argend = fun->arg_end();
	for(;args!=argend;args++){
	   string funName = args->getName();
	   if(funName==strName){
		   //this preg is fun args.
		   char temp[128];
		   sprintf(temp,"_%d",offset);
		   strName= strName+temp;
		   w2lpregMap.insert(pair<string, Value*>(strName, args));
		   return true;
	   }
	}
	return false;	
}


int getArrayDim_llvm(const TY& ty){
	int dim = 1;
	TY_IDX ty_idx = TY_etype(ty);

	while(true){
		if(TY_kind(ty_idx) != TY_KIND::KIND_ARRAY)
			break;
		ty_idx =  TY_etype(ty_idx);
		dim++;
	}
	return dim;
}

Value* createTAS_llvm(const WN* wn, BasicBlock** labelEntry){
	
	OPCODE opc = WN_opcode(wn);
	assert(OPCODE_has_1ty(opc) && WN_ty(wn) != (TY_IDX) 0);
	TY_IDX tyidx = WN_ty(wn);
	TY& ty = Ty_Table[tyidx];
	
	int indent = 1;
	WN* wnkid0 = WN_kid(wn,0);
	Value * val_kid = ir_put_expr_llvm(wnkid0, indent+1,labelEntry);
	
	PointerType* pointer_ty = (PointerType*)createLLVMType_llvm(ty);
	
	CastInst* cast_inst = new BitCastInst( val_kid, pointer_ty, "", *labelEntry);
	return cast_inst;
}

Value* createARRAY_llvm(const WN* wn,  BasicBlock** labelEntry){

	int indent = 1;
	WN* wnkid0 = WN_kid(wn,0);
	WN* wnkid1 = WN_kid(wn,1);
	WN* wnkid2 = WN_kid(wn,2);
	
	if(WN_operator(wnkid0) == OPR_ADD){
		std::vector<Value*> ptr_arrayidx1_indices;
		
		ConstantInt* const_int32_0 = ConstantInt::get(w2lMod->getContext(),
									 APInt(32, StringRef("0"), 10));

		/*char num[256];
		sprintf(num, "%ld" , WN_const_val(wnkid2));
		ConstantInt* const_int64 = ConstantInt::get(w2lMod->getContext(),
								   APInt(64, num, 10));
		ptr_arrayidx1_indices.push_back(const_int64);*/
		
		Value * valtemp2 = ir_put_expr_llvm(wnkid2, indent+1,labelEntry);
		//ptr_arrayidx1_indices.push_back(valtemp2);
		

		PointerType* IntPtrTy = PointerType::get(
									IntegerType::get(w2lMod->getContext(), 32), 0);
		
		Value * valtemp0 = ir_put_expr_llvm(wnkid0, indent+1,labelEntry);
		WN* wnkid1 = WN_kid(wn,1);
		
		IntToPtrInst* ptr = new IntToPtrInst(valtemp0, 
											 IntPtrTy,  "", *labelEntry);
		
		Type* pointee = IntegerType::get(w2lMod->getContext(), 32);
		
		Instruction* ptr_arrayidx1 = GetElementPtrInst::Create(
										 pointee, 
										 ptr, valtemp2, "arrayidx", *labelEntry);
		return ptr_arrayidx1;
	}
	
	/*
	 Value * valtemp0 = ir_put_expr_llvm(wnkid0, indent+1,labelEntry);
	 WN* wnkid1 = WN_kid(wn,1);
	 
	 Value * valtemp1 = ir_put_expr_llvm(wnkid1, indent+1,labelEntry);
	 
	 WN* wnkid2 = WN_kid(wn,2);
	 Value * valtemp2 = ir_put_expr_llvm(wnkid2, indent+1,labelEntry);*/
	
	 
	const ST_IDX& st_idx_kid = WN_st_idx(wnkid0);
	assert( st_idx_kid != (ST_IDX) 0 &&"fail here");
	const ST* st_kid = &St_Table[st_idx_kid];
	const TY& ty_kid = Ty_Table[ST_type (st_idx_kid)];

	
	/*U8ARRAY 2 4
	    U8LDA 0 <2,6,a> T<66,anon_ptr.,8>
	    I4INTCONST 100 (0x64)
	    I4INTCONST 100 (0x64)
	    I8I4LDID 0 <2,9,i> T<4,.predef_I4,4>
	    I8I4LDID 0 <2,10,j> T<4,.predef_I4,4>*/
	
   if(ty_kid.kind ==TY_KIND::KIND_ARRAY){
	   
	   //1) judge array dimension
	   //int dim = getArrayDim_llvm(ty_kid);
	   
	   assert(OPCODE_has_ndim(WN_opcode(wn) ) );	  
	   int dim = WN_num_dim(wn);
	   int dim_index = dim;
	   int kid_num = dim+1;
	   Instruction* ptr_arrayidx1 = nullptr;
	   
	   
	   while(dim_index>0){
   
		   std::vector<Value*> ptr_arrayidx1_indices;
		   
		   ConstantInt* const_int32_0 = ConstantInt::get(w2lMod->getContext(),
										APInt(32, StringRef("0"), 10));
		   ptr_arrayidx1_indices.push_back(const_int32_0);
	   
		   WN* wnkid = WN_kid(wn,kid_num);
		   Value * valtemp2 = ir_put_expr_llvm(wnkid, indent+1,labelEntry);
		   
		   ptr_arrayidx1_indices.push_back(valtemp2);			   
		   
		   TY* element_ty; 
		   
		   if(dim_index == dim){
			   
			   Type* llvm_type = createLLVMType_llvm(ty_kid);
			   char* name_kid_st = ST_name(st_kid);
			   string st_name_whole = buildSymName_llvm(st_kid);

			   if(ST_level(st_kid) == 2 ){
			   
				   assert(w2lAllocMap.find(st_name_whole)!= w2lAllocMap.end() 
						  && "w2lAlloc is empty here");
				   AllocaInst *alloc = w2lAllocMap[st_name_whole];
				   ptr_arrayidx1 = GetElementPtrInst::Create(llvm_type, 
									alloc, ptr_arrayidx1_indices, "arrayidx", *labelEntry);
				   element_ty  = &(Ty_Table[TY_etype(ty_kid)]);
			   }
		      else if(ST_level(st_kid) == 1){
				  assert(w2lGlobalValueMap.find(st_name_whole) != w2lGlobalValueMap.end() && 
						 "error in createStore_llvm->w2lAllocMap\n");	
				  Value *ptrA = w2lGlobalValueMap[st_name_whole];
				  
				  ptr_arrayidx1 = GetElementPtrInst::Create(llvm_type, 
								  ptrA, ptr_arrayidx1_indices, "arrayidx", *labelEntry);
				  element_ty  = &(Ty_Table[TY_etype(ty_kid)]);
			  }
		   }
		   else{
			   
			   Type* llvm_type = createLLVMType_llvm(*element_ty);
			   ptr_arrayidx1 = GetElementPtrInst::Create(llvm_type, 
							   ptr_arrayidx1, ptr_arrayidx1_indices, "arrayidx", *labelEntry);
		       element_ty  = &(Ty_Table[TY_etype(*element_ty)]);
		   }
		   
		   ptr_arrayidx1_indices.clear();
		   dim_index--;
		   kid_num++;
	   }
	   assert(ptr_arrayidx1 != false &&" ptr is nullptr here");
	   return ptr_arrayidx1;
   }
   
   /*U8ARRAY 1 4
     U8LDA 24 <2,12,str1> T<75,anon_ptr.,8> <field_id:4>
     I4INTCONST 3 (0x3)
     I4INTCONST 1 (0x1)*/
   
   else if(ty_kid.kind ==TY_KIND::KIND_STRUCT){
	   
	    Value * valtemp0 = ir_put_expr_llvm(wnkid0, indent+1,labelEntry);
	   		   
	    int field_id = WN_field_id(wnkid0);
	    int offset = WN_offset(wnkid0);
		
		std::vector<Value*> ptr_arrayidx1_indices;
		ConstantInt* const_int32_0 = ConstantInt::get(w2lMod->getContext(),
									 APInt(32, StringRef("0"), 10));
		ptr_arrayidx1_indices.push_back(const_int32_0);

		char num[256];
		sprintf(num, "%ld" , WN_const_val(wnkid2));
		ConstantInt* const_int64 = ConstantInt::get(w2lMod->getContext(),
								   APInt(64, num, 10));
		ptr_arrayidx1_indices.push_back(const_int64);
		
		FLD* pfld = getFieldType_llvm(ty_kid, offset, field_id);
		
		const TY& ty_array = Ty_Table[pfld->type]; 

		Type* llvm_type = createLLVMType_llvm(ty_array);
		
	    Instruction* ptr_arrayidx1 = GetElementPtrInst::Create(llvm_type, 
									 valtemp0, ptr_arrayidx1_indices, "arrayidx", *labelEntry);
		return ptr_arrayidx1;
	   
		
		/*fld verstion method, DON'T DELETE
		----------------------------------------------------------------
		std::vector<Value*> ptr_arrayidx1_indices;
	   
	   ConstantInt* const_int32_0 = ConstantInt::get(w2lMod->getContext(),
									APInt(32, StringRef("0"), 10));
	   ptr_arrayidx1_indices.push_back(const_int32_0);

	   char num[256];
	   sprintf(num, "%ld" , WN_const_val(wnkid2));
	   ConstantInt* const_int64 = ConstantInt::get(w2lMod->getContext(),
								  APInt(64, num, 10));
	   ptr_arrayidx1_indices.push_back(const_int64);
	   
	   
	//   FLD* pfld = getField_llvm(ty_kid, offset, field_id);
	   
	   FLD* pfld = nullptr; 
	   const TY& ty_array = Ty_Table[pfld->type]; 

	   Type* llvm_type = createLLVMType_llvm(ty_array);
	   
		char* nameTemp = ST_name(st_kid);

	   Instruction* ptr_arrayidx1 = GetElementPtrInst::Create(llvm_type, 
									valtemp0, ptr_arrayidx1_indices, "arrayidx", *labelEntry);
	   return ptr_arrayidx1;*/
   }
   
   else{
	   assert(false && "missing a type in array");
   }
}

Value* createILOAD(const WN* wn,  BasicBlock **labelEntry){
	

	int indent = 1;	
	OPCODE op = WN_opcode(wn);
	TYPE_ID rtype= OPCODE_rtype (op);
	TYPE_ID  dtype = OPCODE_desc(op);
	
	WN* wnkid0 = WN_kid(wn,0);
	Value * valtemp0 = ir_put_expr_llvm(wnkid0, indent+1, labelEntry);
	
	
	// get type from wn
	assert(OPCODE_has_2ty(op) && WN_ty(wn) != (TY_IDX) 0
		   && WN_load_addr_ty(wn) != (TY_IDX) 0);
	TY_IDX tyidx = WN_ty(wn);
	TY& ty = Ty_Table[tyidx];
	
	TY_IDX ty_load_idx = WN_load_addr_ty(wn);
	TY& ty_load = Ty_Table[ty_load_idx];
	

	LoadInst* ld  = nullptr;

	if(rtype == MTYPE_V16I4){
		
		WN* wnkid0 = WN_kid(wn,0);
		Value * valtemp0 = ir_put_expr_llvm(wnkid0, indent+1,labelEntry);
		
		
		//I really need to modify these two statement, they will cause memory leaking in the future. About these two kinds of type, I should keep it in one place in the future. 
		VectorType* VectorTy = VectorType::get(IntegerType::get(w2lMod->getContext(),
											   32), 4);
		PointerType* PointerTy = PointerType::get(VectorTy, 0);
		
		
		PointerType* IntPtrTy = PointerType::get(IntegerType::get(w2lMod->getContext(), 32), 0);
		
		IntToPtrInst* ptr = new IntToPtrInst(valtemp0, 
											 IntPtrTy,  "", *labelEntry);
		
		CastInst* castPtr = new BitCastInst(ptr, PointerTy, "", *labelEntry);
		//GetElementPtrInst* ptr_24 = GetElementPtrInst::Create(ptr_aList, int64_index, "", labelEntry);
		
		ld = new LoadInst(castPtr, "wide.load", false, *labelEntry);
		ld->setAlignment(4); 
	}
	else if( WN_operator(wnkid0) == OPR_ARRAY ){
		
		Type* llvm_child_type = valtemp0->getType();	
		Type* llvm_type1 =   createLLVMType_llvm(ty);	
		Type* llvm_type2 = createLLVMType_llvm(ty_load);
		
		if(llvm_child_type == llvm_type1 ){
			ld = new LoadInst(valtemp0, "", false, *labelEntry);
			return ld;
		}
		else if(llvm_child_type == llvm_type2){
		
			int offset = WN_offset(wn);
			
			int offsetNum = offset/8;
			char offsetNumStr[256];
			sprintf(offsetNumStr,"%d",offsetNum);			
			ConstantInt* const_int64_23 = ConstantInt::get(
											  w2lMod->getContext(), APInt(64, StringRef(offsetNumStr), 10));
			
			assert(OPCODE_has_2ty(op) && WN_ty(wn) != (TY_IDX) 0);
			TY_IDX tyidx = WN_ty(wn);
			TY& ty = Ty_Table[tyidx];
			Type* llvm_type = createLLVMType_llvm(ty);
			GetElementPtrInst* ptr_arrayidx = GetElementPtrInst::Create(llvm_type,
											  valtemp0, const_int64_23, "arrayidx", *labelEntry);
			
			ld = new LoadInst(ptr_arrayidx, "", false, *labelEntry);
			ld->setAlignment(8);
			return ld;
		}
		else{
			assert(false && "missing type map here");
		}
		
	}
	else if(ty.kind == TY_KIND::KIND_POINTER ){
		
		int offset = WN_offset(wn);
		int offsetNum = offset/8;
		char offsetNumStr[256];
		sprintf(offsetNumStr,"%d",offsetNum);			
		ConstantInt* const_int64_23 = ConstantInt::get(
										  w2lMod->getContext(), APInt(64, StringRef(offsetNumStr), 10));
		
		
		Type* llvm_type = createLLVMType_llvm(ty);
		GetElementPtrInst* ptr_arrayidx = GetElementPtrInst::Create(llvm_type,
										  valtemp0, const_int64_23, "arrayidx", *labelEntry);
		
		ld = new LoadInst(ptr_arrayidx, "", false, *labelEntry);
		ld->setAlignment(8);
		return ld;
		
		// ld = new LoadInst(valtemp0, "", false, *labelEntry);
		// ld->setAlignment(4);
	}
	else if(ty.kind == TY_KIND::KIND_STRUCT){
		
		/*I4I1ILOAD 0 T<66,str,8> T<68,anon_ptr.,8> <field_id:1>
		  U8U8LDID 0 <2,9,strp> T<68,anon_ptr.,8>*/
		int field_id = WN_field_id(wn);
		
		//std::vector<Value*> ptr_mI_indices;
		//ConstantInt* const_int32_0 = ConstantInt::get(
		//				w2lMod->getContext(), APInt(32, StringRef("0"), 10));
		//ptr_mI_indices.push_back(const_int32_0);
		
		//FLD* pfld =  getField_llvm(ty, field_id);
		
		int offset = 0;
		offset= WN_offset(wn);
		int byte_size = MTYPE_byte_size(dtype);
		
		char num[256];
		assert(offset % byte_size == 0 && "offset value is wrong");
		offset= offset/byte_size;
		sprintf(num, "%ld" , offset);
		ConstantInt* const_int64 = ConstantInt::get(w2lMod->getContext(),
								   APInt(64, num, 10));
		Type* pointee = createScalarLLVMTypeFromTypeID_llvm(dtype);
		
		PointerType* IntPtrTy = PointerType::get(pointee, 0);
		
		CastInst* castPtr = new BitCastInst(valtemp0, IntPtrTy, "", *labelEntry);
		
		Instruction* ptr_offset = GetElementPtrInst::Create(
									  pointee, 
									  castPtr, const_int64, "castPtr", *labelEntry);
		
		/*char numBuffer[20];
		sprintf(numBuffer,"%d", field_id-1) ;
		
		ConstantInt* const_int32_1 = ConstantInt::get(
								w2lMod->getContext(), APInt(32, numBuffer, 10));
		ptr_mI_indices.push_back(const_int32_1);
		
		
		char field_name[256];
		strcpy(field_name, &Str_Table[pfld->name_idx]); 
		
		Type* llvm_type = createLLVMType_llvm(ty);
		
		Instruction* ptr_mI = GetElementPtrInst::Create(
					llvm_type, valtemp0, ptr_mI_indices, field_name, *labelEntry); */
		
		LoadInst* ld = new LoadInst(ptr_offset, "", false, *labelEntry);
		
		Value* ld_convert = convert_llvm(rtype, dtype, ld, *labelEntry);
		
		//ld->setAlignment( TY_align(pfld->type) );
		
		return ld;
	}else if(ty.kind == TY_KIND::KIND_SCALAR){
		LoadInst* ld = new LoadInst(valtemp0, "", false, *labelEntry);
		ld->setAlignment(8);
	}	
	else{
		assert(false && "missing a type here");
	}

	assert(ld != nullptr &&" ld is nullptr in OPR_ILOAD");
	return ld; 
}

void  createPregStore_llvm(WN* wn, Value* val, BasicBlock *& labelEntry)
{
	OPCODE opcode;
	opcode = WN_opcode(wn);
	
	assert(OPCODE_has_1ty(opcode) &&" fail");
	assert(WN_ty(wn) != (TY_IDX) 0 && " fail IDX = 0");
	
	TY_IDX tyidx = WN_ty(wn);
	TY& store_ty = Ty_Table[tyidx];
	
	ST_IDX st_idx = WN_st_idx(wn);
	assert(st_idx != (ST_IDX) 0 && "st_idx error ");
	
	const ST* wn_st = &St_Table[st_idx];
		
	
#if 0 	
	if (Preg_Is_Dedicated(WN_offset(wn)))  // Offset<=48
	{
		if (Preg_Offset_Is_Int(WN_offset(wn))) {  //>=16 Offset >=1
	    		fprintf(ir_ofile, " # $r%d", WN_offset(wn));
			}
			else if (Preg_Offset_Is_Float(WN_offset(wn))) { //>=32 Offset >=17
	    		fprintf(ir_ofile, " # $f%d", 
						WN_offset(wn) - Float_Preg_Min_Offset);
			}
#ifdef TARG_X8664
			else if (Preg_Offset_Is_X87(WN_offset(wn))) { //>=40 Offset >=33
	    		fprintf(ir_ofile, " # $st%d", 
						WN_offset(wn) - X87_Preg_Min_Offset);
			}
#endif
	}
#endif 

	
	    	/* reference to a non-dedicated preg */
		   //yan Last_Dedicated_Preg_Offset = 48
	int wnOffset = WN_offset(wn) - Last_Dedicated_Preg_Offset;
	
	if(WN_offset(wn) ==17){
		
		
		WN* wnkid = WN_kid(wn,0);
		assert(OPCODE_is_expression( WN_opcode(wnkid)) &&" error in OPR_RETURN_VAL\n");
		
		Value *val = ir_put_expr_llvm(wnkid,1,&labelEntry);
		
		ReturnInst::Create(w2lMod->getContext(), val, labelEntry);
		
		return; 
		
	}
	
	assert((wnOffset>0 && wnOffset < PREG_Table_Size (CURRENT_SYMTAB)) &&  " # <Invalid PREG Table index (%d)>");
		
	// For below optimized whirl IR, LDID is not need any more, you can use # x directly, 
	// so for this condtion, I don't need to get value form store child node.
	//I4STID 49 <1,4,.preg_I4> T<4,.predef_I4,4> # x {line: 1/5}
	//  I4I4LDID 5 <1,4,.preg_I4> T<4,.predef_I4,4> # $r5
	// That is why pregMapArgu_llvm is here!
	bool isArgu = pregMapArgu_llvm(WN_offset(wn), Preg_Name(WN_offset(wn)), labelEntry);
	
	if(!isArgu){
		
		//1) get key Name;
		string strName = Preg_Name(WN_offset(wn));
		char temp[128];
		sprintf(temp,"_%d",WN_offset(wn));
		strName= strName+temp;
		
		
		//2) if you have not create a alloc, please create it. 	
		AllocaInst *allocPtr = nullptr;
		if(ST_level(wn_st) == 2){		
			if(w2lAllocMap.find(strName) == w2lAllocMap.end()){
				allocPtr = createAllocByType_llvm(store_ty, strName, labelEntry);
				w2lAllocMap.insert(pair<string, AllocaInst*>(strName, allocPtr));	
			}
			else{
				allocPtr = w2lAllocMap[strName];
			}
		}
	
	    if(ST_level(wn_st) == 1){	
			string str = buildSymName_llvm(wn_st);
			string str_offset = to_string(WN_offset(wn));
			str = str+"_"+str_offset;
			if(w2lPregAllocMap.find(str) == w2lPregAllocMap.end()){
				
				//allocPtr = createAllocByType_llvm(store_ty, str, labelEntry);
				
				BasicBlock* head = labelEntry;
				BasicBlock* head1 = nullptr;
				head1 = head->getSinglePredecessor();
				if(head1 == nullptr){
					head1 = head;
				}
				else{
					while( head1 != nullptr){
					  head = head1->getSinglePredecessor();
					  if(head == nullptr){
						  break;
					  }
					  else{
						  head1 = head;
					  }
					}
				}
				
				//allocPtr = createAllocByType_llvm(store_ty, str, g_entry_labelEntry);
				//allocPtr = createAllocByType_llvm(store_ty, str, head1);
				//allocPtr = createAllocByType_llvm(store_ty, str, labelEntry);
				
				AllocaInst* alloc_yan = nullptr;	
				
				int alignment = getAlignmentFromTy_llvm(store_ty);
				
				Type* type = createLLVMType_llvm(store_ty);
				
				allocPtr = new AllocaInst(type, str.c_str());
				assert(allocPtr != nullptr && "fail here");
				BasicBlock::iterator it = g_entry_labelEntry->begin();
				(g_entry_labelEntry->getInstList()).insert(it,allocPtr);
				
				
				//allocPtr->setAlignment(alignment);
				
				
				
				w2lPregAllocMap.insert(pair<string, AllocaInst*>(str, allocPtr));	
			}
			else{
				allocPtr = (AllocaInst *)(w2lPregAllocMap[str]);
			}
		}
		
		assert(allocPtr!=nullptr && "error in createPregStore_llvm function\n");
		
	
		
#if 0
		//4) create a PHI node if
		string strNamePHI = strName+"_PHI";
		
		if(strNamePHI=="i_50_PHI"){
		
			if(w2lPHIMap.find(strNamePHI) == w2lPHIMap.end()){
						
				PHINode* phi = PHINode::Create(IntegerType::get(w2lMod->getContext(), 32), 2, strNamePHI.c_str());
				WN* wnkid = WN_kid(wn,0);
				val = ir_put_expr_llvm(wnkid, 1,&labelEntry);
				phi->addIncoming(val, labelEntry);
				
				
				//phi->setIncomingBlock(1,labelEntry);			
				
				w2lPHIMap.insert(pair<string,PHINode*>(strNamePHI,phi));
				
			}
			else{
				PHINode* phi = w2lPHIMap[strNamePHI];
				
				WN* wnkid = WN_kid(wn,0);
				val = ir_put_expr_llvm(wnkid, 1,&labelEntry);
				
				phi->addIncoming(val, labelEntry);
				Instruction* pIns = labelEntry->getFirstNonPHI();
				//phi->moveBefore(pIns);
				phi->insertBefore(pIns);
			}
			
			w2lpregMap.insert(pair<string, Value*>(strName, val));
			return ;
		}
		
#endif 		
	
		//3) 
		//you need to call child wn to get value
		Value* val = nullptr;
		
		if(strcmp(strName.c_str(),"__comma_49") ==0 && WN_offset(wn) == 49){ 
			//I4STID 49 <1,4,.preg_I4> T<4,.predef_I4,4> # __comma {line: 1/22}
		    //  I4I4LDID 1 <1,4,.preg_I4> T<4,.predef_I4,4> # $r1
			val = g_FunReturn;
		}
		else{
			WN* wnkid = WN_kid(wn,0);
			val = ir_put_expr_llvm(wnkid, 1,&labelEntry);
		}
		
		assert(val != nullptr &&"val is nullptr in the createPregStore_llvm function");
		
		if(strName =="<preg>_51"){
		   cout<<"hello world"<<endl;
		}
		
		WN* wnkid = WN_kid(wn,0);
		if(strName =="decimalNumber_55"){
			cout<<"hello world"<<endl;
		}
		
		TYPE_ID  dtype = OPCODE_desc(WN_opcode(wn));
		TYPE_ID  child_type = OPCODE_rtype(WN_opcode(wnkid));
		
		TYPE_ID vTY_ID = child_type; 
		if(getValueType_llvm(child_type, vTY_ID , val) ){
		     child_type = vTY_ID;
		}
		
		if(dtype != child_type){
			val = convert_llvm( dtype,child_type, val, labelEntry);
		}
		
		
		if(OPCODE_operator(WN_opcode(wnkid)) == OPR_DIVREM){
		
			string strName = "lowPart";
			string typeID = to_string(child_type);
			strName = strName+"_"+typeID;
			assert(w2lAllocMap.find(strName) != w2lAllocMap.end());
			AllocaInst *allocPtr = w2lAllocMap[strName];
			strName = strName+"_"+to_string(WN_offset(wn));
			w2lAllocMap.insert(pair<string, AllocaInst*>(strName, allocPtr));
			
		}
		
		if(OPCODE_operator(WN_opcode(wnkid)) == OPR_LDA){
			IntegerType* intType = IntegerType::get(w2lMod->getContext(), 64);
			
			PtrToIntInst* ptr = new PtrToIntInst(val, 
												 intType,  "", labelEntry);
			val =  ptr;
		}
		

		
		if(val->getType()->isIntegerTy() && O3 && strName =="_temp_call1_72"){
			
			Type* type = allocPtr->getType();
			Type* type1 = type->getPointerElementType();
			IntToPtrInst* ptr = new IntToPtrInst(val, 
												 type1,  "", labelEntry);			
			val = ptr;
		}
		
		
		StoreInst* void_26 = new StoreInst(val, allocPtr, false, labelEntry);
		void_26->setAlignment(8);
		
		if(void_26 == (StoreInst*)0xcabf30){
//			assert(false);
		}
		
		w2lpregMap.insert(pair<string, Value*>(strName, val));
		
	}
	return;
}	



/// \brief crate Store statement in llvm

/// For example, for below whirl IR 
/// I4STID 0 <2,4,a>
///   ISINTCONST 199

/// 1) get symbol name a from Get_Name_Level_Num_llvm and store in w2lsym
/// 2) find this symbol name in SymMap  
/// From wn get 
/// from wn's kid get 

void createMemcpyCall()
{ /*
	PointerType* PointerTy_7 = PointerType::get(
								   IntegerType::get(w2lMod->getContext(), 8), 0);
	
	CastInst* ptr_40 = new BitCastInst(
		ptrA, PointerTy_7, "", *labelEntry);
	

	Constant* const_ptr_29 = ConstantExpr::getCast(
								 Instruction::BitCast, (Constant*)(valChild), PointerTy_7);

	ConstantInt* const_int64_30 = ConstantInt::get(w2lMod->getContext(), 
								  APInt(64, StringRef("8"), 10));
	ConstantInt* const_int32_31 = ConstantInt::get(w2lMod->getContext(), 
								  APInt(32, StringRef("8"), 10));
	ConstantInt* const_int1_32 = ConstantInt::get(w2lMod->getContext(), 
								 APInt(1, StringRef("0"), 10));

	std::vector<Value*> void_41_params;
	void_41_params.push_back(ptr_40);
	void_41_params.push_back(const_ptr_29);
	void_41_params.push_back(const_int64_30);
	void_41_params.push_back(const_int32_31);
	void_41_params.push_back(const_int1_32);
	

	CallInst* void_41 = CallInst::Create(createMemcpy(), 
										 void_41_params, "", *labelEntry);
	void_41->setCallingConv(CallingConv::C);
	void_41->setTailCall(false);
	AttributeSet void_41_PAL;
	void_41->setAttributes(void_41_PAL);
	*/
}





void  createStore_old_llvm(WN* wn, BasicBlock **labelEntry)
{
	
	
	ST_IDX st_idx = WN_st_idx(wn);
	assert(st_idx != (ST_IDX) 0 && "st_idx error ");
	
	const ST* wn_st = &St_Table[st_idx];
	// get type from symbol
	const TY& st_ty = Ty_Table[ST_type (st_idx)];
	// get st_ty name
	char* char_st_name = ST_name (wn_st);
	
	char name_whole[256];
	strcpy(name_whole, ST_name(wn_st) );
	
	int st_order_whole = ST_IDX_index (st_idx);
	string st_order_str = to_string(st_order_whole);
	strcat(name_whole, st_order_str.c_str() );
	
	//string str(char_st_name);
	
	string str(name_whole);
	
	
	if(ST_level(wn_st) == 2 )
		assert(symMap.find(str) != symMap.end() &&
			   "error in createStore_llvm when search in symMap");	
	else if(ST_level(wn_st) == 1 )
		assert(globalSymMap.find(str) != globalSymMap.end() &&
			   "error in createStore_llvm when search in globalSymMap");
	
	//2) get Alloc
	assert(w2lAllocMap.find(str) != w2lAllocMap.end() && 
		   "error in createStore_llvm->w2lAllocMap\n");	
	AllocaInst *ptrA = w2lAllocMap[str];
	
	//3) get type from opc
	WN* wnkid = WN_kid(wn,0);
	OPCODE opc = WN_opcode(wn);  	
	TYPE_ID  dtype = OPCODE_desc(opc);
	TYPE_ID  child_type = OPCODE_rtype(WN_opcode(wnkid));
	
	//4) get type from wn
	assert(OPCODE_has_1ty(opc) && WN_ty(wn) != (TY_IDX) 0);
	TY_IDX tyidx = WN_ty(wn);
	TY& ty = Ty_Table[tyidx];
	
	//5) get chile value
	Value* valChild = nullptr;	
	valChild = ir_put_expr_llvm(wnkid, 1,labelEntry);
	assert(valChild != nullptr &&"val is nullptr in the createStore_llvm function");
	
	
	if(st_ty.kind == TY_KIND::KIND_SCALAR ){
		
		Value* val_convert = convert_llvm(dtype, child_type, valChild, *labelEntry);	
		StoreInst *st0 = new StoreInst(val_convert, ptrA, false, *labelEntry);
		st0->setAlignment(ptrA->getAlignment());
		return;
		
	}
	//char* p = "Hello";
	else if(st_ty.kind == TY_KIND::KIND_POINTER){
		assert(dtype == MTYPE_U8 && child_type == MTYPE_U8 && "pointer type is wrong"); 
		StoreInst *st0 = new StoreInst(valChild, ptrA, false, *labelEntry);
		st0->setAlignment(ptrA->getAlignment());
		return;
	}
	//U8STID 0 <2,4,a> T<55,anon_ptr.,8> {line: 1/22}
	//  U8LDA 0 <1,51,(5_bytes)_"hell\000"> T<59,anon_ptr.,8>
	// char* a[2] = {"hello", "world"};	
	//This is special case for STID statement 
	else if(st_ty.kind == TY_KIND::KIND_ARRAY && WN_operator(wnkid) == OPR_LDA){
		
		ConstantInt* const_int32_20 = ConstantInt::get(
										  w2lMod->getContext(), APInt(32, StringRef("0"), 10));		
		int offset = WN_offset(wn);
		int offsetNum = offset/8;
		char offsetNumStr[256];
		sprintf(offsetNumStr,"%d",offsetNum);			
		ConstantInt* const_int64_28 = ConstantInt::get(
										  w2lMod->getContext(), APInt(64, StringRef(offsetNumStr), 10));
			
		std::vector<Constant*> const_ptr_29_indices;
		std::vector<Value*> indices;
		indices.push_back(const_int32_20);
		indices.push_back(const_int64_28);
		
		Type* llvm_st_type =  createLLVMType_llvm(st_ty);
		Instruction* ptr_arrayidx = 
			GetElementPtrInst::Create(llvm_st_type,ptrA, indices, "arrayidx", *labelEntry);
			
		StoreInst *st0 = new StoreInst(valChild, ptr_arrayidx, false, *labelEntry);
		st0->setAlignment(ptrA->getAlignment()); 
		return;	
	}
	else if(st_ty.kind == TY_KIND::KIND_STRUCT ){
		
		int field_id = WN_field_id(wn);
			
		int offset = 0;
		offset= WN_offset(wn);
		
		Value* field_address = getField_llvm(st_ty, offset, field_id, ptrA, *labelEntry);	
		
		Value* val_convert = convert_llvm(dtype, child_type, valChild, *labelEntry);
		
		StoreInst* pst = new StoreInst(val_convert, field_address, false, *labelEntry);
		pst->setAlignment( MTYPE_alignment(dtype) );
		
		
		/*fld method, just keep it. DON'T delete
		const TY& ty_fld = Ty_Table[pfld->type];
		int byte_size = MTYPE_byte_size(ty_fld.mtype);
				
		char num[256];
		assert(offset % byte_size == 0 && "offset value is wrong");
		offset= offset/byte_size;
		sprintf(num, "%ld" , offset)int byte_size = MTYPE_byte_size(dtype);		
		
		
		ConstantInt* const_int64 = ConstantInt::get(w2lMod->getContext(),
								   APInt(64, num, 10));
		
		
		Type* fld_type_llvm = createLLVMType_llvm(ty_fld);
		
		Instruction* ptr_offset = GetElementPtrInst::Create(
									  fld_type_llvm, 
									  ptrA, const_int64, "castPtr", *labelEntry);
		
		if(isa<PointerType>(valChild->getType())){
			assert(fld_type_llvm == 
				   cast<PointerType>(valChild->getType())->getElementType());
			
			StoreInst* pst = new StoreInst(valChild, ptr_offset, false, *labelEntry);
		    pst->setAlignment( MTYPE_alignment(dtype) );
		}
		else{
			Value* val_convert = convert_llvm(dtype, child_type, valChild, *labelEntry);
		    StoreInst* pst = new StoreInst(val_convert, ptr_offset, false, *labelEntry);
		    pst->setAlignment( MTYPE_alignment(dtype) );
		}*/
		
		
		/* hard conversion method, just keep it. DON'T delete
		int byte_size = MTYPE_byte_size(dtype);		
		char num[256];
		assert(offset % byte_size == 0 && "offset value is wrong");
		offset= offset/byte_size;
		sprintf(num, "%ld" , offset)int byte_size = MTYPE_byte_size(dtype);		
		char num[256];
		assert(offset % byte_size == 0 && "offset value is wrong");
		offset= offset/byte_size;
		sprintf(num, "%ld" , offset);;
		
		ConstantInt* const_int64 = ConstantInt::get(w2lMod->getContext(),
								   APInt(64, num, 10));
		Type* pointee = createScalarLLVMTypeFromTypeID_llvm(dtype);
		
		PointerType* IntPtrTy = PointerType::get(pointee, 0);
		
		CastInst* castPtr = new BitCastInst(ptrA, IntPtrTy, "", *labelEntry);
		
		Instruction* ptr_offset = GetElementPtrInst::Create(
									  pointee, 
									  castPtr, const_int64, "castPtr", *labelEntry);
		
		if(isa<PointerType>(valChild->getType()) && 
	         pointee != cast<PointerType>(valChild->getType())->getElementType()){
	
			CastInst* castPtr1 = new BitCastInst(valChild, IntPtrTy, "", *labelEntry);
			StoreInst* pst = new StoreInst(castPtr1, ptr_offset, false, *labelEntry);
		    pst->setAlignment( MTYPE_alignment(dtype) );
		}
		else{
			Value* val_convert = convert_llvm(dtype, child_type, valChild, *labelEntry);
		    StoreInst* pst = new StoreInst(val_convert, ptr_offset, false, *labelEntry);
		    pst->setAlignment( MTYPE_alignment(dtype) );
		}*/
		return; 
	}
	else{
		assert(false && "missing type");
	}
		
}


void  createStore_llvm(WN* wn, Value* alloc, BasicBlock **labelEntry)
{
	
	//1) get type from opc
	WN* wnkid = WN_kid(wn,0);
	OPCODE opc = WN_opcode(wn);  	
	TYPE_ID  dtype = OPCODE_desc(opc);
	TYPE_ID  child_type = OPCODE_rtype(WN_opcode(wnkid));
	
	
	//2) get TY
	TY_IDX tyidx;
	TY_IDX ty_load_idx;
	if(OPCODE_has_1ty(opc) && WN_ty(wn) != (TY_IDX) 0){
		tyidx = WN_ty(wn);
	}
	
	if(OPERATOR_has_sym(WN_operator(wn))){
	
		ST_IDX st_idx = WN_st_idx(wn);
		assert(st_idx != (ST_IDX) 0 && "st_idx error ");
		tyidx = ST_type (st_idx);		
	}
	
	TY& ty = Ty_Table[tyidx];
		
	//3) get chile value
	Value* valChild = nullptr;	
	valChild = ir_put_expr_llvm(wnkid, 1,labelEntry);
	
	if(dtype != child_type){
		valChild = convert_llvm( dtype,child_type, valChild, *labelEntry);
	}
	
	assert(valChild != nullptr &&"val is nullptr in the createStore_llvm function");
	
	
	
	if(!alloc->getType()->isPointerTy()){
		/*
		I4ISTORE 0 T<59,anon_ptr.,8> {line: 1/10}
		  I4I4LDID 0 <2,5,i> T<4,.predef_I4,4>
		  U8ADD
			 U8LDA 0 <2,4,data> T<60,anon_ptr.,8>
			 U8MPY
			  U8I8CVT
				I8I4LDID 0 <2,5,i> T<9,.predef_U8,8>
			 U8INTCONST 4 (0x4)*/
		assert(alloc->getType()->isIntegerTy() &&"alloc should be integer type");
		PointerType* IntPtrTy = (PointerType*)createLLVMType_llvm(ty);
		
		IntToPtrInst* ptr = new IntToPtrInst(alloc, 
											 IntPtrTy,  "", *labelEntry);
		alloc = ptr;
	}
	
	//char* p = "Hello";
	//this function deal with STID and ISTORE two statments at the same time. 
	// so in STID, It's a KIND_SCALAR, in ISTORE, it's KIND_POINTER
	if(ty.kind == TY_KIND::KIND_SCALAR || 
			(ty.kind == TY_KIND::KIND_POINTER && TY_kind(ty.Pointed())==TY_KIND::KIND_SCALAR)){
			
		StoreInst *st0 = new StoreInst(valChild, alloc, false, *labelEntry);
		st0->setAlignment(MTYPE_alignment(dtype));
		return;
	}
	
	//U8STID 0 <2,4,a> T<55,anon_ptr.,8> {line: 1/22}
	//  U8LDA 0 <1,51,(5_bytes)_"hell\000"> T<59,anon_ptr.,8>
	// char* a[2] = {"hello", "world"};	
	//This is special case for STID statement 
	else if(ty.kind == TY_KIND::KIND_ARRAY && WN_operator(wnkid) == OPR_LDA){
		
		ConstantInt* const_int32_20 = ConstantInt::get(
										  w2lMod->getContext(), APInt(32, StringRef("0"), 10));		
		int offset = WN_offset(wn);
		int offsetNum = offset/8;
		char offsetNumStr[256];
		sprintf(offsetNumStr,"%d",offsetNum);			
		ConstantInt* const_int64_28 = ConstantInt::get(
										  w2lMod->getContext(), APInt(64, StringRef(offsetNumStr), 10));
		
		std::vector<Constant*> const_ptr_29_indices;
		std::vector<Value*> indices;
		indices.push_back(const_int32_20);
		indices.push_back(const_int64_28);
		
		Type* llvm_st_type =  createLLVMType_llvm(ty);
		Instruction* ptr_arrayidx = 
			GetElementPtrInst::Create(llvm_st_type,alloc, indices, "arrayidx", *labelEntry);
		
		StoreInst *st0 = new StoreInst(valChild, ptr_arrayidx, false, *labelEntry);
		st0->setAlignment(MTYPE_alignment(dtype)); 
		return;	
	}
	else if(ty.kind == TY_KIND::KIND_STRUCT ||
			( ty.kind == TY_KIND::KIND_POINTER && TY_kind(ty.Pointed())==TY_KIND::KIND_STRUCT ) ){
		
		int field_id = 0;
			field_id = WN_field_id(wn);
		
		int offset = 0;
		offset= WN_offset(wn);
		
		Value* field_address = nullptr;
		if(field_id >0 && ty.kind == TY_KIND::KIND_STRUCT  ){
			field_address = getField_llvm(ty, offset, field_id, alloc, *labelEntry);
		}
		else if( field_id >0 && ty.kind == TY_KIND::KIND_POINTER 
				 && TY_kind(ty.Pointed())==TY_KIND::KIND_STRUCT  ){
			TY& ty_pointed = Ty_Table[ty.Pointed()];
			field_address = getField_llvm(ty_pointed, offset, field_id, alloc, *labelEntry);
		}
		else if(field_id == 0){
			field_address = alloc;
		}
		else{
		    assert(false && "store structure error");
		}
		
		
		StoreInst* pst = new StoreInst(valChild, field_address, false, *labelEntry);
		pst->setAlignment( MTYPE_alignment(dtype) );
		return; 
	}
	//MSTID 0 <2,10,a> T<60,.anonymous.2,4> {line: 1/29}
	//   MMLDID 0 <2,11,a.init> T<60,.anonymous.2,4>
	else if(ty.kind == TY_KIND::KIND_ARRAY && dtype == MTYPE_t::MTYPE_M){
		
		/*ST_IDX st_idx = WN_st_idx(wn);
		assert(st_idx != (ST_IDX) 0 && "st_idx error ");
		const ST* wn_st = &St_Table[st_idx];
		string str = buildSymName_llvm(wn_st);*/
		
		PointerType* PointerTy_6 = PointerType::get(
									   IntegerType::get(w2lMod->getContext(), 8), 0);
		
		Constant* const_ptr_17 = ConstantExpr::getCast(
							Instruction::BitCast, (Constant*)valChild, PointerTy_6); 
		
		//Constant*  const_ptr_17 = new BitCastInst(valChild, PointerTy_6, "", *labelEntry);
		
		int align_array = ((AllocaInst*)alloc)->getAlignment();
		
		ArrayType* at=  (ArrayType*)createLLVMType_llvm(ty);
		int num_element = at->getNumElements();
		Type* ty2 = at->getElementType();
		int len_element = ty2->getPrimitiveSizeInBits();
		len_element/=8;
		
		
		CastInst* ptr_24 = new BitCastInst(alloc, PointerTy_6, "", *labelEntry);
		
		//ConstantInt* const_int64_18 = ConstantInt::get(w2lMod->getContext(), 
		//							  APInt(64, StringRef("12"), 10));
		
		ConstantInt* const_int64_18 = ConstantInt::get(IntegerType::get(w2lMod->getContext(), 64), 
									  						num_element*len_element  );
									  
		
		//ConstantInt* const_int32_19 = ConstantInt::get(w2lMod->getContext(), APInt(32, StringRef("4"), 10));
		
		ConstantInt* const_int32_19 = ConstantInt::get(IntegerType::get(w2lMod->getContext(), 32)
									  , align_array);
		
		ConstantInt* const_int1_20 = ConstantInt::get(w2lMod->getContext(), APInt(1, StringRef("0"), 10));
		
		
		
		std::vector<Value*> void_25_params;
		void_25_params.push_back(ptr_24);
		void_25_params.push_back(const_ptr_17);
		void_25_params.push_back(const_int64_18);
		void_25_params.push_back(const_int32_19);
		void_25_params.push_back(const_int1_20);
		
		CallInst* void_25 = CallInst::Create(getIntrinsicMemcpy_llvm(),
											 void_25_params, "", *labelEntry);
		void_25->setCallingConv(CallingConv::C);
		void_25->setTailCall(false);
		AttributeSet void_25_PAL;
		void_25->setAttributes(void_25_PAL);
		
		
		//assert(w2lInitMap.find(str) != w2lInitMap.end() && " fail in w2lInitMap");
		//return w2lInitMap[str];
		
	}
	
	//I4STID 16 <2,4,data> T<4,.predef_I4,16> {line: 1/10}
	   //U4INTCONST 4 (0x4)
	else if(ty.kind == TY_KIND::KIND_ARRAY && WN_operator(wnkid) == OPR_INTCONST){
		

		int offset = 0;
		offset= WN_offset(wn);

		uint64_t pointer_offset = offset/4;
		ConstantInt* const_int64_42 = ConstantInt::get(
										  IntegerType::get(w2lMod->getContext(), 64), 
										  pointer_offset);

		Type* intType = IntegerType::get(w2lMod->getContext(), 32);

		PointerType* PointerTy = PointerType::get(intType, 0);
		

		CastInst* ptr_bcp1 = new BitCastInst(alloc, PointerTy, "bcp1", 
											 *labelEntry);
		GetElementPtrInst* ptr_arrayidx = 
			GetElementPtrInst::Create(intType , ptr_bcp1, const_int64_42, 
									  "pointeridx", *labelEntry);
		
		StoreInst *st0 = new StoreInst(valChild, ptr_arrayidx, false, *labelEntry);
		st0->setAlignment(MTYPE_alignment(dtype));
		return;
		
	}
	else if(dtype == MTYPE_V16I4){
		VectorType* VectorTy = VectorType::get(
								   IntegerType::get(w2lMod->getContext(), 32), 4);
		
		PointerType* PointerTy_8 = PointerType::get(VectorTy, 0);
		
		//PointerType* type_pointer = (PointerType*)createLLVMType_llvm(ty_load);
		//valtemp0 = new IntToPtrInst(valtemp0, 
		//							type_pointer,  "", *labelEntry);
		
		
		CastInst* ptr_bcp1 = new BitCastInst(alloc, PointerTy_8, "bcp1", 
											 *labelEntry);
		
		StoreInst *st0 = new StoreInst(valChild, ptr_bcp1, false, *labelEntry);
		st0->setAlignment(MTYPE_alignment(MTYPE_I4));
		return;	
	}
	else{
		assert(false && "missing type");
	}
}



Value* createCOMMA_llvm(const WN* wn, BasicBlock** labelEntry){
	 
	/* I4STID 0 <2,7,sum> T<4,.predef_I4,4> {line: 1/22}
	  I4COMMA
	  1) BLOCK {line: 0/0}
		 I4CALL 126 <1,51,_Z5test1Pii> # flags 0x7e {line: 1/22}
		  U8PARM 2 T<54,anon_ptr.,8> #  by_value 
		  U8LDA 0 <2,5,a> T<56,anon_ptr.,8>
	   I4PARM 2 T<4,.predef_I4,4> #  by_value 
		  I4INTCONST 40 (0x28)
	   END_BLOCK
	 2) I4I4LDID -1 <1,49,.preg_return_val> T<4,.predef_I4,4>
	*/

	WN* wnkid1 = WN_kid(wn,0);
	ir_put_stmt_llvm(wnkid1, 1,*labelEntry);

	WN* wnkid2 = WN_kid(wn,1);
	assert(OPCODE_is_expression( WN_opcode(wnkid2)) &&"error in COMMA");
	
	Value * valtemp = ir_put_expr_llvm(wnkid2, 1,labelEntry);
	return valtemp;

}



Value* createINTConst_llvm(const WN* wn){
 
	assert(OPCODE_has_value( WN_opcode(wn)) && " there is error \n");

	char num[256];
	sprintf(num, "%ld" , WN_const_val(wn));
	
	ConstantInt* const_int32 = nullptr;
	
	OPCODE opc = WN_opcode(wn);  	
	TYPE_ID  rtype = OPCODE_rtype(opc);
	
	int bitsize = MTYPE_bit_size(rtype);
	const_int32 = ConstantInt::get(w2lMod->getContext(), APInt(bitsize, num, 10));
	
	assert(const_int32!= nullptr &&" nullptr here");
	
	return const_int32;
	 
}

Value* createConst_llvm(WN* wn)
{
	ST_IDX st_idx = WN_st_idx(wn);
	char* name = nullptr;
	if(st_idx != 0){
		const ST* st = &St_Table[st_idx];
		if (ST_class(st) == CLASS_CONST) {
			name = Targ_Print(NULL, STC_val(st));
			/* new lines and spaces in constant strings 
			 * will mess up the ascii reader,
			 * so replace with underlines */
			char* p;
			for (p = name; *p != '\0'; p++)
				switch (*p) {
					case ' ':
					case '\t':
					case '\n':
						*p = '_';
				}
		}
	}
	
	OPCODE opc = WN_opcode(wn);
	//TYPE_ID rtype = (TYPE_ID) ((((UINT32) opcode) >> 8) & 0x3F); 
	TYPE_ID rtype = OPCODE_rtype(opc);
	if(rtype == MTYPE_V16I4){
		
		// Yan, just deal with this statment: V16I4CONST <1,52,0>
		// 1,52, 0 is constant.
		//-----------------------------------------------------
/*[52]: <constant>        <1,52> Constant of type .predef_I4 (#4, I4)
		value: 0
		Address: 0(<1,52>)  Alignment: 4 bytes
location: file (null), line 0
		Flags:  0x00000008 initialized, XLOCAL
Sclass: FSTATIC*/
		//------------------------------------------------------
		
		std::vector<Constant*> const_packed_elems;
		VectorType* VectorTy = VectorType::get(IntegerType::get(w2lMod->getContext(), 32), 4);
		ConstantInt * const_int32 = nullptr;
		if(name!=nullptr){	
			const_int32 = ConstantInt::get(w2lMod->getContext(), 
										   APInt(32, StringRef(name), 10));
		}
		if(strcmp(name,"0") == 0){
			ConstantInt* const_int32_0 = ConstantInt::
							get(w2lMod->getContext(), APInt(32, StringRef("0"), 10));
ConstantInt* const_int32_1 = ConstantInt::
							get(w2lMod->getContext(), APInt(32, StringRef("1"), 10));
ConstantInt* const_int32_2 = ConstantInt::
							get(w2lMod->getContext(), APInt(32, StringRef("2"), 10));
ConstantInt* const_int32_3 = ConstantInt::
							get(w2lMod->getContext(), APInt(32, StringRef("3"), 10));

			const_packed_elems.push_back(const_int32_0);
			const_packed_elems.push_back(const_int32_1);
			const_packed_elems.push_back(const_int32_2);
			const_packed_elems.push_back(const_int32_3);
		}
		else{
			
			
			const_packed_elems.push_back(const_int32);
			const_packed_elems.push_back(const_int32);
			const_packed_elems.push_back(const_int32);
			const_packed_elems.push_back(const_int32);
		}
		//Constant* const_packed = ConstantVector::get(VectorTy, const_packed_elems);
		Constant* const_packed = ConstantVector::get( const_packed_elems);
		return const_packed;
	}
	else if(rtype == MTYPE_V16F4){
		
		
		std::vector<Constant*> const_packed_elems;
		VectorType* VectorTy = VectorType::get(Type::getFloatTy(w2lMod->getContext()), 4);
		
		ConstantFP* const_float_15 = ConstantFP::get(w2lMod->getContext(), 
									 APFloat(0.000000e+01f));
			const_packed_elems.push_back(const_float_15);
			const_packed_elems.push_back(const_float_15);
			const_packed_elems.push_back(const_float_15);
			const_packed_elems.push_back(const_float_15);
		
		//Constant* const_packed = ConstantVector::get(VectorTy, const_packed_elems);
		Constant* const_packed = ConstantVector::get( const_packed_elems);
		return const_packed;
	
	
	}
			
	W2LSYM w2lSym = Get_Name_Level_Num_llvm(WN_st_idx(wn));
	string key;
	key = w2lSym.symName;
	char temp[128];
	sprintf(temp,"_%d_%d",w2lSym.w2lLevel,w2lSym.w2lIndex);
	key +=temp;

	assert(globalConstMap.find(key)!=globalConstMap.end() &&"Error in ir_put_expr_llvm OPR_CONST");
	return globalConstMap[key];
}




Value* createPregLoad_llvm(WN* wn, BasicBlock **labelEntry)
{

	// this function need to be rewrite later. 
	
	OPCODE opcode;
	opcode = WN_opcode(wn);
	assert(opcode!=0 &&"opcode is 0 in createStore");
	
	ST_IDX st_idx = WN_st_idx(wn);
	assert(st_idx != (ST_IDX) 0 && "st_idx error ");
	
	const ST* wn_st = &St_Table[st_idx];
		
	int wnOffset = WN_offset(wn) - Last_Dedicated_Preg_Offset;

	assert((wnOffset>0 && wnOffset < PREG_Table_Size (CURRENT_SYMTAB)) &&  " # <Invalid PREG Table index (%d)>");	
	
	
	string strName = Preg_Name(WN_offset(wn));
	char temp[128];
	sprintf(temp,"_%d",(WN_offset(wn)));
	strName= strName+temp;
	
	//this is a just test code, you need to change it later. 
	/*
	if(strName=="i_50"){
		string strNamePHI = strName+"_PHI";
		if(w2lPHIMap.find(strNamePHI) != w2lPHIMap.end()){		
			return w2lPHIMap[strNamePHI];
		}
				
	}
	*/
	
	
	//if(w2lAllocMap.find(strName) == w2lAllocMap.end()){
	//	printf("%s \n",strName.c_str());
	//}
	
	
	TYPE_ID  rtype = OPCODE_rtype (opcode);
	
	// deal with aList_61 in
	// U8U8LDID 61 <1,9,.preg_U8> T<53,anon_ptr.,8> # aList
	// How to understand it?
	char* typeName;
	if(OPCODE_has_1ty(opcode)){
		typeName = TY_name(WN_ty(wn));
	}
	
	if( rtype == MTYPE_U8 && strcmp(typeName,"anon_ptr.") == 0){
		IntegerType* intType = IntegerType::get(w2lMod->getContext(), 64);
		
		PtrToIntInst* ptr = new PtrToIntInst(w2lpregMap[strName], 
											 intType,  "", *labelEntry);
		return ptr;
	}
	
	
	if(ST_level(wn_st) == 1){	
		string str = buildSymName_llvm(wn_st);
		string str_offset = to_string(WN_offset(wn));
		str = str+"_"+str_offset;
		assert(w2lPregAllocMap.find(str) != w2lPregAllocMap.end());
		AllocaInst *allocPtr = (AllocaInst *)(w2lPregAllocMap[str]);
		LoadInst *ld = new LoadInst(allocPtr, "", false, *labelEntry);
		ld->setAlignment(allocPtr->getAlignment());
		
		return ld;
		
	}
	
	if(ST_level(wn_st) == 2){	
		assert(w2lAllocMap.find(strName) != w2lAllocMap.end());
		if(w2lAllocMap.find(strName) != w2lAllocMap.end()){
		AllocaInst *allocPtr = w2lAllocMap[strName];
		
		LoadInst *ld = new LoadInst(allocPtr, "", false, *labelEntry);
		ld->setAlignment(allocPtr->getAlignment());
		
		return ld;
		
		}
	}
	
	assert(false);
	
	if(w2lpregMap.find(strName) != w2lpregMap.end()){
	  //assert(w2lpregMap.find(strName) != w2lpregMap.end() &&
		//   "no value in w2lpregMap in createPregLoad_llvm function");
		
		//Yan, For this statment, you need to use cast
		//U8STID 60 <1,9,.preg_U8> T<9,.predef_U8,8> # <preg> {line: 0/0}
		//  U8U8LDID 61 <1,9,.preg_U8> T<53,anon_ptr.,8> # aList		
		TYPE_ID  rtype = OPCODE_rtype (opcode);
		char* temp;
		if(OPCODE_has_1ty(opcode)){
			temp = TY_name(WN_ty(wn));
		}
		
		if( rtype == MTYPE_U8 && strcmp(temp,"anon_ptr.") == 0){
			IntegerType* intType = IntegerType::get(w2lMod->getContext(), 64);
			
			PtrToIntInst* ptr = new PtrToIntInst(w2lpregMap[strName], 
												 intType,  "", *labelEntry);
			return ptr;
		}
		else{
			return w2lpregMap[strName];
		}	
	}
	
	assert(false &&" missing load value here");
	return nullptr;
	
}

Value* createLoad_llvm(WN* wn, Value* alloc, BasicBlock *labelEntry)
{
	// below is old implementation, just for reference use. 
	//W2LSYM w2lsym = Get_Name_Level_Num_llvm (WN_st_idx(wn));
	//string str(w2lsym.symName); 
	//assert(symMap.find(str) != symMap.end() && 
	//	   "assert fail in createLoad_llvm\n");
	//W2LSTRUCT strTemp = symMap[str];
	
	//1) get dtype and rtype
	OPCODE opc = WN_opcode(wn);  	
	TYPE_ID  dtype = OPCODE_desc(opc);
	TYPE_ID  rtype = OPCODE_rtype(opc);
	
	//2)  get type from wn
	
//	TY ty_load;
	
	TY_IDX tyidx;
	TY_IDX ty_load_idx;
	if(OPCODE_has_1ty(opc) && WN_ty(wn) != (TY_IDX) 0){
		tyidx = WN_ty(wn);
	}
	else if(OPCODE_has_2ty(opc) && WN_ty(wn) != (TY_IDX) 0
				   && WN_load_addr_ty(wn) != (TY_IDX) 0 ){
		tyidx = WN_ty(wn);
		ty_load_idx = WN_load_addr_ty(wn);
	}
	else{
		assert(false && "no type information here");
	}
	
	
	if(OPERATOR_has_sym(WN_operator(wn))){
		
		ST_IDX st_idx = WN_st_idx(wn);
		assert(st_idx != (ST_IDX) 0 && "st_idx error ");
		const ST* wn_st = &St_Table[st_idx];
		// get type from symbol
		tyidx = ST_type (st_idx);
		const TY& st_ty = Ty_Table[ST_type (st_idx)];
		char* char_st_name = ST_name (wn_st);
		string str(char_st_name);
		
	}
	
	const TY& ty = Ty_Table[tyidx];
	
	//const TY& load_ty = Ty_Table[ty_load_idx];
	
	//assert((w2lAllocMap.find(str) != w2lAllocMap.end()) &&
	//	        "error in createLoad_llvm->w2lAllocMap\n ");
	//AllocaInst *alloc = w2lAllocMap[str];
	
	if(!alloc->getType()->isPointerTy()){
		/*
		I4I4ILOAD 0 T<54,course,4> T<56,anon_ptr.,8> <field_id:1>
          U8ADD
            U8MPY
             U8I8CVT
             I8I4LDID 0 <2,5,i> T<9,.predef_U8,8>
            U8INTCONST 36 (0x24)
           U8U8LDID 0 <2,4,ptr> T<56,anon_ptr.,8>*/
		assert(alloc->getType()->isIntegerTy() &&"alloc should be integer type");
		PointerType* IntPtrTy = (PointerType*)createLLVMType_llvm(ty);
		
		IntToPtrInst* ptr = new IntToPtrInst(alloc, 
											 IntPtrTy,  "", labelEntry);
		alloc = ptr;
	}
		
	if( ty.kind == TY_KIND::KIND_SCALAR){
		
		LoadInst *ld = new LoadInst(alloc, "", false, labelEntry);
		//ld->setAlignment(cast<AllocaInst>(alloc)->getAlignment());
		ld->setAlignment(MTYPE_alignment(dtype));
		Value* ld_convert = convert_llvm(rtype, dtype, ld, labelEntry);
		return ld_convert;
		
	}
	else if(ty.kind == TY_KIND::KIND_POINTER){
		
		assert(rtype == MTYPE_U8 && dtype == MTYPE_U8 &&" error here");
		
		int offset = 0;
		offset= WN_offset(wn);
		
		if (offset>0 && WN_operator(wn) == OPR_LDID){
			assert(false);
		}
		
		if(offset>0 && WN_operator(wn) == OPR_ILOAD ){
			/*U8U8ILOAD 8 T<55,anon_ptr.,8> T<65,anon_ptr.,8>
			    U8U8LDID 0 <2,4,a> T<65,anon_ptr.,8> 
				char** a;
				*/
				
			uint64_t pointer_offset = offset/8;
			ConstantInt* const_int64_42 = ConstantInt::get(
											  IntegerType::get(w2lMod->getContext(), 64), 
											  pointer_offset);
			
			//const TY& ty_load = Ty_Table[ty_load_idx];
			PointerType* PtrTy = (PointerType*)createLLVMType_llvm(ty);
			 GetElementPtrInst* ptr_arrayidx = 
				 GetElementPtrInst::Create(PtrTy , alloc, const_int64_42, "pointeridx", labelEntry);
			 alloc = ptr_arrayidx; 
		}
		
		LoadInst *ld = new LoadInst(alloc, "", false, labelEntry);
		//ld->setAlignment(cast<AllocaInst>(alloc)->getAlignment());
		ld->setAlignment(MTYPE_alignment(dtype));
		return ld;
		
	}	
	else if(ty.kind == TY_KIND::KIND_STRUCT){
		
		int field_id = 0;
		field_id = WN_field_id(wn);
		
		int offset = 0;
		offset= WN_offset(wn);
		int byte_size = MTYPE_byte_size(dtype);
		
		
		//MMLDID 0 <2,4,n1> T<54,complex,4>
		if(field_id == 0){
			LoadInst* ld = new LoadInst(alloc, "", false, labelEntry);
			ld->setAlignment( MTYPE_alignment(dtype) );
			return ld;
		}
		
		
		Value* field_address = getField_llvm(ty, offset, field_id, alloc, labelEntry);	
		
		LoadInst* ld = new LoadInst(field_address, "", false, labelEntry);
		ld->setAlignment( MTYPE_alignment(dtype) );
		
		Value* ld_convert = convert_llvm(rtype, dtype, ld, labelEntry);
		return ld_convert;
		
		/* hard conversion method, DON'T DELETE
		-------------------------------------------------------
		char num[256];
		assert(offset % byte_size == 0 && "offset value is wrong");
		offset= offset/byte_size;
		sprintf(num, "%ld" , offset);
		ConstantInt* const_int64 = ConstantInt::get(w2lMod->getContext(),
								   APInt(64, num, 10));
		
		Type* pointee = createScalarLLVMTypeFromTypeID_llvm(dtype);
		
		PointerType* IntPtrTy = PointerType::get(pointee, 0);
		
		CastInst* castPtr = new BitCastInst(alloc, IntPtrTy, "", labelEntry);
		
		Instruction* ptr_offset = GetElementPtrInst::Create(
									  pointee, 
									  castPtr, const_int64, "castPtr", labelEntry);
		
		LoadInst* ld = new LoadInst(ptr_offset, "", false, labelEntry);
		ld->setAlignment( MTYPE_alignment(dtype) );
		
		Value* ld_convert = convert_llvm(rtype, dtype, ld, labelEntry);
		return ld_convert;
		*/
	}
	else if(ty.kind == TY_KIND::KIND_ARRAY){
		
		if(O3){
			
			
			ConstantInt* const_int32_21 = ConstantInt::get(w2lMod->getContext(), 
										  APInt(32, StringRef("0"), 10));
			
			std::vector<Value*> ptr_arraydecay_indices;
			ptr_arraydecay_indices.push_back(const_int32_21);
			
			ConstantInt* const_int32_22 = ConstantInt::get(w2lMod->getContext(), 
										  APInt(64, StringRef("0"), 10));
			
			ptr_arraydecay_indices.push_back(const_int32_22);
			
			Type* llvmType = createLLVMType_llvm(ty);
			Instruction* ptr_arraydecay = GetElementPtrInst::Create(llvmType,
										  alloc, ptr_arraydecay_indices, "arrayAddress", labelEntry);
			
			
			
			LoadInst *ld = new LoadInst(ptr_arraydecay, "", false, labelEntry);
			//ld->setAlignment(cast<AllocaInst>(alloc)->getAlignment());
			ld->setAlignment(MTYPE_alignment(dtype));
			Value* ld_convert = convert_llvm(rtype, dtype, ld, labelEntry);
			return ld_convert;
		}
		else{
		
		
			ST_IDX st_idx = WN_st_idx(wn);
			assert(st_idx != (ST_IDX) 0 && "st_idx error ");
			const ST* wn_st = &St_Table[st_idx];
			string str = buildSymName_llvm(wn_st);
			assert(w2lInitMap.find(str) != w2lInitMap.end() && " fail in w2lInitMap");
			return w2lInitMap[str];
		}
		
	}
	else{
		assert(false && "missing type here");
	}
}

Value* createCIOR_llvm(const WN* wn, BasicBlock** labelEntry){
#if 0
 %0 = load i8, i8* %c, align 1
  %conv = sext i8 %0 to i32
  %cmp = icmp eq i32 %conv, 97
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %1 = load i8, i8* %c, align 1
  %conv3 = sext i8 %1 to i32
  %cmp4 = icmp eq i32 %conv3, 101
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %2 = phi i1 [ true, %entry ], [ %cmp4, %lor.rhs ]
  %lor.ext = zext i1 %2 to i32
  store i32 %lor.ext, i32* %isLowercaseVowel, align 4


I4STID 0 <2,5,isLowercaseVowel> T<4,.predef_I4,4> {line: 1/13}
  U4I4CVT
   I4CIOR
    U4I4EQ
     I4I1LDID 0 <2,4,c> T<2,.predef_I1,1>
     I4INTCONST 97 (0x61)
    U4I4EQ
     I4I1LDID 0 <2,4,c> T<2,.predef_I1,1>
     I4INTCONST 101 (0x65)	
#endif
	
	 
	BasicBlock* label_lor_rhs = BasicBlock::Create(w2lMod->getContext(), 
			 "lor.rhs",currentFun,0);

     BasicBlock* label_lor_end = BasicBlock::Create(w2lMod->getContext(), 
              "lor.end",currentFun,0);

	 Value* val1 = nullptr;
     Value* val2 = nullptr;
	 
	 ConstantInt* const_int1_17 = ConstantInt::get(w2lMod->getContext(), 
								  APInt(1, StringRef("-1"), 10));  
	 
	 WN* wnkid = WN_kid(wn,0);
	 val1 = ir_put_expr_llvm(wnkid, 1, labelEntry);
	 assert(val1!=nullptr && "there is error ");
	  
	 BranchInst::Create(label_lor_end, label_lor_rhs, val1, *labelEntry); 
	 
	 // Block lor.rhs (label_lor_rhs)
	 wnkid = WN_kid(wn, 1);
	 val2 = ir_put_expr_llvm(wnkid, 1, &label_lor_rhs);
	 assert(val2!=nullptr && "there is error ");
	 
	 BranchInst::Create(label_lor_end, label_lor_rhs);
  
	 // Block lor.end (label_lor_end)	 
  PHINode* int1_23 = PHINode::Create(IntegerType::get(w2lMod->getContext(), 1), 
									 2, "", label_lor_end);
  int1_23->addIncoming(const_int1_17, *labelEntry);
  int1_23->addIncoming(val2, label_lor_rhs);
  *labelEntry = label_lor_end; 
  
  //wn->common.rtype = MTYPE_I1;
  
  return int1_23;
  
  /*
  OPCODE opc = WN_opcode(wn);
  TYPE_ID tyid = OPCODE_rtype(opc);
  
  if(tyid == MTYPE_I4){
	  CastInst* int32_lor_ext = new ZExtInst(int1_23, 
					IntegerType::get(w2lMod->getContext(), 32), "lor.ext", label_lor_end);
	 
	  *labelEntry = label_lor_end; 
	  return int32_lor_ext;
  }
  else{
	  assert(false &&" reach here");
	  //return int1_23;
  }
  */
}


Value* createCAND_llvm(const WN* wn, BasicBlock** labelEntry){

																												 
	BasicBlock* label_land_rhs = BasicBlock::Create(w2lMod->getContext(), 
								 "land.rhs",currentFun,0);
	BasicBlock* label_land_end = BasicBlock::Create(w2lMod->getContext(), 
								 "land.end",currentFun,0);
	
	
	Value* val1 = nullptr;
	Value* val2 = nullptr;
	
	ConstantInt* const_int1_17 = ConstantInt::get(w2lMod->getContext(), 
								 APInt(1, StringRef("0"), 10));  
	
	WN* wnkid = WN_kid(wn,0);
	val1 = ir_put_expr_llvm(wnkid, 1, labelEntry);
	assert(val1!=nullptr && "there is error ");
	
	BranchInst::Create(label_land_rhs, label_land_end, val1, *labelEntry); 
	
	// Block lor.rhs (label_lor_rhs)
	wnkid = WN_kid(wn, 1);
	val2 = ir_put_expr_llvm(wnkid, 1, &label_land_rhs);
	assert(val2!=nullptr && "there is error ");
	
	BranchInst::Create(label_land_end, label_land_rhs);
	
	// Block lor.end (label_lor_end)	 
	PHINode* int1_23 = PHINode::Create(IntegerType::get(w2lMod->getContext(), 1), 
									   2, "", label_land_end);
	int1_23->addIncoming(const_int1_17, *labelEntry);
	int1_23->addIncoming(val2, label_land_rhs);
	*labelEntry = label_land_end; 
	return int1_23;
}



Value* createBOR_llvm(const WN* wn,BasicBlock* labelEntry){
	
	Value* val1 = nullptr;
	Value* val2 = nullptr;

	int i = 0;
	WN* wnkid = WN_kid(wn,i);
	assert(OPCODE_is_expression( WN_opcode(wnkid)) && 
		   "there is error in in ir_put_expr_llvm OPR_ADD");
	val1 = ir_put_expr_llvm(wnkid, 1, &labelEntry);
	
	wnkid = WN_kid(wn,++i);
	assert(OPCODE_is_expression( WN_opcode(wnkid)) && 
		   "there is error in in ir_put_expr_llvm OPR_ADD");
	val2 = ir_put_expr_llvm(wnkid, 1,&labelEntry);
	
	assert(val1!=nullptr && val2!=nullptr && " val1 or val2 is nullptr");
	
	BinaryOperator* bit_or = BinaryOperator::Create(Instruction::Or, val1, val2, 
							  "or", labelEntry);
	
	OPCODE opc = WN_opcode(wn);
	TYPE_ID tyrid = OPCODE_rtype(opc);
	
	//if(tyrid == MTYPE_I4){
	//	CastInst* int32_lor_ext = new ZExtInst(bit_or, 
	//			IntegerType::get(w2lMod->getContext(), 32), "lor.ext", labelEntry);
	//	return int32_lor_ext;
	//}
	
	return bit_or;
}


Value* createBAND_llvm(const WN* wn,BasicBlock* labelEntry){
	
	Value* val1 = nullptr;
	Value* val2 = nullptr;

	int i = 0;
	WN* wnkid = WN_kid(wn,i);
	assert(OPCODE_is_expression( WN_opcode(wnkid)) && 
			   "there is error in in ir_put_expr_llvm OPR_ADD");
	val1 = ir_put_expr_llvm(wnkid, 1,&labelEntry);
		
	wnkid = WN_kid(wn,++i);
	assert(OPCODE_is_expression( WN_opcode(wnkid)) && 
			   "there is error in in ir_put_expr_llvm OPR_ADD");
	val2 = ir_put_expr_llvm(wnkid, 1,&labelEntry);
	
	assert(val1!=nullptr && val2!=nullptr && " val1 or val2 is nullptr");
		
	BinaryOperator* bit_and = BinaryOperator::Create(Instruction::And, val1, val2, 
								"and", labelEntry);
	return bit_and;
	
	//CastInst* int32_lor_ext = new ZExtInst(cmp,
	//				IntegerType::get(w2lMod->getContext(), 32), "lor.ext", labelEntry);
			
	//return int32_lor_ext;
	
}



Value* createLDA_llvm(const WN* wn, BasicBlock* labelEntry)
{
	
	Value* val = nullptr;
	
	OPCODE opc = WN_opcode(wn);		
	TYPE_ID  rtype = OPCODE_rtype(opc);
	
	ST_IDX st_idx= WN_st_idx(wn);
	assert(st_idx != (ST_IDX) 0 &&"error ir_put_expr_llvm  OPR_LDA \n");
	const ST* st = &St_Table[st_idx];
		
	//1) First get symbal name.
	char *name;
	if (ST_class(st) == CLASS_CONST) {
		/*U8LDA 0 <1,53,(4_bytes)_"%d\n\000"> T<61,anon_ptr.,8> 
		  	 [53]: <constant>    	<1,53> Constant of type .anonymous.2 (#60, KIND_ARRAY)*/
		name = "<constant>";
	} else{
		/* U8LDA 0 <2,5,a> T<56,anon_ptr.,8>
		[5]: a        <2,5> Variable of type .anonymous.1 (#55, KIND_ARRAY)*/
		name = ST_name(st);
	}
						
	if(ST_level(st) == 1 && ST_class(st) == CLASS_CONST ){
		
	    //LDA and It's constant, the most possible is constant string, so I need to assert it first.	
		TCON tc = STC_val(st);
		assert(TCON_ty(tc) == MTYPE_STRING && "Constant is not string");
		
		ConstantInt* const_int32_0 = ConstantInt::get(w2lMod->getContext(), 
									 APInt(32, StringRef("0"), 10));
		
		std::vector<Constant*> const_ptr_indices;
		const_ptr_indices.push_back(const_int32_0);
		const_ptr_indices.push_back(const_int32_0);
		
		string key = name;
		char temp[128];
		sprintf(temp, "_%d_%d", ST_level(st), ST_index(st));
		key += temp;
		
		assert(globalConstMap.find(key)!=globalConstMap.end()&&" error in OPR_LDA");
		
		GlobalVariable* array = (GlobalVariable*)(globalConstMap[key]);
		
		Constant* const_ptr = ConstantExpr::getGetElementPtr(
								  array->getValueType(), array, const_ptr_indices);
		return const_ptr;
	}
	
	Value* alloc = nullptr;
	string st_name_whole = buildSymName_llvm(st);
	 if(ST_level(st) == 2){
		 
		assert(w2lAllocMap.find(st_name_whole) != w2lAllocMap.end() &&"fail find Alloc");
		/*AllocaInst * */ alloc = w2lAllocMap[st_name_whole];
	 } else if(ST_level(st) == 1 ){
		 
		 assert(w2lGlobalValueMap.find(st_name_whole) != w2lGlobalValueMap.end() && 
				"error in createStore_llvm->w2lAllocMap\n");	
		 alloc = w2lGlobalValueMap[st_name_whole];	
	 
	 }
	 else{
		 assert(false);
	 }
		
		const TY& ty = Ty_Table[ST_type (st)];
		
		if(ty.kind == TY_KIND::KIND_SCALAR || ty.kind == TY_KIND::KIND_POINTER) {	
			return alloc;
		}
		else if(ty.kind == TY_KIND::KIND_ARRAY){
			
			ConstantInt* const_int32_21 = ConstantInt::get(w2lMod->getContext(), 
										  APInt(32, StringRef("0"), 10));
			
			std::vector<Value*> ptr_arraydecay_indices;
			ptr_arraydecay_indices.push_back(const_int32_21);
			ptr_arraydecay_indices.push_back(const_int32_21);
			
			Type* llvmType = createLLVMType_llvm(ty);
			Instruction* ptr_arraydecay = GetElementPtrInst::Create(llvmType,
										  alloc, ptr_arraydecay_indices, "arrayAddress", labelEntry);
			return ptr_arraydecay;
			
		}
		
		else if(ty.kind == TY_KIND::KIND_STRUCT ){
			
			if( !(OPCODE_has_field_id(WN_opcode(wn)) && WN_field_id(wn))) {
				return alloc;
			}
			
			int field_id = WN_field_id(wn);
			int offset = WN_offset(wn);

			
			Value* field_address = getField_llvm(ty, offset, field_id, alloc, labelEntry);	
			
			return field_address;
			/* fld version method, DON'T DELETE
			FLD* pfld =  getField_llvm(ty, offset, field_id);
			
			std::vector<Value*> ptr_mI_indices;
			ConstantInt* const_int32_0 = ConstantInt::get(
											 w2lMod->getContext(), APInt(32, StringRef("0"), 10));
			ptr_mI_indices.push_back(const_int32_0);		
			
			char numBuffer[20];
			sprintf(numBuffer,"%d", field_id-1) ;
			ConstantInt* const_int32_1 = ConstantInt::get(
											 w2lMod->getContext(), APInt(32, numBuffer, 10));
			ptr_mI_indices.push_back(const_int32_1);
			
			
			char field_name[256];
			strcpy(field_name, &Str_Table[pfld->name_idx]);
			Type* llvm_type = createLLVMType_llvm(ty);
			Instruction* ptr_mI = GetElementPtrInst::Create(
							llvm_type, alloc, ptr_mI_indices, field_name, labelEntry);
			
			return ptr_mI;*/
			
		}
		else{
			assert(false && "LDA, st_level = 2, missing a ty.kind");
		}
	//}
	//else{
	//	assert(false && "why I am here");
	//}
}

Value* createTRUNC_llvm(const WN* wn, BasicBlock** labelEntry){
	OPCODE opcode;
	opcode = WN_opcode(wn);
	assert(opcode != 0 && "opcode is null in ir_put_expr_llvm function");
	TYPE_ID  rtype = OPCODE_rtype(opcode);  
	int bitsize = MTYPE_bit_size(rtype);

	
	WN* wnkid = WN_kid(wn,0);
	
	assert( OPCODE_is_expression( WN_opcode(wnkid)) &&
			"there is error in in ir_put_expr_llvm OPR_ADD");

	Value * valOrigin = ir_put_expr_llvm(wnkid, 1, labelEntry);
	
	CastInst* int32_con = new FPToSIInst(
	  valOrigin, IntegerType::get(w2lMod->getContext(), bitsize), "fp2int", *labelEntry);
	
	return int32_con;
	
}
Value* createCVTL_llvm(const WN* wn, BasicBlock** labelEntry){
    
	
	//Detail of CVTL can be found in test15.c File
	/*
	  U4CVTL 8
        U4SUB
	*/
	OPCODE opcode;
	opcode = WN_opcode(wn);
	assert(opcode != 0 && "opcode is null in ir_put_expr_llvm function");
	TYPE_ID  rtype = OPCODE_rtype(opcode);  
	int bitsize = MTYPE_bit_size(rtype);
	int cvtl_bit_size = WN_cvtl_bits(wn);
	
	Value* val = nullptr;
	
	//1) get Value* from its child
	WN* wnkid = WN_kid(wn,0);
	
	assert( OPCODE_is_expression( WN_opcode(wnkid)) &&
			"there is error in in ir_put_expr_llvm OPR_ADD");

	Value * valOrigin = ir_put_expr_llvm(wnkid, 1, labelEntry);
	
	Value* valTemp = new TruncInst(valOrigin, 
			IntegerType::get(w2lMod->getContext(), cvtl_bit_size), "conv", *labelEntry);					

	val = new SExtInst(valTemp, 
					IntegerType::get(w2lMod->getContext(), bitsize), "conv3", *labelEntry);
	
	return val;
}




Value* createCVT_llvm(const WN* wn,BasicBlock** labelEntry){

	/*
	long z;
	printf("%f",z);
	------------------------------------------------
	F8PARM 2 T<11,.predef_F8,8> #  by_value 
      F8F4CVT
	    F4I8CVT
	      I8I8LDID 51 <1,5,.preg_I8> T<5,.predef_I8,8> # <preg>
		  
	Get a value and return another value;	  
	*/
	
	OPCODE opcode;
	opcode = WN_opcode(wn);
	assert(opcode != 0 && "opcode is null in ir_put_expr_llvm function");

	TYPE_ID  rtype = OPCODE_rtype(opcode);  //((((UINT32) opcode) >> 8) & 0x3F);
	TYPE_ID  desc  = OPCODE_desc (opcode); // ((((UINT32) opcode) >> 14) & 0x3F);
	
	Value* val = nullptr;
	
	//1) get Value* from its child
	WN* wnkid = WN_kid(wn,0);
	//bool isEx = OPCODE_is_expression( WN_opcode(wnkid));
	assert( OPCODE_is_expression( WN_opcode(wnkid)) &&
			"there is error in in ir_put_expr_llvm OPR_ADD");

	Value * valOrigin = ir_put_expr_llvm(wnkid, 1, labelEntry);

	//2) according to result and desc type, produce a llvm conversion statment.
	if(MTYPE_F8==rtype &&  MTYPE_F4==desc){	
		val = new FPExtInst(valOrigin, Type::getDoubleTy(w2lMod->getContext()), "convDouble", *labelEntry);	  
	}
	else if(MTYPE_F4==rtype &&  MTYPE_F8==desc){	
		val = new FPTruncInst(valOrigin, Type::getFloatTy(w2lMod->getContext()), "convDouble", *labelEntry);	  
	}
	else if(MTYPE_I8==rtype &&  MTYPE_I4==desc){
		val = new SExtInst(valOrigin, IntegerType::get(w2lMod->getContext(), 64), "conv6", *labelEntry);
	} 
    else if(MTYPE_F4==rtype &&  (MTYPE_I8==desc || MTYPE_I4==desc ||MTYPE_I2==desc ||
								 MTYPE_U8==desc || MTYPE_U4==desc ||MTYPE_U2==desc)) {
		val = new SIToFPInst(valOrigin, 
					Type::getFloatTy(w2lMod->getContext()), "conv9", *labelEntry);
	}
	else if(MTYPE_F8==rtype &&  (MTYPE_I8==desc || MTYPE_I4==desc ||MTYPE_I2==desc ||
								 MTYPE_U8==desc || MTYPE_U4==desc ||MTYPE_U2==desc)){
		val = new SIToFPInst(valOrigin, Type::getDoubleTy(w2lMod->getContext()), "conv9", *labelEntry);
	}
    else if(MTYPE_U8==rtype &&  MTYPE_I8==desc){
	    return valOrigin;   
    }
	else if(MTYPE_U4==rtype &&  MTYPE_I4==desc){
	   return valOrigin;   
	}
	else if((MTYPE_I4==rtype || MTYPE_U4==rtype) &&  (MTYPE_I8==desc || MTYPE_U8==desc) ){
		
		val = new TruncInst(valOrigin, 
					IntegerType::get(w2lMod->getContext(), 32), "conv", *labelEntry);
	}
	else if(MTYPE_V16F4==rtype &&  MTYPE_V16I4==desc){
	 VectorType* VectorTy_3 = VectorType::get(Type::getFloatTy(w2lMod->getContext()), 4);
		val = new SIToFPInst(
			valOrigin, VectorTy_3, "si2f", *labelEntry);
	}
	else 	
	{
		assert(!"miss type convert");
	}
	assert(val != nullptr && "val is null ");
	return val;
}


Value* createMAX_llvm(const WN* wn,BasicBlock** labelEntry){
	
	OPCODE opc = WN_opcode(wn);
	
	TYPE_ID  rtype = OPCODE_rtype(opc);
	
	Value* val1 = nullptr;
	Value* val2 = nullptr; 

	WN* wnkid1 = WN_kid(wn,0);
	if (wnkid1){	
		val1 = ir_put_expr_llvm(wnkid1, 1, labelEntry);
	}

	WN* wnkid2 = WN_kid(wn,1);
	if(wnkid2){
		val2 = ir_put_expr_llvm(wnkid2, 1,labelEntry);
	}
	
	if(O3 && rtype == MTYPE_V16F4){
		Function* fun = getIntrinsicMaxnum_v4f32_llvm();
		
		std::vector<Value*> packed_max1_params;
		packed_max1_params.push_back(val1);
		packed_max1_params.push_back(val2);
		CallInst* packed_max1 = CallInst::Create(
									fun, packed_max1_params, "maxv4f32", *labelEntry);
		packed_max1->setCallingConv(CallingConv::C);
		packed_max1->setTailCall(false);
		AttributeSet packed_max1_PAL;
		packed_max1->setAttributes(packed_max1_PAL);
		
		return packed_max1;
	}
	
	if(O3 && rtype == MTYPE_F4){
		Function* fun = getIntrinsicMaxnum_f32_llvm();
		
		std::vector<Value*> packed_max1_params;
		packed_max1_params.push_back(val1);
		packed_max1_params.push_back(val2);
		CallInst* packed_max1 = CallInst::Create(
									fun, packed_max1_params, "maxf32", *labelEntry);
		packed_max1->setCallingConv(CallingConv::C);
		packed_max1->setTailCall(false);
		AttributeSet packed_max1_PAL;
		packed_max1->setAttributes(packed_max1_PAL);
		
		return packed_max1;
	}
	
	//TYPE_ID  desc  = OPCODE_desc(opc);
	int bitsize = MTYPE_bit_size(rtype);
	
	
	BasicBlock* label_if_then = BasicBlock::Create(w2lMod->getContext(),
								"if.then", currentFun,0);

	BasicBlock* label_if_else = BasicBlock::Create(w2lMod->getContext(), 
								"if.else",currentFun,0);

	BasicBlock* label_if_end = BasicBlock::Create(w2lMod->getContext(),
							   "if.end", currentFun,0);

	
	/*
	WN* wnkid0 = WN_kid(wn,0);
	Value * val = ir_put_expr_llvm(wnkid0, 1,labelEntry);
	BranchInst::Create(label_if_then, label_if_else, val, *labelEntry);*/

	
	
	Value* cmp;
	
	if(rtype == MTYPE_U8 || rtype == MTYPE_U4){ 
		cmp = new ICmpInst(**labelEntry, ICmpInst::ICMP_UGT, 
						   val1, val2, "");
	}
	else if(rtype == MTYPE_I4 || rtype == MTYPE_I8){ 
		cmp = new ICmpInst(**labelEntry, ICmpInst::ICMP_SGT, 
						   val1, val2, "");
	}
	else if(rtype == MTYPE_F4 || rtype == MTYPE_F8){
		cmp = new FCmpInst(**labelEntry, FCmpInst::FCMP_OGT, 
						   val1, val2, "");
	}
	else{
		assert(false &&" missing mapping in here");
	}
	
	BranchInst::Create(label_if_then, label_if_else, cmp, *labelEntry);
	
	
	Value* val3 = nullptr;
	Value* val4 = nullptr; 

	if (wnkid1){
		
		val3 = ir_put_expr_llvm(wnkid1, 1, &label_if_then);
		
	}	
	BranchInst::Create(label_if_end, label_if_then);
	
	
	if(wnkid2){
		val4 = ir_put_expr_llvm(wnkid2, 1,&label_if_else);	
	}
	
	BranchInst::Create(label_if_end, label_if_else); 

	
	PHINode* int1_23 = PHINode::Create(IntegerType::get(w2lMod->getContext(), bitsize), 
									   2, "", label_if_end);
	int1_23->addIncoming(val3, label_if_then);
	int1_23->addIncoming(val4, label_if_else);
	*labelEntry = label_if_end; 
	return int1_23;
	
}



Value* createMIN_llvm(const WN* wn,BasicBlock** labelEntry){
	
	OPCODE opc = WN_opcode(wn);
	
	TYPE_ID  rtype = OPCODE_rtype(opc);
	//TYPE_ID  desc  = OPCODE_desc(opc);
	int bitsize = MTYPE_bit_size(rtype);
	
	
	BasicBlock* label_if_then = BasicBlock::Create(w2lMod->getContext(),
								"if.then", currentFun,0);

	BasicBlock* label_if_else = BasicBlock::Create(w2lMod->getContext(), 
								"if.else",currentFun,0);

	BasicBlock* label_if_end = BasicBlock::Create(w2lMod->getContext(),
							   "if.end", currentFun,0);

	
	/*
	WN* wnkid0 = WN_kid(wn,0);
	Value * val = ir_put_expr_llvm(wnkid0, 1,labelEntry);
	BranchInst::Create(label_if_then, label_if_else, val, *labelEntry);*/

	Value* val1 = nullptr;
	Value* val2 = nullptr; 

	WN* wnkid1 = WN_kid(wn,0);
	if (wnkid1){	
		val1 = ir_put_expr_llvm(wnkid1, 1, labelEntry);
	}

	WN* wnkid2 = WN_kid(wn,1);
	if(wnkid2){
		val2 = ir_put_expr_llvm(wnkid2, 1,labelEntry);
	}
	
	Value* cmp;
	
	if(rtype == MTYPE_U8 || rtype == MTYPE_U4){ 
		cmp = new ICmpInst(**labelEntry, ICmpInst::ICMP_ULT, 
						   val1, val2, "");
	}
	else if(rtype == MTYPE_I4 || rtype == MTYPE_I8){ 
		cmp = new ICmpInst(**labelEntry, ICmpInst::ICMP_SLT, 
						   val1, val2, "");
	}
	else if(rtype == MTYPE_F4 || rtype == MTYPE_F8){
		cmp = new FCmpInst(**labelEntry, FCmpInst::FCMP_OLT, 
						   val1, val2, "");
	}
	else{
		assert(false &&" missing mapping in here");
	}
	
	BranchInst::Create(label_if_then, label_if_else, cmp, *labelEntry);
	
	
	Value* val3 = nullptr;
	Value* val4 = nullptr; 

	if (wnkid1){
		
		val3 = ir_put_expr_llvm(wnkid1, 1, &label_if_then);
		
	}	
	BranchInst::Create(label_if_end, label_if_then);
	
	
	if(wnkid2){
		val4 = ir_put_expr_llvm(wnkid2, 1,&label_if_else);	
	}
	
	BranchInst::Create(label_if_end, label_if_else); 

	
	PHINode* int1_23 = PHINode::Create(IntegerType::get(w2lMod->getContext(), bitsize), 
									   2, "", label_if_end);
	int1_23->addIncoming(val3, label_if_then);
	int1_23->addIncoming(val4, label_if_else);
	*labelEntry = label_if_end; 
	return int1_23;
	
}

Value* createCSELECT_llvm(const WN* wn,BasicBlock** labelEntry){
	
	OPCODE opc = WN_opcode(wn);
	
	TYPE_ID  rtype = OPCODE_rtype(opc);
	TYPE_ID  desc  = OPCODE_desc(opc);
	int bitsize = MTYPE_bit_size(rtype);
	
	
	BasicBlock* label_if_then = BasicBlock::Create(w2lMod->getContext(),
								"if.then", currentFun,0);

	BasicBlock* label_if_else = BasicBlock::Create(w2lMod->getContext(), 
								"if.else",currentFun,0);

	BasicBlock* label_if_end = BasicBlock::Create(w2lMod->getContext(),
							   "if.end", currentFun,0);

	WN* wnkid0 = WN_kid(wn,0);
	Value * val = ir_put_expr_llvm(wnkid0, 1,labelEntry);
	BranchInst::Create(label_if_then, label_if_else, val, *labelEntry);

	Value* val1 = nullptr;
	Value* val2 = nullptr;

	WN* wnkid1 = WN_kid(wn,1);
	if (wnkid1){
		
		val1 = ir_put_expr_llvm(wnkid1, 1, &label_if_then);
		
	}
	BranchInst::Create(label_if_end, label_if_then);


	WN* wnkid2 = WN_kid(wn,2);
	if(wnkid2){
		
		val2 = ir_put_expr_llvm(wnkid2, 1,&label_if_else);
		
	}
	BranchInst::Create(label_if_end, label_if_else); 

	
	PHINode* int1_23 = PHINode::Create(IntegerType::get(w2lMod->getContext(), bitsize), 
									   2, "", label_if_end);
	int1_23->addIncoming(val1, label_if_then);
	int1_23->addIncoming(val2, label_if_else);
	*labelEntry = label_if_end; 
	return int1_23;
	
}



Value* createREPLICATE_llvm(const WN* wn,BasicBlock** labelEntry){
	
	/*V16I4STID 55 <1,28,.preg_V16I4> T<31,.predef_V16I4,16> # <preg> {line: 0/0}
	  V16I4I4REPLICATE
        U4INTCONST 2 (0x2) 
		*/
	
	//1) get Value* from its child	
	WN* wnkid = WN_kid(wn,0);
	OPCODE opc = WN_opcode(wn);
		
	assert( OPCODE_is_expression( opc) &&
			"there is error in in ir_put_expr_llvm OPR_ADD");
		
	TYPE_ID  rtype = OPCODE_rtype(opc);
	TYPE_ID  desc  = OPCODE_desc(opc);
		
	Value * valOrigin = ir_put_expr_llvm(wnkid, 1,labelEntry);
	
	if(rtype == MTYPE_V16I4 && desc == MTYPE_I4){
		VectorType* VectorTy_yan_4 = VectorType::get(IntegerType::get(w2lMod->getContext(), 32), 4);

		std::vector<Constant*> const_packed_4_elems;
		
		assert(isa<ConstantInt>(valOrigin) && "error in ir_put_expr_llvm OPR_REPLICATE");
		ConstantInt* const_int32 = dyn_cast<ConstantInt>(valOrigin);
		int64_t v1= const_int32->getSExtValue();
		int64_t v2 = v1+v1; 
		int64_t v3= v2+v1;
		int64_t v4= v3+v1;
		
		IntegerType* type = (IntegerType*)createScalarLLVMType_llvm(desc);
		
		ConstantInt* const_int32_2 = ConstantInt::getSigned(type, v2);
		ConstantInt* const_int32_3 = ConstantInt::getSigned(type, v3);
		ConstantInt* const_int32_4 = ConstantInt::getSigned(type, v4);
		
		
		//for(int i = 0;i<4;i++){
			const_packed_4_elems.push_back(const_int32);
			const_packed_4_elems.push_back(const_int32_2);
			const_packed_4_elems.push_back(const_int32_3);
			const_packed_4_elems.push_back(const_int32_4);
		//}
		
		Constant* const_packed = ConstantVector::get( const_packed_4_elems);
		return const_packed;
	}
	else if(rtype == MTYPE_V16F4 && desc == MTYPE_F4){
		VectorType* VectorTy_yan_4 = VectorType::get(
										 Type::getFloatTy(w2lMod->getContext()), 4);

		Constant* const_packed_4_elems[4] ;
		ConstantFP* const_float_15 = ConstantFP::get(w2lMod->getContext(), 
									 APFloat(3.700000e+01f));
		
		ConstantInt* const_int32_0 = ConstantInt::
									 get(w2lMod->getContext(), APInt(32, StringRef("0"), 10));
				
		for(int i = 0;i<4;i++){
		/*const_packed_4_elems.push_back(const_int32_0);
		const_packed_4_elems.push_back(const_int32_0);
		const_packed_4_elems.push_back(const_int32_0);
		const_packed_4_elems.push_back(const_int32_0);*/
			const_packed_4_elems[0] = const_int32_0;
			const_packed_4_elems[1] = const_int32_0;
			const_packed_4_elems[2] = const_int32_0;
			const_packed_4_elems[3] = const_int32_0;
		}
		
		Constant* const_packed = ConstantVector::get( const_packed_4_elems);
		
		Value* val = new SIToFPInst(
			const_packed, VectorTy_yan_4, "si2f", *labelEntry);
		
		
		//CastInst* packed_bc = new BitCastInst(const_packed, VectorTy_yan_4,
		//									  "bc", *labelEntry);
		
		
		
		
		
		ConstantInt* const_int32_20 = ConstantInt::get(
										 w2lMod->getContext(), APInt(32, StringRef("0"), 10));
		ConstantInt* const_int32_21 = ConstantInt::get(
										 w2lMod->getContext(), APInt(32, StringRef("1"), 10));
		ConstantInt* const_int32_22 = ConstantInt::get(
										 w2lMod->getContext(), APInt(32, StringRef("2"), 10));
		ConstantInt* const_int32_23 = ConstantInt::get(
										w2lMod->getContext(), APInt(32, StringRef("3"), 10));
		
		InsertElementInst* packed_bc1 = InsertElementInst::
						Create(val, valOrigin, const_int32_20, "bc1", *labelEntry);

		InsertElementInst* packed_bc2 = InsertElementInst::
						Create(packed_bc1, valOrigin, const_int32_21, "bc2", *labelEntry);

		InsertElementInst* packed_bc3 = InsertElementInst::
						Create(packed_bc2, valOrigin, const_int32_22, "bc3", *labelEntry);

		InsertElementInst* packed_bc4 = InsertElementInst::
						Create(packed_bc3, valOrigin, const_int32_23, "bc4", *labelEntry);
		
		return packed_bc4;
	}
	else{
		assert(!"Missing type support in REPLICATE");
	}
}



/// \breif create Add in llvm
///
/// \p wn includes whirl inforamtion. \p labelEntry is produced previously, which is llvm obj.
/// You don't need to check wn value, it has been checked beore this function. 
///
/// \param vectVale two operand of add operator.
/// \param wn is whirl node. 
/// \return add operator in llvm. NULL if numVal is not empty 
Value* createAdd_llvm(WN* wn , vector<Value*> vectVal, int numVal, BasicBlock **labelEntry)
{
	assert(numVal == 2 && " numVal is not 2 in createAdd_llvm");
	
	
	OPCODE opcode = WN_opcode(wn);

	TYPE_ID  rtype = OPCODE_rtype( opcode);
	if(rtype == MTYPE_F4 || rtype == MTYPE_F8 || rtype == MTYPE_F16||rtype == MTYPE_V16F4){
		BinaryOperator *addRes = BinaryOperator::Create(
									 Instruction::FAdd, vectVal[0], vectVal[1], "add", *labelEntry);
		return  addRes;
	}
	else if ( rtype == MTYPE_I4 || rtype == MTYPE_I8 || rtype == MTYPE_V16I4 || 
			  rtype==MTYPE_U8 || rtype==MTYPE_U4 ){
		
		Type* type1 = vectVal[0]->getType();
		Type* type2 = vectVal[1]->getType();
		IntegerType* intType = IntegerType::get(w2lMod->getContext(), 64);	
		
		if(type1->isPointerTy()){
			PtrToIntInst* ptr = new PtrToIntInst(vectVal[0], intType,  "", *labelEntry);			
			vectVal[0] = ptr;
		}
		if(type2->isPointerTy()){
			PtrToIntInst* ptr = new PtrToIntInst(vectVal[1], intType,  "", *labelEntry);
			vectVal[1] = ptr;
		}
		
		BinaryOperator *addRes = BinaryOperator::Create(Instruction::Add, 
								 vectVal[0], vectVal[1], "add", *labelEntry);
		return  addRes;
	}
	else{
		assert( false &&"error in crateAdd_llvm, you need to add more type map in createAdd_llvm");
	}
	
}


Value* createSub_llvm(WN* wn , vector<Value*> vectVal, int numVal, BasicBlock *labelEntry)
{
	assert(numVal == 2 && " numVal is not 2 in createAdd_llvm");
	
	OPCODE opcode = WN_opcode(wn);

	TYPE_ID  rtype = OPCODE_rtype( opcode);
	
	if(rtype == MTYPE_F4 || rtype == MTYPE_F8 || rtype == MTYPE_F16){
		BinaryOperator *addRes = BinaryOperator::Create(Instruction::FSub, vectVal[0], vectVal[1], "add", labelEntry);
		return  addRes;
	}
	else if ( rtype == MTYPE_I4 || rtype == MTYPE_I8 || rtype == MTYPE_V16I4 || 
			  rtype==MTYPE_U8 || rtype==MTYPE_U4){
		BinaryOperator *addRes = BinaryOperator::Create(Instruction::Sub, 
								 vectVal[0], vectVal[1], "add", labelEntry);
		return  addRes;
	}
	else{
		assert( false &&"error in crateAdd_llvm, you need to add more type map in createAdd_llvm");
	}
	
}


Value* createDIVREM_llvm(const WN* wn ,  BasicBlock **labelEntry)
{
	
	vector<llvm::Value *> vectVal;
	int numVal = 0;
	
	TYPE_ID  rtype = OPCODE_rtype(WN_opcode(wn));

	for( int i = 0;i<2;i++){
		WN* wnkid = WN_kid(wn,i);
		assert(OPCODE_is_expression( WN_opcode(wnkid)) && 
			   "there is error in in ir_put_expr_llvm OPR_ADD");
		
		Value * valtemp = ir_put_expr_llvm(wnkid, 1,labelEntry);	
		
		TYPE_ID  child_type = OPCODE_rtype(WN_opcode(wnkid));
		if(rtype != child_type){
			valtemp = convert_llvm( rtype,child_type, valtemp, *labelEntry);
		}
		
		vectVal.push_back(valtemp);
		numVal++;
	}
	
	assert(numVal == 2 && " numVal is not 2 in createAdd_llvm");
	
	OPCODE opcode = WN_opcode(wn);
	TYPE_ID tyid =  OPCODE_rtype (opcode);
	
	
	AllocaInst *allocPtr = nullptr;
	
	string strName = "lowPart";
	string typeID = to_string(tyid);
	strName = strName+"_"+typeID;
	allocPtr = createAllocByTypeID_llvm(tyid, strName, *labelEntry);
	assert(allocPtr != nullptr);
	if(w2lAllocMap.find(strName) == w2lAllocMap.end()){
		
		w2lAllocMap.insert(pair<string, AllocaInst*>(strName, allocPtr));	
	}
	else{
		
	    w2lAllocMap[strName] = allocPtr;
	}
	
	
	BinaryOperator* div = nullptr;
	if(tyid ==MTYPE_I4 || tyid==MTYPE_I8){
		div = BinaryOperator::Create(Instruction::SDiv, 
									 vectVal[0], vectVal[1], "div", *labelEntry);
	}
	else if(tyid==MTYPE_F8 || tyid==MTYPE_F4)	{
		div = BinaryOperator::Create(Instruction::FDiv, 
									 vectVal[0], vectVal[1], "SDiv", *labelEntry);
	}
	else{
		assert(false &&" missing type here");
	}
	
	BinaryOperator* rem = nullptr;
	if(tyid ==MTYPE_I4 || tyid==MTYPE_I8){
		rem = BinaryOperator::Create(Instruction::SRem, 
									 vectVal[0], vectVal[1], "srem", *labelEntry);
	}
	else if(tyid ==MTYPE_F4 || tyid==MTYPE_F8 ||tyid==MTYPE_F16){
		rem = BinaryOperator::Create(Instruction::FRem, 
									  vectVal[0], vectVal[1], "frem", *labelEntry);
	}
	else{
		assert(false &&" missing type here");
	}
	
	StoreInst *st0 = new StoreInst(div, allocPtr, false, *labelEntry);
	st0->setAlignment(MTYPE_alignment(tyid));
	
	return rem;
	
}


Value* createDIV_llvm(const WN* wn ,  BasicBlock **labelEntry)
{
	
	vector<llvm::Value *> vectVal;
	int numVal = 0;
	TYPE_ID  rtype = OPCODE_rtype(WN_opcode(wn));

	for( int i = 0;i<2;i++){
		WN* wnkid = WN_kid(wn,i);
		assert(OPCODE_is_expression( WN_opcode(wnkid)) && 
			   "there is error in in ir_put_expr_llvm OPR_ADD");
		
		
		Value * valtemp = ir_put_expr_llvm(wnkid, 1, labelEntry);	
		
		TYPE_ID  child_type = OPCODE_rtype(WN_opcode(wnkid));
		if(rtype != child_type){
			valtemp = convert_llvm( rtype,child_type, valtemp, *labelEntry);
		}
		
		vectVal.push_back(valtemp);
		numVal++;
	}
	
	assert(numVal == 2 && " numVal is not 2 in createAdd_llvm");
	
	//OPCODE opcode = WN_opcode(wn);

	//TYPE_ID  rtype = (TYPE_ID)  ((((UINT32) opcode) >> 8) & 0x3F);
	
	//TY_IDX tyidx = WN_ty(wn);
	//TYPE_ID tyid = TY_id(tyidx);
	
	OPCODE opcode = WN_opcode(wn);
	TYPE_ID tyid =  OPCODE_rtype (opcode);
	
	BinaryOperator* div = nullptr;
	if(tyid ==MTYPE_I4 || tyid==MTYPE_I8){
		div = BinaryOperator::Create(Instruction::SDiv, 
									vectVal[0], vectVal[1], "div", *labelEntry);
	}
	else if(tyid==MTYPE_F8 || tyid==MTYPE_F4)	{
		div = BinaryOperator::Create(Instruction::FDiv, 
									 vectVal[0], vectVal[1], "SDiv", *labelEntry);
	}
	else{
		assert(false &&" missing type here");
	}
	return div;
}


Value* createREM_llvm(const WN* wn ,  BasicBlock **labelEntry)
{
	
	vector<llvm::Value *> vectVal;
	int numVal = 0;
	
	TYPE_ID  rtype = OPCODE_rtype(WN_opcode(wn));

	for( int i = 0;i<2;i++){
		WN* wnkid = WN_kid(wn,i);
		assert(OPCODE_is_expression( WN_opcode(wnkid)) && 
			   "there is error in in ir_put_expr_llvm OPR_ADD");
		
		Value * valtemp = ir_put_expr_llvm(wnkid, 1,labelEntry);	
		
		TYPE_ID  child_type = OPCODE_rtype(WN_opcode(wnkid));
		if(rtype != child_type){
			valtemp = convert_llvm( rtype,child_type, valtemp, *labelEntry);
		}
		
		vectVal.push_back(valtemp);
		numVal++;
	}
	
	assert(numVal == 2 && " numVal is not 2 in createAdd_llvm");
	
	//OPCODE opcode = WN_opcode(wn);

	//TYPE_ID  rtype = (TYPE_ID)  ((((UINT32) opcode) >> 8) & 0x3F);
	
	//TY_IDX tyidx = WN_ty(wn);
	//TYPE_ID tyid = TY_id(tyidx);
	
	OPCODE opcode = WN_opcode(wn);
	TYPE_ID tyid =  OPCODE_rtype (opcode);
	
	BinaryOperator* rem = nullptr;
	if(tyid ==MTYPE_I4 || tyid==MTYPE_I8){
		rem = BinaryOperator::Create(Instruction::SRem, 
									 vectVal[0], vectVal[1], "srem", *labelEntry);
	}
	else if(tyid ==MTYPE_F4 || tyid==MTYPE_F8 ||tyid==MTYPE_F16){
		rem = BinaryOperator::Create(Instruction::FRem, 
									 vectVal[0], vectVal[1], "frem", *labelEntry);
	}
	else{
		assert(false &&" missing type here");
	}
	
	return rem;
}




Value* createMpy_llvm(const WN* wn ,  BasicBlock **labelEntry)
{
	
	vector<llvm::Value *> vectVal;
	int numVal = 0;
	TYPE_ID  rtype = OPCODE_rtype(WN_opcode(wn));

	for( int i = 0;i<2;i++){
		WN* wnkid = WN_kid(wn,i);
		assert(OPCODE_is_expression( WN_opcode(wnkid)) && 
			   "there is error in in ir_put_expr_llvm OPR_ADD");
		
		
		
		Value * valtemp = ir_put_expr_llvm(wnkid, 1,labelEntry);	
		
		
		TYPE_ID  child_type = OPCODE_rtype(WN_opcode(wnkid));
		if(rtype != child_type){
			valtemp = convert_llvm( rtype,child_type, valtemp, *labelEntry);
		}
		
		vectVal.push_back(valtemp);
		numVal++;
	}
	
	assert(numVal == 2 && " numVal is not 2 in createAdd_llvm");
	
	//OPCODE opcode = WN_opcode(wn);

	//TYPE_ID  rtype = (TYPE_ID)  ((((UINT32) opcode) >> 8) & 0x3F);
	
	//TY_IDX tyidx = WN_ty(wn);
	//TYPE_ID tyid = TY_id(tyidx);
	
	OPCODE opcode = WN_opcode(wn);
	TYPE_ID tyid =  OPCODE_rtype (opcode);
	
	BinaryOperator* mul = nullptr;
	if(tyid ==MTYPE_I4 || tyid==MTYPE_I8 || tyid ==MTYPE_U4 || tyid==MTYPE_U8 ){
		mul = BinaryOperator::Create(Instruction::Mul, 
							   vectVal[0], vectVal[1], "mul", *labelEntry);
	}
	else if(tyid==MTYPE_F8 || tyid==MTYPE_F4)	{
		mul = BinaryOperator::Create(Instruction::FMul, 
							   vectVal[0], vectVal[1], "mul", *labelEntry);
	}
	else{
		assert(false &&" missing type here");
	}
	return mul;
}


void createDO_WHILE_llvm(const WN* wn, BasicBlock** labelEntry){
	
	BasicBlock* label_do_cond = BasicBlock::Create(w2lMod->getContext(),
								"do.cond", currentFun,0);		
	BasicBlock* label_do_body = BasicBlock::Create(w2lMod->getContext(), 
								"do.body",currentFun,0);		
	BasicBlock* label_do_end = BasicBlock::Create(w2lMod->getContext(),
							   "do.end", currentFun,0);	

	BranchInst::Create(label_do_body, *labelEntry);
	
	
	g_loopEndStack.push(label_do_end);

	ir_put_stmt_llvm(WN_kid(wn, 1), 1,label_do_body);

	g_loopEndStack.pop();

	
	BranchInst::Create(label_do_cond, label_do_body);

	Value *val = ir_put_expr_llvm(WN_kid(wn, 0), 1, &label_do_cond);

	BranchInst::Create(label_do_body, label_do_end, val, label_do_cond);

	*labelEntry = label_do_end;

}


void createSWITCH_llvm(const WN* wn, BasicBlock** labelEntry){

	/*			SWITCH 2 514 {line: 1/25}
	 SWITCH 2 514 {line: 1/25}
	  I4I4LDID 0 <2,6,_temp__switch_index0> T<4,.predef_I4,4>
	  BLOCK {line: 0/0}
	  CASEGOTO L258 0 {line: 0/0}
	  CASEGOTO L770 99 {line: 0/0}
	  END_BLOCK
	  GOTO L1026 {line: 0/0}
	 END_SWITCH
	LABEL L258 0 {line: 1/16}
	LABEL L258 0 {line: 1/16}
	 I4STID 0 <2,5,b> T<4,.predef_I4,4> {line: 1/19}
	  I4INTCONST 2 (0x2)
	 GOTO L514 {line: 1/20}
	LABEL L770 0 {line: 1/20}
	LABEL L770 0 {line: 1/20}
	 I4STID 0 <2,5,b> T<4,.predef_I4,4> {line: 1/22}
	  I4INTCONST 3 (0x3)
	 GOTO L514 {line: 1/23}
	LABEL L1026 0 {line: 1/23}
	LABEL L1026 0 {line: 1/23}
	 GOTO L514 {line: 1/25}
	LABEL L514 0 {line: 0/0}
	LABEL L514 0 {line: 0/0}
		
	   kid0 is expression
	   kid1 is block, includes all the caseGoto.
	   kid2 is default branch.
		
		
	*/

	OPCODE opc = WN_opcode(wn);

	int num_entries = 1;
	// how many cases: not includes default
	if (OPCODE_has_num_entries(opc))
		num_entries = WN_num_entries(wn);

	char block[256];
	//vector<BasicBlock*> vect_sw_block;

	vector<int> switchCase; 
	vector<int> caseLable;
	getSwitchConst_Lable_llvm(wn, switchCase, caseLable);


	// add normal cases:
	vector<BasicBlock*> vect_sw_block;
	for(int i = 0;i<num_entries;i++){
		sprintf(block,"%s%d", "sw.block", i);
		BasicBlock* label_sw_bb = BasicBlock::Create(w2lMod->getContext(), block,currentFun,0);
		vect_sw_block.push_back(label_sw_bb);
		
		g_switchCaseMap.insert(pair<int,BasicBlock*>(caseLable[i], label_sw_bb));
	}

	// add default case:
	BasicBlock* label_sw_default; 
	if (WN_kid_count(wn) > 2){
		//int last_lable =  WN_last_label(wn);
		//	ir_put_stmt_llvm(WN_kid(wn,2), indent+1,labelEntry);
		WN* wnkid2 = WN_kid(wn,2);
		OPCODE opcode = WN_opcode(wnkid2);
		int default_lable = 0;
		if (OPCODE_has_label(opcode))
			default_lable = WN_label_number(wnkid2);
		
		label_sw_default = BasicBlock::Create(w2lMod->getContext(), 
											  "sw.default",currentFun,0);
		
		g_switchCaseMap.insert(pair<int,BasicBlock*>(default_lable, label_sw_default));
	}

	// add end case:
	if(OPCODE_has_last_label(opc)){
		int last_lable =  WN_last_label(wn);
		g_endLabel = last_lable; 
		BasicBlock* label_sw_epilog = BasicBlock::Create(w2lMod->getContext(), 
									  "sw.epilog", currentFun,0);
		g_switchCaseMap.insert(pair<int,BasicBlock*>(last_lable, label_sw_epilog));
	}


	//Create the switch llvm statment. 
	Value* val = ir_put_expr_llvm(WN_kid(wn,0), 1,labelEntry);
	SwitchInst* sw = SwitchInst::Create(val, label_sw_default, num_entries, *labelEntry);

	for(int i = 0;i<num_entries;i++){
		char num[256];
		sprintf(num, "%ld" , switchCase[i]);
		ConstantInt* const_int32 = 
			  ConstantInt::get(w2lMod->getContext(), APInt(32, num, 10));
		sw->addCase(const_int32 , vect_sw_block[i]);
	}

	// Block sw.bb (label_sw_bb)
	//StoreInst* void_28 = new StoreInst(const_int32_12, ptr_b, false, label_sw_bb);
	//void_28->setAlignment(4);
	//BranchInst::Create(label_sw_epilog, label_sw_bb);
    
	//if (WN_kid_count(wn) > 2){
	  //Block sw.default (label_sw_default)
	 // BranchInst::Create(label_sw_epilog, label_sw_default);
	//}
	
	//*labelEntry = label_sw_epilog;
	

	// Block sw.epilog (label_sw_epilog)
	
}



void createWHILE_DO_llvm(const WN* wn, BasicBlock** labelEntry){
	BasicBlock* label_while_cond = BasicBlock::Create(w2lMod->getContext(),
								   "while.cond", currentFun,0);		

	BasicBlock* label_while_body = BasicBlock::Create(w2lMod->getContext(), 
								   "while.body",currentFun,0);

	BasicBlock* label_while_end = BasicBlock::Create(w2lMod->getContext(),
								  "while.end", currentFun,0);
	
	

	BranchInst::Create(label_while_cond, *labelEntry);
	
	BasicBlock* label_temp = label_while_cond;
	
	Value *val = ir_put_expr_llvm(WN_kid(wn, 0), 1, &label_while_cond);
	
	
	//see test21.c while statement judge on int 1, but 1 should be regarded as bool
	Type* llvm_ty =  val->getType();
	if(llvm_ty->isIntegerTy() && 
			((dyn_cast<IntegerType>(llvm_ty)->getBitWidth()) == 32)) {				
			 ConstantInt* const_0 = 
				 ConstantInt::get(w2lMod->getContext(), APInt(32, StringRef("0"), 10));
			Value* valtemp = new ICmpInst(**labelEntry, 
								ICmpInst::ICMP_NE, val,const_0, "icmp");
			val = valtemp;
	}
	
	BranchInst::Create(label_while_body, label_while_end, val, label_while_cond);
    
	g_loopEndStack.push(label_while_end);

	ir_put_stmt_llvm(WN_kid(wn, 1), 1,label_while_body);

	// why we need label_temp? can be seen test20.c
	label_while_cond = label_temp;
	BranchInst::Create(label_while_cond, label_while_body);
    
	*labelEntry = label_while_end;
	g_loopEndStack.pop();
	
}



void createOptLABEL_llvm(const WN* wn, BasicBlock** labelEntry){
	
	
	WN* wn_pre =  WN_prev (wn);
	 OPCODE opc_pre = WN_opcode(wn_pre);
	
   int label = WN_label_number(wn);
   string strLable = to_string(label);
   strLable = "L"+strLable;

   
   BasicBlock* label_br = nullptr;
   if(g_tfBranchMap.find(strLable) == g_tfBranchMap.end()){
	   label_br = BasicBlock::Create(
						w2lMod->getContext(), strLable ,currentFun,0);
	   
	   g_tfBranchMap.insert(pair<string, BasicBlock*>(strLable, label_br) );
	   
   }
   else{
	   label_br = g_tfBranchMap[strLable];
   }
   
   if(OPCODE_operator(opc_pre) != OPR_GOTO &&  !previousIsReturn(*labelEntry) ){
	   BranchInst::Create( label_br, *labelEntry);
   }
   
   //2) previous is not goto 
  //if(!(*labelEntry)->empty() && dyn_cast<BranchInst>(&((*labelEntry)->back())) == nullptr){
	   
   //}

   *labelEntry = label_br;
    	
}



void createLABEL_llvm(const WN* wn, BasicBlock** labelEntry){

	int label = WN_label_number(wn);
	map<int, BasicBlock*>::iterator it = g_switchCaseMap.find(label);
	//lable is associated with a switch statment.
	if( it !=  g_switchCaseMap.end()){
		
		if(label == g_endLabel)
			BranchInst::Create(g_switchCaseMap[g_endLabel], *labelEntry);
		
		*labelEntry = it->second;
		
		// finish mapping this switch statment, so clear g_switchCaseMap
		
		if(label == g_endLabel){
			g_endLabel = -1;
			g_switchCaseMap.clear();
		}
		
	}
	else{
		char num[30];
		sprintf(num,"%d",label);
		char labelName[128];
		strcpy(labelName,"LABEL_");
		strcat(labelName, num);
		BasicBlock* label_body = BasicBlock::Create(w2lMod->getContext(), 
								 labelName,currentFun,0);
		BranchInst::Create(label_body, *labelEntry);
		
		if(g_caseMap.find(label) == g_caseMap.end()){
			g_caseMap.insert(pair<int, BasicBlock*>(label, label_body));
		}
		
		*labelEntry = label_body;
	}
}


void createTRUEBR_llvm(WN* wn, BasicBlock** labelEntry){

	int label = WN_label_number(wn);
	string strLable = to_string(label);
	strLable = "L"+strLable;
	
	WN* wnkid1 = WN_kid(wn,0);
	assert(OPCODE_is_expression( WN_opcode(wnkid1)) &&"It's not expression");

	Value *val1 = ir_put_expr_llvm(wnkid1,1,labelEntry);

	BasicBlock* label_true_block = nullptr;
	
	if(g_tfBranchMap.find(strLable) == g_tfBranchMap.end()){
		
		label_true_block = BasicBlock::Create(w2lMod->getContext(), strLable ,currentFun,0);	
		g_tfBranchMap.insert(pair<string, BasicBlock*>(strLable, label_true_block) );
	}
	else{
		label_true_block = g_tfBranchMap[strLable];
	}
	
	
	BasicBlock* label_false_block = BasicBlock::Create(w2lMod->getContext(), "fb" ,currentFun,0);
	
	BranchInst::Create( label_true_block, label_false_block, val1, *labelEntry);

	*labelEntry = label_false_block;

}

void createFALSEBR_llvm(WN* wn, BasicBlock** labelEntry){

	int label = WN_label_number(wn);
	string strLable = to_string(label);
	strLable = "L"+strLable;

	WN* wnkid1 = WN_kid(wn,0);
	assert(OPCODE_is_expression( WN_opcode(wnkid1)) &&"It's not expression");

	Value *val1 = ir_put_expr_llvm(wnkid1,1,labelEntry);

	BasicBlock* label_true_block = BasicBlock::Create(w2lMod->getContext(), "tb" ,currentFun,0);
	
	BasicBlock* label_false_block = nullptr;
	
	if(g_tfBranchMap.find(strLable) == g_tfBranchMap.end()){
		
		label_false_block = BasicBlock::Create(w2lMod->getContext(), strLable ,currentFun,0);
		
		g_tfBranchMap.insert(pair<string, BasicBlock*>(strLable, label_false_block) );
	}
	else{
		label_false_block = g_tfBranchMap[strLable];
	}
	
	
	if( (val1->getType())->isIntegerTy() ){
		IntegerType* iTy = (IntegerType*)val1->getType();
		int bitWidth = iTy->getBitWidth();
		if(bitWidth != 1){
			val1 = new TruncInst(val1, 
								 IntegerType::get(w2lMod->getContext(), 1), "conv", *labelEntry);
		}
	}
	
	BranchInst::Create( label_true_block, label_false_block, val1, *labelEntry);

	*labelEntry = label_true_block;
}


int getAlign_llvm(const WN* wn){
	
	TY_IDX tyidx;
	OPCODE opc = WN_opcode(wn);
	assert(OPCODE_has_1ty(opc) && WN_ty(wn) != (TY_IDX) 0);
	tyidx = WN_ty(wn);
	int align = TY_align(tyidx );
	return align;
}

void createCALL_llvm(WN* wn,BasicBlock* labelEntry){
	
	vector<Value*> call_parm;
	for (int i = 0; i < WN_kid_count(wn); i++) {
		WN* wnkid = WN_kid(wn,i);
		Value *val = ir_put_expr_llvm(wnkid , 1,&labelEntry);
		//LoadInst* parm = new LoadInst(val, "", false, labelEntry);
		call_parm.push_back(val);
	}
	
	if( WN_operator(wn) == OPR_INTRINSIC_CALL ){
		string funName = INTRINSIC_name((INTRINSIC) WN_intrinsic(wn));
		if(funName == "MEMCPY"){
			
			WN* wnkid = WN_kid(wn,0);
			int align = getAlign_llvm(wnkid);
		
			ConstantInt* const_int32_19 = ConstantInt::get(IntegerType::get(w2lMod->getContext(), 32)
										  , align);
			
			ConstantInt* const_int1_20 = ConstantInt::get(
											 w2lMod->getContext(), APInt(1, StringRef("0"), 10));
			
			call_parm.push_back(const_int32_19);
			call_parm.push_back(const_int1_20);
		}
	}
		
	
	
	g_FunReturn = createCall_llvm(wn,call_parm,labelEntry);	
}

void createIF_llvm(const WN* wn, BasicBlock** labelEntry){

	BasicBlock* label_if_then = BasicBlock::Create(w2lMod->getContext(),
								"if.then", currentFun,0);

	BasicBlock* label_if_else = BasicBlock::Create(w2lMod->getContext(), 
								"if.else",currentFun,0);
  
	BasicBlock* label_if_end = nullptr;
	/*BasicBlock* label_if_end =  BasicBlock::Create(w2lMod->getContext(),
							   "if.end", currentFun,0); */

	WN* wnkid0 = WN_kid(wn,0);
	Value * val = ir_put_expr_llvm(wnkid0, 1,labelEntry);
	BranchInst::Create(label_if_then, label_if_else, val, *labelEntry);

	bool ifFlag = false;
	bool elseFlag = false;

	WN* wnkid1 = WN_kid(wn,1);
	if (wnkid1){
		//BasicBlock* labelTemp = label_if_then;
		ir_put_stmt_llvm(wnkid1, 1, label_if_then);
		//label_if_then = labelTemp;
	}
	
	// Why you need g_gotoFlag, see test21.c file
	
	if(g_gotoFlag != true && !previousIsReturn(label_if_then) ){
		//why I move into BasicBlock::Create here, See test37.c
		if(label_if_end == nullptr)
		     label_if_end =  BasicBlock::Create(w2lMod->getContext(),
									"if.end", currentFun,0); 	
	    BranchInst::Create(label_if_end, label_if_then);
	    ifFlag = true;
    }
	else{
		g_gotoFlag = false;
	}

	WN* wnkid2 = WN_kid(wn,2);
	if(wnkid2){
		//BasicBlock* labelTemp = label_if_else;
		ir_put_stmt_llvm(wnkid2, 1,label_if_else);
		//label_if_else = labelTemp;
	}
	
	if(!previousIsReturn(label_if_else) ){
		if(label_if_end == nullptr)
			label_if_end =  BasicBlock::Create(w2lMod->getContext(),
											   "if.end", currentFun,0); 	
		
		BranchInst::Create(label_if_end, label_if_else); 
		elseFlag = true;
    }
	//delete labelEntry; // delete old labelEntry,
	// maybe you will use it in the future, 
	//but here, just delete it. 
    if(elseFlag== true || ifFlag == true){
	    *labelEntry = label_if_end;
	}
	
}




static void makeLLVMModule(){
  
	SmallVector<Type*, 2> FuncTyArgs;
	FuncTyArgs.push_back(IntegerType::get(w2lMod->getContext(),32));
	FuncTyArgs.push_back(IntegerType::get(w2lMod->getContext(),32));
	
	
	FunctionType *FuncTy = FunctionType::get(IntegerType::get(w2lMod->getContext(),
						   32), FuncTyArgs, false);
	Function *funcSum = 
		Function::Create(FuncTy, GlobalValue::ExternalLinkage, "sum", w2lMod);
	funcSum->setCallingConv(CallingConv::C);
	
	
	
	Function::arg_iterator args = funcSum->arg_begin();
	Value * int32_a = args++;
	int32_a->setName("a");
	Value *int32_b = args++;
	int32_b->setName("b");
	

  BasicBlock *labelEntry = 
    BasicBlock::Create(w2lMod->getContext(), "entry",funcSum, 0);

   g_entry_labelEntry = labelEntry;

    map<string, W2LSTRUCT>::iterator it;
    map<string, W2LSTRUCT>::iterator end = symMap.end();
    //vector<AllocaInst *> vectAlloc;
    for(it = symMap.begin(); it!=end ;it++)
    {
	  
	  if((it->second).w2ltype==w2lI)
	  {
          //AllocaInst *ptrA = new AllocaInst(IntegerType::get(w2lMod->getContext(),it->typeLen), it->symName, labelEntry);
         //ptrA->setAlignment(it->align);
         //vectAlloc.push_back(ptrA);
		 //string strSymName(it->symName);
		 //w2lAllocMap.insert(std::pair<string, AllocaInst *>(strSymName, ptrA));
      }
	  else if((it->second).w2ltype==w2lF)
	  {
	  }
	  else if((it->second).w2ltype==w2lU)
	  {
	  }
	  else if((it->second).w2ltype==w2lA)
	  {
	  }
	  
    }	
  //AllocaInst *ptrA = 
  //  new AllocaInst(IntegerType::get(w2lMod->getContext(),32), "a.addr", labelEntry);
 // ptrA->setAlignment(4);
 // AllocaInst *ptrB =
 //   new AllocaInst(IntegerType::get(w2lMod->getContext(),32), "b.addr", labelEntry);
 // ptrA->setAlignment(4);

 // StoreInst *st0 = new StoreInst(int32_a, ptrA, false, labelEntry);
 // st0->setAlignment(4);
 // StoreInst *st1 = new StoreInst(int32_b, ptrB, false, labelEntry);
 // st1->setAlignment(4);

 // LoadInst *ld0 = new LoadInst(ptrA, "", false, labelEntry);
 // ld0->setAlignment(4);
 // LoadInst *ld1 = new LoadInst(ptrB, "", false, labelEntry);
 // ld0->setAlignment(4);
 // BinaryOperator *addRes = BinaryOperator::Create(Instruction::Add, ld0, ld1, "add", labelEntry);
//  ReturnInst::Create(w2lMod->getContext(), addRes, labelEntry);
  }


  
  
  
  

/*
**  Add node to list to be processed for a dep graph dump
**  List is processed in order
**  reset id if null. start at 1 (no good reason)
*/
static INT32 AddToDUMPDEP(WN *node)
{
  static DUMPDEPp	last;
  static INT32		id= 0;

  if (IR_DUMPDEP_head==NULL)
  {
    id=		1;
    last=	NULL;
  }

  {
    DUMPDEPp p;

    p = TYPE_L_ALLOC(DUMPDEP);

    DUMPDEP_node(p)=	node;
    DUMPDEP_id(p)=	id++;
    DUMPDEP_next(p)=	NULL;

    if (last)
    {
      DUMPDEP_next(last) = p;
      last=		p;
    }
    else
    {
      IR_DUMPDEP_head=	p;
      last=		p;
    }
    return DUMPDEP_id(p);
  }
}



extern void IR_reader_init(void)
{
  is_initialized = TRUE;
  MEM_POOL_Push(&MEM_local_pool);
#ifdef IR_TOOLS
  errmsg = (char *) MEM_POOL_Alloc(&MEM_local_pool, LINE_LEN);
  line   = (char *) MEM_POOL_Alloc(&MEM_local_pool, LINE_LEN);
//ir_build_hashtable();
#endif
  ir_ofile = stdout;
  follow_st = TRUE;
  USRCPOS_clear(last_srcpos);
}





/* If prefix is true, set the dump order to prefix order instead of postfix order */
/* return the old order. */
/* This function will soon be removed. R. Shapiro */
extern BOOL IR_set_dump_order(BOOL prefix)
{
   BOOL old_order;
   old_order = dump_parent_before_children;
   dump_parent_before_children = prefix;
   return (old_order);
}


#ifndef MONGOOSE_BE
extern void IR_reader_finish(void)
{
  MEM_POOL_Pop(&MEM_local_pool);
}


extern FILE *IR_open(char *filename)
{
   char prefix_check[6];
   ir_file = fopen(filename, "r");
   ir_line = 0;
   
   /* Check for prefix ordered file */
   (void) fread(prefix_check,6,1,ir_file);
   if (strncmp(prefix_check,"PREFIX",6)==0) {
      fclose(ir_file);
      fprintf(stderr,"File %s is prefix ordered and cannot be read.\n",filename);
   } else {
      /* seek back to the start of the file */
      rewind(ir_file);
   }
   return ir_file;
}

extern void IR_close(void)
{
  fclose(ir_file);
}


/*
 *  Write the IR into a file in ascii form.
 */
extern BOOL IR_open_output(char *filename)
{
  if (filename == NULL)
    ir_ofile = stdout;
  else
    if ((ir_ofile = fopen(filename, "w+")) == NULL) {
      ir_error("cannot open file for write");
      return FALSE;
    }
  return TRUE;
}


extern void IR_close_output(void)
{
  if (ir_ofile != NULL && ir_ofile != stdout)
    fclose(ir_ofile);
}
#endif /* MONGOOSE_BE */


/*========================================================================

   IR Error Handling
 
 ========================================================================*/
 
#define ir_chk_kids(m,n)   {if (m != n) ir_error("wrong number of kids"); }

static void ir_error(const char *s)
{
  fprintf(stderr, "Error parsing ascii IR at line %d: %s.\n", ir_line, s);
  exit(RC_INTERNAL_ERROR);
}


/*========================================================================

   Get source file path name from DST (taken from cgdwarf.c)
 
 ========================================================================*/

typedef struct {
  char *filename;
  INT incl_index;
  FILE *fileptr;
  INT max_line_printed;
} file_info;

static file_info *file_table = NULL;
static char **incl_table;
static INT cur_file_index = 0;
static BOOL file_table_generated = FALSE;

// separated the following functionality from IR_Dwarf_Gen_File_Table
// ir_print_filename allows us to print path names from DST onto the
// ir_ofile in the form of LOC entries.  This function can
// be called (with dump_filenames == TRUE) from multiple places.

static void ir_print_filename(BOOL dump_filenames)
{
/*
    * Optionally, (if dump_filenames == TRUE)
    * all path names present in the DST information may
    * be dumped to the ir_ofile in the form of LOC entries.
*/
  INT count;
  DST_IDX idx;
  DST_FILE_NAME *file;
  char *name;
  INT file_table_size;
  INT new_size;


  file_table_size = 0;
  file_table = NULL;
  count = 1;
  for (idx = DST_get_file_names (); 
       !DST_IS_NULL(idx); 
       idx = DST_FILE_NAME_next(file))
    {
      file = DST_FILE_IDX_TO_PTR (idx);
      if (DST_IS_NULL(DST_FILE_NAME_name(file))) {
        name = "NULLNAME";
      }
      else {
        name = DST_STR_IDX_TO_PTR (DST_FILE_NAME_name(file));
      }
      if (count >= file_table_size) {
        new_size = count + 10;
        if (file_table == NULL)
          file_table = (file_info *) malloc (new_size * sizeof (file_info));
        else 
          file_table = (file_info *) realloc (file_table, 
                                              new_size * sizeof (file_info));
        if (file_table == NULL) 
          fprintf(stderr, "IR_Dwarf_Gen_File_Table: Run out of memory\n");
        file_table_size = new_size;
      }
      file_table[count].filename = name;
      file_table[count].incl_index = DST_FILE_NAME_dir(file);
      file_table[count].fileptr = NULL;
      file_table[count].max_line_printed = 0;
      if (dump_filenames)
        fprintf (ir_ofile, " LOC 0 0 source files:\t%d\t\"%s/%s\"\n",
                 count,
                 incl_table[DST_FILE_NAME_dir(file)],
                 name);
      count++;
    }
  /* make sure all fileptr values are NULL */
  while (count < file_table_size) {
    file_table[count].fileptr = NULL;
    count++;
  }

} /* ir_print_filename */

extern void IR_Dwarf_Gen_File_Table (BOOL dump_filenames)
{
   /* Generate a table of the include directory names and a table
    * of the file names, based on the DST (Debugging Symbol Table).
    * The USRCPOS_filenum() is a positional index into the list of
    * file names as given by DST_get_file_names(), starting at 
    * index==1.  Similarly, the DST_FILE_NAME_dir(file_info) is a
    * positional index into the list of directory names as given by
    * DST_get_dir_names().
    * call ir_print_filename to optionally print all pathnames in the DST info
    */
  INT count;
  DST_IDX idx;
  DST_INCLUDE_DIR *incl;
  char *name;
  INT incl_table_size;
  INT new_size;

  if (file_table_generated && file_table != NULL) {
	/* only need to reset the fileptr and line_printed info */
  	for (count = 1; file_table[count].fileptr != NULL; count++) {
      		fclose (file_table[count].fileptr);
      		file_table[count].fileptr = NULL;
		file_table[count].max_line_printed = 0;
              }
	cur_file_index = 0;
        return;
        }

  incl_table_size = 0;
  incl_table = NULL;
  file_table = NULL;
  count = 1;
  for (idx = DST_get_include_dirs (); 
       !DST_IS_NULL(idx); 
       idx = DST_INCLUDE_DIR_next(incl))
    {
      incl = DST_DIR_IDX_TO_PTR (idx);
      name = DST_STR_IDX_TO_PTR (DST_INCLUDE_DIR_path(incl));
      if (count >= incl_table_size) {
        new_size = count + 10;
        if (incl_table == NULL)
          incl_table = (char **) malloc (new_size * sizeof (char *));
        else 
          incl_table = (char **) realloc (incl_table, new_size * sizeof (char *));
        if (incl_table == NULL) 
          fprintf(stderr, "IR_Dwarf_Gen_File_Table: Run out of memory\n");
        incl_table_size = new_size;
      }
      incl_table[count] = name;
      count++;
    }
#if defined(TARG_SL)
  /* Wenbo/2007-04-29: Because we use incl_table's 0th entry for current
     working dir. */
  if (incl_table == NULL)
    incl_table = (char **) malloc ((count + 2) * sizeof (char *));
  incl_table[0] = "./";
#endif
  
  ir_print_filename(dump_filenames); /* print the loc 0 0 heading */

  file_table_generated = TRUE;
} /* IR_Dwarf_Gen_File_Table */


extern void IR_Srcpos_Filename(SRCPOS srcpos,        /* in */
			       const char **fname,   /* out */
			       const char **dirname) /* out */
{
   /* Get two character strings denoting the file-name and directory
    * path-name for the file-number denoted by the srcpos.  fname
    * and dirname must both be non-NULL references to objects that
    * can denote the character strings returned by this subroutine.
    * For unknown file/directory components, *fname and/or *dirname 
    * will be set to NULL.
    */
   USRCPOS usrcpos;

   USRCPOS_srcpos(usrcpos) = srcpos;
   if (USRCPOS_filenum(usrcpos) == 0)
   {
      *fname = NULL;
      *dirname = NULL;
   }
   else
   {
      file_info *cur_file;

      if (!file_table_generated)
	 IR_Dwarf_Gen_File_Table(FALSE/*dump_filenames*/);

      cur_file = &file_table[USRCPOS_filenum(usrcpos)];
      *fname = cur_file->filename;
      *dirname = incl_table[cur_file->incl_index];
   }
} /* IR_Srcpos_Filename */

 struct file_dir
  {
    string name;
    int incl_index;
  };

extern void IPA_IR_Filename_Dirname(SRCPOS srcpos,        /* in */
                                     char *&fname,   /* out */
                                     char *&dirname) /* out */
{
  vector<string>incl_table_v;
  vector<file_dir>file_table_v;

  USRCPOS usrcpos;

   USRCPOS_srcpos(usrcpos) = srcpos;
   if (USRCPOS_filenum(usrcpos) == 0)
   {
      fname = NULL;
      dirname = NULL;
   }
   else
   {


  DST_IDX idx;
  DST_INCLUDE_DIR *incl;
  DST_FILE_NAME *file;
  char *name;



  for (idx = DST_get_include_dirs ();
       !DST_IS_NULL(idx);
       idx = DST_INCLUDE_DIR_next(incl))
    {
      incl = DST_DIR_IDX_TO_PTR (idx);
      name = DST_STR_IDX_TO_PTR (DST_INCLUDE_DIR_path(incl));
      string name_s = name;
      incl_table_v.push_back(name_s);

    }


  for (idx = DST_get_file_names ();
       !DST_IS_NULL(idx);
       idx = DST_FILE_NAME_next(file))
    {
      file = DST_FILE_IDX_TO_PTR (idx);
      if (DST_IS_NULL(DST_FILE_NAME_name(file))) {
        name = "NULLNAME";
      }
      else {
        name = DST_STR_IDX_TO_PTR (DST_FILE_NAME_name(file));
      }
      file_dir file_d;
      file_d.name  = name;
      file_d.incl_index = DST_FILE_NAME_dir(file);
      file_table_v.push_back(file_d);
    }

      int fileidx = USRCPOS_filenum(usrcpos) - 1 ;

      int len = strlen(file_table_v[fileidx].name.c_str());
      fname = new char[len+1];
      strcpy(fname,file_table_v[fileidx].name.c_str());

      int diridx = file_table_v[fileidx].incl_index - 1;
      len = strlen(incl_table_v[diridx].c_str());
      dirname = new char[len+1];
      strcpy(dirname,incl_table_v[diridx].c_str());
   }
}

#ifdef DRAGON
// Wei modified on 1211/2011
extern void Dragon_IPA_IR_Filename_Dirname(SRCPOS srcpos,        /* in */
                                     char *&fname,   /* out */
                                     char *&dirname) /* out */
{
  vector<string>incl_table_v;
  vector<file_dir>file_table_v;

  USRCPOS usrcpos;

   USRCPOS_srcpos(usrcpos) = srcpos;
   if (USRCPOS_filenum(usrcpos) == 0)
   {
      fname = NULL;
      dirname = NULL;
   }
   else
   {

  DST_IDX idx;
  DST_INCLUDE_DIR *incl;
  DST_FILE_NAME *file;
  char *name;

  for (idx = DST_get_include_dirs ();
       !DST_IS_NULL(idx);
       idx = DST_INCLUDE_DIR_next(incl))
    {
      incl = DST_DIR_IDX_TO_PTR (idx);
      name = DST_STR_IDX_TO_PTR (DST_INCLUDE_DIR_path(incl));
      string name_s = name;
      incl_table_v.push_back(name_s);

    }

  for (idx = DST_get_file_names ();
       !DST_IS_NULL(idx);
       idx = DST_FILE_NAME_next(file))
    {
      file = DST_FILE_IDX_TO_PTR (idx);
      if (DST_IS_NULL(DST_FILE_NAME_name(file))) {
        name = "NULLNAME";
      }
      else {
        name = DST_STR_IDX_TO_PTR (DST_FILE_NAME_name(file));
      }
      file_dir file_d;
      file_d.name  = name;
      file_d.incl_index = DST_FILE_NAME_dir(file);
      file_table_v.push_back(file_d);
    }

      int fileidx = USRCPOS_filenum(usrcpos) - 1 ;

      // wei added
      int len = strlen(file_table_v[0].name.c_str());
      fname = new char[len+1];
      strcpy(fname,file_table_v[0].name.c_str());

      int diridx = file_table_v[0].incl_index - 1;
      len = strlen(incl_table_v[diridx].c_str());

      dirname = new char[len+1];
      strcpy(dirname,incl_table_v[diridx].c_str());
   }
}

extern void IPA_IR_Srcpos_Filename(SRCPOS srcpos,        /* in */
                               const char **fname,   /* out */
                               const char **dirname) /* out */
{
   /* Get two character strings denoting the file-name and directory            
    * path-name for the file-number denoted by the srcpos.  fname               
    * and dirname must both be non-NULL references to objects that              
    * can denote the character strings returned by this subroutine.             
    * For unknown file/directory components, *fname and/or *dirname             
    * will be set to NULL.                                                      
    */
   USRCPOS usrcpos;

   USRCPOS_srcpos(usrcpos) = srcpos;
   if (USRCPOS_filenum(usrcpos) == 0)
   {
      *fname = NULL;
      *dirname = NULL;
   }
   else
   {
      file_info *cur_file;
      file_table_generated = 0;
      // if (!file_table_generated)                                             
         IR_Dwarf_Gen_File_Table(FALSE/*dump_filenames*/);

      cur_file = &file_table[USRCPOS_filenum(usrcpos)];
      *fname = cur_file->filename;
      *dirname = incl_table[cur_file->incl_index];
   }
} /* IR_Srcpos_Filename */
#endif

/*========================================================================

   Print source lines (taken from cgdwarf.c)
 
 ========================================================================*/
static void
print_source (SRCPOS srcpos)
{
  USRCPOS usrcpos;
  char srcfile[1024];
  char text[1024];
  file_info *cur_file;
  INT i;
  INT newmax;

  USRCPOS_srcpos(usrcpos) = srcpos;

  if (USRCPOS_filenum(usrcpos) == 0) { 
	/* ??? Shouldn't see this, but print it so know when it does occur */
	fprintf(ir_ofile, "LOC 0 %d\n", USRCPOS_linenum(usrcpos));
	return;
  }

  cur_file = &file_table[USRCPOS_filenum(usrcpos)];
  if (USRCPOS_filenum(usrcpos) != cur_file_index) {
    if (cur_file_index != 0) {
      /* close the previous file. */
      file_info *prev_file = &file_table[cur_file_index];
      fclose (prev_file->fileptr);
      prev_file->fileptr = NULL;
    }
    cur_file_index = USRCPOS_filenum(usrcpos);
    cur_file = &file_table[cur_file_index];
    /* open the new file. */
    sprintf (srcfile, "%s/%s", incl_table[cur_file->incl_index], 
				cur_file->filename);
    cur_file->fileptr = fopen (srcfile, "r");
    if (cur_file->fileptr == NULL) {
      cur_file_index = 0;	/* indicate invalid cur_file */
      fprintf (ir_ofile, " LOC %d %d\n", cur_file_index, 
	       USRCPOS_linenum(usrcpos));
      return;
    }
    cur_file->max_line_printed = 0;	/* reset so lines will be reprinted */
    newmax = USRCPOS_linenum(usrcpos) - 2;
  }
  else {
    newmax = USRCPOS_linenum(usrcpos) - 5;
  }
  if (cur_file->max_line_printed < newmax) {
    for (i = cur_file->max_line_printed; i < newmax; i++) {
      fgets (text, sizeof(text), cur_file->fileptr);
    }
    cur_file->max_line_printed = newmax;
  }
  if (cur_file->max_line_printed < USRCPOS_linenum(usrcpos)) {
    for (i = cur_file->max_line_printed; i < USRCPOS_linenum(usrcpos); i++) {
      if (fgets (text, sizeof(text), cur_file->fileptr) != NULL)
        fprintf (ir_ofile, " LOC %d %d %s", cur_file_index, i+1, text);
    }
    cur_file->max_line_printed = USRCPOS_linenum(usrcpos);
  }
  else if (cur_file->max_line_printed != USRCPOS_linenum(usrcpos)
	&& USRCPOS_linenum(usrcpos) != 0) 
  {
    /* 
     * could be a line before the last LOC;
     * too hard to print text, but at least print LOC
     */
    fprintf (ir_ofile, " LOC %d %d\n", cur_file_index, 
	USRCPOS_linenum(usrcpos));
  }
}

/*========================================================================

  Misc utilities

 ========================================================================*/
 

#ifdef IR_TOOLS	/* only included for tools which translate ascii->binary */

/*========================================================================

  Hash table routines for the lookup keywords.

    ir_insert_hash
    ir_lookup
    ir_build_hashtable
 
========================================================================*/


/*
 *  Find a new entry from the string in the operator table.
 */
static BOOL ir_insert_hash(const char *s, IR_OPCODE_TABLE *opcode_entry)
{
  const char *p;
  UINT sum = 0;
  INT i;

  for (p = s; *p != '\0'; p++)
    sum = (sum+sum+3377)^*p;	/* better than sum+= *p for our keyword set */
//if (sum < 0) sum = -sum;
  sum %= HASH_LEN;
  for (i = 0; i < HASH_LEN; i++) {
    if (opcode_hash[sum] == NULL) {
      opcode_hash[sum] = opcode_entry;
      return TRUE;
    }
    if (strcmp(opcode_hash[sum]->pr_name, s) == 0)
      return FALSE;
    sum = (sum + 1) % HASH_LEN;
  }
  return FALSE;
}


/*
 *  Lookup the string from the operator table.
 */
static INT ir_lookup(char *s)
{
  char *p;
  INT sum = 0;
  INT i;

  for (p = s; *p != '\0'; p++)
    sum = (sum+sum+3377)^*p;
  sum %= HASH_LEN;
  for (i = 0; i < HASH_LEN; i++) {
    if (opcode_hash[sum] == NULL)
      return -1;
    if (strcmp(opcode_hash[sum]->pr_name, s) == 0)
      return (opcode_hash[sum] - ir_opcode_table);
    sum = (sum + 1) % HASH_LEN;
  }
  return -1;
}


/*
 *  Enter opcode into the IR table.
 */
static void enter_opcode_table(const char *name, OPCODE opc, INT opr)
{
    ir_opcode_table[opc].pr_name = name;
    if (!ir_insert_hash(ir_opcode_table[opc].pr_name, &ir_opcode_table[opc])) {
      sprintf(errmsg, "cannot insert %s into hash table", OPCODE_name(opc));
      ir_error(errmsg);
    }
}


static inline
void enter_opcode_table (const char *name, OPC_EXTENDED opc, INT opr)
{
    enter_opcode_table (name, (OPCODE) opc, opr);
}

/*
 *  Build IR table 
 */
static void ir_build_hashtable(void)
{
  INT i;
  INT opr;
  
  /* Clear the tables */
  memset(ir_opcode_table, 0, sizeof(ir_opcode_table));
  memset(opcode_hash,     0, sizeof(opcode_hash));

  /* Enter the WHIRL opcodes. */
  for (i = OPCODE_FIRST; i <= OPCODE_LAST; i++) {
      OPCODE opc = (OPCODE) i;
    opr = OPCODE_operator(opc);
    enter_opcode_table(((char *)OPCODE_name(opc))+strlen("OPC_"), opc, opr);
  }

  /* Enter the extended opcodes. */
  enter_opcode_table("BODY", OPC_BODY, OPR_BODY);
  enter_opcode_table("NOOP", OPC_NOOP, OPR_NOOP);
  enter_opcode_table("END_BLOCK", OPC_END_BLOCK, OPR_END_BLOCK);
  enter_opcode_table("THEN", OPC_THEN, OPR_THEN);
  enter_opcode_table("ELSE", OPC_ELSE, OPR_ELSE);
  enter_opcode_table("END_IF", OPC_END_IF, OPR_END_IF);
  enter_opcode_table("INIT", OPC_INIT, OPR_INIT);
  enter_opcode_table("COMP", OPC_COMP, OPR_COMP);
  enter_opcode_table("INCR", OPC_INCR, OPR_INCR);
  enter_opcode_table("END_COMPGOTO", OPC_END_COMPGOTO, OPR_END_COMPGOTO);
  enter_opcode_table("END_XGOTO", OPC_END_XGOTO, OPR_END_XGOTO);
  enter_opcode_table("LOC", OPC_LOC, OPR_LOC);
  enter_opcode_table("END_LOOP_INFO", OPC_END_LINFO, OPR_END_LINFO);
  enter_opcode_table("END_SWITCH", OPC_END_SWITCH, OPR_END_SWITCH);
}

#endif /* IR_TOOLS */


/*========================================================================

  IR Ascii Writer
    ir_put_wn
    ir_put_expr
    ir_put_stmt
    IR_put_func

 ========================================================================*/







static void
ir_put_st (ST_IDX st_idx)
{
  char *name;
  char *p;
  
  if (st_idx == (ST_IDX) 0) {
    /* labels may have empty st */
    fprintf(ir_ofile, " <null-st>");

  } else if (!follow_st) {
    /* Do not follow ST *, so that it can dump useful information
       even when ST * is not valid */
    fprintf(ir_ofile, " <st %d>", (INT32) st_idx);

  } else {
    const ST* st = &St_Table[st_idx];
    if (ST_class(st) == CLASS_CONST) {
      name = Targ_Print(NULL, STC_val(st));
      /* new lines and spaces in constant strings 
       * will mess up the ascii reader,
       * so replace with underlines */
      for (p = name; *p != '\0'; p++)
	switch (*p) {
	case ' ':
	case '\t':
	case '\n':
	  *p = '_';
	}
    } else
      name = ST_name(st);
    fprintf (ir_ofile, " <%d,%d,%s>", ST_level (st), ST_index (st), name);
  }
}

#ifdef BACK_END
extern "C" UINT16 LNOGetVertex(WN *);
extern "C" BOOL LnoDependenceEdge(WN *, WN *, mUINT16 *, DIRECTION *, BOOL *, BOOL *);
#endif


/* 
 * little routine to print TY's and their attributes, since otherwise they aren't visible
 */
static void
ir_put_ty(TY_IDX ty)
{
   fprintf(ir_ofile, " T<%d,%s,%d", TY_id(ty),
	   TY_name(ty),TY_align(ty));
   
   if (TY_is_restrict(ty)) 
      fprintf(ir_ofile, ",R");
   
   if (TY_is_volatile(ty)) 
      fprintf(ir_ofile, ",V");
   
   if (TY_is_const(ty)) 
      fprintf(ir_ofile, ",C");
   
   fprintf(ir_ofile, ">");
}

#if defined(BACK_END) || defined(IR_TOOLS)
static void ir_put_phi_list(WN* wn, INT indent)
{
   const WSSA::WHIRL_SSA_MANAGER * wsm = PU_Info_ssa_ptr(Current_PU_Info);
   if (wsm->Stat() != WSSA::STAT_DUMP)
      return;

   if (! wsm->WN_has_phi(wn))
      return;

   ir_put_marker("PHI NODES",indent);
   for (WSSA::WHIRL_SSA_MANAGER::const_phi_iterator phi_iter = wsm->WN_phi_begin(wn);
        phi_iter != wsm->WN_phi_end(wn);
        ++phi_iter) {
      phi_iter->Print(ir_ofile, indent);
   }
} 

static void ir_put_chi_list(WN* wn, INT indent)
{
   const WSSA::WHIRL_SSA_MANAGER * wsm = PU_Info_ssa_ptr(Current_PU_Info);
   if (wsm->Stat() != WSSA::STAT_DUMP)
      return;

   if (! wsm->WN_has_chi(wn))
      return;

   ir_put_marker("CHI NODES",indent);
   for (WSSA::WHIRL_SSA_MANAGER::const_chi_iterator chi_iter = wsm->WN_chi_begin(wn);
        chi_iter != wsm->WN_chi_end(wn);
        ++chi_iter) {
      chi_iter->Print(ir_ofile, indent);
   }
}

static void ir_put_mu_list(WN* wn, INT indent)
{
   const WSSA::WHIRL_SSA_MANAGER * wsm = PU_Info_ssa_ptr(Current_PU_Info);
   if (wsm->Stat() != WSSA::STAT_DUMP)
       return;

   if (! wsm->WN_has_mu(wn))
      return;

   ir_put_marker("MU NODES",indent);
   for (WSSA::WHIRL_SSA_MANAGER::const_mu_iterator mu_iter = wsm->WN_mu_begin(wn);
        mu_iter != wsm->WN_mu_end(wn);
        ++mu_iter) {
      mu_iter->Print(ir_ofile, indent);
   }
}
#endif




/*
 *  Write an WN * in ascii form on an individual line.
 */ 
static Value*  ir_put_wn_llvm(WN * wn, INT indent , BasicBlock *labelEntry)
{
	Value* val = NULL;
    OPCODE opcode;

    if (wn == NULL) { 
    	/* null statement */
    	fprintf(ir_ofile, "### error: null WN pointer\n");
   	return NULL;
    }
	opcode = WN_opcode(wn);
	
    if (opcode == 0) {
	fprintf(ir_ofile,"### error: WN opcode 0\n");
	return NULL;
    }
	
	/*switch (OPCODE_operator(opcode)) {
		case OPR_LDID:
			val =  createLoad_llvm(wn,labelEntry);
		    break;
	    default:
			break;
	}
	
	return val; */   
	

#if defined(BACK_END) || defined(IR_TOOLS)
    if (OPT_Enable_WHIRL_SSA && WSSA::WN_has_mu(wn)) {
        ir_put_mu_list(wn, indent);
    }
#endif

    if (indent > 0 && opcode == OPC_LABEL)
	fprintf(ir_ofile, "%*s", indent-1, "");
    else
	fprintf(ir_ofile, "%*s", indent, "");

    fprintf(ir_ofile, "%s", OPCODE_name(opcode) + strlen("OPC_"));
    if (OPCODE_has_offset(opcode)) {
	if (OPCODE_operator(opcode) == OPR_PRAGMA || 
	    OPCODE_operator(opcode) == OPR_XPRAGMA)
	    fprintf(ir_ofile, " %d %d", WN_pragma_flags(wn), WN_pragma(wn));
	else
	    fprintf(ir_ofile, " %d", WN_offset(wn));
    } else if (OPCODE_has_2offsets(opcode)) {
	fprintf(ir_ofile, " %d %d",
		WN_loop_trip_est(wn), WN_loop_depth(wn));
    }

    switch (OPCODE_operator(opcode)) {

    case OPR_INTRINSIC_OP:
    case OPR_ARRAYEXP:
#ifdef KEY
    case OPR_PURE_CALL_OP:
#endif
      fprintf(ir_ofile, " %d", WN_kid_count(wn));
      break;

    case OPR_REGION:
      fprintf(ir_ofile, " %d", WN_region_id(wn));
#ifdef BACK_END
      {
	RID *rid = REGION_get_rid(wn);
	if (rid != NULL)
	  fprintf(ir_ofile, " %d", RID_id(rid));
      }
#endif /* BACK_END */
      fprintf(ir_ofile, " (kind=%d)",WN_region_kind(wn));
      break;
#if defined(TARG_SL)
    case OPR_LDA:
    case OPR_ISTORE:
      fprintf(ir_ofile, "im:%d", WN_is_internal_mem_ofst(wn)); 
      break; 
#endif
    case OPR_LDBITS:
    case OPR_ILDBITS:
    case OPR_STBITS:
    case OPR_ISTBITS:
    case OPR_EXTRACT_BITS:
    case OPR_COMPOSE_BITS:
      fprintf(ir_ofile, " <bofst:%d bsize:%d>", WN_bit_offset(wn),
	      WN_bit_size(wn));
      break;

    case OPR_ASM_INPUT:
      fprintf(ir_ofile, " opnd:%d", WN_asm_opnd_num(wn));
      break;

    default:
      break;
    }

    if (OPCODE_has_inumber(opcode)) {
	switch (opcode) {
	case OPC_IO:
	    fprintf(ir_ofile, " <%d,%s,%s>", WN_intrinsic(wn),
		    IOSTATEMENT_name((IOSTATEMENT) WN_intrinsic(wn)),
		    get_iolibrary_name(WN_IO_Library(wn)));
	    break;
	case OPC_IO_ITEM:
	    fprintf(ir_ofile, " <%d,%s>", WN_intrinsic(wn),
		    IOITEM_name((IOITEM) WN_intrinsic(wn)));
	    break;
	default:		/* intrinsic */
	    Is_True(OPCODE_operator(opcode) == OPR_INTRINSIC_OP ||
		    OPCODE_operator(opcode) == OPR_INTRINSIC_CALL,
		    ("ir_put_wn, expected an intrinsic"));
#if defined(BACK_END) || defined(IR_TOOLS)
	    fprintf(ir_ofile, " <%d,%s>", WN_intrinsic(wn),
		    INTRINSIC_name((INTRINSIC) WN_intrinsic(wn)));
#endif
	    break;
	}
    }

    if (OPCODE_has_bits(opcode))
	fprintf(ir_ofile, " %d", WN_cvtl_bits(wn));
    if (OPCODE_has_label(opcode))
	fprintf(ir_ofile, " L%d", WN_label_number(wn));
    if (OPCODE_has_flags(opcode)) 
	fprintf(ir_ofile, " %d", WN_flag(wn));
    if (OPCODE_has_sym(opcode)) {
        ir_put_st (WN_st_idx(wn));
#if defined(BACK_END) || defined(IR_TOOLS)
        if (OPT_Enable_WHIRL_SSA) {
            WSSA::WHIRL_SSA_MANAGER * wsm = PU_Info_ssa_ptr(Current_PU_Info);
            Is_True( wsm != NULL, ("WHIRL SSA MANAGER is NULL") );
            if (wsm->Stat() == WSSA::STAT_DUMP) {
                if (wsm->WN_has_ver(wn)) {
                    WSSA::VER_IDX ver = wsm->Get_wn_ver(wn);
                    fprintf(ir_ofile, " ");
                    Print_ver(ir_ofile, ver);
                }
                else {
                    fprintf(ir_ofile, " INV_VER");
                }
            }
        }
#endif
    }

    if (OPCODE_has_1ty(opcode)) {
	if (WN_ty(wn) != (TY_IDX) 0) 
	   ir_put_ty(WN_ty(wn));
	else
	    if (opcode != OPC_IO_ITEM)
		fprintf(ir_ofile, " T<### ERROR: null ptr>");
    } else if (OPCODE_has_2ty(opcode)) {
	if (WN_ty(wn) != (TY_IDX) 0) 
	   ir_put_ty(WN_ty(wn));
	else
	    fprintf(ir_ofile, " T<### ERROR: null ptr>");
	if (WN_load_addr_ty(wn) != (TY_IDX) 0) 
	   ir_put_ty(WN_load_addr_ty(wn));
	else
	    fprintf(ir_ofile, " T<### ERROR: null ptr>");
    }

    if (OPCODE_has_ndim(opcode))
	fprintf(ir_ofile, " %d", WN_num_dim(wn));
    if (OPCODE_has_esize(opcode))
	fprintf(ir_ofile, " %" LL_FORMAT "d", WN_element_size(wn));

    if (OPCODE_has_num_entries(opcode))
	fprintf(ir_ofile, " %d", WN_num_entries(wn));
    if (OPCODE_has_last_label(opcode))
	fprintf(ir_ofile, " %d", WN_last_label(wn));

    if (OPCODE_has_value(opcode)) {
	fprintf(ir_ofile, " %" LL_FORMAT "d", WN_const_val(wn));
	/* Also print the hex value for INTCONSTs */
	if (OPCODE_operator(opcode) == OPR_INTCONST || opcode == OPC_PRAGMA) {
	    fprintf(ir_ofile, " (0x%" LL_FORMAT "x)", WN_const_val(wn));
	}
    }

    if (OPCODE_has_field_id(opcode) && WN_field_id(wn)) {
	fprintf(ir_ofile, " <field_id:%u>", WN_field_id(wn));
    }
    

    if (OPCODE_has_ereg_supp(opcode)) {
	INITO_IDX ino = WN_ereg_supp(wn);
	if (ino != 0)
	    fprintf (ir_ofile, " INITO<%d,%s>", INITO_IDX_index (ino), 
		     ST_name (INITO_st_idx (Inito_Table[ino])));
    }
    if (opcode == OPC_COMMENT) {
	fprintf(ir_ofile, " # %s", Index_To_Str(WN_offset(wn)));
    }

    if (follow_st && OPCODE_has_sym(opcode) && OPCODE_has_offset(opcode)
	&& WN_st_idx(wn) != (ST_IDX) 0 && (ST_class(WN_st(wn)) == CLASS_PREG)
        && opcode != OPC_PRAGMA)
	{
	    if (Preg_Is_Dedicated(WN_offset(wn))) {
		if (Preg_Offset_Is_Int(WN_offset(wn))) {
	    		fprintf(ir_ofile, " # $r%d", WN_offset(wn));
		}
		else if (Preg_Offset_Is_Float(WN_offset(wn))) {
	    		fprintf(ir_ofile, " # $f%d", 
				WN_offset(wn) - Float_Preg_Min_Offset);
		}
#ifdef TARG_X8664
		else if (Preg_Offset_Is_X87(WN_offset(wn))) {
	    		fprintf(ir_ofile, " # $st%d", 
				WN_offset(wn) - X87_Preg_Min_Offset);
		}
#endif
	    }
	    else { 
	    	/* reference to a non-dedicated preg */
		if ((WN_offset(wn) - Last_Dedicated_Preg_Offset) 
			< PREG_Table_Size (CURRENT_SYMTAB) )
	    	    fprintf(ir_ofile, " # %s", Preg_Name(WN_offset(wn)));
		else
		    fprintf(ir_ofile, " # <Invalid PREG Table index (%d)>",
			    WN_offset(wn));
	    }
	}

    if (opcode == OPC_XPRAGMA) {
	fprintf(ir_ofile, " # %s", WN_pragmas[WN_pragma(wn)].name);
    }

    if (OPCODE_operator(opcode) == OPR_ASM_INPUT) {
      fprintf(ir_ofile, " # \"%s\"", WN_asm_input_constraint(wn));
    }

    if (opcode == OPC_PRAGMA) {
	fprintf(ir_ofile, " # %s", WN_pragmas[WN_pragma(wn)].name);
	//Yan add here
	if(strcmp(WN_pragmas[WN_pragma(wn)].name,"WOPT_FINISHED_OPTIMIZATION") == 0){
	 O3 = true;
	}
	
	
	switch(WN_pragma(wn)) {
	case WN_PRAGMA_DISTRIBUTE:
	case WN_PRAGMA_REDISTRIBUTE:
	case WN_PRAGMA_DISTRIBUTE_RESHAPE:
	    fprintf(ir_ofile, ", %d",WN_pragma_index(wn));
	    switch(WN_pragma_distr_type(wn)) {
	    case DISTRIBUTE_STAR:
		fprintf(ir_ofile, ", *");
		break;
	    case DISTRIBUTE_BLOCK:
		fprintf(ir_ofile, ", BLOCK");
		break;
	    case DISTRIBUTE_CYCLIC_CONST:
		fprintf(ir_ofile, ", CYCLIC(%d)", WN_pragma_preg(wn));
		break;
	    case DISTRIBUTE_CYCLIC_EXPR:
		fprintf(ir_ofile, ", CYCLIC(expr)");
		break;
	    }
	    break;
	case WN_PRAGMA_ASM_CONSTRAINT:
	  fprintf(ir_ofile, ", \"%s\", opnd:%d preg:%d",
		  WN_pragma_asm_constraint(wn),
		  WN_pragma_asm_opnd_num(wn),
		  WN_pragma_asm_copyout_preg(wn));
	  break;
	default:
            if (WN_pragma_arg2(wn) != 0 )
		fprintf(ir_ofile, ", %d, %d", WN_pragma_arg1(wn), WN_pragma_arg2(wn));
            else
		if (WN_pragma_arg1(wn) != 0 )
		    fprintf(ir_ofile, ", %d", WN_pragma_arg1(wn));
	    break;
	} /* switch */
    }

    if (OPCODE_operator(opcode) == OPR_ASM_STMT) {
      fprintf(ir_ofile, " # \"%s\"", WN_asm_string(wn));
      if (WN_Asm_Volatile(wn))
      	fprintf(ir_ofile, " (volatile)");
      if (WN_Asm_Clobbers_Mem(wn))
      	fprintf(ir_ofile, " (memory)");
      if (WN_Asm_Clobbers_Cc(wn))
      	fprintf(ir_ofile, " (cc)");
    }

    if (OPCODE_is_call(opcode))
	fprintf(ir_ofile, " # flags 0x%x", WN_call_flag(wn));

    if (OPCODE_operator(opcode) == OPR_PARM) {
	INT flag =  WN_flag(wn);
	fprintf(ir_ofile, " # ");
	if (flag & WN_PARM_BY_REFERENCE) fprintf(ir_ofile, " by_reference ");
#if defined(TARG_SL)
	if (flag & WN_PARM_DEREFERENCE) fprintf(ir_ofile, " by_dereference ");
#endif
	if (flag & WN_PARM_BY_VALUE)     fprintf(ir_ofile, " by_value ");
	if (flag & WN_PARM_OUT)          fprintf(ir_ofile, " out ");
	if (flag & WN_PARM_DUMMY)        fprintf(ir_ofile, " dummy ");
	if (flag & WN_PARM_READ_ONLY)    fprintf(ir_ofile, " read_only ");
	if (flag & WN_PARM_PASSED_NOT_SAVED) fprintf(ir_ofile, "passed_not_saved ");
	if (flag & WN_PARM_NOT_EXPOSED_USE) fprintf(ir_ofile, " not_euse ");
	if (flag & WN_PARM_IS_KILLED) fprintf(ir_ofile, " killed ");
    }

    if (IR_dump_map_info) {
	fprintf(ir_ofile, " # <id %d:%d>", OPCODE_mapcat(opcode),
		WN_map_id(wn));
	if (ir_put_map && ( WN_map_id(wn) != -1 )) {
	    switch ( WN_MAP_Get_Kind( ir_put_map ) ) {
	    case WN_MAP_KIND_VOIDP:
		fprintf(ir_ofile, " <map %8p>", WN_MAP_Get( ir_put_map, wn ));
		break;
	    case WN_MAP_KIND_INT32:
		fprintf(ir_ofile, " <map %08x>", WN_MAP32_Get( ir_put_map, wn ));
		break;
	    case WN_MAP_KIND_INT64:
		fprintf(ir_ofile, " <map %08llx>", WN_MAP64_Get( ir_put_map, wn ));
		break;
	    }
	}
#ifdef BACK_END
	if (UINT16 vertex = LNOGetVertex(wn)) {
	    fprintf(ir_ofile, " <lno vertex %d>", vertex);
	}
#endif
    }

    if (IR_dump_line_numbers &&
        (OPCODE_is_scf(WN_opcode(wn)) || OPCODE_is_stmt(WN_opcode(wn)))) {

	USRCPOS srcpos;
	USRCPOS_srcpos(srcpos) = WN_Get_Linenum(wn);

	fprintf(ir_ofile, " {line: %d/%d}", USRCPOS_filenum(srcpos), USRCPOS_linenum(srcpos));
    }

#ifdef BACK_END
    if (IR_dump_alias_info(WN_opcode(wn))) {
      fprintf(ir_ofile, " [alias_id: %d%s]", WN_MAP32_Get(IR_alias_map, wn),
	      IR_alias_mgr && IR_alias_mgr->Safe_to_speculate(wn) ? ",fixed" : "");
    }
#endif

    if (IR_freq_map != WN_MAP_UNDEFINED && (OPCODE_is_scf(WN_opcode(wn)) ||
					    OPCODE_is_stmt(WN_opcode(wn)))) {
	USRCPOS srcpos;
	USRCPOS_srcpos(srcpos) = WN_Get_Linenum(wn);

	fprintf(ir_ofile, " {freq: %d, ln: %d, col: %d}",
		WN_MAP32_Get(IR_freq_map, wn),
		USRCPOS_linenum(srcpos),
		USRCPOS_column(srcpos));
    }
    if (Current_Map_Tab != NULL 
    	&& WN_MAP32_Get(WN_MAP_ALIAS_CLASS, wn) != 0) 
    {
      fprintf(ir_ofile, " {class %d}",
	      WN_MAP32_Get(WN_MAP_ALIAS_CLASS, wn));
    }

    if (Current_Map_Tab != NULL &&
        WN_MAP32_Get(WN_MAP_ALIAS_CGNODE, wn) != 0) 
    {
      if (OPERATOR_is_call(WN_operator(wn)))
        fprintf(ir_ofile, " {callsite %d}",
                WN_MAP32_Get(WN_MAP_ALIAS_CGNODE, wn));
      else
        fprintf(ir_ofile, " {cgnode %d}",
                WN_MAP32_Get(WN_MAP_ALIAS_CGNODE, wn));
    }

#ifdef BACK_END
    AliasAnalyzer *aa = AliasAnalyzer::aliasAnalyzer();
    if (Current_Map_Tab != NULL && aa != NULL && aa->getAliasTag(wn) != 0)
      fprintf(ir_ofile," {alias_tag %d}", aa->getAliasTag(wn));
#endif

    fprintf(ir_ofile, "\n");
#if defined(BACK_END) || defined(IR_TOOLS)
    if (OPT_Enable_WHIRL_SSA && WSSA::WN_has_chi(wn)) {
        ir_put_chi_list(wn, indent);
    }
#endif
}


/*
 *  Write an WN * in ascii form on an individual line.
 */ 
static void ir_put_wn(WN * wn, INT indent)
{
    OPCODE opcode;

    if (wn == NULL) {
    	/* null statement */
    	fprintf(ir_ofile, "### error: null WN pointer\n");
   	return;
    } else {
	if (IR_dump_wn_id) {
#ifdef WHIRL_USE_UNIQUE_ID_FOR_DEBUG
	    fprintf(ir_ofile, "[%6u] ", WN_id(wn));
#endif
	}
	if (IR_dump_wn_addr) {
	    fprintf(ir_ofile, "%8p: ", wn);
	}
	opcode = WN_opcode(wn);
    }
    if (opcode == 0) {
	fprintf(ir_ofile,"### error: WN opcode 0\n");
	return;
    }
    /*
     *  for dependency dumping, dump a handle to refer to later
     */
    if (IR_DUMPDEP_info) {
	INT32 handle=  0;
	if (OPCODE_has_alias_info(WN_opcode(wn)) && WN_map_id(wn) != -1) {
	    handle= AddToDUMPDEP(wn);
	}
	fprintf(ir_ofile, "[%5d]", handle);
    }

#if defined(BACK_END) || defined(IR_TOOLS)
    if (OPT_Enable_WHIRL_SSA && WSSA::WN_has_mu(wn)) {
        ir_put_mu_list(wn, indent);
    }
#endif

    if (indent > 0 && opcode == OPC_LABEL)
	fprintf(ir_ofile, "%*s", indent-1, "");
    else
	fprintf(ir_ofile, "%*s", indent, "");

    fprintf(ir_ofile, "%s", OPCODE_name(opcode) + strlen("OPC_"));
    if (OPCODE_has_offset(opcode)) {
	if (OPCODE_operator(opcode) == OPR_PRAGMA || 
	    OPCODE_operator(opcode) == OPR_XPRAGMA)
	    fprintf(ir_ofile, " %d %d", WN_pragma_flags(wn), WN_pragma(wn));
	else
	    fprintf(ir_ofile, " %d", WN_offset(wn));
    } else if (OPCODE_has_2offsets(opcode)) {
	fprintf(ir_ofile, " %d %d",
		WN_loop_trip_est(wn), WN_loop_depth(wn));
    }

    switch (OPCODE_operator(opcode)) {

    case OPR_INTRINSIC_OP:
    case OPR_ARRAYEXP:
#ifdef KEY
    case OPR_PURE_CALL_OP:
#endif
      fprintf(ir_ofile, " %d", WN_kid_count(wn));
      break;

    case OPR_REGION:
      fprintf(ir_ofile, " %d", WN_region_id(wn));
#ifdef BACK_END
      {
	RID *rid = REGION_get_rid(wn);
	if (rid != NULL)
	  fprintf(ir_ofile, " %d", RID_id(rid));
      }
#endif /* BACK_END */
      fprintf(ir_ofile, " (kind=%d)",WN_region_kind(wn));
      break;
#if defined(TARG_SL)
    case OPR_LDA:
    case OPR_ISTORE:
      fprintf(ir_ofile, "im:%d", WN_is_internal_mem_ofst(wn)); 
      break; 
#endif
    case OPR_LDBITS:
    case OPR_ILDBITS:
    case OPR_STBITS:
    case OPR_ISTBITS:
    case OPR_EXTRACT_BITS:
    case OPR_COMPOSE_BITS:
      fprintf(ir_ofile, " <bofst:%d bsize:%d>", WN_bit_offset(wn),
	      WN_bit_size(wn));
      break;

    case OPR_ASM_INPUT:
      fprintf(ir_ofile, " opnd:%d", WN_asm_opnd_num(wn));
      break;

    default:
      break;
    }

    if (OPCODE_has_inumber(opcode)) {
	switch (opcode) {
	case OPC_IO:
	    fprintf(ir_ofile, " <%d,%s,%s>", WN_intrinsic(wn),
		    IOSTATEMENT_name((IOSTATEMENT) WN_intrinsic(wn)),
		    get_iolibrary_name(WN_IO_Library(wn)));
	    break;
	case OPC_IO_ITEM:
	    fprintf(ir_ofile, " <%d,%s>", WN_intrinsic(wn),
		    IOITEM_name((IOITEM) WN_intrinsic(wn)));
	    break;
	default:		/* intrinsic */
	    Is_True(OPCODE_operator(opcode) == OPR_INTRINSIC_OP ||
		    OPCODE_operator(opcode) == OPR_INTRINSIC_CALL,
		    ("ir_put_wn, expected an intrinsic"));
#if defined(BACK_END) || defined(IR_TOOLS)
	    fprintf(ir_ofile, " <%d,%s>", WN_intrinsic(wn),
		    INTRINSIC_name((INTRINSIC) WN_intrinsic(wn)));
#endif
	    break;
	}
    }

    if (OPCODE_has_bits(opcode))
	fprintf(ir_ofile, " %d", WN_cvtl_bits(wn));
    if (OPCODE_has_label(opcode))
	fprintf(ir_ofile, " L%d", WN_label_number(wn));
    if (OPCODE_has_flags(opcode)) 
	fprintf(ir_ofile, " %d", WN_flag(wn));
    if (OPCODE_has_sym(opcode)) {
        ir_put_st (WN_st_idx(wn));
#if defined(BACK_END) || defined(IR_TOOLS)
        if (OPT_Enable_WHIRL_SSA) {
            WSSA::WHIRL_SSA_MANAGER * wsm = PU_Info_ssa_ptr(Current_PU_Info);
            Is_True( wsm != NULL, ("WHIRL SSA MANAGER is NULL") );
            if (wsm->Stat() == WSSA::STAT_DUMP) {
                if (wsm->WN_has_ver(wn)) {
                    WSSA::VER_IDX ver = wsm->Get_wn_ver(wn);
                    fprintf(ir_ofile, " ");
                    Print_ver(ir_ofile, ver);
                }
                else {
                    fprintf(ir_ofile, " INV_VER");
                }
            }
        }
#endif
    }

    if (OPCODE_has_1ty(opcode)) {
	if (WN_ty(wn) != (TY_IDX) 0) 
	   ir_put_ty(WN_ty(wn));
	else
	    if (opcode != OPC_IO_ITEM)
		fprintf(ir_ofile, " T<### ERROR: null ptr>");
    } else if (OPCODE_has_2ty(opcode)) {
	if (WN_ty(wn) != (TY_IDX) 0) 
	   ir_put_ty(WN_ty(wn));
	else
	    fprintf(ir_ofile, " T<### ERROR: null ptr>");
	if (WN_load_addr_ty(wn) != (TY_IDX) 0) 
	   ir_put_ty(WN_load_addr_ty(wn));
	else
	    fprintf(ir_ofile, " T<### ERROR: null ptr>");
    }

    if (OPCODE_has_ndim(opcode))
	fprintf(ir_ofile, " %d", WN_num_dim(wn));
    if (OPCODE_has_esize(opcode))
	fprintf(ir_ofile, " %" LL_FORMAT "d", WN_element_size(wn));

    if (OPCODE_has_num_entries(opcode))
	fprintf(ir_ofile, " %d", WN_num_entries(wn));
    if (OPCODE_has_last_label(opcode))
	fprintf(ir_ofile, " %d", WN_last_label(wn));

    if (OPCODE_has_value(opcode)) {
	fprintf(ir_ofile, " %" LL_FORMAT "d", WN_const_val(wn));
	/* Also print the hex value for INTCONSTs */
	if (OPCODE_operator(opcode) == OPR_INTCONST || opcode == OPC_PRAGMA) {
	    fprintf(ir_ofile, " (0x%" LL_FORMAT "x)", WN_const_val(wn));
	}
    }

    if (OPCODE_has_field_id(opcode) && WN_field_id(wn)) {
	fprintf(ir_ofile, " <field_id:%u>", WN_field_id(wn));
    }
    

    if (OPCODE_has_ereg_supp(opcode)) {
	INITO_IDX ino = WN_ereg_supp(wn);
	if (ino != 0)
	    fprintf (ir_ofile, " INITO<%d,%s>", INITO_IDX_index (ino), 
		     ST_name (INITO_st_idx (Inito_Table[ino])));
    }
    if (opcode == OPC_COMMENT) {
	fprintf(ir_ofile, " # %s", Index_To_Str(WN_offset(wn)));
    }

    if (follow_st && OPCODE_has_sym(opcode) && OPCODE_has_offset(opcode)
	&& WN_st_idx(wn) != (ST_IDX) 0 && (ST_class(WN_st(wn)) == CLASS_PREG)
        && opcode != OPC_PRAGMA)
	{
	    if (Preg_Is_Dedicated(WN_offset(wn))) {
		if (Preg_Offset_Is_Int(WN_offset(wn))) {
	    		fprintf(ir_ofile, " # $r%d", WN_offset(wn));
		}
		else if (Preg_Offset_Is_Float(WN_offset(wn))) {
	    		fprintf(ir_ofile, " # $f%d", 
				WN_offset(wn) - Float_Preg_Min_Offset);
		}
#ifdef TARG_X8664
		else if (Preg_Offset_Is_X87(WN_offset(wn))) {
	    		fprintf(ir_ofile, " # $st%d", 
				WN_offset(wn) - X87_Preg_Min_Offset);
		}
#endif
	    }
	    else { 
	    	/* reference to a non-dedicated preg */
		if ((WN_offset(wn) - Last_Dedicated_Preg_Offset) 
			< PREG_Table_Size (CURRENT_SYMTAB) )
	    	    fprintf(ir_ofile, " # %s", Preg_Name(WN_offset(wn)));
		else
		    fprintf(ir_ofile, " # <Invalid PREG Table index (%d)>",
			    WN_offset(wn));
	    }
	}

    if (opcode == OPC_XPRAGMA) {
	fprintf(ir_ofile, " # %s", WN_pragmas[WN_pragma(wn)].name);
    }

    if (OPCODE_operator(opcode) == OPR_ASM_INPUT) {
      fprintf(ir_ofile, " # \"%s\"", WN_asm_input_constraint(wn));
    }

    if (opcode == OPC_PRAGMA) {
	fprintf(ir_ofile, " # %s", WN_pragmas[WN_pragma(wn)].name);
	switch(WN_pragma(wn)) {
	case WN_PRAGMA_DISTRIBUTE:
	case WN_PRAGMA_REDISTRIBUTE:
	case WN_PRAGMA_DISTRIBUTE_RESHAPE:
	    fprintf(ir_ofile, ", %d",WN_pragma_index(wn));
	    switch(WN_pragma_distr_type(wn)) {
	    case DISTRIBUTE_STAR:
		fprintf(ir_ofile, ", *");
		break;
	    case DISTRIBUTE_BLOCK:
		fprintf(ir_ofile, ", BLOCK");
		break;
	    case DISTRIBUTE_CYCLIC_CONST:
		fprintf(ir_ofile, ", CYCLIC(%d)", WN_pragma_preg(wn));
		break;
	    case DISTRIBUTE_CYCLIC_EXPR:
		fprintf(ir_ofile, ", CYCLIC(expr)");
		break;
	    }
	    break;
	case WN_PRAGMA_ASM_CONSTRAINT:
	  fprintf(ir_ofile, ", \"%s\", opnd:%d preg:%d",
		  WN_pragma_asm_constraint(wn),
		  WN_pragma_asm_opnd_num(wn),
		  WN_pragma_asm_copyout_preg(wn));
	  break;
	default:
            if (WN_pragma_arg2(wn) != 0 )
		fprintf(ir_ofile, ", %d, %d", WN_pragma_arg1(wn), WN_pragma_arg2(wn));
            else
		if (WN_pragma_arg1(wn) != 0 )
		    fprintf(ir_ofile, ", %d", WN_pragma_arg1(wn));
	    break;
	} /* switch */
    }

    if (OPCODE_operator(opcode) == OPR_ASM_STMT) {
      fprintf(ir_ofile, " # \"%s\"", WN_asm_string(wn));
      if (WN_Asm_Volatile(wn))
      	fprintf(ir_ofile, " (volatile)");
      if (WN_Asm_Clobbers_Mem(wn))
      	fprintf(ir_ofile, " (memory)");
      if (WN_Asm_Clobbers_Cc(wn))
      	fprintf(ir_ofile, " (cc)");
    }

    if (OPCODE_is_call(opcode))
	fprintf(ir_ofile, " # flags 0x%x", WN_call_flag(wn));

    if (OPCODE_operator(opcode) == OPR_PARM) {
	INT flag =  WN_flag(wn);
	fprintf(ir_ofile, " # ");
	if (flag & WN_PARM_BY_REFERENCE) fprintf(ir_ofile, " by_reference ");
#if defined(TARG_SL)
	if (flag & WN_PARM_DEREFERENCE) fprintf(ir_ofile, " by_dereference ");
#endif
	if (flag & WN_PARM_BY_VALUE)     fprintf(ir_ofile, " by_value ");
	if (flag & WN_PARM_OUT)          fprintf(ir_ofile, " out ");
	if (flag & WN_PARM_DUMMY)        fprintf(ir_ofile, " dummy ");
	if (flag & WN_PARM_READ_ONLY)    fprintf(ir_ofile, " read_only ");
	if (flag & WN_PARM_PASSED_NOT_SAVED) fprintf(ir_ofile, "passed_not_saved ");
	if (flag & WN_PARM_NOT_EXPOSED_USE) fprintf(ir_ofile, " not_euse ");
	if (flag & WN_PARM_IS_KILLED) fprintf(ir_ofile, " killed ");
    }

    if (IR_dump_map_info) {
	fprintf(ir_ofile, " # <id %d:%d>", OPCODE_mapcat(opcode),
		WN_map_id(wn));
	if (ir_put_map && ( WN_map_id(wn) != -1 )) {
	    switch ( WN_MAP_Get_Kind( ir_put_map ) ) {
	    case WN_MAP_KIND_VOIDP:
		fprintf(ir_ofile, " <map %8p>", WN_MAP_Get( ir_put_map, wn ));
		break;
	    case WN_MAP_KIND_INT32:
		fprintf(ir_ofile, " <map %08x>", WN_MAP32_Get( ir_put_map, wn ));
		break;
	    case WN_MAP_KIND_INT64:
		fprintf(ir_ofile, " <map %08llx>", WN_MAP64_Get( ir_put_map, wn ));
		break;
	    }
	}
#ifdef BACK_END
	if (UINT16 vertex = LNOGetVertex(wn)) {
	    fprintf(ir_ofile, " <lno vertex %d>", vertex);
	}
#endif
    }

    if (IR_dump_line_numbers &&
        (OPCODE_is_scf(WN_opcode(wn)) || OPCODE_is_stmt(WN_opcode(wn)))) {

	USRCPOS srcpos;
	USRCPOS_srcpos(srcpos) = WN_Get_Linenum(wn);

	fprintf(ir_ofile, " {line: %d/%d}", USRCPOS_filenum(srcpos), USRCPOS_linenum(srcpos));
    }

#ifdef BACK_END
    if (IR_dump_alias_info(WN_opcode(wn))) {
      fprintf(ir_ofile, " [alias_id: %d%s]", WN_MAP32_Get(IR_alias_map, wn),
	      IR_alias_mgr && IR_alias_mgr->Safe_to_speculate(wn) ? ",fixed" : "");
    }
#endif

    if (IR_freq_map != WN_MAP_UNDEFINED && (OPCODE_is_scf(WN_opcode(wn)) ||
					    OPCODE_is_stmt(WN_opcode(wn)))) {
	USRCPOS srcpos;
	USRCPOS_srcpos(srcpos) = WN_Get_Linenum(wn);

	fprintf(ir_ofile, " {freq: %d, ln: %d, col: %d}",
		WN_MAP32_Get(IR_freq_map, wn),
		USRCPOS_linenum(srcpos),
		USRCPOS_column(srcpos));
    }
    if (Current_Map_Tab != NULL 
    	&& WN_MAP32_Get(WN_MAP_ALIAS_CLASS, wn) != 0) 
    {
      fprintf(ir_ofile, " {class %d}",
	      WN_MAP32_Get(WN_MAP_ALIAS_CLASS, wn));
    }

    if (Current_Map_Tab != NULL &&
        WN_MAP32_Get(WN_MAP_ALIAS_CGNODE, wn) != 0) 
    {
      if (OPERATOR_is_call(WN_operator(wn)))
        fprintf(ir_ofile, " {callsite %d}",
                WN_MAP32_Get(WN_MAP_ALIAS_CGNODE, wn));
      else
        fprintf(ir_ofile, " {cgnode %d}",
                WN_MAP32_Get(WN_MAP_ALIAS_CGNODE, wn));
    }

#ifdef BACK_END
    AliasAnalyzer *aa = AliasAnalyzer::aliasAnalyzer();
    if (Current_Map_Tab != NULL && aa != NULL && aa->getAliasTag(wn) != 0)
      fprintf(ir_ofile," {alias_tag %d}", aa->getAliasTag(wn));
#endif

    fprintf(ir_ofile, "\n");
#if defined(BACK_END) || defined(IR_TOOLS)
    if (OPT_Enable_WHIRL_SSA && WSSA::WN_has_chi(wn)) {
        ir_put_chi_list(wn, indent);
    }
#endif
}



/*
 *  Write an expression and its children in postfix order.
 *  If dump_parent_before_children, write it in prefix order instead.
 */
static Value* ir_put_expr_llvm(WN * wn, INT indent, BasicBlock **labelEntry)
{
  INT i;
  WN * wn2 = nullptr;
  Value * val = nullptr;
  
  assert(wn!=nullptr &&"wn is null in ir_put_expr_llvm function");
   
  switch (WN_operator(wn)){
		  
		  
	  case OPR_REPLICATE:{
		  
		  val = createREPLICATE_llvm(wn,labelEntry);
		  assert(val !=nullptr &&"val is nullptr");
		  return val;
		  
	  }	
	  case OPR_MAX:{
		  
		  val = createMAX_llvm(wn, labelEntry);
		  assert(val !=nullptr &&"val is nullptr");
		  return val;
		  
	  }	  	 
	  case OPR_CSELECT:{
		  
		  val = createCSELECT_llvm(wn, labelEntry);
		  assert(val !=nullptr &&"val is nullptr");
		  return val;
		  
	  }	  	 
	
	  case OPR_TRUNC:{
		  
		  val = createTRUNC_llvm(wn, labelEntry);
		  
		  assert(val !=nullptr &&"val is nullptr");
		  return val; 
		  
	  }	  	  
		  
	  case OPR_CVTL:{
		  
		  val = createCVTL_llvm(wn, labelEntry);
		  
		  assert(val !=nullptr &&"val is nullptr");
		  return val; 
		  
	  }	  	  
	  case OPR_CVT:{
		  
		  val = createCVT_llvm(wn,labelEntry);
		  assert(val !=nullptr &&"val is nullptr");
		  return val; 
		  
	  }	  
      case OPR_LDA:{
		  val = createLDA_llvm(wn,*labelEntry);
		  assert(val !=nullptr &&"val is nullptr");
		  return val; 
		  
	  }
	  case OPR_BIOR:{
		  val = createBOR_llvm(wn,*labelEntry);
		  assert(val !=nullptr &&"val is nullptr");
		  return val;
	  }	 
	  case OPR_CIOR:{
		  // This function will cause very big modification, 
		  // So by now, I don't implement it. 
		  val = createCIOR_llvm(wn, labelEntry);
		  assert(val !=nullptr &&"val is nullptr");
		  return val;
	  }
	  case OPR_BAND:{
		  val = createBAND_llvm(wn,*labelEntry);
		  assert(val !=nullptr &&" val is nullptr");
		  return val;
	  }  
	  case OPR_CAND:{
		  val = createCAND_llvm(wn,labelEntry);
		  assert(val !=nullptr &&" val is nullptr");
		  return val;
	  }
	  case OPR_NEG:{
		  
		  WN* wnkid0 = WN_kid(wn,0);
		  Value *val1 = ir_put_expr_llvm(wnkid0,indent+1, labelEntry);
		  ConstantFP* const_double_12 = ConstantFP::get(w2lMod->getContext(), 
										APFloat(-0.000000e+00));

		  BinaryOperator* double_sub = BinaryOperator::Create(Instruction::FSub, 
									   const_double_12, val1 , "sub", *labelEntry);
		  return double_sub;
		  
	  }
		  
	  case OPR_NE:
	  case OPR_EQ:  // createICMP_llvm deal with below three conditions.
	  case OPR_GT:
	  case OPR_GE:
	  case OPR_LE:
	  case OPR_LT:{
		
		 val =  createICMP_llvm(wn, indent, labelEntry);
		 assert(val !=nullptr &&"val is nullptr");
		 return val;  
		  
	  }
		  
	 // case OPR_REDUCE_ADD:{
		   
		 // return createReduce_ADD_llvm(wn, indent, labelEntry);
		  
	  //}	  
		  
      /*case OPR_GT:{
		  WN* wnkid0 = WN_kid(wn,0);
		  WN* wnkid1 = WN_kid(wn,1);
		  Value *val1 = ir_put_expr_llvm(wnkid0,indent+1,labelEntry);
		  Value *val2 = ir_put_expr_llvm(wnkid1,indent+1,labelEntry);
		  ICmpInst* int_cmp = new ICmpInst(*labelEntry, ICmpInst::ICMP_SGT, val1,val2, "cmp");
		  return int_cmp; 
	  }*/
	
	  case OPR_CONST:{
		  
		  /*F4STID 56 <1,56,s> T<54,student,4> <field_id:3> {line: 1/55}
		      F4CONST <1,55,____2.000000> */
		  
		  val = createConst_llvm(wn);
		  assert(val !=nullptr &&"val is nullptr");
		  return val;  
	  }
		  
	  case OPR_INTCONST:{
		  
		  val = createINTConst_llvm(wn);
		  assert(val !=nullptr &&"val is nullptr");
		  return val;  
		  
	  }		  
	  case OPR_LDID:{
		  
		  OPCODE opcode;
		  opcode = WN_opcode(wn);
		  
		  TYPE_ID  type = OPCODE_rtype(opcode);
		  
		  assert(opcode!=0 &&"opcode is 0 in createStore");
		  
		  if (OPCODE_has_sym(opcode) && OPCODE_has_offset(opcode)
				  && WN_st_idx(wn) != (ST_IDX) 0 && 
				  (ST_class(WN_st(wn)) == CLASS_PREG)&& 
				  opcode != OPC_PRAGMA ) {
			  
			  int offset = WN_offset(wn);
			  // this is to deal with return statement 
			  // I4I4LDID -1 <1,49,.preg_return_val> T<4,.predef_I4,4>
			  // Usually, this statement appear after a CALL statement 
			  //An example can be seen in OPR_COMMA in ir_put_expr_llvm function.
			  if(offset == -1){
				  return w2lJustCall.second;
			  }
			  if(offset ==1 && O3== true){
				  
				  IntegerType* intType = IntegerType::get(w2lMod->getContext(), 64);			  
				  
				  Value* val1 = w2lJustCall.second;
				  if(val1->getType()->isPointerTy() && O3 && type == MTYPE_U8){
					  val1 = new PtrToIntInst(val1, intType,  "", *labelEntry);
				  } 
				  return val1; 
			  }


			  
			  if( !Preg_Is_Dedicated(WN_offset(wn))) {
			          return createPregLoad_llvm(wn, labelEntry);
			  }
			  else if (offset>=5){
				  if(offset ==17){
					  return g_FunReturn;
				  }
				  
				  Function::arg_iterator args = currentFun->arg_begin();
				  for(int i = 0;i<30;i++){
					  Value * v = args;
					  args++;
					  if(i == (offset-5) )
					     return v;
				  }
			  }
			  assert(!"you should not be here");
		  }
		  else{
			  
			  ST_IDX st_idx = WN_st_idx(wn);
			  assert(st_idx != (ST_IDX) 0 && "st_idx error ");
			  
			  const ST* wn_st = &St_Table[st_idx];
			  char* char_st_name = ST_name (wn_st);
			  
			  //string str(char_st_name);
			  string str = buildSymName_llvm(wn_st);
			  
			  
			  
			  if(ST_level(wn_st) == 1 ){
				  assert(w2lGlobalValueMap.find(str) != w2lGlobalValueMap.end() && 
						 "error in createStore_llvm->w2lAllocMap\n");	
				  Value *ptrA = w2lGlobalValueMap[str];	
				  return createLoad_llvm(wn, ptrA, *labelEntry);
			  }
			  else if(ST_level(wn_st) == 2) {
			  
				  assert((w2lAllocMap.find(str) != w2lAllocMap.end()) &&
						 "error in createLoad_llvm->w2lAllocMap\n ");
				  AllocaInst *alloc = w2lAllocMap[str];
				  return createLoad_llvm(wn, alloc, *labelEntry);
			  }
			  else{
				  assert(false);
			  }
		  }
	  }
		
	  case OPR_MPY:{ //Yan put them in the block, or it will produce cross initiailzation error
		  
		  val = createMpy_llvm(wn, labelEntry);
		  //createMPY_llvm(wn,labelEntry);
		  assert(val !=nullptr &&"val is nullptr");
		  return val; 
		  
		  
	  }
		  
	  case OPR_DIV:{ 
		  
		  val = createDIV_llvm(wn, labelEntry);
		  //createMPY_llvm(wn,labelEntry);
		  assert(val !=nullptr &&"val is nullptr");
		  return val; 
	  }
	  
	  case OPR_DIVREM:{ 
		  
		  val = createDIVREM_llvm(wn, labelEntry);
		  assert(val !=nullptr &&"val is nullptr");
		  return val; 
	  }	  
	  case OPR_HIGHPART:{ 
		  
		  //OPCODE opcode = WN_opcode(wn);
		  //TYPE_ID tyid =  OPCODE_rtype (opcode);
		  
		  
		  WN* wnkid = WN_kid(wn,0);
		  
		  Value * valtemp = ir_put_expr_llvm(wnkid, indent+1,labelEntry);
		  return valtemp; 
	  }	  
	  case OPR_LOWPART:{ 
		  OPCODE opcode = WN_opcode(wn);
		  TYPE_ID tyid =  OPCODE_rtype (opcode);
		    
		  AllocaInst *allocPtr = nullptr;
		  
		  WN* wnkid = WN_kid(wn,0);
		  
		  string strName = "lowPart";
		  string typeID = to_string(tyid);
		  strName = strName+"_"+typeID;
		  strName = strName+"_"+to_string(WN_offset(wnkid));
		  
		  assert(w2lAllocMap.find(strName) != w2lAllocMap.end());
		  
		  LoadInst *ld = new LoadInst(w2lAllocMap[strName], "", false, *labelEntry);
		  
		  return ld;
	  }	  
		  
	  case OPR_REM:{ //Yan put them in the block, or it will produce cross initiailzation error
		  
		  val = createREM_llvm(wn, labelEntry);
		  //createMPY_llvm(wn,labelEntry);
		  assert(val !=nullptr &&"val is nullptr");
		  return val; 
		  
		  
	  }	  
	  case OPR_ADD:{ //Yan put them in the block, or it will produce cross initiailzation error
		  vector<llvm::Value *> vectVal;
		  int numVal = 0;
		  
		  
		  WN* wnkid = WN_kid(wn,0);
		  TYPE_ID  rtype = OPCODE_rtype(WN_opcode(wn));
		  

		  for( int i = 0;i<2;i++){
			  WN* wnkid = WN_kid(wn,i);
			  assert(OPCODE_is_expression( WN_opcode(wnkid)) && 
					 "there is error in in ir_put_expr_llvm OPR_ADD");
			  
			  /* U8ADD
                   U8U8LDID 60 <1,9,.preg_U8> T<9,.predef_U8,8> # <preg>
                   U4INTCONST 16 (0x10) */
			   /*if(i == 0){
			     OPCODE op = WN_opcode(wnkid);
				 TYPE_ID rtype = OPCODE_rtype(op);
				 if(rtype == MTYPE_U8) IsLongInt = true;
			  } */
			  	
			  Value * valtemp = ir_put_expr_llvm(wnkid, indent+1,labelEntry);
			  TYPE_ID  child_type = OPCODE_rtype(WN_opcode(wnkid));
			  if(rtype != child_type){
				  valtemp = convert_llvm( rtype,child_type, valtemp, *labelEntry);
			  }
			  /* if(i == 0 ){				  
				  OPCODE opcode = WN_opcode(wn);
				  TYPE_ID  rtype = (TYPE_ID)  ((((UINT32) opcode) >> 8) & 0x3F);
				  if(rtype == MTYPE_I8){
					  IsLongInt = true; 
				  }
			  } */
			  
			  vectVal.push_back(valtemp);
			  numVal++;
		  }
		  return createAdd_llvm(wn,vectVal,numVal, labelEntry);
	  }
	  case OPR_SUB:{ //Yan put them in the block, or it will produce cross initiailzation error
		  vector<llvm::Value *> vectVal;
		  int numVal = 0;
		  
		   TYPE_ID  rtype = OPCODE_rtype(WN_opcode(wn));

		  for( int i = 0;i<2;i++){
			  WN* wnkid = WN_kid(wn,i);
			  assert(OPCODE_is_expression( WN_opcode(wnkid)) && 
					 "there is error in in ir_put_expr_llvm OPR_ADD");
			  
			  /* U8ADD
                   U8U8LDID 60 <1,9,.preg_U8> T<9,.predef_U8,8> # <preg>
                   U4INTCONST 16 (0x10) */
			 /* if(i == 0){
				  OPCODE op = WN_opcode(wnkid);
				  TYPE_ID rtype = OPCODE_rtype(op);
				  if(rtype == MTYPE_U8) IsLongInt = true;
			  }*/
			 
			  
			  Value * valtemp = ir_put_expr_llvm(wnkid, indent+1,labelEntry);
			  
			  TYPE_ID  child_type = OPCODE_rtype(WN_opcode(wnkid));
			  if(rtype != child_type){
				  valtemp = convert_llvm( rtype,child_type, valtemp, *labelEntry);
			  }
			/*  if(i == 0 ){				  
				  OPCODE opcode = WN_opcode(wn);
				  TYPE_ID  rtype = (TYPE_ID)  ((((UINT32) opcode) >> 8) & 0x3F);
				  if(rtype == MTYPE_I8){
					  IsLongInt = true; 
				  }
			  }*/
			  
			  vectVal.push_back(valtemp);
			  numVal++;
		  }
		  return createSub_llvm(wn,vectVal,numVal, *labelEntry);
	  }	  
	  case OPR_REDUCE_MAX:{
		  WN* wnkid = WN_kid(wn,0);		  
		  Value * valKid = ir_put_expr_llvm(wnkid, indent+1,labelEntry);
		  
		  ConstantInt* const_int32_0 = ConstantInt::
								get(w2lMod->getContext(), APInt(32, StringRef("0"), 10));
   ConstantInt* const_int32_1 = ConstantInt::
								get(w2lMod->getContext(), APInt(32, StringRef("1"), 10));
   ConstantInt* const_int32_2 = ConstantInt::
								get(w2lMod->getContext(), APInt(32, StringRef("2"), 10));
   ConstantInt* const_int32_3 = ConstantInt::
								get(w2lMod->getContext(), APInt(32, StringRef("3"), 10));
		  
		  ExtractElementInst* float_e1 = ExtractElementInst::Create(valKid, const_int32_0,
				  "e1", *labelEntry);
		  ExtractElementInst* float_e2 = ExtractElementInst::Create(valKid, const_int32_1,
				  "e2", *labelEntry);
		  ExtractElementInst* float_e3 = ExtractElementInst::Create(valKid, const_int32_2, 
				  "e3", *labelEntry);
		  ExtractElementInst* float_e4 = ExtractElementInst::Create(valKid, const_int32_3, 
				  "e4", *labelEntry);
		  
		  std::vector<Value*> float_e12_params;
		  float_e12_params.push_back(float_e1);
		  float_e12_params.push_back(float_e2);
		  CallInst* float_e12 = CallInst::Create(getIntrinsicMaxnum_f32_llvm(), 
												 float_e12_params, "e12", *labelEntry);
		  float_e12->setCallingConv(CallingConv::C);
		  float_e12->setTailCall(false);
		  AttributeSet float_e12_PAL;
		  float_e12->setAttributes(float_e12_PAL);
		  
		  std::vector<Value*> float_e123_params;
		  float_e123_params.push_back(float_e12);
		  float_e123_params.push_back(float_e3);
		  CallInst* float_e123 = CallInst::Create(getIntrinsicMaxnum_f32_llvm(), 
												  float_e123_params, "e123", *labelEntry);
		  float_e123->setCallingConv(CallingConv::C);
		  float_e123->setTailCall(false);
		  AttributeSet float_e123_PAL;
		  float_e123->setAttributes(float_e123_PAL);
		  
		  std::vector<Value*> float_e1234_params;
		  float_e1234_params.push_back(float_e123);
		  float_e1234_params.push_back(float_e4);
		  CallInst* float_e1234 = CallInst::Create(getIntrinsicMaxnum_f32_llvm(), 
								  float_e1234_params, "e1234", *labelEntry);
		  float_e1234->setCallingConv(CallingConv::C);
		  float_e1234->setTailCall(false);
		  AttributeSet float_e1234_PAL;
		  float_e1234->setAttributes(float_e1234_PAL);
		  
		  return float_e1234;
	  
	  }
		  
	  case OPR_REDUCE_ADD:{ 
		  
		  
		  TYPE_ID  rtype = OPCODE_rtype(WN_opcode(wn));
		  
		  if(rtype == MTYPE_I4){
		  // below program mapping below statements. 
		  //I4V16I4REDUCE_ADD
		  //  V16I4V16I4LDID 56 <1,28,.preg_V16I4> T<31,.predef_V16I4,16> # sum_1
		  
		  VectorType* VectorTy = VectorType::get(IntegerType::get(w2lMod->getContext(), 32), 4);
		  // define const_packed_1
		  UndefValue* const_packed_1 = UndefValue::get(VectorTy);
		  
		  //define const_packed_2
		  std::vector<Constant*> const_packed_1_elems;
		  ConstantInt* const_int32_2 = ConstantInt::get(w2lMod->getContext(), 
									   APInt(32, StringRef("2"), 10));
		  const_packed_1_elems.push_back(const_int32_2);
		  ConstantInt* const_int32_3 = ConstantInt::get(
								w2lMod->getContext(), APInt(32, StringRef("3"), 10));
		  const_packed_1_elems.push_back(const_int32_3);
		  UndefValue* const_int32_undef = UndefValue::get(
										IntegerType::get(w2lMod->getContext(), 32));
		  const_packed_1_elems.push_back(const_int32_undef);
		  const_packed_1_elems.push_back(const_int32_undef);
		  // It seem that if you add VectorTy, It will compile error, so delete VectorTy in
		  // ConstantVector::get function.
		  //Constant* const_packed_2 = ConstantVector::get(VectorTy, const_packed_1_elems);
          Constant* const_packed_2 = ConstantVector::get(const_packed_1_elems);

		  //define const_packed_3
		  std::vector<Constant*> const_packed_2_elems;
		  ConstantInt* const_int32_1 = ConstantInt::get(w2lMod->getContext(), 
									   APInt(32, StringRef("1"), 10));
		  const_packed_2_elems.push_back(const_int32_1);
		  const_packed_2_elems.push_back(const_int32_undef);
		  const_packed_2_elems.push_back(const_int32_undef);
		  const_packed_2_elems.push_back(const_int32_undef);
		  Constant* const_packed_3 = ConstantVector::get(const_packed_2_elems);
		  
		  
		  WN* wnkid = WN_kid(wn,0);
		  
		  Value * valtemp = ir_put_expr_llvm(wnkid, indent+1,labelEntry);
		  
		  ShuffleVectorInst* packed_rdx_shuf = new ShuffleVectorInst(valtemp, 
				  const_packed_1, const_packed_2, "rdx.shuf", *labelEntry);
		  
		  BinaryOperator* packed_bin_rdx1 = BinaryOperator::Create(Instruction::Add, 
								valtemp, packed_rdx_shuf, "bin.rdx1", *labelEntry);
		  
		  ShuffleVectorInst* packed_rdx_shuf1 = new ShuffleVectorInst(packed_bin_rdx1,
				  const_packed_1, const_packed_3, "rdx.shuf1", *labelEntry);
		  
		  BinaryOperator* packed_bin_rdx2 = BinaryOperator::Create(Instruction::Add,
						packed_bin_rdx1, packed_rdx_shuf1, "bin.rdx2", *labelEntry);
		  
		  
		  ConstantInt* const_int32_0 = ConstantInt::get(w2lMod->getContext(),
									   APInt(32, StringRef("0"), 10));

		  
		  ExtractElementInst* int32_result = ExtractElementInst::Create(packed_bin_rdx2, 
											 const_int32_0, "", *labelEntry);
		  
		  return int32_result;
		  
		  }
		  else if(rtype == MTYPE_F4){
			  VectorType* VectorTy = VectorType::get(
										 Type::getFloatTy(w2lMod->getContext()), 4);
			  WN* wnkid = WN_kid(wn,0);		  
			  Value * valKid = ir_put_expr_llvm(wnkid, indent+1,labelEntry);
			  
			  ConstantInt* const_int32_0 = ConstantInt::
								get(w2lMod->getContext(), APInt(32, StringRef("0"), 10));
			  ConstantInt* const_int32_1 = ConstantInt::
							get(w2lMod->getContext(), APInt(32, StringRef("1"), 10));
			  ConstantInt* const_int32_2 = ConstantInt::
										   get(w2lMod->getContext(), APInt(32, StringRef("2"), 10));
			  ConstantInt* const_int32_3 = ConstantInt::
										   get(w2lMod->getContext(), APInt(32, StringRef("3"), 10));
			  
			  ExtractElementInst* float_e1 = ExtractElementInst::Create(valKid, const_int32_0,
											 "e1", *labelEntry);
			  ExtractElementInst* float_e2 = ExtractElementInst::Create(valKid, const_int32_1,
											 "e2", *labelEntry);
			  ExtractElementInst* float_e3 = ExtractElementInst::Create(valKid, const_int32_2, 
											 "e3", *labelEntry);
			  ExtractElementInst* float_e4 = ExtractElementInst::Create(valKid, const_int32_3, 
											 "e4", *labelEntry);
			  
			 
			  
			  BinaryOperator *addRes = BinaryOperator::Create(
										   Instruction::FAdd, float_e1, float_e2, "add", *labelEntry);
			  
			  BinaryOperator *addRes1 = BinaryOperator::Create(
										   Instruction::FAdd, addRes, float_e3, "add", *labelEntry);
			  
			  BinaryOperator *addRes2 = BinaryOperator::Create(
											Instruction::FAdd, addRes1, float_e4, "add", *labelEntry);
			  return addRes2;

		  
		  }
		  else{
			  assert(!"missing type here");
		  }
		  	  
	  }	  
		  
		  // In optimzied IR, OPR_COMMA disappear, the value has been put in "49 __comma" from  "r1"	  
	  case OPR_COMMA:{ 
		  
		  val = createCOMMA_llvm(wn, labelEntry);
		  assert(val !=nullptr &&"val is nullptr");
		  return val;  
		 
	  }
	  case OPR_SQRT:{
		  
		  OPCODE op = WN_opcode(wn);
		  TYPE_ID rtype= OPCODE_rtype (op);
		 
		  
		  WN* wnkid = WN_kid(wn,0);
		  Value * valtemp = ir_put_expr_llvm(wnkid, indent+1,labelEntry);
		  
		  Function *func = nullptr;
		 if(rtype ==  MTYPE_F8){
			 func = w2lFunMap["sqrt"];
		 }
		 else if(rtype ==  MTYPE_F4){
			 func = w2lFunMap["sqrtf"];
		 }
		 else{
			 assert(false&&"missing type here");
		 }
		 
		 assert(func != nullptr && "SQRT error here");
		  
		  CallInst* double_call7 = CallInst::Create(func, 
								   valtemp, "call7", *labelEntry);
		  
		  assert(double_call7 !=nullptr &&"val is nullptr");
		  return double_call7;  
		  
	  }
	  case OPR_PARM:{ 
		  	  
		  OPCODE opc = WN_opcode(wn);  
		  TYPE_ID  rtype = OPCODE_rtype(opc);
		
		  TY_IDX tyidx = WN_ty(wn);
		  assert(tyidx != (TY_IDX) 0 &&" tyidx is 0 \n");
		   
		  WN* wnkid = WN_kid(wn,0);
		  OPCODE opckid = WN_opcode(wnkid);  
		  TYPE_ID  rtypekid = OPCODE_rtype(opckid);
		  
		  
		  
		  if(rtype == MTYPE_U8 && TY_kind(tyidx) == KIND_POINTER){
			 
			  WN* wnkid = WN_kid(wn,0);
			  Value * valtemp = ir_put_expr_llvm(wnkid, indent+1,labelEntry);
			  	  
			  const TY& ty = Ty_Table[tyidx];
			  //TY_IDX tyidx_pointed = ty.Pointed();
			  
			  Type* PointerTy = createLLVMType_llvm(ty);
			  
			  
			  if(valtemp->getType()->isPointerTy()){
				  CastInst* castPtr = new BitCastInst(valtemp, PointerTy, "", *labelEntry);
				  return castPtr; 
			  }
			  else if(valtemp->getType()->isIntegerTy()){
				  IntToPtrInst* ptrU8 = new IntToPtrInst(valtemp, 
														 PointerTy,  "", *labelEntry);
				  return ptrU8;
			  }
			  else{
				  assert(false);
			  }
			  
			 // PointerType* PointerTy = nullptr;
			  
			 // if(TY_mtype(tyidx_pointed) == MTYPE_I1){
			//	  PointerTy = PointerType::get(IntegerType::get(w2lMod->getContext(), 32), 0);			   
			 // }
			 // else 
			// if(TY_mtype(tyidx_pointed) == MTYPE_I4){
			//	  PointerTy = PointerType::get(IntegerType::get(w2lMod->getContext(), 32), 0);
			//	  assert(PointerTy != nullptr &&" error in ir_put_expr_llvm OPR_PARM \n");
				  
			//	  IntToPtrInst* ptrU8 = new IntToPtrInst(valtemp, 
			//											 PointerTy,  "", *labelEntry);
			//	  return ptrU8; 
			//  } 		   
			  
		  }
		  else{
			  WN* wnkid = WN_kid(wn,0);
			  Value * valtemp = ir_put_expr_llvm(wnkid, indent+1,labelEntry);
			  Value * convert = convert_llvm(rtype,rtypekid,valtemp, *labelEntry);
			  return convert;
		  }
			  
		  break;	  
	  }
		  
	  case OPR_ILOAD:{
		  	  
		  OPCODE op = WN_opcode(wn);
		  TYPE_ID rtype= OPCODE_rtype (op);
		  TYPE_ID  dtype = OPCODE_desc(op);
		  
		  
		  // get type from wn
		  assert(OPCODE_has_2ty(op) && WN_ty(wn) != (TY_IDX) 0
				 && WN_load_addr_ty(wn) != (TY_IDX) 0);
		  
		  TY_IDX tyidx = WN_ty(wn);
		  TY& ty = Ty_Table[tyidx];
		  
		  TY_IDX ty_load_idx = WN_load_addr_ty(wn);
		  TY& ty_load = Ty_Table[ty_load_idx];
		  
		  
		  WN* wnkid0 = WN_kid(wn,0);
		  Value * valtemp0 = ir_put_expr_llvm(wnkid0, indent+1, labelEntry);
		  
		  if(rtype==MTYPE_V16F4){
			  VectorType* VectorTy_3 = VectorType::get(Type::getFloatTy(
										   w2lMod->getContext()), 4);
			  
			  PointerType* PointerTy_8 = PointerType::get(VectorTy_3, 0);
			  
			  PointerType* type_pointer = (PointerType*)createLLVMType_llvm(ty_load);
			  valtemp0 = new IntToPtrInst(valtemp0, 
										  type_pointer,  "", *labelEntry);
			  
			  
			  CastInst* ptr_bcp1 = new BitCastInst(valtemp0, PointerTy_8, "bcp1", 
												   *labelEntry);
			  
			  LoadInst *ld = new LoadInst(ptr_bcp1, "", false, *labelEntry);
			  //ld->setAlignment(cast<AllocaInst>(alloc)->getAlignment());
			  ld->setAlignment(MTYPE_alignment(rtype));
			  
			  return ld;
			   
		  }
		  
		  
		  
		  if(valtemp0->getType()->isIntegerTy()){
			  PointerType* type_pointer = (PointerType*)createLLVMType_llvm(ty_load);
			  valtemp0 = new IntToPtrInst(valtemp0, 
										  type_pointer,  "", *labelEntry);
              
			  int offset= WN_offset(wn);
			  
			  if(offset >0){
				  int byte_size = MTYPE_byte_size(dtype);
				  
				  char num[256];
				  offset= offset/byte_size;
				  sprintf(num, "%ld" , offset);
				  ConstantInt* const_int64 = ConstantInt::get(w2lMod->getContext(),
											 APInt(64, num, 10));
				  Type* pointee = createLLVMType_llvm(ty);
				  
				  GetElementPtrInst* ptr_add_ptr = GetElementPtrInst::Create(pointee,
										valtemp0, const_int64, "add.ptr", *labelEntry);
				  valtemp0 = ptr_add_ptr;
			  }

			  
		  }
		  
		 
		  
		   // val = createILOAD(wn, labelEntry);
		  Value* val = nullptr;
		  val = createLoad_llvm(wn, valtemp0, *labelEntry);
		  assert(val !=nullptr &&"val is nullptr");
		  return val;
		 //break;
	  }
		  
	  case OPR_ARRAY:{		  	  
		  Value* val = nullptr;
		  val = createARRAY_llvm(wn, labelEntry);
		  assert(val !=nullptr &&"val is nullptr");
		  return val;	  
	  }
	  case OPR_IDNAME:{
		  // by now it does nothing
		  return nullptr;
	  }
	  case OPR_TAS:{
		  Value* val = nullptr;
		  val = createTAS_llvm(wn, labelEntry);
		  assert(val !=nullptr &&"val is nullptr");
		  return val;
	  }
		  	  
	  default: 
	  {
		  assert(false &&"missing a mapping in the ir_put_expr_llvm");
	  }
		  break;
  }
  

  /* See if the parent op should be dumped before or after the children */
  if (dump_parent_before_children) {
     val = ir_put_wn_llvm(wn,indent, *labelEntry);
  }
  for (i = 0; i < WN_kid_count(wn); i++) {
    wn2 = WN_kid(wn,i); 
    if (wn2) {
      OPCODE op = WN_opcode(wn2);
      if ((OPCODE_FIRST <= op && op <= OPCODE_LAST) &&
	  (OPCODE_is_expression(op) || OPCODE_is_call(op)))
	  {
	      val = ir_put_expr_llvm(WN_kid(wn,i), indent+1,labelEntry);
		  
	  }
      else
      if (    op == OPC_BLOCK
           && (    (WN_operator(wn) == OPR_RCOMMA && i == 1)
                || (WN_operator(wn) == OPR_COMMA && i == 0)))
        ir_put_stmt_llvm(wn2, indent+1,*labelEntry);
      else
        fprintf(ir_ofile, "%*sopcode %d not an expression\n", indent+1, "", op);
    } else  
      fprintf(ir_ofile, "%*snull-expression\n", indent+1, "");
  }
  
  
  
  
  
  if (!dump_parent_before_children) {
     val = ir_put_wn_llvm(wn, indent, *labelEntry);
  }
  
  return val;
}




/*
 *  Write an expression and its children in postfix order.
 *  If dump_parent_before_children, write it in prefix order instead.
 */
static void ir_put_expr(WN * wn, INT indent)
{
  INT i;
  WN * wn2;

  /* See if the parent op should be dumped before or after the children */
  if (dump_parent_before_children) {
     ir_put_wn(wn,indent);
  }
  for (i = 0; i < WN_kid_count(wn); i++) {
    wn2 = WN_kid(wn,i);
    if (wn2) {
      OPCODE op = WN_opcode(wn2);
      if ((OPCODE_FIRST <= op && op <= OPCODE_LAST) &&
	  (OPCODE_is_expression(op) || OPCODE_is_call(op)))
	      ir_put_expr(WN_kid(wn,i), indent+1);
      else
      if (    op == OPC_BLOCK
           && (    (WN_operator(wn) == OPR_RCOMMA && i == 1)
                || (WN_operator(wn) == OPR_COMMA && i == 0)))
        ir_put_stmt(wn2, indent+1);
      else
        fprintf(ir_ofile, "%*sopcode %d not an expression\n", indent+1, "", op);
    } else  
      fprintf(ir_ofile, "%*snull-expression\n", indent+1, "");
  }
  if (!dump_parent_before_children) {
     ir_put_wn(wn, indent);
  }
}


/*
 *  Write out a marker at the indentation level.
 */
static void ir_put_marker(const char *str, INT indent)
{
  if (IR_dump_wn_id) {
#ifdef WHIRL_USE_UNIQUE_ID_FOR_DEBUG
    indent += 9;
#endif
  }
  if (IR_dump_wn_addr) {
    indent += 10;
  }
  fprintf(ir_ofile, "%*s%s\n", indent, "", str);
}


/*
 *  Write a statement WN * and its children in prefix order.
 */
static void ir_put_stmt_llvm(WN * wn, INT indent, BasicBlock *&labelEntry)
{

	
  INT i;
  WN * wn2;
  USRCPOS srcpos;
  
  if (wn) {
    OPCODE opc = WN_opcode(wn);
    BOOL already_dumped_wn = FALSE;
    USRCPOS_srcpos(srcpos) = WN_Get_Linenum(wn);
    if (USRCPOS_srcpos(srcpos) != 0 && 
	USRCPOS_srcpos(srcpos) != USRCPOS_srcpos(last_srcpos)) {
      last_srcpos = srcpos;
#ifdef FRONT_END
      fprintf(ir_ofile, "%*sLOC %d %d\n", indent, "",
		USRCPOS_filenum(srcpos), USRCPOS_linenum(srcpos));
#else
      //print_source(USRCPOS_srcpos(srcpos));
#endif
    }

    if (OPCODE_is_scf(opc) || dump_parent_before_children) {
      ir_put_wn_llvm(wn, indent,labelEntry);
      already_dumped_wn = TRUE;
    }
		
	vector<Value*> vectVal;
	int numVal = 0;
	
	
	switch(OPCODE_operator(opc))
	{
		case OPR_INTRINSIC_CALL:
		case OPR_CALL:{
			//if( *(OPCODE_name(opc)+4) == 'V' ) return; // just skip VCALL 
			// you need comment it later. 	
			createCALL_llvm(wn,labelEntry);	
			return;
		
		//	Function* func_strcpy = w2lMod->getFunction("llvm.memcpy.p0i8.p0i8.i64");
		//	if (!func_strcpy) {
		//		assert(false);
			
		}
		// ISTORE also map to store statement. 	
		case OPR_ISTORE:{
			
			//OPCODE opc = WN_opcode(wn);	
		//	TYPE_ID  rtype = OPCODE_rtype(opc);
			TYPE_ID  desc  = OPCODE_desc(opc);
			
			if(desc == MTYPE_V16I4){	
				createIStore_V16I4_llvm(wn,labelEntry);
			}
			else if(desc == MTYPE_V16F4){	
				createIStore_V16F4_llvm(wn,labelEntry);
			}
			else{
				
				WN* wnkid1 = WN_kid(wn,1);
				Value * valtemp0 = ir_put_expr_llvm(wnkid1, indent+1, &labelEntry);		
				//createIStore_llvm(wn, valtemp0, labelEntry);
				createStore_llvm(wn, valtemp0, &labelEntry);
			}
			return;
			
		}
			
		case OPR_RETURN: {
			
			/*I4STID 1 <1,5,.preg_I8> T<4,.predef_I4,4> # $r1 {line: 1/0}
			   U4INTCONST 0 (0x0)
			RETURN {line: 1/0} */
			
			// this RETURN should not be mapped, because "I4STID 1" has used in OPR_STID			
			//ReturnInst::Create(w2lMod->getContext(), labelEntry);	
			
			if(previousIsReturn(labelEntry))
				return;
			
			Type* return_type = currentFun->getReturnType();
			if(return_type->getTypeID() == Type::TypeID::VoidTyID){
				ReturnInst::Create(w2lMod->getContext(), labelEntry);
			}
			
			return;	
		}	
			
		case OPR_RETURN_VAL:{
			
			createReturn_llvm(wn,labelEntry);
			return;
		}
		case OPR_STID:{
				
			OPCODE opcode = WN_opcode(wn);
			assert(opcode!=0 &&"opcode is 0 in createStore");

			
			Value * val = nullptr;
									
			if (OPCODE_has_sym(opcode) && OPCODE_has_offset(opcode) && 
					WN_st_idx(wn) != (ST_IDX) 0 && 
					(ST_class(WN_st(wn)) == CLASS_PREG) && 
					opcode != OPC_PRAGMA)
			{
				if(Preg_Is_Dedicated(WN_offset(wn))&& WN_offset(wn)==1){
					//I4STID 1 <1,5,.preg_I8> T<4,.predef_I4,4> # $r1 {line: 1/10}
					//  U4INTCONST 1 (0x1)
					// It seem that It's a return statment from main.			
					createReturn_llvm(wn,labelEntry);
					return;
					
				}else{
					
					createPregStore_llvm(wn, val, labelEntry);
				}
			
			}
			else{
				
				
				assert(OPERATOR_has_sym(WN_operator(wn)) && "STID no sym");
					
					ST_IDX st_idx = WN_st_idx(wn);
					assert(st_idx != (ST_IDX) 0 && "st_idx error ");
					
					const ST* wn_st = &St_Table[st_idx];
					// get type from symbol
					const TY& st_ty = Ty_Table[ST_type (st_idx)];
					// get st_ty name
					char* char_st_name = ST_name (wn_st);
					
					
					//string str(char_st_name);
					string str = buildSymName_llvm(wn_st); // (name_whole);
									
					if(ST_level(wn_st) == 1 ){
						assert(w2lGlobalValueMap.find(str) != w2lGlobalValueMap.end() && 
							   "error in createStore_llvm->w2lAllocMap\n");	
						Value *ptrA = w2lGlobalValueMap[str];	
						createStore_llvm(wn,ptrA, &labelEntry);
					}
					
					else if(ST_level(wn_st) == 2 ){
						assert(w2lAllocMap.find(str) != w2lAllocMap.end() && 
							   "error in createStore_llvm->w2lAllocMap\n");	
						AllocaInst *ptrA = w2lAllocMap[str];	
						createStore_llvm(wn,ptrA, &labelEntry);
					}
					else{
						assert(false);
					}
			}
			return;
		}
			
		case OPR_IF:{
			
			createIF_llvm(wn, &labelEntry);
			return ;
		}
		
		case OPR_WHILE_DO:{
			
			createWHILE_DO_llvm(wn, &labelEntry);
			return;
		}
			
		case OPR_DO_WHILE:{
			
			createDO_WHILE_llvm(wn, &labelEntry);
			return;
		}	
		case OPR_TRUEBR:{
			
			createTRUEBR_llvm(wn, &labelEntry);	
			
			return;
		}
		case OPR_FALSEBR:{
			
			createFALSEBR_llvm(wn, &labelEntry);	
			
			return;
		}	
		case OPR_LABEL:{
			
			if(O3){
				createOptLABEL_llvm(wn, &labelEntry);
				//assert(false);
			}
			else{
				createLABEL_llvm(wn, &labelEntry);
			}
			
			return;
		}
		// OPR_GOTO seems to be map to break in C language. 
		case OPR_GOTO:{
			if(O3){
				int label = WN_label_number(wn);
				string strLable = to_string(label);
				strLable = "L"+strLable;
				
				BasicBlock* label_br = nullptr;
				if(g_tfBranchMap.find(strLable) == g_tfBranchMap.end()){
					label_br = BasicBlock::Create(w2lMod->getContext(), 
											  strLable ,currentFun,0);
					g_tfBranchMap.insert(pair<string, BasicBlock*>(strLable, label_br) );
				}
				else{
					label_br = g_tfBranchMap[strLable];
				
				}
				
				BranchInst::Create( label_br, labelEntry);
				return;
			}
			else{
				int label = WN_label_number(wn);
				if(label == g_endLabel)
					BranchInst::Create(g_switchCaseMap[g_endLabel], labelEntry);
				else{
					BasicBlock* loopEnd = g_loopEndStack.top();
					BranchInst::Create(loopEnd, labelEntry);
					g_gotoFlag = true;
					//labelEntry = g_loopEnd;
				}
				return;
			}
		}
		case OPR_SWITCH: {
			
			createSWITCH_llvm(wn, &labelEntry);
			
			return;
		}
			
		default:
			break;
	}	
	

    switch (opc) {
			
	//case OPC_RETURN

    case OPC_BLOCK:
      wn2 = WN_first(wn);
      while (wn2) {
	ir_put_stmt_llvm(wn2, indent,labelEntry);
	wn2 = WN_next(wn2);
      }
      ir_put_marker("END_BLOCK", indent);
      break;

    case OPC_REGION:
      ir_put_marker("REGION EXITS", indent);
      ir_put_stmt_llvm(WN_region_exits(wn), indent+1,labelEntry);

      ir_put_marker("REGION PRAGMAS", indent);
      ir_put_stmt_llvm(WN_region_pragmas(wn), indent+1,labelEntry);

      ir_put_marker("REGION BODY", indent);
      ir_put_stmt_llvm(WN_region_body(wn), indent+1,labelEntry);

      /* check to make sure cg.so is loaded first */
      /* IR_dump_region will be NULL if it is not */
#ifdef BACK_END
      if (IR_dump_region)
	CG_Dump_Region(ir_ofile, wn);
#endif /* BACK_END */
      { char str[20];
	sprintf(str,"END_REGION %d", WN_region_id(wn));
	ir_put_marker(str, indent);
      }
      break;
      
    case OPC_LABEL:
      ir_put_wn_llvm(wn, indent,labelEntry);
      if ( WN_label_loop_info(wn) != NULL ) {
	ir_put_stmt_llvm(WN_label_loop_info(wn), indent+1,labelEntry);
      }
#if defined(BACK_END) || defined(IR_TOOLS)
      if (OPT_Enable_WHIRL_SSA)
        ir_put_phi_list(wn, indent);
#endif
      already_dumped_wn = TRUE;
      break;

    case OPC_IF:
      ir_put_expr_llvm(WN_if_test(wn), indent+1,&labelEntry);
      if (WN_then(wn)) {
	ir_put_marker("THEN", indent);
	ir_put_stmt_llvm(WN_then(wn), indent+1,labelEntry);
      }
      if (WN_else(wn)) {
	ir_put_marker("ELSE", indent);
	ir_put_stmt_llvm(WN_else(wn), indent+1,labelEntry);
      }
      ir_put_marker("END_IF", indent);
#if defined(BACK_END) || defined(IR_TOOLS)
      if (OPT_Enable_WHIRL_SSA)
        ir_put_phi_list(wn, indent);
#endif
      break;

    case OPC_DO_LOOP:
      ir_put_expr_llvm(WN_index(wn), indent+1,&labelEntry);
      ir_put_marker("INIT",indent);
      ir_put_stmt_llvm(WN_start(wn), indent+1,labelEntry);
#if defined(BACK_END) || defined(IR_TOOLS)
      if (OPT_Enable_WHIRL_SSA) {
        ir_put_phi_list(wn, indent+1);
      }
#endif
      ir_put_marker("COMP", indent);
      ir_put_expr_llvm(WN_end(wn), indent+1,&labelEntry);
      ir_put_marker("INCR", indent);
      ir_put_stmt_llvm(WN_step(wn), indent+1,labelEntry);
      /* optional loop_info */
      if ( WN_do_loop_info(wn) != NULL ) {
	ir_put_stmt_llvm(WN_do_loop_info(wn), indent,labelEntry);
      }
      ir_put_marker("BODY", indent);
      ir_put_stmt_llvm(WN_do_body(wn), indent+1,labelEntry);
      break;

    case OPC_WHILE_DO:
#if defined(BACK_END) || defined(IR_TOOLS)
      if (OPT_Enable_WHIRL_SSA) {
        ir_put_phi_list(wn, indent+1);
      }
#endif
      ir_put_marker("COMP",indent);
      ir_put_expr_llvm(WN_kid(wn, 0), indent+1,&labelEntry);
      ir_put_marker("BODY", indent);
      ir_put_stmt_llvm(WN_kid(wn, 1), indent+1,labelEntry);
      break;

    case OPC_DO_WHILE:
#if defined(BACK_END) || defined(IR_TOOLS)
      if (OPT_Enable_WHIRL_SSA) {
        ir_put_phi_list(wn, indent+1);
      }
#endif
      ir_put_marker("BODY", indent);
      ir_put_stmt_llvm(WN_kid(wn, 1), indent+1,labelEntry);
      ir_put_marker("COMP",indent);
      ir_put_expr_llvm(WN_kid(wn, 0), indent+1,&labelEntry);
      break;

    case OPC_LOOP_INFO:
      ir_put_wn_llvm(wn, indent,labelEntry);
      if ( WN_loop_induction(wn) != NULL ) {
	ir_put_expr_llvm(WN_loop_induction(wn), indent+1,&labelEntry);
      }
      if ( WN_loop_trip(wn) != NULL ) {
	ir_put_expr_llvm(WN_loop_trip(wn), indent+1,&labelEntry);
      }
      ir_put_marker("END_LOOP_INFO", indent);
      already_dumped_wn = TRUE;
      break;

    case OPC_COMPGOTO:
      ir_put_wn_llvm(wn, indent,labelEntry);
      ir_put_expr_llvm(WN_kid(wn,0), indent+1,&labelEntry);
      ir_put_stmt_llvm(WN_kid(wn,1), indent+1,labelEntry);
      if (WN_kid_count(wn) > 2)
	ir_put_stmt_llvm(WN_kid(wn,2), indent+1,labelEntry);
      ir_put_marker("END_COMPGOTO", indent);
      already_dumped_wn = TRUE;
      break;

    case OPC_SWITCH:
      ir_put_wn_llvm(wn, indent,labelEntry);
      ir_put_expr_llvm(WN_kid(wn,0), indent+1,&labelEntry);
      ir_put_stmt_llvm(WN_kid(wn,1), indent+1,labelEntry);
      if (WN_kid_count(wn) > 2)
	ir_put_stmt_llvm(WN_kid(wn,2), indent+1,labelEntry);
      ir_put_marker("END_SWITCH", indent);
#if defined(BACK_END) || defined(IR_TOOLS)
      if (OPT_Enable_WHIRL_SSA) {
        ir_put_phi_list(wn, indent+1);
      }
#endif
      already_dumped_wn = TRUE;
      break;

    case OPC_XGOTO:
      ir_put_wn_llvm(wn, indent,labelEntry);
      ir_put_expr_llvm(WN_kid(wn,0), indent+1,&labelEntry);
      ir_put_stmt_llvm(WN_kid(wn,1), indent+1,labelEntry);
      ir_put_marker("END_XGOTO", indent);
      already_dumped_wn = TRUE;
      break;

    case OPC_WHERE:
      ir_put_expr_llvm(WN_kid(wn,0), indent+1,&labelEntry);
      ir_put_marker("BODY", indent);
      ir_put_stmt_llvm(WN_kid(wn,1), indent+1,labelEntry);
      ir_put_stmt_llvm(WN_kid(wn,2), indent+1,labelEntry);
      break;

    case OPC_EXC_SCOPE_BEGIN:
      {
        INT i;
	for (i = 0; i < WN_kid_count(wn); i++)
	  ir_put_stmt_llvm(WN_kid(wn, i), indent+1,labelEntry);
	break;
      }
    case OPC_EXC_SCOPE_END:
     break;

    case OPC_ASM_STMT:
      ir_put_wn_llvm(wn, indent,labelEntry);
      already_dumped_wn = TRUE;
      ir_put_stmt_llvm(WN_kid(wn,0), indent+1,labelEntry);
      ir_put_stmt_llvm(WN_kid(wn,1), indent+1,labelEntry);
      ir_put_marker("ASM_INPUTS", indent);
      {
	INT i;
	for (i = 2; i < WN_kid_count(wn); i++) {
	  ir_put_expr_llvm(WN_kid(wn,i), indent+1,&labelEntry);
	}
      }
      ir_put_marker("END_ASM_INPUTS", indent);
      break;

    default: 
      {
			INT last_is_expr = TRUE;
			OPCODE opc2;
			for (i = 0; i < WN_kid_count(wn); i++) {
			  wn2 = WN_kid(wn,i);
				 if (wn2) {
					opc2 = WN_opcode(wn2);
					  if (opc2 == 0) {
							 fprintf(ir_ofile, "### error: WN opcode 0\n");
					   } else if (OPCODE_is_expression(opc2)) {
							  Value *val = ir_put_expr_llvm(wn2, indent+1,&labelEntry);
							  vectVal.push_back(val);
							  numVal++;
							  last_is_expr = 1;
					   } else if (OPCODE_is_stmt(opc2) || OPCODE_is_scf(opc2)) {
							 if (last_is_expr) {
								 ir_put_marker("BODY", indent);
								 ir_put_stmt_llvm(wn2, indent+1,labelEntry);
							  } else
								  ir_put_stmt_llvm(WN_kid(wn,i), indent+1,labelEntry);
								  last_is_expr = 0;
							  } else {
								   fprintf(ir_ofile, "### error: unknown opcode type %d\n",opc2);
							  }
					  } 
				   else //match if(wn2)
						ir_put_stmt_llvm(wn2, indent+1,labelEntry);
				} //match for
		  } //match default 
      } // switch case 
  
	
	
	
	if (!already_dumped_wn)
      ir_put_wn_llvm(wn, indent,labelEntry);

  
  } else
    ir_put_wn_llvm(wn, indent,labelEntry);



}


/*
 *  Write a statement WN * and its children in prefix order.
 */
static void ir_put_stmt(WN * wn, INT indent)
{
  INT i;
  WN * wn2;
  USRCPOS srcpos;
  
  if (wn) {
    OPCODE opc = WN_opcode(wn);
    BOOL already_dumped_wn = FALSE;
    USRCPOS_srcpos(srcpos) = WN_Get_Linenum(wn);
    if (USRCPOS_srcpos(srcpos) != 0 && 
	USRCPOS_srcpos(srcpos) != USRCPOS_srcpos(last_srcpos)) {
      last_srcpos = srcpos;
#ifdef FRONT_END
      fprintf(ir_ofile, "%*sLOC %d %d\n", indent, "",
		USRCPOS_filenum(srcpos), USRCPOS_linenum(srcpos));
#else
      //print_source(USRCPOS_srcpos(srcpos));
#endif
    }

    if (OPCODE_is_scf(opc) || dump_parent_before_children) {
      ir_put_wn(wn, indent);
      already_dumped_wn = TRUE;
    }

    switch (opc) {

    case OPC_BLOCK:
      wn2 = WN_first(wn);
      while (wn2) {
	ir_put_stmt(wn2, indent);
	wn2 = WN_next(wn2);
      }
      ir_put_marker("END_BLOCK", indent);
      break;

    case OPC_REGION:
      ir_put_marker("REGION EXITS", indent);
      ir_put_stmt(WN_region_exits(wn), indent+1);

      ir_put_marker("REGION PRAGMAS", indent);
      ir_put_stmt(WN_region_pragmas(wn), indent+1);

      ir_put_marker("REGION BODY", indent);
      ir_put_stmt(WN_region_body(wn), indent+1);

      /* check to make sure cg.so is loaded first */
      /* IR_dump_region will be NULL if it is not */
#ifdef BACK_END
      if (IR_dump_region)
	CG_Dump_Region(ir_ofile, wn);
#endif /* BACK_END */
      { char str[20];
	sprintf(str,"END_REGION %d", WN_region_id(wn));
	ir_put_marker(str, indent);
      }
      break;
      
    case OPC_LABEL:
      ir_put_wn(wn, indent);
      if ( WN_label_loop_info(wn) != NULL ) {
	ir_put_stmt(WN_label_loop_info(wn), indent+1);
      }
#if defined(BACK_END) || defined(IR_TOOLS)
      if (OPT_Enable_WHIRL_SSA)
        ir_put_phi_list(wn, indent);
#endif
      already_dumped_wn = TRUE;
      break;

    case OPC_IF:
      ir_put_expr(WN_if_test(wn), indent+1);
      if (WN_then(wn)) {
	ir_put_marker("THEN", indent);
	ir_put_stmt(WN_then(wn), indent+1);
      }
      if (WN_else(wn)) {
	ir_put_marker("ELSE", indent);
	ir_put_stmt(WN_else(wn), indent+1);
      }
      ir_put_marker("END_IF", indent);
#if defined(BACK_END) || defined(IR_TOOLS)
      if (OPT_Enable_WHIRL_SSA)
        ir_put_phi_list(wn, indent);
#endif
      break;

    case OPC_DO_LOOP:
      ir_put_expr(WN_index(wn), indent+1);
      ir_put_marker("INIT",indent);
      ir_put_stmt(WN_start(wn), indent+1);
#if defined(BACK_END) || defined(IR_TOOLS)
      if (OPT_Enable_WHIRL_SSA) {
        ir_put_phi_list(wn, indent+1);
      }
#endif
      ir_put_marker("COMP", indent);
      ir_put_expr(WN_end(wn), indent+1);
      ir_put_marker("INCR", indent);
      ir_put_stmt(WN_step(wn), indent+1);
      /* optional loop_info */
      if ( WN_do_loop_info(wn) != NULL ) {
	ir_put_stmt(WN_do_loop_info(wn), indent);
      }
      ir_put_marker("BODY", indent);
      ir_put_stmt(WN_do_body(wn), indent+1);
      break;

    case OPC_WHILE_DO:
#if defined(BACK_END) || defined(IR_TOOLS)
      if (OPT_Enable_WHIRL_SSA) {
        ir_put_phi_list(wn, indent+1);
      }
#endif
      ir_put_marker("COMP",indent);
      ir_put_expr(WN_kid(wn, 0), indent+1);
      ir_put_marker("BODY", indent);
      ir_put_stmt(WN_kid(wn, 1), indent+1);
      break;

    case OPC_DO_WHILE:
#if defined(BACK_END) || defined(IR_TOOLS)
      if (OPT_Enable_WHIRL_SSA) {
        ir_put_phi_list(wn, indent+1);
      }
#endif
      ir_put_marker("BODY", indent);
      ir_put_stmt(WN_kid(wn, 1), indent+1);
      ir_put_marker("COMP",indent);
      ir_put_expr(WN_kid(wn, 0), indent+1);
      break;

    case OPC_LOOP_INFO:
      ir_put_wn(wn, indent);
      if ( WN_loop_induction(wn) != NULL ) {
	ir_put_expr(WN_loop_induction(wn), indent+1);
      }
      if ( WN_loop_trip(wn) != NULL ) {
	ir_put_expr(WN_loop_trip(wn), indent+1);
      }
      ir_put_marker("END_LOOP_INFO", indent);
      already_dumped_wn = TRUE;
      break;

    case OPC_COMPGOTO:
      ir_put_wn(wn, indent);
      ir_put_expr(WN_kid(wn,0), indent+1);
      ir_put_stmt(WN_kid(wn,1), indent+1);
      if (WN_kid_count(wn) > 2)
	ir_put_stmt(WN_kid(wn,2), indent+1);
      ir_put_marker("END_COMPGOTO", indent);
      already_dumped_wn = TRUE;
      break;

    case OPC_SWITCH:
      ir_put_wn(wn, indent);
      ir_put_expr(WN_kid(wn,0), indent+1);
      ir_put_stmt(WN_kid(wn,1), indent+1);
      if (WN_kid_count(wn) > 2)
	ir_put_stmt(WN_kid(wn,2), indent+1);
      ir_put_marker("END_SWITCH", indent);
#if defined(BACK_END) || defined(IR_TOOLS)
      if (OPT_Enable_WHIRL_SSA) {
        ir_put_phi_list(wn, indent+1);
      }
#endif
      already_dumped_wn = TRUE;
      break;

    case OPC_XGOTO:
      ir_put_wn(wn, indent);
      ir_put_expr(WN_kid(wn,0), indent+1);
      ir_put_stmt(WN_kid(wn,1), indent+1);
      ir_put_marker("END_XGOTO", indent);
      already_dumped_wn = TRUE;
      break;

    case OPC_WHERE:
      ir_put_expr(WN_kid(wn,0), indent+1);
      ir_put_marker("BODY", indent);
      ir_put_stmt(WN_kid(wn,1), indent+1);
      ir_put_stmt(WN_kid(wn,2), indent+1);
      break;

    case OPC_EXC_SCOPE_BEGIN:
      {
        INT i;
	for (i = 0; i < WN_kid_count(wn); i++)
	  ir_put_stmt(WN_kid(wn, i), indent+1);
	break;
      }
    case OPC_EXC_SCOPE_END:
     break;

    case OPC_ASM_STMT:
      ir_put_wn(wn, indent);
      already_dumped_wn = TRUE;
      ir_put_stmt(WN_kid(wn,0), indent+1);
      ir_put_stmt(WN_kid(wn,1), indent+1);
      ir_put_marker("ASM_INPUTS", indent);
      {
	INT i;
	for (i = 2; i < WN_kid_count(wn); i++) {
	  ir_put_expr(WN_kid(wn,i), indent+1);
	}
      }
      ir_put_marker("END_ASM_INPUTS", indent);
      break;

    default: 
      {
	INT last_is_expr = TRUE;
	OPCODE opc2;
	for (i = 0; i < WN_kid_count(wn); i++) {
	  wn2 = WN_kid(wn,i);
	  if (wn2) {
	    opc2 = WN_opcode(wn2);
	    if (opc2 == 0) {
	       fprintf(ir_ofile, "### error: WN opcode 0\n");
	    } else if (OPCODE_is_expression(opc2)) {
	      ir_put_expr(wn2, indent+1);
	      last_is_expr = 1;
	    }
	    else if (OPCODE_is_stmt(opc2) || OPCODE_is_scf(opc2)) {
	      if (last_is_expr) {
      		ir_put_marker("BODY", indent);
		ir_put_stmt(wn2, indent+1);
	      } else
		ir_put_stmt(WN_kid(wn,i), indent+1);
	      last_is_expr = 0;
	    } else {
	       fprintf(ir_ofile, "### error: unknown opcode type %d\n",opc2);
	    }
	  } else
	    ir_put_stmt(wn2, indent+1);
	}
      }
    }
    if (!already_dumped_wn)
      ir_put_wn(wn, indent);
  } else
    ir_put_wn(wn, indent);
}

#ifdef BACK_END

static void fdump_DUMPDEP(FILE *f, DUMPDEPp head, struct ALIAS_MANAGER *alias)
{
    DUMPDEPp	p,q;

    fprintf(f, "\nDUMP DEPENDENCY DUMP: == SAME_LOCATION, != NOT_ALIASED, ? POSSIBLY ALIASED\n");
    fprintf(f, "LNO dependency edges are <id: distance is_must direction>\n");

    for(p= head; p; p= DUMPDEP_next(p))
    {
	WN		*node= DUMPDEP_node(p);

	if (OPCODE_is_load(WN_opcode(node)))
	    continue;
	if (OPCODE_is_store(WN_opcode(node)))
	{
	    fprintf(f, "STORE[%d] ", DUMPDEP_id(p));
	}
	else if (WN_operator(node) == OPR_PARM)
	{
	    fprintf(f, "PARM[%d] ", DUMPDEP_id(p));
	}
	if (Valid_alias(alias, node))
	{
	    fprintf(f, "\t== {");
	    for(q= head; q; q= DUMPDEP_next(q))
	    {
		WN *wn =	DUMPDEP_node(q);
		if (Valid_alias(alias, wn) &&
		    SAME_LOCATION == Aliased(alias, node, wn))
		    fprintf(f, "%d,", DUMPDEP_id(q));
	    }
	    fprintf(f, "}\n\t\t!= {");

	    for(q= head; q; q= DUMPDEP_next(q))
	    {
		WN *wn =	DUMPDEP_node(q);
		if (Valid_alias(alias, wn) &&
		    NOT_ALIASED == Aliased(alias, node, wn))
		    fprintf(f, "%d,", DUMPDEP_id(q));
	    }

	    fprintf(f, "}\n\t\t? {");
	    for(q= head; q; q= DUMPDEP_next(q))
	    {
		WN *wn =	DUMPDEP_node(q);
		if (Valid_alias(alias, wn) &&
		    POSSIBLY_ALIASED == Aliased(alias, node, wn))
		    fprintf(f, "%d,", DUMPDEP_id(q));
	    }
	    fprintf(f, "}\n");
	}

	/*
	*  lno dependence information
	*/
	fprintf(f, "\t\t== LNO {");
	for(q= head; q; q= DUMPDEP_next(q))
	{
	    mUINT16		dist;
	    DIRECTION	dir;
	    BOOL		is_must, ok;


	    if (LnoDependenceEdge( node, DUMPDEP_node(q), &dist, &dir, &is_must, &ok))
	    {
		fprintf(f, "<%d: %d, %s",
			DUMPDEP_id(q), dist, is_must ? "MUST " : "");
		DIRECTION_Print(dir, f);
		fprintf(f, ">, ");
	    }
	}
	fprintf(f, "}\n");
    }
}
#endif /* BACK_END */



 


/*
 *  Write an WN * with OPC_FUNC_ENTRY.
 */
extern void IR_put_func_llvm(WN * wn, FILE *f /*, bool createFun*/)
{
  FILE *save;
  if (f) {
      save = ir_ofile;
      ir_ofile = f;
  }
  

  assert(wn != NULL && "### error: null WN pointer \n");
  
  OPCODE opcode = WN_opcode(wn);
  assert(opcode != 0 && "### error: WN opcode is 0 \n");
 
  
  
  if(opcode == OPC_FUNC_ENTRY /*&& createFun ==true*/)
  {
	  BasicBlock *labelEntry   = NULL;  
	  //Yan: W2LSYM should be deleted from my project step by step
	  //W2LSYM w2lsym = Get_Name_Level_Num_llvm (WN_st_idx(wn));
	  
	  //Yan get symbol information directly from WN node.	 
	  string symName = getSymNameFromWN(wn);
	  
	  unsigned kidNum = unsigned( WN_kid_count(wn)-3 );
	  
	  
	  //Yan all the functions has been build up in ST::print_LLVM
	  // so I comment below content.
	  
	  //SmallVector<Type*,32> FuncTyArgs; 
	   //createFuncsTyargs_llvm(FuncTyArgs, wn);
	   
	   //func = createFunc_llvm(wn,FuncTyArgs);
	   //assert(func!=nullptr &&" func is nullptr in IR_put_func_llvm");
	   
	  Function *func = nullptr;
	  assert(w2lFunMap.find(symName) != w2lFunMap.end() &&
	                       "no function in w2lFunMap ");
	  
	   func = w2lFunMap[symName];
	   
	   createFuncArgs_llvm(wn,func);
	   
	   labelEntry = BasicBlock::Create(w2lMod->getContext(), "entry",func, 0);
	   g_entry_labelEntry = labelEntry;
	  
	   
	   assert(labelEntry != nullptr &&"labelEntry is nullptr in IR_put_func_llvm");
	   
	   //Yan get all item in symMap to create alloc, for formal , create x.addr
	   //   %a = alloca i32, align 4
	   //     %b = alloca float, align 4
	//		%c = alloca i64, align 8
	//		%x.addr = alloca i32, align 4
	//		%y.addr = alloca float, align 4
	//		%z.addr = alloca i64, align 8
	   createAlloc_llvm(labelEntry);
	   
	   //Yan in this function. store value in function arg to memory alloc!
	   //store i32 %x, i32* %x.addr, align 4
	   //store float %y, float* %y.addr, align 4
	   //store i64 %z, i64* %z.addr, align 8
	   createArgsStore_llvm(wn, func, labelEntry);
	   
	   g_entry_insert== labelEntry->getFirstNonPHI(); 
	   
	   w2lFunMap.insert(pair<string,  Function* > (symName ,func) );
	   w2lEntryMap.insert(pair<string, BasicBlock*>(symName,labelEntry) );
	    
	   currentFun = func; 
		  
	   ir_put_stmt_llvm(wn, 0,  labelEntry);
	  
  }
  
  if (f) {
      ir_ofile = save;
  }
}



/*
 *  Write an WN * with OPC_FUNC_ENTRY.
 */
extern void IR_put_func(WN * wn, FILE *f)
{
  FILE *save;
  if (f) {
      save = ir_ofile;
      ir_ofile = f;
  }
  ir_put_stmt(wn, 0);
  if (f) {
      ir_ofile = save;
  }
}


/*========================================================================
 
      Dumping the WN structure.
  
 ========================================================================*/

/*
 *  Debugging:  dump an wn to stdout.
 */
extern void dump_wn(WN *wn)
{
  FILE *save;
  /* building hashtable is expensive, so may not initialize until dumping */
  if (!is_initialized) IR_reader_init();
  save = ir_ofile;
  ir_ofile = stdout; 
  IR_Dwarf_Gen_File_Table(TRUE/*dump_filenames*/); /* read source pathnames */
  ir_put_wn(wn, 0);
  ir_ofile = save;
}

extern void fdump_wn(FILE *fp, WN *wn)
{
  FILE *save;
  /* building hashtable is expensive, so may not initialize until dumping */
  if (!is_initialized) IR_reader_init();
  save = ir_ofile;
  ir_ofile = fp;
  IR_Dwarf_Gen_File_Table(TRUE/*dump_filenames*/); /* read source pathnames */
  ir_put_wn(wn, 0);
  ir_ofile = save;
}

extern void dump_tree(WN *wn)
{
   fdump_tree(stdout,wn);
}

#ifdef BACK_END
extern void dump_dep_tree(WN *wn, struct ALIAS_MANAGER *alias)
{
   fdump_dep_tree(stdout, wn, alias);
}
#endif /* BACK_END */

extern void dump_region_tree(WN *wn)
{
   BOOL	save = IR_dump_region;
   IR_dump_region = TRUE;
   fdump_tree(stdout, wn);
   IR_dump_region = save;
}

extern void dump_wn_no_st(WN *wn)
{
  BOOL save_follow_st;
  if (!is_initialized) IR_reader_init();
  save_follow_st = follow_st;
  follow_st = FALSE;
  dump_wn(wn);
  follow_st = save_follow_st;
}

extern void fdump_wn_no_st(FILE *fp, WN *wn)
{
  BOOL save_follow_st;
  FILE *save = ir_ofile;
  if (!is_initialized) IR_reader_init();
  save_follow_st = follow_st;
  ir_ofile = fp;
  follow_st = FALSE;
  IR_Dwarf_Gen_File_Table(TRUE/*dump_filenames*/); /* read source pathnames */
  ir_put_wn(wn, 0);
  follow_st = save_follow_st;
  ir_ofile = save;
}

extern void dump_tree_no_st(WN *wn)
{
  BOOL save_follow_st;
  if (!is_initialized) IR_reader_init();
  save_follow_st = follow_st;
  follow_st = FALSE;
  dump_tree(wn);
  follow_st = save_follow_st;
}

extern void fdump_tree(FILE *f, WN *wn)
{
  FILE *save;

  /* building hashtable is expensive, so may not initialize until dumping */
  if (!is_initialized) IR_reader_init();
  save = ir_ofile;
  ir_ofile = f;
  IR_Dwarf_Gen_File_Table(TRUE/*dump_filenames*/); /* read source pathnames */
  if (!wn) {
     fprintf(ir_ofile, "<null whirl tree>\n");
  } else if (OPCODE_is_stmt(WN_opcode(wn)) || OPCODE_is_scf(WN_opcode(wn)))
    ir_put_stmt(wn, 0);
  else if (OPCODE_is_expression(WN_opcode(wn)))
    ir_put_expr(wn, 0);
  else if (WN_opcode(wn) == OPC_FUNC_ENTRY)
    IR_put_func(wn, NULL);
  else
    fprintf(ir_ofile, "unknown opcode in (WN *) %p\n", wn);
  ir_ofile = save;
}


extern void fdump_tree_no_st( FILE *f, WN *wn ){
  BOOL save_follow_st;
  if (!is_initialized) IR_reader_init();
  save_follow_st = follow_st;
  follow_st = FALSE;
  fdump_tree( f, wn );
  follow_st = save_follow_st;
}

extern void fdump_tree_with_alias( FILE *f, const WN *wn, WN_MAP map, const struct ALIAS_MANAGER *am)
{
  WN_MAP save = IR_alias_map;
  const struct ALIAS_MANAGER *save_am = am;
  IR_alias_map = map;
  IR_alias_mgr = am;
  fdump_tree( f, (WN *)wn );
  IR_alias_map = save;
  IR_alias_mgr = save_am;
}

extern void enable_tree_freq_display(void){
  IR_freq_map = WN_MAP_FEEDBACK;
}

extern void disable_tree_freq_display(void){
  IR_freq_map = WN_MAP_UNDEFINED;
}

extern void fdump_tree_with_freq( FILE *f, const WN *wn, WN_MAP map){
  WN_MAP save = IR_freq_map;
  IR_freq_map = map;
  fdump_tree( f, (WN *)wn );
  IR_freq_map = save;
}

extern void fdump_region_tree(FILE *f, WN *wn){
   BOOL	save = IR_dump_region;
   IR_dump_region = TRUE;
   fdump_tree(f, wn);
   IR_dump_region = save;
}
// ======================================================================
//   WN_TREE_put_stmt, WN_TREE_put_expr, WN_TREE_put_func
//   (akin to ir_put_stmt, ir_put_expr, IR_put_func in ir_reader.c)
// ======================================================================

// Expressions are printed in POSTFIX order unless dump_parent_before_children
// is set in which case they are printed in prefix order.
// WN_TREE_put_expr is akin to ir_put_expr

static void WN_TREE_put_expr(WN * wn, INT indent) {
  WN * wn2;
  WN * parent_wn;

  Is_True(OPCODE_is_expression(WN_opcode(wn)) || OPCODE_is_call(WN_opcode(wn)),
          ("WN_TREE_put_expr invoked with non-expression, opcode = %s",
           OPCODE_name(WN_opcode(wn))));

  // See if the parent op should be dumped before the children  (PRE_ORDER)
  if (dump_parent_before_children) {
    // create PRE_ORDER tree iterator; use stack of default size

    WN_TREE_ITER<PRE_ORDER> tree_iter(wn);

#ifndef __GNU_BUG_WORKAROUND
    while (tree_iter != LAST_PRE_ORDER_ITER) {
#else
    while (tree_iter != WN_TREE_ITER<PRE_ORDER, WN*>()) {
#endif
      wn2 = tree_iter.Wn();

      if (OPCODE_is_expression(WN_opcode(wn2)) ||
          OPCODE_is_call(WN_opcode(wn2)))
        ir_put_wn(wn2,indent+tree_iter.Depth());

      else if (WN_operator(wn2) == OPR_BLOCK) {
        // block under an expression had better be due to comma/rcomma
        parent_wn = tree_iter.Get_parent_wn();
        Is_True(parent_wn && 
                ((WN_operator(parent_wn) == OPR_COMMA) ||
                 (WN_operator(parent_wn) == OPR_RCOMMA)),
                ("block under expression not a part of comma/rcomma"));

        WN_TREE_put_stmt(wn2, indent + tree_iter.Depth()); 
        tree_iter.Unwind();// have already traversed down the tree 
                           //  skip going down this tree again
      }

      else
        fprintf(ir_ofile, "%*sopcode %d not an expression\n", 
                indent+1,"", WN_opcode(wn2));
      ++tree_iter;
    } // while
  } // if dump_parent_before_children
  else {
    // post order traversal of expressions falls back on ir_put_expr
    // this is so because expressions can contain statements (through comma)
    // and in post order, we need to stop going down at statement boundaries 
    // which is not currently possible with the generic post_order tree
    // iterator
    ir_put_expr(wn,indent);
  } 
}

// ======================================================================
// Statements are printed in PREFIX order or if dump_parent_before_children
// is set 
// WN_TREE_put_stmt is akin to ir_put_stmt (it is also recursive)
// ======================================================================

static void WN_TREE_put_stmt(WN * wn, INT indent)
{
  INT i;
  WN * wn2;
  USRCPOS srcpos;

  Is_True(wn != 0, ("WN_TREE_put_stmt called with null whirl tree"));

  OPCODE opc = WN_opcode(wn);
  Is_True(OPCODE_is_scf(opc) || OPCODE_is_stmt(opc),
          ("WN_TREE_put_stmt invoked on non statement: opcode = %s",
           OPCODE_name(opc)));

  if (wn) {
    BOOL already_dumped_wn = FALSE;
    USRCPOS_srcpos(srcpos) = WN_Get_Linenum(wn);
    if (USRCPOS_srcpos(srcpos) != 0 && 
	USRCPOS_srcpos(srcpos) != USRCPOS_srcpos(last_srcpos)) {
      last_srcpos = srcpos;
#ifdef FRONT_END
      fprintf(ir_ofile, "%*sLOC %d %d\n", indent, "",
		USRCPOS_filenum(srcpos), USRCPOS_linenum(srcpos));
#else
      print_source(USRCPOS_srcpos(srcpos));
#endif
    }

    if (OPCODE_is_scf(opc) || dump_parent_before_children) {
      ir_put_wn(wn, indent);
      already_dumped_wn = TRUE;
    }

    // create an iterator to go through the statements in a block;
    WN_TREE_ITER<PRE_ORDER> tree_iter(wn);

    switch (opc) {

    case OPC_BLOCK:
      if (WN_first(wn)) {
          ++tree_iter; // get to the first kid of the block
#ifndef __GNU_BUG_WORKAROUND
        while (tree_iter != LAST_PRE_ORDER_ITER) {
#else
        while (tree_iter != WN_TREE_ITER<PRE_ORDER, WN*>()) {
#endif
          wn2 = tree_iter.Wn();
          WN_TREE_put_stmt(wn2, indent);// traverse the tree under wn2
          tree_iter.Unwind(); // have already traversed the tree under wn2
                              // skip going down the tree again
        }
      }
      ir_put_marker("END_BLOCK", indent);
      break;

    case OPC_REGION:
      ir_put_marker("REGION EXITS", indent);
      WN_TREE_put_stmt(WN_region_exits(wn), indent+1);

      ir_put_marker("REGION PRAGMAS", indent);
      WN_TREE_put_stmt(WN_region_pragmas(wn), indent+1);

      ir_put_marker("REGION BODY", indent);
      WN_TREE_put_stmt(WN_region_body(wn), indent+1);

      /* check to make sure cg.so is loaded first */
      /* IR_dump_region will be NULL if it is not */
#ifdef BACK_END
      if (IR_dump_region)
	CG_Dump_Region(ir_ofile, wn);
#endif /* BACK_END */
      { char str[20];
	sprintf(str,"END_REGION %d", WN_region_id(wn));
	ir_put_marker(str, indent);
      }
      break;
      
    case OPC_LABEL:
      ir_put_wn(wn, indent);
      if ( WN_label_loop_info(wn) != NULL ) {
	WN_TREE_put_stmt(WN_label_loop_info(wn), indent+1);
      }
      already_dumped_wn = TRUE;
      break;

    case OPC_IF:
      WN_TREE_put_expr(WN_if_test(wn), indent+1);
      if (WN_then(wn)) {
	ir_put_marker("THEN", indent);
	WN_TREE_put_stmt(WN_then(wn), indent+1);
      }
      if (WN_else(wn)) {
	ir_put_marker("ELSE", indent);
	WN_TREE_put_stmt(WN_else(wn), indent+1);
      }
      ir_put_marker("END_IF", indent);
      break;

    case OPC_DO_LOOP:
      WN_TREE_put_expr(WN_index(wn), indent+1);
      ir_put_marker("INIT",indent);
      WN_TREE_put_stmt(WN_start(wn), indent+1);
      ir_put_marker("COMP", indent);
      WN_TREE_put_expr(WN_end(wn), indent+1);
      ir_put_marker("INCR", indent);
      WN_TREE_put_stmt(WN_step(wn), indent+1);
      /* optional loop_info */
      if ( WN_do_loop_info(wn) != NULL ) {
	WN_TREE_put_stmt(WN_do_loop_info(wn), indent);
      }
      ir_put_marker("BODY", indent);
      WN_TREE_put_stmt(WN_do_body(wn), indent+1);
      break;

    case OPC_LOOP_INFO:
      ir_put_wn(wn, indent);
      if ( WN_loop_induction(wn) != NULL ) {
	WN_TREE_put_expr(WN_loop_induction(wn), indent+1);
      }
      if ( WN_loop_trip(wn) != NULL ) {
	WN_TREE_put_expr(WN_loop_trip(wn), indent+1);
      }
      ir_put_marker("END_LOOP_INFO", indent);
      already_dumped_wn = TRUE;
      break;

    case OPC_COMPGOTO:
      ir_put_wn(wn, indent);
      WN_TREE_put_expr(WN_kid(wn,0), indent+1);
      WN_TREE_put_stmt(WN_kid(wn,1), indent+1);
      if (WN_kid_count(wn) > 2)
	WN_TREE_put_stmt(WN_kid(wn,2), indent+1);
      ir_put_marker("END_COMPGOTO", indent);
      already_dumped_wn = TRUE;
      break;

    case OPC_SWITCH:
      ir_put_wn(wn, indent);
      WN_TREE_put_expr(WN_kid(wn,0), indent+1);
      WN_TREE_put_stmt(WN_kid(wn,1), indent+1);
      if (WN_kid_count(wn) > 2)
	WN_TREE_put_stmt(WN_kid(wn,2), indent+1);
      ir_put_marker("END_SWITCH", indent);
      already_dumped_wn = TRUE;
      break;

    case OPC_XGOTO:
      ir_put_wn(wn, indent);
      WN_TREE_put_expr(WN_kid(wn,0), indent+1);
      WN_TREE_put_stmt(WN_kid(wn,1), indent+1);
      ir_put_marker("END_XGOTO", indent);
      already_dumped_wn = TRUE;
      break;

    case OPC_WHERE:
      WN_TREE_put_expr(WN_kid(wn,0), indent+1);
      ir_put_marker("BODY", indent);
      WN_TREE_put_stmt(WN_kid(wn,1), indent+1);
      WN_TREE_put_stmt(WN_kid(wn,2), indent+1);
      break;

    case OPC_EXC_SCOPE_BEGIN:
      {
        INT i;
	for (i = 0; i < WN_kid_count(wn); i++)
	  WN_TREE_put_stmt(WN_kid(wn, i), indent+1);
	break;
      }
    case OPC_EXC_SCOPE_END:
     break;
    default: 
      {
	INT last_is_expr = TRUE;
	OPCODE opc2;
	for (i = 0; i < WN_kid_count(wn); i++) {
	  wn2 = WN_kid(wn,i);
	  if (wn2) {
	    opc2 = WN_opcode(wn2);
	    if (opc2 == 0) {
	       fprintf(ir_ofile, "### error: WN opcode 0\n");
	    } else if (OPCODE_is_expression(opc2)) {
	      WN_TREE_put_expr(wn2, indent+1);
	      last_is_expr = 1;
	    }
	    else if (OPCODE_is_stmt(opc2) || OPCODE_is_scf(opc2)) {
	      if (last_is_expr) {
      		ir_put_marker("BODY", indent);
		WN_TREE_put_stmt(wn2, indent+1);
	      } else
		WN_TREE_put_stmt(WN_kid(wn,i), indent+1);
	      last_is_expr = 0;
	    } else {
	       fprintf(ir_ofile, "### error: unknown opcode type %d\n",opc2);
	    }
	  } else
	    WN_TREE_put_stmt(wn2, indent+1);
	}
      }
    }
    if (!already_dumped_wn)
      ir_put_wn(wn, indent);
  } else
    ir_put_wn(wn, indent);
}

extern void WN_TREE_put_func(WN * wn, FILE *f)
{
  FILE *save;
  if (f) {
      save = ir_ofile;
      ir_ofile = f;
  }
  WN_TREE_put_stmt(wn, 0);
  if (f) {
      ir_ofile = save;
  }
}

extern void WN_TREE_fdump_tree(FILE *f, WN *wn)
{
  FILE *save;

  /* building hashtable is expensive, so may not initialize until dumping */
  if (!is_initialized) IR_reader_init();
  save = ir_ofile;
  ir_ofile = f;
  IR_Dwarf_Gen_File_Table(TRUE/*dump_filenames*/); /* read source pathnames */
  ir_print_filename(TRUE/*dump_filenames*/); /* print LOC 0 0 header */
  if (!wn) {
     fprintf(ir_ofile, "<null whirl tree>\n");
  } else if (OPCODE_is_stmt(WN_opcode(wn)) || OPCODE_is_scf(WN_opcode(wn)))
    WN_TREE_put_stmt(wn, 0);
  else if (OPCODE_is_expression(WN_opcode(wn)))
    WN_TREE_put_expr(wn, 0);
  else if (WN_opcode(wn) == OPC_FUNC_ENTRY)
  WN_TREE_put_func(wn, NULL);
  else
    fprintf(ir_ofile, "unknown opcode in (WN *) %p\n", wn);
  ir_ofile = save;
} 

extern void WN_TREE_dump_tree(WN *wn)
{
   WN_TREE_fdump_tree(stdout,wn);
}


#ifdef BACK_END
extern void fdump_dep_tree( FILE *f, WN *wn, struct ALIAS_MANAGER *alias)
{
  if (alias)
  {
    BOOL   save= IR_dump_map_info;

    L_Save();
    IR_dump_map_info= TRUE;
    IR_DUMPDEP_info = TRUE;
    IR_DUMPDEP_head= NULL;;

    fdump_tree( f, (WN *)wn );
    fdump_DUMPDEP(f, IR_DUMPDEP_head, alias); 


    L_Free();
    IR_DUMPDEP_head= NULL;;
    IR_DUMPDEP_info = FALSE;
    IR_dump_map_info = save;
  }
  else
  {
    fprintf(f, "\talias manager not initialized\n");
  }
}
#endif /* BACK_END */

static void Check_for_IR_Dump0(INT phase, WN *pu, const char *phase_name, BOOL before)
{
    BOOL dump_ir;
    BOOL dump_symtab;
    dump_ir = Get_Trace ( TKIND_IR, phase );
    dump_symtab =  Get_Trace ( TKIND_SYMTAB, phase );
    if (dump_ir || dump_symtab) {
	fprintf(TFile,"\n\n========== Driver dump %s %s ==========\n",
		before == TRUE ? "before" : "after", phase_name);
	if (dump_ir) fdump_tree (TFile,pu);
	if (dump_symtab) {
	    Print_symtab (TFile, GLOBAL_SYMTAB);
	    Print_symtab (TFile, CURRENT_SYMTAB);
	}
    }
}

extern void Check_for_IR_Dump(INT phase, WN *pu, const char *phase_name)
{
    Check_for_IR_Dump0(phase, pu, phase_name, FALSE);
}

extern void Check_for_IR_Dump_Before_Phase(INT phase, WN *pu, const char *phase_name)
{
    Check_for_IR_Dump0(phase, pu, phase_name, TRUE);
}


// The following routines convert an ir data structure
// into a string.  We use helper routines to build the 
// string using stringstream.  Then the string is copied
// into a memory pool.  
//
//  ir_put_wn  -> help_image_wn(stringstream &ss, WN *, INT indent);
//  ir_put_expr -> help_image_expr(stringstream &ss, WN *wn, INT indent);
//  ir_put_stmt -> help_image_stmt(stringstream &ss, WN *wn, INT indent);
//  ir_put_WN_TREE_expr -> help_WN_TREE_image_expr(stringstream &ss, WN *wn, INT indent);
//  ir_put_WN_TREE_stmr -> help_WN_TREE_image_stmt(stringstream &ss, WN *wn, INT indent);
//  ir_put_wn  -> char *image_wn(MEM_POOL *p, WN *wn);


// foward declarations. 

static  void image_marker(stringstream &ss, const char *str, INT indent); 
char *image_st(MEM_POOL *pool, ST_IDX st_idx);
char *image_ty(MEM_POOL *pool, ST_IDX st_idx);
char *image_wn(MEM_POOL *pool, WN *wn);
char *image_stmt(MEM_POOL *pool, WN *wn);
char *image_WN_TREE_stmt(MEM_POOL *pool, WN *wn);
char *image_expr(MEM_POOL *pool, WN *wn);
char *image_WN_TREE_expr(MEM_POOL *pool, WN *wn);
char *image_lineno(MEM_POOL *pool, WN *wn);

void help_image_lineno(stringstream &ss, WN *wn);
void help_image_st (stringstream &ss, ST_IDX st_idx);
void help_image_ty(stringstream &ss, TY_IDX ty);
void help_image_expr(stringstream &ss, WN * wn, INT indent);
void help_image_wn(stringstream &ss, WN *wn, INT indent);
void help_image_stmt(stringstream &ss, WN * wn, INT indent);
void help_WN_TREE_image_stmt(stringstream &ss, WN *wn, int indent);
void help_WN_TREE_image_expr(stringstream &ss, WN * wn, INT indent);


/*
 *  Write out a marker at the indentation level.
 */
static 
void image_marker(stringstream &ss, const char *str, INT indent)
{
  int i;
  for (i = 0; i < indent; i++)
     ss << ' ';
  ss << str; 
}

char *
image_st(MEM_POOL *pool, ST_IDX st_idx)
{ 
  stringstream ss; 
  help_image_st(ss, st_idx);
  char *str  = (char *) MEM_POOL_Alloc(pool, strlen(ss.str().data()) + 1);
  
  // copy string into mempool.
  strcpy(str, ss.str().data()); 
  return str; 
}


char *
image_ty(MEM_POOL *pool, TY_IDX ty_idx)
{ 
  stringstream ss; 
  help_image_ty(ss, ty_idx);
  char *str  = (char *) MEM_POOL_Alloc(pool, strlen(ss.str().data()) + 1);
  
  // copy string into mempool.
  strcpy(str, ss.str().data()); 
  return str; 
}


void
help_image_st (stringstream &ss, ST_IDX st_idx)
{
  char *name;
  char *p;
  
  if (st_idx == (ST_IDX) 0) {
    /* labels may have empty st */
    ss << " <null-st>";

  } else if (!follow_st) {
    /* Do not follow ST *, so that it can dump useful information
       even when ST * is not valid */
    ss << " <st ";
    ss << (INT32) st_idx;
    ss << ">";

  } else {
    const ST* st = &St_Table[st_idx];
    if (ST_class(st) == CLASS_CONST) {
      name = Targ_Print(NULL, STC_val(st));
      /* new lines and spaces in constant strings 
       * will mess up the ascii reader,
       * so replace with underlines */
      for (p = name; *p != '\0'; p++)
	switch (*p) {
	case ' ':
	case '\t':
	case '\n':
	  *p = '_';
	}
    } else
    { 
      name = ST_name(st);
      ss << name; 
    }
  }
}

void
help_image_ty(stringstream &ss, TY_IDX ty)
{
   ss << " T<";
   ss << TY_id(ty);
   ss << ",";
   ss << TY_name(ty);
   ss << ",";
   ss << TY_align(ty);
   
   if (TY_is_restrict(ty)) 
      ss << ",R";
   
   if (TY_is_volatile(ty)) 
      ss << ",V";
   
   if (TY_is_const(ty)) 
      ss << ",C";
   
   ss << ">";
}

char *
image_lineno(MEM_POOL *pool, WN *wn)
{ 
  stringstream ss; 
  help_image_lineno(ss, wn);
  char *str  = (char *) MEM_POOL_Alloc(pool, strlen(ss.str().data()) + 1);
  Is_True(str != NULL, ("Allocation failed"));
  // copy string into mempool.
  strcpy(str, ss.str().data()); 
  return str; 
}

void
help_image_lineno(stringstream &ss, WN *wn)
{ 
  USRCPOS srcpos;
  USRCPOS_srcpos(srcpos) = WN_Get_Linenum(wn);
  if (USRCPOS_srcpos(srcpos) != 0)
  {
    ss << USRCPOS_linenum(srcpos);
  } 
} 

char *
image_wn(MEM_POOL *pool, WN *wn)
{ 
  stringstream ss; 
  help_image_wn(ss, wn, 0);
  char *str  = (char *) MEM_POOL_Alloc(pool, strlen(ss.str().data()) + 1);
  Is_True(str != NULL, ("Allocation failed"));
  // copy string into mempool.
  strcpy(str, ss.str().data()); 
  return str; 
}

char *
image_stmt(MEM_POOL *pool, WN *wn)
{ 
  stringstream ss; 
  help_image_stmt(ss, wn, 0);
  char *str  = (char *) MEM_POOL_Alloc(pool, strlen(ss.str().data()) + 1);
  Is_True(str != NULL, ("Allocation failed"));
  // copy string into mempool.
  strcpy(str, ss.str().data()); 
  return str; 
}





char *
image_WN_TREE_stmt(MEM_POOL *pool, WN *wn)
{ 
  stringstream ss; 
  help_WN_TREE_image_stmt(ss, wn, 0);
  char *str  = (char *) MEM_POOL_Alloc(pool, strlen(ss.str().data()) + 1);
  Is_True(str != NULL, ("Allocation failed"));
  // copy string into mempool.
  strcpy(str, ss.str().data()); 
  return str; 
}


char *
image_expr(MEM_POOL *pool, WN *wn)
{ 
  stringstream ss; 
  help_image_expr(ss, wn, 0);
  char *str  = (char *) MEM_POOL_Alloc(pool, strlen(ss.str().data()) + 1);
  Is_True(str != NULL, ("Allocation failed"));  
  // copy string into mempool.
  strcpy(str, ss.str().data()); 
  return str; 
}


char *
image_WN_TREE_expr(MEM_POOL *pool, WN *wn)
{ 
  stringstream ss; 
  help_WN_TREE_image_expr(ss, wn, 0);
  char *str  = (char *) MEM_POOL_Alloc(pool, strlen(ss.str().data()) + 1);
  Is_True(str != NULL, ("Allocation failed"));
  // copy string into mempool.
  strcpy(str, ss.str().data()); 
  return str; 
}


/*
 *  Write an expression and its children in postfix order.
 *  If dump_parent_before_children, write it in prefix order instead.
 */

void
help_image_expr(stringstream &ss, WN * wn, INT indent)
{
  INT i;
  WN * wn2;

  /* See if the parent op should be dumped before or after the children */
  if (dump_parent_before_children) {
     help_image_wn(ss,wn,indent);
  }
  for (i = 0; i < WN_kid_count(wn); i++) {
    wn2 = WN_kid(wn,i);
    if (wn2) {
      OPCODE op = WN_opcode(wn2);
      if ((OPCODE_FIRST <= op && op <= OPCODE_LAST) &&
	  (OPCODE_is_expression(op) || OPCODE_is_call(op)))
	      help_image_expr(ss, WN_kid(wn,i), indent+1);
      else
      if (    op == OPC_BLOCK
           && (    (WN_operator(wn) == OPR_RCOMMA && i == 1)
                || (WN_operator(wn) == OPR_COMMA && i == 0)))
        help_image_stmt(ss, wn2, indent+1);
      else
      { 
        int i; 
        for (i = 0; i < indent+1; i++) ss << ' ';
	ss << "opcode ";
        ss << op; 
        ss << " not an expression\n";
      } 
    } else  
    { 
        int i; 
        for (i = 0; i < indent+1; i++) ss << ' ';
        ss << "null-expression\n";
    } 
  }
  if (!dump_parent_before_children) {
     help_image_wn(ss, wn, indent);
  }
}


/*
 *  Write an WN * in ascii form on an individual line.
 */ 
void
help_image_wn(stringstream &ss, WN *wn, INT indent)
{ 
    OPCODE opcode;

    if (wn == NULL) {
    	/* null statement */
        ss << "### error: null WN pointer\n";
   	return;
    } else {
	if (IR_dump_wn_addr) {
	    char buff[32];
            sprintf(buff, "0x%8p: ", wn);
            ss << buff;
	}
	opcode = WN_opcode(wn);
    }
    if (opcode == 0) {
        ss << "### error: WN opcode 0\n";
	return;
    }

    if (indent > 0)
    { 
      image_marker(ss, "", indent);
    } 
    /*
     *  for dependency dumping, dump a handle to refer to later
     */
    if (IR_DUMPDEP_info) {
	INT32 handle=  0;
	if (OPCODE_has_alias_info(WN_opcode(wn)) && WN_map_id(wn) != -1) {
	    handle= AddToDUMPDEP(wn);
	}
	char buff[32];
        sprintf(buff, "[%5d]", handle);
        ss << buff;
    }
    if (indent > 0 && opcode == OPC_LABEL)
	image_marker(ss, "", indent-1);
    else
        image_marker(ss,"",indent);


    ss << OPCODE_name(opcode) + strlen("OPC_");

    if (OPCODE_has_offset(opcode)) {
	if (OPCODE_operator(opcode) == OPR_PRAGMA || 
	    OPCODE_operator(opcode) == OPR_XPRAGMA)
	{
	    ss << " ";
	    ss << WN_pragma_flags(wn);
            ss << " ";
            ss << WN_pragma(wn);
	}
	else
	{
	  ss << " ";
          ss << WN_offset(wn);
	}
    } else if (OPCODE_has_2offsets(opcode)) {
      ss << " ";
      ss << WN_loop_trip_est(wn);
      ss << " ";
      ss << WN_loop_depth(wn);
    }

    switch (OPCODE_operator(opcode)) {

    case OPR_INTRINSIC_OP:
    case OPR_ARRAYEXP:
#ifdef KEY
    case OPR_PURE_CALL_OP:
#endif
      {
	ss << " ";
        ss << WN_kid_count(wn);
        break;
      }

    case OPR_REGION:
	ss << " ";
        ss << WN_region_id(wn);

#ifdef BACK_END
      {
	RID *rid = REGION_get_rid(wn);
	if (rid != NULL)
        {
	  ss << " ";
          ss << RID_id(rid);
        }
      }
#endif /* BACK_END */
      ss << "(kind=";
      ss << WN_region_kind(wn);
      ss << ")";
      break;
#if defined(TARG_SL)
    case OPR_LDA:
    case OPR_ISTORE:
      ss << "im:";
      ss << WN_is_internal_mem_ofst(wn);
      break; 
#endif
    case OPR_LDBITS:
    case OPR_ILDBITS:
    case OPR_STBITS:
    case OPR_ISTBITS:
    case OPR_EXTRACT_BITS:
    case OPR_COMPOSE_BITS:
      ss << " <bofst:";
      ss << WN_bit_offset(wn);
      ss << " bsize:";
      ss << WN_bit_size(wn);
      ss << ">";
      break;

    case OPR_ASM_INPUT:
      ss << " opnd:";
      ss << WN_asm_opnd_num(wn);
      break;

    default:
      break;
    }

    if (OPCODE_has_inumber(opcode)) {
	switch (opcode) {
	case OPC_IO:
	  ss << " <";
          ss << WN_intrinsic(wn);
          ss << ",";
          ss << IOSTATEMENT_name((IOSTATEMENT) WN_intrinsic(wn));
          ss << ","; 
          ss <<     get_iolibrary_name(WN_IO_Library(wn)); 
          ss << ">";
          break;
	case OPC_IO_ITEM:
	  ss << " <";
          ss << WN_intrinsic(wn);
          ss << ",";
          ss << IOITEM_name((IOITEM) WN_intrinsic(wn));
          ss << ">";
          break;
	default:		/* intrinsic */
	    Is_True(OPCODE_operator(opcode) == OPR_INTRINSIC_OP ||
		    OPCODE_operator(opcode) == OPR_INTRINSIC_CALL,
		    ("ir_put_wn, expected an intrinsic"));
#if defined(BACK_END) || defined(IR_TOOLS) || defined(TARG_NVISA)
	    ss << " <";
            ss << WN_intrinsic(wn);
            ss << "," ;
            ss << INTRINSIC_name((INTRINSIC) WN_intrinsic(wn));
            ss << ">";
		
#endif
	    break;
	}
    }

    if (OPCODE_has_bits(opcode))
    {
      ss << " ";
      ss << WN_cvtl_bits(wn);
    }
    if (OPCODE_has_label(opcode))
    {
      ss << " L";
      ss << WN_label_number(wn);
    }
    if (OPCODE_has_flags(opcode)) 
    {
      ss << " ";
      ss << WN_flag(wn);
    }
    if (OPCODE_has_sym(opcode)) {
      ss << " ";
      help_image_st (ss,WN_st_idx(wn));
    }

    if (OPCODE_has_1ty(opcode)) {
	if (WN_ty(wn) != (TY_IDX) 0) 
	   help_image_ty(ss, WN_ty(wn));
	else
	    if (opcode != OPC_IO_ITEM)
	      ss << " T<### ERROR: null ptr>";
    } else if (OPCODE_has_2ty(opcode)) {
	if (WN_ty(wn) != (TY_IDX) 0) 
	   help_image_ty(ss,WN_ty(wn));
	else
	  ss << " T<### ERROR: null ptr>";
	if (WN_load_addr_ty(wn) != (TY_IDX) 0) 
	   help_image_ty(ss, WN_load_addr_ty(wn));
	else
	  ss << " T<### ERROR: null ptr>";
    }

    if (OPCODE_has_ndim(opcode))
    { 
      ss << " ";
      ss << WN_num_dim(wn);
    }
    if (OPCODE_has_esize(opcode))
    {
        ss << " ";
        ss << WN_element_size(wn);
    }
    if (OPCODE_has_num_entries(opcode))
    {
      ss << " ";
      ss << WN_num_entries(wn);
    }
    if (OPCODE_has_last_label(opcode))
    { 
      ss << " ";
      ss << WN_last_label(wn);
    }

    if (OPCODE_has_value(opcode)) {
      ss << " ";
      ss << WN_const_val(wn);
      /* Also print the hex value for INTCONSTs */
      if (OPCODE_operator(opcode) == OPR_INTCONST || opcode == OPC_PRAGMA) {
	ss << " (0x";
        ss << hex << WN_const_val(wn);
      }
    }

    if (OPCODE_has_field_id(opcode) && WN_field_id(wn)) {
      ss << " <field_id:";
      ss << WN_field_id(wn);
      ss << ">";
    }
    
    if (OPCODE_has_ereg_supp(opcode)) {
	INITO_IDX ino = WN_ereg_supp(wn);
	if (ino != 0)
	  {
	    ss << " INITO<";
            ss << INITO_IDX_index (ino); 
            ss << ",";
            ss << INITO_IDX_index (ino);
            ss << ">";
	  }
    }
    if (opcode == OPC_COMMENT) {
      ss << " # ";
      ss << Index_To_Str(WN_offset(wn));
    }

    if (follow_st && OPCODE_has_sym(opcode) && OPCODE_has_offset(opcode)
	&& WN_st_idx(wn) != (ST_IDX) 0 && (ST_class(WN_st(wn)) == CLASS_PREG)
        && opcode != OPC_PRAGMA)
	{
	    if (Preg_Is_Dedicated(WN_offset(wn))) {
		if (Preg_Offset_Is_Int(WN_offset(wn))) {
		  ss << " # $r";
                  ss << WN_offset(wn);
		}
		else if (Preg_Offset_Is_Float(WN_offset(wn))) {
		  ss << " # $f";
                  ss << WN_offset(wn) - Float_Preg_Min_Offset;
		}
#ifdef TARG_X8664
		else if (Preg_Offset_Is_X87(WN_offset(wn))) {
		  ss << " # $st";
                  ss << WN_offset(wn) - X87_Preg_Min_Offset;
		}
#endif
	    }
	    else { 
	    	/* reference to a non-dedicated preg */
		if ((WN_offset(wn) - Last_Dedicated_Preg_Offset) 
			< PREG_Table_Size (CURRENT_SYMTAB) )
	    	{ 
                  ss << " # ";
                  ss << Preg_Name(WN_offset(wn));
		}
		else
		{ 
		  ss << " # <Invalid PREG Table index (";
                  ss << WN_offset(wn);
                  ss << ")>";
		}
	    }
	}

    if (opcode == OPC_XPRAGMA) {
      ss << " # ";
      ss << WN_pragmas[WN_pragma(wn)].name;
    }

    if (OPCODE_operator(opcode) == OPR_ASM_INPUT) {
      ss << " # \"";
      ss << WN_asm_input_constraint(wn);
      ss << "\"";
    }

    if (opcode == OPC_PRAGMA) {
        ss << " # ";
        ss << WN_pragmas[WN_pragma(wn)].name;
	switch(WN_pragma(wn)) {
	case WN_PRAGMA_DISTRIBUTE:
	case WN_PRAGMA_REDISTRIBUTE:
	case WN_PRAGMA_DISTRIBUTE_RESHAPE:
	  ss << ", ";
          ss << WN_pragma_index(wn);
          switch(WN_pragma_distr_type(wn)) {
	    case DISTRIBUTE_STAR:
	      ss << ", *";
              break;
	    case DISTRIBUTE_BLOCK:
	      ss << ", BLOCK";
		break;
	    case DISTRIBUTE_CYCLIC_CONST:
	      ss << ", CYCLIC(";
              ss << WN_pragma_preg(wn);
              ss << ")";
		break;
	    case DISTRIBUTE_CYCLIC_EXPR:
	      ss << ", CYCLIC(expr)";
		break;
	    }
	    break;
	case WN_PRAGMA_ASM_CONSTRAINT:
	  ss << ", \"";
          ss <<   WN_pragma_asm_constraint(wn);
          ss << "\", opnd:";
          ss << 		  WN_pragma_asm_opnd_num(wn);
          ss << " preg:" ;
          ss <<   WN_pragma_asm_copyout_preg(wn);
	  break;
	default:
            if (WN_pragma_arg2(wn) != 0 ) 
	    { 
	      ss << ", ";
	      ss << WN_pragma_arg1(wn), WN_pragma_arg2(wn);
              ss << ", ";
              ss << WN_pragma_arg2(wn);
	    }
            else
		if (WN_pragma_arg1(wn) != 0 )
		  { 
		    ss << ", ";
                    ss << WN_pragma_arg1(wn);
		  }
	    break;
	} /* switch */
    }

    if (OPCODE_operator(opcode) == OPR_ASM_STMT) {
      ss << " # \"";
      ss << WN_asm_string(wn); 
      ss << "\"";
      if (WN_Asm_Volatile(wn))
      	ss << " (volatile)";
      if (WN_Asm_Clobbers_Mem(wn))
      	ss << " (memory)";
      if (WN_Asm_Clobbers_Cc(wn))
      	ss << " (cc)";
    }

    if (OPCODE_is_call(opcode))
    {
      ss << " # flags 0x";
      ss << hex << WN_call_flag(wn);
    }

    if (OPCODE_operator(opcode) == OPR_PARM) {
	INT flag =  WN_flag(wn);
	ss << " # ";
	if (flag & WN_PARM_BY_REFERENCE) 
	  ss << " by_reference ";
#if defined(TARG_SL)
	if (flag & WN_PARM_DEREFERENCE)  
          ss << " by_dereference ";
#endif
	if (flag & WN_PARM_BY_VALUE)     
	  ss << " by_value ";
	if (flag & WN_PARM_OUT)          
          ss << " out ";
	if (flag & WN_PARM_DUMMY)        
          ss << " dummy ";
	if (flag & WN_PARM_READ_ONLY)    
          ss << " read_only ";
	if (flag & WN_PARM_PASSED_NOT_SAVED) 
          ss << "passed_not_saved ";
	if (flag & WN_PARM_NOT_EXPOSED_USE) 
          ss << " not_euse ";
	if (flag & WN_PARM_IS_KILLED) 
          ss << " killed ";
    }

    if (IR_dump_map_info) {
      ss << " # <id ";
      ss << OPCODE_mapcat(opcode);
      ss << ":";
      ss << WN_map_id(wn);
      ss << ">";
		
	if (ir_put_map && ( WN_map_id(wn) != -1 )) {
	    switch ( WN_MAP_Get_Kind( ir_put_map ) ) {
	    case WN_MAP_KIND_VOIDP:
	      ss << " <map ";
              ss << hex << WN_MAP_Get( ir_put_map, wn );
              ss << ">";
		break;
	    case WN_MAP_KIND_INT32:
	      ss << " <map 0x";
              ss << hex << WN_MAP32_Get( ir_put_map, wn );
              ss << ">";
		break;
	    case WN_MAP_KIND_INT64:
	      ss <<  " <map 0x", 
 	      ss << WN_MAP64_Get( ir_put_map, wn );
              ss << ">";
	      break;
	    }
	}
#ifdef BACK_END
	if (UINT16 vertex = LNOGetVertex(wn)) {
	  ss << " <lno vertex ";
          ss << vertex;
          ss << ">";
	}
#endif
    }

    if (OPCODE_is_scf(WN_opcode(wn)) || OPCODE_is_stmt(WN_opcode(wn))) {

	USRCPOS srcpos;
	USRCPOS_srcpos(srcpos) = WN_Get_Linenum(wn);

	ss <<  " {line: ";
        ss << USRCPOS_filenum(srcpos); 
        ss <<  "/";
        ss << USRCPOS_linenum(srcpos); 
        ss << "}"; 
    }

#ifdef BACK_END
    if (IR_dump_alias_info(WN_opcode(wn))) {
      ss << " [alias_id: ";
      ss << WN_MAP32_Get(IR_alias_map, wn);
      ss << (IR_alias_mgr && IR_alias_mgr->Safe_to_speculate(wn) ? ",fixed" : "");
      ss << "]";
	      
    }
#endif

    if (IR_freq_map != WN_MAP_UNDEFINED && (OPCODE_is_scf(WN_opcode(wn)) ||
					    OPCODE_is_stmt(WN_opcode(wn)))) {
	USRCPOS srcpos;
	USRCPOS_srcpos(srcpos) = WN_Get_Linenum(wn);

	ss << " {freq: ";
        ss << WN_MAP32_Get(IR_freq_map, wn);
        ss << ", ln: ";
        ss << USRCPOS_linenum(srcpos);
        ss << ", col: ";
        ss << USRCPOS_column(srcpos);
        ss << "}";
		
    }
    if (Current_Map_Tab != NULL 
    	&& WN_MAP32_Get(WN_MAP_ALIAS_CLASS, wn) != 0) 
    {
      ss << " {class ";
      ss <<      WN_MAP32_Get(WN_MAP_ALIAS_CLASS, wn);
      ss << "}";
    }
    ss <<  "\n";
}


/*
 *  Write a statement WN * and its children in prefix order.
 */
void help_image_stmt(stringstream &ss, WN * wn, INT indent)
{
  INT i;
  WN * wn2;
  USRCPOS srcpos;

  if (wn) { 

    OPCODE opc = WN_opcode(wn);
    BOOL already_dumped_wn = FALSE;
  
    if (OPCODE_is_scf(opc) || dump_parent_before_children) {
      help_image_wn(ss, wn, indent);
      already_dumped_wn = TRUE;
    }

    switch (opc) {

    case OPC_BLOCK:
      wn2 = WN_first(wn);
      while (wn2) {
	help_image_stmt(ss,wn2, indent);
	wn2 = WN_next(wn2);
      }
      image_marker(ss, "END_BLOCK", indent);
      break;

    case OPC_REGION:
      image_marker(ss, "REGION EXITS", indent);
      help_image_stmt(ss, WN_region_exits(wn), indent+1);

      image_marker(ss, "REGION PRAGMAS", indent);
      help_image_stmt(ss, WN_region_pragmas(wn), indent+1);

      image_marker(ss, "REGION BODY", indent);
      help_image_stmt(ss, WN_region_body(wn), indent+1);

      /* check to make sure cg.so is loaded first */
      /* IR_dump_region will be NULL if it is not */
#ifdef BACK_END
      if (IR_dump_region)
	//CG_Dump_Region(ir_ofile, wn);
#endif /* BACK_END */
      { char str[20];
	sprintf(str,"END_REGION %d", WN_region_id(wn));
	image_marker(ss,str, indent);
      }
      break;
      
    case OPC_LABEL:
      help_image_wn(ss,wn, indent);
      if ( WN_label_loop_info(wn) != NULL ) {
	help_image_stmt(ss,WN_label_loop_info(wn), indent+1);
      }
      already_dumped_wn = TRUE;
      break;

    case OPC_IF:
      help_image_expr(ss,WN_if_test(wn), indent+1);
      if (WN_then(wn)) {
	image_marker(ss,"THEN", indent);
	help_image_stmt(ss, WN_then(wn), indent+1);
      }
      if (WN_else(wn)) {
	image_marker(ss,"ELSE", indent);
	help_image_stmt(ss,WN_else(wn), indent+1);
      }
      image_marker(ss,"END_IF", indent);
      break;

    case OPC_DO_LOOP:
      help_image_expr(ss,WN_index(wn), indent+1);
      image_marker(ss,"INIT",indent);
      help_image_stmt(ss,WN_start(wn), indent+1);
      image_marker(ss,"COMP", indent);
      help_image_expr(ss,WN_end(wn), indent+1);
      image_marker(ss,"INCR", indent);
      help_image_stmt(ss,WN_step(wn), indent+1);
      /* optional loop_info */
      if ( WN_do_loop_info(wn) != NULL ) {
	help_image_stmt(ss,WN_do_loop_info(wn), indent);
      }
      image_marker(ss,"BODY", indent);
      help_image_stmt(ss,WN_do_body(wn), indent+1);
      break;

    case OPC_LOOP_INFO:
      help_image_wn(ss,wn, indent);
      if ( WN_loop_induction(wn) != NULL ) {
	help_image_expr(ss,WN_loop_induction(wn), indent+1);
      }
      if ( WN_loop_trip(wn) != NULL ) {
	help_image_expr(ss,WN_loop_trip(wn), indent+1);
      }
      image_marker(ss,"END_LOOP_INFO", indent);
      already_dumped_wn = TRUE;
      break;

    case OPC_COMPGOTO:
      help_image_wn(ss,wn, indent);
      help_image_expr(ss,WN_kid(wn,0), indent+1);
      help_image_stmt(ss,WN_kid(wn,1), indent+1);
      if (WN_kid_count(wn) > 2)
	help_image_stmt(ss,WN_kid(wn,2), indent+1);
      image_marker(ss,"END_COMPGOTO", indent);
      already_dumped_wn = TRUE;
      break;

    case OPC_SWITCH:
      help_image_wn(ss,wn, indent);
      help_image_expr(ss,WN_kid(wn,0), indent+1);
      help_image_stmt(ss,WN_kid(wn,1), indent+1);
      if (WN_kid_count(wn) > 2)
	help_image_stmt(ss,WN_kid(wn,2), indent+1);
      image_marker(ss,"END_SWITCH", indent);
      already_dumped_wn = TRUE;
      break;

    case OPC_XGOTO:
      help_image_wn(ss,wn, indent);
      help_image_expr(ss,WN_kid(wn,0), indent+1);
      help_image_stmt(ss,WN_kid(wn,1), indent+1);
      image_marker(ss,"END_XGOTO", indent);
      already_dumped_wn = TRUE;
      break;

    case OPC_WHERE:
      help_image_expr(ss,WN_kid(wn,0), indent+1);
      image_marker(ss,"BODY", indent);
      help_image_stmt(ss,WN_kid(wn,1), indent+1);
      help_image_stmt(ss,WN_kid(wn,2), indent+1);
      break;

    case OPC_EXC_SCOPE_BEGIN:
      {
        INT i;
	for (i = 0; i < WN_kid_count(wn); i++)
	  help_image_stmt(ss,WN_kid(wn, i), indent+1);
	break;
      }
    case OPC_EXC_SCOPE_END:
     break;

    case OPC_ASM_STMT:
      help_image_wn(ss,wn, indent);
      already_dumped_wn = TRUE;
      help_image_stmt(ss,WN_kid(wn,0), indent+1);
      help_image_stmt(ss,WN_kid(wn,1), indent+1);
      image_marker(ss,"ASM_INPUTS", indent);
      {
	INT i;
	for (i = 2; i < WN_kid_count(wn); i++) {
	  help_image_expr(ss,WN_kid(wn,i), indent+1);
	}
      }
      image_marker(ss,"END_ASM_INPUTS", indent);
      break;

    default: 
      {
	INT last_is_expr = TRUE;
	OPCODE opc2;
	for (i = 0; i < WN_kid_count(wn); i++) {
	  wn2 = WN_kid(wn,i);
	  if (wn2) {
	    opc2 = WN_opcode(wn2);
	    if (opc2 == 0) {
	       fprintf(ir_ofile, "### error: WN opcode 0\n");
	    } else if (OPCODE_is_expression(opc2)) {
	      help_image_expr(ss,wn2, indent+1);
	      last_is_expr = 1;
	    }
	    else if (OPCODE_is_stmt(opc2) || OPCODE_is_scf(opc2)) {
	      if (last_is_expr) {
      		image_marker(ss,"BODY", indent);
		help_image_stmt(ss,wn2, indent+1);
	      } else
		help_image_stmt(ss,WN_kid(wn,i), indent+1);
	      last_is_expr = 0;
	    } else {
	       ss << "### error: unknown opcode type ";
               ss << opc2;
               ss << "\n";
	    }
	  } else
	    help_image_stmt(ss,wn2, indent+1);
	}
      }
    }
    if (!already_dumped_wn)
      help_image_wn(ss,wn, indent);
 } else
      help_image_wn(ss,wn, indent);
}


void
help_WN_TREE_image_stmt(stringstream& ss, WN *wn, int indent)
{
  INT i;
  WN * wn2;
  USRCPOS srcpos;

  Is_True(wn != 0, ("WN_TREE_image_stmt called with null whirl tree"));

  OPCODE opc = WN_opcode(wn);
  Is_True(OPCODE_is_scf(opc) || OPCODE_is_stmt(opc),
          ("WN_TREE_image_stmt invoked on non statement: opcode = %s", OPCODE_name(opc)));

  if (wn)
  { 
    BOOL already_dumped_wn = FALSE;
    if (OPCODE_is_scf(opc) || dump_parent_before_children) {
      help_image_wn(ss, wn, indent);
      already_dumped_wn = TRUE;
    }

    // create an iterator to go through the statements in a block;
    WN_TREE_ITER<PRE_ORDER> tree_iter(wn);

   switch (opc) {

      if (WN_first(wn)) {
          ++tree_iter; // get to the first kid of the block
#ifndef __GNU_BUG_WORKAROUND
        while (tree_iter != LAST_PRE_ORDER_ITER) {
#else
        while (tree_iter != WN_TREE_ITER<PRE_ORDER, WN*>()) {
#endif
          wn2 = tree_iter.Wn();
          help_WN_TREE_image_stmt(ss, wn2, indent);// traverse the tree under wn2
          tree_iter.Unwind(); // have already traversed the tree under wn2
                              // skip going down the tree again
        }
      }
      image_marker(ss,"END_BLOCK", indent);
      break;

    case OPC_REGION:
      image_marker(ss, "REGION EXITS", indent);
      help_WN_TREE_image_stmt(ss,WN_region_exits(wn), indent+1);

      image_marker(ss,"REGION PRAGMAS", indent);
      help_WN_TREE_image_stmt(ss, WN_region_pragmas(wn), indent+1);

      image_marker(ss, "REGION BODY", indent);
      help_WN_TREE_image_stmt(ss, WN_region_body(wn), indent+1);

      /* check to make sure cg.so is loaded first */
      /* IR_dump_region will be NULL if it is not */
#ifdef BACK_END
      if (IR_dump_region)
	CG_Dump_Region(ir_ofile, wn);
#endif /* BACK_END */
      { 
        char str[20];
	sprintf(str,"END_REGION %d", WN_region_id(wn));
        ss << "END_REGION ";
        ss << str; 
      }
      break;
      
    case OPC_LABEL:
      help_image_wn(ss, wn, indent);
      if ( WN_label_loop_info(wn) != NULL ) {
	help_WN_TREE_image_stmt(ss,WN_label_loop_info(wn), indent+1);
      }
      already_dumped_wn = TRUE;
      break;

    case OPC_IF:
      help_WN_TREE_image_expr(ss, WN_if_test(wn), indent+1);
      if (WN_then(wn)) {
	image_marker(ss, "THEN", indent);
	help_WN_TREE_image_stmt(ss,WN_then(wn), indent+1);
      }
      if (WN_else(wn)) {
	image_marker(ss,"ELSE", indent);
	help_WN_TREE_image_stmt(ss, WN_else(wn), indent+1);
      }
      image_marker(ss,"END_IF", indent);
      break;

    case OPC_DO_LOOP:
      help_WN_TREE_image_expr(ss,WN_index(wn), indent+1);
      image_marker(ss, "INIT",indent);
      help_WN_TREE_image_stmt(ss,WN_start(wn), indent+1);
      image_marker(ss,"COMP", indent);
      help_WN_TREE_image_expr(ss,WN_end(wn), indent+1);
      image_marker(ss,"INCR", indent);
      help_WN_TREE_image_stmt(ss,WN_step(wn), indent+1);
      /* optional loop_info */
      if ( WN_do_loop_info(wn) != NULL ) {
	help_WN_TREE_image_stmt(ss,WN_do_loop_info(wn), indent);
      }
      image_marker(ss,"BODY", indent);
      help_WN_TREE_image_stmt(ss,WN_do_body(wn), indent+1);
      break;

    case OPC_LOOP_INFO:
      help_image_wn(ss,wn, indent);
      if ( WN_loop_induction(wn) != NULL ) {
	help_WN_TREE_image_expr(ss,WN_loop_induction(wn), indent+1);
      }
      if ( WN_loop_trip(wn) != NULL ) {
	help_WN_TREE_image_expr(ss,WN_loop_trip(wn), indent+1);
      }
      image_marker(ss,"END_LOOP_INFO", indent);
      already_dumped_wn = TRUE;
      break;

    case OPC_COMPGOTO:
      help_image_wn(ss,wn, indent);
      help_WN_TREE_image_expr(ss,WN_kid(wn,0), indent+1);
      help_WN_TREE_image_stmt(ss,WN_kid(wn,1), indent+1);
      if (WN_kid_count(wn) > 2)
	help_WN_TREE_image_stmt(ss,WN_kid(wn,2), indent+1);
      image_marker(ss,"END_COMPGOTO", indent);
      already_dumped_wn = TRUE;
      break;

    case OPC_SWITCH:
      help_image_wn(ss,wn, indent);
      help_WN_TREE_image_expr(ss,WN_kid(wn,0), indent+1);
      help_WN_TREE_image_stmt(ss,WN_kid(wn,1), indent+1);
      if (WN_kid_count(wn) > 2)
	help_WN_TREE_image_stmt(ss,WN_kid(wn,2), indent+1);
      image_marker(ss,"END_SWITCH", indent);
      already_dumped_wn = TRUE;
      break;

    case OPC_XGOTO:
      help_image_wn(ss,wn, indent);
      help_WN_TREE_image_expr(ss,WN_kid(wn,0), indent+1);
      help_WN_TREE_image_stmt(ss,WN_kid(wn,1), indent+1);
      image_marker(ss,"END_XGOTO", indent);
      already_dumped_wn = TRUE;
      break;

    case OPC_WHERE:
      help_WN_TREE_image_expr(ss,WN_kid(wn,0), indent+1);
      image_marker(ss,"BODY", indent);
      help_WN_TREE_image_stmt(ss,WN_kid(wn,1), indent+1);
      help_WN_TREE_image_stmt(ss,WN_kid(wn,2), indent+1);
      break;

    case OPC_EXC_SCOPE_BEGIN:
      {
        INT i;
	for (i = 0; i < WN_kid_count(wn); i++)
	  help_WN_TREE_image_stmt(ss,WN_kid(wn, i), indent+1);
	break;
      }
    case OPC_EXC_SCOPE_END:
     break;
    default: 
      {
	INT last_is_expr = TRUE;
	OPCODE opc2;
	for (i = 0; i < WN_kid_count(wn); i++) {
	  wn2 = WN_kid(wn,i);
	  if (wn2) {
	    opc2 = WN_opcode(wn2);
	    if (opc2 == 0) {
	      ss << "### error: WN opcode 0\n";
	    } else if (OPCODE_is_expression(opc2)) {
	      help_WN_TREE_image_expr(ss,wn2, indent+1);
	      last_is_expr = 1;
	    }
	    else if (OPCODE_is_stmt(opc2) || OPCODE_is_scf(opc2)) {
	      if (last_is_expr) {
      		image_marker(ss,"BODY", indent);
		help_WN_TREE_image_stmt(ss,wn2, indent+1);
	      } else
		help_WN_TREE_image_stmt(ss,WN_kid(wn,i), indent+1);
	      last_is_expr = 0;
	    } else {
	      ss << "### error: unknown opcode type ";
              ss << opc2;
	    }
	  } else
	    help_WN_TREE_image_stmt(ss,wn2, indent+1);
	}
      }
   }
   if (!already_dumped_wn)
     help_image_wn(ss,wn, indent);
  }
}




void 
help_WN_TREE_image_expr(stringstream &ss, WN * wn, INT indent) {
  WN * wn2;
  WN * parent_wn;

  Is_True(OPCODE_is_expression(WN_opcode(wn)) || OPCODE_is_call(WN_opcode(wn)),
          ("WN_TREE_put_expr invoked with non-expression, opcode = %s",
           OPCODE_name(WN_opcode(wn))));

  // See if the parent op should be dumped before the children  (PRE_ORDER)
  if (dump_parent_before_children) {
    // create PRE_ORDER tree iterator; use stack of default size

    WN_TREE_ITER<PRE_ORDER> tree_iter(wn);

#ifndef __GNU_BUG_WORKAROUND
    while (tree_iter != LAST_PRE_ORDER_ITER) {
#else
    while (tree_iter != WN_TREE_ITER<PRE_ORDER, WN*>()) {
#endif
      wn2 = tree_iter.Wn();

      if (OPCODE_is_expression(WN_opcode(wn2)) ||
          OPCODE_is_call(WN_opcode(wn2)))
        help_image_wn(ss,wn2,indent+tree_iter.Depth());

      else if (WN_operator(wn2) == OPR_BLOCK) {
        // block under an expression had better be due to comma/rcomma
        parent_wn = tree_iter.Get_parent_wn();
        Is_True(parent_wn && 
                ((WN_operator(parent_wn) == OPR_COMMA) ||
                 (WN_operator(parent_wn) == OPR_RCOMMA)),
                ("block under expression not a part of comma/rcomma"));

        help_WN_TREE_image_stmt(ss,wn2, indent + tree_iter.Depth()); 
        tree_iter.Unwind();// have already traversed down the tree 
                           //  skip going down this tree again
      }

      else
        //fprintf(ir_ofile, "%*sopcode %d not an expression\n", 
        //        indent+1,"", WN_opcode(wn2));
      ++tree_iter;
    } // while
  } // if dump_parent_before_children
  else {
    // post order traversal of expressions falls back on ir_put_expr
    // this is so because expressions can contain statements (through comma)
    // and in post order, we need to stop going down at statement boundaries 
    // which is not currently possible with the generic post_order tree
    // iterator
    help_WN_TREE_image_expr(ss,wn,indent);
  } 
}


