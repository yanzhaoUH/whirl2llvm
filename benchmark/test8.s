	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test8.c (test8.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test8.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# firstNumber = 0
	# secondNumber = 8
	.loc	1	2	0
 #   1  #include <stdio.h>
 #   2  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_258:
	xorl %eax,%eax                	# [0] 
	.loc	1	6	0
 #   3  {
 #   4        double firstNumber, secondNumber, temporaryVariable;
 #   5  
 #   6        printf("Enter first number: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	7	0
 #   7        scanf("%lf", &firstNumber);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	11	0
 #   8  //firstNumber = 111.11;
 #   9  //secondNumber = 222.22;
 #  10  
 #  11        printf("Enter second number: ");
	movq $(.rodata+48),%rdi       	# [0] .rodata+48
	call printf                   	# [0] printf
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	12	0
 #  12        scanf("%lf",&secondNumber);
	leaq 8(%rsp),%rsi             	# [1] secondNumber
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	call scanf                    	# [1] scanf
.LBB6_main:
	.loc	1	15	0
 #  13  
 #  14        // Value of firstNumber is assigned to temporaryVariable
 #  15        temporaryVariable = firstNumber;
	movsd 0(%rsp),%xmm1           	# [0] firstNumber
	.loc	1	18	0
 #  16  
 #  17        // Value of secondNumber is assigned to firstNumber
 #  18        firstNumber = secondNumber;
	movsd 8(%rsp),%xmm0           	# [1] secondNumber
	.loc	1	23	0
 #  19  
 #  20        // Value of temporaryVariable (which contains the initial value of firstNumber) is assigned to secondNumber
 #  21        secondNumber = temporaryVariable;
 #  22  
 #  23        printf("\nAfter swapping, firstNumber = %.2lf\n", firstNumber);
	movl $1,%eax                  	# [4] 
	.loc	1	18	0
	movsd %xmm0,0(%rsp)           	# [4] firstNumber
	.loc	1	23	0
	movq $(.rodata+80),%rdi       	# [5] .rodata+80
	.loc	1	21	0
	movsd %xmm1,8(%rsp)           	# [5] secondNumber
	.loc	1	23	0
	call printf                   	# [5] printf
.LBB7_main:
	.loc	1	24	0
 #  24        printf("After swapping, secondNumber = %.2lf", secondNumber);
	movl $1,%eax                  	# [0] 
	movsd 8(%rsp),%xmm0           	# [1] secondNumber
	movq $(.rodata+128),%rdi      	# [1] .rodata+128
	call printf                   	# [1] printf
.LBB8_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	26	0
 #  25  
 #  26        return 0;
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.L_0_514:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter first number: "
	.org 0x20
	.align	0
	# offset 32
	.string "%lf"
	.org 0x30
	.align	0
	# offset 48
	.string "Enter second number: "
	.org 0x50
	.align	0
	# offset 80
	.byte	0xa, 0x41, 0x66, 0x74, 0x65, 0x72, 0x20, 0x73	# \nAfter s
	.byte	0x77, 0x61, 0x70, 0x70, 0x69, 0x6e, 0x67, 0x2c	# wapping,
	.byte	0x20, 0x66, 0x69, 0x72, 0x73, 0x74, 0x4e, 0x75	#  firstNu
	.byte	0x6d, 0x62, 0x65, 0x72, 0x20, 0x3d, 0x20, 0x25	# mber = %
	.byte	0x2e, 0x32, 0x6c, 0x66, 0xa, 0x0	# .2lf\n\000
	.org 0x80
	.align	0
	# offset 128
	.string "After swapping, secondNumber = %.2lf"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_258-main
	.uleb128	.L_0_514-.L_0_258
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test8.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

