	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test340.c (test340.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test340.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 40, %rsp
	# i = 0
	# _temp_gra_spill1 = 8
	# _temp_gra_spill2 = 16
	# _temp_gra_spill3 = 24
	.loc	1	3	0
 #   1  #include<stdio.h>
 #   2  
 #   3  int main(void){
.LBB1_main:
.LEH_adjustsp_main:
.LEH_csr_main:
	addq $-40,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
	movq %rbx,24(%rsp)            	# [0] _temp_gra_spill3
	movq %rbp,16(%rsp)            	# [0] _temp_gra_spill2
	movq %r12,8(%rsp)             	# [0] _temp_gra_spill1
.L_0_3074:
	xorl %eax,%eax                	# [0] 
	.loc	1	5	0
 #   4   int i;
 #   5   scanf("%d", &i);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata),%rdi          	# [1] .rodata
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB3_main:
	.loc	1	18	0
 #  14   
 #  15   printf("%d", j);
 #  16  
 #  17  */
 #  18  int j = i;
	movl 0(%rsp),%ebp             	# [0] i
	cmpl $9,%ebp                  	# [3] 
	movl %ebp,%ebx                	# [4] 
	jg .Lt_0_1282                 	# [4] 
.LBB4_main:
	leal -10(%rbp),%eax           	# [0] 
	negl %eax                     	# [2] 
	movl $1,%edi                  	# [3] 
	cmpl $1,%eax                  	# [3] 
	movl %edi,%r12d               	# [4] 
	cmovge %rax,%r12              	# [4] 
.Lt_0_1794:
 #<loop> Loop body line 18, nesting depth: 1, estimated iterations: 100
	xorl %eax,%eax                	# [0] 
	.loc	1	21	0
 #  19  
 #  20  while(j<10){
 #  21  printf("%d",j);
	movl %ebx,%esi                	# [1] 
	movq $(.rodata),%rdi          	# [1] .rodata
	.globl	printf
	call printf                   	# [1] printf
.LBB7_main:
 #<loop> Part of loop body line 18, head labeled .Lt_0_1794
	.loc	1	22	0
 #  22  j++;
	addl $1,%ebx                  	# [0] 
	cmpl $9,%ebx                  	# [1] 
	jle .Lt_0_1794                	# [2] 
.LBB15_main:
	leal 0(%r12,%rbp,1), %ebx     	# [0] 
.Lt_0_1282:
	.loc	1	26	0
 #  23  }
 #  24  
 #  25  j = j+10;
 #  26  if(j>100)
	leal 10(%rbx),%eax            	# [0] 
	cmpl $100,%eax                	# [2] 
	jg .LBB10_main                	# [3] 
.Lt_0_2562:
	xorq %rax,%rax                	# [0] 
	.loc	1	30	0
 #  27    printf("hello");
 #  28  
 #  29  
 #  30  return 0; 
	movq 24(%rsp),%rbx            	# [0] _temp_gra_spill3
	movq 16(%rsp),%rbp            	# [0] _temp_gra_spill2
	movq 8(%rsp),%r12             	# [1] _temp_gra_spill1
	addq $40,%rsp                 	# [1] 
	ret                           	# [1] 
.LBB10_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	27	0
	movq $(.rodata+16),%rdi       	# [0] .rodata+16
	call printf                   	# [0] printf
.LBB16_main:
	jmp .Lt_0_2562                	# [0] 
.L_0_3330:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "%d"
	.org 0x10
	.align	0
	# offset 16
	.string "hello"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_3074-main
	.uleb128	.L_0_3330-.L_0_3074
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x30, 0x04
	.4byte	.LEH_csr_main - .LEH_adjustsp_main
	.byte	0x8c, 0x05, 0x86, 0x04, 0x83, 0x03
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test340.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

