	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test66.c (test66.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test66.c"


	.text
	.align	2

	.section .bss, "wa",@nobits

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits

	.section .bss
	.org 0x0
	.align	4
.globl	s
	.type	s, @object
	.size	s, 600
s:	# 0x0
	.skip 600
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 56, %rsp
	# i = 0
	# _temp_gra_spill1 = 8
	# _temp_gra_spill2 = 16
	# _temp_gra_spill3 = 24
	# _temp_gra_spill4 = 32
	.loc	1	10	0
 #   6      int roll;
 #   7      float marks;
 #   8  } s[10];
 #   9  
 #  10  int main()
.LBB1_main:
.LEH_adjustsp_main:
.LEH_csr_main:
	addq $-56,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
	movq %rbx,32(%rsp)            	# [0] _temp_gra_spill4
	movq %rbp,16(%rsp)            	# [0] _temp_gra_spill2
	movq %r12,8(%rsp)             	# [0] _temp_gra_spill1
	movq %r13,24(%rsp)            	# [0] _temp_gra_spill3
.L_0_3330:
	xorl %eax,%eax                	# [0] 
	.loc	1	14	0
 #  11  {
 #  12      int i;
 #  13  
 #  14      printf("Enter information of students:\n");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %r13d,%r13d              	# [0] 
	movq $(.bss+56),%r12          	# [1] .bss+56
	movq $(.bss),%rbx             	# [1] .bss
	movl $1,%ebp                  	# [1] 
.Lt_0_1794:
 #<loop> Loop body line 14, nesting depth: 1, iterations: 2
	xorl %eax,%eax                	# [0] 
	.loc	1	21	0
 #  17      for(i=0; i<2; ++i)
 #  18      {
 #  19          s[i].roll = i+1;
 #  20  
 #  21          printf("\nFor roll number%d,\n",s[i].roll);
	movl %ebp,%esi                	# [0] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.loc	1	19	0
	movl %ebp,52(%rbx)            	# [1] id:23 s+0x0
	.loc	1	21	0
	call printf                   	# [1] printf
.LBB5_main:
 #<loop> Part of loop body line 14, head labeled .Lt_0_1794
	xorl %eax,%eax                	# [0] 
	.loc	1	23	0
 #  22  
 #  23          printf("Enter name: ");
	movq $(.rodata+64),%rdi       	# [0] .rodata+64
	call printf                   	# [0] printf
.LBB6_main:
 #<loop> Part of loop body line 14, head labeled .Lt_0_1794
	xorl %eax,%eax                	# [0] 
	.loc	1	24	0
 #  24          scanf("%s",s[i].name);
	movq %rbx,%rsi                	# [1] 
	movq $(.rodata+80),%rdi       	# [1] .rodata+80
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB7_main:
 #<loop> Part of loop body line 14, head labeled .Lt_0_1794
	xorl %eax,%eax                	# [0] 
	.loc	1	27	0
 #  25          //strcpy(s[i].name, "zhao"); 
 #  26  
 #  27          printf("Enter marks: ");
	movq $(.rodata+96),%rdi       	# [0] .rodata+96
	call printf                   	# [0] printf
.LBB8_main:
 #<loop> Part of loop body line 14, head labeled .Lt_0_1794
	xorl %eax,%eax                	# [0] 
	.loc	1	28	0
 #  28          scanf("%f",&s[i].marks);
	movq %r12,%rsi                	# [1] 
	movq $(.rodata+112),%rdi      	# [1] .rodata+112
	call scanf                    	# [1] scanf
.LBB9_main:
 #<loop> Part of loop body line 14, head labeled .Lt_0_1794
	xorl %eax,%eax                	# [0] 
	.loc	1	31	0
 #  29          //s[i].marks = 2.5f;
 #  30  
 #  31          printf("\n");
	movq $(.rodata+128),%rdi      	# [0] .rodata+128
	call printf                   	# [0] printf
.LBB10_main:
 #<loop> Part of loop body line 14, head labeled .Lt_0_1794
	addl $1,%ebp                  	# [0] 
	addl $1,%r13d                 	# [0] 
	addq $60,%r12                 	# [1] 
	addq $60,%rbx                 	# [1] 
	cmpl $1,%r13d                 	# [1] 
	jle .Lt_0_1794                	# [2] 
.LBB23_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	34	0
 #  32      }
 #  33  
 #  34      printf("Displaying Information:\n\n");
	movq $(.rodata+144),%rdi      	# [0] .rodata+144
	call printf                   	# [0] printf
.LBB12_main:
	xorl %r13d,%r13d              	# [0] 
	movq $(.bss),%rbx             	# [0] .bss
	movl $1,%ebp                  	# [0] 
.Lt_0_2818:
 #<loop> Loop body line 34, nesting depth: 1, iterations: 2
	xorl %eax,%eax                	# [0] 
	.loc	1	38	0
 #  35      // displaying information
 #  36      for(i=0; i<2; ++i)
 #  37      {
 #  38          printf("\nRoll number: %d\n",i+1);
	movl %ebp,%esi                	# [1] 
	movq $(.rodata+176),%rdi      	# [1] .rodata+176
	call printf                   	# [1] printf
.LBB14_main:
 #<loop> Part of loop body line 34, head labeled .Lt_0_2818
	xorl %eax,%eax                	# [0] 
	.loc	1	39	0
 #  39          printf("Name: ");
	movq $(.rodata+208),%rdi      	# [0] .rodata+208
	call printf                   	# [0] printf
.LBB15_main:
 #<loop> Part of loop body line 34, head labeled .Lt_0_2818
	.loc	1	40	0
 #  40          puts(s[i].name);
	movq %rbx,%rdi                	# [0] 
	.globl	puts
	call puts                     	# [0] puts
.LBB16_main:
 #<loop> Part of loop body line 34, head labeled .Lt_0_2818
	.loc	1	41	0
 #  41          printf("Marks: %.1f",s[i].marks);
	movss 56(%rbx),%xmm0          	# [0] id:24 s+0x0
	movl $1,%eax                  	# [2] 
	movq $(.rodata+224),%rdi      	# [3] .rodata+224
	cvtss2sd %xmm0,%xmm0          	# [3] 
	call printf                   	# [3] printf
.LBB17_main:
 #<loop> Part of loop body line 34, head labeled .Lt_0_2818
	xorl %eax,%eax                	# [0] 
	.loc	1	42	0
 #  42          printf("\n");
	movq $(.rodata+128),%rdi      	# [0] .rodata+128
	call printf                   	# [0] printf
.LBB18_main:
 #<loop> Part of loop body line 34, head labeled .Lt_0_2818
	addl $1,%r13d                 	# [0] 
	addl $1,%ebp                  	# [1] 
	addq $60,%rbx                 	# [1] 
	cmpl $1,%r13d                 	# [1] 
	jle .Lt_0_2818                	# [2] 
.LBB25_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	44	0
 #  43      }
 #  44      return 0;
	movq 32(%rsp),%rbx            	# [1] _temp_gra_spill4
	movq 16(%rsp),%rbp            	# [1] _temp_gra_spill2
	movq 8(%rsp),%r12             	# [1] _temp_gra_spill1
	movq 24(%rsp),%r13            	# [2] _temp_gra_spill3
	addq $56,%rsp                 	# [2] 
	ret                           	# [2] 
.L_0_3586:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.byte	0x45, 0x6e, 0x74, 0x65, 0x72, 0x20, 0x69, 0x6e	# Enter in
	.byte	0x66, 0x6f, 0x72, 0x6d, 0x61, 0x74, 0x69, 0x6f	# formatio
	.byte	0x6e, 0x20, 0x6f, 0x66, 0x20, 0x73, 0x74, 0x75	# n of stu
	.byte	0x64, 0x65, 0x6e, 0x74, 0x73, 0x3a, 0xa, 0x0	# dents:\n\000
	# 
	.org 0x20
	.align	0
	# offset 32
	.byte	0xa, 0x46, 0x6f, 0x72, 0x20, 0x72, 0x6f, 0x6c	# \nFor rol
	.byte	0x6c, 0x20, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72	# l number
	.byte	0x25, 0x64, 0x2c, 0xa, 0x0	# %d,\n\000
	.org 0x40
	.align	0
	# offset 64
	.string "Enter name: "
	.org 0x50
	.align	0
	# offset 80
	.string "%s"
	.org 0x60
	.align	0
	# offset 96
	.string "Enter marks: "
	.org 0x70
	.align	0
	# offset 112
	.string "%f"
	.org 0x80
	.align	0
	# offset 128
	.byte	0xa, 0x0	# \n\000
	.org 0x90
	.align	0
	# offset 144
	.byte	0x44, 0x69, 0x73, 0x70, 0x6c, 0x61, 0x79, 0x69	# Displayi
	.byte	0x6e, 0x67, 0x20, 0x49, 0x6e, 0x66, 0x6f, 0x72	# ng Infor
	.byte	0x6d, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x3a, 0xa	# mation:\n
	.byte	0xa, 0x0	# \n\000
	.org 0xb0
	.align	0
	# offset 176
	.byte	0xa, 0x52, 0x6f, 0x6c, 0x6c, 0x20, 0x6e, 0x75	# \nRoll nu
	.byte	0x6d, 0x62, 0x65, 0x72, 0x3a, 0x20, 0x25, 0x64	# mber: %d
	.byte	0xa, 0x0	# \n\000
	.org 0xd0
	.align	0
	# offset 208
	.string "Name: "
	.org 0xe0
	.align	0
	# offset 224
	.string "Marks: %.1f"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_3330-main
	.uleb128	.L_0_3586-.L_0_3330
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .bss
	.align	16
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x40, 0x04
	.4byte	.LEH_csr_main - .LEH_adjustsp_main
	.byte	0x8c, 0x07, 0x86, 0x06, 0x8d, 0x05, 0x83, 0x04
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test66.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

