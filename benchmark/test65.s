	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test65.c (test65.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test65.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 56, %rsp
	# startTime = 0
	# stopTime = 16
	# diff = 32
	.loc	1	10	0
 #   6    int hours;
 #   7  };
 #   8  void differenceBetweenTimePeriod(struct TIME t1, struct TIME t2, struct TIME *diff);
 #   9  
 #  10  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-56,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_258:
	xorl %eax,%eax                	# [0] 
	.loc	1	14	0
 #  11  {
 #  12      struct TIME startTime, stopTime, diff;
 #  13  
 #  14      printf("Enter start time: \n");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	15	0
 #  15      printf("Enter hours, minutes and seconds respectively: ");
	movq $(.rodata+32),%rdi       	# [0] .rodata+32
	call printf                   	# [0] printf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	16	0
 #  16      scanf("%d %d %d", &startTime.hours, &startTime.minutes, &startTime.seconds);
	movq %rsp,%rcx                	# [0] 
	leaq 4(%rsp),%rdx             	# [0] startTime+4
	leaq 8(%rsp),%rsi             	# [1] startTime+8
	movq $(.rodata+80),%rdi       	# [1] .rodata+80
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	21	0
 #  17  //startTime.hours = 9;
 #  18  //startTime.minutes = 30; 
 #  19  //startTime.seconds = 30;
 #  20  
 #  21      printf("Enter stop time: \n");
	movq $(.rodata+96),%rdi       	# [0] .rodata+96
	call printf                   	# [0] printf
.LBB6_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	22	0
 #  22      printf("Enter hours, minutes and seconds respectively: ");
	movq $(.rodata+32),%rdi       	# [0] .rodata+32
	call printf                   	# [0] printf
.LBB7_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	23	0
 #  23      scanf("%d %d %d", &stopTime.hours, &stopTime.minutes, &stopTime.seconds);
	leaq 16(%rsp),%rcx            	# [0] stopTime
	leaq 20(%rsp),%rdx            	# [0] stopTime+4
	leaq 24(%rsp),%rsi            	# [1] stopTime+8
	movq $(.rodata+80),%rdi       	# [1] .rodata+80
	call scanf                    	# [1] scanf
.LBB8_main:
	.loc	1	29	0
 #  25  //stopTime.minutes = 20; 
 #  26  //stopTime.seconds = 20;
 #  27  
 #  28      // Calculate the difference between the start and stop time period.
 #  29      differenceBetweenTimePeriod(startTime, stopTime, &diff);
	leaq 32(%rsp),%r8             	# [0] diff
	movl 24(%rsp),%ecx            	# [0] stopTime+8
	movq 16(%rsp),%rdx            	# [0] stopTime
	movl 8(%rsp),%esi             	# [1] startTime+8
	movq 0(%rsp),%rdi             	# [1] startTime
	call _Z27differenceBetweenTimePeriod4TIMES_PS_	# [1] _Z27differenceBetweenTimePeriod4TIMES_PS_
.LBB9_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	31	0
 #  30  
 #  31      printf("\nTIME DIFFERENCE: %d:%d:%d - ", startTime.hours, startTime.minutes, startTime.seconds);
	movl 0(%rsp),%ecx             	# [0] startTime
	movl 4(%rsp),%edx             	# [0] startTime+4
	movl 8(%rsp),%esi             	# [1] startTime+8
	movq $(.rodata+128),%rdi      	# [1] .rodata+128
	call printf                   	# [1] printf
.LBB10_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	32	0
 #  32      printf("%d:%d:%d ", stopTime.hours, stopTime.minutes, stopTime.seconds);
	movl 16(%rsp),%ecx            	# [0] stopTime
	movl 20(%rsp),%edx            	# [0] stopTime+4
	movl 24(%rsp),%esi            	# [1] stopTime+8
	movq $(.rodata+160),%rdi      	# [1] .rodata+160
	call printf                   	# [1] printf
.LBB11_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	33	0
 #  33      printf("= %d:%d:%d\n", diff.hours, diff.minutes, diff.seconds);
	movl 32(%rsp),%ecx            	# [0] diff
	movl 36(%rsp),%edx            	# [0] diff+4
	movl 40(%rsp),%esi            	# [1] diff+8
	movq $(.rodata+176),%rdi      	# [1] .rodata+176
	call printf                   	# [1] printf
.LBB12_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	35	0
 #  34  
 #  35      return 0;
	addq $56,%rsp                 	# [0] 
	ret                           	# [0] 
.L_0_514:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.byte	0x45, 0x6e, 0x74, 0x65, 0x72, 0x20, 0x73, 0x74	# Enter st
	.byte	0x61, 0x72, 0x74, 0x20, 0x74, 0x69, 0x6d, 0x65	# art time
	.byte	0x3a, 0x20, 0xa, 0x0	# : \n\000
	.org 0x20
	.align	0
	# offset 32
	.string "Enter hours, minutes and seconds respectively: "
	.org 0x50
	.align	0
	# offset 80
	.string "%d %d %d"
	.org 0x60
	.align	0
	# offset 96
	.byte	0x45, 0x6e, 0x74, 0x65, 0x72, 0x20, 0x73, 0x74	# Enter st
	.byte	0x6f, 0x70, 0x20, 0x74, 0x69, 0x6d, 0x65, 0x3a	# op time:
	.byte	0x20, 0xa, 0x0	#  \n\000
	.org 0x80
	.align	0
	# offset 128
	.byte	0xa, 0x54, 0x49, 0x4d, 0x45, 0x20, 0x44, 0x49	# \nTIME DI
	.byte	0x46, 0x46, 0x45, 0x52, 0x45, 0x4e, 0x43, 0x45	# FFERENCE
	.byte	0x3a, 0x20, 0x25, 0x64, 0x3a, 0x25, 0x64, 0x3a	# : %d:%d:
	.byte	0x25, 0x64, 0x20, 0x2d, 0x20, 0x0	# %d - \000
	.org 0xa0
	.align	0
	# offset 160
	.string "%d:%d:%d "
	.org 0xb0
	.align	0
	# offset 176
	.byte	0x3d, 0x20, 0x25, 0x64, 0x3a, 0x25, 0x64, 0x3a	# = %d:%d:
	.byte	0x25, 0x64, 0xa, 0x0	# %d\n\000

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_258-main
	.uleb128	.L_0_514-.L_0_258
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.p2align 5,,

	# Program Unit: _Z27differenceBetweenTimePeriod4TIMES_PS_
.globl	_Z27differenceBetweenTimePeriod4TIMES_PS_
	.type	_Z27differenceBetweenTimePeriod4TIMES_PS_, @function
_Z27differenceBetweenTimePeriod4TIMES_PS_:	# 0xd0
	# .frame	%rsp, 72, %rsp
	# _temp_gra_spill1 = 0
	.loc	1	38	0
 #  36  }
 #  37  
 #  38  void differenceBetweenTimePeriod(struct TIME start, struct TIME stop, struct TIME *diff)
.LBB1__Z27differenceBetweenTimePeriod4TIMES_PS_:
	addq $-72,%rsp                	# [0] 
	movq %rdx,32(%rsp)            	# [1] stop
	.loc	1	40	0
 #  39  {
 #  40      if(stop.seconds > start.seconds){
	movl 32(%rsp),%r11d           	# [1] stop
	.loc	1	38	0
	movq %rsi,%rsi                	# [3] 
	movl %edi,%eax                	# [3] 
	movq %rdi,%r10                	# [3] 
	movq %rbx,0(%rsp)             	# [4] _temp_gra_spill1
	shrq $32,%r10                 	# [4] 
	.loc	1	40	0
	cmpl %eax,%r11d               	# [4] 
	.loc	1	38	0
	movq %rcx,%rbx                	# [5] 
	movq %rdi,16(%rsp)            	# [5] start
	.loc	1	40	0
	jle .Lt_1_1794                	# [5] 
.LBB2__Z27differenceBetweenTimePeriod4TIMES_PS_:
	.loc	1	42	0
 #  41          --start.minutes;
 #  42          start.seconds += 60;
	addl $60,%eax                 	# [0] 
	decl %r10d                    	# [0] 
.Lt_1_1794:
	.loc	1	46	0
 #  43      }
 #  44  
 #  45      diff->seconds = start.seconds - stop.seconds;
 #  46      if(stop.minutes > start.minutes){
	movl 36(%rsp),%edx            	# [0] stop+4
	.loc	1	45	0
	movl %eax,%edi                	# [2] 
	subl %r11d,%edi               	# [3] 
	.loc	1	46	0
	cmpl %r10d,%edx               	# [3] 
	.loc	1	45	0
	movl %edi,0(%r8)              	# [4] id:38
	.loc	1	46	0
	jle .Lt_1_2050                	# [4] 
.LBB4__Z27differenceBetweenTimePeriod4TIMES_PS_:
	.loc	1	48	0
 #  47          --start.hours;
 #  48          start.minutes += 60;
	leal 60(%r10),%eax            	# [0] 
	.loc	1	47	0
	leal -1(%rsi),%edi            	# [1] 
	.loc	1	51	0
 #  49      }
 #  50  
 #  51      diff->minutes = start.minutes - stop.minutes;
	subl %edx,%eax                	# [2] 
	.loc	1	52	0
 #  52      diff->hours = start.hours - stop.hours;
	subl %ebx,%edi                	# [3] 
	movq 0(%rsp),%rbx             	# [3] _temp_gra_spill1
	.loc	1	51	0
	movl %eax,4(%r8)              	# [3] id:39
	.loc	1	52	0
	movl %edi,8(%r8)              	# [4] id:40
	addq $72,%rsp                 	# [4] 
	ret                           	# [4] 
.Lt_1_2050:
	.loc	1	51	0
	subl %edx,%r10d               	# [0] 
	.loc	1	52	0
	subl %ebx,%esi                	# [1] 
	movq 0(%rsp),%rbx             	# [1] _temp_gra_spill1
	.loc	1	51	0
	movl %r10d,4(%r8)             	# [1] id:39
	.loc	1	52	0
	movl %esi,8(%r8)              	# [2] id:40
	addq $72,%rsp                 	# [2] 
	ret                           	# [2] 
.LDWend__Z27differenceBetweenTimePeriod4TIMES_PS_:
	.size _Z27differenceBetweenTimePeriod4TIMES_PS_, .LDWend__Z27differenceBetweenTimePeriod4TIMES_PS_-_Z27differenceBetweenTimePeriod4TIMES_PS_
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x40
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test65.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

