; ModuleID = 'test10.bc'

@.str = private constant [20 x i8] c"Enter an alphabet: \00", align 1
@.str.1 = private constant [3 x i8] c"%c\00", align 1
@.str.2 = private constant [15 x i8] c"%c is a vowel.\00", align 1
@.str.3 = private constant [19 x i8] c"%c is a consonant.\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %_temp_ehpit0_11 = alloca i32, align 4
  %c_4 = alloca i8, align 1
  %isLowercaseVowel_5 = alloca i32, align 4
  %isUppercaseVowel_6 = alloca i32, align 4
  %old_frame_pointer_16.addr = alloca i64, align 8
  %return_address_17.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i8* %c_4 to i8*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i8* %2)
  %c_50 = alloca i32, align 4
  %3 = load i8, i8* %c_4, align 1
  %conv3 = sext i8 %3 to i32
  store i32 %conv3, i32* %c_50, align 8
  %4 = load i32, i32* %c_50, align 4
  %cmp1 = icmp eq i32 %4, 97
  br i1 %cmp1, label %L3074, label %fb

L3074:                                            ; preds = %tb, %entry
  br label %L3842

fb:                                               ; preds = %entry
  %5 = load i32, i32* %c_50, align 4
  %cmp2 = icmp eq i32 %5, 101
  br i1 %cmp2, label %tb, label %L2818

tb:                                               ; preds = %fb
  br label %L3074

L2818:                                            ; preds = %fb
  br label %L2306

L3842:                                            ; preds = %L3074
  %6 = bitcast i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i32 0, i32 0) to i8*
  %7 = load i32, i32* %c_50, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %6, i32 %7)
  br label %L2050

L2306:                                            ; preds = %L2818
  %8 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.3, i32 0, i32 0) to i8*
  %9 = load i32, i32* %c_50, align 4
  %call4 = call i32 (i8*, ...) @printf(i8* %8, i32 %9)
  br label %L2050

L2050:                                            ; preds = %L2306, %L3842
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
