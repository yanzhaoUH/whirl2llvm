	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test57.c (test57.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test57.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 168, %rsp
	# line = 0
	# i = 152
	.loc	1	3	0
 #   1  #include<stdio.h>
 #   2  #include <string.h>
 #   3  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-168,%rsp               	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_13570:
	xorl %eax,%eax                	# [0] 
	.loc	1	7	0
 #   4  {
 #   5      char line[150];
 #   6      int i, j;
 #   7      printf("Enter a string: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	.loc	1	8	0
 #   8      gets(line);
	movq %rsp,%rdi                	# [0] 
	.globl	gets
	call gets                     	# [0] gets
.LBB4_main:
	movsbl 0(%rsp),%esi           	# [0] line
	cmpl $0,%esi                  	# [3] 
	je .Lt_0_6914                 	# [4] 
.LBB5_main:
	xorl %r8d,%r8d                	# [0] 
	movq %rsp,%rcx                	# [0] 
	jmp .Lt_0_7426                	# [0] 
.L_0_4098:
 #<loop> Part of loop body line 13, head labeled .Lt_0_7426
	xorl %edx,%edx                	# [0] 
.Lt_0_9474:
 #<loop> Part of loop body line 13, head labeled .Lt_0_7426
	.loc	1	13	0
 #   9      //strcpy(line, "hello ?# world 123");
 #  10  
 #  11      for(i = 0; line[i] != '\0'; ++i)
 #  12      {
 #  13          while (!( (line[i] >= 'a' && line[i] <= 'z') || (line[i] >= 'A' && line[i] <= 'Z') || line[i] == '\0') )
	testl %edx,%edx               	# [0] 
	je .L_0_5634                  	# [1] 
.LBB14_main:
 #<loop> Part of loop body line 13, head labeled .Lt_0_7426
	testl %esi,%esi               	# [0] 
	setne %dl                     	# [1] 
	movzbl %dl,%edx               	# [2] 
.L_0_3330:
 #<loop> Loop body line 15
	.loc	1	15	0
 #  14          {
 #  15              for(j = i; line[j] != '\0'; ++j)
	testl %edx,%edx               	# [0] 
	movl %r8d,%esi                	# [1] 
	je .Lt_0_7682                 	# [1] 
.LBB16_main:
 #<loop> Part of loop body line 15, head labeled .L_0_3330
	movslq %r8d,%rax              	# [0] 
	leaq 0(%rsp,%rax,1), %rax     	# [1] line
.LBB32_main:
 #<loop> Loop body line 17
 #<loop> unrolled 3 times
	.loc	1	17	0
 #  16              {
 #  17                  line[j] = line[j+1];
	movsbl 1(%rax),%edx           	# [0] id:35 line+0x0
	addq $1,%rax                  	# [3] 
	addl $1,%esi                  	# [3] 
	cmpl $0,%edx                  	# [3] 
	movb %dl,-1(%rax)             	# [4] id:36 line+0x0
	je .Lt_0_7682                 	# [4] 
.LBB33_main:
 #<loop> Part of loop body line 17, head labeled .LBB32_main
 #<loop> unrolled 3 times
	movsbl 1(%rax),%edx           	# [0] id:35 line+0x0
	movb %dl,0(%rax)              	# [3] id:36 line+0x0
	incq %rax                     	# [3] 
	movsbl 0(%rax),%edi           	# [4] id:37 line+0x0
	addl $1,%esi                  	# [7] 
	cmpl $0,%edi                  	# [7] 
	je .Lt_0_7682                 	# [8] 
.Lt_0_8194:
 #<loop> Part of loop body line 17, head labeled .LBB32_main
	movsbl 1(%rax),%edx           	# [0] id:35 line+0x0
	movb %dl,0(%rax)              	# [3] id:36 line+0x0
	incq %rax                     	# [3] 
	movsbl 0(%rax),%edi           	# [4] id:37 line+0x0
	addl $1,%esi                  	# [7] 
	cmpl $0,%edi                  	# [7] 
	jne .LBB32_main               	# [8] 
.Lt_0_7682:
 #<loop> Part of loop body line 15, head labeled .L_0_3330
	.loc	1	19	0
 #  18              }
 #  19              line[j] = '\0';
	movslq %esi,%rdx              	# [0] 
	xorl %edi,%edi                	# [0] 
	movb %dil,0(%rsp,%rdx,1)      	# [1] line
	.loc	1	13	0
	movsbl 0(%rcx),%esi           	# [1] id:39 line+0x0
	leal -97(%rsi),%eax           	# [4] 
	cmpb $25,%al                  	# [6] 
	jbe .L_0_5634                 	# [7] 
.LBB19_main:
 #<loop> Part of loop body line 15, head labeled .L_0_3330
	leal -65(%rsi),%eax           	# [0] 
	cmpb $25,%al                  	# [2] 
	jbe .L_0_5634                 	# [3] 
.Lt_0_10754:
 #<loop> Part of loop body line 15, head labeled .L_0_3330
	testl %esi,%esi               	# [0] 
	setne %dl                     	# [1] 
	movzbl %dl,%edx               	# [2] 
	testl %edx,%edx               	# [3] 
	jne .L_0_3330                 	# [4] 
.L_0_5634:
.L_0_3586:
 #<loop> Part of loop body line 13, head labeled .Lt_0_7426
	.loc	1	19	0
	movsbl 1(%rcx),%esi           	# [0] id:40 line+0x0
	addq $1,%rcx                  	# [3] 
	addl $1,%r8d                  	# [3] 
	testl %esi,%esi               	# [3] 
	je .Lt_0_6914                 	# [4] 
.Lt_0_7426:
 #<loop> Loop body line 13
	.loc	1	13	0
	leal -97(%rsi),%eax           	# [0] 
	cmpb $25,%al                  	# [2] 
	jbe .L_0_4098                 	# [3] 
.LBB7_main:
 #<loop> Part of loop body line 13, head labeled .Lt_0_7426
	leal -65(%rsi),%eax           	# [0] 
	cmpb $25,%al                  	# [2] 
	jbe .L_0_4098                 	# [3] 
.Lt_0_9730:
 #<loop> Part of loop body line 13, head labeled .Lt_0_7426
	testl %esi,%esi               	# [0] 
	je .L_0_4098                  	# [1] 
.LBB11_main:
 #<loop> Part of loop body line 13, head labeled .Lt_0_7426
	movl $1,%edx                  	# [0] 
	jmp .Lt_0_9474                	# [0] 
.Lt_0_6914:
	xorl %eax,%eax                	# [0] 
	.loc	1	22	0
 #  20          }
 #  21      }
 #  22      printf("Output String: ");
	movq $(.rodata+32),%rdi       	# [0] .rodata+32
	call printf                   	# [0] printf
.LBB26_main:
	.loc	1	23	0
 #  23      puts(line);
	movq %rsp,%rdi                	# [0] 
	.globl	puts
	call puts                     	# [0] puts
.LBB27_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	24	0
 #  24      return 0;
	addq $168,%rsp                	# [0] 
	ret                           	# [0] 
.L_0_13826:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter a string: "
	.org 0x20
	.align	0
	# offset 32
	.string "Output String: "

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_13570-main
	.uleb128	.L_0_13826-.L_0_13570
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0xb0, 0x01
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test57.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

