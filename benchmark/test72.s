	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test72.c (test72.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test72.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 40, %rsp
	# i = 4
	# num = 0
	# odd_sum = 12
	# even_sum = 8
	# _temp_gra_spill1 = 16
	.loc	1	10	0
 #   6  
 #   7      #include <stdio.h>
 #   8  
 #   9       
 #  10      int main()
.LBB1_main:
.LEH_adjustsp_main:
.LEH_csr_main:
	addq $-40,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
	movq %rbx,16(%rsp)            	# [0] _temp_gra_spill1
.L_0_3074:
	xorl %eax,%eax                	# [0] 
	.loc	1	18	0
 #  14          int i, num, odd_sum = 0, even_sum = 0;
 #  15  
 #  16       
 #  17  
 #  18          printf("Enter the value of num\n");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	20	0
 #  19  
 #  20          scanf("%d", &num);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	movl 0(%rsp),%edx             	# [0] num
	cmpl $1,%edx                  	# [3] 
	jl .Lt_0_2818                 	# [4] 
.LBB5_main:
	movl $1,%eax                  	# [0] 
	xorl %ebx,%ebx                	# [1] 
	xorl %esi,%esi                	# [1] 
	jmp .Lt_0_1794                	# [1] 
.LBB8_main:
 #<loop> Part of loop body line 20, head labeled .Lt_0_1794
	.loc	1	28	0
 #  24          {
 #  25  
 #  26              if (i % 2 == 0)
 #  27  
 #  28                  even_sum = even_sum + i;
	addl %eax,%ebx                	# [0] 
.Lt_0_2562:
 #<loop> Part of loop body line 20, head labeled .Lt_0_1794
	.loc	1	32	0
 #  29  
 #  30              else
 #  31  
 #  32                  odd_sum = odd_sum + i;
	addl $1,%eax                  	# [0] 
	cmpl %eax,%edx                	# [1] 
	jl .Lt_0_1282                 	# [2] 
.Lt_0_1794:
 #<loop> Loop body line 20, nesting depth: 1, estimated iterations: 100
	.loc	1	26	0
	testl $1,%eax                 	# [0] 
	je .LBB8_main                 	# [1] 
.Lt_0_2050:
 #<loop> Part of loop body line 20, head labeled .Lt_0_1794
	.loc	1	32	0
	addl %eax,%esi                	# [0] 
	jmp .Lt_0_2562                	# [0] 
.Lt_0_1282:
	xorl %eax,%eax                	# [0] 
	.loc	1	36	0
 #  33  
 #  34          }
 #  35  
 #  36          printf("Sum of all odd numbers  = %d\n", odd_sum);
	movl %esi,%esi                	# [1] 
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call printf                   	# [1] printf
.LBB14_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	38	0
 #  37  
 #  38          printf("Sum of all even numbers = %d\n", even_sum);
	movl %ebx,%esi                	# [1] 
	movq $(.rodata+80),%rdi       	# [1] .rodata+80
	call printf                   	# [1] printf
.LBB15_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	39	0
 #  39       return 0;
	movq 16(%rsp),%rbx            	# [1] _temp_gra_spill1
	addq $40,%rsp                 	# [1] 
	ret                           	# [1] 
.Lt_0_2818:
	xorl %esi,%esi                	# [0] 
	xorl %ebx,%ebx                	# [0] 
	jmp .Lt_0_1282                	# [0] 
.L_0_3330:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.byte	0x45, 0x6e, 0x74, 0x65, 0x72, 0x20, 0x74, 0x68	# Enter th
	.byte	0x65, 0x20, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x20	# e value 
	.byte	0x6f, 0x66, 0x20, 0x6e, 0x75, 0x6d, 0xa, 0x0	# of num\n\000
	# 
	.org 0x20
	.align	0
	# offset 32
	.string "%d"
	.org 0x30
	.align	0
	# offset 48
	.byte	0x53, 0x75, 0x6d, 0x20, 0x6f, 0x66, 0x20, 0x61	# Sum of a
	.byte	0x6c, 0x6c, 0x20, 0x6f, 0x64, 0x64, 0x20, 0x6e	# ll odd n
	.byte	0x75, 0x6d, 0x62, 0x65, 0x72, 0x73, 0x20, 0x20	# umbers  
	.byte	0x3d, 0x20, 0x25, 0x64, 0xa, 0x0	# = %d\n\000
	.org 0x50
	.align	0
	# offset 80
	.byte	0x53, 0x75, 0x6d, 0x20, 0x6f, 0x66, 0x20, 0x61	# Sum of a
	.byte	0x6c, 0x6c, 0x20, 0x65, 0x76, 0x65, 0x6e, 0x20	# ll even 
	.byte	0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x73, 0x20	# numbers 
	.byte	0x3d, 0x20, 0x25, 0x64, 0xa, 0x0	# = %d\n\000

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_3074-main
	.uleb128	.L_0_3330-.L_0_3074
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x30, 0x04
	.4byte	.LEH_csr_main - .LEH_adjustsp_main
	.byte	0x83, 0x04
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test72.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

