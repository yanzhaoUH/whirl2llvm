	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test30.c (test30.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test30.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 56, %rsp
	# n1 = 0
	# n2 = 4
	# n = 8
	# result = 12
	# _temp_gra_spill1 = 16
	# _temp_gra_spill2 = 24
	# _temp_gra_spill3 = 32
	# _temp_gra_spill4 = 40
	.loc	1	4	0
 #   1  #include <stdio.h>
 #   2  #include <math.h>
 #   3  
 #   4  int main()
.LBB1_main:
.LEH_adjustsp_main:
.LEH_csr_main:
	addq $-56,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
	movq %rbx,32(%rsp)            	# [0] _temp_gra_spill3
	movq %rbp,24(%rsp)            	# [0] _temp_gra_spill2
	movq %r12,16(%rsp)            	# [0] _temp_gra_spill1
.L_0_6914:
	xorl %eax,%eax                	# [0] 
	.loc	1	8	0
 #   5  {
 #   6      int n1, n2, i, temp1, temp2, remainder, n = 0, result = 0;
 #   7  
 #   8      printf("Enter two numbers(intervals): ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	9	0
 #   9      scanf("%d %d", &n1, &n2);
	leaq 4(%rsp),%rdx             	# [0] n2
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	13	0
 #  10  
 #  11      //n1 = 999;
 #  12      //n2 = 99999;
 #  13      printf("Armstrong numbers between %d an %d are: ", n1, n2);
	movl 4(%rsp),%edx             	# [0] n2
	movl 0(%rsp),%esi             	# [1] n1
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call printf                   	# [1] printf
.LBB5_main:
	.loc	1	15	0
 #  14  
 #  15      for(i = n1+1; i<n2; ++i)
	movl 0(%rsp),%r12d            	# [0] n1
	movl 4(%rsp),%eax             	# [1] n2
	incl %r12d                    	# [3] 
	cmpl %r12d,%eax               	# [4] 
	jg .Lt_0_2818                 	# [5] 
.Lt_0_2306:
	xorq %rax,%rax                	# [0] 
	.loc	1	45	0
 #  41          n = 0;
 #  42          result = 0;
 #  43  
 #  44      }
 #  45      return 0;
	movq 32(%rsp),%rbx            	# [0] _temp_gra_spill3
	movq 24(%rsp),%rbp            	# [0] _temp_gra_spill2
	movq 16(%rsp),%r12            	# [1] _temp_gra_spill1
	addq $56,%rsp                 	# [1] 
	ret                           	# [1] 
.Lt_0_5634:
 #<loop> Part of loop body line 18, head labeled .Lt_0_2818
	.loc	1	42	0
	movl 4(%rsp),%eax             	# [0] n2
	addl $1,%r12d                 	# [2] 
	cmpl %r12d,%eax               	# [3] 
	jle .Lt_0_2306                	# [4] 
.Lt_0_2818:
 #<loop> Loop body line 18
	.loc	1	18	0
	testl %r12d,%r12d             	# [0] 
	movl %r12d,%eax               	# [1] 
	.loc	1	17	0
	movl %r12d,%ecx               	# [1] 
	.loc	1	18	0
	je .Lt_0_6658                 	# [1] 
.LBB7_main:
 #<loop> Part of loop body line 18, head labeled .Lt_0_2818
	xorl %edx,%edx                	# [0] 
.LBB26_main:
 #<loop> Loop body line 23
 #<loop> unrolled 3 times
	.loc	1	23	0
	movslq %eax,%rsi              	# [0] 
	imulq $1717986919,%rsi        	# [1] 
	shrq $32,%rsi                 	# [6] 
	movl %eax,%edi                	# [7] 
	movq %rsi,%rax                	# [7] 
	sarl $31,%edi                 	# [8] 
	sarl $2,%eax                  	# [8] 
	subl %edi,%eax                	# [9] 
	.loc	1	24	0
	addl $1,%edx                  	# [10] 
	testl %eax,%eax               	# [10] 
	je .Lt_0_3074                 	# [11] 
.LBB27_main:
 #<loop> Part of loop body line 23, head labeled .LBB26_main
 #<loop> unrolled 3 times
	.loc	1	23	0
	movslq %eax,%rsi              	# [0] 
	imulq $1717986919,%rsi        	# [1] 
	shrq $32,%rsi                 	# [6] 
	movl %eax,%edi                	# [7] 
	movq %rsi,%rax                	# [7] 
	sarl $31,%edi                 	# [8] 
	sarl $2,%eax                  	# [8] 
	subl %edi,%eax                	# [9] 
	.loc	1	24	0
	addl $1,%edx                  	# [10] 
	testl %eax,%eax               	# [10] 
	je .Lt_0_3074                 	# [11] 
.Lt_0_3586:
 #<loop> Part of loop body line 23, head labeled .LBB26_main
	.loc	1	23	0
	movslq %eax,%rsi              	# [0] 
	imulq $1717986919,%rsi        	# [1] 
	shrq $32,%rsi                 	# [6] 
	movl %eax,%edi                	# [7] 
	movq %rsi,%rax                	# [7] 
	sarl $31,%edi                 	# [8] 
	sarl $2,%eax                  	# [8] 
	subl %edi,%eax                	# [9] 
	.loc	1	24	0
	addl $1,%edx                  	# [10] 
	testl %eax,%eax               	# [10] 
	jne .LBB26_main               	# [11] 
.Lt_0_3074:
 #<loop> Part of loop body line 18, head labeled .Lt_0_2818
	testl %r12d,%r12d             	# [0] 
	je .Lt_0_6146                 	# [1] 
.LBB12_main:
 #<loop> Part of loop body line 18, head labeled .Lt_0_2818
	cvtsi2sd %edx,%xmm0           	# [0] 
	xorl %ebp,%ebp                	# [4] 
	movsd %xmm0,40(%rsp)          	# [4] _temp_gra_spill4
.Lt_0_4610:
 #<loop> Loop body line 31
	.loc	1	31	0
	movslq %ecx,%rbx              	# [0] 
	imulq $1717986919,%rbx        	# [1] 
	shrq $32,%rbx                 	# [6] 
	movl %ecx,%edi                	# [7] 
	movq %rbx,%rbx                	# [7] 
	sarl $31,%edi                 	# [8] 
	sarl $2,%ebx                  	# [8] 
	subl %edi,%ebx                	# [9] 
	movl %ebx,%eax                	# [10] 
	imull $10,%eax                	# [11] 
	subl %eax,%ecx                	# [14] 
	movsd 40(%rsp),%xmm1          	# [15] _temp_gra_spill4
	cvtsi2sd %ecx,%xmm0           	# [15] 
	.globl	pow
	call pow                      	# [15] pow
.LBB14_main:
 #<loop> Part of loop body line 31, head labeled .Lt_0_4610
	cvtsi2sd %ebp,%xmm1           	# [0] 
	addsd %xmm1,%xmm0             	# [4] 
	.loc	1	32	0
	testl %ebx,%ebx               	# [6] 
	movl %ebx,%ecx                	# [7] 
	.loc	1	31	0
	cvttsd2si %xmm0,%ebp          	# [7] 
	.loc	1	32	0
	jne .Lt_0_4610                	# [7] 
.LBB15_main:
 #<loop> Part of loop body line 18, head labeled .Lt_0_2818
	.loc	1	36	0
	cmpl %ebp,%r12d               	# [0] 
	jne .Lt_0_5634                	# [1] 
.Lt_0_6146:
 #<loop> Part of loop body line 18, head labeled .Lt_0_2818
	xorl %eax,%eax                	# [0] 
	.loc	1	37	0
	movl %r12d,%esi               	# [1] 
	movq $(.rodata+96),%rdi       	# [1] .rodata+96
	call printf                   	# [1] printf
.LBB28_main:
 #<loop> Part of loop body line 18, head labeled .Lt_0_2818
	jmp .Lt_0_5634                	# [0] 
.Lt_0_6658:
 #<loop> Part of loop body line 18, head labeled .Lt_0_2818
	xorl %edx,%edx                	# [0] 
	jmp .Lt_0_3074                	# [0] 
.L_0_7170:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter two numbers(intervals): "
	.org 0x20
	.align	0
	# offset 32
	.string "%d %d"
	.org 0x30
	.align	0
	# offset 48
	.string "Armstrong numbers between %d an %d are: "
	.org 0x60
	.align	0
	# offset 96
	.string "%d "

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_6914-main
	.uleb128	.L_0_7170-.L_0_6914
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x40, 0x04
	.4byte	.LEH_csr_main - .LEH_adjustsp_main
	.byte	0x8c, 0x06, 0x86, 0x05, 0x83, 0x04
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test30.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

