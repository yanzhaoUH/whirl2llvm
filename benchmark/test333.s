	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test333.c (test333.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test333.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# a = 4
	# i = 0
	.loc	1	10	0
 #   6  {
 #   7   return x+y;
 #   8  }
 #   9  
 #  10  int main(void)
.LBB1_main:
.LEH_adjustsp_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_6914:
	movl $1,%eax                  	# [0] 
	xorl %esi,%esi                	# [0] 
.LBB12_main:
 #<loop> Loop body line 10, nesting depth: 1, iterations: 1
 #<loop> unrolled 5 times (fully)
	.loc	1	28	0
 #  24    }
 #  25  
 #  26    for(int i = 0;i<5;i++)
 #  27    {
 #  28  	  a += i;
	leal 1(%rsi),%edi             	# [0] 
	addl %esi,%eax                	# [1] 
	addl %edi,%eax                	# [2] 
	incl %edi                     	# [2] 
	addl %edi,%eax                	# [3] 
	incl %edi                     	# [3] 
	addl %edi,%eax                	# [4] 
	incl %edi                     	# [4] 
	addl %edi,%eax                	# [5] 
.LBB11_main:
	.loc	1	40	0
 #  36    do{
 #  37  	  a+=1;
 #  38    }while(a<20);
 #  39  
 #  40     int sum = Sum(a,b);
	leal -20(%rax),%edx           	# [0] 
	negl %edx                     	# [2] 
	cmpl $1,%edx                  	# [3] 
	movl $1,%edi                  	# [4] 
	cmovge %rdx,%rdi              	# [4] 
	movq $15,%rsi                 	# [5] 
	addl %eax,%edi                	# [5] 
	call _Z3Sumii                 	# [5] _Z3Sumii
.LBB5_main:
	movl %eax,%esi                	# [0] 
	.loc	1	41	0
 #  41     if(sum >0){
	testl %esi,%esi               	# [1] 
	jle .Lt_0_6402                	# [2] 
.LBB6_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	42	0
 #  42  		printf("%d", sum);
	movl %esi,%esi                	# [1] 
	movq $(.rodata),%rdi          	# [1] .rodata
	.globl	printf
	call printf                   	# [1] printf
.LBB17_main:
.Lt_0_6402:
	xorq %rax,%rax                	# [0] 
	.loc	1	0	0
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.L_0_7170:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "%d"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_6914-main
	.uleb128	.L_0_7170-.L_0_6914
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.p2align 5,,

	# Program Unit: _Z3Sumii
.globl	_Z3Sumii
	.type	_Z3Sumii, @function
_Z3Sumii:	# 0x78
	# .frame	%rsp, 24, %rsp
	.loc	1	5	0
.LBB1__Z3Sumii:
	.loc	1	7	0
	leal 0(%rdi,%rsi,1), %eax     	# [0] 
	ret                           	# [0] 
.LDWend__Z3Sumii:
	.size _Z3Sumii, .LDWend__Z3Sumii-_Z3Sumii
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test333.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

