; ModuleID = 'test35.bc'

@.str = private constant [27 x i8] c"Enter a positive integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [23 x i8] c"%d is a prime number.\0A\00", align 1
@.str.3 = private constant [27 x i8] c"%d is not a prime number.\0A\00", align 1
@.str.4 = private constant [27 x i8] c"%d is an Armstrong number.\00", align 1
@.str.5 = private constant [31 x i8] c"%d is not an Armstrong number.\00", align 1
@_LIB_VERSION_62 = common global i32 0, align 4
@signgam_63 = common global i32 0, align 4

define i32 @main() {
entry:
  %.preg_F8_11_51 = alloca double
  %.preg_I4_4_57 = alloca i32
  %.preg_I4_4_66 = alloca i32
  %.preg_F8_11_58 = alloca double
  %.preg_I4_4_54 = alloca i32
  %.preg_I4_4_63 = alloca i32
  %.preg_I4_4_61 = alloca i32
  %.preg_I4_4_59 = alloca i32
  %.preg_I4_4_68 = alloca i32
  %.preg_I4_4_60 = alloca i32
  %.preg_I4_4_56 = alloca i32
  %.preg_I4_4_67 = alloca i32
  %.preg_I4_4_52 = alloca i32
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %_temp_dummy13_9 = alloca i32, align 4
  %_temp_dummy14_10 = alloca i32, align 4
  %_temp_dummy15_11 = alloca i32, align 4
  %_temp_ehpit0_29 = alloca i32, align 4
  %flag_20 = alloca i32, align 4
  %flag_27 = alloca i32, align 4
  %flag_5 = alloca i32, align 4
  %i_26 = alloca i32, align 4
  %n_19 = alloca i32, align 4
  %n_25.addr = alloca i32, align 4
  %n_28 = alloca i32, align 4
  %n_4 = alloca i32, align 4
  %number_15.addr = alloca i32, align 4
  %number_21 = alloca i32, align 4
  %old_frame_pointer_34.addr = alloca i64, align 8
  %originalNumber_16 = alloca i32, align 4
  %remainder_17 = alloca i32, align 4
  %result_18 = alloca i32, align 4
  %return_address_35.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  store i32 0, i32* %.preg_I4_4_52, align 8
  %3 = load i32, i32* %n_4, align 4
  store i32 %3, i32* %.preg_I4_4_67, align 8
  %4 = load i32, i32* %.preg_I4_4_67
  %div = sdiv i32 %4, 2
  store i32 %div, i32* %.preg_I4_4_56, align 8
  %5 = load i32, i32* %.preg_I4_4_56
  %add = add i32 %5, -2
  store i32 %add, i32* %.preg_I4_4_60, align 8
  %6 = load i32, i32* %.preg_I4_4_60
  %cmp1 = icmp sge i32 %6, 0
  br i1 %cmp1, label %tb, label %L9218

tb:                                               ; preds = %entry
  %7 = load i32, i32* %.preg_I4_4_56
  %add.1 = add i32 %7, -1
  store i32 %add.1, i32* %.preg_I4_4_68, align 8
  store i32 2, i32* %.preg_I4_4_59, align 8
  br label %L9730

L9218:                                            ; preds = %fb, %entry
  %8 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.2, i32 0, i32 0) to i8*
  %9 = load i32, i32* %.preg_I4_4_67
  %call3 = call i32 (i8*, ...) @printf(i8* %8, i32 %9)
  br label %L13570

L9730:                                            ; preds = %L9986, %tb
  %10 = load i32, i32* %.preg_I4_4_67
  %11 = load i32, i32* %.preg_I4_4_59
  %srem = srem i32 %10, %11
  %cmp2 = icmp eq i32 %srem, 0
  br i1 %cmp2, label %tb2, label %L9986

tb2:                                              ; preds = %L9730
  br label %L10498

L9986:                                            ; preds = %L9730
  %12 = load i32, i32* %.preg_I4_4_52
  %add.3 = add i32 %12, 1
  store i32 %add.3, i32* %.preg_I4_4_52, align 8
  %13 = load i32, i32* %.preg_I4_4_59
  %add.4 = add i32 %13, 1
  store i32 %add.4, i32* %.preg_I4_4_59, align 8
  %14 = load i32, i32* %.preg_I4_4_52
  %15 = load i32, i32* %.preg_I4_4_60
  %cmp3 = icmp sle i32 %14, %15
  br i1 %cmp3, label %L9730, label %fb

L10498:                                           ; preds = %tb2
  %16 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.3, i32 0, i32 0) to i8*
  %17 = load i32, i32* %.preg_I4_4_67
  %call4 = call i32 (i8*, ...) @printf(i8* %16, i32 %17)
  br label %L13570

fb:                                               ; preds = %L9986
  br label %L9218

L13570:                                           ; preds = %L10498, %L9218
  %18 = load i32, i32* %n_4, align 4
  store i32 %18, i32* %.preg_I4_4_67, align 8
  %19 = load i32, i32* %.preg_I4_4_67
  store i32 %19, i32* %.preg_I4_4_61, align 8
  %20 = load i32, i32* %.preg_I4_4_61
  store i32 %20, i32* %.preg_I4_4_63, align 8
  store i32 0, i32* %.preg_I4_4_54, align 8
  %21 = load i32, i32* %.preg_I4_4_61
  %cmp4 = icmp ne i32 %21, 0
  br i1 %cmp4, label %tb5, label %L10754

tb5:                                              ; preds = %L13570
  br label %L11266

L10754:                                           ; preds = %fb8, %L13570
  %22 = load i32, i32* %.preg_I4_4_61
  store i32 %22, i32* %.preg_I4_4_63, align 8
  %23 = load i32, i32* %.preg_I4_4_61
  %cmp6 = icmp ne i32 %23, 0
  br i1 %cmp6, label %tb9, label %L14850

L11266:                                           ; preds = %L11266, %tb5
  %24 = load i32, i32* %.preg_I4_4_63
  %div.6 = sdiv i32 %24, 10
  store i32 %div.6, i32* %.preg_I4_4_63, align 8
  %25 = load i32, i32* %.preg_I4_4_54
  %add.7 = add i32 %25, 1
  store i32 %add.7, i32* %.preg_I4_4_54, align 8
  %26 = load i32, i32* %.preg_I4_4_63
  %cmp5 = icmp ne i32 %26, 0
  br i1 %cmp5, label %L11266, label %fb8

fb8:                                              ; preds = %L11266
  br label %L10754

tb9:                                              ; preds = %L10754
  %27 = load i32, i32* %.preg_I4_4_54
  %conv9 = sitofp i32 %27 to double
  store double %conv9, double* %.preg_F8_11_58, align 8
  store i32 0, i32* %.preg_I4_4_66, align 8
  br label %L12290

L14850:                                           ; preds = %L10754
  br label %L15106

L12290:                                           ; preds = %L12290, %tb9
  %28 = load i32, i32* %.preg_I4_4_63
  %lowPart_4 = alloca i32, align 4
  %div.10 = sdiv i32 %28, 10
  %srem.11 = srem i32 %28, 10
  store i32 %div.10, i32* %lowPart_4, align 4
  store i32 %srem.11, i32* %.preg_I4_4_57, align 8
  %29 = load i32, i32* %.preg_I4_4_57
  %conv912 = sitofp i32 %29 to double
  %30 = load double, double* %.preg_F8_11_58
  %call5 = call double @pow(double %conv912, double %30)
  store double %call5, double* %.preg_F8_11_51, align 8
  %31 = load i32, i32* %.preg_I4_4_66
  %conv913 = sitofp i32 %31 to double
  %32 = load double, double* %.preg_F8_11_51
  %add.14 = fadd double %conv913, %32
  %fp2int = fptosi double %add.14 to i32
  store i32 %fp2int, i32* %.preg_I4_4_66, align 8
  %33 = load i32, i32* %lowPart_4
  store i32 %33, i32* %.preg_I4_4_63, align 8
  %34 = load i32, i32* %.preg_I4_4_63
  %cmp7 = icmp ne i32 %34, 0
  br i1 %cmp7, label %L12290, label %fb15

fb15:                                             ; preds = %L12290
  %35 = load i32, i32* %n_4, align 4
  store i32 %35, i32* %.preg_I4_4_67, align 8
  %36 = load i32, i32* %.preg_I4_4_66
  %37 = load i32, i32* %.preg_I4_4_61
  %cmp8 = icmp eq i32 %36, %37
  br i1 %cmp8, label %tb16, label %L12802

tb16:                                             ; preds = %fb15
  br label %L15106

L12802:                                           ; preds = %fb15
  %38 = bitcast i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.5, i32 0, i32 0) to i8*
  %39 = load i32, i32* %.preg_I4_4_67
  %call7 = call i32 (i8*, ...) @printf(i8* %38, i32 %39)
  br label %L13826

L15106:                                           ; preds = %tb16, %L14850
  %40 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.4, i32 0, i32 0) to i8*
  %41 = load i32, i32* %.preg_I4_4_67
  %call6 = call i32 (i8*, ...) @printf(i8* %40, i32 %41)
  br label %L13826

L13826:                                           ; preds = %L15106, %L12802
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define i32 @_Z16checkPrimeNumberi(i32 %n_4) {
entry:
  %.preg_I4_4_55 = alloca i32
  %.preg_I4_4_52 = alloca i32
  %.preg_I4_4_56 = alloca i32
  %.preg_I4_4_53 = alloca i32
  %.preg_I4_4_51 = alloca i32
  %.preg_I4_4_49 = alloca i32
  %.preg_I4_4_54 = alloca i32
  %flag_6 = alloca i32, align 4
  %i_5 = alloca i32, align 4
  %n_4.addr = alloca i32, align 4
  %old_frame_pointer_11.addr = alloca i64, align 8
  %return_address_12.addr = alloca i64, align 8
  store i32 %n_4, i32* %n_4.addr, align 4
  store i32 %n_4, i32* %.preg_I4_4_54, align 8
  store i32 0, i32* %.preg_I4_4_49, align 8
  %0 = load i32, i32* %.preg_I4_4_54
  %div = sdiv i32 %0, 2
  store i32 %div, i32* %.preg_I4_4_51, align 8
  %1 = load i32, i32* %.preg_I4_4_51
  %add = add i32 %1, -2
  store i32 %add, i32* %.preg_I4_4_53, align 8
  %2 = load i32, i32* %.preg_I4_4_53
  %cmp14 = icmp sge i32 %2, 0
  br i1 %cmp14, label %tb, label %L3074

tb:                                               ; preds = %entry
  %3 = load i32, i32* %.preg_I4_4_51
  %add.1 = add i32 %3, -1
  store i32 %add.1, i32* %.preg_I4_4_56, align 8
  store i32 2, i32* %.preg_I4_4_52, align 8
  br label %L3586

L3074:                                            ; preds = %fb, %entry
  store i32 1, i32* %.preg_I4_4_55, align 8
  br label %L258

L3586:                                            ; preds = %L3842, %tb
  %4 = load i32, i32* %.preg_I4_4_54
  %5 = load i32, i32* %.preg_I4_4_52
  %srem = srem i32 %4, %5
  %cmp15 = icmp eq i32 %srem, 0
  br i1 %cmp15, label %tb2, label %L3842

tb2:                                              ; preds = %L3586
  store i32 0, i32* %.preg_I4_4_55, align 8
  br label %L258

L3842:                                            ; preds = %L3586
  %6 = load i32, i32* %.preg_I4_4_49
  %add.3 = add i32 %6, 1
  store i32 %add.3, i32* %.preg_I4_4_49, align 8
  %7 = load i32, i32* %.preg_I4_4_52
  %add.4 = add i32 %7, 1
  store i32 %add.4, i32* %.preg_I4_4_52, align 8
  %8 = load i32, i32* %.preg_I4_4_49
  %9 = load i32, i32* %.preg_I4_4_53
  %cmp16 = icmp sle i32 %8, %9
  br i1 %cmp16, label %L3586, label %fb

L258:                                             ; preds = %tb2, %L3074
  %10 = load i32, i32* %.preg_I4_4_55
  ret i32 %10

fb:                                               ; preds = %L3842
  br label %L3074
}

define i32 @_Z20checkArmstrongNumberi(i32 %number_4) {
entry:
  %.preg_F8_11_49 = alloca double
  %.preg_I4_4_52 = alloca i32
  %.preg_I4_4_58 = alloca i32
  %.preg_F8_11_53 = alloca double
  %.preg_I4_4_50 = alloca i32
  %.preg_I4_4_55 = alloca i32
  %.preg_I4_4_54 = alloca i32
  %flag_9 = alloca i32, align 4
  %n_8 = alloca i32, align 4
  %number_4.addr = alloca i32, align 4
  %old_frame_pointer_14.addr = alloca i64, align 8
  %originalNumber_5 = alloca i32, align 4
  %remainder_6 = alloca i32, align 4
  %result_7 = alloca i32, align 4
  %return_address_15.addr = alloca i64, align 8
  store i32 %number_4, i32* %number_4.addr, align 4
  store i32 %number_4, i32* %.preg_I4_4_54, align 8
  %0 = load i32, i32* %.preg_I4_4_54
  store i32 %0, i32* %.preg_I4_4_55, align 8
  store i32 0, i32* %.preg_I4_4_50, align 8
  %1 = load i32, i32* %.preg_I4_4_54
  %cmp9 = icmp ne i32 %1, 0
  br i1 %cmp9, label %tb, label %L4354

tb:                                               ; preds = %entry
  br label %L4866

L4354:                                            ; preds = %fb, %entry
  %2 = load i32, i32* %.preg_I4_4_54
  store i32 %2, i32* %.preg_I4_4_55, align 8
  %3 = load i32, i32* %.preg_I4_4_54
  %cmp11 = icmp ne i32 %3, 0
  br i1 %cmp11, label %tb1, label %L6914

L4866:                                            ; preds = %L4866, %tb
  %4 = load i32, i32* %.preg_I4_4_55
  %div = sdiv i32 %4, 10
  store i32 %div, i32* %.preg_I4_4_55, align 8
  %5 = load i32, i32* %.preg_I4_4_50
  %add = add i32 %5, 1
  store i32 %add, i32* %.preg_I4_4_50, align 8
  %6 = load i32, i32* %.preg_I4_4_55
  %cmp10 = icmp ne i32 %6, 0
  br i1 %cmp10, label %L4866, label %fb

fb:                                               ; preds = %L4866
  br label %L4354

tb1:                                              ; preds = %L4354
  %7 = load i32, i32* %.preg_I4_4_50
  %conv9 = sitofp i32 %7 to double
  store double %conv9, double* %.preg_F8_11_53, align 8
  store i32 0, i32* %.preg_I4_4_58, align 8
  br label %L5890

L6914:                                            ; preds = %L4354
  store i32 0, i32* %.preg_I4_4_58, align 8
  br label %L5378

L5890:                                            ; preds = %L5890, %tb1
  %8 = load i32, i32* %.preg_I4_4_55
  %lowPart_4 = alloca i32, align 4
  %div.2 = sdiv i32 %8, 10
  %srem = srem i32 %8, 10
  store i32 %div.2, i32* %lowPart_4, align 4
  store i32 %srem, i32* %.preg_I4_4_52, align 8
  %9 = load i32, i32* %.preg_I4_4_52
  %conv93 = sitofp i32 %9 to double
  %10 = load double, double* %.preg_F8_11_53
  %call8 = call double @pow(double %conv93, double %10)
  store double %call8, double* %.preg_F8_11_49, align 8
  %11 = load i32, i32* %.preg_I4_4_58
  %conv94 = sitofp i32 %11 to double
  %12 = load double, double* %.preg_F8_11_49
  %add.5 = fadd double %conv94, %12
  %fp2int = fptosi double %add.5 to i32
  store i32 %fp2int, i32* %.preg_I4_4_58, align 8
  %13 = load i32, i32* %lowPart_4
  store i32 %13, i32* %.preg_I4_4_55, align 8
  %14 = load i32, i32* %.preg_I4_4_55
  %cmp12 = icmp ne i32 %14, 0
  br i1 %cmp12, label %L5890, label %fb6

fb6:                                              ; preds = %L5890
  br label %L5378

L5378:                                            ; preds = %fb6, %L6914
  %15 = load i32, i32* %.preg_I4_4_54
  %16 = load i32, i32* %.preg_I4_4_58
  %cmp13 = icmp eq i32 %15, %16
  ret i1 %cmp13
}

declare double @pow(double, double)
