	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test63.c (test63.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test63.c"


	.text
	.align	2

	.section .bss, "wa",@nobits

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits

	.section .bss
	.org 0x0
	.align	4
.globl	d1
	.type	d1, @object
	.size	d1, 8
d1:	# 0x0
	.skip 16
	.org 0x10
	.align	4
.globl	d2
	.type	d2, @object
	.size	d2, 8
d2:	# 0x10
	.skip 16
	.org 0x20
	.align	4
.globl	sumOfDistances
	.type	sumOfDistances, @object
	.size	sumOfDistances, 8
sumOfDistances:	# 0x20
	.skip 8
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 8, %rsp
	.loc	1	9	0
 #   5      int feet;
 #   6      float inch;
 #   7  } d1, d2, sumOfDistances;
 #   8  
 #   9  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-8,%rsp                 	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_1282:
	xorl %eax,%eax                	# [0] 
	.loc	1	11	0
 #  10  {
 #  11      printf("Enter information for 1st distance\n");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	12	0
 #  12      printf("Enter feet: ");
	movq $(.rodata+48),%rdi       	# [0] .rodata+48
	call printf                   	# [0] printf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	13	0
 #  13      scanf("%d", &d1.feet);
	movq $(.bss),%rsi             	# [1] .bss
	movq $(.rodata+64),%rdi       	# [1] .rodata+64
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	15	0
 #  14      //d1.feet = 1;
 #  15      printf("Enter inch: ");
	movq $(.rodata+80),%rdi       	# [0] .rodata+80
	call printf                   	# [0] printf
.LBB6_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	16	0
 #  16      scanf("%f", &d1.inch);
	movq $(.bss+4),%rsi           	# [1] .bss+4
	movq $(.rodata+96),%rdi       	# [1] .rodata+96
	call scanf                    	# [1] scanf
.LBB7_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	19	0
 #  17      //d1.inch = 2.0f;
 #  18  
 #  19      printf("\nEnter information for 2nd distance\n");
	movq $(.rodata+112),%rdi      	# [0] .rodata+112
	call printf                   	# [0] printf
.LBB8_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	20	0
 #  20      printf("Enter feet: ");
	movq $(.rodata+48),%rdi       	# [0] .rodata+48
	call printf                   	# [0] printf
.LBB9_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	21	0
 #  21      scanf("%d", &d2.feet);
	movq $(.bss+16),%rsi          	# [1] .bss+16
	movq $(.rodata+64),%rdi       	# [1] .rodata+64
	call scanf                    	# [1] scanf
.LBB10_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	23	0
 #  22      //d2.feet = 2;
 #  23      printf("Enter inch: ");
	movq $(.rodata+80),%rdi       	# [0] .rodata+80
	call printf                   	# [0] printf
.LBB11_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	24	0
 #  24      scanf("%f", &d2.inch);
	movq $(.bss+20),%rsi          	# [1] .bss+20
	movq $(.rodata+96),%rdi       	# [1] .rodata+96
	call scanf                    	# [1] scanf
.LBB12_main:
	.loc	1	29	0
 #  25      //d2.inch = 3.0f;
 #  26  
 #  27  
 #  28      sumOfDistances.feet = d1.feet+d2.feet;
 #  29      sumOfDistances.inch = d1.inch+d2.inch;
	movss .bss+4(%rip),%xmm3      	# [0] id:20 d1+0x4
	movss .bss+20(%rip),%xmm0     	# [1] id:21 d2+0x4
	.loc	1	33	0
 #  30  
 #  31      // If inch is greater than 12, changing it to feet.
 #  32  
 #  33      if (sumOfDistances.inch>12.0)
	movss .rodata+152(%rip),%xmm2 	# [3] 
	.loc	1	28	0
	movl .bss+16(%rip),%eax       	# [4] id:19 d2+0x0
	movl .bss(%rip),%esi          	# [4] id:18 d1+0x0
	.loc	1	29	0
	addss %xmm3,%xmm0             	# [4] 
	.loc	1	28	0
	addl %eax,%esi                	# [7] 
	.loc	1	33	0
	comiss %xmm2,%xmm0            	# [7] 
	.loc	1	29	0
	movaps %xmm0,%xmm1            	# [11] 
	.loc	1	33	0
	jbe .Lt_0_770                 	# [11] 
.LBB13_main:
	.loc	1	35	0
 #  34      {
 #  35          sumOfDistances.inch = sumOfDistances.inch-12.0;
	movss .rodata+156(%rip),%xmm1 	# [0] 
	addss %xmm0,%xmm1             	# [3] 
	incl %esi                     	# [5] 
	.loc	1	36	0
 #  36          ++sumOfDistances.feet;
	movl %esi,.bss+32(%rip)       	# [6] id:17 sumOfDistances+0x0
	.loc	1	35	0
	movss %xmm1,.bss+36(%rip)     	# [6] id:16 sumOfDistances+0x4
	jmp .Lt_0_1026                	# [6] 
.Lt_0_770:
	movl %esi,.bss+32(%rip)       	# [0] id:17 sumOfDistances+0x0
	movss %xmm0,.bss+36(%rip)     	# [0] id:16 sumOfDistances+0x4
.Lt_0_1026:
	.loc	1	39	0
 #  37      }
 #  38  
 #  39      printf("\nSum of distances = %d\'-%.1f\"",sumOfDistances.feet, sumOfDistances.inch);
	movl $1,%eax                  	# [0] 
	cvtss2sd %xmm1,%xmm0          	# [0] 
	movl %esi,%esi                	# [1] 
	movq $(.rodata+160),%rdi      	# [1] .rodata+160
	call printf                   	# [1] printf
.LBB16_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	40	0
 #  40      return 0;
	addq $8,%rsp                  	# [0] 
	ret                           	# [0] 
.L_0_1538:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.byte	0x45, 0x6e, 0x74, 0x65, 0x72, 0x20, 0x69, 0x6e	# Enter in
	.byte	0x66, 0x6f, 0x72, 0x6d, 0x61, 0x74, 0x69, 0x6f	# formatio
	.byte	0x6e, 0x20, 0x66, 0x6f, 0x72, 0x20, 0x31, 0x73	# n for 1s
	.byte	0x74, 0x20, 0x64, 0x69, 0x73, 0x74, 0x61, 0x6e	# t distan
	.byte	0x63, 0x65, 0xa, 0x0	# ce\n\000
	.org 0x30
	.align	0
	# offset 48
	.string "Enter feet: "
	.org 0x40
	.align	0
	# offset 64
	.string "%d"
	.org 0x50
	.align	0
	# offset 80
	.string "Enter inch: "
	.org 0x60
	.align	0
	# offset 96
	.string "%f"
	.org 0x70
	.align	0
	# offset 112
	.byte	0xa, 0x45, 0x6e, 0x74, 0x65, 0x72, 0x20, 0x69	# \nEnter i
	.byte	0x6e, 0x66, 0x6f, 0x72, 0x6d, 0x61, 0x74, 0x69	# nformati
	.byte	0x6f, 0x6e, 0x20, 0x66, 0x6f, 0x72, 0x20, 0x32	# on for 2
	.byte	0x6e, 0x64, 0x20, 0x64, 0x69, 0x73, 0x74, 0x61	# nd dista
	.byte	0x6e, 0x63, 0x65, 0xa, 0x0	# nce\n\000
	.org 0x98
	.align	0
	# offset 152
	.4byte	1094713344
	# float 12.0000
	.org 0x9c
	.align	0
	# offset 156
	.4byte	-1052770304
	# float -12.0000
	.org 0xa0
	.align	0
	# offset 160
	.byte	0xa, 0x53, 0x75, 0x6d, 0x20, 0x6f, 0x66, 0x20	# \nSum of 
	.byte	0x64, 0x69, 0x73, 0x74, 0x61, 0x6e, 0x63, 0x65	# distance
	.byte	0x73, 0x20, 0x3d, 0x20, 0x25, 0x64, 0x27, 0x2d	# s = %d'-
	.byte	0x25, 0x2e, 0x31, 0x66, 0x22, 0x0	# %.1f"\000

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_1282-main
	.uleb128	.L_0_1538-.L_0_1282
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .bss
	.align	16
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x10
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test63.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

