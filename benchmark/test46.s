	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test46.c (test46.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test46.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 456, %rsp
	# i = 416
	# n = 0
	# arr = 16
	# _temp_gra_spill1 = 424
	# _temp_gra_spill2 = 432
	.loc	1	2	0
 #   1  #include <stdio.h>
 #   2  int main(){
.LBB1_main:
.LEH_adjustsp_main:
.LEH_csr_main:
	addq $-456,%rsp               	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
	movq %rbx,432(%rsp)           	# [0] _temp_gra_spill2
	movq %rbp,424(%rsp)           	# [0] _temp_gra_spill1
.L_0_4866:
	xorl %eax,%eax                	# [0] 
	.loc	1	5	0
 #   3      int i,n;
 #   4      float arr[100];
 #   5      printf("Enter total number of elements(1 to 100): ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	6	0
 #   6      scanf("%d",&n);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	8	0
 #   7      //n = 4;
 #   8      printf("\n");
	movq $(.rodata+64),%rdi       	# [0] .rodata+64
	call printf                   	# [0] printf
.LBB5_main:
	movl 0(%rsp),%eax             	# [0] n
	cmpl $0,%eax                  	# [3] 
	jle .Lt_0_1794                	# [4] 
.LBB6_main:
	xorl %ebp,%ebp                	# [0] 
	leaq 16(%rsp),%rbx            	# [0] arr
.Lt_0_2306:
 #<loop> Loop body line 8, nesting depth: 1, estimated iterations: 100
	xorl %eax,%eax                	# [0] 
	.loc	1	11	0
 #   9      for(i=0;i<n;++i)  /* Stores number entered by user. */
 #  10      {
 #  11         printf("Enter Number %d\n: ",i);
	movl %ebp,%esi                	# [1] 
	movq $(.rodata+80),%rdi       	# [1] .rodata+80
	call printf                   	# [1] printf
.LBB8_main:
 #<loop> Part of loop body line 8, head labeled .Lt_0_2306
	.loc	1	13	0
 #  12         //scanf("%f",&arr[i]);
 #  13         arr[i] = i;
	movl 0(%rsp),%eax             	# [0] n
	cvtsi2ss %ebp,%xmm0           	# [0] 
	addl $1,%ebp                  	# [2] 
	addq $4,%rbx                  	# [3] 
	cmpl %ebp,%eax                	# [3] 
	movss %xmm0,-4(%rbx)          	# [4] id:28 arr+0x0
	jg .Lt_0_2306                 	# [4] 
.Lt_0_1794:
	xorl %eax,%eax                	# [0] 
	.loc	1	17	0
 #  14         
 #  15      }
 #  16      
 #  17      printf("reach here\n");
	movq $(.rodata+112),%rdi      	# [0] .rodata+112
	call printf                   	# [0] printf
.LBB10_main:
	.loc	1	18	0
 #  18      printf("%f",arr[11]);
	movss 60(%rsp),%xmm0          	# [0] arr+44
	movl $1,%eax                  	# [2] 
	movq $(.rodata+128),%rdi      	# [3] .rodata+128
	cvtss2sd %xmm0,%xmm0          	# [3] 
	call printf                   	# [3] printf
.LBB11_main:
	movl 0(%rsp),%eax             	# [0] n
	cmpl $1,%eax                  	# [3] 
	jle .Lt_0_4610                	# [4] 
.LBB12_main:
	leaq 20(%rsp),%rbx            	# [0] arr+4
	movss 16(%rsp),%xmm1          	# [1] arr
	movl $1,%ebp                  	# [1] 
	jmp .Lt_0_3330                	# [1] 
.Lt_0_4098:
 #<loop> Part of loop body line 18, head labeled .Lt_0_3330
	.loc	1	24	0
 #  20      //#if 0
 #  21      for(i=1;i<n;++i)  /* Loop to store largest number to arr[0] */
 #  22      {
 #  23         if(arr[0]<arr[i]) /* Change < to > if you want to find smallest element*/
 #  24             arr[0]=arr[i];
	addl $1,%ebp                  	# [0] 
	addq $4,%rbx                  	# [1] 
	cmpl %ebp,%eax                	# [1] 
	jle .Lt_0_2818                	# [2] 
.Lt_0_3330:
 #<loop> Loop body line 18, nesting depth: 1, estimated iterations: 100
	.loc	1	23	0
	movss 0(%rbx),%xmm0           	# [0] id:30 arr+0x0
	comiss %xmm1,%xmm0            	# [3] 
	jbe .Lt_0_4098                	# [7] 
.LBB15_main:
 #<loop> Part of loop body line 18, head labeled .Lt_0_3330
	.loc	1	24	0
	movss %xmm0,16(%rsp)          	# [0] arr
	movaps %xmm0,%xmm1            	# [0] 
	jmp .Lt_0_4098                	# [0] 
.Lt_0_2818:
	.loc	1	26	0
 #  25      }
 #  26      printf("Largest element = %.2f",arr[0]);
	movl $1,%eax                  	# [0] 
	cvtss2sd %xmm1,%xmm0          	# [1] 
	movq $(.rodata+144),%rdi      	# [1] .rodata+144
	call printf                   	# [1] printf
.LBB20_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	29	0
 #  27     //#endif
 #  28  
 #  29      return 0;
	movq 432(%rsp),%rbx           	# [0] _temp_gra_spill2
	movq 424(%rsp),%rbp           	# [1] _temp_gra_spill1
	addq $456,%rsp                	# [1] 
	ret                           	# [1] 
.Lt_0_4610:
	.loc	1	24	0
	movss 16(%rsp),%xmm1          	# [0] arr
	jmp .Lt_0_2818                	# [0] 
.L_0_5122:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter total number of elements(1 to 100): "
	.org 0x30
	.align	0
	# offset 48
	.string "%d"
	.org 0x40
	.align	0
	# offset 64
	.byte	0xa, 0x0	# \n\000
	.org 0x50
	.align	0
	# offset 80
	.byte	0x45, 0x6e, 0x74, 0x65, 0x72, 0x20, 0x4e, 0x75	# Enter Nu
	.byte	0x6d, 0x62, 0x65, 0x72, 0x20, 0x25, 0x64, 0xa	# mber %d\n
	.byte	0x3a, 0x20, 0x0	# : \000
	.org 0x70
	.align	0
	# offset 112
	.byte	0x72, 0x65, 0x61, 0x63, 0x68, 0x20, 0x68, 0x65	# reach he
	.byte	0x72, 0x65, 0xa, 0x0	# re\n\000
	.org 0x80
	.align	0
	# offset 128
	.string "%f"
	.org 0x90
	.align	0
	# offset 144
	.string "Largest element = %.2f"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_4866-main
	.uleb128	.L_0_5122-.L_0_4866
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0xd0, 0x03, 0x04
	.4byte	.LEH_csr_main - .LEH_adjustsp_main
	.byte	0x86, 0x05, 0x83, 0x04
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test46.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

