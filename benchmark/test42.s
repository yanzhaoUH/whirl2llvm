	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test42.c (test42.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test42.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# n = 4
	# c = 0
	.loc	1	7	0
 #   3  #include <stdio.h>
 #   4  #include <math.h>
 #   5  int binary_octal(int n);
 #   6  int octal_binary(int n);
 #   7  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_6914:
	xorl %eax,%eax                	# [0] 
	.loc	1	11	0
 #   8  {
 #   9      int n;
 #  10      char c;
 #  11      printf("Instructions:\n");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	12	0
 #  12      printf("1. Enter alphabet 'o' to convert binary to octal.\n");
	movq $(.rodata+16),%rdi       	# [0] .rodata+16
	call printf                   	# [0] printf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	13	0
 #  13      printf("2. Enter alphabet 'b' to convert octal to binary.\n");
	movq $(.rodata+80),%rdi       	# [0] .rodata+80
	call printf                   	# [0] printf
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	14	0
 #  14      scanf("%c",&c);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+144),%rdi      	# [1] .rodata+144
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB6_main:
	.loc	1	16	0
 #  15      //c='o';
 #  16      if ( c=='o' || c=='O')
	movsbl 0(%rsp),%eax           	# [0] c
	cmpl $111,%eax                	# [3] 
	je .Lt_0_5634                 	# [4] 
.LBB7_main:
	cmpl $79,%eax                 	# [0] 
	je .Lt_0_5634                 	# [1] 
.L_0_2818:
	.loc	1	23	0
 #  19          scanf("%d",&n);
 #  20          //n=1101;
 #  21          printf("%d in binary = %d in octal", n, binary_octal(n));
 #  22      }
 #  23      if ( c=='b' || c=='B')
	cmpl $98,%eax                 	# [0] 
	je .Lt_0_6146                 	# [1] 
.LBB16_main:
	cmpl $66,%eax                 	# [0] 
	je .Lt_0_6146                 	# [1] 
.L_0_3842:
	xorq %rax,%rax                	# [0] 
	.loc	1	29	0
 #  25          printf("Enter a octal number: ");
 #  26          scanf("%d",&n);
 #  27          printf("%d in octal = %d in binary",n, octal_binary(n));
 #  28      }
 #  29      return 0;
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.Lt_0_5634:
	xorl %eax,%eax                	# [0] 
	.loc	1	18	0
	movq $(.rodata+160),%rdi      	# [0] .rodata+160
	call printf                   	# [0] printf
.LBB11_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	19	0
	leaq 4(%rsp),%rsi             	# [1] n
	movq $(.rodata+192),%rdi      	# [1] .rodata+192
	call scanf                    	# [1] scanf
.LBB12_main:
	.loc	1	21	0
	movl 4(%rsp),%edi             	# [0] n
	call _Z12binary_octali        	# [0] _Z12binary_octali
.LBB13_main:
	movl 4(%rsp),%esi             	# [0] n
	movq $(.rodata+208),%rdi      	# [0] .rodata+208
	movl %eax,%eax                	# [0] 
	movl %eax,%edx                	# [1] 
	xorl %eax,%eax                	# [1] 
	call printf                   	# [1] printf
.LBB14_main:
	movsbl 0(%rsp),%eax           	# [0] c
	jmp .L_0_2818                 	# [0] 
.Lt_0_6146:
	xorl %eax,%eax                	# [0] 
	.loc	1	25	0
	movq $(.rodata+240),%rdi      	# [0] .rodata+240
	call printf                   	# [0] printf
.LBB20_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	26	0
	leaq 4(%rsp),%rsi             	# [1] n
	movq $(.rodata+192),%rdi      	# [1] .rodata+192
	call scanf                    	# [1] scanf
.LBB21_main:
	.loc	1	27	0
	movl 4(%rsp),%edi             	# [0] n
	call _Z12octal_binaryi        	# [0] _Z12octal_binaryi
.LBB22_main:
	movl 4(%rsp),%esi             	# [0] n
	movq $(.rodata+272),%rdi      	# [0] .rodata+272
	movl %eax,%eax                	# [0] 
	movl %eax,%edx                	# [1] 
	xorl %eax,%eax                	# [1] 
	call printf                   	# [1] printf
.LBB26_main:
	jmp .L_0_3842                 	# [0] 
.L_0_7170:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.byte	0x49, 0x6e, 0x73, 0x74, 0x72, 0x75, 0x63, 0x74	# Instruct
	.byte	0x69, 0x6f, 0x6e, 0x73, 0x3a, 0xa, 0x0	# ions:\n\000
	.org 0x10
	.align	0
	# offset 16
	.byte	0x31, 0x2e, 0x20, 0x45, 0x6e, 0x74, 0x65, 0x72	# 1. Enter
	.byte	0x20, 0x61, 0x6c, 0x70, 0x68, 0x61, 0x62, 0x65	#  alphabe
	.byte	0x74, 0x20, 0x27, 0x6f, 0x27, 0x20, 0x74, 0x6f	# t 'o' to
	.byte	0x20, 0x63, 0x6f, 0x6e, 0x76, 0x65, 0x72, 0x74	#  convert
	.byte	0x20, 0x62, 0x69, 0x6e, 0x61, 0x72, 0x79, 0x20	#  binary 
	.byte	0x74, 0x6f, 0x20, 0x6f, 0x63, 0x74, 0x61, 0x6c	# to octal
	.byte	0x2e, 0xa, 0x0	# .\n\000
	.org 0x50
	.align	0
	# offset 80
	.byte	0x32, 0x2e, 0x20, 0x45, 0x6e, 0x74, 0x65, 0x72	# 2. Enter
	.byte	0x20, 0x61, 0x6c, 0x70, 0x68, 0x61, 0x62, 0x65	#  alphabe
	.byte	0x74, 0x20, 0x27, 0x62, 0x27, 0x20, 0x74, 0x6f	# t 'b' to
	.byte	0x20, 0x63, 0x6f, 0x6e, 0x76, 0x65, 0x72, 0x74	#  convert
	.byte	0x20, 0x6f, 0x63, 0x74, 0x61, 0x6c, 0x20, 0x74	#  octal t
	.byte	0x6f, 0x20, 0x62, 0x69, 0x6e, 0x61, 0x72, 0x79	# o binary
	.byte	0x2e, 0xa, 0x0	# .\n\000
	.org 0x90
	.align	0
	# offset 144
	.string "%c"
	.org 0xa0
	.align	0
	# offset 160
	.string "Enter a binary number: "
	.org 0xc0
	.align	0
	# offset 192
	.string "%d"
	.org 0xd0
	.align	0
	# offset 208
	.string "%d in binary = %d in octal"
	.org 0xf0
	.align	0
	# offset 240
	.string "Enter a octal number: "
	.org 0x110
	.align	0
	# offset 272
	.string "%d in octal = %d in binary"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_6914-main
	.uleb128	.L_0_7170-.L_0_6914
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.p2align 5,,

	# Program Unit: _Z12octal_binaryi
.globl	_Z12octal_binaryi
	.type	_Z12octal_binaryi, @function
_Z12octal_binaryi:	# 0xec
	# .frame	%rsp, 72, %rsp
	# decimal = 4
	# binary = 8
	# i = 0
	# _temp_gra_spill1 = 16
	# _temp_gra_spill2 = 24
	# _temp_gra_spill3 = 32
	.loc	1	52	0
 #  48          i*=10;
 #  49      }
 #  50      return octal;
 #  51  }
 #  52  int octal_binary(int n)  /* Function to convert octal to binary.*/
.LBB1__Z12octal_binaryi:
	addq $-72,%rsp                	# [0] 
	testl %edi,%edi               	# [1] 
	movq %rbp,24(%rsp)            	# [1] _temp_gra_spill2
	movl %edi,%ebp                	# [1] 
	movq %r12,16(%rsp)            	# [2] _temp_gra_spill1
	movq %rbx,32(%rsp)            	# [2] _temp_gra_spill3
	je .Lt_1_3330                 	# [2] 
.LBB2__Z12octal_binaryi:
	xorl %r12d,%r12d              	# [0] 
	xorl %ebx,%ebx                	# [0] 
.Lt_1_1794:
 #<loop> Loop body line 57
	.loc	1	57	0
 #  53  {
 #  54      int decimal=0, binary=0, i=0;
 #  55      while (n!=0)
 #  56      {
 #  57          decimal+=(n%10)*pow(8,i);
	cvtsi2sd %ebx,%xmm1           	# [0] 
	movsd .rodata+304(%rip),%xmm0 	# [0] 
	.globl	pow
	call pow                      	# [0] pow
.LBB4__Z12octal_binaryi:
 #<loop> Part of loop body line 57, head labeled .Lt_1_1794
	movslq %ebp,%rax              	# [0] 
	imulq $1717986919,%rax        	# [1] 
	shrq $32,%rax                 	# [6] 
	movl %ebp,%esi                	# [7] 
	movq %rax,%rax                	# [7] 
	sarl $31,%esi                 	# [8] 
	sarl $2,%eax                  	# [8] 
	subl %esi,%eax                	# [9] 
	movl %eax,%edi                	# [10] 
	imull $10,%edi                	# [11] 
	subl %edi,%ebp                	# [14] 
	cvtsi2sd %ebp,%xmm2           	# [15] 
	mulsd %xmm2,%xmm0             	# [19] 
	cvtsi2sd %r12d,%xmm1          	# [20] 
	addsd %xmm1,%xmm0             	# [24] 
	.loc	1	58	0
 #  58          ++i;
	addl $1,%ebx                  	# [26] 
	.loc	1	59	0
 #  59          n/=10;
	testl %eax,%eax               	# [26] 
	movl %eax,%ebp                	# [27] 
	.loc	1	57	0
	cvttsd2si %xmm0,%r12d         	# [27] 
	.loc	1	59	0
	jne .Lt_1_1794                	# [27] 
.LBB5__Z12octal_binaryi:
	testl %r12d,%r12d             	# [0] 
	movl $1,%ebx                  	# [1] 
	je .Lt_1_3586                 	# [1] 
.LBB6__Z12octal_binaryi:
	xorl %edi,%edi                	# [0] 
.LBB15__Z12octal_binaryi:
 #<loop> Loop body line 65
 #<loop> unrolled 3 times
	.loc	1	65	0
 #  61  /* At this point, the decimal variable contains corresponding decimal value of that octal number. */
 #  62      i=1;
 #  63      while(decimal!=0)
 #  64      {
 #  65          binary+=(decimal%2)*i;
	movl %r12d,%esi               	# [0] 
	sarl $31,%esi                 	# [1] 
	andl $1,%esi                  	# [2] 
	addl %esi,%r12d               	# [3] 
	movl %r12d,%eax               	# [4] 
	andl $1,%eax                  	# [5] 
	subl %esi,%eax                	# [6] 
	imull %ebx,%eax               	# [7] 
	.loc	1	66	0
 #  66          decimal/=2;
	sarl $1,%r12d                 	# [9] 
	imull $10,%ebx                	# [10] 
	.loc	1	65	0
	addl %eax,%edi                	# [10] 
	.loc	1	67	0
 #  67          i*=10;
	testl %r12d,%r12d             	# [10] 
	je .Lt_1_2306                 	# [11] 
.LBB16__Z12octal_binaryi:
 #<loop> Part of loop body line 65, head labeled .LBB15__Z12octal_binaryi
 #<loop> unrolled 3 times
	.loc	1	65	0
	movl %r12d,%esi               	# [0] 
	sarl $31,%esi                 	# [1] 
	andl $1,%esi                  	# [2] 
	addl %esi,%r12d               	# [3] 
	movl %r12d,%eax               	# [4] 
	andl $1,%eax                  	# [5] 
	subl %esi,%eax                	# [6] 
	imull %ebx,%eax               	# [7] 
	.loc	1	66	0
	sarl $1,%r12d                 	# [9] 
	imull $10,%ebx                	# [10] 
	.loc	1	65	0
	addl %eax,%edi                	# [10] 
	.loc	1	67	0
	testl %r12d,%r12d             	# [10] 
	je .Lt_1_2306                 	# [11] 
.Lt_1_2818:
 #<loop> Part of loop body line 65, head labeled .LBB15__Z12octal_binaryi
	.loc	1	65	0
	movl %r12d,%esi               	# [0] 
	sarl $31,%esi                 	# [1] 
	andl $1,%esi                  	# [2] 
	addl %esi,%r12d               	# [3] 
	movl %r12d,%eax               	# [4] 
	andl $1,%eax                  	# [5] 
	subl %esi,%eax                	# [6] 
	imull %ebx,%eax               	# [7] 
	.loc	1	66	0
	sarl $1,%r12d                 	# [9] 
	imull $10,%ebx                	# [10] 
	.loc	1	65	0
	addl %eax,%edi                	# [10] 
	.loc	1	67	0
	testl %r12d,%r12d             	# [10] 
	jne .LBB15__Z12octal_binaryi  	# [11] 
.Lt_1_2306:
	.loc	1	69	0
 #  68      }
 #  69      return binary;
	movl %edi,%eax                	# [0] 
	movq 32(%rsp),%rbx            	# [0] _temp_gra_spill3
	movq 24(%rsp),%rbp            	# [0] _temp_gra_spill2
	movq 16(%rsp),%r12            	# [1] _temp_gra_spill1
	addq $72,%rsp                 	# [1] 
	ret                           	# [1] 
.Lt_1_3330:
	movq 24(%rsp),%rbp            	# [0] _temp_gra_spill2
	xorl %eax,%eax                	# [0] 
	movl %eax,%eax                	# [1] 
	addq $72,%rsp                 	# [1] 
	ret                           	# [1] 
.Lt_1_3586:
	movq 32(%rsp),%rbx            	# [0] _temp_gra_spill3
	movq 24(%rsp),%rbp            	# [1] _temp_gra_spill2
	movq 16(%rsp),%r12            	# [1] _temp_gra_spill1
	xorl %eax,%eax                	# [1] 
	movl %eax,%eax                	# [2] 
	addq $72,%rsp                 	# [2] 
	ret                           	# [2] 
.LDWend__Z12octal_binaryi:
	.size _Z12octal_binaryi, .LDWend__Z12octal_binaryi-_Z12octal_binaryi

	.section .rodata
	.org 0x130
	.align	0
	# offset 304
	.4byte	0
	.4byte	1075838976
	# double 8.00000
	.section .text
	.p2align 5,,

	# Program Unit: _Z12binary_octali
.globl	_Z12binary_octali
	.type	_Z12binary_octali, @function
_Z12binary_octali:	# 0x260
	# .frame	%rsp, 72, %rsp
	# octal = 8
	# decimal = 4
	# i = 0
	# _temp_gra_spill4 = 16
	# _temp_gra_spill5 = 24
	# _temp_gra_spill6 = 32
	.loc	1	31	0
.LBB1__Z12binary_octali:
	addq $-72,%rsp                	# [0] 
	testl %edi,%edi               	# [1] 
	movq %rbp,24(%rsp)            	# [1] _temp_gra_spill5
	movl %edi,%ebp                	# [1] 
	movq %r12,16(%rsp)            	# [2] _temp_gra_spill4
	movq %rbx,32(%rsp)            	# [2] _temp_gra_spill6
	je .Lt_2_3330                 	# [2] 
.LBB2__Z12binary_octali:
	xorl %r12d,%r12d              	# [0] 
	xorl %ebx,%ebx                	# [0] 
.Lt_2_1794:
 #<loop> Loop body line 36
	.loc	1	36	0
	cvtsi2sd %ebx,%xmm1           	# [0] 
	movsd .rodata+312(%rip),%xmm0 	# [0] 
	call pow                      	# [0] pow
.LBB4__Z12binary_octali:
 #<loop> Part of loop body line 36, head labeled .Lt_2_1794
	movslq %ebp,%rax              	# [0] 
	imulq $1717986919,%rax        	# [1] 
	shrq $32,%rax                 	# [6] 
	movl %ebp,%esi                	# [7] 
	movq %rax,%rax                	# [7] 
	sarl $31,%esi                 	# [8] 
	sarl $2,%eax                  	# [8] 
	subl %esi,%eax                	# [9] 
	movl %eax,%edi                	# [10] 
	imull $10,%edi                	# [11] 
	subl %edi,%ebp                	# [14] 
	cvtsi2sd %ebp,%xmm2           	# [15] 
	mulsd %xmm2,%xmm0             	# [19] 
	cvtsi2sd %r12d,%xmm1          	# [20] 
	addsd %xmm1,%xmm0             	# [24] 
	.loc	1	37	0
	addl $1,%ebx                  	# [26] 
	.loc	1	38	0
	testl %eax,%eax               	# [26] 
	movl %eax,%ebp                	# [27] 
	.loc	1	36	0
	cvttsd2si %xmm0,%r12d         	# [27] 
	.loc	1	38	0
	jne .Lt_2_1794                	# [27] 
.LBB5__Z12binary_octali:
	testl %r12d,%r12d             	# [0] 
	movl $1,%ebx                  	# [1] 
	je .Lt_2_3586                 	# [1] 
.LBB6__Z12binary_octali:
	xorl %edi,%edi                	# [0] 
.LBB15__Z12binary_octali:
 #<loop> Loop body line 46
 #<loop> unrolled 3 times
	.loc	1	46	0
	movl %r12d,%esi               	# [0] 
	sarl $31,%esi                 	# [1] 
	andl $7,%esi                  	# [2] 
	addl %esi,%r12d               	# [3] 
	movl %r12d,%eax               	# [4] 
	andl $7,%eax                  	# [5] 
	subl %esi,%eax                	# [6] 
	imull %ebx,%eax               	# [7] 
	.loc	1	47	0
	sarl $3,%r12d                 	# [9] 
	imull $10,%ebx                	# [10] 
	.loc	1	46	0
	addl %eax,%edi                	# [10] 
	.loc	1	48	0
	testl %r12d,%r12d             	# [10] 
	je .Lt_2_2306                 	# [11] 
.LBB16__Z12binary_octali:
 #<loop> Part of loop body line 46, head labeled .LBB15__Z12binary_octali
 #<loop> unrolled 3 times
	.loc	1	46	0
	movl %r12d,%esi               	# [0] 
	sarl $31,%esi                 	# [1] 
	andl $7,%esi                  	# [2] 
	addl %esi,%r12d               	# [3] 
	movl %r12d,%eax               	# [4] 
	andl $7,%eax                  	# [5] 
	subl %esi,%eax                	# [6] 
	imull %ebx,%eax               	# [7] 
	.loc	1	47	0
	sarl $3,%r12d                 	# [9] 
	imull $10,%ebx                	# [10] 
	.loc	1	46	0
	addl %eax,%edi                	# [10] 
	.loc	1	48	0
	testl %r12d,%r12d             	# [10] 
	je .Lt_2_2306                 	# [11] 
.Lt_2_2818:
 #<loop> Part of loop body line 46, head labeled .LBB15__Z12binary_octali
	.loc	1	46	0
	movl %r12d,%esi               	# [0] 
	sarl $31,%esi                 	# [1] 
	andl $7,%esi                  	# [2] 
	addl %esi,%r12d               	# [3] 
	movl %r12d,%eax               	# [4] 
	andl $7,%eax                  	# [5] 
	subl %esi,%eax                	# [6] 
	imull %ebx,%eax               	# [7] 
	.loc	1	47	0
	sarl $3,%r12d                 	# [9] 
	imull $10,%ebx                	# [10] 
	.loc	1	46	0
	addl %eax,%edi                	# [10] 
	.loc	1	48	0
	testl %r12d,%r12d             	# [10] 
	jne .LBB15__Z12binary_octali  	# [11] 
.Lt_2_2306:
	.loc	1	50	0
	movl %edi,%eax                	# [0] 
	movq 32(%rsp),%rbx            	# [0] _temp_gra_spill6
	movq 24(%rsp),%rbp            	# [0] _temp_gra_spill5
	movq 16(%rsp),%r12            	# [1] _temp_gra_spill4
	addq $72,%rsp                 	# [1] 
	ret                           	# [1] 
.Lt_2_3330:
	movq 24(%rsp),%rbp            	# [0] _temp_gra_spill5
	xorl %eax,%eax                	# [0] 
	movl %eax,%eax                	# [1] 
	addq $72,%rsp                 	# [1] 
	ret                           	# [1] 
.Lt_2_3586:
	movq 32(%rsp),%rbx            	# [0] _temp_gra_spill6
	movq 24(%rsp),%rbp            	# [1] _temp_gra_spill5
	movq 16(%rsp),%r12            	# [1] _temp_gra_spill4
	xorl %eax,%eax                	# [1] 
	movl %eax,%eax                	# [2] 
	addq $72,%rsp                 	# [2] 
	ret                           	# [2] 
.LDWend__Z12binary_octali:
	.size _Z12binary_octali, .LDWend__Z12binary_octali-_Z12binary_octali

	.section .rodata
	.org 0x138
	.align	0
	# offset 312
	.4byte	0
	.4byte	1073741824
	# double 2.00000
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test42.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

