	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test20.c (test20.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test20.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# n1 = 0
	# n2 = 4
	# i = 12
	# gcd = 8
	.loc	1	2	0
 #   1  #include <stdio.h>
 #   2  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_9218:
	xorl %eax,%eax                	# [0] 
	.loc	1	6	0
 #   3  {
 #   4      int n1, n2, i, gcd;
 #   5  
 #   6      printf("Enter two integers: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	7	0
 #   7      scanf("%d %d", &n1, &n2);
	leaq 4(%rsp),%rdx             	# [0] n2
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	.loc	1	12	0
 #   8  
 #   9      //n1 = 24;
 #  10      //n2 = 8;
 #  11  
 #  12      for(i=1; i <= n1 && i <= n2; ++i)
	movl 0(%rsp),%esi             	# [0] n1
	cmpl $1,%esi                  	# [3] 
	movl 4(%rsp),%r9d             	# [4] n2
	jl .Lt_0_7426                 	# [4] 
.LBB5_main:
	cmpl $1,%r9d                  	# [0] 
	jl .Lt_0_7426                 	# [1] 
.LBB6_main:
	movl $1,%eax                  	# [0] 
.Lt_0_5890:
	testl %eax,%eax               	# [0] 
	je .Lt_0_7938                 	# [1] 
.LBB9_main:
	movl $1,%r8d                  	# [0] 
	movl 8(%rsp),%r10d            	# [0] gcd
	jmp .L_0_2818                 	# [0] 
.LBB16_main:
 #<loop> Part of loop body line 15, head labeled .L_0_2818
	cmpl %r8d,%r9d                	# [0] 
	jl .L_0_3074                  	# [1] 
.L_0_2818:
 #<loop> Loop body line 15
	.loc	1	15	0
 #  13      {
 #  14          // Checks if i is factor of both integers
 #  15          if(n1%i==0 && n2%i==0)
	movl %esi,%eax                	# [0] 
	cltd                          	# [1] 
	idivl %r8d                    	# [2] 
	testl %edx,%edx               	# [24] 
	je .LBB11_main                	# [25] 
.L_0_4098:
 #<loop> Part of loop body line 15, head labeled .L_0_2818
	.loc	1	16	0
 #  16              gcd = i;
	addl $1,%r8d                  	# [0] 
	.loc	1	12	0
	cmpl %r8d,%esi                	# [1] 
	jge .LBB16_main               	# [2] 
.L_0_3074:
	xorl %eax,%eax                	# [0] 
	.loc	1	19	0
 #  17      }
 #  18  
 #  19      printf("G.C.D of %d and %d is %d", n1, n2, gcd);
	movl %r10d,%ecx               	# [0] 
	movl %r9d,%edx                	# [0] 
	movl %esi,%esi                	# [1] 
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call printf                   	# [1] printf
.LBB21_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	21	0
 #  20  
 #  21      return 0;
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.LBB11_main:
 #<loop> Part of loop body line 15, head labeled .L_0_2818
	.loc	1	15	0
	movl %r9d,%eax                	# [0] 
	cltd                          	# [1] 
	idivl %r8d                    	# [2] 
	testl %edx,%edx               	# [24] 
	jne .L_0_4098                 	# [25] 
.Lt_0_6658:
 #<loop> Part of loop body line 15, head labeled .L_0_2818
	.loc	1	16	0
	movl %r8d,%r10d               	# [0] 
	jmp .L_0_4098                 	# [0] 
.Lt_0_7426:
.L_0_3586:
	xorl %eax,%eax                	# [0] 
	jmp .Lt_0_5890                	# [0] 
.Lt_0_7938:
	.loc	1	12	0
	movl 8(%rsp),%r10d            	# [0] gcd
	jmp .L_0_3074                 	# [0] 
.L_0_9474:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter two integers: "
	.org 0x20
	.align	0
	# offset 32
	.string "%d %d"
	.org 0x30
	.align	0
	# offset 48
	.string "G.C.D of %d and %d is %d"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_9218-main
	.uleb128	.L_0_9474-.L_0_9218
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test20.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

