	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test29.c (test29.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test29.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# number = 0
	# result = 4
	.loc	1	2	0
 #   1  #include <stdio.h>
 #   2  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_3330:
	xorl %eax,%eax                	# [0] 
	.loc	1	6	0
 #   3  {
 #   4      int number, originalNumber, remainder, result = 0;
 #   5  
 #   6      printf("Enter a three digit integer: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	7	0
 #   7      scanf("%d", &number);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	.loc	1	10	0
 #   8  
 #   9      //number = 153;
 #  10      originalNumber = number;
	movl 0(%rsp),%esi             	# [0] number
	testl %esi,%esi               	# [3] 
	movl %esi,%r8d                	# [4] 
	je .Lt_0_3074                 	# [4] 
.LBB5_main:
	xorl %edx,%edx                	# [0] 
.LBB17_main:
 #<loop> Loop body line 15
 #<loop> unrolled 2 times
	.loc	1	15	0
 #  11  
 #  12      while (originalNumber != 0)
 #  13      {
 #  14          remainder = originalNumber%10;
 #  15          result += remainder*remainder*remainder;
	movslq %r8d,%rax              	# [0] 
	imulq $1717986919,%rax        	# [1] 
	shrq $32,%rax                 	# [6] 
	movl %r8d,%r10d               	# [7] 
	movq %rax,%rax                	# [7] 
	sarl $31,%r10d                	# [8] 
	sarl $2,%eax                  	# [8] 
	subl %r10d,%eax               	# [9] 
	movl %eax,%r9d                	# [10] 
	imull $10,%r9d                	# [11] 
	movl %r8d,%ecx                	# [13] 
	subl %r9d,%ecx                	# [14] 
	movl %ecx,%edi                	# [15] 
	imull %ecx,%edi               	# [16] 
	imull %ecx,%edi               	# [19] 
	addl %edi,%edx                	# [22] 
	.loc	1	16	0
 #  16          originalNumber /= 10;
	testl %eax,%eax               	# [22] 
	je .LBB16_main                	# [23] 
.Lt_0_1794:
 #<loop> Part of loop body line 15, head labeled .LBB17_main
	.loc	1	15	0
	movslq %eax,%r8               	# [0] 
	imulq $1717986919,%r8         	# [1] 
	shrq $32,%r8                  	# [6] 
	movl %eax,%r10d               	# [7] 
	movq %r8,%r8                  	# [7] 
	sarl $31,%r10d                	# [8] 
	sarl $2,%r8d                  	# [8] 
	subl %r10d,%r8d               	# [9] 
	movl %r8d,%r9d                	# [10] 
	imull $10,%r9d                	# [11] 
	movl %eax,%ecx                	# [13] 
	subl %r9d,%ecx                	# [14] 
	movl %ecx,%edi                	# [15] 
	imull %ecx,%edi               	# [16] 
	imull %ecx,%edi               	# [19] 
	addl %edi,%edx                	# [22] 
	.loc	1	16	0
	testl %r8d,%r8d               	# [22] 
	jne .LBB17_main               	# [23] 
.LBB16_main:
	.loc	1	19	0
 #  17      }
 #  18  
 #  19      if(result == number)
	cmpl %edx,%esi                	# [0] 
	jne .Lt_0_2306                	# [1] 
.Lt_0_3074:
	xorl %eax,%eax                	# [0] 
	.loc	1	20	0
 #  20          printf("%d is an Armstrong number.",number);
	movl %esi,%esi                	# [1] 
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call printf                   	# [1] printf
.LBB18_main:
	jmp .Lt_0_2562                	# [0] 
.Lt_0_2306:
	xorl %eax,%eax                	# [0] 
	.loc	1	22	0
 #  21      else
 #  22          printf("%d is not an Armstrong number.",number);
	movl %esi,%esi                	# [1] 
	movq $(.rodata+80),%rdi       	# [1] .rodata+80
	call printf                   	# [1] printf
.LBB22_main:
.Lt_0_2562:
	xorq %rax,%rax                	# [0] 
	.loc	1	24	0
 #  23  
 #  24      return 0;
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.L_0_3586:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter a three digit integer: "
	.org 0x20
	.align	0
	# offset 32
	.string "%d"
	.org 0x30
	.align	0
	# offset 48
	.string "%d is an Armstrong number."
	.org 0x50
	.align	0
	# offset 80
	.string "%d is not an Armstrong number."

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_3330-main
	.uleb128	.L_0_3586-.L_0_3330
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test29.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

