	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test48.c (test48.I)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O3	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test48.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 120136, %rsp
	# r = 0
	# c = 4
	# a = 16
	# b = 40016
	# sum = 80016
	# _temp_gra_spill1 = 120016
	# _temp_gra_spill2 = 120024
	# _temp_gra_spill3 = 120032
	# _temp_gra_spill4 = 120040
	# _temp_gra_spill5 = 120048
	# _temp_gra_spill6 = 120056
	# _temp_gra_spill7 = 120064
	# _temp_gra_spill8 = 120072
	# _temp_gra_spill9 = 120080
	# _temp_gra_spill10 = 120088
	# _temp_gra_spill11 = 120096
	# _temp_gra_spill12 = 120104
	# _temp_gra_spill13 = 120112
	# _temp_gra_spill14 = 120120
	.loc	1	2	0
 #   1  #include <stdio.h>
 #   2  int main(){
.LBB1_main:
.LEH_adjustsp_main:
.LEH_csr_main:
	addq $-120136,%rsp            	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
	addq    $-8,%rsp
	stmxcsr (%rsp)
	orq $32768, (%rsp)
	ldmxcsr (%rsp)
	addq    $8,%rsp
	movq %rbx,120056(%rsp)        	# [0] _temp_gra_spill6
	movq %rbp,120040(%rsp)        	# [0] _temp_gra_spill4
	movq %r12,120032(%rsp)        	# [0] _temp_gra_spill3
	movq %r13,120048(%rsp)        	# [0] _temp_gra_spill5
	movq %r14,120024(%rsp)        	# [0] _temp_gra_spill2
	movq %r15,120016(%rsp)        	# [0] _temp_gra_spill1
	movq $1,%rdi                  	# [0] 
	xorl %eax,%eax                	# [0] 
	.globl	__pathscale_malloc_alg
	call __pathscale_malloc_alg   	# [0] __pathscale_malloc_alg
.L_0_35074:
	xorl %eax,%eax                	# [0] 
	.loc	1	5	0
 #   3      int r, c, a[100][100], b[100][100], sum[100][100], i, j;
 #   4  
 #   5      printf("Enter number of rows (between 1 and 100): ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	6	0
 #   6      scanf("%d", &r);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	8	0
 #   7      //r= 2;
 #   8      printf("Enter number of columns (between 1 and 100): ");
	movq $(.rodata+64),%rdi       	# [0] .rodata+64
	call printf                   	# [0] printf
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	9	0
 #   9      scanf("%d", &c);
	leaq 4(%rsp),%rsi             	# [1] c
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call scanf                    	# [1] scanf
.LBB6_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	11	0
 #  10      //c = 3;
 #  11      printf("\nEnter elements of 1st matrix:\n");
	movq $(.rodata+112),%rdi      	# [0] .rodata+112
	call printf                   	# [0] printf
.LBB7_main:
	.loc	1	13	0
 #  12  
 #  13      for(i=0; i<r; ++i)
	movl 0(%rsp),%eax             	# [0] r
	xorl %edi,%edi                	# [3] 
	testl %eax,%eax               	# [3] 
	movq %rdi,120120(%rsp)        	# [4] _temp_gra_spill14
	jle .Lt_0_13570               	# [4] 
.LBB8_main:
	movl 4(%rsp),%r11d            	# [0] c
	leaq 16(%rsp),%r15            	# [0] a
	movl $1,%r14d                 	# [0] 
.Lt_0_14082:
 #<loop> Loop body line 13, nesting depth: 1, estimated iterations: 100
	xorl %r13d,%r13d              	# [0] 
	.loc	1	14	0
 #  14          for(j=0; j<c; ++j)
	testl %r11d,%r11d             	# [0] 
	jle .Lt_0_14338               	# [1] 
.LBB10_main:
 #<loop> Part of loop body line 13, head labeled .Lt_0_14082
	movl $1,%ebx                  	# [0] 
	movl %r14d,%ebp               	# [0] 
	movq %r15,%r12                	# [0] 
.Lt_0_14850:
 #<loop> Loop body line 14, nesting depth: 2, estimated iterations: 100
	xorl %eax,%eax                	# [0] 
	.loc	1	16	0
 #  15          {
 #  16              printf("Enter element a%d%d: ",i+1,j+1);
	movl %ebx,%edx                	# [0] 
	movl %r14d,%esi               	# [1] 
	movq $(.rodata+144),%rdi      	# [1] .rodata+144
	call printf                   	# [1] printf
.LBB12_main:
 #<loop> Part of loop body line 14, head labeled .Lt_0_14850
	.loc	1	18	0
 #  17              //scanf("%d",&a[i][j]);
 #  18              a[i][j] = (i+1)*(j+1);
	movl 4(%rsp),%r11d            	# [0] c
	movl %ebp,0(%r12)             	# [2] id:53 a+0x0
	addl %r14d,%ebp               	# [2] 
	addl $1,%r13d                 	# [2] 
	addl $1,%ebx                  	# [3] 
	addq $4,%r12                  	# [3] 
	cmpl %r11d,%r13d              	# [3] 
	jl .Lt_0_14850                	# [4] 
.LBB13_main:
 #<loop> Part of loop body line 13, head labeled .Lt_0_14082
	movl 0(%rsp),%eax             	# [0] r
.Lt_0_14338:
 #<loop> Part of loop body line 13, head labeled .Lt_0_14082
	movq 120120(%rsp),%rdi        	# [0] _temp_gra_spill14
	incl %edi                     	# [3] 
	addl $1,%r14d                 	# [4] 
	addq $400,%r15                	# [4] 
	cmpl %eax,%edi                	# [4] 
	movq %rdi,120120(%rsp)        	# [5] _temp_gra_spill14
	jl .Lt_0_14082                	# [5] 
.Lt_0_13570:
	xorl %eax,%eax                	# [0] 
	.loc	1	24	0
 #  20  
 #  21  
 #  22  
 #  23  
 #  24      printf("Enter elements of 2nd matrix:\n");
	movq $(.rodata+176),%rdi      	# [0] .rodata+176
	call printf                   	# [0] printf
.LBB16_main:
	.loc	1	25	0
 #  25      for(i=0; i<r; ++i)
	movl 0(%rsp),%r8d             	# [0] r
	xorl %eax,%eax                	# [3] 
	testl %r8d,%r8d               	# [3] 
	movq %rax,120112(%rsp)        	# [4] _temp_gra_spill13
	jle .Lt_0_15618               	# [4] 
.LBB17_main:
	movl 4(%rsp),%r11d            	# [0] c
	leaq 40016(%rsp),%r15         	# [0] b
	movl $1,%r14d                 	# [0] 
.Lt_0_16130:
 #<loop> Loop body line 25, nesting depth: 1, estimated iterations: 100
	xorl %r13d,%r13d              	# [0] 
	.loc	1	26	0
 #  26          for(j=0; j<c; ++j)
	testl %r11d,%r11d             	# [0] 
	jle .Lt_0_16386               	# [1] 
.LBB19_main:
 #<loop> Part of loop body line 25, head labeled .Lt_0_16130
	movl $1,%ebx                  	# [0] 
	movl %r14d,%r12d              	# [0] 
	movq %r15,%rbp                	# [0] 
.Lt_0_16898:
 #<loop> Loop body line 26, nesting depth: 2, estimated iterations: 100
	xorl %eax,%eax                	# [0] 
	.loc	1	28	0
 #  27          {
 #  28              printf("Enter element a%d%d: ",i+1, j+1);
	movl %ebx,%edx                	# [0] 
	movl %r14d,%esi               	# [1] 
	movq $(.rodata+144),%rdi      	# [1] .rodata+144
	call printf                   	# [1] printf
.LBB21_main:
 #<loop> Part of loop body line 26, head labeled .Lt_0_16898
	.loc	1	30	0
 #  29              //scanf("%d", &b[i][j]);
 #  30              b[i][j] = (i+1)*(j+1);
	movl 4(%rsp),%r11d            	# [0] c
	movl %r12d,0(%rbp)            	# [2] id:54 b+0x0
	addl %r14d,%r12d              	# [2] 
	addl $1,%r13d                 	# [2] 
	addl $1,%ebx                  	# [3] 
	addq $4,%rbp                  	# [3] 
	cmpl %r11d,%r13d              	# [3] 
	jl .Lt_0_16898                	# [4] 
.LBB22_main:
 #<loop> Part of loop body line 25, head labeled .Lt_0_16130
	movl 0(%rsp),%r8d             	# [0] r
.Lt_0_16386:
 #<loop> Part of loop body line 25, head labeled .Lt_0_16130
	movq 120112(%rsp),%rax        	# [0] _temp_gra_spill13
	incl %eax                     	# [3] 
	addl $1,%r14d                 	# [4] 
	addq $400,%r15                	# [4] 
	cmpl %r8d,%eax                	# [4] 
	movq %rax,120112(%rsp)        	# [5] _temp_gra_spill13
	jl .Lt_0_16130                	# [5] 
.Lt_0_15618:
	leal -3(%r8),%eax             	# [0] 
	testl %eax,%eax               	# [2] 
	movq %rax,120096(%rsp)        	# [3] _temp_gra_spill11
	jl .Lt_0_17666                	# [3] 
.LBB25_main:
	.loc	1	35	0
 #  31          }
 #  32  
 #  33      // Adding Two matrices
 #  34  
 #  35      for(i=0;i<r;++i)
	movslq %r8d,%rax              	# [0] 
	imulq $1431655766,%rax        	# [1] 
	movl 4(%rsp),%r11d            	# [3] c
	xorl %r9d,%r9d                	# [5] 
	movl %r8d,%ecx                	# [6] 
	leal -1(%r11),%edx            	# [6] 
	shrq $32,%rax                 	# [6] 
	sarl $31,%ecx                 	# [7] 
	leal -4(%r11),%ebp            	# [7] 
	movl %eax,%eax                	# [7] 
	movq %r9,120104(%rsp)         	# [8] _temp_gra_spill12
	subl %ecx,%eax                	# [8] 
	testl %edx,%edx               	# [8] 
	movq %rax,120072(%rsp)        	# [9] _temp_gra_spill8
	setge %sil                    	# [9] 
	testl %ebp,%ebp               	# [9] 
	setge %dil                    	# [10] 
	leaq 40016(%rsp),%r15         	# [10] b
	leaq 16(%rsp),%r14            	# [10] a
	leaq 80016(%rsp),%r13         	# [11] sum
	movzbl %sil,%esi              	# [11] 
	movzbl %dil,%edi              	# [11] 
	movq %rsi,120088(%rsp)        	# [12] _temp_gra_spill10
	movq %rdi,120080(%rsp)        	# [12] _temp_gra_spill9
	jmp .Lt_0_18434               	# [12] 
.LBB92_main:
 #<loop> Part of loop body line 35, head labeled .Lt_0_18434
	movl %r8d,%r9d                	# [0] 
	sarl $2,%r9d                  	# [1] 
	testl %r9d,%r9d               	# [2] 
	je .LBB90_main                	# [3] 
.LBB97_main:
 #<loop> Part of loop body line 35, head labeled .Lt_0_18434
	movl %ebx,%ecx                	# [0] 
.LBB91_main:
 #<loop> Loop body line 36, nesting depth: 2, estimated iterations: 25
 #<loop> vectorized
 #<loop> unrolled 4 times
	.loc	1	38	0
 #  36          for(j=0;j<c;++j)
 #  37          {
 #  38              sum[i][j]=a[i][j]+b[i][j];
	movdqa 432(%rax),%xmm15       	# [0] id:58 a+0x0
	movdqa 432(%rdx),%xmm8        	# [1] id:59 b+0x0
	movdqa 400(%rax),%xmm9        	# [2] id:58 a+0x0
	movdqa 400(%rdx),%xmm11       	# [3] id:59 b+0x0
	paddd %xmm15,%xmm8            	# [5] 
	paddd %xmm9,%xmm11            	# [6] 
	movdqa 0(%rax),%xmm13         	# [7] id:55 a+0x0
	movdqa 16(%rax),%xmm4         	# [8] id:55 a+0x0
	movdqa 32(%rax),%xmm1         	# [9] id:55 a+0x0
	movdqa 416(%rax),%xmm3        	# [10] id:58 a+0x0
	movdqa 800(%rax),%xmm5        	# [11] id:61 a+0x0
	movdqa 0(%rdx),%xmm0          	# [12] id:56 b+0x0
	movdqa 16(%rdx),%xmm14        	# [13] id:56 b+0x0
	movdqa 32(%rdx),%xmm12        	# [14] id:56 b+0x0
	movdqa 48(%rdx),%xmm9         	# [15] id:56 b+0x0
	movdqa 416(%rdx),%xmm10       	# [16] id:59 b+0x0
	movdqa 800(%rdx),%xmm7        	# [17] id:62 b+0x0
	movdqa 816(%rdx),%xmm6        	# [18] id:62 b+0x0
	paddd %xmm13,%xmm0            	# [19] 
	movdqa 48(%rax),%xmm13        	# [20] id:55 a+0x0
	paddd %xmm5,%xmm7             	# [21] 
	movdqa 448(%rax),%xmm5        	# [22] id:58 a+0x0
	movdqa 816(%rax),%xmm2        	# [23] id:61 a+0x0
	paddd %xmm4,%xmm14            	# [24] 
	movdqa 832(%rax),%xmm4        	# [25] id:61 a+0x0
	addq $64,%rsi                 	# [26] 
	paddd %xmm1,%xmm12            	# [26] 
	prefetcht0 64(%rsi)           	# [27] L1
	movdqa 848(%rax),%xmm1        	# [27] id:61 a+0x0
	movdqa %xmm0,-64(%rsi)        	# [27] id:57 sum+0x0
	prefetcht0 464(%rsi)          	# [28] L1
	paddd %xmm3,%xmm10            	# [28] 
	movdqa %xmm14,-48(%rsi)       	# [28] id:57 sum+0x0
	prefetcht0 864(%rsi)          	# [29] L1
	paddd %xmm13,%xmm9            	# [29] 
	movdqa %xmm12,-32(%rsi)       	# [29] id:57 sum+0x0
	prefetcht0 128(%rax)          	# [30] L1
	movdqa 448(%rdx),%xmm3        	# [30] id:59 b+0x0
	movdqa %xmm9,-16(%rsi)        	# [30] id:57 sum+0x0
	prefetcht0 528(%rax)          	# [31] L1
	paddd %xmm2,%xmm6             	# [31] 
	movdqa %xmm11,336(%rsi)       	# [31] id:60 sum+0x0
	prefetcht0 928(%rax)          	# [32] L1
	movdqa 832(%rdx),%xmm2        	# [32] id:62 b+0x0
	movdqa %xmm10,352(%rsi)       	# [32] id:60 sum+0x0
	prefetcht0 128(%rdx)          	# [33] L1
	paddd %xmm5,%xmm3             	# [33] 
	movdqa %xmm8,368(%rsi)        	# [33] id:60 sum+0x0
	prefetcht0 528(%rdx)          	# [34] L1
	movdqa %xmm3,384(%rsi)        	# [34] id:60 sum+0x0
	movdqa 848(%rdx),%xmm0        	# [34] id:62 b+0x0
	addq $64,%rdx                 	# [35] 
	addq $64,%rax                 	# [35] 
	movdqa %xmm7,736(%rsi)        	# [35] id:63 sum+0x0
	addl $16,%ecx                 	# [36] 
	paddd %xmm4,%xmm2             	# [36] 
	movdqa %xmm6,752(%rsi)        	# [36] id:63 sum+0x0
	cmpl %ebp,%ecx                	# [37] 
	movdqa %xmm2,768(%rsi)        	# [37] id:63 sum+0x0
	paddd %xmm1,%xmm0             	# [37] 
	prefetcht0 864(%rdx)          	# [38] L1
	movdqa %xmm0,784(%rsi)        	# [38] id:63 sum+0x0
	jle .LBB91_main               	# [38] 
.LBB98_main:
 #<loop> Part of loop body line 35, head labeled .Lt_0_18434
	movl %ecx,%ebx                	# [0] 
.LBB90_main:
 #<loop> Part of loop body line 35, head labeled .Lt_0_18434
	.loc	1	36	0
	cmpl %r11d,%ebx               	# [0] 
	jge .Lt_0_33026               	# [1] 
.Lt_0_20738:
 #<loop> Part of loop body line 35, head labeled .Lt_0_18434
	movslq %ebx,%rdx              	# [0] 
	leaq 0(%r13,%rdx,4), %rsi     	# [1] 
	leaq 0(%r14,%rdx,4), %rax     	# [1] 
	leaq 0(%r15,%rdx,4), %rdx     	# [1] 
.Lt_0_20994:
 #<loop> Loop body line 36, nesting depth: 2, estimated iterations: 3
	.loc	1	38	0
	prefetcht0 128(%rsi)          	# [0] L1
	prefetcht0 528(%rsi)          	# [0] L1
	prefetcht0 928(%rsi)          	# [0] L1
	prefetcht0 128(%rax)          	# [1] L1
	movl 0(%rax),%r8d             	# [1] id:64 a+0x0
	movl 0(%rdx),%r10d            	# [1] id:65 b+0x0
	prefetcht0 528(%rax)          	# [2] L1
	prefetcht0 928(%rax)          	# [2] L1
	prefetcht0 128(%rdx)          	# [2] L1
	prefetcht0 528(%rdx)          	# [3] L1
	movl 400(%rax),%r9d           	# [3] id:67 a+0x0
	movl 400(%rdx),%ecx           	# [3] id:68 b+0x0
	movl 800(%rdx),%edi           	# [4] id:71 b+0x0
	addl %r8d,%r10d               	# [4] 
	movl 800(%rax),%r8d           	# [4] id:70 a+0x0
	prefetcht0 928(%rdx)          	# [5] L1
	addq $4,%rdx                  	# [5] 
	addq $4,%rax                  	# [5] 
	addq $4,%rsi                  	# [6] 
	addl $1,%ebx                  	# [6] 
	addl %r9d,%ecx                	# [6] 
	movl %ecx,396(%rsi)           	# [7] id:69 sum+0x0
	addl %r8d,%edi                	# [7] 
	cmpl %r11d,%ebx               	# [7] 
	movl %r10d,-4(%rsi)           	# [8] id:66 sum+0x0
	movl %edi,796(%rsi)           	# [8] id:72 sum+0x0
	jl .Lt_0_20994                	# [8] 
.Lt_0_33026:
.Lt_0_20226:
.Lt_0_28418:
.Lt_0_29698:
 #<loop> Part of loop body line 35, head labeled .Lt_0_18434
	movq 120104(%rsp),%rax        	# [0] _temp_gra_spill12
	movq 120096(%rsp),%rdi        	# [1] _temp_gra_spill11
	addq $1200,%r15               	# [3] 
	addl $3,%eax                  	# [3] 
	addq $1200,%r14               	# [4] 
	addq $1200,%r13               	# [4] 
	cmpl %edi,%eax                	# [4] 
	movq %rax,120104(%rsp)        	# [5] _temp_gra_spill12
	jg .Lt_0_28674                	# [5] 
.Lt_0_18434:
 #<loop> Loop body line 35, nesting depth: 1, estimated iterations: 100
	.loc	1	35	0
	movq 120088(%rsp),%rax        	# [0] _temp_gra_spill10
	cmpl $0,%eax                  	# [3] 
	je .Lt_0_33026                	# [4] 
.LBB28_main:
 #<loop> Part of loop body line 35, head labeled .Lt_0_18434
	movq 120080(%rsp),%rax        	# [0] _temp_gra_spill9
	cmpl $0,%eax                  	# [3] 
	je .Lt_0_18946                	# [4] 
.LBB29_main:
 #<loop> Part of loop body line 35, head labeled .Lt_0_18434
	.loc	1	36	0
	movl %r11d,%r8d               	# [0] 
	sarl $31,%r8d                 	# [1] 
	andl $3,%r8d                  	# [2] 
	addl %r11d,%r8d               	# [3] 
	sarl $2,%r8d                  	# [4] 
	movq %r13,%rsi                	# [5] 
	movl %r8d,%ecx                	# [5] 
	xorl %ebx,%ebx                	# [6] 
	andl $3,%ecx                  	# [6] 
	testl $3,%r8d                 	# [6] 
	movq %r14,%rax                	# [7] 
	movq %r15,%rdx                	# [7] 
	je .LBB92_main                	# [7] 
.LBB93_main:
 #<loop> Part of loop body line 35, head labeled .Lt_0_18434
	.loc	1	38	0
	movdqa 0(%r14),%xmm5          	# [0] id:55 a+0x0
	movdqa 400(%r14),%xmm3        	# [1] id:58 a+0x0
	movdqa 800(%r14),%xmm1        	# [2] id:61 a+0x0
	movdqa 0(%r15),%xmm4          	# [3] id:56 b+0x0
	movdqa 400(%r15),%xmm2        	# [4] id:59 b+0x0
	leaq 16(%r15),%rdx            	# [5] 
	leaq 16(%r14),%rax            	# [5] 
	movdqa 800(%r15),%xmm0        	# [5] id:62 b+0x0
	leaq 16(%r13),%rsi            	# [6] 
	addl $4,%ebx                  	# [6] 
	paddd %xmm5,%xmm4             	# [6] 
	decl %ecx                     	# [7] 
	paddd %xmm3,%xmm2             	# [7] 
	movdqa %xmm4,0(%r13)          	# [7] id:57 sum+0x0
	testl %ecx,%ecx               	# [8] 
	paddd %xmm1,%xmm0             	# [8] 
	movdqa %xmm2,400(%r13)        	# [8] id:60 sum+0x0
	movdqa %xmm0,800(%r13)        	# [9] id:63 sum+0x0
	je .LBB92_main                	# [9] 
.LBB94_main:
 #<loop> Part of loop body line 35, head labeled .Lt_0_18434
	movdqa 16(%r14),%xmm5         	# [0] id:55 a+0x0
	movdqa 416(%r14),%xmm3        	# [1] id:58 a+0x0
	movdqa 816(%r14),%xmm1        	# [2] id:61 a+0x0
	movdqa 16(%r15),%xmm4         	# [3] id:56 b+0x0
	addq $16,%rdx                 	# [4] 
	movdqa 416(%r15),%xmm2        	# [4] id:59 b+0x0
	addq $16,%rax                 	# [5] 
	addq $16,%rsi                 	# [5] 
	movdqa 816(%r15),%xmm0        	# [5] id:62 b+0x0
	addl $4,%ebx                  	# [6] 
	movl %ecx,%edi                	# [6] 
	paddd %xmm5,%xmm4             	# [6] 
	decl %edi                     	# [7] 
	paddd %xmm3,%xmm2             	# [7] 
	movdqa %xmm4,16(%r13)         	# [7] id:57 sum+0x0
	testl %edi,%edi               	# [8] 
	paddd %xmm1,%xmm0             	# [8] 
	movdqa %xmm2,416(%r13)        	# [8] id:60 sum+0x0
	movdqa %xmm0,816(%r13)        	# [9] id:63 sum+0x0
	je .LBB92_main                	# [9] 
.LBB95_main:
 #<loop> Part of loop body line 35, head labeled .Lt_0_18434
	addq $16,%rax                 	# [0] 
	movdqa -16(%rax),%xmm5        	# [1] id:55 a+0x0
	movdqa 384(%rax),%xmm3        	# [2] id:58 a+0x0
	addq $16,%rdx                 	# [3] 
	movdqa 784(%rax),%xmm1        	# [3] id:61 a+0x0
	addq $16,%rsi                 	# [4] 
	addl $4,%ebx                  	# [4] 
	movdqa -16(%rdx),%xmm4        	# [4] id:56 b+0x0
	prefetcht0 112(%rsi)          	# [5] L1
	prefetcht0 512(%rsi)          	# [5] L1
	movdqa 384(%rdx),%xmm2        	# [5] id:59 b+0x0
	prefetcht0 912(%rsi)          	# [6] L1
	prefetcht0 112(%rax)          	# [6] L1
	movdqa 784(%rdx),%xmm0        	# [6] id:62 b+0x0
	prefetcht0 512(%rax)          	# [7] L1
	prefetcht0 912(%rax)          	# [7] L1
	paddd %xmm5,%xmm4             	# [7] 
	prefetcht0 112(%rdx)          	# [8] L1
	paddd %xmm3,%xmm2             	# [8] 
	movdqa %xmm4,-16(%rsi)        	# [8] id:57 sum+0x0
	prefetcht0 512(%rdx)          	# [9] L1
	paddd %xmm1,%xmm0             	# [9] 
	movdqa %xmm2,384(%rsi)        	# [9] id:60 sum+0x0
	prefetcht0 912(%rdx)          	# [10] L1
	movdqa %xmm0,784(%rsi)        	# [10] id:63 sum+0x0
	jmp .LBB92_main               	# [10] 
.Lt_0_18946:
 #<loop> Part of loop body line 35, head labeled .Lt_0_18434
	xorl %ebx,%ebx                	# [0] 
	.loc	1	36	0
	testl %r11d,%r11d             	# [0] 
	jg .Lt_0_20738                	# [1] 
.LBB116_main:
 #<loop> Part of loop body line 35, head labeled .Lt_0_18434
	jmp .Lt_0_33026               	# [0] 
.Lt_0_28674:
	.loc	1	35	0
	movl 0(%rsp),%edi             	# [0] r
	cmpl %edi,%eax                	# [3] 
	jge .Lt_0_33538               	# [4] 
.Lt_0_31234:
	movq 120088(%rsp),%rax        	# [0] _temp_gra_spill10
	cmpl $0,%eax                  	# [3] 
	movl 0(%rsp),%r8d             	# [4] r
	je .Lt_0_33538                	# [4] 
.LBB47_main:
	movq 120104(%rsp),%rsi        	# [0] _temp_gra_spill12
	leal -4(%r11),%ebp            	# [1] 
	movl %r8d,%eax                	# [2] 
	movslq %esi,%r13              	# [3] 
	subl %esi,%eax                	# [3] 
	testl %ebp,%ebp               	# [3] 
	setge %dil                    	# [4] 
	imulq $400,%r13               	# [4] 
	movzbl %dil,%edi              	# [7] 
	movl %esi,%r15d               	# [8] 
	movq %rax,120064(%rsp)        	# [8] _temp_gra_spill7
	movq %rdi,120080(%rsp)        	# [8] _temp_gra_spill9
	leaq 80016(%rsp,%r13,1), %rbx 	# [9] sum
	leaq 16(%rsp,%r13,1), %r12    	# [9] a
	leaq 40016(%rsp,%r13,1), %r13 	# [9] b
.Lt_0_22786:
 #<loop> Loop body line 35, nesting depth: 1, estimated iterations: 2
	movq 120080(%rsp),%rax        	# [0] _temp_gra_spill9
	cmpl $0,%eax                  	# [3] 
	je .Lt_0_23042                	# [4] 
.LBB53_main:
 #<loop> Part of loop body line 35, head labeled .Lt_0_22786
	.loc	1	36	0
	movl %r11d,%r10d              	# [0] 
	sarl $31,%r10d                	# [1] 
	andl $3,%r10d                 	# [2] 
	addl %r11d,%r10d              	# [3] 
	sarl $2,%r10d                 	# [4] 
	movq %rbx,%rdx                	# [5] 
	movl %r10d,%ecx               	# [5] 
	xorl %r9d,%r9d                	# [6] 
	andl $3,%ecx                  	# [6] 
	testl $3,%r10d                	# [6] 
	movq %r12,%rax                	# [7] 
	movq %r13,%rsi                	# [7] 
	je .LBB102_main               	# [7] 
.LBB103_main:
 #<loop> Part of loop body line 35, head labeled .Lt_0_22786
	.loc	1	38	0
	movdqa 0(%r12),%xmm1          	# [0] id:73 a+0x0
	movdqa 0(%r13),%xmm0          	# [1] id:74 b+0x0
	leaq 16(%r13),%rsi            	# [2] 
	leaq 16(%r12),%rax            	# [3] 
	leaq 16(%rbx),%rdx            	# [3] 
	decl %ecx                     	# [3] 
	addl $4,%r9d                  	# [4] 
	testl %ecx,%ecx               	# [4] 
	paddd %xmm1,%xmm0             	# [4] 
	movdqa %xmm0,0(%rbx)          	# [5] id:75 sum+0x0
	je .LBB102_main               	# [5] 
.LBB104_main:
 #<loop> Part of loop body line 35, head labeled .Lt_0_22786
	movdqa 16(%r12),%xmm1         	# [0] id:73 a+0x0
	movdqa 16(%r13),%xmm0         	# [1] id:74 b+0x0
	addq $16,%rsi                 	# [2] 
	movl %ecx,%edi                	# [2] 
	addq $16,%rax                 	# [3] 
	addq $16,%rdx                 	# [3] 
	decl %edi                     	# [3] 
	addl $4,%r9d                  	# [4] 
	testl %edi,%edi               	# [4] 
	paddd %xmm1,%xmm0             	# [4] 
	movdqa %xmm0,16(%rbx)         	# [5] id:75 sum+0x0
	je .LBB102_main               	# [5] 
.LBB105_main:
 #<loop> Part of loop body line 35, head labeled .Lt_0_22786
	movdqa 0(%rax),%xmm1          	# [0] id:73 a+0x0
	movdqa 0(%rsi),%xmm0          	# [1] id:74 b+0x0
	addq $16,%rsi                 	# [4] 
	addq $16,%rdx                 	# [4] 
	paddd %xmm1,%xmm0             	# [4] 
	addq $16,%rax                 	# [5] 
	addl $4,%r9d                  	# [5] 
	movdqa %xmm0,-16(%rdx)        	# [5] id:75 sum+0x0
.LBB102_main:
 #<loop> Part of loop body line 35, head labeled .Lt_0_22786
	movl %r10d,%r14d              	# [0] 
	sarl $2,%r14d                 	# [1] 
	testl %r14d,%r14d             	# [2] 
	je .LBB100_main               	# [3] 
.LBB107_main:
 #<loop> Part of loop body line 35, head labeled .Lt_0_22786
	movl %r9d,%ecx                	# [0] 
.LBB101_main:
 #<loop> Loop body line 36, nesting depth: 2, estimated iterations: 25
 #<loop> vectorized
 #<loop> unrolled 4 times
	movdqa 0(%rax),%xmm7          	# [0] id:73 a+0x0
	movdqa 16(%rax),%xmm5         	# [1] id:73 a+0x0
	movdqa 32(%rax),%xmm3         	# [2] id:73 a+0x0
	movdqa 48(%rax),%xmm1         	# [3] id:73 a+0x0
	movdqa 0(%rsi),%xmm6          	# [4] id:74 b+0x0
	movdqa 16(%rsi),%xmm4         	# [5] id:74 b+0x0
	movdqa 32(%rsi),%xmm2         	# [6] id:74 b+0x0
	movdqa 48(%rsi),%xmm0         	# [7] id:74 b+0x0
	addq $64,%rsi                 	# [8] 
	addq $64,%rdx                 	# [8] 
	paddd %xmm7,%xmm6             	# [8] 
	addq $64,%rax                 	# [9] 
	paddd %xmm5,%xmm4             	# [9] 
	movdqa %xmm6,-64(%rdx)        	# [9] id:75 sum+0x0
	addl $16,%ecx                 	# [10] 
	paddd %xmm3,%xmm2             	# [10] 
	movdqa %xmm4,-48(%rdx)        	# [10] id:75 sum+0x0
	cmpl %ebp,%ecx                	# [11] 
	paddd %xmm1,%xmm0             	# [11] 
	movdqa %xmm2,-32(%rdx)        	# [11] id:75 sum+0x0
	movdqa %xmm0,-16(%rdx)        	# [12] id:75 sum+0x0
	jle .LBB101_main              	# [12] 
.LBB108_main:
 #<loop> Part of loop body line 35, head labeled .Lt_0_22786
	movl %ecx,%r9d                	# [0] 
.LBB100_main:
 #<loop> Part of loop body line 35, head labeled .Lt_0_22786
	.loc	1	36	0
	cmpl %r11d,%r9d               	# [0] 
	jge .Lt_0_34050               	# [1] 
.Lt_0_24834:
 #<loop> Part of loop body line 35, head labeled .Lt_0_22786
	movslq %r9d,%rsi              	# [0] 
	leaq 0(%rbx,%rsi,4), %rdx     	# [1] 
	leaq 0(%r12,%rsi,4), %rax     	# [1] 
	leaq 0(%r13,%rsi,4), %rsi     	# [1] 
.Lt_0_25090:
 #<loop> Loop body line 36, nesting depth: 2, estimated iterations: 3
	.loc	1	38	0
	movl 0(%rax),%ecx             	# [0] id:76 a+0x0
	movl 0(%rsi),%edi             	# [0] id:77 b+0x0
	addq $4,%rsi                  	# [2] 
	addq $4,%rax                  	# [2] 
	addl $1,%r9d                  	# [2] 
	addq $4,%rdx                  	# [3] 
	addl %ecx,%edi                	# [3] 
	cmpl %r11d,%r9d               	# [3] 
	movl %edi,-4(%rdx)            	# [4] id:78 sum+0x0
	jl .Lt_0_25090                	# [4] 
.Lt_0_34050:
.Lt_0_24322:
.Lt_0_29186:
 #<loop> Part of loop body line 35, head labeled .Lt_0_22786
	addq $400,%r13                	# [0] 
	addl $1,%r15d                 	# [0] 
	addq $400,%r12                	# [1] 
	addq $400,%rbx                	# [1] 
	cmpl %r8d,%r15d               	# [1] 
	jl .Lt_0_22786                	# [2] 
.Lt_0_33538:
.Lt_0_21762:
.Lt_0_29954:
	xorl %eax,%eax                	# [0] 
	.loc	1	42	0
 #  39          }
 #  40  
 #  41      // Displaying the result
 #  42      printf("\nSum of two matrix is: \n\n");
	movq $(.rodata+208),%rdi      	# [0] .rodata+208
	call printf                   	# [0] printf
.LBB69_main:
	.loc	1	44	0
 #  43  
 #  44      for(i=0;i<r;++i)
	movl 0(%rsp),%eax             	# [0] r
	xorl %r13d,%r13d              	# [3] 
	testl %eax,%eax               	# [3] 
	jle .Lt_0_25858               	# [4] 
.LBB70_main:
	movl 4(%rsp),%r11d            	# [0] c
	leaq 80016(%rsp),%r12         	# [0] sum
	jmp .Lt_0_26370               	# [0] 
.LBB78_main:
 #<loop> Part of loop body line 44, head labeled .Lt_0_26370
	.loc	1	52	0
 #  48              printf("%d   ",sum[i][j]);
 #  49  
 #  50              if(j==c-1)
 #  51              {
 #  52                  printf("\n\n");
	movl 0(%rsp),%eax             	# [0] r
.Lt_0_26626:
 #<loop> Part of loop body line 44, head labeled .Lt_0_26370
	addl $1,%r13d                 	# [0] 
	addq $400,%r12                	# [1] 
	cmpl %eax,%r13d               	# [1] 
	jge .Lt_0_25858               	# [2] 
.Lt_0_26370:
 #<loop> Loop body line 44, nesting depth: 1, estimated iterations: 100
	xorl %ebp,%ebp                	# [0] 
	.loc	1	45	0
	testl %r11d,%r11d             	# [0] 
	jle .Lt_0_26626               	# [1] 
.LBB72_main:
 #<loop> Part of loop body line 44, head labeled .Lt_0_26370
	movq %r12,%rbx                	# [0] 
	jmp .Lt_0_27138               	# [0] 
.Lt_0_30210:
 #<loop> Part of loop body line 48, head labeled .Lt_0_27138
	.loc	1	52	0
	addl $1,%ebp                  	# [0] 
	addq $4,%rbx                  	# [1] 
	cmpl %r11d,%ebp               	# [1] 
	jge .LBB78_main               	# [2] 
.Lt_0_27138:
 #<loop> Loop body line 48
	xorl %eax,%eax                	# [0] 
	.loc	1	48	0
	movl 0(%rbx),%esi             	# [1] id:79 sum+0x0
	movq $(.rodata+240),%rdi      	# [1] .rodata+240
	call printf                   	# [1] printf
.LBB74_main:
 #<loop> Part of loop body line 48, head labeled .Lt_0_27138
	movl 4(%rsp),%r11d            	# [0] c
	leal -1(%r11),%eax            	# [3] 
	cmpl %eax,%ebp                	# [5] 
	jne .Lt_0_30210               	# [6] 
.LBB75_main:
 #<loop> Part of loop body line 48, head labeled .Lt_0_27138
	xorl %eax,%eax                	# [0] 
	.loc	1	52	0
	movq $(.rodata+256),%rdi      	# [0] .rodata+256
	call printf                   	# [0] printf
.LBB76_main:
 #<loop> Part of loop body line 48, head labeled .Lt_0_27138
	movl 4(%rsp),%r11d            	# [0] c
	jmp .Lt_0_30210               	# [0] 
.Lt_0_25858:
	xorq %rax,%rax                	# [0] 
	.loc	1	56	0
 #  53              }
 #  54          }
 #  55    
 #  56      return 0;
	movq 120056(%rsp),%rbx        	# [0] _temp_gra_spill6
	movq 120040(%rsp),%rbp        	# [0] _temp_gra_spill4
	movq 120032(%rsp),%r12        	# [1] _temp_gra_spill3
	movq 120048(%rsp),%r13        	# [1] _temp_gra_spill5
	movq 120024(%rsp),%r14        	# [1] _temp_gra_spill2
	movq 120016(%rsp),%r15        	# [2] _temp_gra_spill1
	addq $120136,%rsp             	# [2] 
	ret                           	# [2] 
.Lt_0_23042:
 #<loop> Part of loop body line 35, head labeled .Lt_0_22786
	xorl %r9d,%r9d                	# [0] 
	.loc	1	36	0
	testl %r11d,%r11d             	# [0] 
	jg .Lt_0_24834                	# [1] 
.LBB117_main:
 #<loop> Part of loop body line 35, head labeled .Lt_0_22786
	jmp .Lt_0_34050               	# [0] 
.Lt_0_17666:
	xorl %eax,%eax                	# [0] 
	.loc	1	35	0
	testl %r8d,%r8d               	# [0] 
	movq %rax,120104(%rsp)        	# [1] _temp_gra_spill12
	jle .Lt_0_33538               	# [1] 
.LBB49_main:
	movl 4(%rsp),%r11d            	# [0] c
	leal -1(%r11),%edi            	# [3] 
	testl %edi,%edi               	# [5] 
	setge %al                     	# [6] 
	movzbl %al,%eax               	# [7] 
	movq %rax,120088(%rsp)        	# [8] _temp_gra_spill10
	jmp .Lt_0_31234               	# [8] 
.L_0_35330:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter number of rows (between 1 and 100): "
	.org 0x30
	.align	0
	# offset 48
	.string "%d"
	.org 0x40
	.align	0
	# offset 64
	.string "Enter number of columns (between 1 and 100): "
	.org 0x70
	.align	0
	# offset 112
	.byte	0xa, 0x45, 0x6e, 0x74, 0x65, 0x72, 0x20, 0x65	# \nEnter e
	.byte	0x6c, 0x65, 0x6d, 0x65, 0x6e, 0x74, 0x73, 0x20	# lements 
	.byte	0x6f, 0x66, 0x20, 0x31, 0x73, 0x74, 0x20, 0x6d	# of 1st m
	.byte	0x61, 0x74, 0x72, 0x69, 0x78, 0x3a, 0xa, 0x0	# atrix:\n\000
	# 
	.org 0x90
	.align	0
	# offset 144
	.string "Enter element a%d%d: "
	.org 0xb0
	.align	0
	# offset 176
	.byte	0x45, 0x6e, 0x74, 0x65, 0x72, 0x20, 0x65, 0x6c	# Enter el
	.byte	0x65, 0x6d, 0x65, 0x6e, 0x74, 0x73, 0x20, 0x6f	# ements o
	.byte	0x66, 0x20, 0x32, 0x6e, 0x64, 0x20, 0x6d, 0x61	# f 2nd ma
	.byte	0x74, 0x72, 0x69, 0x78, 0x3a, 0xa, 0x0	# trix:\n\000
	.org 0xd0
	.align	0
	# offset 208
	.byte	0xa, 0x53, 0x75, 0x6d, 0x20, 0x6f, 0x66, 0x20	# \nSum of 
	.byte	0x74, 0x77, 0x6f, 0x20, 0x6d, 0x61, 0x74, 0x72	# two matr
	.byte	0x69, 0x78, 0x20, 0x69, 0x73, 0x3a, 0x20, 0xa	# ix is: \n
	.byte	0xa, 0x0	# \n\000
	.org 0xf0
	.align	0
	# offset 240
	.string "%d   "
	.org 0x100
	.align	0
	# offset 256
	.byte	0xa, 0xa, 0x0	# \n\n\000

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_35074-main
	.uleb128	.L_0_35330-.L_0_35074
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0xd0, 0xaa, 0x07, 0x04
	.4byte	.LEH_csr_main - .LEH_adjustsp_main
	.byte	0x8f, 0x10, 0x8e, 0x0f, 0x8c, 0x0e, 0x86, 0x0d
	.byte	0x8d, 0x0c, 0x83, 0x0b
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test48.c compiled with : -O3 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

