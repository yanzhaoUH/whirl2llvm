	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test50.c (test50.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test50.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 888, %rsp
	# a = 16
	# transpose = 416
	# r = 0
	# c = 4
	# i = 820
	# j = 816
	# _temp_gra_spill1 = 824
	# _temp_gra_spill2 = 832
	# _temp_gra_spill3 = 840
	# _temp_gra_spill4 = 848
	# _temp_gra_spill5 = 856
	# _temp_gra_spill6 = 864
	.loc	1	3	0
 #   1  #include <stdio.h>
 #   2  
 #   3  int main()
.LBB1_main:
.LEH_adjustsp_main:
.LEH_csr_main:
	addq $-888,%rsp               	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
	movq %rbx,864(%rsp)           	# [0] _temp_gra_spill6
	movq %rbp,848(%rsp)           	# [0] _temp_gra_spill4
	movq %r12,840(%rsp)           	# [0] _temp_gra_spill3
	movq %r13,856(%rsp)           	# [0] _temp_gra_spill5
	movq %r14,832(%rsp)           	# [0] _temp_gra_spill2
	movq %r15,824(%rsp)           	# [0] _temp_gra_spill1
.L_0_16642:
	xorl %eax,%eax                	# [0] 
	.loc	1	6	0
 #   4  {
 #   5      int a[10][10], transpose[10][10], r, c, i, j;
 #   6      printf("Enter rows and columns of matrix: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	7	0
 #   7      scanf("%d %d", &r, &c);
	leaq 4(%rsp),%rdx             	# [0] c
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	12	0
 #   8  //    r = 2; 
 #   9  //    c = 3;
 #  10  
 #  11      // Storing elements of the matrix
 #  12      printf("\nEnter elements of matrix:\n");
	movq $(.rodata+64),%rdi       	# [0] .rodata+64
	call printf                   	# [0] printf
.LBB5_main:
	movl 0(%rsp),%r10d            	# [0] r
	testl %r10d,%r10d             	# [3] 
	jle .Lt_0_5378                	# [4] 
.LBB6_main:
	movl 4(%rsp),%edx             	# [0] c
	xorl %eax,%eax                	# [0] 
	leaq 16(%rsp),%r15            	# [1] a
	movl $1,%r14d                 	# [1] 
	movl %eax,820(%rsp)           	# [1] i
.Lt_0_5890:
 #<loop> Loop body line 12, nesting depth: 1, estimated iterations: 100
	testl %edx,%edx               	# [0] 
	jle .Lt_0_6146                	# [1] 
.LBB8_main:
 #<loop> Part of loop body line 12, head labeled .Lt_0_5890
	xorl %r12d,%r12d              	# [0] 
	movq %r15,%r13                	# [1] 
	movl %r14d,%ebp               	# [1] 
	movl $1,%ebx                  	# [1] 
.Lt_0_6658:
 #<loop> Loop body line 16
	xorl %eax,%eax                	# [0] 
	.loc	1	16	0
 #  13      for(i=0; i<r; ++i)
 #  14          for(j=0; j<c; ++j)
 #  15          {
 #  16              printf("Enter element a%d%d: ",i+1, j+1);
	movl %ebx,%edx                	# [0] 
	movl %r14d,%esi               	# [1] 
	movq $(.rodata+96),%rdi       	# [1] .rodata+96
	call printf                   	# [1] printf
.LBB10_main:
 #<loop> Part of loop body line 16, head labeled .Lt_0_6658
	.loc	1	18	0
 #  17              //scanf("%d", &a[i][j]);
 #  18  		a[i][j] = (i+1)*(j+1);
	movl 4(%rsp),%edx             	# [0] c
	addq $4,%r13                  	# [1] 
	movl %ebp,-4(%r13)            	# [2] id:70 a+0x0
	addl $1,%r12d                 	# [2] 
	addl %r14d,%ebp               	# [3] 
	addl $1,%ebx                  	# [3] 
	cmpl %edx,%r12d               	# [3] 
	jl .Lt_0_6658                 	# [4] 
.LBB11_main:
 #<loop> Part of loop body line 12, head labeled .Lt_0_5890
	movl 0(%rsp),%r10d            	# [0] r
.Lt_0_6146:
 #<loop> Part of loop body line 12, head labeled .Lt_0_5890
	movl 820(%rsp),%eax           	# [0] i
	incl %eax                     	# [3] 
	addq $40,%r15                 	# [4] 
	addl $1,%r14d                 	# [4] 
	cmpl %eax,%r10d               	# [4] 
	movl %eax,820(%rsp)           	# [5] i
	jg .Lt_0_5890                 	# [5] 
.Lt_0_5378:
	xorl %eax,%eax                	# [0] 
	.loc	1	22	0
 #  19          }
 #  20  
 #  21      // Displaying the matrix a[][] */
 #  22      printf("\nEntered Matrix: \n");
	movq $(.rodata+128),%rdi      	# [0] .rodata+128
	call printf                   	# [0] printf
.LBB14_main:
	movl 0(%rsp),%r10d            	# [0] r
	testl %r10d,%r10d             	# [3] 
	jle .Lt_0_7426                	# [4] 
.LBB15_main:
	movl 4(%rsp),%edx             	# [0] c
	xorl %eax,%eax                	# [0] 
	leaq 16(%rsp),%r15            	# [1] a
	movl %eax,820(%rsp)           	# [1] i
	jmp .Lt_0_7938                	# [1] 
.LBB23_main:
 #<loop> Part of loop body line 22, head labeled .Lt_0_7938
	.loc	1	28	0
 #  24          for(j=0; j<c; ++j)
 #  25          {
 #  26              printf("%d  ", a[i][j]);
 #  27              if (j == c-1)
 #  28                  printf("\n\n");
	movl 0(%rsp),%r10d            	# [0] r
.Lt_0_8194:
 #<loop> Part of loop body line 22, head labeled .Lt_0_7938
	movl 820(%rsp),%eax           	# [0] i
	incl %eax                     	# [3] 
	addq $40,%r15                 	# [4] 
	cmpl %eax,%r10d               	# [4] 
	movl %eax,820(%rsp)           	# [5] i
	jle .Lt_0_7426                	# [5] 
.Lt_0_7938:
 #<loop> Loop body line 22, nesting depth: 1, estimated iterations: 100
	.loc	1	22	0
	testl %edx,%edx               	# [0] 
	jle .Lt_0_8194                	# [1] 
.LBB17_main:
 #<loop> Part of loop body line 22, head labeled .Lt_0_7938
	xorl %r12d,%r12d              	# [0] 
	movq %r15,%r13                	# [0] 
	jmp .Lt_0_8706                	# [0] 
.Lt_0_14082:
 #<loop> Part of loop body line 26, head labeled .Lt_0_8706
	.loc	1	28	0
	addl $1,%r12d                 	# [0] 
	addq $4,%r13                  	# [1] 
	cmpl %edx,%r12d               	# [1] 
	jge .LBB23_main               	# [2] 
.Lt_0_8706:
 #<loop> Loop body line 26
	xorl %eax,%eax                	# [0] 
	.loc	1	26	0
	movl 0(%r13),%esi             	# [1] id:71 a+0x0
	movq $(.rodata+160),%rdi      	# [1] .rodata+160
	call printf                   	# [1] printf
.LBB19_main:
 #<loop> Part of loop body line 26, head labeled .Lt_0_8706
	.loc	1	27	0
	movl 4(%rsp),%edx             	# [0] c
	leal -1(%rdx),%eax            	# [3] 
	cmpl %eax,%r12d               	# [5] 
	jne .Lt_0_14082               	# [6] 
.LBB20_main:
 #<loop> Part of loop body line 26, head labeled .Lt_0_8706
	xorl %eax,%eax                	# [0] 
	.loc	1	28	0
	movq $(.rodata+176),%rdi      	# [0] .rodata+176
	call printf                   	# [0] printf
.LBB21_main:
 #<loop> Part of loop body line 26, head labeled .Lt_0_8706
	movl 4(%rsp),%edx             	# [0] c
	jmp .Lt_0_14082               	# [0] 
.Lt_0_7426:
	testl %r10d,%r10d             	# [0] 
	jle .Lt_0_9730                	# [1] 
.LBB26_main:
	leaq 416(%rsp),%r11           	# [0] transpose
	leaq 16(%rsp),%r15            	# [0] a
	xorl %eax,%eax                	# [0] 
	movl 4(%rsp),%edx             	# [1] c
	movl %eax,820(%rsp)           	# [1] i
	jmp .Lt_0_10242               	# [1] 
.LBB51_main:
 #<loop> Part of loop body line 28, head labeled .Lt_0_10242
	movl %r8d,%ebx                	# [0] 
	sarl $2,%ebx                  	# [1] 
	testl %ebx,%ebx               	# [2] 
	je .Lt_0_10498                	# [3] 
.LBB56_main:
 #<loop> Part of loop body line 28, head labeled .Lt_0_10242
	movq %r13,%rax                	# [0] 
.LBB50_main:
 #<loop> Loop body line 28, nesting depth: 2, estimated iterations: 25
 #<loop> unrolled 4 times
	.loc	1	35	0
 #  31      // Finding the transpose of matrix a
 #  32      for(i=0; i<r; ++i)
 #  33          for(j=0; j<c; ++j)
 #  34          {
 #  35              transpose[j][i] = a[i][j];
	movl 0(%rax),%r9d             	# [0] id:72 a+0x0
	movl 4(%rax),%r8d             	# [0] id:72 a+0x0
	movl 8(%rax),%ecx             	# [1] id:72 a+0x0
	movl 12(%rax),%edi            	# [1] id:72 a+0x0
	addq $160,%rsi                	# [2] 
	addq $16,%rax                 	# [2] 
	addl $4,%r12d                 	# [2] 
	cmpl %edx,%r12d               	# [3] 
	movl %r8d,-120(%rsi)          	# [3] id:73 transpose+0x0
	movl %r9d,-160(%rsi)          	# [3] id:73 transpose+0x0
	movl %edi,-40(%rsi)           	# [4] id:73 transpose+0x0
	movl %ecx,-80(%rsi)           	# [4] id:73 transpose+0x0
	jl .LBB50_main                	# [4] 
.Lt_0_10498:
 #<loop> Part of loop body line 28, head labeled .Lt_0_10242
	movl 820(%rsp),%eax           	# [0] i
	incl %eax                     	# [3] 
	addq $40,%r15                 	# [4] 
	addq $4,%r11                  	# [4] 
	cmpl %eax,%r10d               	# [4] 
	movl %eax,820(%rsp)           	# [5] i
	jle .Lt_0_9730                	# [5] 
.Lt_0_10242:
 #<loop> Loop body line 28, nesting depth: 1, estimated iterations: 100
	.loc	1	28	0
	testl %edx,%edx               	# [0] 
	jle .Lt_0_10498               	# [1] 
.LBB29_main:
 #<loop> Part of loop body line 28, head labeled .Lt_0_10242
	movq %r15,%r13                	# [0] 
	movl %edx,%eax                	# [0] 
	xorl %r12d,%r12d              	# [1] 
	andl $3,%eax                  	# [1] 
	testl $3,%edx                 	# [1] 
	movq %r11,%rsi                	# [2] 
	movl %edx,%r8d                	# [2] 
	je .LBB51_main                	# [2] 
.LBB52_main:
 #<loop> Part of loop body line 28, head labeled .Lt_0_10242
	.loc	1	35	0
	movl 0(%r15),%edi             	# [0] id:72 a+0x0
	leaq 40(%r11),%rsi            	# [1] 
	decl %eax                     	# [1] 
	leaq 4(%r15),%r13             	# [2] 
	incl %r12d                    	# [2] 
	testl %eax,%eax               	# [2] 
	movl %edi,0(%r11)             	# [3] id:73 transpose+0x0
	je .LBB51_main                	# [3] 
.LBB53_main:
 #<loop> Part of loop body line 28, head labeled .Lt_0_10242
	movl 4(%r15),%edi             	# [0] id:72 a+0x0
	movl %eax,%ecx                	# [0] 
	addq $40,%rsi                 	# [1] 
	decl %ecx                     	# [1] 
	addq $4,%r13                  	# [2] 
	incl %r12d                    	# [2] 
	testl %ecx,%ecx               	# [2] 
	movl %edi,40(%r11)            	# [3] id:73 transpose+0x0
	je .LBB51_main                	# [3] 
.LBB54_main:
 #<loop> Part of loop body line 28, head labeled .Lt_0_10242
	movl 0(%r13),%eax             	# [0] id:72 a+0x0
	addq $40,%rsi                 	# [2] 
	addq $4,%r13                  	# [2] 
	incl %r12d                    	# [3] 
	movl %eax,-40(%rsi)           	# [3] id:73 transpose+0x0
	jmp .LBB51_main               	# [3] 
.Lt_0_9730:
	xorl %eax,%eax                	# [0] 
	.loc	1	39	0
 #  36          }
 #  37  
 #  38      // Displaying the transpose of matrix a
 #  39      printf("\nTranspose of Matrix:\n");
	movq $(.rodata+192),%rdi      	# [0] .rodata+192
	call printf                   	# [0] printf
.LBB34_main:
	movl 4(%rsp),%edx             	# [0] c
	testl %edx,%edx               	# [3] 
	jle .Lt_0_11778               	# [4] 
.LBB35_main:
	xorl %r13d,%r13d              	# [0] 
	movl 0(%rsp),%r10d            	# [1] r
	leaq 416(%rsp),%rbp           	# [1] transpose
	jmp .Lt_0_12290               	# [1] 
.LBB43_main:
 #<loop> Part of loop body line 39, head labeled .Lt_0_12290
	.loc	1	45	0
 #  41          for(j=0; j<r; ++j)
 #  42          {
 #  43              printf("%d  ",transpose[i][j]);
 #  44              if(j==r-1)
 #  45                  printf("\n\n");
	movl 4(%rsp),%edx             	# [0] c
.Lt_0_12546:
 #<loop> Part of loop body line 39, head labeled .Lt_0_12290
	addl $1,%r13d                 	# [0] 
	addq $40,%rbp                 	# [1] 
	cmpl %r13d,%edx               	# [1] 
	jle .Lt_0_11778               	# [2] 
.Lt_0_12290:
 #<loop> Loop body line 39, nesting depth: 1, estimated iterations: 100
	.loc	1	39	0
	testl %r10d,%r10d             	# [0] 
	jle .Lt_0_12546               	# [1] 
.LBB37_main:
 #<loop> Part of loop body line 39, head labeled .Lt_0_12290
	xorl %r12d,%r12d              	# [0] 
	movq %rbp,%rbx                	# [0] 
	jmp .Lt_0_13058               	# [0] 
.Lt_0_14338:
 #<loop> Part of loop body line 43, head labeled .Lt_0_13058
	.loc	1	45	0
	addl $1,%r12d                 	# [0] 
	addq $4,%rbx                  	# [1] 
	cmpl %r10d,%r12d              	# [1] 
	jge .LBB43_main               	# [2] 
.Lt_0_13058:
 #<loop> Loop body line 43
	xorl %eax,%eax                	# [0] 
	.loc	1	43	0
	movl 0(%rbx),%esi             	# [1] id:74 transpose+0x0
	movq $(.rodata+160),%rdi      	# [1] .rodata+160
	call printf                   	# [1] printf
.LBB39_main:
 #<loop> Part of loop body line 43, head labeled .Lt_0_13058
	.loc	1	44	0
	movl 0(%rsp),%r10d            	# [0] r
	leal -1(%r10),%eax            	# [3] 
	cmpl %eax,%r12d               	# [5] 
	jne .Lt_0_14338               	# [6] 
.LBB40_main:
 #<loop> Part of loop body line 43, head labeled .Lt_0_13058
	xorl %eax,%eax                	# [0] 
	.loc	1	45	0
	movq $(.rodata+176),%rdi      	# [0] .rodata+176
	call printf                   	# [0] printf
.LBB41_main:
 #<loop> Part of loop body line 43, head labeled .Lt_0_13058
	movl 0(%rsp),%r10d            	# [0] r
	jmp .Lt_0_14338               	# [0] 
.Lt_0_11778:
	xorq %rax,%rax                	# [0] 
	.loc	1	48	0
 #  46          }
 #  47  
 #  48      return 0;
	movq 864(%rsp),%rbx           	# [0] _temp_gra_spill6
	movq 848(%rsp),%rbp           	# [0] _temp_gra_spill4
	movq 840(%rsp),%r12           	# [1] _temp_gra_spill3
	movq 856(%rsp),%r13           	# [1] _temp_gra_spill5
	movq 832(%rsp),%r14           	# [1] _temp_gra_spill2
	movq 824(%rsp),%r15           	# [2] _temp_gra_spill1
	addq $888,%rsp                	# [2] 
	ret                           	# [2] 
.L_0_16898:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter rows and columns of matrix: "
	.org 0x30
	.align	0
	# offset 48
	.string "%d %d"
	.org 0x40
	.align	0
	# offset 64
	.byte	0xa, 0x45, 0x6e, 0x74, 0x65, 0x72, 0x20, 0x65	# \nEnter e
	.byte	0x6c, 0x65, 0x6d, 0x65, 0x6e, 0x74, 0x73, 0x20	# lements 
	.byte	0x6f, 0x66, 0x20, 0x6d, 0x61, 0x74, 0x72, 0x69	# of matri
	.byte	0x78, 0x3a, 0xa, 0x0	# x:\n\000
	.org 0x60
	.align	0
	# offset 96
	.string "Enter element a%d%d: "
	.org 0x80
	.align	0
	# offset 128
	.byte	0xa, 0x45, 0x6e, 0x74, 0x65, 0x72, 0x65, 0x64	# \nEntered
	.byte	0x20, 0x4d, 0x61, 0x74, 0x72, 0x69, 0x78, 0x3a	#  Matrix:
	.byte	0x20, 0xa, 0x0	#  \n\000
	.org 0xa0
	.align	0
	# offset 160
	.string "%d  "
	.org 0xb0
	.align	0
	# offset 176
	.byte	0xa, 0xa, 0x0	# \n\n\000
	.org 0xc0
	.align	0
	# offset 192
	.byte	0xa, 0x54, 0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f	# \nTranspo
	.byte	0x73, 0x65, 0x20, 0x6f, 0x66, 0x20, 0x4d, 0x61	# se of Ma
	.byte	0x74, 0x72, 0x69, 0x78, 0x3a, 0xa, 0x0	# trix:\n\000

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_16642-main
	.uleb128	.L_0_16898-.L_0_16642
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x80, 0x07, 0x04
	.4byte	.LEH_csr_main - .LEH_adjustsp_main
	.byte	0x8f, 0x09, 0x8e, 0x08, 0x8c, 0x07, 0x86, 0x06
	.byte	0x8d, 0x05, 0x83, 0x04
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test50.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

