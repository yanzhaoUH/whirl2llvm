#include <stdio.h>
int main()
{
    float firstNumber, secondNumber, productOfTwoNumbers;
    printf("Enter two numbers: ");

    // Stores two floating point numbers in variable firstNumber and secondNumber respectively
     scanf("%f %f",&firstNumber, &secondNumber);  
    //firstNumber = 1.1f;
    //secondNumber = 2.2f;
     
    // Performs multiplication and stores the result in variable productOfTwoNumbers
    productOfTwoNumbers = firstNumber * secondNumber;  

    // Result up to 2 decimal point is displayed using %.2lf
    printf("Product = %.2lf", productOfTwoNumbers);
    
    return 0;
}
