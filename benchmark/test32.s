	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test32.c (test32.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test32.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 56, %rsp
	# i = 4
	# j = 8
	# rows = 0
	# _temp_gra_spill1 = 16
	# _temp_gra_spill2 = 24
	# _temp_gra_spill3 = 32
	.loc	1	2	0
 #   1  #include <stdio.h>
 #   2  int main()
.LBB1_main:
.LEH_adjustsp_main:
.LEH_csr_main:
	addq $-56,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
	movq %rbx,32(%rsp)            	# [0] _temp_gra_spill3
	movq %rbp,24(%rsp)            	# [0] _temp_gra_spill2
	movq %r12,16(%rsp)            	# [0] _temp_gra_spill1
.L_0_3842:
	xorl %eax,%eax                	# [0] 
	.loc	1	6	0
 #   3  {
 #   4      int i, j, rows;
 #   5  
 #   6      printf("Enter the number of rows: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	7	0
 #   7      scanf("%d",&rows);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	movl 0(%rsp),%eax             	# [0] rows
	cmpl $1,%eax                  	# [3] 
	jl .Lt_0_1282                 	# [4] 
.LBB5_main:
	movl $1,%ebp                  	# [0] 
.Lt_0_1794:
 #<loop> Loop body line 7, nesting depth: 1, estimated iterations: 100
	cmpl $1,%ebp                  	# [0] 
	jl .Lt_0_2050                 	# [1] 
.LBB7_main:
 #<loop> Part of loop body line 7, head labeled .Lt_0_1794
	movl $1,%ebx                  	# [0] 
.Lt_0_2562:
 #<loop> Loop body line 7, nesting depth: 2, estimated iterations: 100
	xorl %eax,%eax                	# [0] 
	.loc	1	14	0
 #  10      for(i=1; i<=rows; ++i)
 #  11      {
 #  12          for(j=1; j<=i; ++j)
 #  13          {
 #  14              printf("%d ",j);
	movl %ebx,%esi                	# [1] 
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call printf                   	# [1] printf
.LBB10_main:
 #<loop> Part of loop body line 7, head labeled .Lt_0_2562
	addl $1,%ebx                  	# [0] 
	cmpl %ebp,%ebx                	# [1] 
	jle .Lt_0_2562                	# [2] 
.Lt_0_2050:
 #<loop> Part of loop body line 7, head labeled .Lt_0_1794
	xorl %eax,%eax                	# [0] 
	.loc	1	16	0
 #  15          }
 #  16          printf("\n");
	movq $(.rodata+64),%rdi       	# [0] .rodata+64
	call printf                   	# [0] printf
.LBB12_main:
 #<loop> Part of loop body line 7, head labeled .Lt_0_1794
	movl 0(%rsp),%eax             	# [0] rows
	addl $1,%ebp                  	# [2] 
	cmpl %ebp,%eax                	# [3] 
	jge .Lt_0_1794                	# [4] 
.Lt_0_1282:
	xorq %rax,%rax                	# [0] 
	.loc	1	18	0
 #  17      }
 #  18      return 0;
	movq 32(%rsp),%rbx            	# [0] _temp_gra_spill3
	movq 24(%rsp),%rbp            	# [0] _temp_gra_spill2
	movq 16(%rsp),%r12            	# [1] _temp_gra_spill1
	addq $56,%rsp                 	# [1] 
	ret                           	# [1] 
.L_0_4098:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter the number of rows: "
	.org 0x20
	.align	0
	# offset 32
	.string "%d"
	.org 0x30
	.align	0
	# offset 48
	.string "%d "
	.org 0x40
	.align	0
	# offset 64
	.byte	0xa, 0x0	# \n\000

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_3842-main
	.uleb128	.L_0_4098-.L_0_3842
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x40, 0x04
	.4byte	.LEH_csr_main - .LEH_adjustsp_main
	.byte	0x8c, 0x06, 0x86, 0x05, 0x83, 0x04
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test32.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

