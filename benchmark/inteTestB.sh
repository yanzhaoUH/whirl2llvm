#!/bin/bash
#testFiles=("test1" "test2" "test3" "test4" "test-basic" "test-type1" "test-type2" )

testFiles=(1)
#testFiles=($(seq 70) 333 334 335 336 337 338 339)


#1 produce whirl IR file
for i in "${testFiles[@]}"
do
        
        CNAME="test$i"
        BNAME="test$i.B"
        rm $BNAME
        echo $CNAME; 
	./createWhirl.sh $CNAME
        if [ $? -ne 0 ]
	    then
	       exit 1
	    fi         

        UHNAME="test$i.uh"
        rm "./B/$UHNANE"
        mv $UHNAME ./B
        rm $UHNAME
done



#2 call whirl2llvm application to produce .bc file
for i in "${testFiles[@]}"
do
   bcFileName="test$i.bc"
   rm $bcFileName
   BfileName="test$i.B"
	../whirl2llvm $BfileName

   llFileName="test$i.ll"
    
   llvm-dis $bcFileName -o ./B/$llFileName
done


#3 call lli command to test .bc is right

for i in "${testFiles[@]}"
do

	BCfileName="test$i.bc"
        #echo $BCfileName
        if [ $i -eq 43 ]
        then
          echo "hello world" | lli $BCfileName >resultmy
          echo "hello world" | lli "./L/test$i.ll" >resultright
        else
	  lli $BCfileName >resultmy
          lli "./L/test$i.ll" >resultright
        fi   

	diff resultmy resultright > /dev/null
	    output=$?
	    if [ "$output" -ne 0 ]
	    then
               printf "***"
               echo "test$i fail!!!***"
               meld resultmy resultright &
	       exit
            else
	       printf "***"
               echo "test$i pass!!!***"
	    fi 
done



