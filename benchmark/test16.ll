; ModuleID = 'test16.bc'

@.str = private constant [27 x i8] c"Enter a positive integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [9 x i8] c"Sum = %d\00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_75 = alloca i32
  %.preg_I4_4_71 = alloca i32
  %.preg_V16I4_28_73 = alloca <4 x i32>
  %.preg_V16I4_28_69 = alloca <4 x i32>
  %.preg_I4_4_74 = alloca i32
  %.preg_I4_4_66 = alloca i32
  %.preg_I4_4_68 = alloca i32
  %.preg_V16I4_28_64 = alloca <4 x i32>
  %.preg_V16I4_28_62 = alloca <4 x i32>
  %.preg_V16I4_28_72 = alloca <4 x i32>
  %.preg_I4_4_70 = alloca i32
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_ehpit0_10 = alloca i32, align 4
  %i_5 = alloca i32, align 4
  %n_4 = alloca i32, align 4
  %old_frame_pointer_15.addr = alloca i64, align 8
  %return_address_16.addr = alloca i64, align 8
  %sum_6 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = load i32, i32* %n_4, align 4
  store i32 %3, i32* %.preg_I4_4_70, align 8
  %4 = load i32, i32* %.preg_I4_4_70
  %add = add i32 %4, -1
  %cmp1 = icmp sge i32 %add, 0
  br i1 %cmp1, label %tb, label %L1794

tb:                                               ; preds = %entry
  store <4 x i32> zeroinitializer, <4 x i32>* %.preg_V16I4_28_72, align 8
  %5 = load <4 x i32>, <4 x i32>* %.preg_V16I4_28_72
  store <4 x i32> %5, <4 x i32>* %.preg_V16I4_28_62, align 8
  store <4 x i32> zeroinitializer, <4 x i32>* %.preg_V16I4_28_64, align 8
  %6 = load i32, i32* %.preg_I4_4_70
  %add.1 = add i32 %6, -4
  store i32 %add.1, i32* %.preg_I4_4_68, align 8
  %7 = load i32, i32* %.preg_I4_4_68
  %cmp2 = icmp sge i32 %7, 0
  br i1 %cmp2, label %tb2, label %L2050

L1794:                                            ; preds = %entry
  store i32 0, i32* %.preg_I4_4_71, align 8
  br label %L5122

tb2:                                              ; preds = %tb
  store i32 0, i32* %.preg_I4_4_66, align 8
  %8 = load i32, i32* %.preg_I4_4_70
  %div = sdiv i32 %8, 4
  store i32 %div, i32* %.preg_I4_4_74, align 8
  store <4 x i32> <i32 1, i32 1, i32 1, i32 1>, <4 x i32>* %.preg_V16I4_28_69, align 8
  store <4 x i32> <i32 4, i32 4, i32 4, i32 4>, <4 x i32>* %.preg_V16I4_28_73, align 8
  br label %L2818

L2050:                                            ; preds = %tb
  store i32 0, i32* %.preg_I4_4_66, align 8
  br label %L5378

L2818:                                            ; preds = %L2818, %tb2
  %9 = load <4 x i32>, <4 x i32>* %.preg_V16I4_28_62
  %10 = load <4 x i32>, <4 x i32>* %.preg_V16I4_28_64
  %11 = load <4 x i32>, <4 x i32>* %.preg_V16I4_28_69
  %add.3 = add <4 x i32> %10, %11
  %add.4 = add <4 x i32> %9, %add.3
  store <4 x i32> %add.4, <4 x i32>* %.preg_V16I4_28_62, align 8
  %12 = load <4 x i32>, <4 x i32>* %.preg_V16I4_28_64
  %13 = load <4 x i32>, <4 x i32>* %.preg_V16I4_28_73
  %add.5 = add <4 x i32> %12, %13
  store <4 x i32> %add.5, <4 x i32>* %.preg_V16I4_28_64, align 8
  %14 = load i32, i32* %.preg_I4_4_66
  %add.6 = add i32 %14, 4
  store i32 %add.6, i32* %.preg_I4_4_66, align 8
  %15 = load i32, i32* %.preg_I4_4_66
  %16 = load i32, i32* %.preg_I4_4_68
  %cmp3 = icmp sle i32 %15, %16
  br i1 %cmp3, label %L2818, label %fb

fb:                                               ; preds = %L2818
  br label %L4610

L4610:                                            ; preds = %fb
  %17 = load <4 x i32>, <4 x i32>* %.preg_V16I4_28_62
  %rdx.shuf = shufflevector <4 x i32> %17, <4 x i32> undef, <4 x i32> <i32 2, i32 3, i32 undef, i32 undef>
  %bin.rdx1 = add <4 x i32> %17, %rdx.shuf
  %rdx.shuf1 = shufflevector <4 x i32> %bin.rdx1, <4 x i32> undef, <4 x i32> <i32 1, i32 undef, i32 undef, i32 undef>
  %bin.rdx2 = add <4 x i32> %bin.rdx1, %rdx.shuf1
  %18 = extractelement <4 x i32> %bin.rdx2, i32 0
  store i32 %18, i32* %.preg_I4_4_71, align 8
  %19 = load i32, i32* %.preg_I4_4_70
  %20 = load i32, i32* %.preg_I4_4_66
  %cmp4 = icmp sgt i32 %19, %20
  br i1 %cmp4, label %tb7, label %L5890

L5378:                                            ; preds = %L2050
  %21 = load <4 x i32>, <4 x i32>* %.preg_V16I4_28_72
  %rdx.shuf8 = shufflevector <4 x i32> %21, <4 x i32> undef, <4 x i32> <i32 2, i32 3, i32 undef, i32 undef>
  %bin.rdx1.9 = add <4 x i32> %21, %rdx.shuf8
  %rdx.shuf110 = shufflevector <4 x i32> %bin.rdx1.9, <4 x i32> undef, <4 x i32> <i32 1, i32 undef, i32 undef, i32 undef>
  %bin.rdx2.11 = add <4 x i32> %bin.rdx1.9, %rdx.shuf110
  %22 = extractelement <4 x i32> %bin.rdx2.11, i32 0
  store i32 %22, i32* %.preg_I4_4_71, align 8
  %23 = load i32, i32* %.preg_I4_4_70
  %cmp5 = icmp sgt i32 %23, 0
  br i1 %cmp5, label %tb12, label %L3330

tb7:                                              ; preds = %L4610
  br label %L5634

L5890:                                            ; preds = %L4610
  br label %L3330

L5634:                                            ; preds = %tb12, %tb7
  br label %L3842

L3842:                                            ; preds = %L5634
  %24 = load i32, i32* %.preg_I4_4_70
  %25 = load i32, i32* %.preg_I4_4_66
  %add.13 = sub i32 %24, %25
  store i32 %add.13, i32* %.preg_I4_4_75, align 8
  br label %L4098

tb12:                                             ; preds = %L5378
  br label %L5634

L3330:                                            ; preds = %L5890, %L5378
  br label %L4866

L4098:                                            ; preds = %L4098, %L3842
  %26 = load i32, i32* %.preg_I4_4_66
  %27 = load i32, i32* %.preg_I4_4_71
  %add.14 = add i32 %26, %27
  %add.15 = add i32 %add.14, 1
  store i32 %add.15, i32* %.preg_I4_4_71, align 8
  %28 = load i32, i32* %.preg_I4_4_66
  %add.16 = add i32 %28, 1
  store i32 %add.16, i32* %.preg_I4_4_66, align 8
  %29 = load i32, i32* %.preg_I4_4_66
  %30 = load i32, i32* %.preg_I4_4_70
  %cmp6 = icmp slt i32 %29, %30
  br i1 %cmp6, label %L4098, label %fb17

fb17:                                             ; preds = %L4098
  br label %L4866

L4866:                                            ; preds = %fb17, %L3330
  br label %L5122

L5122:                                            ; preds = %L4866, %L1794
  %31 = bitcast i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.2, i32 0, i32 0) to i8*
  %32 = load i32, i32* %.preg_I4_4_71
  %call3 = call i32 (i8*, ...) @printf(i8* %31, i32 %32)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
