	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test69.c (test69.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test69.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 1016, %rsp
	# c = 0
	# _temp_gra_spill2 = 1000
	.loc	1	3	0
 #   1  #include <stdio.h>
 #   2  #include <stdlib.h> // For exit() function
 #   3  int main()
.LBB1_main:
.LEH_adjustsp_main:
.LEH_csr_main:
	addq $-1016,%rsp              	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
	movq %rbx,1000(%rsp)          	# [0] _temp_gra_spill2
.L_0_1026:
	.loc	1	8	0
 #   4  {
 #   5      char c[1000];
 #   6      FILE *fptr;
 #   7  
 #   8      if ((fptr = fopen("program.txt", "r")) == NULL)
	movq $(.rodata+16),%rsi       	# [0] .rodata+16
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	fopen
	call fopen                    	# [0] fopen
.LBB3_main:
	testq %rax,%rax               	# [0] 
	movq %rax,%rbx                	# [1] 
	jne .Lt_0_770                 	# [1] 
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	10	0
 #   9      {
 #  10          printf("Error! opening file");
	movq $(.rodata+32),%rdi       	# [0] .rodata+32
	.globl	printf
	call printf                   	# [0] printf
.LBB5_main:
	.loc	1	12	0
 #  11          // Program exits if file pointer returns NULL.
 #  12          exit(1);         
	movq $1,%rdi                  	# [0] 
	.globl	exit
	call exit                     	# [0] exit
.LBB6_main:
	movq 1000(%rsp),%rbx          	# [0] _temp_gra_spill2
	addq $1016,%rsp               	# [0] 
	ret                           	# [0] 
.Lt_0_770:
	.loc	1	16	0
 #  13      }
 #  14  
 #  15      // reads text until newline 
 #  16      fscanf(fptr,"%[^\n]", c);
	movq %rax,%rdi                	# [0] 
	xorl %eax,%eax                	# [0] 
	movq %rsp,%rdx                	# [1] 
	movq $(.rodata+64),%rsi       	# [1] .rodata+64
	.globl	fscanf
	call fscanf                   	# [1] fscanf
.LBB8_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	18	0
 #  17  
 #  18      printf("Data from the file:\n%s", c);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+80),%rdi       	# [1] .rodata+80
	call printf                   	# [1] printf
.LBB9_main:
	.loc	1	19	0
 #  19      fclose(fptr);
	movq %rbx,%rdi                	# [0] 
	.globl	fclose
	call fclose                   	# [0] fclose
.LBB10_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	21	0
 #  20      
 #  21      return 0;
	movq 1000(%rsp),%rbx          	# [1] _temp_gra_spill2
	addq $1016,%rsp               	# [1] 
	ret                           	# [1] 
.L_0_1282:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "program.txt"
	.org 0x10
	.align	0
	# offset 16
	.string "r"
	.org 0x20
	.align	0
	# offset 32
	.string "Error! opening file"
	.org 0x40
	.align	0
	# offset 64
	.byte	0x25, 0x5b, 0x5e, 0xa, 0x5d, 0x0	# %[^\n]\000
	.org 0x50
	.align	0
	# offset 80
	.byte	0x44, 0x61, 0x74, 0x61, 0x20, 0x66, 0x72, 0x6f	# Data fro
	.byte	0x6d, 0x20, 0x74, 0x68, 0x65, 0x20, 0x66, 0x69	# m the fi
	.byte	0x6c, 0x65, 0x3a, 0xa, 0x25, 0x73, 0x0	# le:\n%s\000

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_1026-main
	.uleb128	.L_0_1282-.L_0_1026
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x80, 0x08, 0x04
	.4byte	.LEH_csr_main - .LEH_adjustsp_main
	.byte	0x83, 0x03
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test69.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

