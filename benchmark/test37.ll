; ModuleID = 'test37.bc'

@.str = private constant [27 x i8] c"Enter a positive integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [9 x i8] c"Sum = %d\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_dummy11_6 = alloca i32, align 4
  %_temp_dummy12_7 = alloca i32, align 4
  %_temp_ehpit0_13 = alloca i32, align 4
  %n_11.addr = alloca i32, align 4
  %n_12 = alloca i32, align 4
  %n_4 = alloca i32, align 4
  %old_frame_pointer_18.addr = alloca i64, align 8
  %return_address_19.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = bitcast i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %3, i32 1)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define i32 @_Z10addNumbersi(i32 %n_4) {
entry:
  %n_4.addr = alloca i32, align 4
  %old_frame_pointer_9.addr = alloca i64, align 8
  %return_address_10.addr = alloca i64, align 8
  store i32 %n_4, i32* %n_4.addr, align 4
  ret i32 1
}
