; ModuleID = 'test21.bc'

@.str = private constant [30 x i8] c"Enter two positive integers: \00", align 1
@.str.1 = private constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private constant [28 x i8] c"The LCM of %d and %d is %d.\00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_50 = alloca i32
  %.preg_I4_4_52 = alloca i32
  %.preg_I4_4_51 = alloca i32
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_ehpit0_10 = alloca i32, align 4
  %minMultiple_6 = alloca i32, align 4
  %n1_4 = alloca i32, align 4
  %n2_5 = alloca i32, align 4
  %old_frame_pointer_15.addr = alloca i64, align 8
  %return_address_16.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n1_4 to i32*
  %3 = bitcast i32* %n2_5 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2, i32* %3)
  %4 = load i32, i32* %n2_5, align 4
  store i32 %4, i32* %.preg_I4_4_51, align 8
  %5 = load i32, i32* %n1_4, align 4
  store i32 %5, i32* %.preg_I4_4_52, align 8
  %6 = load i32, i32* %.preg_I4_4_52
  %7 = load i32, i32* %.preg_I4_4_51
  %8 = icmp sgt i32 %6, %7
  br i1 %8, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  br label %L2818

if.else:                                          ; preds = %entry
  %9 = load i32, i32* %.preg_I4_4_52
  br label %if.end

if.end:                                           ; preds = %if.end, %if.else
  %10 = load i32, i32* %.preg_I4_4_51
  br label %if.end

L2818:                                            ; preds = %L4098, %if.then
  %11 = phi i32 [ %9, %if.then ], [ %10, %if.else ]
  store i32 %11, i32* %.preg_I4_4_50, align 8
  %12 = load i32, i32* %.preg_I4_4_50
  %13 = load i32, i32* %.preg_I4_4_52
  %srem = srem i32 %12, %13
  %cmp1 = icmp eq i32 %srem, 0
  br i1 %cmp1, label %tb, label %L4610

tb:                                               ; preds = %L2818
  %14 = load i32, i32* %.preg_I4_4_50
  %15 = load i32, i32* %.preg_I4_4_51
  %srem.1 = srem i32 %14, %15
  %cmp2 = icmp eq i32 %srem.1, 0
  br i1 %cmp2, label %tb2, label %L4098

L4610:                                            ; preds = %L2818
  br label %L4098

tb2:                                              ; preds = %tb
  %16 = bitcast i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.2, i32 0, i32 0) to i8*
  %17 = load i32, i32* %.preg_I4_4_52
  %18 = load i32, i32* %.preg_I4_4_51
  %19 = load i32, i32* %.preg_I4_4_50
  %call3 = call i32 (i8*, ...) @printf(i8* %16, i32 %17, i32 %18, i32 %19)
  br label %L258

L4098:                                            ; preds = %L4610, %tb
  %20 = load i32, i32* %.preg_I4_4_50
  %add = add i32 %20, 1
  store i32 %add, i32* %.preg_I4_4_50, align 8
  br label %L2818

L258:                                             ; preds = %tb2
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
