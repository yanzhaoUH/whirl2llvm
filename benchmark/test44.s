	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test44.c (test44.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test44.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# base = 0
	# exp = 4
	.loc	1	5	0
 #   1  /* Source Code to calculate power using recursive function */
 #   2  
 #   3  #include <stdio.h>
 #   4  int power(int n1,int n2);
 #   5  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_258:
	xorl %eax,%eax                	# [0] 
	.loc	1	8	0
 #   6  {
 #   7      int base, exp;
 #   8      printf("Enter base number: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	9	0
 #   9      scanf("%d",&base);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	12	0
 #  10      //base = 10;
 #  11  
 #  12      printf("Enter power number(positive integer): ");
	movq $(.rodata+48),%rdi       	# [0] .rodata+48
	call printf                   	# [0] printf
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	15	0
 #  13  
 #  14      //exp = 2;
 #  15      scanf("%d",&exp);
	leaq 4(%rsp),%rsi             	# [1] exp
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	call scanf                    	# [1] scanf
.LBB6_main:
	.loc	1	16	0
 #  16      printf("%d^%d = %d", base, exp, power(base, exp));
	movl 4(%rsp),%esi             	# [0] exp
	movl 0(%rsp),%edi             	# [0] base
	call _Z5powerii               	# [0] _Z5powerii
.LBB7_main:
	movl 4(%rsp),%edx             	# [0] exp
	movl 0(%rsp),%esi             	# [1] base
	movq $(.rodata+96),%rdi       	# [1] .rodata+96
	movl %eax,%eax                	# [1] 
	movl %eax,%ecx                	# [2] 
	xorl %eax,%eax                	# [2] 
	call printf                   	# [2] printf
.LBB8_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	17	0
 #  17      return 0;
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.L_0_514:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter base number: "
	.org 0x20
	.align	0
	# offset 32
	.string "%d"
	.org 0x30
	.align	0
	# offset 48
	.string "Enter power number(positive integer): "
	.org 0x60
	.align	0
	# offset 96
	.string "%d^%d = %d"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_258-main
	.uleb128	.L_0_514-.L_0_258
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.p2align 5,,

	# Program Unit: _Z5powerii
.globl	_Z5powerii
	.type	_Z5powerii, @function
_Z5powerii:	# 0x70
	# .frame	%rsp, 40, %rsp
	# _temp_gra_spill2 = 0
	.loc	1	19	0
 #  18  }
 #  19  int power(int base,int exp)
.LBB1__Z5powerii:
.LEH_adjustsp__Z5powerii:
.LEH_csr__Z5powerii:
	addq $-40,%rsp                	# [0] 
	movq %rbx,0(%rsp)             	# [1] _temp_gra_spill2
	movl %edi,%ebx                	# [1] 
.L_1_1026:
	.loc	1	21	0
 #  20  {
 #  21      if ( exp!=1 )
	cmpl $1,%esi                  	# [0] 
	je .Lt_1_770                  	# [1] 
.LBB3__Z5powerii:
	.loc	1	22	0
 #  22          return (base*power(base,exp-1));
	addl $-1,%esi                 	# [0] 
	movl %edi,%edi                	# [0] 
	call _Z5powerii               	# [0] _Z5powerii
.LBB4__Z5powerii:
	movl %eax,%eax                	# [0] 
	movl %eax,%eax                	# [1] 
	imull %ebx,%eax               	# [2] 
	movq 0(%rsp),%rbx             	# [3] _temp_gra_spill2
	addq $40,%rsp                 	# [3] 
	ret                           	# [3] 
.Lt_1_770:
	.loc	1	25	0
 #  23      else{
 #  24     //     int exp = 333;
 #  25          return base;
	movl %edi,%eax                	# [0] 
	movq 0(%rsp),%rbx             	# [1] _temp_gra_spill2
	addq $40,%rsp                 	# [1] 
	ret                           	# [1] 
.L_1_1282:
.LDWend__Z5powerii:
	.size _Z5powerii, .LDWend__Z5powerii-_Z5powerii

	.section .except_table
	.align	0
	.type	.range_table._Z5powerii, @object
.range_table._Z5powerii:	# 0x8
	# offset 8
	.byte	255
	# offset 9
	.byte	0
	.uleb128	.LSDATTYPEB2-.LSDATTYPED2
.LSDATTYPED2:
	# offset 14
	.byte	1
	.uleb128	.LSDACSE2-.LSDACSB2
.LSDACSB2:
	.uleb128	.L_1_1026-_Z5powerii
	.uleb128	.L_1_1282-.L_1_1026
	# offset 25
	.uleb128	0
	# offset 29
	.uleb128	0
.LSDACSE2:
	# offset 33
	.sleb128	0
	# offset 37
	.sleb128	0
.LSDATTYPEB2:
	# end of initialization for .range_table._Z5powerii
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20
	.align 8
.LFDE1_end:
	.4byte	.LFDE2_end - .LFDE2_begin
.LFDE2_begin:
	.4byte	.LFDE2_begin - .LEHCIE
	.quad	.LBB1__Z5powerii
	.quad	.LDWend__Z5powerii - .LBB1__Z5powerii
	.byte	0x08
	.quad	.range_table._Z5powerii
	.byte	0x04
	.4byte	.LEH_adjustsp__Z5powerii - .LBB1__Z5powerii
	.byte	0x0e, 0x30, 0x04
	.4byte	.LEH_csr__Z5powerii - .LEH_adjustsp__Z5powerii
	.byte	0x83, 0x06
	.align 8
.LFDE2_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test44.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

