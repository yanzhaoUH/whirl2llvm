	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test5.c (test5.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test5.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# dividend = 0
	# divisor = 4
	# _temp_gra_spill1 = 8
	.loc	1	2	0
 #   1  #include <stdio.h>
 #   2  int main(){
.LBB1_main:
.LEH_adjustsp_main:
.LEH_csr_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
	movq %rbx,8(%rsp)             	# [0] _temp_gra_spill1
.L_0_258:
	xorl %eax,%eax                	# [0] 
	.loc	1	6	0
 #   3  
 #   4      int dividend, divisor, quotient, remainder;
 #   5  
 #   6      printf("Enter dividend: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	7	0
 #   7      scanf("%d", &dividend);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	9	0
 #   8  //dividend = 5;
 #   9      printf("Enter divisor: ");
	movq $(.rodata+48),%rdi       	# [0] .rodata+48
	call printf                   	# [0] printf
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	10	0
 #  10      scanf("%d", &divisor);
	leaq 4(%rsp),%rsi             	# [1] divisor
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	call scanf                    	# [1] scanf
.LBB6_main:
	.loc	1	16	0
 #  12      // Computes quotient
 #  13      quotient = dividend/divisor;
 #  14  
 #  15      // Computes remainder
 #  16      remainder = dividend%divisor;
	movl 0(%rsp),%eax             	# [0] dividend
	movl 4(%rsp),%ecx             	# [1] divisor
	cltd                          	# [3] 
	idivl %ecx                    	# [4] 
	.loc	1	18	0
 #  17  
 #  18      printf("Quotient = %d\n",quotient);
	movq $(.rodata+64),%rdi       	# [26] .rodata+64
	movl %eax,%esi                	# [26] 
	xorl %eax,%eax                	# [27] 
	.loc	1	16	0
	movl %edx,%ebx                	# [27] 
	.loc	1	18	0
	call printf                   	# [27] printf
.LBB7_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	19	0
 #  19      printf("Remainder = %d",remainder);
	movl %ebx,%esi                	# [1] 
	movq $(.rodata+80),%rdi       	# [1] .rodata+80
	call printf                   	# [1] printf
.LBB8_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	21	0
 #  20  
 #  21      return 0;
	movq 8(%rsp),%rbx             	# [1] _temp_gra_spill1
	addq $24,%rsp                 	# [1] 
	ret                           	# [1] 
.L_0_514:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter dividend: "
	.org 0x20
	.align	0
	# offset 32
	.string "%d"
	.org 0x30
	.align	0
	# offset 48
	.string "Enter divisor: "
	.org 0x40
	.align	0
	# offset 64
	.byte	0x51, 0x75, 0x6f, 0x74, 0x69, 0x65, 0x6e, 0x74	# Quotient
	.byte	0x20, 0x3d, 0x20, 0x25, 0x64, 0xa, 0x0	#  = %d\n\000
	.org 0x50
	.align	0
	# offset 80
	.string "Remainder = %d"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_258-main
	.uleb128	.L_0_514-.L_0_258
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20, 0x04
	.4byte	.LEH_csr_main - .LEH_adjustsp_main
	.byte	0x83, 0x03
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test5.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

