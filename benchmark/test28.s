	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test28.c (test28.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test28.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# n1 = 0
	# n2 = 4
	# i = 8
	.loc	1	2	0
 #   1  #include <stdio.h>
 #   2  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_6658:
	xorl %eax,%eax                	# [0] 
	.loc	1	5	0
 #   3  {
 #   4      int n1, n2, i, flag;
 #   5      printf("Enter two numbers(intevals): ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	6	0
 #   6      scanf("%d %d", &n1, &n2);
	leaq 4(%rsp),%rdx             	# [0] n2
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	10	0
 #   7  
 #   8      //n1 = 3;
 #   9      //n2 = 100;
 #  10      printf("Prime numbers between %d and %d are: ", n1, n2);
	movl 4(%rsp),%edx             	# [0] n2
	movl 0(%rsp),%esi             	# [1] n1
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call printf                   	# [1] printf
.LBB5_main:
	movl 0(%rsp),%esi             	# [0] n1
	movl 4(%rsp),%r9d             	# [0] n2
	cmpl %esi,%r9d                	# [3] 
	jle .Lt_0_2562                	# [4] 
.LBB6_main:
.Lt_0_3074:
 #<loop> Loop body line 10, nesting depth: 1, estimated iterations: 100
	movl %esi,%r8d                	# [0] 
	sarl $31,%r8d                 	# [1] 
	andl $1,%r8d                  	# [2] 
	addl %esi,%r8d                	# [3] 
	sarl $1,%r8d                  	# [4] 
	cmpl $2,%r8d                  	# [5] 
	jl .Lt_0_3330                 	# [6] 
.LBB8_main:
 #<loop> Part of loop body line 10, head labeled .Lt_0_3074
	movl $2,%ecx                  	# [0] 
.Lt_0_3842:
 #<loop> Loop body line 10, nesting depth: 2, estimated iterations: 100
	.loc	1	18	0
 #  14          flag=0;
 #  15  
 #  16          for(i=2; i<=n1/2; ++i)
 #  17          {
 #  18              if(n1%i == 0)
	movl %esi,%eax                	# [0] 
	cltd                          	# [1] 
	idivl %ecx                    	# [2] 
	testl %edx,%edx               	# [24] 
	je .Lt_0_5122                 	# [25] 
.Lt_0_4098:
 #<loop> Part of loop body line 10, head labeled .Lt_0_3842
	.loc	1	21	0
 #  19              {
 #  20                  flag=1;
 #  21                  break;
	addl $1,%ecx                  	# [0] 
	cmpl %ecx,%r8d                	# [1] 
	jge .Lt_0_3842                	# [2] 
.Lt_0_3330:
 #<loop> Part of loop body line 10, head labeled .Lt_0_3074
	xorl %eax,%eax                	# [0] 
	.loc	1	26	0
 #  22              }
 #  23          }
 #  24  
 #  25          if (flag == 0)
 #  26              printf("%d ",n1);
	movl %esi,%esi                	# [1] 
	movq $(.rodata+96),%rdi       	# [1] .rodata+96
	call printf                   	# [1] printf
.LBB14_main:
 #<loop> Part of loop body line 10, head labeled .Lt_0_3074
	movl 0(%rsp),%esi             	# [0] n1
	movl 4(%rsp),%r9d             	# [0] n2
.Lt_0_5122:
 #<loop> Part of loop body line 10, head labeled .Lt_0_3074
	.loc	1	28	0
 #  27  
 #  28          ++n1;
	addl $1,%esi                  	# [0] 
	cmpl %esi,%r9d                	# [1] 
	movl %esi,0(%rsp)             	# [2] n1
	jg .Lt_0_3074                 	# [2] 
.Lt_0_2562:
	xorq %rax,%rax                	# [0] 
	.loc	1	31	0
 #  29      }
 #  30  
 #  31      return 0;
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.L_0_6914:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter two numbers(intevals): "
	.org 0x20
	.align	0
	# offset 32
	.string "%d %d"
	.org 0x30
	.align	0
	# offset 48
	.string "Prime numbers between %d and %d are: "
	.org 0x60
	.align	0
	# offset 96
	.string "%d "

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_6658-main
	.uleb128	.L_0_6914-.L_0_6658
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test28.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

