	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test53.c (test53.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test53.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# a = 0
	# b = 4
	# c = 8
	.loc	1	4	0
 #   1  #include<stdio.h>
 #   2  void cyclicSwap(int *a,int *b,int *c);
 #   3  
 #   4  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_258:
	xorl %eax,%eax                	# [0] 
	.loc	1	8	0
 #   5  {
 #   6      int a, b, c;
 #   7  
 #   8      printf("Enter a, b and c respectively: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	9	0
 #   9      scanf("%d %d %d",&a,&b,&c);
	leaq 8(%rsp),%rcx             	# [0] c
	leaq 4(%rsp),%rdx             	# [0] b
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	12	0
 #  10      //a = 1; b= 2;c = 3;
 #  11  
 #  12      printf("Value before swapping:\n");
	movq $(.rodata+48),%rdi       	# [0] .rodata+48
	call printf                   	# [0] printf
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	13	0
 #  13      printf("a = %d \nb = %d \nc = %d\n",a,b,c);
	movl 8(%rsp),%ecx             	# [0] c
	movl 4(%rsp),%edx             	# [0] b
	movl 0(%rsp),%esi             	# [1] a
	movq $(.rodata+80),%rdi       	# [1] .rodata+80
	call printf                   	# [1] printf
.LBB6_main:
	.loc	1	15	0
 #  14  
 #  15      cyclicSwap(&a, &b, &c);
	leaq 8(%rsp),%rdx             	# [0] c
	leaq 4(%rsp),%rsi             	# [1] b
	movq %rsp,%rdi                	# [1] 
	call _Z10cyclicSwapPiS_S_     	# [1] _Z10cyclicSwapPiS_S_
.LBB7_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	17	0
 #  16  
 #  17      printf("Value after swapping:\n");
	movq $(.rodata+112),%rdi      	# [0] .rodata+112
	call printf                   	# [0] printf
.LBB8_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	18	0
 #  18      printf("a = %d \nb = %d \nc = %d",a, b, c);
	movl 8(%rsp),%ecx             	# [0] c
	movl 4(%rsp),%edx             	# [0] b
	movl 0(%rsp),%esi             	# [1] a
	movq $(.rodata+144),%rdi      	# [1] .rodata+144
	call printf                   	# [1] printf
.LBB9_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	20	0
 #  19  
 #  20      return 0;
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.L_0_514:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter a, b and c respectively: "
	.org 0x20
	.align	0
	# offset 32
	.string "%d %d %d"
	.org 0x30
	.align	0
	# offset 48
	.byte	0x56, 0x61, 0x6c, 0x75, 0x65, 0x20, 0x62, 0x65	# Value be
	.byte	0x66, 0x6f, 0x72, 0x65, 0x20, 0x73, 0x77, 0x61	# fore swa
	.byte	0x70, 0x70, 0x69, 0x6e, 0x67, 0x3a, 0xa, 0x0	# pping:\n\000
	# 
	.org 0x50
	.align	0
	# offset 80
	.byte	0x61, 0x20, 0x3d, 0x20, 0x25, 0x64, 0x20, 0xa	# a = %d \n
	.byte	0x62, 0x20, 0x3d, 0x20, 0x25, 0x64, 0x20, 0xa	# b = %d \n
	.byte	0x63, 0x20, 0x3d, 0x20, 0x25, 0x64, 0xa, 0x0	# c = %d\n\000
	# 
	.org 0x70
	.align	0
	# offset 112
	.byte	0x56, 0x61, 0x6c, 0x75, 0x65, 0x20, 0x61, 0x66	# Value af
	.byte	0x74, 0x65, 0x72, 0x20, 0x73, 0x77, 0x61, 0x70	# ter swap
	.byte	0x70, 0x69, 0x6e, 0x67, 0x3a, 0xa, 0x0	# ping:\n\000
	.org 0x90
	.align	0
	# offset 144
	.byte	0x61, 0x20, 0x3d, 0x20, 0x25, 0x64, 0x20, 0xa	# a = %d \n
	.byte	0x62, 0x20, 0x3d, 0x20, 0x25, 0x64, 0x20, 0xa	# b = %d \n
	.byte	0x63, 0x20, 0x3d, 0x20, 0x25, 0x64, 0x0	# c = %d\000

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_258-main
	.uleb128	.L_0_514-.L_0_258
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.p2align 5,,

	# Program Unit: _Z10cyclicSwapPiS_S_
.globl	_Z10cyclicSwapPiS_S_
	.type	_Z10cyclicSwapPiS_S_, @function
_Z10cyclicSwapPiS_S_:	# 0x8c
	# .frame	%rsp, 40, %rsp
	.loc	1	22	0
 #  21  }
 #  22  void cyclicSwap(int *a,int *b,int *c)
.LBB1__Z10cyclicSwapPiS_S_:
	.loc	1	29	0
 #  25      int temp;
 #  26  
 #  27      // swapping in cyclic order
 #  28      temp = *b;
 #  29      *b = *a;
	movl 0(%rdi),%ecx             	# [0] id:21
	.loc	1	28	0
	movl 0(%rsi),%eax             	# [2] id:20
	.loc	1	29	0
	movl %ecx,0(%rsi)             	# [3] id:22
	.loc	1	30	0
 #  30      *a = *c;
	movl 0(%rdx),%esi             	# [3] id:23
	movl %esi,0(%rdi)             	# [6] id:24
	.loc	1	31	0
 #  31      *c = temp;
	movl %eax,0(%rdx)             	# [6] id:25
	ret                           	# [6] 
.LDWend__Z10cyclicSwapPiS_S_:
	.size _Z10cyclicSwapPiS_S_, .LDWend__Z10cyclicSwapPiS_S_-_Z10cyclicSwapPiS_S_
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test53.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

