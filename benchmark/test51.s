	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test51.c (test51.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test51.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 1240, %rsp
	# firstMatrix = 32
	# secondMatrix = 432
	# mult = 832
	# rowFirst = 16
	# columnFirst = 20
	# rowSecond = 24
	# columnSecond = 28
	.loc	1	7	0
 #   3  void enterData(int firstMatrix[][10], int secondMatrix[][10], int rowFirst, int columnFirst, int rowSecond, int columnSecond);
 #   4  void multiplyMatrices(int firstMatrix[][10], int secondMatrix[][10], int multResult[][10], int rowFirst, int columnFirst, int rowSecond, int columnSecond);
 #   5  void display(int mult[][10], int rowFirst, int columnSecond);
 #   6  
 #   7  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-1240,%rsp              	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_2050:
	xorl %eax,%eax                	# [0] 
	.loc	1	11	0
 #   8  {
 #   9  	int firstMatrix[10][10], secondMatrix[10][10], mult[10][10], rowFirst, columnFirst, rowSecond, columnSecond, i, j, k;
 #  10  
 #  11  	printf("Enter rows and column for first matrix: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	12	0
 #  12  	scanf("%d %d", &rowFirst, &columnFirst);
	leaq 20(%rsp),%rdx            	# [0] columnFirst
	leaq 16(%rsp),%rsi            	# [1] rowFirst
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	16	0
 #  13          //rowFirst = 2;
 #  14          //columnFirst = 3;
 #  15  
 #  16  	printf("Enter rows and column for second matrix: ");
	movq $(.rodata+64),%rdi       	# [0] .rodata+64
	call printf                   	# [0] printf
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	17	0
 #  17  	scanf("%d %d", &rowSecond, &columnSecond);
	leaq 28(%rsp),%rdx            	# [0] columnSecond
	leaq 24(%rsp),%rsi            	# [1] rowSecond
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call scanf                    	# [1] scanf
.LBB6_main:
	movl 20(%rsp),%eax            	# [0] columnFirst
	movl 24(%rsp),%r8d            	# [0] rowSecond
	cmpl %eax,%r8d                	# [3] 
	je .Lt_0_770                  	# [4] 
.Lt_0_1282:
 #<loop> Loop body line 24
	xorl %eax,%eax                	# [0] 
	.loc	1	24	0
 #  20  
 #  21  	// If colum of first matrix in not equal to row of second matrix, asking user to enter the size of matrix again.
 #  22  	while (columnFirst != rowSecond)
 #  23  	{
 #  24  		printf("Error! column of first matrix not equal to row of second.\n");
	movq $(.rodata+112),%rdi      	# [0] .rodata+112
	call printf                   	# [0] printf
.LBB8_main:
 #<loop> Part of loop body line 24, head labeled .Lt_0_1282
	xorl %eax,%eax                	# [0] 
	.loc	1	25	0
 #  25  		printf("Enter rows and column for first matrix: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	call printf                   	# [0] printf
.LBB9_main:
 #<loop> Part of loop body line 24, head labeled .Lt_0_1282
	xorl %eax,%eax                	# [0] 
	.loc	1	26	0
 #  26  		scanf("%d%d", &rowFirst, &columnFirst);
	leaq 20(%rsp),%rdx            	# [0] columnFirst
	leaq 16(%rsp),%rsi            	# [1] rowFirst
	movq $(.rodata+176),%rdi      	# [1] .rodata+176
	call scanf                    	# [1] scanf
.LBB10_main:
 #<loop> Part of loop body line 24, head labeled .Lt_0_1282
	xorl %eax,%eax                	# [0] 
	.loc	1	27	0
 #  27  		printf("Enter rows and column for second matrix: ");
	movq $(.rodata+64),%rdi       	# [0] .rodata+64
	call printf                   	# [0] printf
.LBB11_main:
 #<loop> Part of loop body line 24, head labeled .Lt_0_1282
	xorl %eax,%eax                	# [0] 
	.loc	1	28	0
 #  28  		scanf("%d%d", &rowSecond, &columnSecond);
	leaq 28(%rsp),%rdx            	# [0] columnSecond
	leaq 24(%rsp),%rsi            	# [1] rowSecond
	movq $(.rodata+176),%rdi      	# [1] .rodata+176
	call scanf                    	# [1] scanf
.LBB12_main:
 #<loop> Part of loop body line 24, head labeled .Lt_0_1282
	movl 20(%rsp),%eax            	# [0] columnFirst
	movl 24(%rsp),%r8d            	# [0] rowSecond
	cmpl %eax,%r8d                	# [3] 
	jne .Lt_0_1282                	# [4] 
.Lt_0_770:
	.loc	1	32	0
 #  29  	}
 #  30  
 #  31  	// Function to take matrices data
 #  32          enterData(firstMatrix, secondMatrix, rowFirst, columnFirst, rowSecond, columnSecond);
	movl 28(%rsp),%r9d            	# [0] columnSecond
	movl %r8d,%r8d                	# [1] 
	movl %eax,%ecx                	# [1] 
	movl 16(%rsp),%edx            	# [1] rowFirst
	leaq 432(%rsp),%rsi           	# [2] secondMatrix
	leaq 32(%rsp),%rdi            	# [2] firstMatrix
	call _Z9enterDataPA10_iS0_iiii	# [2] _Z9enterDataPA10_iS0_iiii
.LBB14_main:
	.loc	1	35	0
 #  33  
 #  34          // Function to multiply two matrices.
 #  35          multiplyMatrices(firstMatrix, secondMatrix, mult, rowFirst, columnFirst, rowSecond, columnSecond);
	movl 28(%rsp),%eax            	# [0] columnSecond
	movl 24(%rsp),%r9d            	# [1] rowSecond
	movl 20(%rsp),%r8d            	# [1] columnFirst
	movl 16(%rsp),%ecx            	# [2] rowFirst
	leaq 832(%rsp),%rdx           	# [2] mult
	leaq 432(%rsp),%rsi           	# [2] secondMatrix
	leaq 32(%rsp),%rdi            	# [3] firstMatrix
	movl %eax,0(%rsp)             	# [3] id:19
	call _Z16multiplyMatricesPA10_iS0_S0_iiii	# [3] _Z16multiplyMatricesPA10_iS0_S0_iiii
.LBB15_main:
	.loc	1	38	0
 #  36  
 #  37          // Function to display resultant matrix after multiplication.
 #  38          display(mult, rowFirst, columnSecond);
	movl 28(%rsp),%edx            	# [0] columnSecond
	movl 16(%rsp),%esi            	# [1] rowFirst
	leaq 832(%rsp),%rdi           	# [1] mult
	call _Z7displayPA10_iii       	# [1] _Z7displayPA10_iii
.LBB16_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	41	0
 #  39          //display(firstMatrix, rowFirst, columnFirst);
 #  40           
 #  41  	return 0;
	addq $1240,%rsp               	# [0] 
	ret                           	# [0] 
.L_0_2306:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter rows and column for first matrix: "
	.org 0x30
	.align	0
	# offset 48
	.string "%d %d"
	.org 0x40
	.align	0
	# offset 64
	.string "Enter rows and column for second matrix: "
	.org 0x70
	.align	0
	# offset 112
	.byte	0x45, 0x72, 0x72, 0x6f, 0x72, 0x21, 0x20, 0x63	# Error! c
	.byte	0x6f, 0x6c, 0x75, 0x6d, 0x6e, 0x20, 0x6f, 0x66	# olumn of
	.byte	0x20, 0x66, 0x69, 0x72, 0x73, 0x74, 0x20, 0x6d	#  first m
	.byte	0x61, 0x74, 0x72, 0x69, 0x78, 0x20, 0x6e, 0x6f	# atrix no
	.byte	0x74, 0x20, 0x65, 0x71, 0x75, 0x61, 0x6c, 0x20	# t equal 
	.byte	0x74, 0x6f, 0x20, 0x72, 0x6f, 0x77, 0x20, 0x6f	# to row o
	.byte	0x66, 0x20, 0x73, 0x65, 0x63, 0x6f, 0x6e, 0x64	# f second
	.byte	0x2e, 0xa, 0x0	# .\n\000
	.org 0xb0
	.align	0
	# offset 176
	.string "%d%d"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_2050-main
	.uleb128	.L_0_2306-.L_0_2050
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.p2align 5,,

	# Program Unit: _Z7displayPA10_iii
.globl	_Z7displayPA10_iii
	.type	_Z7displayPA10_iii, @function
_Z7displayPA10_iii:	# 0x10c
	# .frame	%rsp, 120, %rsp
	# i = 0
	# j = 4
	# _temp_gra_spill2 = 8
	# _temp_gra_spill3 = 16
	# _temp_gra_spill4 = 24
	# _temp_gra_spill5 = 32
	# _temp_gra_spill6 = 40
	# _temp_gra_spill7 = 48
	# _temp_gra_spill8 = 56
	# _temp_gra_spill9 = 64
	.loc	1	99	0
 #  95  	}
 #  96  }
 #  97  
 #  98  
 #  99  void display(int mult[][10], int rowFirst, int columnSecond)
.LBB1__Z7displayPA10_iii:
.LEH_adjustsp__Z7displayPA10_iii:
.LEH_csr__Z7displayPA10_iii:
	addq $-120,%rsp               	# [0] 
	movq %rbx,56(%rsp)            	# [1] _temp_gra_spill8
	movq %rbp,32(%rsp)            	# [1] _temp_gra_spill5
	movq %r12,24(%rsp)            	# [1] _temp_gra_spill4
	movq %r15,40(%rsp)            	# [2] _temp_gra_spill6
	movq %r14,16(%rsp)            	# [2] _temp_gra_spill3
	movq %rdi,%r14                	# [2] 
	movq %rsi,64(%rsp)            	# [3] _temp_gra_spill9
	movq %r13,48(%rsp)            	# [3] _temp_gra_spill7
	movl %edx,%r13d               	# [3] 
.L_1_4866:
	xorl %eax,%eax                	# [0] 
	.loc	1	102	0
 # 100  {
 # 101  	int i, j;
 # 102  	printf("\nOutput Matrix:\n");
	movq $(.rodata+192),%rdi      	# [0] .rodata+192
	call printf                   	# [0] printf
.LBB3__Z7displayPA10_iii:
	movq 64(%rsp),%rax            	# [0] _temp_gra_spill9
	cmpl $0,%eax                  	# [3] 
	jle .Lt_1_1794                	# [4] 
.LBB4__Z7displayPA10_iii:
	leal -1(%r13),%r12d           	# [0] 
	movq %rax,8(%rsp)             	# [2] _temp_gra_spill2
	movslq %r12d,%r12             	# [2] 
	xorl %r15d,%r15d              	# [3] 
	leaq 0(%r14,%r12,4), %r12     	# [3] 
	jmp .Lt_1_2306                	# [3] 
.Lt_1_2562:
 #<loop> Part of loop body line 102, head labeled .Lt_1_2306
	.loc	1	109	0
 # 105  		for(j = 0; j < columnSecond; ++j)
 # 106  		{
 # 107  			printf("%d  ", mult[i][j]);
 # 108  			if(j == columnSecond - 1)
 # 109  				printf("\n\n");
	movq 64(%rsp),%rax            	# [0] _temp_gra_spill9
	addl $1,%r15d                 	# [2] 
	addq $40,%r12                 	# [3] 
	addq $40,%r14                 	# [3] 
	cmpl %r15d,%eax               	# [3] 
	jle .Lt_1_1794                	# [4] 
.Lt_1_2306:
 #<loop> Loop body line 102, nesting depth: 1, estimated iterations: 100
	.loc	1	102	0
	testl %r13d,%r13d             	# [0] 
	jle .Lt_1_2562                	# [1] 
.LBB7__Z7displayPA10_iii:
 #<loop> Part of loop body line 102, head labeled .Lt_1_2306
	xorl %ebp,%ebp                	# [0] 
	movq %r14,%rbx                	# [0] 
	jmp .Lt_1_3074                	# [0] 
.Lt_1_4098:
 #<loop> Part of loop body line 102, head labeled .Lt_1_3074
	.loc	1	109	0
	addl $1,%ebp                  	# [0] 
	addq $4,%rbx                  	# [1] 
	cmpl %ebp,%r13d               	# [1] 
	jle .Lt_1_2562                	# [2] 
.Lt_1_3074:
 #<loop> Loop body line 102, nesting depth: 2, estimated iterations: 100
	xorl %eax,%eax                	# [0] 
	.loc	1	107	0
	movl 0(%rbx),%esi             	# [1] id:24
	movq $(.rodata+224),%rdi      	# [1] .rodata+224
	call printf                   	# [1] printf
.LBB10__Z7displayPA10_iii:
 #<loop> Part of loop body line 102, head labeled .Lt_1_3074
	.loc	1	108	0
	cmpq %r12,%rbx                	# [0] 
	jne .Lt_1_4098                	# [1] 
.LBB11__Z7displayPA10_iii:
 #<loop> Part of loop body line 102, head labeled .Lt_1_3074
	xorl %eax,%eax                	# [0] 
	.loc	1	109	0
	movq $(.rodata+240),%rdi      	# [0] .rodata+240
	call printf                   	# [0] printf
.LBB19__Z7displayPA10_iii:
 #<loop> Part of loop body line 102, head labeled .Lt_1_3074
	jmp .Lt_1_4098                	# [0] 
.Lt_1_1794:
	movq 56(%rsp),%rbx            	# [0] _temp_gra_spill8
	movq 32(%rsp),%rbp            	# [0] _temp_gra_spill5
	movq 24(%rsp),%r12            	# [1] _temp_gra_spill4
	movq 48(%rsp),%r13            	# [1] _temp_gra_spill7
	movq 16(%rsp),%r14            	# [1] _temp_gra_spill3
	movq 40(%rsp),%r15            	# [2] _temp_gra_spill6
	addq $120,%rsp                	# [2] 
	ret                           	# [2] 
.L_1_5122:
.LDWend__Z7displayPA10_iii:
	.size _Z7displayPA10_iii, .LDWend__Z7displayPA10_iii-_Z7displayPA10_iii

	.section .rodata
	.org 0xc0
	.align	0
	# offset 192
	.byte	0xa, 0x4f, 0x75, 0x74, 0x70, 0x75, 0x74, 0x20	# \nOutput 
	.byte	0x4d, 0x61, 0x74, 0x72, 0x69, 0x78, 0x3a, 0xa	# Matrix:\n
	.byte	0x0	# \000
	.org 0xe0
	.align	0
	# offset 224
	.string "%d  "
	.org 0xf0
	.align	0
	# offset 240
	.byte	0xa, 0xa, 0x0	# \n\n\000

	.section .except_table
	.align	0
	.type	.range_table._Z7displayPA10_iii, @object
.range_table._Z7displayPA10_iii:	# 0x8
	# offset 8
	.byte	255
	# offset 9
	.byte	0
	.uleb128	.LSDATTYPEB2-.LSDATTYPED2
.LSDATTYPED2:
	# offset 14
	.byte	1
	.uleb128	.LSDACSE2-.LSDACSB2
.LSDACSB2:
	.uleb128	.L_1_4866-_Z7displayPA10_iii
	.uleb128	.L_1_5122-.L_1_4866
	# offset 25
	.uleb128	0
	# offset 29
	.uleb128	0
.LSDACSE2:
	# offset 33
	.sleb128	0
	# offset 37
	.sleb128	0
.LSDATTYPEB2:
	# end of initialization for .range_table._Z7displayPA10_iii
	.section .text
	.p2align 5,,

	# Program Unit: _Z16multiplyMatricesPA10_iS0_S0_iiii
.globl	_Z16multiplyMatricesPA10_iS0_S0_iiii
	.type	_Z16multiplyMatricesPA10_iS0_S0_iiii, @function
_Z16multiplyMatricesPA10_iS0_S0_iiii:	# 0x1e8
	# .frame	%rsp, 152, %rsp
	# i = 4
	# j = 0
	# k = 8
	# _temp_gra_spill10 = 16
	# _temp_gra_spill11 = 24
	# _temp_gra_spill12 = 32
	# _temp_gra_spill13 = 40
	# _temp_gra_spill14 = 48
	# _temp_gra_spill15 = 56
	# _temp_gra_spill16 = 64
	# _temp_gra_spill17 = 72
	# _temp_gra_spill18 = 80
	# _temp_gra_spill19 = 88
	.loc	1	72	0
.LBB1__Z16multiplyMatricesPA10_iS0_S0_iiii:
	addq $-152,%rsp               	# [0] 
	testl %ecx,%ecx               	# [1] 
	movq %rcx,80(%rsp)            	# [1] _temp_gra_spill18
	movq %rbx,56(%rsp)            	# [2] _temp_gra_spill15
	movq %rdx,%rbx                	# [2] 
	movq %rsi,72(%rsp)            	# [2] _temp_gra_spill17
	movq %r15,16(%rsp)            	# [3] _temp_gra_spill10
	movq %r14,24(%rsp)            	# [3] _temp_gra_spill11
	movq %r13,48(%rsp)            	# [3] _temp_gra_spill14
	movq %r12,32(%rsp)            	# [4] _temp_gra_spill12
	movq %rbp,40(%rsp)            	# [4] _temp_gra_spill13
	jle .Lt_2_2818                	# [4] 
.LBB2__Z16multiplyMatricesPA10_iS0_S0_iiii:
	movl 160(%rsp),%r15d          	# [0] columnSecond
	xorl %esi,%esi                	# [1] 
	movq %rdx,%rcx                	# [1] 
	jmp .Lt_2_3330                	# [1] 
.LBB25__Z16multiplyMatricesPA10_iS0_S0_iiii:
 #<loop> Part of loop body line 72, head labeled .Lt_2_3330
	movl %r10d,%eax               	# [0] 
	sarl $2,%eax                  	# [1] 
	testl %eax,%eax               	# [2] 
	je .Lt_2_3586                 	# [3] 
.LBB30__Z16multiplyMatricesPA10_iS0_S0_iiii:
 #<loop> Part of loop body line 72, head labeled .Lt_2_3330
.LBB24__Z16multiplyMatricesPA10_iS0_S0_iiii:
 #<loop> Loop body line 72, nesting depth: 2, estimated iterations: 25
 #<loop> unrolled 4 times
	.loc	1	81	0
	addq $16,%rbp                 	# [0] 
	xorl %r9d,%r9d                	# [0] 
	addl $4,%r14d                 	# [0] 
	movl %r9d,-12(%rbp)           	# [1] id:58
	movl %r9d,-16(%rbp)           	# [1] id:58
	cmpl %r14d,%r15d              	# [1] 
	movl %r9d,-4(%rbp)            	# [2] id:58
	movl %r9d,-8(%rbp)            	# [2] id:58
	jg .LBB24__Z16multiplyMatricesPA10_iS0_S0_iiii	# [2] 
.Lt_2_3586:
 #<loop> Part of loop body line 72, head labeled .Lt_2_3330
	movq 80(%rsp),%rax            	# [0] _temp_gra_spill18
	addl $1,%esi                  	# [2] 
	addq $40,%rcx                 	# [3] 
	cmpl %esi,%eax                	# [3] 
	jle .Lt_2_2818                	# [4] 
.Lt_2_3330:
 #<loop> Loop body line 72, nesting depth: 1, estimated iterations: 100
	.loc	1	72	0
	testl %r15d,%r15d             	# [0] 
	jle .Lt_2_3586                	# [1] 
.LBB5__Z16multiplyMatricesPA10_iS0_S0_iiii:
 #<loop> Part of loop body line 72, head labeled .Lt_2_3330
	movl %r15d,%eax               	# [0] 
	xorl %r14d,%r14d              	# [1] 
	andl $3,%eax                  	# [1] 
	testl $3,%r15d                	# [1] 
	movq %rcx,%rbp                	# [2] 
	movl %r15d,%r10d              	# [2] 
	je .LBB25__Z16multiplyMatricesPA10_iS0_S0_iiii	# [2] 
.LBB26__Z16multiplyMatricesPA10_iS0_S0_iiii:
 #<loop> Part of loop body line 72, head labeled .Lt_2_3330
	.loc	1	81	0
	leaq 4(%rcx),%rbp             	# [0] 
	decl %eax                     	# [0] 
	movl %r14d,%r11d              	# [1] 
	incl %r14d                    	# [1] 
	testl %eax,%eax               	# [1] 
	movl %r11d,0(%rcx)            	# [2] id:58
	je .LBB25__Z16multiplyMatricesPA10_iS0_S0_iiii	# [2] 
.LBB27__Z16multiplyMatricesPA10_iS0_S0_iiii:
 #<loop> Part of loop body line 72, head labeled .Lt_2_3330
	movl %eax,%r9d                	# [0] 
	decl %r9d                     	# [1] 
	addq $4,%rbp                  	# [2] 
	incl %r14d                    	# [2] 
	testl %r9d,%r9d               	# [2] 
	movl %r11d,4(%rcx)            	# [3] id:58
	je .LBB25__Z16multiplyMatricesPA10_iS0_S0_iiii	# [3] 
.LBB28__Z16multiplyMatricesPA10_iS0_S0_iiii:
 #<loop> Part of loop body line 72, head labeled .Lt_2_3330
	addq $4,%rbp                  	# [0] 
	xorl %eax,%eax                	# [0] 
	incl %r14d                    	# [1] 
	movl %eax,-4(%rbp)            	# [1] id:58
	jmp .LBB25__Z16multiplyMatricesPA10_iS0_S0_iiii	# [1] 
.Lt_2_2818:
	movq 80(%rsp),%rax            	# [0] _temp_gra_spill18
	cmpl $0,%eax                  	# [3] 
	jle .Lt_2_4866                	# [4] 
.LBB10__Z16multiplyMatricesPA10_iS0_S0_iiii:
	movq %rax,64(%rsp)            	# [0] _temp_gra_spill16
	movl 160(%rsp),%r15d          	# [0] columnSecond
	xorl %esi,%esi                	# [1] 
	movq %rbx,%rcx                	# [1] 
	jmp .Lt_2_5378                	# [1] 
.Lt_2_5634:
 #<loop> Part of loop body line 81, head labeled .Lt_2_5378
	.loc	1	92	0
	movq 80(%rsp),%rax            	# [0] _temp_gra_spill18
	addl $1,%esi                  	# [2] 
	addq $40,%rcx                 	# [3] 
	addq $40,%rdi                 	# [3] 
	cmpl %esi,%eax                	# [3] 
	jle .Lt_2_4866                	# [4] 
.Lt_2_5378:
 #<loop> Loop body line 81, nesting depth: 1, estimated iterations: 100
	.loc	1	81	0
	testl %r15d,%r15d             	# [0] 
	jle .Lt_2_5634                	# [1] 
.LBB13__Z16multiplyMatricesPA10_iS0_S0_iiii:
 #<loop> Part of loop body line 81, head labeled .Lt_2_5378
	xorl %r14d,%r14d              	# [0] 
	movq 72(%rsp),%r13            	# [0] _temp_gra_spill17
	movl %r15d,%eax               	# [0] 
	movq %rcx,%rbp                	# [1] 
	movq %rax,88(%rsp)            	# [1] _temp_gra_spill19
	jmp .Lt_2_6146                	# [1] 
.LBB35__Z16multiplyMatricesPA10_iS0_S0_iiii:
 #<loop> Part of loop body line 81, head labeled .Lt_2_6146
	movl %r8d,%r12d               	# [0] 
	sarl $2,%r12d                 	# [1] 
	testl %r12d,%r12d             	# [2] 
	je .Lt_2_6402                 	# [3] 
.LBB40__Z16multiplyMatricesPA10_iS0_S0_iiii:
 #<loop> Part of loop body line 81, head labeled .Lt_2_6146
.LBB34__Z16multiplyMatricesPA10_iS0_S0_iiii:
 #<loop> Loop body line 81, nesting depth: 3, estimated iterations: 25
 #<loop> unrolled 4 times
	.loc	1	92	0
	movl 0(%rdx),%r10d            	# [0] id:60
	movl 0(%rax),%r9d             	# [0] id:61
	imull %r10d,%r9d              	# [3] 
	addl %r9d,%r11d               	# [6] 
	movl %r11d,0(%rbp)            	# [7] id:62
	movl 4(%rdx),%r10d            	# [7] id:60
	movl 40(%rax),%r9d            	# [7] id:61
	imull %r10d,%r9d              	# [10] 
	addl %r9d,%r11d               	# [13] 
	movl %r11d,0(%rbp)            	# [14] id:62
	movl 8(%rdx),%r10d            	# [14] id:60
	movl 80(%rax),%r9d            	# [14] id:61
	imull %r10d,%r9d              	# [17] 
	addl %r9d,%r11d               	# [20] 
	movl %r11d,0(%rbp)            	# [21] id:62
	movl 12(%rdx),%r10d           	# [21] id:60
	movl 120(%rax),%r9d           	# [21] id:61
	imull %r10d,%r9d              	# [24] 
	addq $16,%rdx                 	# [26] 
	addl $4,%ebx                  	# [26] 
	addq $160,%rax                	# [27] 
	addl %r9d,%r11d               	# [27] 
	cmpl %ebx,%r8d                	# [27] 
	movl %r11d,0(%rbp)            	# [28] id:62
	jg .LBB34__Z16multiplyMatricesPA10_iS0_S0_iiii	# [28] 
.Lt_2_6402:
 #<loop> Part of loop body line 81, head labeled .Lt_2_6146
	addl $1,%r14d                 	# [0] 
	addq $4,%rbp                  	# [1] 
	addq $4,%r13                  	# [1] 
	cmpl %r14d,%r15d              	# [1] 
	jle .Lt_2_5634                	# [2] 
.Lt_2_6146:
 #<loop> Loop body line 81, nesting depth: 2, estimated iterations: 100
	.loc	1	81	0
	testl %r8d,%r8d               	# [0] 
	jle .Lt_2_6402                	# [1] 
.LBB16__Z16multiplyMatricesPA10_iS0_S0_iiii:
 #<loop> Part of loop body line 81, head labeled .Lt_2_6146
	movl 0(%rbp),%r11d            	# [0] id:59
	movl %r8d,%r12d               	# [0] 
	xorl %ebx,%ebx                	# [1] 
	andl $3,%r12d                 	# [1] 
	testl $3,%r8d                 	# [1] 
	movq %r13,%rax                	# [2] 
	movq %rdi,%rdx                	# [2] 
	je .LBB35__Z16multiplyMatricesPA10_iS0_S0_iiii	# [2] 
.LBB36__Z16multiplyMatricesPA10_iS0_S0_iiii:
 #<loop> Part of loop body line 81, head labeled .Lt_2_6146
	.loc	1	92	0
	movl 0(%rdi),%r10d            	# [0] id:60
	movl 0(%r13),%r9d             	# [0] id:61
	imull %r10d,%r9d              	# [3] 
	leaq 4(%rdi),%rdx             	# [5] 
	leaq 40(%r13),%rax            	# [5] 
	decl %r12d                    	# [5] 
	incl %ebx                     	# [6] 
	addl %r9d,%r11d               	# [6] 
	testl %r12d,%r12d             	# [6] 
	movl %r11d,0(%rbp)            	# [7] id:62
	je .LBB35__Z16multiplyMatricesPA10_iS0_S0_iiii	# [7] 
.LBB37__Z16multiplyMatricesPA10_iS0_S0_iiii:
 #<loop> Part of loop body line 81, head labeled .Lt_2_6146
	movl 4(%rdi),%r9d             	# [0] id:60
	movl 40(%r13),%r10d           	# [0] id:61
	imull %r9d,%r10d              	# [3] 
	movl %r12d,%r9d               	# [4] 
	addq $4,%rdx                  	# [5] 
	addq $40,%rax                 	# [5] 
	decl %r9d                     	# [5] 
	incl %ebx                     	# [6] 
	addl %r10d,%r11d              	# [6] 
	testl %r9d,%r9d               	# [6] 
	movl %r11d,0(%rbp)            	# [7] id:62
	je .LBB35__Z16multiplyMatricesPA10_iS0_S0_iiii	# [7] 
.LBB38__Z16multiplyMatricesPA10_iS0_S0_iiii:
 #<loop> Part of loop body line 81, head labeled .Lt_2_6146
	movl 0(%rdx),%r10d            	# [0] id:60
	movl 0(%rax),%r9d             	# [0] id:61
	imull %r10d,%r9d              	# [3] 
	addq $4,%rdx                  	# [6] 
	addq $40,%rax                 	# [6] 
	addl %r9d,%r11d               	# [6] 
	incl %ebx                     	# [7] 
	movl %r11d,0(%rbp)            	# [7] id:62
	jmp .LBB35__Z16multiplyMatricesPA10_iS0_S0_iiii	# [7] 
.Lt_2_4866:
	movq 56(%rsp),%rbx            	# [0] _temp_gra_spill15
	movq 40(%rsp),%rbp            	# [0] _temp_gra_spill13
	movq 32(%rsp),%r12            	# [1] _temp_gra_spill12
	movq 48(%rsp),%r13            	# [1] _temp_gra_spill14
	movq 24(%rsp),%r14            	# [1] _temp_gra_spill11
	movq 16(%rsp),%r15            	# [2] _temp_gra_spill10
	addq $152,%rsp                	# [2] 
	ret                           	# [2] 
.LDWend__Z16multiplyMatricesPA10_iS0_S0_iiii:
	.size _Z16multiplyMatricesPA10_iS0_S0_iiii, .LDWend__Z16multiplyMatricesPA10_iS0_S0_iiii-_Z16multiplyMatricesPA10_iS0_S0_iiii
	.section .text
	.p2align 5,,

	# Program Unit: _Z9enterDataPA10_iS0_iiii
.globl	_Z9enterDataPA10_iS0_iiii
	.type	_Z9enterDataPA10_iS0_iiii, @function
_Z9enterDataPA10_iS0_iiii:	# 0x488
	# .frame	%rsp, 184, %rsp
	# i = 4
	# j = 0
	# _temp_gra_spill21 = 8
	# _temp_gra_spill22 = 16
	# _temp_gra_spill23 = 24
	# _temp_gra_spill24 = 32
	# _temp_gra_spill25 = 40
	# _temp_gra_spill26 = 48
	# _temp_gra_spill27 = 56
	# _temp_gra_spill28 = 64
	# _temp_gra_spill29 = 72
	# _temp_gra_spill30 = 80
	# _temp_gra_spill31 = 88
	# _temp_gra_spill32 = 96
	# _temp_gra_spill33 = 104
	# _temp_gra_spill34 = 112
	# _temp_gra_spill35 = 120
	.loc	1	44	0
.LBB1__Z9enterDataPA10_iS0_iiii:
.LEH_adjustsp__Z9enterDataPA10_iS0_iiii:
.LEH_csr__Z9enterDataPA10_iS0_iiii:
	addq $-184,%rsp               	# [0] 
	movq %rbp,40(%rsp)            	# [1] _temp_gra_spill25
	movq %r12,32(%rsp)            	# [2] _temp_gra_spill24
	movq %r13,48(%rsp)            	# [2] _temp_gra_spill26
	movq %r14,24(%rsp)            	# [2] _temp_gra_spill23
	movq %rbx,56(%rsp)            	# [3] _temp_gra_spill27
	movq %rdi,%rbx                	# [3] 
	movq %rdx,88(%rsp)            	# [3] _temp_gra_spill31
	movq %r15,16(%rsp)            	# [4] _temp_gra_spill22
	movl %ecx,%r15d               	# [4] 
	movq %r8,80(%rsp)             	# [4] _temp_gra_spill30
.L_3_7426:
	xorl %eax,%eax                	# [0] 
	.loc	1	47	0
	movq $(.rodata+256),%rdi      	# [0] .rodata+256
	movq %r9,120(%rsp)            	# [1] _temp_gra_spill35
	movq %rsi,8(%rsp)             	# [1] _temp_gra_spill21
	call printf                   	# [1] printf
.LBB3__Z9enterDataPA10_iS0_iiii:
	movq 88(%rsp),%rax            	# [0] _temp_gra_spill31
	cmpl $0,%eax                  	# [3] 
	jle .Lt_3_2306                	# [4] 
.LBB4__Z9enterDataPA10_iS0_iiii:
	movl $1,%r14d                 	# [0] 
	movq %rbx,%rdi                	# [0] 
	xorl %esi,%esi                	# [0] 
	movq %rax,64(%rsp)            	# [1] _temp_gra_spill28
	movl %esi,4(%rsp)             	# [1] i
	movq %rdi,104(%rsp)           	# [1] _temp_gra_spill33
.Lt_3_2818:
 #<loop> Loop body line 47, nesting depth: 1, estimated iterations: 100
	testl %r15d,%r15d             	# [0] 
	jle .Lt_3_3074                	# [1] 
.LBB7__Z9enterDataPA10_iS0_iiii:
 #<loop> Part of loop body line 47, head labeled .Lt_3_2818
	xorl %r13d,%r13d              	# [0] 
	movq 104(%rsp),%rbp           	# [1] _temp_gra_spill33
	movl %r14d,%r12d              	# [1] 
	movl $1,%ebx                  	# [1] 
.Lt_3_3586:
 #<loop> Loop body line 47, nesting depth: 2, estimated iterations: 100
	xorl %eax,%eax                	# [0] 
	.loc	1	52	0
	movl %ebx,%edx                	# [0] 
	movl %r14d,%esi               	# [1] 
	movq $(.rodata+288),%rdi      	# [1] .rodata+288
	call printf                   	# [1] printf
.LBB10__Z9enterDataPA10_iS0_iiii:
 #<loop> Part of loop body line 47, head labeled .Lt_3_3586
	.loc	1	54	0
	movl %r12d,0(%rbp)            	# [0] id:47
	addl %r14d,%r12d              	# [0] 
	addl $1,%r13d                 	# [0] 
	addl $1,%ebx                  	# [1] 
	addq $4,%rbp                  	# [1] 
	cmpl %r13d,%r15d              	# [1] 
	jg .Lt_3_3586                 	# [2] 
.Lt_3_3074:
 #<loop> Part of loop body line 47, head labeled .Lt_3_2818
	movl 4(%rsp),%edi             	# [0] i
	movq 88(%rsp),%rax            	# [1] _temp_gra_spill31
	movq 104(%rsp),%rsi           	# [1] _temp_gra_spill33
	incl %edi                     	# [3] 
	addl $1,%r14d                 	# [4] 
	addq $40,%rsi                 	# [4] 
	cmpl %edi,%eax                	# [4] 
	movq %rsi,104(%rsp)           	# [5] _temp_gra_spill33
	movl %edi,4(%rsp)             	# [5] i
	jg .Lt_3_2818                 	# [5] 
.Lt_3_2306:
	xorl %eax,%eax                	# [0] 
	.loc	1	59	0
	movq $(.rodata+320),%rdi      	# [0] .rodata+320
	call printf                   	# [0] printf
.LBB13__Z9enterDataPA10_iS0_iiii:
	movq 80(%rsp),%rax            	# [0] _temp_gra_spill30
	cmpl $0,%eax                  	# [3] 
	jle .Lt_3_4354                	# [4] 
.LBB14__Z9enterDataPA10_iS0_iiii:
	movq 8(%rsp),%rdi             	# [0] _temp_gra_spill21
	movl $1,%r14d                 	# [2] 
	movq %rax,72(%rsp)            	# [2] _temp_gra_spill29
	xorl %esi,%esi                	# [2] 
	movq 120(%rsp),%r15           	# [3] _temp_gra_spill35
	movl %esi,4(%rsp)             	# [3] i
	movq %rdi,112(%rsp)           	# [3] _temp_gra_spill34
.Lt_3_4866:
 #<loop> Loop body line 59, nesting depth: 1, estimated iterations: 100
	testl %r15d,%r15d             	# [0] 
	jle .Lt_3_5122                	# [1] 
.LBB17__Z9enterDataPA10_iS0_iiii:
 #<loop> Part of loop body line 59, head labeled .Lt_3_4866
	xorl %r13d,%r13d              	# [0] 
	movq 112(%rsp),%rbp           	# [0] _temp_gra_spill34
	movl %r15d,%eax               	# [0] 
	movl %r14d,%r12d              	# [1] 
	movl $1,%ebx                  	# [1] 
	movq %rax,96(%rsp)            	# [1] _temp_gra_spill32
.Lt_3_5634:
 #<loop> Loop body line 59, nesting depth: 2, estimated iterations: 100
	xorl %eax,%eax                	# [0] 
	.loc	1	64	0
	movl %ebx,%edx                	# [0] 
	movl %r14d,%esi               	# [1] 
	movq $(.rodata+352),%rdi      	# [1] .rodata+352
	call printf                   	# [1] printf
.LBB20__Z9enterDataPA10_iS0_iiii:
 #<loop> Part of loop body line 59, head labeled .Lt_3_5634
	.loc	1	66	0
	movl %r12d,0(%rbp)            	# [0] id:48
	addl %r14d,%r12d              	# [0] 
	addl $1,%r13d                 	# [0] 
	addl $1,%ebx                  	# [1] 
	addq $4,%rbp                  	# [1] 
	cmpl %r13d,%r15d              	# [1] 
	jg .Lt_3_5634                 	# [2] 
.Lt_3_5122:
 #<loop> Part of loop body line 59, head labeled .Lt_3_4866
	movl 4(%rsp),%edi             	# [0] i
	movq 80(%rsp),%rax            	# [1] _temp_gra_spill30
	movq 112(%rsp),%rsi           	# [1] _temp_gra_spill34
	incl %edi                     	# [3] 
	addl $1,%r14d                 	# [4] 
	addq $40,%rsi                 	# [4] 
	cmpl %edi,%eax                	# [4] 
	movq %rsi,112(%rsp)           	# [5] _temp_gra_spill34
	movl %edi,4(%rsp)             	# [5] i
	jg .Lt_3_4866                 	# [5] 
.Lt_3_4354:
	movq 56(%rsp),%rbx            	# [0] _temp_gra_spill27
	movq 40(%rsp),%rbp            	# [0] _temp_gra_spill25
	movq 32(%rsp),%r12            	# [1] _temp_gra_spill24
	movq 48(%rsp),%r13            	# [1] _temp_gra_spill26
	movq 24(%rsp),%r14            	# [1] _temp_gra_spill23
	movq 16(%rsp),%r15            	# [2] _temp_gra_spill22
	addq $184,%rsp                	# [2] 
	ret                           	# [2] 
.L_3_7682:
.LDWend__Z9enterDataPA10_iS0_iiii:
	.size _Z9enterDataPA10_iS0_iiii, .LDWend__Z9enterDataPA10_iS0_iiii-_Z9enterDataPA10_iS0_iiii

	.section .rodata
	.org 0x100
	.align	0
	# offset 256
	.byte	0xa, 0x45, 0x6e, 0x74, 0x65, 0x72, 0x20, 0x65	# \nEnter e
	.byte	0x6c, 0x65, 0x6d, 0x65, 0x6e, 0x74, 0x73, 0x20	# lements 
	.byte	0x6f, 0x66, 0x20, 0x6d, 0x61, 0x74, 0x72, 0x69	# of matri
	.byte	0x78, 0x20, 0x31, 0x3a, 0xa, 0x0	# x 1:\n\000
	.org 0x120
	.align	0
	# offset 288
	.string "Enter elements a%d%d: "
	.org 0x140
	.align	0
	# offset 320
	.byte	0xa, 0x45, 0x6e, 0x74, 0x65, 0x72, 0x20, 0x65	# \nEnter e
	.byte	0x6c, 0x65, 0x6d, 0x65, 0x6e, 0x74, 0x73, 0x20	# lements 
	.byte	0x6f, 0x66, 0x20, 0x6d, 0x61, 0x74, 0x72, 0x69	# of matri
	.byte	0x78, 0x20, 0x32, 0x3a, 0xa, 0x0	# x 2:\n\000
	.org 0x160
	.align	0
	# offset 352
	.string "Enter elements b%d%d: "

	.section .except_table
	.align	0
	.type	.range_table._Z9enterDataPA10_iS0_iiii, @object
.range_table._Z9enterDataPA10_iS0_iiii:	# 0x10
	# offset 16
	.byte	255
	# offset 17
	.byte	0
	.uleb128	.LSDATTYPEB3-.LSDATTYPED3
.LSDATTYPED3:
	# offset 22
	.byte	1
	.uleb128	.LSDACSE3-.LSDACSB3
.LSDACSB3:
	.uleb128	.L_3_7426-_Z9enterDataPA10_iS0_iiii
	.uleb128	.L_3_7682-.L_3_7426
	# offset 33
	.uleb128	0
	# offset 37
	.uleb128	0
.LSDACSE3:
	# offset 41
	.sleb128	0
	# offset 45
	.sleb128	0
.LSDATTYPEB3:
	# end of initialization for .range_table._Z9enterDataPA10_iS0_iiii
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0xe0, 0x09
	.align 8
.LFDE1_end:
	.4byte	.LFDE2_end - .LFDE2_begin
.LFDE2_begin:
	.4byte	.LFDE2_begin - .LEHCIE
	.quad	.LBB1__Z7displayPA10_iii
	.quad	.LDWend__Z7displayPA10_iii - .LBB1__Z7displayPA10_iii
	.byte	0x08
	.quad	.range_table._Z7displayPA10_iii
	.byte	0x04
	.4byte	.LEH_adjustsp__Z7displayPA10_iii - .LBB1__Z7displayPA10_iii
	.byte	0x0e, 0x80, 0x01, 0x04
	.4byte	.LEH_csr__Z7displayPA10_iii - .LEH_adjustsp__Z7displayPA10_iii
	.byte	0x8e, 0x0e, 0x8c, 0x0d, 0x86, 0x0c, 0x8f, 0x0b
	.byte	0x8d, 0x0a, 0x83, 0x09
	.align 8
.LFDE2_end:
	.4byte	.LFDE3_end - .LFDE3_begin
.LFDE3_begin:
	.4byte	.LFDE3_begin - .LEHCIE
	.quad	.LBB1__Z9enterDataPA10_iS0_iiii
	.quad	.LDWend__Z9enterDataPA10_iS0_iiii - .LBB1__Z9enterDataPA10_iS0_iiii
	.byte	0x08
	.quad	.range_table._Z9enterDataPA10_iS0_iiii
	.byte	0x04
	.4byte	.LEH_adjustsp__Z9enterDataPA10_iS0_iiii - .LBB1__Z9enterDataPA10_iS0_iiii
	.byte	0x0e, 0xc0, 0x01, 0x04
	.4byte	.LEH_csr__Z9enterDataPA10_iS0_iiii - .LEH_adjustsp__Z9enterDataPA10_iS0_iiii
	.byte	0x8f, 0x16, 0x8e, 0x15, 0x8c, 0x14, 0x86, 0x13
	.byte	0x8d, 0x12, 0x83, 0x11
	.align 8
.LFDE3_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test51.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

