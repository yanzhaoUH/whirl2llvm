	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test666.c (test666.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test666.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 8, %rsp
	.loc	1	8	0
 #   4  /*int fun(char* a, short* b, int* c, long* d, float* e, double* f){
 #   5  printf("aaaa");
 #   6  }*/
 #   7  
 #   8  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-8,%rsp                 	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_2562:
	xorl %eax,%eax                	# [0] 
	.loc	1	16	0
 #  12  
 #  13    if(i>1){
 #  14  
 #  15       if(j>3){
 #  16           printf("hello world 1\n");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	21	0
 #  17       }
 #  18    }
 #  19  
 #  20    if(j>1){
 #  21     printf("hello world 2\n");  
	movq $(.rodata+16),%rdi       	# [0] .rodata+16
	call printf                   	# [0] printf
.LBB4_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	74	0
 #  70    printf("%d %d %d %d %d %d %d", c3, s3, i3, l3, ll3, ui3, ul3);
 #  71  
 #  72    printf("%f %f", f3, d3); */
 #  73  
 #  74      return 0;
	addq $8,%rsp                  	# [0] 
	ret                           	# [0] 
.L_0_2818:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.byte	0x68, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x77, 0x6f	# hello wo
	.byte	0x72, 0x6c, 0x64, 0x20, 0x31, 0xa, 0x0	# rld 1\n\000
	.org 0x10
	.align	0
	# offset 16
	.byte	0x68, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x77, 0x6f	# hello wo
	.byte	0x72, 0x6c, 0x64, 0x20, 0x32, 0xa, 0x0	# rld 2\n\000

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_2562-main
	.uleb128	.L_0_2818-.L_0_2562
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x10
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test666.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

