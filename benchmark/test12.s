	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test12.c (test12.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test12.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 56, %rsp
	# a = 0
	# b = 8
	# c = 16
	# _temp_gra_spill1 = 24
	# _temp_gra_spill2 = 32
	# _temp_gra_spill3 = 40
	.loc	1	4	0
 #   1  #include <stdio.h>
 #   2  #include <math.h>
 #   3  
 #   4  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-56,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_5378:
	xorl %eax,%eax                	# [0] 
	.loc	1	8	0
 #   5  {
 #   6      double a, b, c, determinant, root1,root2, realPart, imaginaryPart;
 #   7  
 #   8      printf("Enter coefficients a, b and c: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	9	0
 #   9      scanf("%lf %lf %lf",&a, &b, &c);
	leaq 16(%rsp),%rcx            	# [0] c
	leaq 8(%rsp),%rdx             	# [0] b
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	.loc	1	14	0
 #  10      //a = 1.0;
 #  11      //c = 2.0;
 #  12      //b = 3.0;    
 #  13  
 #  14      determinant = b*b-4*a*c;
	movsd 0(%rsp),%xmm6           	# [0] a
	movsd .rodata+48(%rip),%xmm0  	# [1] 
	movsd 8(%rsp),%xmm7           	# [4] b
	mulsd %xmm6,%xmm0             	# [4] 
	movsd 16(%rsp),%xmm2          	# [6] c
	movaps %xmm7,%xmm4            	# [7] 
	mulsd %xmm4,%xmm4             	# [8] 
	mulsd %xmm2,%xmm0             	# [9] 
	addsd %xmm0,%xmm4             	# [14] 
	.loc	1	17	0
 #  15  
 #  16      // condition for real and different roots
 #  17      if (determinant > 0)
	xorps %xmm1,%xmm1             	# [16] 
	comisd %xmm1,%xmm4            	# [17] 
	jbe .Lt_0_2818                	# [21] 
.LBB5_main:
	.loc	1	20	0
 #  18      {
 #  19      // sqrt() function returns square root
 #  20          root1 = (-b+sqrt(a))/(2*a);
	sqrtsd %xmm6,%xmm0            	# [0] 
	comisd %xmm0,%xmm0            	# [58] 
	movaps %xmm0,%xmm1            	# [62] 
	jne .LBB8_main                	# [62] 
.LBB6_main:
	comisd %xmm0,%xmm0            	# [0] 
	jp .LBB8_main                 	# [4] 
.LBB43_main:
	jmp .Lt_0_4098                	# [0] 
.LBB8_main:
	movaps %xmm6,%xmm0            	# [0] 
	movsd %xmm4,40(%rsp)          	# [0] _temp_gra_spill3
	.globl	sqrt
	call sqrt                     	# [0] sqrt
.LBB9_main:
	movaps %xmm0,%xmm1            	# [0] 
	movsd 8(%rsp),%xmm7           	# [1] b
	movsd 0(%rsp),%xmm6           	# [2] a
	movsd 40(%rsp),%xmm4          	# [3] _temp_gra_spill3
.Lt_0_4098:
	.loc	1	21	0
 #  21          root2 = (-b-sqrt(determinant))/(2*a);
	sqrtsd %xmm4,%xmm0            	# [0] 
	.loc	1	20	0
	movsd .rodata+64(%rip),%xmm8  	# [22] 
	movaps %xmm6,%xmm5            	# [24] 
	mulsd %xmm8,%xmm5             	# [25] 
	movaps %xmm1,%xmm3            	# [26] 
	subsd %xmm7,%xmm3             	# [27] 
	divsd %xmm5,%xmm3             	# [30] 
	.loc	1	21	0
	comisd %xmm0,%xmm0            	# [58] 
	.loc	1	20	0
	movsd %xmm3,32(%rsp)          	# [62] _temp_gra_spill2
	.loc	1	21	0
	movaps %xmm0,%xmm2            	# [62] 
	jne .LBB13_main               	# [62] 
.LBB11_main:
	comisd %xmm0,%xmm0            	# [0] 
	jp .LBB13_main                	# [4] 
.LBB44_main:
	jmp .Lt_0_4354                	# [0] 
.LBB13_main:
	movaps %xmm4,%xmm0            	# [0] 
	call sqrt                     	# [0] sqrt
.LBB14_main:
	movaps %xmm0,%xmm2            	# [0] 
	movsd 0(%rsp),%xmm0           	# [1] a
	movsd .rodata+64(%rip),%xmm5  	# [2] 
	movsd 8(%rsp),%xmm7           	# [5] b
	mulsd %xmm0,%xmm5             	# [5] 
.Lt_0_4354:
	.loc	1	23	0
 #  22  
 #  23          printf("root1 = %.2lf and root2 = %.2lf",root1 , root2);
	addsd %xmm2,%xmm7             	# [0] 
	movsd .rodata+112(%rip),%xmm1 	# [1] 
	movl $2,%eax                  	# [4] 
	movsd 32(%rsp),%xmm0          	# [4] _temp_gra_spill2
	xorps %xmm7,%xmm1             	# [4] 
	movq $(.rodata+80),%rdi       	# [5] .rodata+80
	divsd %xmm5,%xmm1             	# [5] 
	call printf                   	# [5] printf
.LBB42_main:
.Lt_0_4866:
.Lt_0_4610:
	xorq %rax,%rax                	# [0] 
	.loc	1	42	0
 #  38          imaginaryPart = sqrt(-determinant)/(2*a);
 #  39          printf("root1 = %.2lf+%.2lfi and root2 = %.2f-%.2fi", realPart, imaginaryPart, realPart, imaginaryPart);
 #  40      }
 #  41  
 #  42      return 0;
	addq $56,%rsp                 	# [0] 
	ret                           	# [0] 
.Lt_0_2818:
	.loc	1	27	0
	movsd .rodata+120(%rip),%xmm2 	# [0] 
	mulsd %xmm6,%xmm2             	# [3] 
	movaps %xmm7,%xmm0            	# [7] 
	divsd %xmm2,%xmm0             	# [8] 
	comisd %xmm1,%xmm4            	# [36] 
	movsd %xmm0,24(%rsp)          	# [40] _temp_gra_spill1
	jp .Lt_0_3586                 	# [40] 
.LBB18_main:
	comisd %xmm1,%xmm4            	# [0] 
	je .LBB20_main                	# [4] 
.Lt_0_3586:
	.loc	1	38	0
	movsd .rodata+112(%rip),%xmm1 	# [0] 
	xorps %xmm4,%xmm1             	# [3] 
	sqrtsd %xmm1,%xmm0            	# [4] 
	comisd %xmm0,%xmm0            	# [62] 
	movaps %xmm0,%xmm5            	# [66] 
	jne .LBB25_main               	# [66] 
.LBB23_main:
	comisd %xmm0,%xmm0            	# [0] 
	jp .LBB25_main                	# [4] 
.Lt_0_5122:
	.loc	1	39	0
	movsd .rodata+64(%rip),%xmm4  	# [0] 
	mulsd %xmm6,%xmm4             	# [3] 
	movaps %xmm5,%xmm3            	# [7] 
	divsd %xmm4,%xmm3             	# [8] 
	movsd 24(%rsp),%xmm2          	# [36] _temp_gra_spill1
	movl $4,%eax                  	# [39] 
	movaps %xmm2,%xmm0            	# [39] 
	movq $(.rodata+160),%rdi      	# [40] .rodata+160
	movaps %xmm3,%xmm1            	# [40] 
	call printf                   	# [40] printf
.LBB36_main:
	jmp .Lt_0_4866                	# [0] 
.LBB25_main:
	.loc	1	38	0
	movaps %xmm1,%xmm0            	# [0] 
	call sqrt                     	# [0] sqrt
.LBB26_main:
	movsd 0(%rsp),%xmm6           	# [0] a
	movaps %xmm0,%xmm5            	# [1] 
	jmp .Lt_0_5122                	# [1] 
.LBB20_main:
	.loc	1	31	0
	movl $1,%eax                  	# [0] 
	movq $(.rodata+128),%rdi      	# [0] .rodata+128
	call printf                   	# [0] printf
.LBB37_main:
	jmp .Lt_0_4866                	# [0] 
.L_0_8706:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter coefficients a, b and c: "
	.org 0x20
	.align	0
	# offset 32
	.string "%lf %lf %lf"
	.org 0x30
	.align	0
	# offset 48
	.4byte	0
	.4byte	-1072693248
	# double -4.00000
	.org 0x38
	.align	0
	# offset 56
	.4byte	0
	.4byte	0
	# double 0.00000
	.org 0x40
	.align	0
	# offset 64
	.4byte	0
	.4byte	1073741824
	# double 2.00000
	.org 0x50
	.align	0
	# offset 80
	.string "root1 = %.2lf and root2 = %.2lf"
	.org 0x70
	.align	0
	# offset 112
	.quad	0x8000000000000000
	.org 0x78
	.align	0
	# offset 120
	.4byte	0
	.4byte	-1073741824
	# double -2.00000
	.org 0x80
	.align	0
	# offset 128
	.string "root1 = root2 = %.2lf;"
	.org 0xa0
	.align	0
	# offset 160
	.string "root1 = %.2lf+%.2lfi and root2 = %.2f-%.2fi"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_5378-main
	.uleb128	.L_0_8706-.L_0_5378
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x40
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test12.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

