	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test73.c (test73.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test73.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# number = 0
	.loc	1	13	0
 #   9      #include <stdio.h>
 #  10  
 #  11       
 #  12  
 #  13      int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_1282:
	xorl %eax,%eax                	# [0] 
	.loc	1	21	0
 #  17          int number;
 #  18  
 #  19       
 #  20  
 #  21          printf("Enter a number \n");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	23	0
 #  22  
 #  23          scanf("%d", &number);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	.loc	1	25	0
 #  24  
 #  25          if (number >= 0)
	movl 0(%rsp),%esi             	# [0] number
	testl %esi,%esi               	# [3] 
	jl .Lt_0_770                  	# [4] 
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	27	0
 #  26  
 #  27              printf("%d is a positive number \n", number);
	movl %esi,%esi                	# [1] 
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call printf                   	# [1] printf
.LBB15_main:
.Lt_0_1026:
	xorq %rax,%rax                	# [0] 
	.loc	1	32	0
 #  28  
 #  29          else
 #  30  
 #  31              printf("%d is a negative number \n", number);
 #  32          return 0;
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.Lt_0_770:
	xorl %eax,%eax                	# [0] 
	.loc	1	31	0
	movl %esi,%esi                	# [1] 
	movq $(.rodata+80),%rdi       	# [1] .rodata+80
	call printf                   	# [1] printf
.LBB11_main:
	jmp .Lt_0_1026                	# [0] 
.L_0_1538:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.byte	0x45, 0x6e, 0x74, 0x65, 0x72, 0x20, 0x61, 0x20	# Enter a 
	.byte	0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x20, 0xa	# number \n
	.byte	0x0	# \000
	.org 0x20
	.align	0
	# offset 32
	.string "%d"
	.org 0x30
	.align	0
	# offset 48
	.byte	0x25, 0x64, 0x20, 0x69, 0x73, 0x20, 0x61, 0x20	# %d is a 
	.byte	0x70, 0x6f, 0x73, 0x69, 0x74, 0x69, 0x76, 0x65	# positive
	.byte	0x20, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x20	#  number 
	.byte	0xa, 0x0	# \n\000
	.org 0x50
	.align	0
	# offset 80
	.byte	0x25, 0x64, 0x20, 0x69, 0x73, 0x20, 0x61, 0x20	# %d is a 
	.byte	0x6e, 0x65, 0x67, 0x61, 0x74, 0x69, 0x76, 0x65	# negative
	.byte	0x20, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x20	#  number 
	.byte	0xa, 0x0	# \n\000

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_1282-main
	.uleb128	.L_0_1538-.L_0_1282
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test73.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

