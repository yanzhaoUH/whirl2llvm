#include <stdio.h>
int main()
{
    int base, exponent;

    long long result = 1;

    printf("Enter a base number: ");
    scanf("%d", &base);
    //base = 3;
    printf("Enter an exponent: 4");
    scanf("%d", &exponent);
    //exponent = 4; 
    while (exponent != 0)
    {
        result *= base;
        --exponent;
    }

    printf("Answer = %lld", result);

    return 0;
}
