	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test339.c (test339.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test339.c"


	.text
	.align	2

	.data

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 56, %rsp
	# b = 32
	.loc	1	14	0
 #  10  }
 #  11  
 #  12  char g_c = 'a';
 #  13  
 #  14  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-56,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_258:
	xorl %eax,%eax                	# [0] 
	.loc	1	17	0
 #  15  {
 #  16   
 #  17  printf("test1\n");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	.loc	1	20	0
 #  18  char* ap = "hello";
 #  19  char b[10];
 #  20  char*cp = (char*) memcpy(b,ap, 6);
	movq $(.rodata+16),%rdx       	# [0] .rodata+16
	movl 0(%rdx),%ecx             	# [1] id:32 not_variable+0x0
	movzwl 4(%rdx),%edx           	# [1] id:32 not_variable+0x0
	xorl %eax,%eax                	# [3] 
	.loc	1	21	0
 #  21  printf("%s\n",cp);
	movq $(.rodata+32),%rdi       	# [3] .rodata+32
	.loc	1	20	0
	leaq 32(%rsp),%rsi            	# [3] b
	movw %dx,36(%rsp)             	# [4] b+4
	movl %ecx,32(%rsp)            	# [4] b
	.loc	1	21	0
	call printf                   	# [4] printf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	23	0
 #  22  
 #  23  printf("test2\n");
	movq $(.rodata+48),%rdi       	# [0] .rodata+48
	call printf                   	# [0] printf
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	26	0
 #  24  
 #  25      int n =2;
 #  26      printf("%d\n", n);
	movq $2,%rsi                  	# [1] 
	movq $(.rodata+64),%rdi       	# [1] .rodata+64
	call printf                   	# [1] printf
.LBB6_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	29	0
 #  27      {
 #  28  	int n = 33;
 #  29          printf("%d\n", n);
	movq $33,%rsi                 	# [1] 
	movq $(.rodata+64),%rdi       	# [1] .rodata+64
	call printf                   	# [1] printf
.LBB7_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	32	0
 #  30      }
 #  31  
 #  32      printf("%d\n", n);
	movq $2,%rsi                  	# [1] 
	movq $(.rodata+64),%rdi       	# [1] .rodata+64
	call printf                   	# [1] printf
.LBB8_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	35	0
 #  33  
 #  34  
 #  35  printf("test3\n");
	movq $(.rodata+80),%rdi       	# [0] .rodata+80
	call printf                   	# [0] printf
.LBB9_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	38	0
 #  36  
 #  37      int a[3] = {1,2,3};
 #  38      printf("%d\n",a[1]);
	movl .data+20(%rip),%esi      	# [1] id:36 a.init+0x4
	movq $(.rodata+64),%rdi       	# [1] .rodata+64
	call printf                   	# [1] printf
.LBB10_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	40	0
 #  39  
 #  40  printf("test4\n");
	movq $(.rodata+96),%rdi       	# [0] .rodata+96
	call printf                   	# [0] printf
.LBB11_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	43	0
 #  41  
 #  42      char c[3] = {'a', 'b','c'};
 #  43      printf("%c\n", c[1]);
	movsbl .data+33(%rip),%esi    	# [1] id:39 c.init+0x1
	movq $(.rodata+112),%rdi      	# [1] .rodata+112
	call printf                   	# [1] printf
.LBB12_main:
	.loc	1	46	0
 #  44  
 #  45      float f[3] ={1.0, 2.0, 3.0};
 #  46      printf("%f\n",f[1]);
	movss .data+52(%rip),%xmm0    	# [0] id:42 f.init+0x4
	movl $1,%eax                  	# [2] 
	movq $(.rodata+128),%rdi      	# [3] .rodata+128
	cvtss2sd %xmm0,%xmm0          	# [3] 
	call printf                   	# [3] printf
.LBB13_main:
	.loc	1	49	0
 #  47  
 #  48      double d[3] = {1.0, 2.0, 3.0};
 #  49      printf("%f\n", d[1]);
	movl $1,%eax                  	# [0] 
	movsd .data+72(%rip),%xmm0    	# [1] 
	movq $(.rodata+128),%rdi      	# [1] .rodata+128
	call printf                   	# [1] printf
.LBB14_main:
	.loc	1	50	0
 #  50      printf("%f\n",f1(d[1]));
	movsd .data+72(%rip),%xmm0    	# [0] 
	call _Z2f1d                   	# [0] _Z2f1d
.LBB15_main:
	movl $1,%eax                  	# [0] 
	movq $(.rodata+128),%rdi      	# [0] .rodata+128
	call printf                   	# [0] printf
.LBB16_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	52	0
 #  51    
 #  52  return 0;
	addq $56,%rsp                 	# [0] 
	ret                           	# [0] 
.L_0_514:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .data
	.org 0x0
	.align	0
.globl	g_c
	.type	g_c, @object
	.size	g_c, 1
g_c:	# 0x0
	# offset 0
	.byte	97
	# end of initialization for g_c
	.org 0x10
	.align	0
	.type	a.init_1_18, @object
	.size	a.init_1_18, 12
a.init_1_18:	# 0x10
	# offset 16
	.4byte	1
	# offset 20
	.4byte	2
	# offset 24
	.4byte	3
	# end of initialization for a.init
	.org 0x20
	.align	0
	.type	c.init_1_22, @object
	.size	c.init_1_22, 3
c.init_1_22:	# 0x20
	# offset 32
	.byte	97
	# offset 33
	.byte	98
	# offset 34
	.byte	99
	# end of initialization for c.init
	.org 0x30
	.align	0
	.type	f.init_1_25, @object
	.size	f.init_1_25, 12
f.init_1_25:	# 0x30
	# offset 48
	.4byte	1065353216
	# float 1.00000
	# offset 52
	.4byte	1073741824
	# float 2.00000
	# offset 56
	.4byte	1077936128
	# float 3.00000
	# end of initialization for f.init
	.org 0x40
	.align	0
	.type	d.init_1_28, @object
	.size	d.init_1_28, 24
d.init_1_28:	# 0x40
	# offset 64
	.4byte	0
	.4byte	1072693248
	# double 1.00000
	# offset 72
	.4byte	0
	.4byte	1073741824
	# double 2.00000
	# offset 80
	.4byte	0
	.4byte	1074266112
	# double 3.00000
	# end of initialization for d.init

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.byte	0x74, 0x65, 0x73, 0x74, 0x31, 0xa, 0x0	# test1\n\000
	.org 0x10
	.align	0
	# offset 16
	.string "hello"
	.org 0x20
	.align	0
	# offset 32
	.byte	0x25, 0x73, 0xa, 0x0	# %s\n\000
	.org 0x30
	.align	0
	# offset 48
	.byte	0x74, 0x65, 0x73, 0x74, 0x32, 0xa, 0x0	# test2\n\000
	.org 0x40
	.align	0
	# offset 64
	.byte	0x25, 0x64, 0xa, 0x0	# %d\n\000
	.org 0x50
	.align	0
	# offset 80
	.byte	0x74, 0x65, 0x73, 0x74, 0x33, 0xa, 0x0	# test3\n\000
	.org 0x60
	.align	0
	# offset 96
	.byte	0x74, 0x65, 0x73, 0x74, 0x34, 0xa, 0x0	# test4\n\000
	.org 0x70
	.align	0
	# offset 112
	.byte	0x25, 0x63, 0xa, 0x0	# %c\n\000
	.org 0x80
	.align	0
	# offset 128
	.byte	0x25, 0x66, 0xa, 0x0	# %f\n\000

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_258-main
	.uleb128	.L_0_514-.L_0_258
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.p2align 5,,

	# Program Unit: _Z2f1d
.globl	_Z2f1d
	.type	_Z2f1d, @function
_Z2f1d:	# 0xec
	# .frame	%rsp, 24, %rsp
	.loc	1	7	0
.LBB1__Z2f1d:
	addq $-24,%rsp                	# [0] 
	.loc	1	9	0
	sqrtsd %xmm0,%xmm1            	# [0] 
	comisd %xmm1,%xmm1            	# [58] 
	.loc	1	7	0
	movaps %xmm0,%xmm3            	# [62] 
	.loc	1	9	0
	jne .LBB4__Z2f1d              	# [62] 
.LBB2__Z2f1d:
	comisd %xmm1,%xmm1            	# [0] 
	jp .LBB4__Z2f1d               	# [4] 
.Lt_1_1026:
	movaps %xmm1,%xmm0            	# [0] 
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.LBB4__Z2f1d:
	movaps %xmm3,%xmm0            	# [0] 
	.globl	sqrt
	call sqrt                     	# [0] sqrt
.LBB5__Z2f1d:
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.LDWend__Z2f1d:
	.size _Z2f1d, .LDWend__Z2f1d-_Z2f1d
	.section .text
	.align	4
	.section .data
	.align	16
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x40
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test339.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

