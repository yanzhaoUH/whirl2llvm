	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test16.c (test16.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test16.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# n = 0
	# i = 8
	# sum = 4
	.loc	1	2	0
 #   1  #include <stdio.h>
 #   2  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_2050:
	xorl %eax,%eax                	# [0] 
	.loc	1	6	0
 #   3  {
 #   4      int n, i, sum = 0;
 #   5      
 #   6      printf("Enter a positive integer: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	7	0
 #   7      scanf("%d",&n);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	movl 0(%rsp),%edx             	# [0] n
	cmpl $1,%edx                  	# [3] 
	jl .Lt_0_1794                 	# [4] 
.LBB5_main:
	movl %edx,%ecx                	# [0] 
	xorl %esi,%esi                	# [1] 
	andl $3,%ecx                  	# [1] 
	testl $3,%edx                 	# [1] 
	movl $1,%eax                  	# [2] 
	movl %edx,%r8d                	# [2] 
	je .LBB18_main                	# [2] 
.LBB19_main:
	decl %ecx                     	# [0] 
	incl %esi                     	# [1] 
	testl %ecx,%ecx               	# [1] 
	movl $2,%eax                  	# [2] 
	je .LBB18_main                	# [2] 
.LBB20_main:
	movl %ecx,%edi                	# [0] 
	decl %edi                     	# [1] 
	incl %eax                     	# [2] 
	addl $2,%esi                  	# [2] 
	testl %edi,%edi               	# [2] 
	je .LBB18_main                	# [3] 
.LBB21_main:
	.loc	1	13	0
 #   9      
 #  10  
 #  11      for(i=1; i <= n; ++i)
 #  12      {
 #  13          sum += i;   // sum = sum+i;
	addl %eax,%esi                	# [0] 
	incl %eax                     	# [0] 
.LBB18_main:
	movl %r8d,%r9d                	# [0] 
	sarl $2,%r9d                  	# [1] 
	testl %r9d,%r9d               	# [2] 
	je .Lt_0_770                  	# [3] 
.LBB23_main:
.LBB17_main:
 #<loop> Loop body line 7, nesting depth: 1, estimated iterations: 25
 #<loop> unrolled 4 times
	leal 1(%rax),%r8d             	# [0] 
	leal 1(%r8),%ecx              	# [2] 
	leal 1(%rcx),%edi             	# [4] 
	addl %eax,%esi                	# [5] 
	addl %r8d,%esi                	# [6] 
	leal 1(%rdi),%eax             	# [6] 
	addl %ecx,%esi                	# [7] 
	addl %edi,%esi                	# [8] 
	cmpl %eax,%edx                	# [8] 
	jge .LBB17_main               	# [9] 
.Lt_0_770:
	xorl %eax,%eax                	# [0] 
	.loc	1	16	0
 #  14      }
 #  15  
 #  16      printf("Sum = %d",sum);
	movl %esi,%esi                	# [1] 
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call printf                   	# [1] printf
.LBB11_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	18	0
 #  17  
 #  18      return 0;
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.Lt_0_1794:
	xorl %esi,%esi                	# [0] 
	jmp .Lt_0_770                 	# [0] 
.L_0_2306:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter a positive integer: "
	.org 0x20
	.align	0
	# offset 32
	.string "%d"
	.org 0x30
	.align	0
	# offset 48
	.string "Sum = %d"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_2050-main
	.uleb128	.L_0_2306-.L_0_2050
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test16.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

