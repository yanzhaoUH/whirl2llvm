; ModuleID = 'test31.bc'

@.str = private constant [27 x i8] c"Enter a positive integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [20 x i8] c"Factors of %d are: \00", align 1
@.str.3 = private constant [4 x i8] c"%d \00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_51 = alloca i32
  %.preg_I4_4_50 = alloca i32
  %.preg_I4_4_52 = alloca i32
  %.preg_I4_4_49 = alloca i32
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %_temp_dummy13_9 = alloca i32, align 4
  %_temp_ehpit0_10 = alloca i32, align 4
  %i_5 = alloca i32, align 4
  %number_4 = alloca i32, align 4
  %old_frame_pointer_15.addr = alloca i64, align 8
  %return_address_16.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %number_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = bitcast i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.2, i32 0, i32 0) to i8*
  %4 = load i32, i32* %number_4, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %3, i32 %4)
  store i32 0, i32* %.preg_I4_4_49, align 8
  %5 = load i32, i32* %number_4, align 4
  store i32 %5, i32* %.preg_I4_4_52, align 8
  %6 = load i32, i32* %.preg_I4_4_52
  %cmp1 = icmp sgt i32 %6, 0
  br i1 %cmp1, label %tb, label %L2818

tb:                                               ; preds = %entry
  store i32 1, i32* %.preg_I4_4_50, align 8
  br label %L4610

L2818:                                            ; preds = %L3842, %entry
  ret i32 0

L4610:                                            ; preds = %L4354, %tb
  br label %L5122

L5378:                                            ; No predecessors!
  br label %L4866

L4866:                                            ; preds = %L5378
  %7 = load i32, i32* %.preg_I4_4_49
  %add.2 = add i32 %7, 1
  store i32 %add.2, i32* %.preg_I4_4_51, align 8
  %8 = load i32, i32* %.preg_I4_4_51
  store i32 %8, i32* %.preg_I4_4_49, align 8
  %9 = load i32, i32* %.preg_I4_4_50
  %add.3 = add i32 %9, 1
  store i32 %add.3, i32* %.preg_I4_4_50, align 8
  %10 = load i32, i32* %.preg_I4_4_51
  %11 = load i32, i32* %.preg_I4_4_52
  %cmp3 = icmp slt i32 %10, %11
  br i1 %cmp3, label %L5122, label %fb4

L4354:                                            ; preds = %tb5
  %12 = load i32, i32* %.preg_I4_4_49
  %add = add i32 %12, 1
  store i32 %add, i32* %.preg_I4_4_51, align 8
  %13 = load i32, i32* %.preg_I4_4_51
  store i32 %13, i32* %.preg_I4_4_49, align 8
  %14 = load i32, i32* %.preg_I4_4_50
  %add.1 = add i32 %14, 1
  store i32 %add.1, i32* %.preg_I4_4_50, align 8
  %15 = load i32, i32* %number_4, align 4
  store i32 %15, i32* %.preg_I4_4_52, align 8
  %16 = load i32, i32* %.preg_I4_4_52
  %17 = load i32, i32* %.preg_I4_4_51
  %cmp2 = icmp sgt i32 %16, %17
  br i1 %cmp2, label %L4610, label %fb

fb:                                               ; preds = %L4354
  br label %L3842

L3842:                                            ; preds = %fb4, %fb
  br label %L2818

L5122:                                            ; preds = %L4866, %L4610
  %18 = load i32, i32* %.preg_I4_4_52
  %19 = load i32, i32* %.preg_I4_4_50
  %srem = srem i32 %18, %19
  %cmp4 = icmp eq i32 %srem, 0
  br i1 %cmp4, label %tb5, label %L53786

fb4:                                              ; preds = %L4866
  br label %L3842

tb5:                                              ; preds = %L5122
  %20 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.3, i32 0, i32 0) to i8*
  %21 = load i32, i32* %.preg_I4_4_50
  %call4 = call i32 (i8*, ...) @printf(i8* %20, i32 %21)
  br label %L4354

L53786:                                           ; preds = %L5122
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
