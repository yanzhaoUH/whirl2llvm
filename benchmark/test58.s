	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test58.c (test58.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test58.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 1016, %rsp
	# s = 0
	# i = 1000
	.loc	1	3	0
 #   1  #include <stdio.h>
 #   2  #include <string.h>
 #   3  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-1016,%rsp              	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_2050:
	xorl %eax,%eax                	# [0] 
	.loc	1	7	0
 #   4  {
 #   5      char s[1000], i;
 #   6  
 #   7      printf("Enter a string: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	8	0
 #   8      scanf("%s", s);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	movsbl 0(%rsp),%eax           	# [0] s
	cmpl $0,%eax                  	# [3] 
	je .Lt_0_1794                 	# [4] 
.LBB5_main:
	xorl %esi,%esi                	# [0] 
.LBB16_main:
 #<loop> Loop body line 11
 #<loop> unrolled 3 times
	.loc	1	11	0
 #   9      //strcpy(s, "helloworld");
 #  10  
 #  11      for(i = 0; s[i] != '\0'; ++i);
	addl $1,%esi                  	# [0] 
	movsbq %sil,%rax              	# [1] 
	movsbl 0(%rsp,%rax,1), %eax   	# [2] s
	cmpl $0,%eax                  	# [5] 
	movsbl %sil,%esi              	# [6] 
	je .Lt_0_770                  	# [6] 
.LBB17_main:
 #<loop> Part of loop body line 11, head labeled .LBB16_main
 #<loop> unrolled 3 times
	incl %esi                     	# [0] 
	movsbq %sil,%rax              	# [1] 
	movsbl 0(%rsp,%rax,1), %eax   	# [2] s
	cmpl $0,%eax                  	# [5] 
	movsbl %sil,%esi              	# [6] 
	je .Lt_0_770                  	# [6] 
.Lt_0_1282:
 #<loop> Part of loop body line 11, head labeled .LBB16_main
	incl %esi                     	# [0] 
	movsbq %sil,%rax              	# [1] 
	movsbl 0(%rsp,%rax,1), %eax   	# [2] s
	cmpl $0,%eax                  	# [5] 
	movsbl %sil,%esi              	# [6] 
	jne .LBB16_main               	# [6] 
.Lt_0_770:
	xorl %eax,%eax                	# [0] 
	.loc	1	13	0
 #  12  
 #  13      printf("Length of string: %d", i);
	movl %esi,%esi                	# [1] 
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call printf                   	# [1] printf
.LBB10_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	14	0
 #  14      return 0;
	addq $1016,%rsp               	# [0] 
	ret                           	# [0] 
.Lt_0_1794:
	xorl %esi,%esi                	# [0] 
	jmp .Lt_0_770                 	# [0] 
.L_0_2306:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter a string: "
	.org 0x20
	.align	0
	# offset 32
	.string "%s"
	.org 0x30
	.align	0
	# offset 48
	.string "Length of string: %d"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_2050-main
	.uleb128	.L_0_2306-.L_0_2050
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x80, 0x08
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test58.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

