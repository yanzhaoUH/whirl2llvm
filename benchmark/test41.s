	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test41.c (test41.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test41.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# n = 4
	# c = 0
	.loc	1	7	0
 #   3  #include <stdio.h>
 #   4  #include <math.h>
 #   5  int decimal_octal(int n);
 #   6  int octal_decimal(int n);
 #   7  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_6914:
	xorl %eax,%eax                	# [0] 
	.loc	1	11	0
 #   8  {
 #   9     int n;
 #  10     char c;
 #  11     printf("Instructions:\n"); 
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	12	0
 #  12     printf("1. Enter alphabet 'o' to convert decimal to octal.\n");
	movq $(.rodata+16),%rdi       	# [0] .rodata+16
	call printf                   	# [0] printf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	13	0
 #  13     printf("2. Enter alphabet 'd' to convert octal to decimal.\n");
	movq $(.rodata+80),%rdi       	# [0] .rodata+80
	call printf                   	# [0] printf
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	14	0
 #  14     scanf("%c",&c);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+144),%rdi      	# [1] .rodata+144
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB6_main:
	.loc	1	17	0
 #  15     //c= 'd';
 #  16  
 #  17     if (c =='d' || c == 'D')
	movsbl 0(%rsp),%eax           	# [0] c
	cmpl $100,%eax                	# [3] 
	je .Lt_0_5634                 	# [4] 
.LBB7_main:
	cmpl $68,%eax                 	# [0] 
	je .Lt_0_5634                 	# [1] 
.L_0_2818:
	.loc	1	24	0
 #  20         scanf("%d", &n);
 #  21         //n = 10;
 #  22         printf("%d in octal = %d in decimal", n, octal_decimal(n));
 #  23     }
 #  24     if (c =='o' || c == 'O')
	cmpl $111,%eax                	# [0] 
	je .Lt_0_6146                 	# [1] 
.LBB16_main:
	cmpl $79,%eax                 	# [0] 
	je .Lt_0_6146                 	# [1] 
.L_0_3842:
	xorq %rax,%rax                	# [0] 
	.loc	1	31	0
 #  27         scanf("%d", &n);
 #  28         //n = 10;
 #  29         printf("%d in decimal = %d in octal", n, decimal_octal(n));
 #  30     }
 #  31     return 0;
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.Lt_0_5634:
	xorl %eax,%eax                	# [0] 
	.loc	1	19	0
	movq $(.rodata+160),%rdi      	# [0] .rodata+160
	call printf                   	# [0] printf
.LBB11_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	20	0
	leaq 4(%rsp),%rsi             	# [1] n
	movq $(.rodata+192),%rdi      	# [1] .rodata+192
	call scanf                    	# [1] scanf
.LBB12_main:
	.loc	1	22	0
	movl 4(%rsp),%edi             	# [0] n
	call _Z13octal_decimali       	# [0] _Z13octal_decimali
.LBB13_main:
	movl 4(%rsp),%esi             	# [0] n
	movq $(.rodata+208),%rdi      	# [0] .rodata+208
	movl %eax,%eax                	# [0] 
	movl %eax,%edx                	# [1] 
	xorl %eax,%eax                	# [1] 
	call printf                   	# [1] printf
.LBB14_main:
	movsbl 0(%rsp),%eax           	# [0] c
	jmp .L_0_2818                 	# [0] 
.Lt_0_6146:
	xorl %eax,%eax                	# [0] 
	.loc	1	26	0
	movq $(.rodata+240),%rdi      	# [0] .rodata+240
	call printf                   	# [0] printf
.LBB20_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	27	0
	leaq 4(%rsp),%rsi             	# [1] n
	movq $(.rodata+192),%rdi      	# [1] .rodata+192
	call scanf                    	# [1] scanf
.LBB21_main:
	.loc	1	29	0
	movl 4(%rsp),%edi             	# [0] n
	call _Z13decimal_octali       	# [0] _Z13decimal_octali
.LBB22_main:
	movl 4(%rsp),%esi             	# [0] n
	movq $(.rodata+272),%rdi      	# [0] .rodata+272
	movl %eax,%eax                	# [0] 
	movl %eax,%edx                	# [1] 
	xorl %eax,%eax                	# [1] 
	call printf                   	# [1] printf
.LBB26_main:
	jmp .L_0_3842                 	# [0] 
.L_0_7170:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.byte	0x49, 0x6e, 0x73, 0x74, 0x72, 0x75, 0x63, 0x74	# Instruct
	.byte	0x69, 0x6f, 0x6e, 0x73, 0x3a, 0xa, 0x0	# ions:\n\000
	.org 0x10
	.align	0
	# offset 16
	.byte	0x31, 0x2e, 0x20, 0x45, 0x6e, 0x74, 0x65, 0x72	# 1. Enter
	.byte	0x20, 0x61, 0x6c, 0x70, 0x68, 0x61, 0x62, 0x65	#  alphabe
	.byte	0x74, 0x20, 0x27, 0x6f, 0x27, 0x20, 0x74, 0x6f	# t 'o' to
	.byte	0x20, 0x63, 0x6f, 0x6e, 0x76, 0x65, 0x72, 0x74	#  convert
	.byte	0x20, 0x64, 0x65, 0x63, 0x69, 0x6d, 0x61, 0x6c	#  decimal
	.byte	0x20, 0x74, 0x6f, 0x20, 0x6f, 0x63, 0x74, 0x61	#  to octa
	.byte	0x6c, 0x2e, 0xa, 0x0	# l.\n\000
	.org 0x50
	.align	0
	# offset 80
	.byte	0x32, 0x2e, 0x20, 0x45, 0x6e, 0x74, 0x65, 0x72	# 2. Enter
	.byte	0x20, 0x61, 0x6c, 0x70, 0x68, 0x61, 0x62, 0x65	#  alphabe
	.byte	0x74, 0x20, 0x27, 0x64, 0x27, 0x20, 0x74, 0x6f	# t 'd' to
	.byte	0x20, 0x63, 0x6f, 0x6e, 0x76, 0x65, 0x72, 0x74	#  convert
	.byte	0x20, 0x6f, 0x63, 0x74, 0x61, 0x6c, 0x20, 0x74	#  octal t
	.byte	0x6f, 0x20, 0x64, 0x65, 0x63, 0x69, 0x6d, 0x61	# o decima
	.byte	0x6c, 0x2e, 0xa, 0x0	# l.\n\000
	.org 0x90
	.align	0
	# offset 144
	.string "%c"
	.org 0xa0
	.align	0
	# offset 160
	.string "Enter an octal number: "
	.org 0xc0
	.align	0
	# offset 192
	.string "%d"
	.org 0xd0
	.align	0
	# offset 208
	.string "%d in octal = %d in decimal"
	.org 0xf0
	.align	0
	# offset 240
	.string "Enter a decimal number: "
	.org 0x110
	.align	0
	# offset 272
	.string "%d in decimal = %d in octal"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_6914-main
	.uleb128	.L_0_7170-.L_0_6914
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.p2align 5,,

	# Program Unit: _Z13octal_decimali
.globl	_Z13octal_decimali
	.type	_Z13octal_decimali, @function
_Z13octal_decimali:	# 0xec
	# .frame	%rsp, 72, %rsp
	# decimal = 4
	# i = 0
	# _temp_gra_spill1 = 8
	# _temp_gra_spill2 = 16
	# _temp_gra_spill3 = 24
	# _temp_gra_spill4 = 32
	.loc	1	47	0
 #  43      }
 #  44      return octal;
 #  45  }
 #  46  
 #  47  int octal_decimal(int n) /* Function to convert octal to decimal */
.LBB1__Z13octal_decimali:
	addq $-72,%rsp                	# [0] 
	testl %edi,%edi               	# [0] 
	movq %r12,8(%rsp)             	# [1] _temp_gra_spill1
	movl %edi,%r12d               	# [1] 
	movq %r13,24(%rsp)            	# [1] _temp_gra_spill3
	movq %rbp,16(%rsp)            	# [2] _temp_gra_spill2
	movq %rbx,32(%rsp)            	# [2] _temp_gra_spill4
	je .Lt_1_1794                 	# [2] 
.LBB2__Z13octal_decimali:
	xorl %r13d,%r13d              	# [0] 
	xorl %ebp,%ebp                	# [0] 
.Lt_1_1282:
 #<loop> Loop body line 52
	.loc	1	52	0
 #  48  {
 #  49      int decimal=0, i=0, rem;
 #  50      while (n!=0)
 #  51      {
 #  52          rem = n%10;
	movslq %r12d,%rax             	# [0] 
	imulq $1717986919,%rax        	# [1] 
	shrq $32,%rax                 	# [6] 
	movl %r12d,%esi               	# [7] 
	movq %rax,%rax                	# [7] 
	sarl $31,%esi                 	# [8] 
	sarl $2,%eax                  	# [8] 
	subl %esi,%eax                	# [9] 
	movl %eax,%edi                	# [10] 
	imull $10,%edi                	# [11] 
	.loc	1	54	0
 #  53          n/=10;
 #  54          decimal += rem*pow(8,i);
	cvtsi2sd %ebp,%xmm1           	# [13] 
	movsd .rodata+304(%rip),%xmm0 	# [13] 
	.loc	1	52	0
	movl %r12d,%ebx               	# [13] 
	.loc	1	53	0
	movl %eax,%r12d               	# [14] 
	.loc	1	52	0
	subl %edi,%ebx                	# [14] 
	.loc	1	54	0
	.globl	pow
	call pow                      	# [14] pow
.LBB4__Z13octal_decimali:
 #<loop> Part of loop body line 52, head labeled .Lt_1_1282
	cvtsi2sd %ebx,%xmm2           	# [0] 
	mulsd %xmm2,%xmm0             	# [4] 
	cvtsi2sd %r13d,%xmm1          	# [5] 
	addsd %xmm1,%xmm0             	# [9] 
	.loc	1	55	0
 #  55          ++i;
	addl $1,%ebp                  	# [11] 
	testl %r12d,%r12d             	# [11] 
	.loc	1	54	0
	cvttsd2si %xmm0,%r13d         	# [12] 
	.loc	1	55	0
	jne .Lt_1_1282                	# [12] 
.Lt_1_770:
	.loc	1	57	0
 #  56      }
 #  57      return decimal;
	movq 32(%rsp),%rbx            	# [0] _temp_gra_spill4
	movq 16(%rsp),%rbp            	# [1] _temp_gra_spill2
	movq 8(%rsp),%r12             	# [1] _temp_gra_spill1
	movl %r13d,%eax               	# [1] 
	movq 24(%rsp),%r13            	# [2] _temp_gra_spill3
	addq $72,%rsp                 	# [2] 
	ret                           	# [2] 
.Lt_1_1794:
	movq 8(%rsp),%r12             	# [0] _temp_gra_spill1
	xorl %eax,%eax                	# [0] 
	movl %eax,%eax                	# [1] 
	addq $72,%rsp                 	# [1] 
	ret                           	# [1] 
.LDWend__Z13octal_decimali:
	.size _Z13octal_decimali, .LDWend__Z13octal_decimali-_Z13octal_decimali

	.section .rodata
	.org 0x130
	.align	0
	# offset 304
	.4byte	0
	.4byte	1075838976
	# double 8.00000
	.section .text
	.p2align 5,,

	# Program Unit: _Z13decimal_octali
.globl	_Z13decimal_octali
	.type	_Z13decimal_octali, @function
_Z13decimal_octali:	# 0x1a4
	# .frame	%rsp, 40, %rsp
	# i = 4
	# octal = 0
	.loc	1	34	0
.LBB1__Z13decimal_octali:
	addq $-40,%rsp                	# [0] 
	testl %edi,%edi               	# [0] 
	je .Lt_2_1794                 	# [1] 
.LBB2__Z13decimal_octali:
	movl $1,%edx                  	# [0] 
	xorl %r8d,%r8d                	# [0] 
.LBB10__Z13decimal_octali:
 #<loop> Loop body line 39
 #<loop> unrolled 3 times
	.loc	1	39	0
	movl %edi,%esi                	# [0] 
	sarl $31,%esi                 	# [1] 
	andl $7,%esi                  	# [2] 
	addl %esi,%edi                	# [3] 
	movl %edi,%eax                	# [4] 
	andl $7,%eax                  	# [5] 
	subl %esi,%eax                	# [6] 
	.loc	1	41	0
	imull %edx,%eax               	# [7] 
	.loc	1	40	0
	sarl $3,%edi                  	# [9] 
	imull $10,%edx                	# [10] 
	.loc	1	41	0
	addl %eax,%r8d                	# [10] 
	.loc	1	42	0
	testl %edi,%edi               	# [10] 
	je .Lt_2_770                  	# [11] 
.LBB11__Z13decimal_octali:
 #<loop> Part of loop body line 39, head labeled .LBB10__Z13decimal_octali
 #<loop> unrolled 3 times
	.loc	1	39	0
	movl %edi,%esi                	# [0] 
	sarl $31,%esi                 	# [1] 
	andl $7,%esi                  	# [2] 
	addl %esi,%edi                	# [3] 
	movl %edi,%eax                	# [4] 
	andl $7,%eax                  	# [5] 
	subl %esi,%eax                	# [6] 
	.loc	1	41	0
	imull %edx,%eax               	# [7] 
	.loc	1	40	0
	sarl $3,%edi                  	# [9] 
	imull $10,%edx                	# [10] 
	.loc	1	41	0
	addl %eax,%r8d                	# [10] 
	.loc	1	42	0
	testl %edi,%edi               	# [10] 
	je .Lt_2_770                  	# [11] 
.Lt_2_1282:
 #<loop> Part of loop body line 39, head labeled .LBB10__Z13decimal_octali
	.loc	1	39	0
	movl %edi,%esi                	# [0] 
	sarl $31,%esi                 	# [1] 
	andl $7,%esi                  	# [2] 
	addl %esi,%edi                	# [3] 
	movl %edi,%eax                	# [4] 
	andl $7,%eax                  	# [5] 
	subl %esi,%eax                	# [6] 
	.loc	1	41	0
	imull %edx,%eax               	# [7] 
	.loc	1	40	0
	sarl $3,%edi                  	# [9] 
	imull $10,%edx                	# [10] 
	.loc	1	41	0
	addl %eax,%r8d                	# [10] 
	.loc	1	42	0
	testl %edi,%edi               	# [10] 
	jne .LBB10__Z13decimal_octali 	# [11] 
.Lt_2_770:
	.loc	1	44	0
	movl %r8d,%eax                	# [0] 
	addq $40,%rsp                 	# [0] 
	ret                           	# [0] 
.Lt_2_1794:
	xorl %eax,%eax                	# [0] 
	movl %eax,%eax                	# [1] 
	addq $40,%rsp                 	# [1] 
	ret                           	# [1] 
.LDWend__Z13decimal_octali:
	.size _Z13decimal_octali, .LDWend__Z13decimal_octali-_Z13decimal_octali
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test41.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

