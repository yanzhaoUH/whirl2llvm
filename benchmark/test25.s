	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test25.c (test25.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test25.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# base = 0
	# exponent = 4
	# result = 8
	.loc	1	2	0
 #   1  #include <stdio.h>
 #   2  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_2050:
	xorl %eax,%eax                	# [0] 
	.loc	1	8	0
 #   4      int base, exponent;
 #   5  
 #   6      long long result = 1;
 #   7  
 #   8      printf("Enter a base number: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	9	0
 #   9      scanf("%d", &base);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	11	0
 #  10      //base = 3;
 #  11      printf("Enter an exponent: 4");
	movq $(.rodata+48),%rdi       	# [0] .rodata+48
	call printf                   	# [0] printf
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	12	0
 #  12      scanf("%d", &exponent);
	leaq 4(%rsp),%rsi             	# [1] exponent
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	call scanf                    	# [1] scanf
.LBB6_main:
	movl 4(%rsp),%eax             	# [0] exponent
	testl %eax,%eax               	# [3] 
	je .Lt_0_1794                 	# [4] 
.LBB7_main:
	movl %eax,%ecx                	# [0] 
	movq $1,%rsi                  	# [1] 
	andl $3,%ecx                  	# [1] 
	testl $3,%eax                 	# [1] 
	movslq 0(%rsp),%rdx           	# [2] base
	movl %eax,%r8d                	# [2] 
	je .LBB19_main                	# [2] 
.LBB20_main:
	decl %ecx                     	# [0] 
	decl %eax                     	# [1] 
	.loc	1	16	0
 #  13      //exponent = 4; 
 #  14      while (exponent != 0)
 #  15      {
 #  16          result *= base;
	imulq %rdx,%rsi               	# [1] 
	testl %ecx,%ecx               	# [1] 
	je .LBB19_main                	# [2] 
.LBB21_main:
	movl %ecx,%edi                	# [0] 
	decl %edi                     	# [1] 
	decl %eax                     	# [2] 
	imulq %rdx,%rsi               	# [2] 
	testl %edi,%edi               	# [2] 
	je .LBB19_main                	# [3] 
.LBB22_main:
	decl %eax                     	# [0] 
	imulq %rdx,%rsi               	# [0] 
.LBB19_main:
	movl %r8d,%ecx                	# [0] 
	sarl $2,%ecx                  	# [1] 
	testl %ecx,%ecx               	# [2] 
	je .LBB17_main                	# [3] 
.LBB24_main:
.LBB18_main:
 #<loop> Loop body line 12, nesting depth: 1, estimated iterations: 25
 #<loop> unrolled 4 times
	movq %rdx,%rdi                	# [0] 
	imulq %rsi,%rdi               	# [1] 
	movq %rdi,%rsi                	# [6] 
	imulq %rdx,%rsi               	# [7] 
	imulq %rdx,%rsi               	# [12] 
	.loc	1	17	0
 #  17          --exponent;
	addl $-4,%eax                 	# [16] 
	.loc	1	16	0
	imulq %rdx,%rsi               	# [17] 
	.loc	1	17	0
	testl %eax,%eax               	# [17] 
	jne .LBB18_main               	# [18] 
.LBB17_main:
	movl %eax,4(%rsp)             	# [0] exponent
.Lt_0_770:
	xorl %eax,%eax                	# [0] 
	.loc	1	20	0
 #  18      }
 #  19  
 #  20      printf("Answer = %lld", result);
	movq $(.rodata+80),%rdi       	# [0] .rodata+80
	call printf                   	# [0] printf
.LBB13_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	22	0
 #  21  
 #  22      return 0;
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.Lt_0_1794:
	.loc	1	17	0
	movq $1,%rsi                  	# [0] 
	jmp .Lt_0_770                 	# [0] 
.L_0_2306:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter a base number: "
	.org 0x20
	.align	0
	# offset 32
	.string "%d"
	.org 0x30
	.align	0
	# offset 48
	.string "Enter an exponent: 4"
	.org 0x50
	.align	0
	# offset 80
	.string "Answer = %lld"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_2050-main
	.uleb128	.L_0_2306-.L_0_2050
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test25.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

