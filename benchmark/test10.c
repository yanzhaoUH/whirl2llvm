#include <stdio.h>
int main()
{
    char c;
    int isLowercaseVowel, isUppercaseVowel;

    printf("Enter an alphabet: ");
    scanf("%c",&c);
    
    //c='a';
    // evaluates to 1 (true) if c is a lowercase vowel
    isLowercaseVowel = (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u');
    // isLowercaseVowel = (c == 'a' || c == 'e' );

    // evaluates to 1 (true) if c is an uppercase vowel
    isUppercaseVowel = (c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U');

    // evaluates to 1 (true) if either isLowercaseVowel or isUppercaseVowel is true


    if (isLowercaseVowel || isUppercaseVowel)
    //if (isLowercaseVowel )
        printf("%c is a vowel.",c);
    else
        printf("%c is a consonant.",c); 
    return 0;
}
