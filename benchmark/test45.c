#include <stdio.h>
int main(){
    int n, i;
    float num[100], sum=0.0, average;
    printf("Enter the numbers of data: ");
    scanf("%d",&n);
    //n = 101;
    while (n>100 || n<=0)
    {
        printf("Error! number should in range of (1 to 100).\n");
        printf("Enter the number again: ");
        scanf("%d",&n);
        //n = 10;
    }
   for(i=0; i<n; ++i)
   {
      printf("%d. Enter number: ",i+1);
      //scanf("%f",&num[i]);
      num[i] = i;
      sum+=num[i];
   }

   average=sum/n;
   printf("Average = %.2f",average);
   return 0;
}
