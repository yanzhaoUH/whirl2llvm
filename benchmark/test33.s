	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test33.c (test33.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test33.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 40, %rsp
	# op = 0
	# firstNumber = 8
	# secondNumber = 16
	.loc	1	3	0
 #   1  # include <stdio.h>
 #   2  
 #   3  int main() {
.LBB1_main:
.LEH_adjustsp_main:
	addq $-40,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_1794:
	xorl %eax,%eax                	# [0] 
	.loc	1	7	0
 #   4      char op;
 #   5      double firstNumber,secondNumber;
 #   6  
 #   7      printf("Enter an operator (+, -, *,): ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	8	0
 #   8      scanf("%c", &op);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	11	0
 #   9      //op = '*';
 #  10  
 #  11      printf("Enter two operands: ");
	movq $(.rodata+48),%rdi       	# [0] .rodata+48
	call printf                   	# [0] printf
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	12	0
 #  12      scanf("%lf %lf",&firstNumber, &secondNumber);
	leaq 16(%rsp),%rdx            	# [0] secondNumber
	leaq 8(%rsp),%rsi             	# [1] firstNumber
	movq $(.rodata+80),%rdi       	# [1] .rodata+80
	call scanf                    	# [1] scanf
.LBB6_main:
	.loc	1	36	0
 #  32              break;
 #  33  
 #  34          // operator doesn't match any case constant (+, -, *, /)
 #  35          default:
 #  36              printf("Error! operator is not correct");
	movsbl 0(%rsp),%eax           	# [0] op
	cmpl $43,%eax                 	# [3] 
	je .Lt_0_258                  	# [4] 
.LBB7_main:
	cmpl $45,%eax                 	# [0] 
	je .Lt_0_770                  	# [1] 
.LBB8_main:
	cmpl $42,%eax                 	# [0] 
	je .Lt_0_1026                 	# [1] 
.LBB9_main:
	cmpl $47,%eax                 	# [0] 
	je .Lt_0_1282                 	# [1] 
.Lt_0_1538:
	xorl %eax,%eax                	# [0] 
	movq $(.rodata+224),%rdi      	# [0] .rodata+224
	call printf                   	# [0] printf
.LBB31_main:
.Lt_0_514:
	xorq %rax,%rax                	# [0] 
	.loc	1	39	0
 #  37      }
 #  38      
 #  39      return 0;
	addq $40,%rsp                 	# [0] 
	ret                           	# [0] 
.Lt_0_1282:
	.loc	1	31	0
	movsd 8(%rsp),%xmm2           	# [0] firstNumber
	movl $3,%eax                  	# [2] 
	movsd 16(%rsp),%xmm1          	# [2] secondNumber
	movq $(.rodata+192),%rdi      	# [2] .rodata+192
	movaps %xmm2,%xmm0            	# [3] 
	divsd %xmm2,%xmm2             	# [3] 
	call printf                   	# [3] printf
.LBB24_main:
	jmp .Lt_0_514                 	# [0] 
.Lt_0_258:
	.loc	1	19	0
	movsd 8(%rsp),%xmm2           	# [0] firstNumber
	movsd 16(%rsp),%xmm3          	# [1] secondNumber
	movaps %xmm2,%xmm0            	# [3] 
	movl $3,%eax                  	# [4] 
	addsd %xmm3,%xmm2             	# [4] 
	movq $(.rodata+96),%rdi       	# [5] .rodata+96
	movaps %xmm3,%xmm1            	# [5] 
	call printf                   	# [5] printf
.LBB25_main:
	jmp .Lt_0_514                 	# [0] 
.Lt_0_770:
	.loc	1	23	0
	movsd 8(%rsp),%xmm2           	# [0] firstNumber
	movsd 16(%rsp),%xmm3          	# [1] secondNumber
	movaps %xmm2,%xmm0            	# [3] 
	movl $3,%eax                  	# [4] 
	subsd %xmm3,%xmm2             	# [4] 
	movq $(.rodata+128),%rdi      	# [5] .rodata+128
	movaps %xmm3,%xmm1            	# [5] 
	call printf                   	# [5] printf
.LBB26_main:
	jmp .Lt_0_514                 	# [0] 
.Lt_0_1026:
	.loc	1	27	0
	movsd 8(%rsp),%xmm2           	# [0] firstNumber
	movsd 16(%rsp),%xmm3          	# [1] secondNumber
	movl $3,%eax                  	# [3] 
	movq $(.rodata+160),%rdi      	# [3] .rodata+160
	movaps %xmm2,%xmm0            	# [3] 
	mulsd %xmm3,%xmm2             	# [4] 
	movaps %xmm3,%xmm1            	# [4] 
	call printf                   	# [4] printf
.LBB27_main:
	jmp .Lt_0_514                 	# [0] 
.L_0_2050:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter an operator (+, -, *,): "
	.org 0x20
	.align	0
	# offset 32
	.string "%c"
	.org 0x30
	.align	0
	# offset 48
	.string "Enter two operands: "
	.org 0x50
	.align	0
	# offset 80
	.string "%lf %lf"
	.org 0x60
	.align	0
	# offset 96
	.string "%.1lf + %.1lf = %.1lf"
	.org 0x80
	.align	0
	# offset 128
	.string "%.1lf - %.1lf = %.1lf"
	.org 0xa0
	.align	0
	# offset 160
	.string "%.1lf * %.1lf = %.1lf"
	.org 0xc0
	.align	0
	# offset 192
	.string "%.1lf / %.1lf = %.1lf"
	.org 0xe0
	.align	0
	# offset 224
	.string "Error! operator is not correct"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_1794-main
	.uleb128	.L_0_2050-.L_0_1794
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x30
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test33.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

