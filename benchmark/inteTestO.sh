#!/bin/bash
testFiles=( "test-opt" "test-vect")

#1 produce whirl IR file
for i in "${testFiles[@]}"
do
	echo $i
	./createWhirl.sh $i
done

#2 call whirl2llvm application to produce .bc file
for i in "${testFiles[@]}"
do
   BfileName="$i.O"
	../whirl2llvm $BfileName
done


#3 call lli command to test .bc is right
for i in "${testFiles[@]}"
do
	printf "$i result is \n"
	BCfileName="$i.bc"
	lli $BCfileName
	printf "\n"
	printf "\n"
done



