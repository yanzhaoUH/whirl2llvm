	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test56.c (test56.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test56.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 200, %rsp
	# line = 0
	# vowels = 152
	# consonants = 156
	# digits = 160
	# spaces = 164
	# _temp_gra_spill1 = 168
	# _temp_gra_spill2 = 176
	# _temp_gra_spill3 = 184
	.loc	1	3	0
 #   1  #include <stdio.h>
 #   2  #include <string.h>
 #   3  int main()
.LBB1_main:
.LEH_adjustsp_main:
.LEH_csr_main:
	addq $-200,%rsp               	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
	movq %rbx,184(%rsp)           	# [0] _temp_gra_spill3
	movq %rbp,176(%rsp)           	# [0] _temp_gra_spill2
	movq %r12,168(%rsp)           	# [0] _temp_gra_spill1
.L_0_29698:
	xorl %eax,%eax                	# [0] 
	.loc	1	10	0
 #   6      int i, vowels, consonants, digits, spaces;
 #   7  
 #   8      vowels =  consonants = digits = spaces = 0;
 #   9  
 #  10      printf("Enter a line of string: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	11	0
 #  11      scanf("%[^\n]", line);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	movsbl 0(%rsp),%eax           	# [0] line
	cmpl $0,%eax                  	# [3] 
	je .Lt_0_28930                	# [4] 
.LBB5_main:
	xorl %r12d,%r12d              	# [0] 
	xorl %ebp,%ebp                	# [0] 
	xorl %ebx,%ebx                	# [1] 
	xorl %esi,%esi                	# [1] 
	movq %rsp,%rdx                	# [1] 
.Lt_0_19714:
 #<loop> Loop body line 16
	.loc	1	16	0
 #  12      //strcpy(line, "hello world");
 #  13  
 #  14      for(i=0; line[i]!='\0'; ++i)
 #  15      {
 #  16          if(line[i]=='a' || line[i]=='e' || line[i]=='i' ||
	cmpl $97,%eax                 	# [0] 
	je .Lt_0_27906                	# [1] 
.LBB7_main:
 #<loop> Part of loop body line 16, head labeled .Lt_0_19714
	cmpl $101,%eax                	# [0] 
	je .Lt_0_27906                	# [1] 
.Lt_0_22018:
 #<loop> Part of loop body line 16, head labeled .Lt_0_19714
	cmpl $105,%eax                	# [0] 
	je .Lt_0_27906                	# [1] 
.Lt_0_22786:
 #<loop> Part of loop body line 16, head labeled .Lt_0_19714
	cmpl $111,%eax                	# [0] 
	je .Lt_0_27906                	# [1] 
.Lt_0_23554:
 #<loop> Part of loop body line 16, head labeled .Lt_0_19714
	cmpl $117,%eax                	# [0] 
	je .Lt_0_27906                	# [1] 
.Lt_0_24322:
 #<loop> Part of loop body line 16, head labeled .Lt_0_19714
	cmpl $65,%eax                 	# [0] 
	je .Lt_0_27906                	# [1] 
.Lt_0_25090:
 #<loop> Part of loop body line 16, head labeled .Lt_0_19714
	cmpl $69,%eax                 	# [0] 
	je .Lt_0_27906                	# [1] 
.Lt_0_25858:
 #<loop> Part of loop body line 16, head labeled .Lt_0_19714
	cmpl $73,%eax                 	# [0] 
	je .Lt_0_27906                	# [1] 
.Lt_0_26626:
 #<loop> Part of loop body line 16, head labeled .Lt_0_19714
	cmpl $79,%eax                 	# [0] 
	je .Lt_0_27906                	# [1] 
.Lt_0_27394:
 #<loop> Part of loop body line 16, head labeled .Lt_0_19714
	cmpl $85,%eax                 	# [0] 
	je .Lt_0_27906                	# [1] 
.L_0_10754:
 #<loop> Part of loop body line 16, head labeled .Lt_0_19714
	.loc	1	23	0
 #  19             line[i]=='U')
 #  20          {
 #  21              ++vowels;
 #  22          }
 #  23          else if((line[i]>='a'&& line[i]<='z') || (line[i]>='A'&& line[i]<='Z'))
	leal -97(%rax),%edi           	# [0] 
	cmpb $25,%dil                 	# [2] 
	jbe .Lt_0_28674               	# [3] 
.LBB28_main:
 #<loop> Part of loop body line 16, head labeled .Lt_0_19714
	leal -65(%rax),%edi           	# [0] 
	cmpb $25,%dil                 	# [2] 
	jbe .Lt_0_28674               	# [3] 
.L_0_18178:
 #<loop> Part of loop body line 16, head labeled .Lt_0_19714
	.loc	1	27	0
 #  24          {
 #  25              ++consonants;
 #  26          }
 #  27          else if(line[i]>='0' && line[i]<='9')
	leal -48(%rax),%edi           	# [0] 
	cmpb $9,%dil                  	# [2] 
	jbe .LBB33_main               	# [3] 
.Lt_0_19970:
 #<loop> Part of loop body line 16, head labeled .Lt_0_19714
	.loc	1	31	0
 #  28          {
 #  29              ++digits;
 #  30          }
 #  31          else if (line[i]==' ')
	cmpl $32,%eax                 	# [0] 
	je .LBB35_main                	# [1] 
.Lt_0_20994:
.Lt_0_20738:
.L_0_17922:
.L_0_10498:
 #<loop> Part of loop body line 16, head labeled .Lt_0_19714
	.loc	1	33	0
 #  32          {
 #  33              ++spaces;
	movsbl 1(%rdx),%eax           	# [0] id:61 line+0x0
	addq $1,%rdx                  	# [3] 
	testl %eax,%eax               	# [3] 
	jne .Lt_0_19714               	# [4] 
.Lt_0_19202:
	xorl %eax,%eax                	# [0] 
	.loc	1	37	0
 #  34          }
 #  35      }
 #  36  
 #  37      printf("Vowels: %d",vowels);
	movl %esi,%esi                	# [1] 
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call printf                   	# [1] printf
.LBB40_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	38	0
 #  38      printf("\nConsonants: %d",consonants);
	movl %ebx,%esi                	# [1] 
	movq $(.rodata+64),%rdi       	# [1] .rodata+64
	call printf                   	# [1] printf
.LBB41_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	39	0
 #  39      printf("\nDigits: %d",digits);
	movl %ebp,%esi                	# [1] 
	movq $(.rodata+80),%rdi       	# [1] .rodata+80
	call printf                   	# [1] printf
.LBB42_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	40	0
 #  40      printf("\nWhite spaces: %d", spaces);
	movl %r12d,%esi               	# [1] 
	movq $(.rodata+96),%rdi       	# [1] .rodata+96
	call printf                   	# [1] printf
.LBB43_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	42	0
 #  41  
 #  42      return 0;
	movq 184(%rsp),%rbx           	# [0] _temp_gra_spill3
	movq 176(%rsp),%rbp           	# [0] _temp_gra_spill2
	movq 168(%rsp),%r12           	# [1] _temp_gra_spill1
	addq $200,%rsp                	# [1] 
	ret                           	# [1] 
.Lt_0_27906:
 #<loop> Part of loop body line 16, head labeled .Lt_0_19714
	.loc	1	21	0
	addl $1,%esi                  	# [0] 
	jmp .Lt_0_20994               	# [0] 
.Lt_0_28674:
 #<loop> Part of loop body line 16, head labeled .Lt_0_19714
	.loc	1	25	0
	addl $1,%ebx                  	# [0] 
	jmp .Lt_0_20994               	# [0] 
.LBB33_main:
 #<loop> Part of loop body line 16, head labeled .Lt_0_19714
	.loc	1	29	0
	addl $1,%ebp                  	# [0] 
	jmp .Lt_0_20994               	# [0] 
.LBB35_main:
 #<loop> Part of loop body line 16, head labeled .Lt_0_19714
	.loc	1	33	0
	addl $1,%r12d                 	# [0] 
	jmp .Lt_0_20994               	# [0] 
.Lt_0_28930:
	xorl %r12d,%r12d              	# [0] 
	xorl %ebp,%ebp                	# [0] 
	xorl %ebx,%ebx                	# [1] 
	xorl %esi,%esi                	# [1] 
	jmp .Lt_0_19202               	# [1] 
.L_0_29954:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter a line of string: "
	.org 0x20
	.align	0
	# offset 32
	.byte	0x25, 0x5b, 0x5e, 0xa, 0x5d, 0x0	# %[^\n]\000
	.org 0x30
	.align	0
	# offset 48
	.string "Vowels: %d"
	.org 0x40
	.align	0
	# offset 64
	.byte	0xa, 0x43, 0x6f, 0x6e, 0x73, 0x6f, 0x6e, 0x61	# \nConsona
	.byte	0x6e, 0x74, 0x73, 0x3a, 0x20, 0x25, 0x64, 0x0	# nts: %d\000
	# 
	.org 0x50
	.align	0
	# offset 80
	.byte	0xa, 0x44, 0x69, 0x67, 0x69, 0x74, 0x73, 0x3a	# \nDigits:
	.byte	0x20, 0x25, 0x64, 0x0	#  %d\000
	.org 0x60
	.align	0
	# offset 96
	.byte	0xa, 0x57, 0x68, 0x69, 0x74, 0x65, 0x20, 0x73	# \nWhite s
	.byte	0x70, 0x61, 0x63, 0x65, 0x73, 0x3a, 0x20, 0x25	# paces: %
	.byte	0x64, 0x0	# d\000

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_29698-main
	.uleb128	.L_0_29954-.L_0_29698
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0xd0, 0x01, 0x04
	.4byte	.LEH_csr_main - .LEH_adjustsp_main
	.byte	0x8c, 0x05, 0x86, 0x04, 0x83, 0x03
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test56.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

