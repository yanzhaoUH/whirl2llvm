	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test55.c (test55.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test55.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 1016, %rsp
	# str = 0
	# ch = 1000
	# frequency = 1004
	.loc	1	4	0
 #   1  #include <stdio.h>
 #   2  #include <string.h>
 #   3  
 #   4  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-1016,%rsp              	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_3074:
	xorl %eax,%eax                	# [0] 
	.loc	1	9	0
 #   5  {
 #   6     char str[1000], ch;
 #   7     int i, frequency = 0;
 #   8  
 #   9     printf("Enter a string: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	.loc	1	10	0
 #  10     gets(str);
	movq %rsp,%rdi                	# [0] 
	.globl	gets
	call gets                     	# [0] gets
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	13	0
 #  11    //strcpy(str, "hello world");
 #  12  
 #  13     printf("Enter a character to find the frequency: ");
	movq $(.rodata+32),%rdi       	# [0] .rodata+32
	call printf                   	# [0] printf
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	14	0
 #  14     scanf(" %c",&ch);
	leaq 1000(%rsp),%rsi          	# [1] ch
	movq $(.rodata+80),%rdi       	# [1] .rodata+80
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB6_main:
	movsbl 0(%rsp),%eax           	# [0] str
	cmpl $0,%eax                  	# [3] 
	je .Lt_0_2818                 	# [4] 
.LBB7_main:
	xorl %r8d,%r8d                	# [0] 
	movsbl 1000(%rsp),%esi        	# [0] ch
	movq %rsp,%rcx                	# [0] 
.Lt_0_1794:
 #<loop> Loop body line 19
	.loc	1	19	0
 #  15     //ch = 'l';
 #  16  
 #  17     for(i = 0; str[i] != '\0'; ++i)
 #  18     {
 #  19         if(ch == str[i])
	cmpl %esi,%eax                	# [0] 
	jne .Lt_0_2562                	# [1] 
.LBB9_main:
 #<loop> Part of loop body line 19, head labeled .Lt_0_1794
	.loc	1	20	0
 #  20             ++frequency;
	addl $1,%r8d                  	# [0] 
.Lt_0_2562:
 #<loop> Part of loop body line 19, head labeled .Lt_0_1794
	movsbl 1(%rcx),%eax           	# [0] id:20 str+0x0
	addq $1,%rcx                  	# [3] 
	testl %eax,%eax               	# [3] 
	jne .Lt_0_1794                	# [4] 
.Lt_0_1282:
	xorl %eax,%eax                	# [0] 
	.loc	1	23	0
 #  21     }
 #  22  
 #  23     printf("Frequency of %c = %d", ch, frequency);
	movl %r8d,%edx                	# [0] 
	movl %esi,%esi                	# [1] 
	movq $(.rodata+96),%rdi       	# [1] .rodata+96
	call printf                   	# [1] printf
.LBB14_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	25	0
 #  24  
 #  25     return 0;
	addq $1016,%rsp               	# [0] 
	ret                           	# [0] 
.Lt_0_2818:
	xorl %r8d,%r8d                	# [0] 
	.loc	1	20	0
	movsbl 1000(%rsp),%esi        	# [0] ch
	jmp .Lt_0_1282                	# [0] 
.L_0_3330:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter a string: "
	.org 0x20
	.align	0
	# offset 32
	.string "Enter a character to find the frequency: "
	.org 0x50
	.align	0
	# offset 80
	.string " %c"
	.org 0x60
	.align	0
	# offset 96
	.string "Frequency of %c = %d"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_3074-main
	.uleb128	.L_0_3330-.L_0_3074
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x80, 0x08
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test55.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

