	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test40.c (test40.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test40.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# n = 0
	.loc	1	5	0
 #   1  #include <stdio.h>
 #   2  #include <math.h>
 #   3  int ConvertBinaryToDecimal(long long n);
 #   4  
 #   5  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_258:
	xorl %eax,%eax                	# [0] 
	.loc	1	8	0
 #   6  {
 #   7      long long n;
 #   8      printf("Enter a binary number: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	9	0
 #   9      scanf("%lld", &n);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	.loc	1	11	0
 #  10      //n = 1111;
 #  11      printf("%lld in binary = %d in decimal", n, ConvertBinaryToDecimal(n));
	movq 0(%rsp),%rdi             	# [0] n
	call _Z22ConvertBinaryToDecimalx	# [0] _Z22ConvertBinaryToDecimalx
.LBB5_main:
	movq 0(%rsp),%rsi             	# [0] n
	movq $(.rodata+48),%rdi       	# [0] .rodata+48
	movl %eax,%eax                	# [0] 
	movl %eax,%edx                	# [1] 
	xorl %eax,%eax                	# [1] 
	call printf                   	# [1] printf
.LBB6_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	12	0
 #  12      return 0;
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.L_0_514:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter a binary number: "
	.org 0x20
	.align	0
	# offset 32
	.string "%lld"
	.org 0x30
	.align	0
	# offset 48
	.string "%lld in binary = %d in decimal"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_258-main
	.uleb128	.L_0_514-.L_0_258
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.p2align 5,,

	# Program Unit: _Z22ConvertBinaryToDecimalx
.globl	_Z22ConvertBinaryToDecimalx
	.type	_Z22ConvertBinaryToDecimalx, @function
_Z22ConvertBinaryToDecimalx:	# 0x4c
	# .frame	%rsp, 72, %rsp
	# decimalNumber = 4
	# i = 0
	# _temp_gra_spill1 = 8
	# _temp_gra_spill2 = 16
	# _temp_gra_spill3 = 24
	# _temp_gra_spill4 = 32
	.loc	1	15	0
 #  13  }
 #  14  
 #  15  int ConvertBinaryToDecimal(long long n)
.LBB1__Z22ConvertBinaryToDecimalx:
	addq $-72,%rsp                	# [0] 
	testq %rdi,%rdi               	# [0] 
	movq %r12,8(%rsp)             	# [1] _temp_gra_spill1
	movq %rdi,%r12                	# [1] 
	movq %r13,24(%rsp)            	# [1] _temp_gra_spill3
	movq %rbp,16(%rsp)            	# [2] _temp_gra_spill2
	movq %rbx,32(%rsp)            	# [2] _temp_gra_spill4
	je .Lt_1_1794                 	# [2] 
.LBB2__Z22ConvertBinaryToDecimalx:
	xorl %r13d,%r13d              	# [0] 
	xorl %ebp,%ebp                	# [0] 
.Lt_1_1282:
 #<loop> Loop body line 20
	.loc	1	20	0
 #  16  {
 #  17      int decimalNumber = 0, i = 0, remainder;
 #  18      while (n!=0)
 #  19      {
 #  20          remainder = n%10;
	movq %r12,%rax                	# [0] 
	movq $-3689348814741910323,%rcx	# [1] 
	negq %rax                     	# [1] 
	cmovs %r12,%rax               	# [1] 
	mulq %rcx                     	# [2] 
	movq %rdx,%rsi                	# [7] 
	sarq $3,%rsi                  	# [8] 
	movq %rsi,%rax                	# [9] 
	negq %rax                     	# [10] 
	testq %r12,%r12               	# [10] 
	cmovge %rsi,%rax              	# [11] 
	leaq 0(%rax,%rax,4), %rdi     	# [12] 
	addq %rdi,%rdi                	# [14] 
	.loc	1	22	0
 #  21          n /= 10;
 #  22          decimalNumber += remainder*pow(2,i);
	cvtsi2sd %ebp,%xmm1           	# [15] 
	movsd .rodata+80(%rip),%xmm0  	# [15] 
	.loc	1	20	0
	subq %rdi,%r12                	# [15] 
	movl %r12d,%ebx               	# [16] 
	.loc	1	21	0
	movq %rax,%r12                	# [16] 
	.loc	1	22	0
	.globl	pow
	call pow                      	# [16] pow
.LBB4__Z22ConvertBinaryToDecimalx:
 #<loop> Part of loop body line 20, head labeled .Lt_1_1282
	cvtsi2sd %ebx,%xmm2           	# [0] 
	mulsd %xmm2,%xmm0             	# [4] 
	cvtsi2sd %r13d,%xmm1          	# [5] 
	addsd %xmm1,%xmm0             	# [9] 
	.loc	1	23	0
 #  23          ++i;
	addl $1,%ebp                  	# [11] 
	testq %r12,%r12               	# [11] 
	.loc	1	22	0
	cvttsd2si %xmm0,%r13d         	# [12] 
	.loc	1	23	0
	jne .Lt_1_1282                	# [12] 
.Lt_1_770:
	.loc	1	25	0
 #  24      }
 #  25      return decimalNumber;
	movq 32(%rsp),%rbx            	# [0] _temp_gra_spill4
	movq 16(%rsp),%rbp            	# [1] _temp_gra_spill2
	movq 8(%rsp),%r12             	# [1] _temp_gra_spill1
	movl %r13d,%eax               	# [1] 
	movq 24(%rsp),%r13            	# [2] _temp_gra_spill3
	addq $72,%rsp                 	# [2] 
	ret                           	# [2] 
.Lt_1_1794:
	movq 8(%rsp),%r12             	# [0] _temp_gra_spill1
	xorl %eax,%eax                	# [0] 
	movl %eax,%eax                	# [1] 
	addq $72,%rsp                 	# [1] 
	ret                           	# [1] 
.LDWend__Z22ConvertBinaryToDecimalx:
	.size _Z22ConvertBinaryToDecimalx, .LDWend__Z22ConvertBinaryToDecimalx-_Z22ConvertBinaryToDecimalx

	.section .rodata
	.org 0x50
	.align	0
	# offset 80
	.4byte	0
	.4byte	1073741824
	# double 2.00000
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test40.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

