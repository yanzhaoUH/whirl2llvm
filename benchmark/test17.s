	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test17.c (test17.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test17.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 40, %rsp
	# n = 0
	# i = 16
	# factorial = 8
	.loc	1	2	0
 #   1  #include <stdio.h>
 #   2  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-40,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_3074:
	xorl %eax,%eax                	# [0] 
	.loc	1	7	0
 #   3  {
 #   4      int n, i;
 #   5      unsigned long long factorial = 1;
 #   6  
 #   7      printf("Enter an integer: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	8	0
 #   8      scanf("%d",&n);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	.loc	1	13	0
 #   9  
 #  10     //n = 10;
 #  11  
 #  12      // show error if the user enters a negative integer
 #  13      if (n < 0)
	movl 0(%rsp),%esi             	# [0] n
	testl %esi,%esi               	# [3] 
	jl .LBB5_main                 	# [4] 
.Lt_0_1282:
	.loc	1	18	0
 #  14          printf("Error! Factorial of a negative number doesn't exist.");
 #  15  
 #  16      else
 #  17      {
 #  18          for(i=1; i<=n; ++i)
	cmpl $1,%esi                  	# [0] 
	jl .Lt_0_2818                 	# [1] 
.LBB8_main:
	movq $1,%r8                   	# [0] 
	movl %esi,%edx                	# [0] 
	movl $1,%ecx                  	# [1] 
	andl $3,%edx                  	# [1] 
	testl $3,%esi                 	# [1] 
	movq $1,%rax                  	# [2] 
	movl %esi,%r9d                	# [2] 
	je .LBB21_main                	# [2] 
.LBB22_main:
	decl %edx                     	# [0] 
	.loc	1	20	0
 #  19          {
 #  20              factorial *= i;              // factorial = factorial*i;
	imulq %rax,%r8                	# [1] 
	incq %rax                     	# [1] 
	testl %edx,%edx               	# [1] 
	movl $2,%ecx                  	# [2] 
	je .LBB21_main                	# [2] 
.LBB23_main:
	movl %edx,%edi                	# [0] 
	imulq %rax,%r8                	# [1] 
	decl %edi                     	# [1] 
	incq %rax                     	# [2] 
	incl %ecx                     	# [2] 
	testl %edi,%edi               	# [2] 
	je .LBB21_main                	# [3] 
.LBB24_main:
	imulq %rax,%r8                	# [0] 
	incq %rax                     	# [0] 
	incl %ecx                     	# [0] 
.LBB21_main:
	movl %r9d,%edx                	# [0] 
	sarl $2,%edx                  	# [1] 
	testl %edx,%edx               	# [2] 
	je .Lt_0_1538                 	# [3] 
.LBB26_main:
.LBB20_main:
 #<loop> Loop body line 18, nesting depth: 1, estimated iterations: 25
 #<loop> unrolled 4 times
	movq %rax,%r11                	# [0] 
	imulq %r8,%r11                	# [1] 
	leaq 1(%rax),%r10             	# [5] 
	movq %r11,%r8                 	# [6] 
	imulq %r10,%r8                	# [7] 
	leaq 2(%rax),%r9              	# [10] 
	imulq %r9,%r8                 	# [12] 
	leaq 3(%rax),%rdi             	# [15] 
	addl $4,%ecx                  	# [16] 
	addq $4,%rax                  	# [17] 
	imulq %rdi,%r8                	# [17] 
	cmpl %esi,%ecx                	# [17] 
	jle .LBB20_main               	# [18] 
.Lt_0_1538:
	xorl %eax,%eax                	# [0] 
	.loc	1	22	0
 #  21          }
 #  22          printf("Factorial of %d = %llu", n, factorial);
	movq %r8,%rdx                 	# [0] 
	movl %esi,%esi                	# [1] 
	movq $(.rodata+112),%rdi      	# [1] .rodata+112
	call printf                   	# [1] printf
.LBB33_main:
.Lt_0_2562:
	xorq %rax,%rax                	# [0] 
	.loc	1	25	0
 #  23      }
 #  24  
 #  25      return 0;
	addq $40,%rsp                 	# [0] 
	ret                           	# [0] 
.Lt_0_2818:
	.loc	1	20	0
	movq $1,%r8                   	# [0] 
	jmp .Lt_0_1538                	# [0] 
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	14	0
	movq $(.rodata+48),%rdi       	# [0] .rodata+48
	call printf                   	# [0] printf
.LBB28_main:
	jmp .Lt_0_2562                	# [0] 
.L_0_3330:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter an integer: "
	.org 0x20
	.align	0
	# offset 32
	.string "%d"
	.org 0x30
	.align	0
	# offset 48
	.string "Error! Factorial of a negative number doesn't exist."
	.org 0x70
	.align	0
	# offset 112
	.string "Factorial of %d = %llu"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_3074-main
	.uleb128	.L_0_3330-.L_0_3074
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x30
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test17.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

