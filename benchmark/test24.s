	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test24.c (test24.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test24.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# n = 0
	# reversedNumber = 4
	.loc	1	2	0
 #   1  #include <stdio.h>
 #   2  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_2050:
	xorl %eax,%eax                	# [0] 
	.loc	1	6	0
 #   3  {
 #   4      int n, reversedNumber = 0, remainder;
 #   5  
 #   6      printf("Enter an integer: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	7	0
 #   7      scanf("%d", &n);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	movl 0(%rsp),%r8d             	# [0] n
	testl %r8d,%r8d               	# [3] 
	je .Lt_0_1794                 	# [4] 
.LBB5_main:
	xorl %esi,%esi                	# [0] 
.LBB15_main:
 #<loop> Loop body line 7, nesting depth: 1, estimated iterations: 34
 #<loop> unrolled 3 times
	.loc	1	13	0
 #   9  
 #  10      while(n != 0)
 #  11      {
 #  12          remainder = n%10;
 #  13          reversedNumber = reversedNumber*10 + remainder;
	movslq %r8d,%rax              	# [0] 
	imulq $1717986919,%rax        	# [1] 
	shrq $32,%rax                 	# [6] 
	movl %r8d,%edx                	# [7] 
	movq %rax,%rax                	# [7] 
	sarl $31,%edx                 	# [8] 
	sarl $2,%eax                  	# [8] 
	subl %edx,%eax                	# [9] 
	movl %eax,%edi                	# [10] 
	imull $10,%edi                	# [11] 
	imull $10,%esi                	# [12] 
	subl %edi,%r8d                	# [14] 
	addl %r8d,%esi                	# [15] 
	.loc	1	14	0
 #  14          n /= 10;
	testl %eax,%eax               	# [15] 
	movl %eax,%r8d                	# [16] 
	je .LBB14_main                	# [16] 
.LBB16_main:
 #<loop> Part of loop body line 7, head labeled .LBB15_main
 #<loop> unrolled 3 times
	.loc	1	13	0
	movslq %eax,%rdx              	# [0] 
	imulq $1717986919,%rdx        	# [1] 
	shrq $32,%rdx                 	# [6] 
	movl %eax,%r9d                	# [7] 
	movq %rdx,%rdx                	# [7] 
	sarl $31,%r9d                 	# [8] 
	sarl $2,%edx                  	# [8] 
	subl %r9d,%edx                	# [9] 
	movl %edx,%ecx                	# [10] 
	movl %esi,%edi                	# [11] 
	imull $10,%ecx                	# [11] 
	imull $10,%edi                	# [12] 
	movl %eax,%esi                	# [13] 
	subl %ecx,%esi                	# [14] 
	addl %edi,%esi                	# [15] 
	.loc	1	14	0
	testl %edx,%edx               	# [15] 
	movl %edx,%r8d                	# [16] 
	je .LBB14_main                	# [16] 
.Lt_0_1282:
 #<loop> Part of loop body line 7, head labeled .LBB15_main
	.loc	1	13	0
	movslq %edx,%r8               	# [0] 
	imulq $1717986919,%r8         	# [1] 
	shrq $32,%r8                  	# [6] 
	movl %edx,%ecx                	# [7] 
	movq %r8,%r8                  	# [7] 
	sarl $31,%ecx                 	# [8] 
	sarl $2,%r8d                  	# [8] 
	subl %ecx,%r8d                	# [9] 
	movl %r8d,%edi                	# [10] 
	movl %esi,%eax                	# [11] 
	imull $10,%edi                	# [11] 
	imull $10,%eax                	# [12] 
	movl %edx,%esi                	# [13] 
	subl %edi,%esi                	# [14] 
	addl %eax,%esi                	# [15] 
	.loc	1	14	0
	testl %r8d,%r8d               	# [15] 
	jne .LBB15_main               	# [16] 
.LBB14_main:
	movl %r8d,0(%rsp)             	# [0] n
.Lt_0_770:
	xorl %eax,%eax                	# [0] 
	.loc	1	17	0
 #  15      }
 #  16  
 #  17      printf("Reversed Number = %d",reversedNumber);
	movl %esi,%esi                	# [1] 
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call printf                   	# [1] printf
.LBB10_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	19	0
 #  18  
 #  19      return 0;
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.Lt_0_1794:
	xorl %esi,%esi                	# [0] 
	jmp .Lt_0_770                 	# [0] 
.L_0_2306:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter an integer: "
	.org 0x20
	.align	0
	# offset 32
	.string "%d"
	.org 0x30
	.align	0
	# offset 48
	.string "Reversed Number = %d"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_2050-main
	.uleb128	.L_0_2306-.L_0_2050
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test24.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

