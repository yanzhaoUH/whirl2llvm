; ModuleID = 'test25.bc'

@.str = private constant [22 x i8] c"Enter a base number: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [21 x i8] c"Enter an exponent: 4\00", align 1
@.str.3 = private constant [14 x i8] c"Answer = %lld\00", align 1

define i32 @main() {
entry:
  %.preg_I8_5_53 = alloca i64
  %.preg_I8_5_51 = alloca i64
  %.preg_I4_4_54 = alloca i32
  %.preg_I4_4_52 = alloca i32
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %_temp_dummy14_11 = alloca i32, align 4
  %_temp_ehpit0_12 = alloca i32, align 4
  %base_4 = alloca i32, align 4
  %exponent_5 = alloca i32, align 4
  %old_frame_pointer_17.addr = alloca i64, align 8
  %result_6 = alloca i64, align 8
  %return_address_18.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %base_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = bitcast i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %3)
  %4 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %5 = bitcast i32* %exponent_5 to i32*
  %call4 = call i32 (i8*, ...) @scanf(i8* %4, i32* %5)
  %6 = load i32, i32* %exponent_5, align 4
  store i32 %6, i32* %.preg_I4_4_52, align 8
  %7 = load i32, i32* %.preg_I4_4_52
  %cmp1 = icmp ne i32 %7, 0
  br i1 %cmp1, label %tb, label %L2818

tb:                                               ; preds = %entry
  %8 = load i32, i32* %.preg_I4_4_52
  store i32 %8, i32* %.preg_I4_4_54, align 8
  %9 = load i32, i32* %base_4, align 4
  %conv3 = sext i32 %9 to i64
  store i64 %conv3, i64* %.preg_I8_5_51, align 8
  %conv31 = zext i32 1 to i64
  store i64 %conv31, i64* %.preg_I8_5_53, align 8
  br label %L2306

L2818:                                            ; preds = %entry
  %conv32 = zext i32 1 to i64
  store i64 %conv32, i64* %.preg_I8_5_53, align 8
  br label %L1794

L2306:                                            ; preds = %L2306, %tb
  %10 = load i64, i64* %.preg_I8_5_51
  %11 = load i64, i64* %.preg_I8_5_53
  %mul = mul i64 %10, %11
  store i64 %mul, i64* %.preg_I8_5_53, align 8
  %12 = load i32, i32* %.preg_I4_4_52
  %add = add i32 %12, -1
  store i32 %add, i32* %.preg_I4_4_52, align 8
  %13 = load i32, i32* %.preg_I4_4_52
  %cmp2 = icmp ne i32 %13, 0
  br i1 %cmp2, label %L2306, label %fb

fb:                                               ; preds = %L2306
  %14 = load i32, i32* %.preg_I4_4_52
  store i32 %14, i32* %exponent_5, align 4
  br label %L1794

L1794:                                            ; preds = %fb, %L2818
  %15 = bitcast i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.3, i32 0, i32 0) to i8*
  %16 = load i64, i64* %.preg_I8_5_53
  %call5 = call i32 (i8*, ...) @printf(i8* %15, i64 %16)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
