; ModuleID = 'test5.bc'

@.str = private constant [17 x i8] c"Enter dividend: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [16 x i8] c"Enter divisor: \00", align 1
@.str.3 = private constant [15 x i8] c"Quotient = %d\0A\00", align 1
@.str.4 = private constant [15 x i8] c"Remainder = %d\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_8 = alloca i32, align 4
  %_temp_dummy11_9 = alloca i32, align 4
  %_temp_dummy12_10 = alloca i32, align 4
  %_temp_dummy13_11 = alloca i32, align 4
  %_temp_dummy14_12 = alloca i32, align 4
  %_temp_dummy15_13 = alloca i32, align 4
  %_temp_ehpit0_14 = alloca i32, align 4
  %dividend_4 = alloca i32, align 4
  %divisor_5 = alloca i32, align 4
  %old_frame_pointer_19.addr = alloca i64, align 8
  %quotient_6 = alloca i32, align 4
  %remainder_7 = alloca i32, align 4
  %return_address_20.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %dividend_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = bitcast i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %3)
  %4 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %5 = bitcast i32* %divisor_5 to i32*
  %call4 = call i32 (i8*, ...) @scanf(i8* %4, i32* %5)
  %"<preg>_49" = alloca i32, align 4
  %6 = load i32, i32* %dividend_4, align 4
  %7 = load i32, i32* %divisor_5, align 4
  %lowPart_4 = alloca i32, align 4
  %div = sdiv i32 %6, %7
  %srem = srem i32 %6, %7
  store i32 %div, i32* %lowPart_4, align 4
  store i32 %srem, i32* %"<preg>_49", align 8
  %remainder_51 = alloca i32, align 4
  %8 = load i32, i32* %"<preg>_49", align 4
  store i32 %8, i32* %remainder_51, align 8
  %9 = bitcast i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.3, i32 0, i32 0) to i8*
  %call5 = call i32 (i8*, ...) @printf(i8* %9, i32* %lowPart_4)
  %10 = bitcast i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.4, i32 0, i32 0) to i8*
  %11 = load i32, i32* %remainder_51, align 4
  %call6 = call i32 (i8*, ...) @printf(i8* %10, i32 %11)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
