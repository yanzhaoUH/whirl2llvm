; ModuleID = 'test45.bc'

@.str = private constant [28 x i8] c"Enter the numbers of data: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [46 x i8] c"Error! number should in range of (1 to 100).\0A\00", align 1
@.str.3 = private constant [25 x i8] c"Enter the number again: \00", align 1
@.str.4 = private constant [19 x i8] c"%d. Enter number: \00", align 1
@.str.5 = private constant [15 x i8] c"Average = %.2f\00", align 1

define i32 @main() {
entry:
  %.preg_F4_10_54 = alloca float
  %.preg_I4_4_51 = alloca i32
  %.preg_I4_4_50 = alloca i32
  %.preg_I4_4_53 = alloca i32
  %_temp_dummy10_9 = alloca i32, align 4
  %_temp_dummy11_10 = alloca i32, align 4
  %_temp_dummy12_11 = alloca i32, align 4
  %_temp_dummy13_12 = alloca i32, align 4
  %_temp_dummy14_13 = alloca i32, align 4
  %_temp_dummy15_14 = alloca i32, align 4
  %_temp_dummy16_15 = alloca i32, align 4
  %_temp_ehpit0_16 = alloca i32, align 4
  %average_8 = alloca float, align 4
  %i_5 = alloca i32, align 4
  %n_4 = alloca i32, align 4
  %num_6 = alloca [100 x float], align 4
  %old_frame_pointer_21.addr = alloca i64, align 8
  %return_address_22.addr = alloca i64, align 8
  %sum_7 = alloca float, align 4
  %0 = bitcast i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = load i32, i32* %n_4, align 4
  store i32 %3, i32* %.preg_I4_4_53, align 8
  %4 = load i32, i32* %.preg_I4_4_53
  %add = add i32 %4, -1
  %cmp1 = icmp ugt i32 %add, 99
  br i1 %cmp1, label %tb, label %L3330

tb:                                               ; preds = %entry
  br label %L3842

L3330:                                            ; preds = %fb, %entry
  store i32 0, i32* %.preg_I4_4_50, align 8
  %5 = load i32, i32* %.preg_I4_4_53
  %cmp3 = icmp sgt i32 %5, 0
  br i1 %cmp3, label %tb2, label %L5890

L3842:                                            ; preds = %L3842, %tb
  %6 = bitcast i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %6)
  %7 = bitcast i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.3, i32 0, i32 0) to i8*
  %call4 = call i32 (i8*, ...) @printf(i8* %7)
  %8 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %9 = bitcast i32* %n_4 to i32*
  %call5 = call i32 (i8*, ...) @scanf(i8* %8, i32* %9)
  %10 = load i32, i32* %n_4, align 4
  store i32 %10, i32* %.preg_I4_4_53, align 8
  %11 = load i32, i32* %.preg_I4_4_53
  %add.1 = add i32 %11, -1
  %cmp2 = icmp ugt i32 %add.1, 99
  br i1 %cmp2, label %L3842, label %fb

fb:                                               ; preds = %L3842
  br label %L3330

tb2:                                              ; preds = %L3330
  store i32 1, i32* %.preg_I4_4_51, align 8
  store float 0.000000e+00, float* %.preg_F4_10_54, align 8
  br label %L4866

L5890:                                            ; preds = %L3330
  store float 0.000000e+00, float* %.preg_F4_10_54, align 8
  br label %L4354

L4866:                                            ; preds = %L4866, %tb2
  %12 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.4, i32 0, i32 0) to i8*
  %13 = load i32, i32* %.preg_I4_4_51
  %call6 = call i32 (i8*, ...) @printf(i8* %12, i32 %13)
  %14 = load i32, i32* %.preg_I4_4_50
  %conv9 = sitofp i32 %14 to float
  %15 = load float, float* %.preg_F4_10_54
  %add.3 = fadd float %conv9, %15
  store float %add.3, float* %.preg_F4_10_54, align 8
  %16 = load i32, i32* %.preg_I4_4_50
  %add.4 = add i32 %16, 1
  store i32 %add.4, i32* %.preg_I4_4_50, align 8
  %17 = load i32, i32* %.preg_I4_4_51
  %add.5 = add i32 %17, 1
  store i32 %add.5, i32* %.preg_I4_4_51, align 8
  %18 = load i32, i32* %n_4, align 4
  store i32 %18, i32* %.preg_I4_4_53, align 8
  %19 = load i32, i32* %.preg_I4_4_53
  %20 = load i32, i32* %.preg_I4_4_50
  %cmp4 = icmp sgt i32 %19, %20
  br i1 %cmp4, label %L4866, label %fb6

fb6:                                              ; preds = %L4866
  br label %L4354

L4354:                                            ; preds = %fb6, %L5890
  %21 = bitcast i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.5, i32 0, i32 0) to i8*
  %22 = load float, float* %.preg_F4_10_54
  %23 = load i32, i32* %.preg_I4_4_53
  %conv97 = sitofp i32 %23 to float
  %SDiv = fdiv float %22, %conv97
  %convDouble = fpext float %SDiv to double
  %call7 = call i32 (i8*, ...) @printf(i8* %21, double %convDouble)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
