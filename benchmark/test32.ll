; ModuleID = 'test32.bc'

@.str = private constant [27 x i8] c"Enter the number of rows: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [4 x i8] c"%d \00", align 1
@.str.3 = private constant [2 x i8] c"\0A\00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_52 = alloca i32
  %.preg_I4_4_53 = alloca i32
  %.preg_I4_4_49 = alloca i32
  %.preg_I4_4_51 = alloca i32
  %.preg_I4_4_50 = alloca i32
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %_temp_ehpit0_11 = alloca i32, align 4
  %i_4 = alloca i32, align 4
  %j_5 = alloca i32, align 4
  %old_frame_pointer_16.addr = alloca i64, align 8
  %return_address_17.addr = alloca i64, align 8
  %rows_6 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %rows_6 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = load i32, i32* %rows_6, align 4
  %cmp1 = icmp sgt i32 %3, 0
  br i1 %cmp1, label %tb, label %L3330

tb:                                               ; preds = %entry
  store i32 1, i32* %.preg_I4_4_51, align 8
  br label %L3842

L3330:                                            ; preds = %fb5, %entry
  ret i32 0

L3842:                                            ; preds = %L5634, %tb
  %cmp2 = icmp sge i32 %i_50_PHI, 0
  br i1 %cmp2, label %tb1, label %L5634

tb1:                                              ; preds = %L3842
  store i32 0, i32* %.preg_I4_4_49, align 8
  %4 = load i32, i32* %.preg_I4_4_51
  store i32 %4, i32* %.preg_I4_4_53, align 8
  store i32 1, i32* %.preg_I4_4_52, align 8
  br label %L4866

L5634:                                            ; preds = %fb, %L3842
  %i_50_PHI = phi i32 [ 0, %entry ], [ %add.3, %L5634 ]
  %5 = bitcast i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.3, i32 0, i32 0) to i8*
  %call4 = call i32 (i8*, ...) @printf(i8* %5)
  %add.3 = add i32 %i_50_PHI, 1
  %6 = load i32, i32* %.preg_I4_4_51
  %add.4 = add i32 %6, 1
  store i32 %add.4, i32* %.preg_I4_4_51, align 8
  %7 = load i32, i32* %rows_6, align 4
  %cmp4 = icmp sgt i32 %7, %i_50_PHI
  br i1 %cmp4, label %L3842, label %fb5

L4866:                                            ; preds = %L4866, %tb1
  %8 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.2, i32 0, i32 0) to i8*
  %9 = load i32, i32* %.preg_I4_4_52
  %call3 = call i32 (i8*, ...) @printf(i8* %8, i32 %9)
  %10 = load i32, i32* %.preg_I4_4_49
  %add = add i32 %10, 1
  store i32 %add, i32* %.preg_I4_4_49, align 8
  %11 = load i32, i32* %.preg_I4_4_52
  %add.2 = add i32 %11, 1
  store i32 %add.2, i32* %.preg_I4_4_52, align 8
  %12 = load i32, i32* %.preg_I4_4_49
  %cmp3 = icmp sle i32 %12, %i_50_PHI
  br i1 %cmp3, label %L4866, label %fb

fb:                                               ; preds = %L4866
  br label %L5634

fb5:                                              ; preds = %L5634
  br label %L3330
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
