	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test13.c (test13.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test13.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# year = 0
	.loc	1	2	0
 #   1  #include <stdio.h>
 #   2  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_3330:
	xorl %eax,%eax                	# [0] 
	.loc	1	6	0
 #   3  {
 #   4      int year;
 #   5  
 #   6      printf("Enter a year: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	7	0
 #   7      scanf("%d",&year);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+16),%rdi       	# [1] .rodata+16
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	.loc	1	10	0
 #   8      //year = 345;
 #   9  
 #  10      if(year%4 == 0)
	movl 0(%rsp),%esi             	# [0] year
	testl $3,%esi                 	# [3] 
	jne .Lt_0_1794                	# [4] 
.LBB5_main:
	.loc	1	12	0
 #  11      {
 #  12          if( year%100 == 0)
	movslq %esi,%rax              	# [0] 
	imulq $1374389535,%rax        	# [1] 
	shrq $32,%rax                 	# [6] 
	movl %esi,%edx                	# [7] 
	movq %rax,%rcx                	# [7] 
	sarl $31,%edx                 	# [8] 
	sarl $5,%ecx                  	# [8] 
	subl %edx,%ecx                	# [9] 
	imull $100,%ecx               	# [10] 
	movl %esi,%edi                	# [12] 
	subl %ecx,%edi                	# [13] 
	testl %edi,%edi               	# [14] 
	jne .Lt_0_2050                	# [15] 
.LBB6_main:
	.loc	1	15	0
 #  13          {
 #  14              // year is divisible by 400, hence the year is a leap year
 #  15              if ( year%400 == 0)
	movq %rax,%rcx                	# [0] 
	sarl $7,%ecx                  	# [1] 
	subl %edx,%ecx                	# [2] 
	imull $400,%ecx               	# [3] 
	movl %esi,%edi                	# [5] 
	subl %ecx,%edi                	# [6] 
	testl %edi,%edi               	# [7] 
	je .LBB7_main                 	# [8] 
.Lt_0_2306:
	xorl %eax,%eax                	# [0] 
	.loc	1	18	0
 #  16                  printf("%d is a leap year.", year);
 #  17              else
 #  18                  printf("%d is not a leap year.", year);
	movl %esi,%esi                	# [1] 
	movq $(.rodata+64),%rdi       	# [1] .rodata+64
	call printf                   	# [1] printf
.LBB23_main:
.Lt_0_3074:
	xorq %rax,%rax                	# [0] 
	.loc	1	26	0
 #  22      }
 #  23      else
 #  24          printf("%d is not a leap year.", year);
 #  25      
 #  26      return 0;
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.Lt_0_2050:
	xorl %eax,%eax                	# [0] 
	.loc	1	21	0
	movl %esi,%esi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	call printf                   	# [1] printf
.LBB17_main:
	jmp .Lt_0_3074                	# [0] 
.Lt_0_1794:
	xorl %eax,%eax                	# [0] 
	.loc	1	24	0
	movl %esi,%esi                	# [1] 
	movq $(.rodata+64),%rdi       	# [1] .rodata+64
	call printf                   	# [1] printf
.LBB18_main:
	jmp .Lt_0_3074                	# [0] 
.LBB7_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	16	0
	movl %esi,%esi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	call printf                   	# [1] printf
.LBB19_main:
	jmp .Lt_0_3074                	# [0] 
.L_0_3586:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter a year: "
	.org 0x10
	.align	0
	# offset 16
	.string "%d"
	.org 0x20
	.align	0
	# offset 32
	.string "%d is a leap year."
	.org 0x40
	.align	0
	# offset 64
	.string "%d is not a leap year."

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_3330-main
	.uleb128	.L_0_3586-.L_0_3330
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test13.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

