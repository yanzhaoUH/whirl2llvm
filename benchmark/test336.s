	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test336.c (test336.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test336.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	.loc	1	8	0
 #   4  /*int fun(char* a, short* b, int* c, long* d, float* e, double* f){
 #   5  printf("aaaa");
 #   6  }*/
 #   7  
 #   8  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_258:
	xorl %eax,%eax                	# [0] 
	.loc	1	56	0
 #  52    double d2 = 2.0;
 #  53    double d3 = d1*d2;
 #  54  
 #  55  
 #  56    printf("%d %d %d %d %d %d %d", c3, s3, i3, l3, ll3, ui3, ul3);
	movq $2,%r9                   	# [0] 
	movq $2,%r8                   	# [1] 
	movq $2,%rcx                  	# [1] 
	movq $2,%rdx                  	# [1] 
	movq $2,%rsi                  	# [2] 
	movq $(.rodata),%rdi          	# [2] .rodata
	movl $1,%r10d                 	# [2] 
	movq %r10,8(%rsp)             	# [3] id:63
	movl %r10d,0(%rsp)            	# [3] id:62
	.globl	printf
	call printf                   	# [3] printf
.LBB3_main:
	.loc	1	58	0
 #  57  
 #  58    printf("%f %f", f3, d3); 
	movsd .rodata+24(%rip),%xmm1  	# [0] 
	movl $2,%eax                  	# [2] 
	movq $(.rodata+32),%rdi       	# [3] .rodata+32
	movaps %xmm1,%xmm0            	# [3] 
	call printf                   	# [3] printf
.LBB4_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	60	0
 #  59  
 #  60      return 0;
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.L_0_514:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "%d %d %d %d %d %d %d"
	.org 0x18
	.align	0
	# offset 24
	.4byte	0
	.4byte	1073741824
	# double 2.00000
	.org 0x20
	.align	0
	# offset 32
	.string "%f %f"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_258-main
	.uleb128	.L_0_514-.L_0_258
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test336.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

