	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test337.c (test337.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test337.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 104, %rsp
	# array = 0
	# farray = 64
	# str1 = 16
	.loc	1	24	0
 #  20  double* fp;
 #  21  int array123[3];
 #  22  };
 #  23  
 #  24  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-104,%rsp               	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_258:
	xorl %eax,%eax                	# [0] 
	.loc	1	30	0
 #  26  
 #  27  
 #  28   // test 1
 #  29   char* array[2] = {"test 1","test 1 1"};
 #  30   printf("%s \n",array[1]);
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.loc	1	29	0
	movq $(.rodata),%rdx          	# [1] .rodata
	movq $(.rodata+16),%rsi       	# [1] .rodata+16
	movq %rsi,8(%rsp)             	# [2] array+8
	movq %rdx,0(%rsp)             	# [2] array
	.loc	1	30	0
	.globl	printf
	call printf                   	# [2] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	33	0
 #  31  
 #  32   char* ap= "test 2";
 #  33    printf("%s\n",ap);
	movq $(.rodata+64),%rsi       	# [1] .rodata+64
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call printf                   	# [1] printf
.LBB4_main:
	.loc	1	35	0
 #  34    
 #  35    f1(array); 
	movq %rsp,%rdi                	# [0] 
	call _Z2f1PPc                 	# [0] _Z2f1PPc
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	47	0
 #  43   struct str str1;
 #  44   str1.a = 100;
 #  45   str1.array123[1] = 33;
 #  46   str1.fp = farray;
 #  47   printf("%d\n",str1.array123[1]);
	movq $(.rodata+80),%rdi       	# [1] .rodata+80
	.loc	1	45	0
	movl $33,%esi                 	# [1] 
	.loc	1	46	0
	leaq 64(%rsp),%rdx            	# [1] farray
	.loc	1	44	0
	movl $100,%ecx                	# [2] 
	.loc	1	45	0
	movl %esi,44(%rsp)            	# [2] str1+28
	.loc	1	47	0
	movl %esi,%esi                	# [2] 
	.loc	1	44	0
	movq %rcx,24(%rsp)            	# [3] str1+8
	.loc	1	46	0
	movq %rdx,32(%rsp)            	# [3] str1+16
	.loc	1	47	0
	call printf                   	# [3] printf
.LBB6_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	51	0
 #  48  
 #  49  
 #  50   struct str* strp = &str1;
 #  51   printf("%d\n",strp->array123[1]);
	movq $33,%rsi                 	# [1] 
	movq $(.rodata+80),%rdi       	# [1] .rodata+80
	call printf                   	# [1] printf
.LBB7_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	54	0
 #  52  
 #  53  
 #  54   return 0;
	addq $104,%rsp                	# [0] 
	ret                           	# [0] 
.L_0_514:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "test 1"
	.org 0x10
	.align	0
	# offset 16
	.string "test 1 1"
	.org 0x20
	.align	0
	# offset 32
	.byte	0x25, 0x73, 0x20, 0xa, 0x0	# %s \n\000
	.org 0x30
	.align	0
	# offset 48
	.byte	0x25, 0x73, 0xa, 0x0	# %s\n\000
	.org 0x40
	.align	0
	# offset 64
	.string "test 2"
	.org 0x50
	.align	0
	# offset 80
	.byte	0x25, 0x64, 0xa, 0x0	# %d\n\000

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_258-main
	.uleb128	.L_0_514-.L_0_258
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.p2align 5,,

	# Program Unit: _Z2f1PPc
.globl	_Z2f1PPc
	.type	_Z2f1PPc, @function
_Z2f1PPc:	# 0x7c
	# .frame	%rsp, 24, %rsp
	.loc	1	9	0
.LBB1__Z2f1PPc:
.LEH_adjustsp__Z2f1PPc:
	addq $-24,%rsp                	# [0] 
	movq %rdi,%rdx                	# [0] 
.L_1_258:
	xorl %eax,%eax                	# [0] 
	.loc	1	11	0
	movq 8(%rdx),%rsi             	# [1] id:6
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call printf                   	# [1] printf
.LBB3__Z2f1PPc:
	xorq %rax,%rax                	# [0] 
	.loc	1	12	0
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.L_1_514:
.LDWend__Z2f1PPc:
	.size _Z2f1PPc, .LDWend__Z2f1PPc-_Z2f1PPc

	.section .except_table
	.align	0
	.type	.range_table._Z2f1PPc, @object
.range_table._Z2f1PPc:	# 0x8
	# offset 8
	.byte	255
	# offset 9
	.byte	0
	.uleb128	.LSDATTYPEB2-.LSDATTYPED2
.LSDATTYPED2:
	# offset 14
	.byte	1
	.uleb128	.LSDACSE2-.LSDACSB2
.LSDACSB2:
	.uleb128	.L_1_258-_Z2f1PPc
	.uleb128	.L_1_514-.L_1_258
	# offset 25
	.uleb128	0
	# offset 29
	.uleb128	0
.LSDACSE2:
	# offset 33
	.sleb128	0
	# offset 37
	.sleb128	0
.LSDATTYPEB2:
	# end of initialization for .range_table._Z2f1PPc
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x70
	.align 8
.LFDE1_end:
	.4byte	.LFDE2_end - .LFDE2_begin
.LFDE2_begin:
	.4byte	.LFDE2_begin - .LEHCIE
	.quad	.LBB1__Z2f1PPc
	.quad	.LDWend__Z2f1PPc - .LBB1__Z2f1PPc
	.byte	0x08
	.quad	.range_table._Z2f1PPc
	.byte	0x04
	.4byte	.LEH_adjustsp__Z2f1PPc - .LBB1__Z2f1PPc
	.byte	0x0e, 0x20
	.align 8
.LFDE2_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test337.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

