	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test35.c (test35.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test35.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# n = 0
	.loc	1	7	0
 #   3  
 #   4  int checkPrimeNumber(int n);
 #   5  int checkArmstrongNumber(int n);
 #   6  
 #   7  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_2306:
	xorl %eax,%eax                	# [0] 
	.loc	1	11	0
 #   8  {
 #   9      int n, flag;
 #  10  
 #  11      printf("Enter a positive integer: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	12	0
 #  12      scanf("%d", &n); 
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	.loc	1	17	0
 #  13      //n = 31;
 #  14  
 #  15      // Check prime number
 #  16  
 #  17      flag = checkPrimeNumber(n);
	movl 0(%rsp),%edi             	# [0] n
	call _Z16checkPrimeNumberi    	# [0] _Z16checkPrimeNumberi
.LBB5_main:
	movl %eax,%eax                	# [0] 
	.loc	1	18	0
 #  18      if (flag == 1)
	cmpl $1,%eax                  	# [1] 
	movl 0(%rsp),%esi             	# [2] n
	je .LBB6_main                 	# [2] 
.Lt_0_1282:
	xorl %eax,%eax                	# [0] 
	.loc	1	21	0
 #  19          printf("%d is a prime number.\n", n);
 #  20      else
 #  21          printf("%d is not a prime number.\n", n);
	movl %esi,%esi                	# [1] 
	movq $(.rodata+80),%rdi       	# [1] .rodata+80
	call printf                   	# [1] printf
.LBB23_main:
.Lt_0_1794:
	.loc	1	25	0
 #  22  
 #  23      // check Armstrong number
 #  24  
 #  25      flag = checkArmstrongNumber(n);
	movl 0(%rsp),%edi             	# [0] n
	call _Z20checkArmstrongNumberi	# [0] _Z20checkArmstrongNumberi
.LBB10_main:
	movl %eax,%eax                	# [0] 
	.loc	1	26	0
 #  26      if (flag == 1)
	cmpl $1,%eax                  	# [1] 
	movl 0(%rsp),%esi             	# [2] n
	je .LBB11_main                	# [2] 
.Lt_0_1538:
	xorl %eax,%eax                	# [0] 
	.loc	1	29	0
 #  27          printf("%d is an Armstrong number.", n);
 #  28      else
 #  29          printf("%d is not an Armstrong number.",n);
	movl %esi,%esi                	# [1] 
	movq $(.rodata+144),%rdi      	# [1] .rodata+144
	call printf                   	# [1] printf
.LBB24_main:
.Lt_0_2050:
	xorq %rax,%rax                	# [0] 
	.loc	1	30	0
 #  30      return 0;
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.LBB6_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	19	0
	movl %esi,%esi                	# [1] 
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call printf                   	# [1] printf
.LBB17_main:
	jmp .Lt_0_1794                	# [0] 
.LBB11_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	27	0
	movl %esi,%esi                	# [1] 
	movq $(.rodata+112),%rdi      	# [1] .rodata+112
	call printf                   	# [1] printf
.LBB18_main:
	jmp .Lt_0_2050                	# [0] 
.L_0_2562:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter a positive integer: "
	.org 0x20
	.align	0
	# offset 32
	.string "%d"
	.org 0x30
	.align	0
	# offset 48
	.byte	0x25, 0x64, 0x20, 0x69, 0x73, 0x20, 0x61, 0x20	# %d is a 
	.byte	0x70, 0x72, 0x69, 0x6d, 0x65, 0x20, 0x6e, 0x75	# prime nu
	.byte	0x6d, 0x62, 0x65, 0x72, 0x2e, 0xa, 0x0	# mber.\n\000
	.org 0x50
	.align	0
	# offset 80
	.byte	0x25, 0x64, 0x20, 0x69, 0x73, 0x20, 0x6e, 0x6f	# %d is no
	.byte	0x74, 0x20, 0x61, 0x20, 0x70, 0x72, 0x69, 0x6d	# t a prim
	.byte	0x65, 0x20, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72	# e number
	.byte	0x2e, 0xa, 0x0	# .\n\000
	.org 0x70
	.align	0
	# offset 112
	.string "%d is an Armstrong number."
	.org 0x90
	.align	0
	# offset 144
	.string "%d is not an Armstrong number."

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_2306-main
	.uleb128	.L_0_2562-.L_0_2306
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.p2align 5,,

	# Program Unit: _Z20checkArmstrongNumberi
.globl	_Z20checkArmstrongNumberi
	.type	_Z20checkArmstrongNumberi, @function
_Z20checkArmstrongNumberi:	# 0xa4
	# .frame	%rsp, 72, %rsp
	# result = 4
	# n = 0
	# _temp_gra_spill1 = 8
	# _temp_gra_spill2 = 16
	# _temp_gra_spill3 = 24
	# _temp_gra_spill4 = 32
	.loc	1	50	0
 #  46      }
 #  47      return flag;
 #  48  }
 #  49  
 #  50  int checkArmstrongNumber(int number)
.LBB1__Z20checkArmstrongNumberi:
	addq $-72,%rsp                	# [0] 
	.loc	1	54	0
 #  51  {
 #  52      int originalNumber, remainder, result = 0, n = 0, flag;
 #  53  
 #  54      originalNumber = number;
	movl %edi,%eax                	# [0] 
	testl %edi,%edi               	# [1] 
	.loc	1	50	0
	movq %r12,8(%rsp)             	# [1] _temp_gra_spill1
	movl %edi,%r12d               	# [1] 
	movq %rbp,16(%rsp)            	# [2] _temp_gra_spill2
	movq %rbx,24(%rsp)            	# [2] _temp_gra_spill3
	.loc	1	54	0
	je .Lt_1_3842                 	# [2] 
.LBB2__Z20checkArmstrongNumberi:
	xorl %edx,%edx                	# [0] 
.LBB17__Z20checkArmstrongNumberi:
 #<loop> Loop body line 58
 #<loop> unrolled 3 times
	.loc	1	58	0
 #  55  
 #  56      while (originalNumber != 0)
 #  57      {
 #  58          originalNumber /= 10;
	movslq %eax,%rsi              	# [0] 
	imulq $1717986919,%rsi        	# [1] 
	shrq $32,%rsi                 	# [6] 
	movl %eax,%edi                	# [7] 
	movq %rsi,%rax                	# [7] 
	sarl $31,%edi                 	# [8] 
	sarl $2,%eax                  	# [8] 
	subl %edi,%eax                	# [9] 
	.loc	1	59	0
 #  59          ++n;
	addl $1,%edx                  	# [10] 
	testl %eax,%eax               	# [10] 
	je .Lt_1_1794                 	# [11] 
.LBB18__Z20checkArmstrongNumberi:
 #<loop> Part of loop body line 58, head labeled .LBB17__Z20checkArmstrongNumberi
 #<loop> unrolled 3 times
	.loc	1	58	0
	movslq %eax,%rsi              	# [0] 
	imulq $1717986919,%rsi        	# [1] 
	shrq $32,%rsi                 	# [6] 
	movl %eax,%edi                	# [7] 
	movq %rsi,%rax                	# [7] 
	sarl $31,%edi                 	# [8] 
	sarl $2,%eax                  	# [8] 
	subl %edi,%eax                	# [9] 
	.loc	1	59	0
	addl $1,%edx                  	# [10] 
	testl %eax,%eax               	# [10] 
	je .Lt_1_1794                 	# [11] 
.Lt_1_2306:
 #<loop> Part of loop body line 58, head labeled .LBB17__Z20checkArmstrongNumberi
	.loc	1	58	0
	movslq %eax,%rsi              	# [0] 
	imulq $1717986919,%rsi        	# [1] 
	shrq $32,%rsi                 	# [6] 
	movl %eax,%edi                	# [7] 
	movq %rsi,%rax                	# [7] 
	sarl $31,%edi                 	# [8] 
	sarl $2,%eax                  	# [8] 
	subl %edi,%eax                	# [9] 
	.loc	1	59	0
	addl $1,%edx                  	# [10] 
	testl %eax,%eax               	# [10] 
	jne .LBB17__Z20checkArmstrongNumberi	# [11] 
.Lt_1_1794:
	.loc	1	62	0
 #  60      }
 #  61  
 #  62      originalNumber = number;
	testl %r12d,%r12d             	# [0] 
	movl %r12d,%eax               	# [1] 
	je .Lt_1_4098                 	# [1] 
.LBB7__Z20checkArmstrongNumberi:
	cvtsi2sd %edx,%xmm0           	# [0] 
	xorl %ebp,%ebp                	# [4] 
	movsd %xmm0,32(%rsp)          	# [4] _temp_gra_spill4
.Lt_1_3330:
 #<loop> Loop body line 67
	.loc	1	67	0
 #  63  
 #  64      while (originalNumber != 0)
 #  65      {
 #  66          remainder = originalNumber%10;
 #  67          result += pow(remainder, n);
	movslq %eax,%rbx              	# [0] 
	imulq $1717986919,%rbx        	# [1] 
	shrq $32,%rbx                 	# [6] 
	movl %eax,%esi                	# [7] 
	movq %rbx,%rbx                	# [7] 
	sarl $31,%esi                 	# [8] 
	sarl $2,%ebx                  	# [8] 
	subl %esi,%ebx                	# [9] 
	movl %ebx,%edi                	# [10] 
	imull $10,%edi                	# [11] 
	subl %edi,%eax                	# [14] 
	movsd 32(%rsp),%xmm1          	# [15] _temp_gra_spill4
	cvtsi2sd %eax,%xmm0           	# [15] 
	.globl	pow
	call pow                      	# [15] pow
.LBB9__Z20checkArmstrongNumberi:
 #<loop> Part of loop body line 67, head labeled .Lt_1_3330
	cvtsi2sd %ebp,%xmm1           	# [0] 
	addsd %xmm1,%xmm0             	# [4] 
	.loc	1	68	0
 #  68          originalNumber /= 10;
	testl %ebx,%ebx               	# [6] 
	movl %ebx,%eax                	# [7] 
	.loc	1	67	0
	cvttsd2si %xmm0,%ebp          	# [7] 
	.loc	1	68	0
	jne .Lt_1_3330                	# [7] 
.Lt_1_2818:
	.loc	1	77	0
 #  73          flag = 1;
 #  74      else
 #  75          flag = 0;
 #  76  
 #  77      return flag;
	movq 24(%rsp),%rbx            	# [0] _temp_gra_spill3
	cmpl %ebp,%r12d               	# [0] 
	movq 16(%rsp),%rbp            	# [1] _temp_gra_spill2
	movq 8(%rsp),%r12             	# [1] _temp_gra_spill1
	sete %al                      	# [1] 
	movzbq %al,%rax               	# [2] 
	addq $72,%rsp                 	# [2] 
	ret                           	# [2] 
.Lt_1_4098:
	xorl %edi,%edi                	# [0] 
	movq 24(%rsp),%rbx            	# [1] _temp_gra_spill3
	cmpl %edi,%r12d               	# [1] 
	movq 16(%rsp),%rbp            	# [2] _temp_gra_spill2
	movq 8(%rsp),%r12             	# [2] _temp_gra_spill1
	sete %al                      	# [2] 
	movzbq %al,%rax               	# [3] 
	addq $72,%rsp                 	# [3] 
	ret                           	# [3] 
.Lt_1_3842:
	xorl %edx,%edx                	# [0] 
	jmp .Lt_1_1794                	# [0] 
.LDWend__Z20checkArmstrongNumberi:
	.size _Z20checkArmstrongNumberi, .LDWend__Z20checkArmstrongNumberi-_Z20checkArmstrongNumberi
	.section .text
	.p2align 5,,

	# Program Unit: _Z16checkPrimeNumberi
.globl	_Z16checkPrimeNumberi
	.type	_Z16checkPrimeNumberi, @function
_Z16checkPrimeNumberi:	# 0x200
	# .frame	%rsp, 40, %rsp
	# i = 0
	.loc	1	33	0
.LBB1__Z16checkPrimeNumberi:
	addq $-40,%rsp                	# [0] 
	movl %edi,%r8d                	# [0] 
	sarl $31,%r8d                 	# [1] 
	andl $1,%r8d                  	# [2] 
	addl %edi,%r8d                	# [3] 
	sarl $1,%r8d                  	# [4] 
	cmpl $2,%r8d                  	# [5] 
	jl .Lt_2_1538                 	# [6] 
.LBB2__Z16checkPrimeNumberi:
	movl $2,%ecx                  	# [0] 
.Lt_2_2050:
 #<loop> Loop body line 33, nesting depth: 1, estimated iterations: 100
	.loc	1	41	0
	movl %edi,%eax                	# [0] 
	cltd                          	# [1] 
	idivl %ecx                    	# [2] 
	testl %edx,%edx               	# [24] 
	je .LBB5__Z16checkPrimeNumberi	# [25] 
.Lt_2_2306:
 #<loop> Part of loop body line 33, head labeled .Lt_2_2050
	.loc	1	44	0
	addl $1,%ecx                  	# [0] 
	cmpl %ecx,%r8d                	# [1] 
	jge .Lt_2_2050                	# [2] 
.Lt_2_1538:
	movl $1,%eax                  	# [0] 
	.loc	1	47	0
	movl %eax,%eax                	# [1] 
	addq $40,%rsp                 	# [1] 
	ret                           	# [1] 
.LBB5__Z16checkPrimeNumberi:
	xorl %eax,%eax                	# [0] 
	movl %eax,%eax                	# [1] 
	addq $40,%rsp                 	# [1] 
	ret                           	# [1] 
.LDWend__Z16checkPrimeNumberi:
	.size _Z16checkPrimeNumberi, .LDWend__Z16checkPrimeNumberi-_Z16checkPrimeNumberi
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test35.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

