	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test52.c (test52.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test52.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 56, %rsp
	# data = 0
	# i = 20
	# _temp_gra_spill1 = 24
	# _temp_gra_spill2 = 32
	.loc	1	3	0
 #   1  #include <stdio.h>
 #   2  
 #   3  int main()
.LBB1_main:
.LEH_adjustsp_main:
.LEH_csr_main:
	addq $-56,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
	movq %rbx,32(%rsp)            	# [0] _temp_gra_spill2
	movq %rbp,24(%rsp)            	# [0] _temp_gra_spill1
.L_0_3330:
	xorl %eax,%eax                	# [0] 
	.loc	1	6	0
 #   4  {
 #   5     int data[5], i;
 #   6     printf("Enter elements: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %ebp,%ebp                	# [0] 
.LBB14_main:
 #<loop> Loop body line 6, nesting depth: 1, iterations: 1
 #<loop> unrolled 5 times (fully)
	.loc	1	10	0
 #   7  
 #   8     for(i = 0; i < 5; ++i)
 #   9       //scanf("%d", data + i);
 #  10      *(data+i) = i;
	leal 1(%rbp),%edx             	# [0] 
	leal 1(%rdx),%esi             	# [2] 
	leal 1(%rsi),%edi             	# [4] 
	leal 1(%rdi),%eax             	# [6] 
	movl %ebp,0(%rsp)             	# [7] id:17 data+0x0
	movl %edx,4(%rsp)             	# [7] id:17 data+0x0
	movl %esi,8(%rsp)             	# [8] id:17 data+0x0
	movl %edi,12(%rsp)            	# [8] id:17 data+0x0
	movl %eax,16(%rsp)            	# [8] id:17 data+0x0
.LBB13_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	12	0
 #  11  
 #  12     printf("You entered: \n");
	movq $(.rodata+32),%rdi       	# [0] .rodata+32
	call printf                   	# [0] printf
.LBB6_main:
	xorl %ebp,%ebp                	# [0] 
	movq %rsp,%rbx                	# [0] 
.Lt_0_2818:
 #<loop> Loop body line 12, nesting depth: 1, iterations: 5
	xorl %eax,%eax                	# [0] 
	.loc	1	15	0
 #  13     for(i = 0; i < 5; ++i){
 #  14        
 #  15        printf("%d\n", *(data + i));
	movl 0(%rbx),%esi             	# [1] id:18 data+0x0
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call printf                   	# [1] printf
.LBB8_main:
 #<loop> Part of loop body line 12, head labeled .Lt_0_2818
	addl $1,%ebp                  	# [0] 
	addq $4,%rbx                  	# [1] 
	cmpl $4,%ebp                  	# [1] 
	jle .Lt_0_2818                	# [2] 
.LBB16_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	18	0
 #  16     }
 #  17  
 #  18     return 0;
	movq 32(%rsp),%rbx            	# [0] _temp_gra_spill2
	movq 24(%rsp),%rbp            	# [1] _temp_gra_spill1
	addq $56,%rsp                 	# [1] 
	ret                           	# [1] 
.L_0_3586:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter elements: "
	.org 0x20
	.align	0
	# offset 32
	.byte	0x59, 0x6f, 0x75, 0x20, 0x65, 0x6e, 0x74, 0x65	# You ente
	.byte	0x72, 0x65, 0x64, 0x3a, 0x20, 0xa, 0x0	# red: \n\000
	.org 0x30
	.align	0
	# offset 48
	.byte	0x25, 0x64, 0xa, 0x0	# %d\n\000

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_3330-main
	.uleb128	.L_0_3586-.L_0_3330
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x40, 0x04
	.4byte	.LEH_csr_main - .LEH_adjustsp_main
	.byte	0x86, 0x05, 0x83, 0x04
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test52.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

