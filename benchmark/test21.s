	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test21.c (test21.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test21.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# n1 = 0
	# n2 = 4
	.loc	1	2	0
 #   1  #include <stdio.h>
 #   2  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_6146:
	xorl %eax,%eax                	# [0] 
	.loc	1	5	0
 #   3  {
 #   4      int n1, n2, minMultiple;
 #   5      printf("Enter two positive integers: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	6	0
 #   6      scanf("%d %d", &n1, &n2);
	leaq 4(%rsp),%rdx             	# [0] n2
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	.loc	1	10	0
 #   7      //n1 = 3;
 #   8      //n2 = 4;
 #   9      // maximum number between n1 and n2 is stored in minMultiple
 #  10      minMultiple = (n1>n2) ? n1 : n2;
	movl 0(%rsp),%esi             	# [0] n1
	movl 4(%rsp),%r10d            	# [0] n2
	cmpl %esi,%r10d               	# [3] 
	movl %esi,%r9d                	# [4] 
	cmovge %r10,%r9               	# [4] 
	jmp .Lt_0_4354                	# [4] 
.L_0_2818:
 #<loop> Part of loop body line 15, head labeled .Lt_0_4354
	.loc	1	20	0
 #  16          {
 #  17              printf("The LCM of %d and %d is %d.", n1, n2,minMultiple);
 #  18              break;
 #  19          }
 #  20          ++minMultiple;
	addl $1,%r9d                  	# [0] 
.Lt_0_4354:
 #<loop> Loop body line 15
	.loc	1	15	0
	movl %r9d,%eax                	# [0] 
	cltd                          	# [1] 
	movl %eax,%ecx                	# [2] 
	movl %edx,%edi                	# [2] 
	idivl %esi                    	# [2] 
	testl %edx,%edx               	# [24] 
	jne .L_0_2818                 	# [25] 
.LBB6_main:
 #<loop> Part of loop body line 15, head labeled .Lt_0_4354
	movl %edi,%edx                	# [0] 
	movl %ecx,%eax                	# [0] 
	idivl %r10d                   	# [1] 
	testl %edx,%edx               	# [23] 
	jne .L_0_2818                 	# [24] 
.Lt_0_5378:
	xorl %eax,%eax                	# [0] 
	.loc	1	17	0
	movl %r9d,%ecx                	# [0] 
	movl %r10d,%edx               	# [0] 
	movl %esi,%esi                	# [1] 
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call printf                   	# [1] printf
.LBB10_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	22	0
 #  21      }
 #  22      return 0;
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.L_0_6402:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter two positive integers: "
	.org 0x20
	.align	0
	# offset 32
	.string "%d %d"
	.org 0x30
	.align	0
	# offset 48
	.string "The LCM of %d and %d is %d."

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_6146-main
	.uleb128	.L_0_6402-.L_0_6146
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test21.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

