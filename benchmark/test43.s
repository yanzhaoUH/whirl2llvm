	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test43.c (test43.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test43.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 8, %rsp
	.loc	1	5	0
 #   1  /* Example to reverse a sentence entered by user without using strings. */
 #   2  
 #   3  #include <stdio.h>
 #   4  void Reverse();
 #   5  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-8,%rsp                 	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_258:
	xorl %eax,%eax                	# [0] 
	.loc	1	7	0
 #   6  {
 #   7      printf("Enter a sentence: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	.loc	1	8	0
 #   8      Reverse();
	call _Z7Reversev              	# [0] _Z7Reversev
.LBB4_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	9	0
 #   9      return 0;
	addq $8,%rsp                  	# [0] 
	ret                           	# [0] 
.L_0_514:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter a sentence: "

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_258-main
	.uleb128	.L_0_514-.L_0_258
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.p2align 5,,

	# Program Unit: _Z7Reversev
.globl	_Z7Reversev
	.type	_Z7Reversev, @function
_Z7Reversev:	# 0x20
	# .frame	%rsp, 24, %rsp
	# c = 0
	.loc	1	11	0
 #  10  }
 #  11  void Reverse()
.LBB1__Z7Reversev:
.LEH_adjustsp__Z7Reversev:
	addq $-24,%rsp                	# [0] 
.L_1_1282:
	xorl %eax,%eax                	# [0] 
	.loc	1	14	0
 #  12  {
 #  13      char c;
 #  14      scanf("%c",&c);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB3__Z7Reversev:
	.loc	1	16	0
 #  15      
 #  16      if( c != '\n')
	movsbl 0(%rsp),%eax           	# [0] c
	cmpl $10,%eax                 	# [3] 
	je .Lt_1_1026                 	# [4] 
.LBB4__Z7Reversev:
	.loc	1	18	0
 #  17      {
 #  18          Reverse();
	call _Z7Reversev              	# [0] _Z7Reversev
.LBB5__Z7Reversev:
	xorl %eax,%eax                	# [0] 
	.loc	1	19	0
 #  19          printf("%c",c);
	movsbl 0(%rsp),%esi           	# [1] c
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	call printf                   	# [1] printf
.LBB13__Z7Reversev:
.Lt_1_1026:
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.L_1_1538:
.LDWend__Z7Reversev:
	.size _Z7Reversev, .LDWend__Z7Reversev-_Z7Reversev

	.section .rodata
	.org 0x20
	.align	0
	# offset 32
	.string "%c"

	.section .except_table
	.align	0
	.type	.range_table._Z7Reversev, @object
.range_table._Z7Reversev:	# 0x8
	# offset 8
	.byte	255
	# offset 9
	.byte	0
	.uleb128	.LSDATTYPEB2-.LSDATTYPED2
.LSDATTYPED2:
	# offset 14
	.byte	1
	.uleb128	.LSDACSE2-.LSDACSB2
.LSDACSB2:
	.uleb128	.L_1_1282-_Z7Reversev
	.uleb128	.L_1_1538-.L_1_1282
	# offset 25
	.uleb128	0
	# offset 29
	.uleb128	0
.LSDACSE2:
	# offset 33
	.sleb128	0
	# offset 37
	.sleb128	0
.LSDATTYPEB2:
	# end of initialization for .range_table._Z7Reversev
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x10
	.align 8
.LFDE1_end:
	.4byte	.LFDE2_end - .LFDE2_begin
.LFDE2_begin:
	.4byte	.LFDE2_begin - .LEHCIE
	.quad	.LBB1__Z7Reversev
	.quad	.LDWend__Z7Reversev - .LBB1__Z7Reversev
	.byte	0x08
	.quad	.range_table._Z7Reversev
	.byte	0x04
	.4byte	.LEH_adjustsp__Z7Reversev - .LBB1__Z7Reversev
	.byte	0x0e, 0x20
	.align 8
.LFDE2_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test43.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

