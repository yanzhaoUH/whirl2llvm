	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test68.c (test68.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test68.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 1016, %rsp
	# sentence = 0
	# _temp_gra_spill2 = 1000
	.loc	1	4	0
 #   1  #include <stdio.h>
 #   2  #include <stdlib.h>  /* For exit() function */
 #   3  #include <string.h>
 #   4  int main()
.LBB1_main:
.LEH_adjustsp_main:
.LEH_csr_main:
	addq $-1016,%rsp              	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
	movq %rbx,1000(%rsp)          	# [0] _temp_gra_spill2
.L_0_1026:
	.loc	1	9	0
 #   5  {
 #   6     char sentence[1000];
 #   7     FILE *fptr;
 #   8  
 #   9     fptr = fopen("program.txt", "w");
	movq $(.rodata+16),%rsi       	# [0] .rodata+16
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	fopen
	call fopen                    	# [0] fopen
.LBB3_main:
	.loc	1	10	0
 #  10     if(fptr == NULL)
	testq %rax,%rax               	# [0] 
	.loc	1	9	0
	movq %rax,%rbx                	# [1] 
	.loc	1	10	0
	jne .Lt_0_770                 	# [1] 
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	12	0
 #  11     {
 #  12        printf("Error!");
	movq $(.rodata+32),%rdi       	# [0] .rodata+32
	.globl	printf
	call printf                   	# [0] printf
.LBB5_main:
	.loc	1	13	0
 #  13        exit(1);
	movq $1,%rdi                  	# [0] 
	.globl	exit
	call exit                     	# [0] exit
.LBB6_main:
	movq 1000(%rsp),%rbx          	# [0] _temp_gra_spill2
	addq $1016,%rsp               	# [0] 
	ret                           	# [0] 
.Lt_0_770:
	xorl %eax,%eax                	# [0] 
	.loc	1	16	0
 #  14     }
 #  15     
 #  16     printf("Enter a sentence:\n");
	movq $(.rodata+48),%rdi       	# [0] .rodata+48
	call printf                   	# [0] printf
.LBB8_main:
	.loc	1	17	0
 #  17     gets(sentence);
	movq %rsp,%rdi                	# [0] 
	.globl	gets
	call gets                     	# [0] gets
.LBB9_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	20	0
 #  18     //strcpy(sentence, "hello world\n");
 #  19  
 #  20     fprintf(fptr,"%s", sentence);
	movq %rsp,%rdx                	# [0] 
	movq $(.rodata+80),%rsi       	# [1] .rodata+80
	movq %rbx,%rdi                	# [1] 
	.globl	fprintf
	call fprintf                  	# [1] fprintf
.LBB10_main:
	.loc	1	21	0
 #  21     fclose(fptr);
	movq %rbx,%rdi                	# [0] 
	.globl	fclose
	call fclose                   	# [0] fclose
.LBB11_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	23	0
 #  22  
 #  23     return 0;
	movq 1000(%rsp),%rbx          	# [1] _temp_gra_spill2
	addq $1016,%rsp               	# [1] 
	ret                           	# [1] 
.L_0_1282:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "program.txt"
	.org 0x10
	.align	0
	# offset 16
	.string "w"
	.org 0x20
	.align	0
	# offset 32
	.string "Error!"
	.org 0x30
	.align	0
	# offset 48
	.byte	0x45, 0x6e, 0x74, 0x65, 0x72, 0x20, 0x61, 0x20	# Enter a 
	.byte	0x73, 0x65, 0x6e, 0x74, 0x65, 0x6e, 0x63, 0x65	# sentence
	.byte	0x3a, 0xa, 0x0	# :\n\000
	.org 0x50
	.align	0
	# offset 80
	.string "%s"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_1026-main
	.uleb128	.L_0_1282-.L_0_1026
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x80, 0x08, 0x04
	.4byte	.LEH_csr_main - .LEH_adjustsp_main
	.byte	0x83, 0x03
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test68.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

