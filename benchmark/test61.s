	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test61.c (test61.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test61.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 648, %rsp
	# i = 580
	# str = 16
	# temp = 528
	# _temp_gra_spill1 = 584
	# _temp_gra_spill2 = 592
	# _temp_gra_spill3 = 600
	# _temp_gra_spill4 = 608
	# _temp_gra_spill5 = 616
	# _temp_gra_spill6 = 624
	.loc	1	4	0
 #   1  #include<stdio.h>
 #   2  #include <string.h>
 #   3  
 #   4  int main()
.LBB1_main:
.LEH_adjustsp_main:
.LEH_csr_main:
	addq $-648,%rsp               	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
	movq %rbx,624(%rsp)           	# [0] _temp_gra_spill6
	movq %rbp,608(%rsp)           	# [0] _temp_gra_spill4
	movq %r12,600(%rsp)           	# [0] _temp_gra_spill3
	movq %r13,616(%rsp)           	# [0] _temp_gra_spill5
	movq %r14,592(%rsp)           	# [0] _temp_gra_spill2
	movq %r15,584(%rsp)           	# [0] _temp_gra_spill1
.L_0_7682:
	xorl %eax,%eax                	# [0] 
	.loc	1	9	0
 #   5  {
 #   6      int i, j;
 #   7      char str[10][50], temp[50];
 #   8  
 #   9      printf("Enter 10 words:\n");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %r13d,%r13d              	# [0] 
	leaq 16(%rsp),%rbp            	# [0] str
.Lt_0_3330:
 #<loop> Loop body line 9, nesting depth: 1, iterations: 3
	xorl %eax,%eax                	# [0] 
	.loc	1	12	0
 #  10  
 #  11      for(i=0; i<3; ++i)
 #  12          scanf("%s[^\n]",str[i]);
	movq %rbp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB5_main:
 #<loop> Part of loop body line 9, head labeled .Lt_0_3330
	addl $1,%r13d                 	# [0] 
	addq $50,%rbp                 	# [1] 
	cmpl $2,%r13d                 	# [1] 
	jle .Lt_0_3330                	# [2] 
.LBB25_main:
	xorl %r13d,%r13d              	# [0] 
	leaq 16(%rsp),%rbp            	# [1] str
	movl $2,%r14d                 	# [1] 
	movl $1,%r12d                 	# [1] 
.Lt_0_4354:
 #<loop> Loop body line 12, nesting depth: 1, iterations: 2
	.loc	1	20	0
 #  16  
 #  17  
 #  18  
 #  19      for(i=0; i<2; ++i)
 #  20          for(j=i+1; j<3 ; ++j)
	cmpl $2,%r12d                 	# [0] 
	movl %r12d,%r15d              	# [1] 
	jg .Lt_0_4610                 	# [1] 
.LBB8_main:
 #<loop> Part of loop body line 12, head labeled .Lt_0_4354
	movslq %r12d,%rbx             	# [0] 
	imulq $50,%rbx                	# [1] 
	leaq 16(%rsp,%rbx,1), %rbx    	# [6] str
.Lt_0_5122:
 #<loop> Loop body line 20, nesting depth: 2, estimated iterations: 100
	.loc	1	22	0
 #  21          {
 #  22              if(strcmp(str[i], str[j])>0)
	movq %rbx,%rsi                	# [0] 
	movq %rbp,%rdi                	# [0] 
	.globl	strcmp
	call strcmp                   	# [0] strcmp
.LBB11_main:
 #<loop> Part of loop body line 20, head labeled .Lt_0_5122
	movl %eax,%eax                	# [0] 
	testl %eax,%eax               	# [1] 
	jle .Lt_0_7170                	# [2] 
.LBB12_main:
 #<loop> Part of loop body line 20, head labeled .Lt_0_5122
	.loc	1	24	0
 #  23              {
 #  24                  strcpy(temp, str[i]);
	movq %rbp,%rsi                	# [0] 
	leaq 528(%rsp),%rdi           	# [0] temp
	.globl	strcpy
	call strcpy                   	# [0] strcpy
.LBB13_main:
 #<loop> Part of loop body line 20, head labeled .Lt_0_5122
	.loc	1	25	0
 #  25                  strcpy(str[i], str[j]);
	movq %rbx,%rsi                	# [0] 
	movq %rbp,%rdi                	# [0] 
	.globl	strcpy
	call strcpy                   	# [0] strcpy
.LBB14_main:
 #<loop> Part of loop body line 20, head labeled .Lt_0_5122
	.loc	1	26	0
 #  26                  strcpy(str[j], temp);
	leaq 528(%rsp),%rsi           	# [0] temp
	movq %rbx,%rdi                	# [0] 
	.globl	strcpy
	call strcpy                   	# [0] strcpy
.LBB35_main:
 #<loop> Part of loop body line 20, head labeled .Lt_0_5122
.Lt_0_7170:
 #<loop> Part of loop body line 20, head labeled .Lt_0_5122
	addl $1,%r12d                 	# [0] 
	addq $50,%rbx                 	# [1] 
	cmpl $2,%r12d                 	# [1] 
	jle .Lt_0_5122                	# [2] 
.Lt_0_4610:
 #<loop> Part of loop body line 12, head labeled .Lt_0_4354
	addq $50,%rbp                 	# [0] 
	addl $1,%r13d                 	# [0] 
	leal 1(%r15),%r12d            	# [1] 
	addl $-1,%r14d                	# [1] 
	cmpl $1,%r13d                 	# [1] 
	jle .Lt_0_4354                	# [2] 
.LBB17_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	30	0
 #  27              }
 #  28          }
 #  29  
 #  30      printf("\nIn lexicographical order: \n");
	movq $(.rodata+48),%rdi       	# [0] .rodata+48
	call printf                   	# [0] printf
.LBB18_main:
	xorl %r13d,%r13d              	# [0] 
	leaq 16(%rsp),%rbp            	# [0] str
.Lt_0_6658:
 #<loop> Loop body line 30, nesting depth: 1, iterations: 3
	.loc	1	33	0
 #  31      for(i=0; i<3; ++i)
 #  32      {
 #  33          puts(str[i]);
	movq %rbp,%rdi                	# [0] 
	.globl	puts
	call puts                     	# [0] puts
.LBB20_main:
 #<loop> Part of loop body line 30, head labeled .Lt_0_6658
	addl $1,%r13d                 	# [0] 
	addq $50,%rbp                 	# [1] 
	cmpl $2,%r13d                 	# [1] 
	jle .Lt_0_6658                	# [2] 
.LBB29_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	36	0
 #  34      }
 #  35  
 #  36      return 0;
	movq 624(%rsp),%rbx           	# [0] _temp_gra_spill6
	movq 608(%rsp),%rbp           	# [0] _temp_gra_spill4
	movq 600(%rsp),%r12           	# [1] _temp_gra_spill3
	movq 616(%rsp),%r13           	# [1] _temp_gra_spill5
	movq 592(%rsp),%r14           	# [1] _temp_gra_spill2
	movq 584(%rsp),%r15           	# [2] _temp_gra_spill1
	addq $648,%rsp                	# [2] 
	ret                           	# [2] 
.L_0_7938:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.byte	0x45, 0x6e, 0x74, 0x65, 0x72, 0x20, 0x31, 0x30	# Enter 10
	.byte	0x20, 0x77, 0x6f, 0x72, 0x64, 0x73, 0x3a, 0xa	#  words:\n
	.byte	0x0	# \000
	.org 0x20
	.align	0
	# offset 32
	.byte	0x25, 0x73, 0x5b, 0x5e, 0xa, 0x5d, 0x0	# %s[^\n]\000
	.org 0x30
	.align	0
	# offset 48
	.byte	0xa, 0x49, 0x6e, 0x20, 0x6c, 0x65, 0x78, 0x69	# \nIn lexi
	.byte	0x63, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69	# cographi
	.byte	0x63, 0x61, 0x6c, 0x20, 0x6f, 0x72, 0x64, 0x65	# cal orde
	.byte	0x72, 0x3a, 0x20, 0xa, 0x0	# r: \n\000

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_7682-main
	.uleb128	.L_0_7938-.L_0_7682
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x90, 0x05, 0x04
	.4byte	.LEH_csr_main - .LEH_adjustsp_main
	.byte	0x8f, 0x09, 0x8e, 0x08, 0x8c, 0x07, 0x86, 0x06
	.byte	0x8d, 0x05, 0x83, 0x04
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test61.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

