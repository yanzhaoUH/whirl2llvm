	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test59.c (test59.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test59.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 232, %rsp
	# s1 = 0
	# s2 = 112
	# i = 212
	# j = 213
	.loc	1	3	0
 #   1  #include <stdio.h>
 #   2  #include <string.h>
 #   3  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-232,%rsp               	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_3842:
	xorl %eax,%eax                	# [0] 
	.loc	1	7	0
 #   4  {
 #   5      char s1[100], s2[100], i, j;
 #   6  
 #   7      printf("Enter first string: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	8	0
 #   8      scanf("%s", s1);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	11	0
 #   9  //strcpy(s1, "hello");
 #  10  
 #  11      printf("Enter second string: ");
	movq $(.rodata+48),%rdi       	# [0] .rodata+48
	call printf                   	# [0] printf
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	12	0
 #  12      scanf("%s", s2);
	leaq 112(%rsp),%rsi           	# [1] s2
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	call scanf                    	# [1] scanf
.LBB6_main:
	movsbl 0(%rsp),%eax           	# [0] s1
	cmpl $0,%eax                  	# [3] 
	je .Lt_0_3330                 	# [4] 
.LBB7_main:
	xorl %r8d,%r8d                	# [0] 
.LBB21_main:
 #<loop> Loop body line 17
 #<loop> unrolled 3 times
	.loc	1	17	0
 #  13  //strcpy(s2, " world");
 #  14  
 #  15      // calculate the length of string s1
 #  16      // and store it in i
 #  17      for(i = 0; s1[i] != '\0'; ++i);
	addl $1,%r8d                  	# [0] 
	movsbq %r8b,%rax              	# [1] 
	movsbl 0(%rsp,%rax,1), %eax   	# [2] s1
	cmpl $0,%eax                  	# [5] 
	movsbl %r8b,%r8d              	# [6] 
	je .Lt_0_1282                 	# [6] 
.LBB22_main:
 #<loop> Part of loop body line 17, head labeled .LBB21_main
 #<loop> unrolled 3 times
	incl %r8d                     	# [0] 
	movsbq %r8b,%rax              	# [1] 
	movsbl 0(%rsp,%rax,1), %eax   	# [2] s1
	cmpl $0,%eax                  	# [5] 
	movsbl %r8b,%r8d              	# [6] 
	je .Lt_0_1282                 	# [6] 
.Lt_0_1794:
 #<loop> Part of loop body line 17, head labeled .LBB21_main
	incl %r8d                     	# [0] 
	movsbq %r8b,%rax              	# [1] 
	movsbl 0(%rsp,%rax,1), %eax   	# [2] s1
	cmpl $0,%eax                  	# [5] 
	movsbl %r8b,%r8d              	# [6] 
	jne .LBB21_main               	# [6] 
.Lt_0_1282:
	movsbl 112(%rsp),%eax         	# [0] s2
	cmpl $0,%eax                  	# [3] 
	je .Lt_0_2306                 	# [4] 
.LBB12_main:
	xorl %edx,%edx                	# [0] 
.LBB25_main:
 #<loop> Loop body line 21
 #<loop> unrolled 3 times
	.loc	1	21	0
 #  18  
 #  19      for(j = 0; s2[j] != '\0'; ++j, ++i)
 #  20      {
 #  21          s1[i] = s2[j];
	addl $1,%edx                  	# [0] 
	movslq %r8d,%rcx              	# [1] 
	movsbq %dl,%rdi               	# [1] 
	movb %al,0(%rsp,%rcx,1)       	# [2] s1
	movsbl 112(%rsp,%rdi,1), %eax 	# [2] s2
	leal 1(%r8),%esi              	# [4] 
	testl %eax,%eax               	# [5] 
	movsbl %dl,%edx               	# [6] 
	movsbl %sil,%r8d              	# [6] 
	je .Lt_0_2306                 	# [6] 
.LBB26_main:
 #<loop> Part of loop body line 21, head labeled .LBB25_main
 #<loop> unrolled 3 times
	incl %edx                     	# [0] 
	movsbq %sil,%r9               	# [1] 
	movsbq %dl,%rdi               	# [1] 
	movb %al,0(%rsp,%r9,1)        	# [2] s1
	movsbl 112(%rsp,%rdi,1), %eax 	# [2] s2
	leal 1(%r8),%ecx              	# [4] 
	testl %eax,%eax               	# [5] 
	movsbl %dl,%edx               	# [6] 
	movsbl %cl,%r8d               	# [6] 
	je .Lt_0_2306                 	# [6] 
.Lt_0_2818:
 #<loop> Part of loop body line 21, head labeled .LBB25_main
	incl %edx                     	# [0] 
	movsbq %cl,%rsi               	# [1] 
	movsbq %dl,%rdi               	# [1] 
	movb %al,0(%rsp,%rsi,1)       	# [2] s1
	movsbl 112(%rsp,%rdi,1), %eax 	# [2] s2
	incl %r8d                     	# [5] 
	testl %eax,%eax               	# [5] 
	movsbl %r8b,%r8d              	# [6] 
	movsbl %dl,%edx               	# [6] 
	jne .LBB25_main               	# [6] 
.Lt_0_2306:
	.loc	1	24	0
 #  22      }
 #  23  
 #  24      s1[i] = '\0';
	movq %rsp,%rsi                	# [0] 
	movslq %r8d,%rdx              	# [0] 
	xorl %eax,%eax                	# [0] 
	.loc	1	25	0
 #  25      printf("After concatenation: %s", s1);
	movq $(.rodata+80),%rdi       	# [1] .rodata+80
	.loc	1	24	0
	movb %al,0(%rsp,%rdx,1)       	# [1] s1
	.loc	1	25	0
	call printf                   	# [1] printf
.LBB15_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	27	0
 #  26  
 #  27      return 0;
	addq $232,%rsp                	# [0] 
	ret                           	# [0] 
.Lt_0_3330:
	xorl %r8d,%r8d                	# [0] 
	jmp .Lt_0_1282                	# [0] 
.L_0_4098:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter first string: "
	.org 0x20
	.align	0
	# offset 32
	.string "%s"
	.org 0x30
	.align	0
	# offset 48
	.string "Enter second string: "
	.org 0x50
	.align	0
	# offset 80
	.string "After concatenation: %s"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_3842-main
	.uleb128	.L_0_4098-.L_0_3842
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0xf0, 0x01
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test59.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

