#!/bin/bash
FILENAME="$1"

BNAME="$FILENAME.B"
echo $BNAME
ONAME="$FILENAME.O"
echo $ONAME
UHNAME="$FILENAME.uh"
OUHNAME="$FILENAME-o.uh"
LLNAME="$FILENAME.ll"
echo $OUHNAME
CPPNAME="$FILENAME.c"
rm $BNAME 
rm $UHNAME
rm $OUHNAME
#openCC  -fno-inline -c -keep -show  -Wb,-PHASE:cg=off $CPPNAME

#to produce .O file
openCC -c -keep -show  -Wb,-PHASE:cg=off $CPPNAME


ir_b2a -st -pre  $BNAME > $UHNAME
ir_b2a -st -pre  $ONAME > $OUHNAME

clang -emit-llvm -S $CPPNAME -o $LLNAME
if [ $? -ne 0 ]
	    then
               printf "***"
               echo "clang $CPPNAME fail!!!***"
	       exit 1
	    fi 

rm "./L/$LLNAME"
mv $LLNAME "./L/$LLNAME"





