	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test335.c (test335.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test335.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# a = 0
	.loc	1	11	0
 #   7  	float mF;
 #   8  };
 #   9  
 #  10  
 #  11  int main(void)
.LBB1_main:
.LEH_adjustsp_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_258:
	xorl %eax,%eax                	# [0] 
	.loc	1	15	0
 #  12  {
 #  13    int a[4];
 #  14    a[2] = 26;
 #  15    printf("%d\n", a[2]);
	movq $(.rodata),%rdi          	# [0] .rodata
	.loc	1	14	0
	movl $26,%esi                 	# [0] 
	movl %esi,8(%rsp)             	# [1] a+8
	.loc	1	15	0
	movl %esi,%esi                	# [1] 
	.globl	printf
	call printf                   	# [1] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	19	0
 #  16  
 #  17    struct sTest s1;
 #  18  	s1.mI = 10;
 #  19    printf("%d\n", s1.mI);
	movq $10,%rsi                 	# [1] 
	movq $(.rodata),%rdi          	# [1] .rodata
	call printf                   	# [1] printf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	25	0
 #  21  
 #  22    //int i = 0;
 #  23    int *p = a;
 #  24    *p = 66;
 #  25    printf("%d\n", a[0]);
	movq $(.rodata),%rdi          	# [0] .rodata
	.loc	1	24	0
	movl $66,%esi                 	# [0] 
	movl %esi,0(%rsp)             	# [1] a
	.loc	1	25	0
	movl %esi,%esi                	# [1] 
	call printf                   	# [1] printf
.LBB5_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	0	0
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.L_0_514:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.byte	0x25, 0x64, 0xa, 0x0	# %d\n\000

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_258-main
	.uleb128	.L_0_514-.L_0_258
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test335.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

