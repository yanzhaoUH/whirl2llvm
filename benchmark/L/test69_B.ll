; ModuleID = 'test69.bc'

%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i64, i32, [20 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }

@.str = private constant [12 x i8] c"program.txt\00", align 1
@.str.1 = private constant [2 x i8] c"r\00", align 1
@.str.2 = private constant [20 x i8] c"Error! opening file\00", align 1
@.str.3 = private constant [6 x i8] c"%[^\0A]\00", align 1
@.str.4 = private constant [23 x i8] c"Data from the file:\0A%s\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %_temp_dummy13_9 = alloca i32, align 4
  %_temp_dummy14_10 = alloca i32, align 4
  %c_4 = alloca [1000 x i8], align 1
  %fptr_5 = alloca %struct._IO_FILE*, align 8
  %0 = bitcast i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0) to i8*
  %1 = bitcast i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call1 = call %struct._IO_FILE* @fopen(i8* %0, i8* %1)
  store %struct._IO_FILE* %call1, %struct._IO_FILE** %fptr_5, align 8
  %2 = load %struct._IO_FILE*, %struct._IO_FILE** %fptr_5, align 8
  %3 = ptrtoint %struct._IO_FILE* %2 to i64
  %cmp1 = icmp eq i64 %3, 0
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = bitcast i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call2 = call i32 (i8*, ...) @printf(i8* %4)
  call void @exit(i32 1)
  br label %if.end

if.else:                                          ; preds = %entry
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %5 = load %struct._IO_FILE*, %struct._IO_FILE** %fptr_5, align 8
  %6 = bitcast %struct._IO_FILE* %5 to %struct._IO_FILE*
  %7 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.3, i32 0, i32 0) to i8*
  %arrayAddress = getelementptr [1000 x i8], [1000 x i8]* %c_4, i32 0, i32 0
  %8 = bitcast i8* %arrayAddress to i8*
  %call4 = call i32 (%struct._IO_FILE*, i8*, ...) @fscanf(%struct._IO_FILE* %6, i8* %7, i8* %8)
  %9 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.4, i32 0, i32 0) to i8*
  %arrayAddress1 = getelementptr [1000 x i8], [1000 x i8]* %c_4, i32 0, i32 0
  %10 = bitcast i8* %arrayAddress1 to i8*
  %call5 = call i32 (i8*, ...) @printf(i8* %9, i8* %10)
  %11 = load %struct._IO_FILE*, %struct._IO_FILE** %fptr_5, align 8
  %12 = bitcast %struct._IO_FILE* %11 to %struct._IO_FILE*
  %call6 = call i32 @fclose(%struct._IO_FILE* %12)
  ret i32 0
}

declare %struct._IO_FILE* @fopen(i8*, i8*)

declare i32 @printf(i8*, ...)

declare void @exit(i32)

declare i32 @fscanf(%struct._IO_FILE*, i8*, ...)

declare i32 @fclose(%struct._IO_FILE*)
