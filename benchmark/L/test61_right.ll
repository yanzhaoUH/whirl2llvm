; ModuleID = 'test61.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [17 x i8] c"Enter 10 words:\0A\00", align 1
@.str.1 = private unnamed_addr constant [7 x i8] c"%s[^\0A]\00", align 1
@.str.2 = private unnamed_addr constant [29 x i8] c"\0AIn lexicographical order: \0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %str = alloca [10 x [50 x i8]], align 16
  %temp = alloca [50 x i8], align 16
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %idxprom = sext i32 %1 to i64
  %arrayidx = getelementptr inbounds [10 x [50 x i8]], [10 x [50 x i8]]* %str, i32 0, i64 %idxprom
  %arraydecay = getelementptr inbounds [50 x i8], [50 x i8]* %arrayidx, i32 0, i32 0
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.1, i32 0, i32 0), i8* %arraydecay)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %2 = load i32, i32* %i, align 4
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond.2

for.cond.2:                                       ; preds = %for.inc.36, %for.end
  %3 = load i32, i32* %i, align 4
  %cmp3 = icmp slt i32 %3, 2
  br i1 %cmp3, label %for.body.4, label %for.end.38

for.body.4:                                       ; preds = %for.cond.2
  %4 = load i32, i32* %i, align 4
  %add = add nsw i32 %4, 1
  store i32 %add, i32* %j, align 4
  br label %for.cond.5

for.cond.5:                                       ; preds = %for.inc.33, %for.body.4
  %5 = load i32, i32* %j, align 4
  %cmp6 = icmp slt i32 %5, 3
  br i1 %cmp6, label %for.body.7, label %for.end.35

for.body.7:                                       ; preds = %for.cond.5
  %6 = load i32, i32* %i, align 4
  %idxprom8 = sext i32 %6 to i64
  %arrayidx9 = getelementptr inbounds [10 x [50 x i8]], [10 x [50 x i8]]* %str, i32 0, i64 %idxprom8
  %arraydecay10 = getelementptr inbounds [50 x i8], [50 x i8]* %arrayidx9, i32 0, i32 0
  %7 = load i32, i32* %j, align 4
  %idxprom11 = sext i32 %7 to i64
  %arrayidx12 = getelementptr inbounds [10 x [50 x i8]], [10 x [50 x i8]]* %str, i32 0, i64 %idxprom11
  %arraydecay13 = getelementptr inbounds [50 x i8], [50 x i8]* %arrayidx12, i32 0, i32 0
  %call14 = call i32 @strcmp(i8* %arraydecay10, i8* %arraydecay13) #4
  %cmp15 = icmp sgt i32 %call14, 0
  br i1 %cmp15, label %if.then, label %if.end

if.then:                                          ; preds = %for.body.7
  %arraydecay16 = getelementptr inbounds [50 x i8], [50 x i8]* %temp, i32 0, i32 0
  %8 = load i32, i32* %i, align 4
  %idxprom17 = sext i32 %8 to i64
  %arrayidx18 = getelementptr inbounds [10 x [50 x i8]], [10 x [50 x i8]]* %str, i32 0, i64 %idxprom17
  %arraydecay19 = getelementptr inbounds [50 x i8], [50 x i8]* %arrayidx18, i32 0, i32 0
  %call20 = call i8* @strcpy(i8* %arraydecay16, i8* %arraydecay19) #5
  %9 = load i32, i32* %i, align 4
  %idxprom21 = sext i32 %9 to i64
  %arrayidx22 = getelementptr inbounds [10 x [50 x i8]], [10 x [50 x i8]]* %str, i32 0, i64 %idxprom21
  %arraydecay23 = getelementptr inbounds [50 x i8], [50 x i8]* %arrayidx22, i32 0, i32 0
  %10 = load i32, i32* %j, align 4
  %idxprom24 = sext i32 %10 to i64
  %arrayidx25 = getelementptr inbounds [10 x [50 x i8]], [10 x [50 x i8]]* %str, i32 0, i64 %idxprom24
  %arraydecay26 = getelementptr inbounds [50 x i8], [50 x i8]* %arrayidx25, i32 0, i32 0
  %call27 = call i8* @strcpy(i8* %arraydecay23, i8* %arraydecay26) #5
  %11 = load i32, i32* %j, align 4
  %idxprom28 = sext i32 %11 to i64
  %arrayidx29 = getelementptr inbounds [10 x [50 x i8]], [10 x [50 x i8]]* %str, i32 0, i64 %idxprom28
  %arraydecay30 = getelementptr inbounds [50 x i8], [50 x i8]* %arrayidx29, i32 0, i32 0
  %arraydecay31 = getelementptr inbounds [50 x i8], [50 x i8]* %temp, i32 0, i32 0
  %call32 = call i8* @strcpy(i8* %arraydecay30, i8* %arraydecay31) #5
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body.7
  br label %for.inc.33

for.inc.33:                                       ; preds = %if.end
  %12 = load i32, i32* %j, align 4
  %inc34 = add nsw i32 %12, 1
  store i32 %inc34, i32* %j, align 4
  br label %for.cond.5

for.end.35:                                       ; preds = %for.cond.5
  br label %for.inc.36

for.inc.36:                                       ; preds = %for.end.35
  %13 = load i32, i32* %i, align 4
  %inc37 = add nsw i32 %13, 1
  store i32 %inc37, i32* %i, align 4
  br label %for.cond.2

for.end.38:                                       ; preds = %for.cond.2
  %call39 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.2, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond.40

for.cond.40:                                      ; preds = %for.inc.47, %for.end.38
  %14 = load i32, i32* %i, align 4
  %cmp41 = icmp slt i32 %14, 3
  br i1 %cmp41, label %for.body.42, label %for.end.49

for.body.42:                                      ; preds = %for.cond.40
  %15 = load i32, i32* %i, align 4
  %idxprom43 = sext i32 %15 to i64
  %arrayidx44 = getelementptr inbounds [10 x [50 x i8]], [10 x [50 x i8]]* %str, i32 0, i64 %idxprom43
  %arraydecay45 = getelementptr inbounds [50 x i8], [50 x i8]* %arrayidx44, i32 0, i32 0
  %call46 = call i32 @puts(i8* %arraydecay45)
  br label %for.inc.47

for.inc.47:                                       ; preds = %for.body.42
  %16 = load i32, i32* %i, align 4
  %inc48 = add nsw i32 %16, 1
  store i32 %inc48, i32* %i, align 4
  br label %for.cond.40

for.end.49:                                       ; preds = %for.cond.40
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @__isoc99_scanf(i8*, ...) #1

; Function Attrs: nounwind readonly
declare i32 @strcmp(i8*, i8*) #2

; Function Attrs: nounwind
declare i8* @strcpy(i8*, i8*) #3

declare i32 @puts(i8*) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind readonly "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind readonly }
attributes #5 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
