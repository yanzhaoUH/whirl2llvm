; ModuleID = 'test46.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [43 x i8] c"Enter total number of elements(1 to 100): \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.2 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.3 = private unnamed_addr constant [19 x i8] c"Enter Number %d\0A: \00", align 1
@.str.4 = private unnamed_addr constant [12 x i8] c"reach here\0A\00", align 1
@.str.5 = private unnamed_addr constant [3 x i8] c"%f\00", align 1
@.str.6 = private unnamed_addr constant [23 x i8] c"Largest element = %.2f\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %i = alloca i32, align 4
  %n = alloca i32, align 4
  %arr = alloca [100 x float], align 16
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([43 x i8], [43 x i8]* @.str, i32 0, i32 0))
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0), i32* %n)
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.2, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %n, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %i, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.3, i32 0, i32 0), i32 %2)
  %3 = load i32, i32* %i, align 4
  %conv = sitofp i32 %3 to float
  %4 = load i32, i32* %i, align 4
  %idxprom = sext i32 %4 to i64
  %arrayidx = getelementptr inbounds [100 x float], [100 x float]* %arr, i32 0, i64 %idxprom
  store float %conv, float* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.4, i32 0, i32 0))
  %arrayidx5 = getelementptr inbounds [100 x float], [100 x float]* %arr, i32 0, i64 11
  %6 = load float, float* %arrayidx5, align 4
  %conv6 = fpext float %6 to double
  %call7 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.5, i32 0, i32 0), double %conv6)
  store i32 1, i32* %i, align 4
  br label %for.cond.8

for.cond.8:                                       ; preds = %for.inc.20, %for.end
  %7 = load i32, i32* %i, align 4
  %8 = load i32, i32* %n, align 4
  %cmp9 = icmp slt i32 %7, %8
  br i1 %cmp9, label %for.body.11, label %for.end.22

for.body.11:                                      ; preds = %for.cond.8
  %arrayidx12 = getelementptr inbounds [100 x float], [100 x float]* %arr, i32 0, i64 0
  %9 = load float, float* %arrayidx12, align 4
  %10 = load i32, i32* %i, align 4
  %idxprom13 = sext i32 %10 to i64
  %arrayidx14 = getelementptr inbounds [100 x float], [100 x float]* %arr, i32 0, i64 %idxprom13
  %11 = load float, float* %arrayidx14, align 4
  %cmp15 = fcmp olt float %9, %11
  br i1 %cmp15, label %if.then, label %if.end

if.then:                                          ; preds = %for.body.11
  %12 = load i32, i32* %i, align 4
  %idxprom17 = sext i32 %12 to i64
  %arrayidx18 = getelementptr inbounds [100 x float], [100 x float]* %arr, i32 0, i64 %idxprom17
  %13 = load float, float* %arrayidx18, align 4
  %arrayidx19 = getelementptr inbounds [100 x float], [100 x float]* %arr, i32 0, i64 0
  store float %13, float* %arrayidx19, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body.11
  br label %for.inc.20

for.inc.20:                                       ; preds = %if.end
  %14 = load i32, i32* %i, align 4
  %inc21 = add nsw i32 %14, 1
  store i32 %inc21, i32* %i, align 4
  br label %for.cond.8

for.end.22:                                       ; preds = %for.cond.8
  %arrayidx23 = getelementptr inbounds [100 x float], [100 x float]* %arr, i32 0, i64 0
  %15 = load float, float* %arrayidx23, align 4
  %conv24 = fpext float %15 to double
  %call25 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.6, i32 0, i32 0), double %conv24)
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @__isoc99_scanf(i8*, ...) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
