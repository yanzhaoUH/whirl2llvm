; ModuleID = 'test18_O.bc'

@.str = private constant [19 x i8] c"Enter an integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [15 x i8] c"%d * %d = %d \0A\00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_51 = alloca i32
  %.preg_I4_4_50 = alloca i32
  %.preg_I4_4_49 = alloca i32
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %_temp_ehpit0_9 = alloca i32, align 4
  %i_5 = alloca i32, align 4
  %n_4 = alloca i32, align 4
  %old_frame_pointer_14.addr = alloca i64, align 8
  %return_address_15.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  store i32 0, i32* %.preg_I4_4_49, align 8
  store i32 1, i32* %.preg_I4_4_50, align 8
  br label %L2306

L2306:                                            ; preds = %L2306, %entry
  %3 = load i32, i32* %n_4, align 4
  store i32 %3, i32* %.preg_I4_4_51, align 8
  %4 = bitcast i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i32 0, i32 0) to i8*
  %5 = load i32, i32* %.preg_I4_4_51
  %6 = load i32, i32* %.preg_I4_4_50
  %7 = load i32, i32* %.preg_I4_4_50
  %8 = load i32, i32* %.preg_I4_4_51
  %mul = mul i32 %7, %8
  %call3 = call i32 (i8*, ...) @printf(i8* %4, i32 %5, i32 %6, i32 %mul)
  %9 = load i32, i32* %.preg_I4_4_49
  %add = add i32 %9, 1
  store i32 %add, i32* %.preg_I4_4_49, align 8
  %10 = load i32, i32* %.preg_I4_4_50
  %add.1 = add i32 %10, 1
  store i32 %add.1, i32* %.preg_I4_4_50, align 8
  %11 = load i32, i32* %.preg_I4_4_49
  %cmp1 = icmp sle i32 %11, 9
  br i1 %cmp1, label %L2306, label %fb

fb:                                               ; preds = %L2306
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
