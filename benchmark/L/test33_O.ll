; ModuleID = 'test33_O.bc'

@.str = private constant [31 x i8] c"Enter an operator (+, -, *,): \00", align 1
@.str.1 = private constant [3 x i8] c"%c\00", align 1
@.str.2 = private constant [21 x i8] c"Enter two operands: \00", align 1
@.str.3 = private constant [8 x i8] c"%lf %lf\00", align 1
@.str.4 = private constant [22 x i8] c"%.1lf + %.1lf = %.1lf\00", align 1
@.str.5 = private constant [22 x i8] c"%.1lf - %.1lf = %.1lf\00", align 1
@.str.6 = private constant [22 x i8] c"%.1lf * %.1lf = %.1lf\00", align 1
@.str.7 = private constant [22 x i8] c"%.1lf / %.1lf = %.1lf\00", align 1
@.str.8 = private constant [31 x i8] c"Error! operator is not correct\00", align 1

define i32 @main() {
entry:
  %.preg_F8_11_50 = alloca double
  %.preg_F8_11_49 = alloca double
  %.preg_I4_4_51 = alloca i32
  %_temp__switch_index4_11 = alloca i32, align 4
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %_temp_dummy15_12 = alloca i32, align 4
  %_temp_dummy16_13 = alloca i32, align 4
  %_temp_dummy17_14 = alloca i32, align 4
  %_temp_dummy18_15 = alloca i32, align 4
  %_temp_dummy19_16 = alloca i32, align 4
  %_temp_ehpit0_17 = alloca i32, align 4
  %firstNumber_5 = alloca double, align 8
  %old_frame_pointer_22.addr = alloca i64, align 8
  %op_4 = alloca i8, align 1
  %return_address_23.addr = alloca i64, align 8
  %secondNumber_6 = alloca double, align 8
  %0 = bitcast i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i8* %op_4 to i8*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i8* %2)
  %3 = bitcast i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %3)
  %4 = bitcast i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.3, i32 0, i32 0) to i8*
  %5 = bitcast double* %firstNumber_5 to double*
  %6 = bitcast double* %secondNumber_6 to double*
  %call4 = call i32 (i8*, ...) @scanf(i8* %4, double* %5, double* %6)
  %7 = load i8, i8* %op_4, align 1
  %conv3 = sext i8 %7 to i32
  store i32 %conv3, i32* %.preg_I4_4_51, align 8
  %8 = load i32, i32* %.preg_I4_4_51
  %cmp1 = icmp eq i32 %8, 43
  br i1 %cmp1, label %L258, label %fb

L258:                                             ; preds = %entry
  %9 = load double, double* %secondNumber_6, align 8
  store double %9, double* %.preg_F8_11_49, align 8
  %10 = load double, double* %firstNumber_5, align 8
  store double %10, double* %.preg_F8_11_50, align 8
  %11 = bitcast i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.4, i32 0, i32 0) to i8*
  %12 = load double, double* %.preg_F8_11_50
  %13 = load double, double* %.preg_F8_11_49
  %14 = load double, double* %.preg_F8_11_49
  %15 = load double, double* %.preg_F8_11_50
  %add = fadd double %14, %15
  %call5 = call i32 (i8*, ...) @printf(i8* %11, double %12, double %13, double %add)
  br label %L514

fb:                                               ; preds = %entry
  %16 = load i32, i32* %.preg_I4_4_51
  %cmp2 = icmp eq i32 %16, 45
  br i1 %cmp2, label %L770, label %fb1

L770:                                             ; preds = %fb
  %17 = load double, double* %secondNumber_6, align 8
  store double %17, double* %.preg_F8_11_49, align 8
  %18 = load double, double* %firstNumber_5, align 8
  store double %18, double* %.preg_F8_11_50, align 8
  %19 = bitcast i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.5, i32 0, i32 0) to i8*
  %20 = load double, double* %.preg_F8_11_50
  %21 = load double, double* %.preg_F8_11_49
  %22 = load double, double* %.preg_F8_11_50
  %23 = load double, double* %.preg_F8_11_49
  %add.3 = fsub double %22, %23
  %call6 = call i32 (i8*, ...) @printf(i8* %19, double %20, double %21, double %add.3)
  br label %L514

fb1:                                              ; preds = %fb
  %24 = load i32, i32* %.preg_I4_4_51
  %cmp3 = icmp eq i32 %24, 42
  br i1 %cmp3, label %L1026, label %fb2

L1026:                                            ; preds = %fb1
  %25 = load double, double* %secondNumber_6, align 8
  store double %25, double* %.preg_F8_11_49, align 8
  %26 = load double, double* %firstNumber_5, align 8
  store double %26, double* %.preg_F8_11_50, align 8
  %27 = bitcast i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.6, i32 0, i32 0) to i8*
  %28 = load double, double* %.preg_F8_11_50
  %29 = load double, double* %.preg_F8_11_49
  %30 = load double, double* %.preg_F8_11_49
  %31 = load double, double* %.preg_F8_11_50
  %mul = fmul double %30, %31
  %call7 = call i32 (i8*, ...) @printf(i8* %27, double %28, double %29, double %mul)
  br label %L514

fb2:                                              ; preds = %fb1
  %32 = load i32, i32* %.preg_I4_4_51
  %cmp4 = icmp ne i32 %32, 47
  br i1 %cmp4, label %tb, label %L1282

tb:                                               ; preds = %fb2
  br label %L1538

L1282:                                            ; preds = %fb2
  %33 = load double, double* %firstNumber_5, align 8
  store double %33, double* %.preg_F8_11_50, align 8
  %34 = bitcast i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.7, i32 0, i32 0) to i8*
  %35 = load double, double* %.preg_F8_11_50
  %36 = load double, double* %secondNumber_6, align 8
  %37 = load double, double* %.preg_F8_11_50
  %38 = load double, double* %.preg_F8_11_50
  %SDiv = fdiv double %37, %38
  %call8 = call i32 (i8*, ...) @printf(i8* %34, double %35, double %36, double %SDiv)
  br label %L514

L1538:                                            ; preds = %tb
  %39 = bitcast i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.8, i32 0, i32 0) to i8*
  %call9 = call i32 (i8*, ...) @printf(i8* %39)
  br label %L514

L514:                                             ; preds = %L1538, %L1282, %L1026, %L770, %L258
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
