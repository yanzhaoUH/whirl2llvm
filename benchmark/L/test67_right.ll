; ModuleID = 'test67.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.course = type { i32, [30 x i8] }

@.str = private unnamed_addr constant [26 x i8] c"Enter number of records: \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.2 = private unnamed_addr constant [51 x i8] c"Enter name of the subject and marks respectively:\0A\00", align 1
@.str.3 = private unnamed_addr constant [6 x i8] c"%s %d\00", align 1
@.str.4 = private unnamed_addr constant [25 x i8] c"Displaying Information:\0A\00", align 1
@.str.5 = private unnamed_addr constant [7 x i8] c"%s\09%d\0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %ptr = alloca %struct.course*, align 8
  %i = alloca i32, align 4
  %noOfRecords = alloca i32, align 4
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str, i32 0, i32 0))
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0), i32* %noOfRecords)
  %0 = load i32, i32* %noOfRecords, align 4
  %conv = sext i32 %0 to i64
  %mul = mul i64 %conv, 36
  %call2 = call noalias i8* @malloc(i64 %mul) #3
  %1 = bitcast i8* %call2 to %struct.course*
  store %struct.course* %1, %struct.course** %ptr, align 8
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4
  %3 = load i32, i32* %noOfRecords, align 4
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([51 x i8], [51 x i8]* @.str.2, i32 0, i32 0))
  %4 = load %struct.course*, %struct.course** %ptr, align 8
  %5 = load i32, i32* %i, align 4
  %idx.ext = sext i32 %5 to i64
  %add.ptr = getelementptr inbounds %struct.course, %struct.course* %4, i64 %idx.ext
  %subject = getelementptr inbounds %struct.course, %struct.course* %add.ptr, i32 0, i32 1
  %6 = load %struct.course*, %struct.course** %ptr, align 8
  %7 = load i32, i32* %i, align 4
  %idx.ext5 = sext i32 %7 to i64
  %add.ptr6 = getelementptr inbounds %struct.course, %struct.course* %6, i64 %idx.ext5
  %marks = getelementptr inbounds %struct.course, %struct.course* %add.ptr6, i32 0, i32 0
  %call7 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.3, i32 0, i32 0), [30 x i8]* %subject, i32* %marks)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call8 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.4, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond.9

for.cond.9:                                       ; preds = %for.inc.20, %for.end
  %9 = load i32, i32* %i, align 4
  %10 = load i32, i32* %noOfRecords, align 4
  %cmp10 = icmp slt i32 %9, %10
  br i1 %cmp10, label %for.body.12, label %for.end.22

for.body.12:                                      ; preds = %for.cond.9
  %11 = load %struct.course*, %struct.course** %ptr, align 8
  %12 = load i32, i32* %i, align 4
  %idx.ext13 = sext i32 %12 to i64
  %add.ptr14 = getelementptr inbounds %struct.course, %struct.course* %11, i64 %idx.ext13
  %subject15 = getelementptr inbounds %struct.course, %struct.course* %add.ptr14, i32 0, i32 1
  %arraydecay = getelementptr inbounds [30 x i8], [30 x i8]* %subject15, i32 0, i32 0
  %13 = load %struct.course*, %struct.course** %ptr, align 8
  %14 = load i32, i32* %i, align 4
  %idx.ext16 = sext i32 %14 to i64
  %add.ptr17 = getelementptr inbounds %struct.course, %struct.course* %13, i64 %idx.ext16
  %marks18 = getelementptr inbounds %struct.course, %struct.course* %add.ptr17, i32 0, i32 0
  %15 = load i32, i32* %marks18, align 4
  %call19 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.5, i32 0, i32 0), i8* %arraydecay, i32 %15)
  br label %for.inc.20

for.inc.20:                                       ; preds = %for.body.12
  %16 = load i32, i32* %i, align 4
  %inc21 = add nsw i32 %16, 1
  store i32 %inc21, i32* %i, align 4
  br label %for.cond.9

for.end.22:                                       ; preds = %for.cond.9
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @__isoc99_scanf(i8*, ...) #1

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) #2

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
