; ModuleID = 'test337_O.bc'

%struct._temp_dummy15 = type {}
%struct.str = type { i8, i64, double*, [3 x i32] }

@.str = private constant [7 x i8] c"test 1\00", align 1
@.str.1 = private constant [9 x i8] c"test 1 1\00", align 1
@.str.2 = private constant [5 x i8] c"%s \0A\00", align 1
@.str.3 = private constant [7 x i8] c"test 2\00", align 1
@.str.4 = private constant [4 x i8] c"%s\0A\00", align 1
@.str.5 = private constant [4 x i8] c"%d\0A\00", align 1
@_LIB_VERSION_59 = common global i32 0, align 4
@signgam_60 = common global i32 0, align 4

define i32 @main() {
entry:
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %_temp_dummy13_13 = alloca i32, align 4
  %_temp_dummy14_15 = alloca i32, align 4
  %_temp_dummy15_20 = alloca i32, align 4
  %_temp_dummy15_22 = alloca %struct._temp_dummy15, align 4
  %_temp_ehpit0_23 = alloca i32, align 4
  %a_19.addr = alloca i8**, align 8
  %a_21 = alloca i8**, align 8
  %aa_9 = alloca [3 x i32], align 4
  %ap_6 = alloca i8*, align 1
  %array_4 = alloca [2 x i8*], align 8
  %farray_11 = alloca [3 x double], align 8
  %old_frame_pointer_28.addr = alloca i64, align 8
  %p_10 = alloca i32*, align 4
  %return_address_29.addr = alloca i64, align 8
  %str1_12 = alloca %struct.str, align 4
  %strp_14 = alloca %struct.str*, align 8
  %0 = bitcast i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i32 0, i32 0) to i8*
  %1 = bitcast i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0, i8* %1)
  %2 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.4, i32 0, i32 0) to i8*
  %3 = bitcast i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.3, i32 0, i32 0) to i8*
  %call2 = call i32 (i8*, ...) @printf(i8* %2, i8* %3)
  %4 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.4, i32 0, i32 0) to i8*
  %5 = bitcast i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %4, i8* %5)
  %6 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.5, i32 0, i32 0) to i8*
  %call4 = call i32 (i8*, ...) @printf(i8* %6, i32 33)
  %7 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.5, i32 0, i32 0) to i8*
  %call5 = call i32 (i8*, ...) @printf(i8* %7, i32 33)
  ret i32 0
}

declare i32 @printf(i8*, ...)

define i32 @_Z2f1PPc(i8** %a_4) {
entry:
  %.preg_U8_9_49 = alloca i8**
  %_temp_dummy15_5 = alloca i32, align 4
  %_temp_ehpit1_6 = alloca i32, align 4
  %a_4.addr = alloca i8**, align 8
  %old_frame_pointer_11.addr = alloca i64, align 8
  %return_address_12.addr = alloca i64, align 8
  store i8** %a_4, i8*** %a_4.addr, align 8
  store i8** %a_4, i8*** %.preg_U8_9_49, align 8
  %0 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.4, i32 0, i32 0) to i8*
  %1 = ptrtoint i8** %a_4 to i64
  %2 = inttoptr i64 %1 to i8**
  %pointeridx = getelementptr i8*, i8** %2, i64 1
  %3 = load i8*, i8** %pointeridx, align 8
  %4 = bitcast i8* %3 to i8*
  %call6 = call i32 (i8*, ...) @printf(i8* %0, i8* %4)
  ret i32 0
}
