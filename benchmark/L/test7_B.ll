; ModuleID = 'test7.bc'

@.str = private constant [26 x i8] c"Size of int = %ld bytes \0A\00", align 1
@.str.1 = private constant [26 x i8] c"Size of long = %ld bytes\0A\00", align 1
@.str.2 = private constant [31 x i8] c"Size of long long = %ld bytes\0A\00", align 1
@.str.3 = private constant [28 x i8] c"Size of double = %ld bytes\0A\00", align 1
@.str.4 = private constant [33 x i8] c"Size of long double = %ld bytes\0A\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_9 = alloca i32, align 4
  %_temp_dummy11_10 = alloca i32, align 4
  %_temp_dummy12_11 = alloca i32, align 4
  %_temp_dummy13_12 = alloca i32, align 4
  %_temp_dummy14_13 = alloca i32, align 4
  %a_4 = alloca i32, align 4
  %b_5 = alloca i64, align 8
  %c_6 = alloca i64, align 8
  %e_7 = alloca double, align 8
  %f_8 = alloca x86_fp80, align 16
  %0 = bitcast i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0, i64 4)
  %1 = bitcast i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call2 = call i32 (i8*, ...) @printf(i8* %1, i64 8)
  %2 = bitcast i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %2, i64 8)
  %3 = bitcast i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.3, i32 0, i32 0) to i8*
  %call4 = call i32 (i8*, ...) @printf(i8* %3, i64 8)
  %4 = bitcast i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.4, i32 0, i32 0) to i8*
  %call5 = call i32 (i8*, ...) @printf(i8* %4, i64 16)
  ret i32 0
}

declare i32 @printf(i8*, ...)
