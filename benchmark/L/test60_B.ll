; ModuleID = 'test60.bc'

@.str = private constant [18 x i8] c"Enter string s1: \00", align 1
@.str.1 = private constant [3 x i8] c"%s\00", align 1
@.str.2 = private constant [14 x i8] c"String s2: %s\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %i_6 = alloca i8, align 1
  %s1_4 = alloca [100 x i8], align 1
  %s2_5 = alloca [100 x i8], align 1
  %0 = bitcast i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %arrayAddress = getelementptr [100 x i8], [100 x i8]* %s1_4, i32 0, i32 0
  %2 = bitcast i8* %arrayAddress to i8*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i8* %2)
  %conv = trunc i32 0 to i8
  store i8 %conv, i8* %i_6, align 1
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %3 = load i8, i8* %i_6, align 1
  %arrayidx = getelementptr [100 x i8], [100 x i8]* %s1_4, i32 0, i8 %3
  %4 = load i8, i8* %arrayidx, align 1
  %conv3 = sext i8 %4 to i32
  %cmp1 = icmp ne i32 %conv3, 0
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = load i8, i8* %i_6, align 1
  %arrayidx1 = getelementptr [100 x i8], [100 x i8]* %s2_5, i32 0, i8 %5
  %6 = load i8, i8* %i_6, align 1
  %arrayidx2 = getelementptr [100 x i8], [100 x i8]* %s1_4, i32 0, i8 %6
  %7 = load i8, i8* %arrayidx2, align 1
  %conv33 = sext i8 %7 to i32
  %conv4 = trunc i32 %conv33 to i8
  store i8 %conv4, i8* %arrayidx1, align 1
  %8 = load i8, i8* %i_6, align 1
  %conv35 = sext i8 %8 to i32
  %add = add i32 %conv35, 1
  %conv6 = trunc i32 %add to i8
  store i8 %conv6, i8* %i_6, align 1
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %9 = load i8, i8* %i_6, align 1
  %arrayidx7 = getelementptr [100 x i8], [100 x i8]* %s2_5, i32 0, i8 %9
  %conv8 = trunc i32 0 to i8
  store i8 %conv8, i8* %arrayidx7, align 1
  %10 = bitcast i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.2, i32 0, i32 0) to i8*
  %arrayAddress9 = getelementptr [100 x i8], [100 x i8]* %s2_5, i32 0, i32 0
  %11 = bitcast i8* %arrayAddress9 to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %10, i8* %11)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
