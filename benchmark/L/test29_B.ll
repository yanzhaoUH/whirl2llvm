; ModuleID = 'test29.bc'

@.str = private constant [30 x i8] c"Enter a three digit integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [27 x i8] c"%d is an Armstrong number.\00", align 1
@.str.3 = private constant [31 x i8] c"%d is not an Armstrong number.\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_8 = alloca i32, align 4
  %_temp_dummy11_9 = alloca i32, align 4
  %_temp_dummy12_10 = alloca i32, align 4
  %_temp_dummy13_11 = alloca i32, align 4
  %number_4 = alloca i32, align 4
  %originalNumber_5 = alloca i32, align 4
  %remainder_6 = alloca i32, align 4
  %result_7 = alloca i32, align 4
  store i32 0, i32* %result_7, align 4
  %0 = bitcast i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %number_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = load i32, i32* %number_4, align 4
  store i32 %3, i32* %originalNumber_5, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %4 = load i32, i32* %originalNumber_5, align 4
  %cmp1 = icmp ne i32 %4, 0
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = load i32, i32* %originalNumber_5, align 4
  %srem = srem i32 %5, 10
  store i32 %srem, i32* %remainder_6, align 4
  %6 = load i32, i32* %remainder_6, align 4
  %7 = load i32, i32* %remainder_6, align 4
  %mul = mul i32 %6, %7
  %8 = load i32, i32* %remainder_6, align 4
  %mul.1 = mul i32 %mul, %8
  %9 = load i32, i32* %result_7, align 4
  %add = add i32 %mul.1, %9
  store i32 %add, i32* %result_7, align 4
  %10 = load i32, i32* %originalNumber_5, align 4
  %div = sdiv i32 %10, 10
  store i32 %div, i32* %originalNumber_5, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %11 = load i32, i32* %result_7, align 4
  %12 = load i32, i32* %number_4, align 4
  %cmp2 = icmp eq i32 %11, %12
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %while.end
  %13 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.2, i32 0, i32 0) to i8*
  %14 = load i32, i32* %number_4, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %13, i32 %14)
  br label %if.end

if.else:                                          ; preds = %while.end
  %15 = bitcast i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.3, i32 0, i32 0) to i8*
  %16 = load i32, i32* %number_4, align 4
  %call4 = call i32 (i8*, ...) @printf(i8* %15, i32 %16)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
