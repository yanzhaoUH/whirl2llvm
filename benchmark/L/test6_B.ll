; ModuleID = 'test6.bc'

@.str = private constant [24 x i8] c"Size of int: %ld bytes\0A\00", align 1
@.str.1 = private constant [26 x i8] c"Size of float: %ld bytes\0A\00", align 1
@.str.2 = private constant [27 x i8] c"Size of double: %ld bytes\0A\00", align 1
@.str.3 = private constant [24 x i8] c"Size of char: %ld byte\0A\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_8 = alloca i32, align 4
  %_temp_dummy11_9 = alloca i32, align 4
  %_temp_dummy12_10 = alloca i32, align 4
  %_temp_dummy13_11 = alloca i32, align 4
  %charType_7 = alloca i8, align 1
  %doubleType_6 = alloca double, align 8
  %floatType_5 = alloca float, align 4
  %integerType_4 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0, i64 4)
  %1 = bitcast i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call2 = call i32 (i8*, ...) @printf(i8* %1, i64 4)
  %2 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %2, i64 8)
  %3 = bitcast i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.3, i32 0, i32 0) to i8*
  %call4 = call i32 (i8*, ...) @printf(i8* %3, i64 1)
  ret i32 0
}

declare i32 @printf(i8*, ...)
