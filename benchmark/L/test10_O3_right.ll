; ModuleID = 'test10.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [20 x i8] c"Enter an alphabet: \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%c\00", align 1
@.str.2 = private unnamed_addr constant [15 x i8] c"%c is a vowel.\00", align 1
@.str.3 = private unnamed_addr constant [19 x i8] c"%c is a consonant.\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %c = alloca i8, align 1
  call void @llvm.lifetime.start(i64 1, i8* nonnull %c) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i8* nonnull %c) #1
  %0 = load i8, i8* %c, align 1, !tbaa !1
  %switch.tableidx = add i8 %0, -97
  %1 = icmp ult i8 %switch.tableidx, 21
  %switch.cast = zext i8 %switch.tableidx to i21
  %switch.downshift = lshr i21 -1031919, %switch.cast
  %2 = and i21 %switch.downshift, 1
  %switch.masked = icmp ne i21 %2, 0
  %3 = and i1 %1, %switch.masked
  %switch.tableidx48 = add i8 %0, -65
  %4 = icmp ult i8 %switch.tableidx48, 21
  %switch.cast49 = zext i8 %switch.tableidx48 to i21
  %switch.downshift51 = lshr i21 -1031919, %switch.cast49
  %5 = and i21 %switch.downshift51, 1
  %switch.masked52 = icmp ne i21 %5, 0
  %6 = and i1 %4, %switch.masked52
  %or.cond = or i1 %3, %6
  %conv40 = sext i8 %0 to i32
  br i1 %or.cond, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %call41 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i64 0, i64 0), i32 %conv40) #1
  br label %if.end

if.else:                                          ; preds = %entry
  %call43 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.3, i64 0, i64 0), i32 %conv40) #1
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  call void @llvm.lifetime.end(i64 1, i8* nonnull %c) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"omnipotent char", !3, i64 0}
!3 = !{!"Simple C/C++ TBAA"}
