; ModuleID = 'test35.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [27 x i8] c"Enter a positive integer: \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.2 = private unnamed_addr constant [23 x i8] c"%d is a prime number.\0A\00", align 1
@.str.3 = private unnamed_addr constant [27 x i8] c"%d is not a prime number.\0A\00", align 1
@.str.4 = private unnamed_addr constant [27 x i8] c"%d is an Armstrong number.\00", align 1
@.str.5 = private unnamed_addr constant [31 x i8] c"%d is not an Armstrong number.\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %n = alloca i32, align 4
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start(i64 4, i8* %0) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %n) #1
  %1 = load i32, i32* %n, align 4, !tbaa !1
  %div.i = sdiv i32 %1, 2
  %cmp.7.i = icmp slt i32 %1, 4
  br i1 %cmp.7.i, label %if.then, label %for.body.i.preheader

for.body.i.preheader:                             ; preds = %entry
  br label %for.body.i

for.cond.i:                                       ; preds = %for.body.i
  %inc.i = add nuw nsw i32 %i.08.i, 1
  %cmp.i = icmp slt i32 %i.08.i, %div.i
  br i1 %cmp.i, label %for.body.i, label %if.then.loopexit

for.body.i:                                       ; preds = %for.body.i.preheader, %for.cond.i
  %i.08.i = phi i32 [ %inc.i, %for.cond.i ], [ 2, %for.body.i.preheader ]
  %rem.i = srem i32 %1, %i.08.i
  %cmp1.i = icmp eq i32 %rem.i, 0
  br i1 %cmp1.i, label %if.else, label %for.cond.i

if.then.loopexit:                                 ; preds = %for.cond.i
  br label %if.then

if.then:                                          ; preds = %if.then.loopexit, %entry
  %call3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.2, i64 0, i64 0), i32 %1) #1
  br label %if.end

if.else:                                          ; preds = %for.body.i
  %call4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.3, i64 0, i64 0), i32 %1) #1
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %2 = load i32, i32* %n, align 4, !tbaa !1
  %cmp.27.i = icmp eq i32 %2, 0
  br i1 %cmp.27.i, label %checkArmstrongNumber.exit, label %while.body.i.preheader

while.body.i.preheader:                           ; preds = %if.end
  br label %while.body.i

while.body.3.i.preheader:                         ; preds = %while.body.i
  %inc.i.15.lcssa = phi i32 [ %inc.i.15, %while.body.i ]
  %phitmp.i = sitofp i32 %inc.i.15.lcssa to double
  br label %while.body.3.i

while.body.i:                                     ; preds = %while.body.i.preheader, %while.body.i
  %n.029.i = phi i32 [ %inc.i.15, %while.body.i ], [ 0, %while.body.i.preheader ]
  %originalNumber.028.i = phi i32 [ %div.i.14, %while.body.i ], [ %2, %while.body.i.preheader ]
  %div.i.14 = sdiv i32 %originalNumber.028.i, 10
  %inc.i.15 = add nuw nsw i32 %n.029.i, 1
  %originalNumber.028.off.i = add i32 %originalNumber.028.i, 9
  %3 = icmp ult i32 %originalNumber.028.off.i, 19
  br i1 %3, label %while.body.3.i.preheader, label %while.body.i

while.body.3.i:                                   ; preds = %while.body.3.i.preheader, %while.body.3.i
  %result.026.i = phi i32 [ %conv6.i, %while.body.3.i ], [ 0, %while.body.3.i.preheader ]
  %originalNumber.125.i = phi i32 [ %div7.i, %while.body.3.i ], [ %2, %while.body.3.i.preheader ]
  %rem.i.16 = srem i32 %originalNumber.125.i, 10
  %conv.i = sitofp i32 %rem.i.16 to double
  %call.i = call double @pow(double %conv.i, double %phitmp.i) #1
  %conv5.i = sitofp i32 %result.026.i to double
  %add.i = fadd double %conv5.i, %call.i
  %conv6.i = fptosi double %add.i to i32
  %div7.i = sdiv i32 %originalNumber.125.i, 10
  %originalNumber.125.off.i = add i32 %originalNumber.125.i, 9
  %4 = icmp ult i32 %originalNumber.125.off.i, 19
  br i1 %4, label %checkArmstrongNumber.exit.loopexit, label %while.body.3.i

checkArmstrongNumber.exit.loopexit:               ; preds = %while.body.3.i
  %conv6.i.lcssa = phi i32 [ %conv6.i, %while.body.3.i ]
  %.pre = load i32, i32* %n, align 4, !tbaa !1
  br label %checkArmstrongNumber.exit

checkArmstrongNumber.exit:                        ; preds = %checkArmstrongNumber.exit.loopexit, %if.end
  %5 = phi i32 [ 0, %if.end ], [ %.pre, %checkArmstrongNumber.exit.loopexit ]
  %result.0.lcssa.i = phi i32 [ 0, %if.end ], [ %conv6.i.lcssa, %checkArmstrongNumber.exit.loopexit ]
  %cmp9.i = icmp eq i32 %result.0.lcssa.i, %2
  br i1 %cmp9.i, label %if.then.7, label %if.else.9

if.then.7:                                        ; preds = %checkArmstrongNumber.exit
  %call8 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.4, i64 0, i64 0), i32 %5) #1
  br label %if.end.11

if.else.9:                                        ; preds = %checkArmstrongNumber.exit
  %call10 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.5, i64 0, i64 0), i32 %5) #1
  br label %if.end.11

if.end.11:                                        ; preds = %if.else.9, %if.then.7
  call void @llvm.lifetime.end(i64 4, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind readnone uwtable
define i32 @checkPrimeNumber(i32 %n) #3 {
entry:
  %div = sdiv i32 %n, 2
  %cmp.7 = icmp slt i32 %n, 4
  br i1 %cmp.7, label %for.end, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  br label %for.body

for.cond:                                         ; preds = %for.body
  %inc = add nuw nsw i32 %i.08, 1
  %cmp = icmp slt i32 %i.08, %div
  br i1 %cmp, label %for.body, label %for.end.loopexit

for.body:                                         ; preds = %for.body.preheader, %for.cond
  %i.08 = phi i32 [ %inc, %for.cond ], [ 2, %for.body.preheader ]
  %rem = srem i32 %n, %i.08
  %cmp1 = icmp eq i32 %rem, 0
  br i1 %cmp1, label %for.end.loopexit, label %for.cond

for.end.loopexit:                                 ; preds = %for.body, %for.cond
  %flag.0.ph = phi i32 [ 1, %for.cond ], [ 0, %for.body ]
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  %flag.0 = phi i32 [ 1, %entry ], [ %flag.0.ph, %for.end.loopexit ]
  ret i32 %flag.0
}

; Function Attrs: nounwind uwtable
define i32 @checkArmstrongNumber(i32 %number) #0 {
entry:
  %cmp.27 = icmp eq i32 %number, 0
  br i1 %cmp.27, label %while.end.8, label %while.body.preheader

while.body.preheader:                             ; preds = %entry
  br label %while.body

while.cond.1.preheader:                           ; preds = %while.body
  %inc.lcssa = phi i32 [ %inc, %while.body ]
  %phitmp = sitofp i32 %inc.lcssa to double
  br i1 %cmp.27, label %while.end.8, label %while.body.3.preheader

while.body.3.preheader:                           ; preds = %while.cond.1.preheader
  br label %while.body.3

while.body:                                       ; preds = %while.body.preheader, %while.body
  %n.029 = phi i32 [ %inc, %while.body ], [ 0, %while.body.preheader ]
  %originalNumber.028 = phi i32 [ %div, %while.body ], [ %number, %while.body.preheader ]
  %div = sdiv i32 %originalNumber.028, 10
  %inc = add nuw nsw i32 %n.029, 1
  %originalNumber.028.off = add i32 %originalNumber.028, 9
  %0 = icmp ult i32 %originalNumber.028.off, 19
  br i1 %0, label %while.cond.1.preheader, label %while.body

while.body.3:                                     ; preds = %while.body.3.preheader, %while.body.3
  %result.026 = phi i32 [ %conv6, %while.body.3 ], [ 0, %while.body.3.preheader ]
  %originalNumber.125 = phi i32 [ %div7, %while.body.3 ], [ %number, %while.body.3.preheader ]
  %rem = srem i32 %originalNumber.125, 10
  %conv = sitofp i32 %rem to double
  %call = tail call double @pow(double %conv, double %phitmp) #1
  %conv5 = sitofp i32 %result.026 to double
  %add = fadd double %conv5, %call
  %conv6 = fptosi double %add to i32
  %div7 = sdiv i32 %originalNumber.125, 10
  %originalNumber.125.off = add i32 %originalNumber.125, 9
  %1 = icmp ult i32 %originalNumber.125.off, 19
  br i1 %1, label %while.end.8.loopexit, label %while.body.3

while.end.8.loopexit:                             ; preds = %while.body.3
  %conv6.lcssa = phi i32 [ %conv6, %while.body.3 ]
  br label %while.end.8

while.end.8:                                      ; preds = %while.end.8.loopexit, %entry, %while.cond.1.preheader
  %result.0.lcssa = phi i32 [ 0, %while.cond.1.preheader ], [ 0, %entry ], [ %conv6.lcssa, %while.end.8.loopexit ]
  %cmp9 = icmp eq i32 %result.0.lcssa, %number
  %. = zext i1 %cmp9 to i32
  ret i32 %.
}

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare double @pow(double, double) #2

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readnone uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
