; ModuleID = 'test335_O.bc'

%struct.sTest = type { i32, float }

@.str = private constant [4 x i8] c"%d\0A\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_ehpit0_10 = alloca i32, align 4
  %a_4 = alloca [4 x i32], align 4
  %old_frame_pointer_15.addr = alloca i64, align 8
  %p_8 = alloca i32*, align 4
  %return_address_16.addr = alloca i64, align 8
  %s1_6 = alloca %struct.sTest, align 4
  %0 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0, i32 26)
  %1 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0) to i8*
  %call2 = call i32 (i8*, ...) @printf(i8* %1, i32 10)
  %2 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %2, i32 66)
  ret i32 0
}

declare i32 @printf(i8*, ...)
