; ModuleID = 'test64.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.complex = type { float, float }

@.str.2 = private unnamed_addr constant [6 x i8] c"%f %f\00", align 1
@.str.4 = private unnamed_addr constant [19 x i8] c"Sum = %.1f + %.1fi\00", align 1
@str = private unnamed_addr constant [24 x i8] c"For 1st complex number \00"
@str.6 = private unnamed_addr constant [25 x i8] c"\0AFor 2nd complex number \00"
@str.7 = private unnamed_addr constant [44 x i8] c"Enter real and imaginary part respectively:\00"

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %n1 = alloca <2 x float>, align 8
  %tmpcast = bitcast <2 x float>* %n1 to %struct.complex*
  %n2 = alloca <2 x float>, align 8
  %tmpcast17 = bitcast <2 x float>* %n2 to %struct.complex*
  %0 = bitcast <2 x float>* %n1 to i8*
  call void @llvm.lifetime.start(i64 8, i8* %0) #1
  %1 = bitcast <2 x float>* %n2 to i8*
  call void @llvm.lifetime.start(i64 8, i8* %1) #1
  %puts = tail call i32 @puts(i8* getelementptr inbounds ([24 x i8], [24 x i8]* @str, i64 0, i64 0))
  %puts14 = tail call i32 @puts(i8* getelementptr inbounds ([44 x i8], [44 x i8]* @str.7, i64 0, i64 0))
  %imag = getelementptr inbounds %struct.complex, %struct.complex* %tmpcast, i64 0, i32 1
  %call2 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.2, i64 0, i64 0), <2 x float>* nonnull %n1, float* %imag) #1
  %puts15 = call i32 @puts(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @str.6, i64 0, i64 0))
  %puts16 = call i32 @puts(i8* getelementptr inbounds ([44 x i8], [44 x i8]* @str.7, i64 0, i64 0))
  %imag6 = getelementptr inbounds %struct.complex, %struct.complex* %tmpcast17, i64 0, i32 1
  %call7 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.2, i64 0, i64 0), <2 x float>* nonnull %n2, float* %imag6) #1
  %2 = load <2 x float>, <2 x float>* %n1, align 8
  %3 = load <2 x float>, <2 x float>* %n2, align 8
  %n1.sroa.0.0.vec.extract.i = extractelement <2 x float> %2, i32 0
  %n2.sroa.0.0.vec.extract.i = extractelement <2 x float> %3, i32 0
  %add.i = fadd float %n1.sroa.0.0.vec.extract.i, %n2.sroa.0.0.vec.extract.i
  %n1.sroa.0.4.vec.extract.i = extractelement <2 x float> %2, i32 1
  %n2.sroa.0.4.vec.extract.i = extractelement <2 x float> %3, i32 1
  %add4.i = fadd float %n1.sroa.0.4.vec.extract.i, %n2.sroa.0.4.vec.extract.i
  %conv = fpext float %add.i to double
  %conv11 = fpext float %add4.i to double
  %call12 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.4, i64 0, i64 0), double %conv, double %conv11) #1
  call void @llvm.lifetime.end(i64 8, i8* %1) #1
  call void @llvm.lifetime.end(i64 8, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind readnone uwtable
define <2 x float> @add(<2 x float> %n1.coerce, <2 x float> %n2.coerce) #3 {
entry:
  %0 = fadd <2 x float> %n1.coerce, %n2.coerce
  ret <2 x float> %0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @puts(i8* nocapture readonly) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readnone uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
