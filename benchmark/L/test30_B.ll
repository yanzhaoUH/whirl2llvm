; ModuleID = 'test30.bc'

@.str = private constant [31 x i8] c"Enter two numbers(intervals): \00", align 1
@.str.1 = private constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private constant [41 x i8] c"Armstrong numbers between %d an %d are: \00", align 1
@.str.3 = private constant [4 x i8] c"%d \00", align 1
@_LIB_VERSION_58 = common global i32 0, align 4
@signgam_59 = common global i32 0, align 4

define i32 @main() {
entry:
  %_temp_dummy10_12 = alloca i32, align 4
  %_temp_dummy11_13 = alloca i32, align 4
  %_temp_dummy12_14 = alloca i32, align 4
  %_temp_dummy13_15 = alloca i32, align 4
  %i_6 = alloca i32, align 4
  %n1_4 = alloca i32, align 4
  %n2_5 = alloca i32, align 4
  %n_10 = alloca i32, align 4
  %remainder_9 = alloca i32, align 4
  %result_11 = alloca i32, align 4
  %temp1_7 = alloca i32, align 4
  %temp2_8 = alloca i32, align 4
  store i32 0, i32* %n_10, align 4
  store i32 0, i32* %result_11, align 4
  %0 = bitcast i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n1_4 to i32*
  %3 = bitcast i32* %n2_5 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2, i32* %3)
  %4 = bitcast i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.2, i32 0, i32 0) to i8*
  %5 = load i32, i32* %n1_4, align 4
  %6 = load i32, i32* %n2_5, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %4, i32 %5, i32 %6)
  %7 = load i32, i32* %n1_4, align 4
  %add = add i32 %7, 1
  store i32 %add, i32* %i_6, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %8 = load i32, i32* %i_6, align 4
  %9 = load i32, i32* %n2_5, align 4
  %cmp1 = icmp slt i32 %8, %9
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %10 = load i32, i32* %i_6, align 4
  store i32 %10, i32* %temp2_8, align 4
  %11 = load i32, i32* %i_6, align 4
  store i32 %11, i32* %temp1_7, align 4
  br label %while.cond1

while.end:                                        ; preds = %while.cond
  ret i32 0

while.cond1:                                      ; preds = %while.body2, %while.body
  %12 = load i32, i32* %temp1_7, align 4
  %cmp2 = icmp ne i32 %12, 0
  br i1 %cmp2, label %while.body2, label %while.end3

while.body2:                                      ; preds = %while.cond1
  %13 = load i32, i32* %temp1_7, align 4
  %div = sdiv i32 %13, 10
  store i32 %div, i32* %temp1_7, align 4
  %14 = load i32, i32* %n_10, align 4
  %add.4 = add i32 %14, 1
  store i32 %add.4, i32* %n_10, align 4
  br label %while.cond1

while.end3:                                       ; preds = %while.cond1
  br label %while.cond5

while.cond5:                                      ; preds = %while.body6, %while.end3
  %15 = load i32, i32* %temp2_8, align 4
  %cmp3 = icmp ne i32 %15, 0
  br i1 %cmp3, label %while.body6, label %while.end7

while.body6:                                      ; preds = %while.cond5
  %16 = load i32, i32* %temp2_8, align 4
  %srem = srem i32 %16, 10
  store i32 %srem, i32* %remainder_9, align 4
  %17 = load i32, i32* %result_11, align 4
  %conv9 = sitofp i32 %17 to double
  %18 = load i32, i32* %remainder_9, align 4
  %conv98 = sitofp i32 %18 to double
  %19 = load i32, i32* %n_10, align 4
  %conv99 = sitofp i32 %19 to double
  %call4 = call double @pow(double %conv98, double %conv99)
  %add.10 = fadd double %conv9, %call4
  %fp2int = fptosi double %add.10 to i32
  store i32 %fp2int, i32* %result_11, align 4
  %20 = load i32, i32* %temp2_8, align 4
  %div.11 = sdiv i32 %20, 10
  store i32 %div.11, i32* %temp2_8, align 4
  br label %while.cond5

while.end7:                                       ; preds = %while.cond5
  %21 = load i32, i32* %result_11, align 4
  %22 = load i32, i32* %i_6, align 4
  %cmp4 = icmp eq i32 %21, %22
  br i1 %cmp4, label %if.then, label %if.else

if.then:                                          ; preds = %while.end7
  %23 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.3, i32 0, i32 0) to i8*
  %24 = load i32, i32* %i_6, align 4
  %call5 = call i32 (i8*, ...) @printf(i8* %23, i32 %24)
  br label %if.end

if.else:                                          ; preds = %while.end7
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  store i32 0, i32* %n_10, align 4
  store i32 0, i32* %result_11, align 4
  %25 = load i32, i32* %i_6, align 4
  %add.12 = add i32 %25, 1
  store i32 %add.12, i32* %i_6, align 4
  br label %while.cond
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

declare double @pow(double, double)
