; ModuleID = 'test43.bc'

@.str = private constant [19 x i8] c"Enter a sentence: \00", align 1
@.str.1 = private constant [3 x i8] c"%c\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_4 = alloca i32, align 4
  %_temp_dummy11_5 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  call void @_Z7Reversev()
  ret i32 0
}

declare i32 @printf(i8*, ...)

define void @_Z7Reversev() {
entry:
  %_temp_dummy12_5 = alloca i32, align 4
  %_temp_dummy13_6 = alloca i32, align 4
  %_temp_dummy14_7 = alloca i32, align 4
  %c_4 = alloca i8, align 1
  %0 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %1 = bitcast i8* %c_4 to i8*
  %call3 = call i32 (i8*, ...) @scanf(i8* %0, i8* %1)
  %2 = load i8, i8* %c_4, align 1
  %conv3 = sext i8 %2 to i32
  %cmp1 = icmp ne i32 %conv3, 10
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  call void @_Z7Reversev()
  %3 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %4 = load i8, i8* %c_4, align 1
  %conv31 = sext i8 %4 to i32
  %call5 = call i32 (i8*, ...) @printf(i8* %3, i32 %conv31)
  br label %if.end

if.else:                                          ; preds = %entry
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

declare i32 @scanf(i8*, ...)
