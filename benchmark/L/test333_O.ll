; ModuleID = 'test333_O.bc'

@.str = private constant [21 x i8] c"go into the default\0A\00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_65 = alloca i32
  %.preg_I4_4_66 = alloca i32
  %.preg_V16I4_28_60 = alloca <4 x i32>
  %_temp__switch_index0_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_10 = alloca i32, align 4
  %_temp_ehpit0_18 = alloca i32, align 4
  %a_4 = alloca i32, align 4
  %b_5 = alloca i32, align 4
  %i_8 = alloca i32, align 4
  %old_frame_pointer_23.addr = alloca i64, align 8
  %return_address_24.addr = alloca i64, align 8
  %sum_9 = alloca i32, align 4
  %x_14.addr = alloca i32, align 4
  %x_16 = alloca i32, align 4
  %y_15.addr = alloca i32, align 4
  %y_17 = alloca i32, align 4
  %add = add <4 x i32> zeroinitializer, zeroinitializer
  store <4 x i32> %add, <4 x i32>* %.preg_V16I4_28_60, align 8
  %0 = load <4 x i32>, <4 x i32>* %.preg_V16I4_28_60
  %rdx.shuf = shufflevector <4 x i32> %0, <4 x i32> undef, <4 x i32> <i32 2, i32 3, i32 undef, i32 undef>
  %bin.rdx1 = add <4 x i32> %0, %rdx.shuf
  %rdx.shuf1 = shufflevector <4 x i32> %bin.rdx1, <4 x i32> undef, <4 x i32> <i32 1, i32 undef, i32 undef, i32 undef>
  %bin.rdx2 = add <4 x i32> %bin.rdx1, %rdx.shuf1
  %1 = extractelement <4 x i32> %bin.rdx2, i32 0
  %add.1 = add i32 %1, 5
  store i32 %add.1, i32* %.preg_I4_4_66, align 8
  %2 = load i32, i32* %.preg_I4_4_66
  %3 = load i32, i32* %.preg_I4_4_66
  %add.2 = sub i32 20, %3
  %4 = icmp sgt i32 %add.2, 1
  br i1 %4, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %.preg_I4_4_66
  %add.3 = sub i32 20, %5
  br label %if.end

if.else:                                          ; preds = %entry
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %6 = phi i32 [ %add.3, %if.then ], [ 1, %if.else ]
  %add.4 = add i32 %2, %6
  %add.5 = add i32 %add.4, 15
  store i32 %add.5, i32* %.preg_I4_4_65, align 8
  %7 = load i32, i32* %.preg_I4_4_65
  %cmp1 = icmp sgt i32 %7, 0
  br i1 %cmp1, label %tb, label %L10242

tb:                                               ; preds = %if.end
  %8 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %9 = load i32, i32* %.preg_I4_4_65
  %call1 = call i32 (i8*, ...) @printf(i8* %8, i32 %9)
  br label %L10242

L10242:                                           ; preds = %tb, %if.end
  ret i32 0
}

declare i32 @printf(i8*, ...)

define i32 @_Z3Sumii(i32 %x_4, i32 %y_5) {
entry:
  %.preg_I4_4_50 = alloca i32
  %.preg_I4_4_49 = alloca i32
  %old_frame_pointer_10.addr = alloca i64, align 8
  %return_address_11.addr = alloca i64, align 8
  %x_4.addr = alloca i32, align 4
  %y_5.addr = alloca i32, align 4
  store i32 %x_4, i32* %x_4.addr, align 4
  store i32 %y_5, i32* %y_5.addr, align 4
  store i32 %x_4, i32* %.preg_I4_4_49, align 8
  store i32 %y_5, i32* %.preg_I4_4_50, align 8
  %0 = load i32, i32* %.preg_I4_4_49
  %1 = load i32, i32* %.preg_I4_4_50
  %add = add i32 %0, %1
  ret i32 %add
}
