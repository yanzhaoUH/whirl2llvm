; ModuleID = 'test39.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [30 x i8] c"Enter two positive integers: \00", align 1
@.str.1 = private unnamed_addr constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private unnamed_addr constant [26 x i8] c"G.C.D of %d and %d is %d.\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %n1 = alloca i32, align 4
  %n2 = alloca i32, align 4
  %0 = bitcast i32* %n1 to i8*
  call void @llvm.lifetime.start(i64 4, i8* %0) #1
  %1 = bitcast i32* %n2 to i8*
  call void @llvm.lifetime.start(i64 4, i8* %1) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %n1, i32* nonnull %n2) #1
  %2 = load i32, i32* %n1, align 4, !tbaa !1
  %3 = load i32, i32* %n2, align 4, !tbaa !1
  %cmp.4.i = icmp eq i32 %3, 0
  br i1 %cmp.4.i, label %hcf.exit, label %if.then.i.preheader

if.then.i.preheader:                              ; preds = %entry
  br label %if.then.i

if.then.i:                                        ; preds = %if.then.i.preheader, %if.then.i
  %n2.tr6.i = phi i32 [ %rem.i, %if.then.i ], [ %3, %if.then.i.preheader ]
  %n1.tr5.i = phi i32 [ %n2.tr6.i, %if.then.i ], [ %2, %if.then.i.preheader ]
  %rem.i = srem i32 %n1.tr5.i, %n2.tr6.i
  %cmp.i = icmp eq i32 %rem.i, 0
  br i1 %cmp.i, label %hcf.exit.loopexit, label %if.then.i

hcf.exit.loopexit:                                ; preds = %if.then.i
  %n2.tr6.i.lcssa = phi i32 [ %n2.tr6.i, %if.then.i ]
  br label %hcf.exit

hcf.exit:                                         ; preds = %hcf.exit.loopexit, %entry
  %n1.tr.lcssa.i = phi i32 [ %2, %entry ], [ %n2.tr6.i.lcssa, %hcf.exit.loopexit ]
  %call3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.2, i64 0, i64 0), i32 %2, i32 %3, i32 %n1.tr.lcssa.i) #1
  call void @llvm.lifetime.end(i64 4, i8* %1) #1
  call void @llvm.lifetime.end(i64 4, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind readnone uwtable
define i32 @hcf(i32 %n1, i32 %n2) #3 {
entry:
  %cmp.4 = icmp eq i32 %n2, 0
  br i1 %cmp.4, label %return, label %if.then.preheader

if.then.preheader:                                ; preds = %entry
  br label %if.then

if.then:                                          ; preds = %if.then.preheader, %if.then
  %n2.tr6 = phi i32 [ %rem, %if.then ], [ %n2, %if.then.preheader ]
  %n1.tr5 = phi i32 [ %n2.tr6, %if.then ], [ %n1, %if.then.preheader ]
  %rem = srem i32 %n1.tr5, %n2.tr6
  %cmp = icmp eq i32 %rem, 0
  br i1 %cmp, label %return.loopexit, label %if.then

return.loopexit:                                  ; preds = %if.then
  %n2.tr6.lcssa = phi i32 [ %n2.tr6, %if.then ]
  br label %return

return:                                           ; preds = %return.loopexit, %entry
  %n1.tr.lcssa = phi i32 [ %n1, %entry ], [ %n2.tr6.lcssa, %return.loopexit ]
  ret i32 %n1.tr.lcssa
}

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readnone uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
