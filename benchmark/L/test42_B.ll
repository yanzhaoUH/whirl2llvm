; ModuleID = 'test42.bc'

@.str = private constant [15 x i8] c"Instructions:\0A\00", align 1
@.str.1 = private constant [51 x i8] c"1. Enter alphabet 'o' to convert binary to octal.\0A\00", align 1
@.str.2 = private constant [51 x i8] c"2. Enter alphabet 'b' to convert octal to binary.\0A\00", align 1
@.str.3 = private constant [3 x i8] c"%c\00", align 1
@.str.4 = private constant [24 x i8] c"Enter a binary number: \00", align 1
@.str.5 = private constant [3 x i8] c"%d\00", align 1
@.str.6 = private constant [27 x i8] c"%d in binary = %d in octal\00", align 1
@.str.7 = private constant [23 x i8] c"Enter a octal number: \00", align 1
@.str.8 = private constant [27 x i8] c"%d in octal = %d in binary\00", align 1
@_LIB_VERSION_67 = common global i32 0, align 4
@signgam_68 = common global i32 0, align 4

define i32 @main() {
entry:
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %_temp_dummy13_9 = alloca i32, align 4
  %_temp_dummy14_10 = alloca i32, align 4
  %_temp_dummy15_11 = alloca i32, align 4
  %_temp_dummy16_12 = alloca i32, align 4
  %_temp_dummy17_13 = alloca i32, align 4
  %_temp_dummy18_14 = alloca i32, align 4
  %_temp_dummy19_15 = alloca i32, align 4
  %c_5 = alloca i8, align 1
  %n_4 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([51 x i8], [51 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call2 = call i32 (i8*, ...) @printf(i8* %1)
  %2 = bitcast i8* getelementptr inbounds ([51 x i8], [51 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %2)
  %3 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.3, i32 0, i32 0) to i8*
  %4 = bitcast i8* %c_5 to i8*
  %call4 = call i32 (i8*, ...) @scanf(i8* %3, i8* %4)
  %5 = load i8, i8* %c_5, align 1
  %conv3 = sext i8 %5 to i32
  %cmp1 = icmp eq i32 %conv3, 111
  br i1 %cmp1, label %lor.end, label %lor.rhs

if.then:                                          ; preds = %lor.end
  %6 = bitcast i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.4, i32 0, i32 0) to i8*
  %call5 = call i32 (i8*, ...) @printf(i8* %6)
  %7 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.5, i32 0, i32 0) to i8*
  %8 = bitcast i32* %n_4 to i32*
  %call6 = call i32 (i8*, ...) @scanf(i8* %7, i32* %8)
  %9 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.6, i32 0, i32 0) to i8*
  %10 = load i32, i32* %n_4, align 4
  %11 = load i32, i32* %n_4, align 4
  %call7 = call i32 @_Z12binary_octali(i32 %11)
  %call8 = call i32 (i8*, ...) @printf(i8* %9, i32 %10, i32 %call7)
  br label %if.end

if.else:                                          ; preds = %lor.end
  br label %if.end

lor.rhs:                                          ; preds = %entry
  %12 = load i8, i8* %c_5, align 1
  %conv31 = sext i8 %12 to i32
  %cmp2 = icmp eq i32 %conv31, 79
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %13 = phi i1 [ true, %entry ], [ %cmp2, %lor.rhs ]
  br i1 %13, label %if.then, label %if.else

if.end:                                           ; preds = %if.else, %if.then
  %14 = load i8, i8* %c_5, align 1
  %conv36 = sext i8 %14 to i32
  %cmp3 = icmp eq i32 %conv36, 98
  br i1 %cmp3, label %lor.end5, label %lor.rhs4

if.then2:                                         ; preds = %lor.end5
  %15 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.7, i32 0, i32 0) to i8*
  %call9 = call i32 (i8*, ...) @printf(i8* %15)
  %16 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.5, i32 0, i32 0) to i8*
  %17 = bitcast i32* %n_4 to i32*
  %call10 = call i32 (i8*, ...) @scanf(i8* %16, i32* %17)
  %18 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.8, i32 0, i32 0) to i8*
  %19 = load i32, i32* %n_4, align 4
  %20 = load i32, i32* %n_4, align 4
  %call11 = call i32 @_Z12octal_binaryi(i32 %20)
  %call12 = call i32 (i8*, ...) @printf(i8* %18, i32 %19, i32 %call11)
  br label %if.end8

if.else3:                                         ; preds = %lor.end5
  br label %if.end8

lor.rhs4:                                         ; preds = %if.end
  %21 = load i8, i8* %c_5, align 1
  %conv37 = sext i8 %21 to i32
  %cmp4 = icmp eq i32 %conv37, 66
  br label %lor.end5

lor.end5:                                         ; preds = %lor.rhs4, %if.end
  %22 = phi i1 [ true, %if.end ], [ %cmp4, %lor.rhs4 ]
  br i1 %22, label %if.then2, label %if.else3

if.end8:                                          ; preds = %if.else3, %if.then2
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define i32 @_Z12binary_octali(i32 %n_4) {
entry:
  %decimal_6 = alloca i32, align 4
  %i_7 = alloca i32, align 4
  %n_4.addr = alloca i32, align 4
  %octal_5 = alloca i32, align 4
  store i32 %n_4, i32* %n_4.addr, align 4
  store i32 0, i32* %octal_5, align 4
  store i32 0, i32* %decimal_6, align 4
  store i32 0, i32* %i_7, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %n_4.addr, align 4
  %cmp7 = icmp ne i32 %0, 0
  br i1 %cmp7, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load i32, i32* %decimal_6, align 4
  %conv9 = sitofp i32 %1 to double
  %2 = load i32, i32* %n_4.addr, align 4
  %srem = srem i32 %2, 10
  %conv91 = sitofp i32 %srem to double
  %3 = load i32, i32* %i_7, align 4
  %conv92 = sitofp i32 %3 to double
  %call14 = call double @pow(double 2.000000e+00, double %conv92)
  %mul = fmul double %conv91, %call14
  %add = fadd double %conv9, %mul
  %fp2int = fptosi double %add to i32
  store i32 %fp2int, i32* %decimal_6, align 4
  %4 = load i32, i32* %i_7, align 4
  %add.3 = add i32 %4, 1
  store i32 %add.3, i32* %i_7, align 4
  %5 = load i32, i32* %n_4.addr, align 4
  %div = sdiv i32 %5, 10
  store i32 %div, i32* %n_4.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 1, i32* %i_7, align 4
  br label %while.cond4

while.cond4:                                      ; preds = %while.body5, %while.end
  %6 = load i32, i32* %decimal_6, align 4
  %cmp8 = icmp ne i32 %6, 0
  br i1 %cmp8, label %while.body5, label %while.end6

while.body5:                                      ; preds = %while.cond4
  %7 = load i32, i32* %decimal_6, align 4
  %srem.7 = srem i32 %7, 8
  %8 = load i32, i32* %i_7, align 4
  %mul.8 = mul i32 %srem.7, %8
  %9 = load i32, i32* %octal_5, align 4
  %add.9 = add i32 %mul.8, %9
  store i32 %add.9, i32* %octal_5, align 4
  %10 = load i32, i32* %decimal_6, align 4
  %div.10 = sdiv i32 %10, 8
  store i32 %div.10, i32* %decimal_6, align 4
  %11 = load i32, i32* %i_7, align 4
  %mul.11 = mul i32 %11, 10
  store i32 %mul.11, i32* %i_7, align 4
  br label %while.cond4

while.end6:                                       ; preds = %while.cond4
  %12 = load i32, i32* %octal_5, align 4
  ret i32 %12
}

define i32 @_Z12octal_binaryi(i32 %n_4) {
entry:
  %binary_6 = alloca i32, align 4
  %decimal_5 = alloca i32, align 4
  %i_7 = alloca i32, align 4
  %n_4.addr = alloca i32, align 4
  store i32 %n_4, i32* %n_4.addr, align 4
  store i32 0, i32* %decimal_5, align 4
  store i32 0, i32* %binary_6, align 4
  store i32 0, i32* %i_7, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %n_4.addr, align 4
  %cmp5 = icmp ne i32 %0, 0
  br i1 %cmp5, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load i32, i32* %decimal_5, align 4
  %conv9 = sitofp i32 %1 to double
  %2 = load i32, i32* %n_4.addr, align 4
  %srem = srem i32 %2, 10
  %conv91 = sitofp i32 %srem to double
  %3 = load i32, i32* %i_7, align 4
  %conv92 = sitofp i32 %3 to double
  %call13 = call double @pow(double 8.000000e+00, double %conv92)
  %mul = fmul double %conv91, %call13
  %add = fadd double %conv9, %mul
  %fp2int = fptosi double %add to i32
  store i32 %fp2int, i32* %decimal_5, align 4
  %4 = load i32, i32* %i_7, align 4
  %add.3 = add i32 %4, 1
  store i32 %add.3, i32* %i_7, align 4
  %5 = load i32, i32* %n_4.addr, align 4
  %div = sdiv i32 %5, 10
  store i32 %div, i32* %n_4.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 1, i32* %i_7, align 4
  br label %while.cond4

while.cond4:                                      ; preds = %while.body5, %while.end
  %6 = load i32, i32* %decimal_5, align 4
  %cmp6 = icmp ne i32 %6, 0
  br i1 %cmp6, label %while.body5, label %while.end6

while.body5:                                      ; preds = %while.cond4
  %7 = load i32, i32* %decimal_5, align 4
  %srem.7 = srem i32 %7, 2
  %8 = load i32, i32* %i_7, align 4
  %mul.8 = mul i32 %srem.7, %8
  %9 = load i32, i32* %binary_6, align 4
  %add.9 = add i32 %mul.8, %9
  store i32 %add.9, i32* %binary_6, align 4
  %10 = load i32, i32* %decimal_5, align 4
  %div.10 = sdiv i32 %10, 2
  store i32 %div.10, i32* %decimal_5, align 4
  %11 = load i32, i32* %i_7, align 4
  %mul.11 = mul i32 %11, 10
  store i32 %mul.11, i32* %i_7, align 4
  br label %while.cond4

while.end6:                                       ; preds = %while.cond4
  %12 = load i32, i32* %binary_6, align 4
  ret i32 %12
}

declare double @pow(double, double)
