; ModuleID = 'test30.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [31 x i8] c"Enter two numbers(intervals): \00", align 1
@.str.1 = private unnamed_addr constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private unnamed_addr constant [41 x i8] c"Armstrong numbers between %d an %d are: \00", align 1
@.str.3 = private unnamed_addr constant [4 x i8] c"%d \00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %n1 = alloca i32, align 4
  %n2 = alloca i32, align 4
  %0 = bitcast i32* %n1 to i8*
  call void @llvm.lifetime.start(i64 4, i8* %0) #1
  %1 = bitcast i32* %n2 to i8*
  call void @llvm.lifetime.start(i64 4, i8* %1) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %n1, i32* nonnull %n2) #1
  %2 = load i32, i32* %n1, align 4, !tbaa !1
  %3 = load i32, i32* %n2, align 4, !tbaa !1
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.2, i64 0, i64 0), i32 %2, i32 %3) #1
  %4 = load i32, i32* %n1, align 4, !tbaa !1
  %i.0.41 = add nsw i32 %4, 1
  %5 = load i32, i32* %n2, align 4, !tbaa !1
  %cmp.42 = icmp slt i32 %i.0.41, %5
  br i1 %cmp.42, label %while.cond.preheader.preheader, label %for.end

while.cond.preheader.preheader:                   ; preds = %entry
  br label %while.cond.preheader

while.cond.preheader:                             ; preds = %while.cond.preheader.preheader, %for.cond.backedge
  %i.043 = phi i32 [ %i.0, %for.cond.backedge ], [ %i.0.41, %while.cond.preheader.preheader ]
  %cmp3.34 = icmp eq i32 %i.043, 0
  br i1 %cmp3.34, label %while.end.13, label %while.body.preheader

while.body.preheader:                             ; preds = %while.cond.preheader
  br label %while.body

while.cond.4.preheader:                           ; preds = %while.body
  %inc.lcssa = phi i32 [ %inc, %while.body ]
  br i1 %cmp3.34, label %while.end.13, label %while.body.6.lr.ph

while.body.6.lr.ph:                               ; preds = %while.cond.4.preheader
  %conv7 = sitofp i32 %inc.lcssa to double
  br label %while.body.6

while.body:                                       ; preds = %while.body.preheader, %while.body
  %n.136 = phi i32 [ %inc, %while.body ], [ 0, %while.body.preheader ]
  %temp1.035 = phi i32 [ %div, %while.body ], [ %i.043, %while.body.preheader ]
  %div = sdiv i32 %temp1.035, 10
  %inc = add nuw nsw i32 %n.136, 1
  %temp1.035.off = add i32 %temp1.035, 9
  %6 = icmp ult i32 %temp1.035.off, 19
  br i1 %6, label %while.cond.4.preheader, label %while.body

while.body.6:                                     ; preds = %while.body.6.lr.ph, %while.body.6
  %result.139 = phi i32 [ 0, %while.body.6.lr.ph ], [ %conv11, %while.body.6 ]
  %temp2.038 = phi i32 [ %i.043, %while.body.6.lr.ph ], [ %div12, %while.body.6 ]
  %rem = srem i32 %temp2.038, 10
  %conv = sitofp i32 %rem to double
  %call8 = call double @pow(double %conv, double %conv7) #1
  %conv9 = sitofp i32 %result.139 to double
  %add10 = fadd double %conv9, %call8
  %conv11 = fptosi double %add10 to i32
  %div12 = sdiv i32 %temp2.038, 10
  %temp2.038.off = add i32 %temp2.038, 9
  %7 = icmp ult i32 %temp2.038.off, 19
  br i1 %7, label %while.end.13.loopexit, label %while.body.6

while.end.13.loopexit:                            ; preds = %while.body.6
  %conv11.lcssa = phi i32 [ %conv11, %while.body.6 ]
  br label %while.end.13

while.end.13:                                     ; preds = %while.end.13.loopexit, %while.cond.preheader, %while.cond.4.preheader
  %result.1.lcssa = phi i32 [ 0, %while.cond.4.preheader ], [ 0, %while.cond.preheader ], [ %conv11.lcssa, %while.end.13.loopexit ]
  %cmp14 = icmp eq i32 %result.1.lcssa, %i.043
  br i1 %cmp14, label %if.then, label %for.cond.backedge

if.then:                                          ; preds = %while.end.13
  %call16 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.3, i64 0, i64 0), i32 %i.043) #1
  br label %for.cond.backedge

for.cond.backedge:                                ; preds = %if.then, %while.end.13
  %i.0 = add nsw i32 %i.043, 1
  %8 = load i32, i32* %n2, align 4, !tbaa !1
  %cmp = icmp slt i32 %i.0, %8
  br i1 %cmp, label %while.cond.preheader, label %for.end.loopexit

for.end.loopexit:                                 ; preds = %for.cond.backedge
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  call void @llvm.lifetime.end(i64 4, i8* %1) #1
  call void @llvm.lifetime.end(i64 4, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare double @pow(double, double) #2

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
