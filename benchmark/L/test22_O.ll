; ModuleID = 'test22_O.bc'

@.str = private constant [4 x i8] c"%c \00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_50 = alloca i32
  %.preg_I4_4_49 = alloca i32
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_ehpit0_6 = alloca i32, align 4
  %c_4 = alloca i8, align 1
  %old_frame_pointer_11.addr = alloca i64, align 8
  %return_address_12.addr = alloca i64, align 8
  store i32 0, i32* %.preg_I4_4_49, align 8
  store i32 65, i32* %.preg_I4_4_50, align 8
  br label %L2306

L2306:                                            ; preds = %L2306, %entry
  %0 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0) to i8*
  %1 = load i32, i32* %.preg_I4_4_50
  %call1 = call i32 (i8*, ...) @printf(i8* %0, i32 %1)
  %2 = load i32, i32* %.preg_I4_4_50
  %add = add i32 %2, 1
  %conv = trunc i32 %add to i8
  %conv3 = sext i8 %conv to i32
  store i32 %conv3, i32* %.preg_I4_4_50, align 8
  %3 = load i32, i32* %.preg_I4_4_49
  %add.1 = add i32 %3, 1
  store i32 %add.1, i32* %.preg_I4_4_49, align 8
  %4 = load i32, i32* %.preg_I4_4_49
  %cmp1 = icmp sle i32 %4, 25
  br i1 %cmp1, label %L2306, label %fb

fb:                                               ; preds = %L2306
  ret i32 0
}

declare i32 @printf(i8*, ...)
