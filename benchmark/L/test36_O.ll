; ModuleID = 'test36_O.bc'

@.str = private constant [27 x i8] c"Enter a positive integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [14 x i8] c"%d = %d + %d\0A\00", align 1
@.str.3 = private constant [55 x i8] c"%d can't be expressed as the sum of two prime numbers.\00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_62 = alloca i32
  %.preg_I4_4_74 = alloca i32
  %.preg_I4_4_53 = alloca i32
  %.preg_I4_4_69 = alloca i32
  %.preg_I4_4_68 = alloca i32
  %.preg_I4_4_66 = alloca i32
  %.preg_I4_4_56 = alloca i32
  %.preg_I4_4_70 = alloca i32
  %.preg_I4_4_63 = alloca i32
  %.preg_I4_4_73 = alloca i32
  %.preg_I4_4_51 = alloca i32
  %.preg_I4_4_67 = alloca i32
  %.preg_I4_4_65 = alloca i32
  %.preg_I4_4_72 = alloca i32
  %.preg_I4_4_60 = alloca i32
  %.preg_I4_4_64 = alloca i32
  %.preg_I4_4_71 = alloca i32
  %.preg_I4_4_55 = alloca i32
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %_temp_ehpit0_19 = alloca i32, align 4
  %flag_16 = alloca i32, align 4
  %flag_6 = alloca i32, align 4
  %i_15 = alloca i32, align 4
  %i_5 = alloca i32, align 4
  %n_14.addr = alloca i32, align 4
  %n_17 = alloca i32, align 4
  %n_18 = alloca i32, align 4
  %n_4 = alloca i32, align 4
  %old_frame_pointer_24.addr = alloca i64, align 8
  %return_address_25.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  store i32 0, i32* %.preg_I4_4_55, align 8
  %3 = load i32, i32* %n_4, align 4
  store i32 %3, i32* %.preg_I4_4_71, align 8
  %4 = load i32, i32* %.preg_I4_4_71
  %div = sdiv i32 %4, 2
  %add = add i32 %div, -2
  store i32 %add, i32* %.preg_I4_4_64, align 8
  %5 = load i32, i32* %.preg_I4_4_64
  %cmp1 = icmp sge i32 %5, 0
  br i1 %cmp1, label %tb, label %L16642

tb:                                               ; preds = %entry
  store i32 2, i32* %.preg_I4_4_60, align 8
  store i32 0, i32* %.preg_I4_4_72, align 8
  br label %L10498

L16642:                                           ; preds = %entry
  br label %L17922

L10498:                                           ; preds = %L16130, %tb
  %6 = load i32, i32* %.preg_I4_4_60
  %div.1 = sdiv i32 %6, 2
  store i32 %div.1, i32* %.preg_I4_4_65, align 8
  %7 = load i32, i32* %.preg_I4_4_65
  %add.2 = add i32 %7, -2
  store i32 %add.2, i32* %.preg_I4_4_67, align 8
  %8 = load i32, i32* %.preg_I4_4_67
  %cmp2 = icmp sge i32 %8, 0
  br i1 %cmp2, label %tb3, label %L10754

tb3:                                              ; preds = %L10498
  store i32 0, i32* %.preg_I4_4_51, align 8
  %9 = load i32, i32* %.preg_I4_4_65
  %add.4 = add i32 %9, -1
  store i32 %add.4, i32* %.preg_I4_4_73, align 8
  store i32 2, i32* %.preg_I4_4_63, align 8
  store i32 1, i32* %.preg_I4_4_70, align 8
  br label %L11522

L10754:                                           ; preds = %L10498
  br label %L17154

L11522:                                           ; preds = %L15362, %tb3
  %10 = load i32, i32* %.preg_I4_4_60
  %11 = load i32, i32* %.preg_I4_4_63
  %srem = srem i32 %10, %11
  %cmp3 = icmp eq i32 %srem, 0
  br i1 %cmp3, label %tb5, label %L15362

tb5:                                              ; preds = %L11522
  store i32 0, i32* %.preg_I4_4_70, align 8
  br label %L15362

L15362:                                           ; preds = %tb5, %L11522
  %12 = load i32, i32* %.preg_I4_4_51
  %add.6 = add i32 %12, 1
  store i32 %add.6, i32* %.preg_I4_4_51, align 8
  %13 = load i32, i32* %.preg_I4_4_63
  %add.7 = add i32 %13, 1
  store i32 %add.7, i32* %.preg_I4_4_63, align 8
  %14 = load i32, i32* %.preg_I4_4_51
  %15 = load i32, i32* %.preg_I4_4_67
  %cmp4 = icmp sle i32 %14, %15
  br i1 %cmp4, label %L11522, label %fb

fb:                                               ; preds = %L15362
  br label %L14850

L14850:                                           ; preds = %fb
  %16 = load i32, i32* %.preg_I4_4_70
  %cmp5 = icmp eq i32 %16, 1
  br i1 %cmp5, label %tb8, label %L16130

L17154:                                           ; preds = %tb8, %L10754
  store i32 1, i32* %.preg_I4_4_56, align 8
  %17 = load i32, i32* %.preg_I4_4_71
  %18 = load i32, i32* %.preg_I4_4_55
  %add.9 = sub i32 %17, %18
  %add.10 = add i32 %add.9, -2
  store i32 %add.10, i32* %.preg_I4_4_66, align 8
  %19 = load i32, i32* %.preg_I4_4_66
  %div.11 = sdiv i32 %19, 2
  store i32 %div.11, i32* %.preg_I4_4_68, align 8
  %20 = load i32, i32* %.preg_I4_4_68
  %add.12 = add i32 %20, -2
  store i32 %add.12, i32* %.preg_I4_4_69, align 8
  %21 = load i32, i32* %.preg_I4_4_69
  %cmp6 = icmp sge i32 %21, 0
  br i1 %cmp6, label %tb13, label %L12546

tb8:                                              ; preds = %L14850
  br label %L17154

L16130:                                           ; preds = %L15874, %L14850
  %22 = load i32, i32* %.preg_I4_4_55
  %add.23 = add i32 %22, 1
  store i32 %add.23, i32* %.preg_I4_4_55, align 8
  %23 = load i32, i32* %.preg_I4_4_60
  %add.24 = add i32 %23, 1
  store i32 %add.24, i32* %.preg_I4_4_60, align 8
  %24 = load i32, i32* %.preg_I4_4_55
  %25 = load i32, i32* %.preg_I4_4_64
  %cmp10 = icmp sle i32 %24, %25
  br i1 %cmp10, label %L10498, label %fb25

tb13:                                             ; preds = %L17154
  store i32 0, i32* %.preg_I4_4_53, align 8
  %26 = load i32, i32* %.preg_I4_4_68
  %add.14 = add i32 %26, -1
  store i32 %add.14, i32* %.preg_I4_4_74, align 8
  store i32 2, i32* %.preg_I4_4_62, align 8
  br label %L13314

L12546:                                           ; preds = %L17154
  br label %L17666

L13314:                                           ; preds = %L15618, %tb13
  %27 = load i32, i32* %.preg_I4_4_66
  %28 = load i32, i32* %.preg_I4_4_62
  %srem.15 = srem i32 %27, %28
  %cmp7 = icmp eq i32 %srem.15, 0
  br i1 %cmp7, label %tb16, label %L15618

tb16:                                             ; preds = %L13314
  store i32 0, i32* %.preg_I4_4_56, align 8
  br label %L15618

L15618:                                           ; preds = %tb16, %L13314
  %29 = load i32, i32* %.preg_I4_4_53
  %add.17 = add i32 %29, 1
  store i32 %add.17, i32* %.preg_I4_4_53, align 8
  %30 = load i32, i32* %.preg_I4_4_62
  %add.18 = add i32 %30, 1
  store i32 %add.18, i32* %.preg_I4_4_62, align 8
  %31 = load i32, i32* %.preg_I4_4_53
  %32 = load i32, i32* %.preg_I4_4_69
  %cmp8 = icmp sle i32 %31, %32
  br i1 %cmp8, label %L13314, label %fb19

fb19:                                             ; preds = %L15618
  br label %L15106

L15106:                                           ; preds = %fb19
  %33 = load i32, i32* %.preg_I4_4_56
  %cmp9 = icmp eq i32 %33, 1
  br i1 %cmp9, label %tb20, label %L15874

L17666:                                           ; preds = %tb20, %L12546
  %34 = bitcast i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.2, i32 0, i32 0) to i8*
  %35 = load i32, i32* %.preg_I4_4_71
  %36 = load i32, i32* %.preg_I4_4_60
  %37 = load i32, i32* %.preg_I4_4_66
  %call3 = call i32 (i8*, ...) @printf(i8* %34, i32 %35, i32 %36, i32 %37)
  %38 = load i32, i32* %n_4, align 4
  store i32 %38, i32* %.preg_I4_4_71, align 8
  store i32 1, i32* %.preg_I4_4_72, align 8
  br label %L15874

tb20:                                             ; preds = %L15106
  br label %L17666

L15874:                                           ; preds = %L17666, %L15106
  %39 = load i32, i32* %.preg_I4_4_71
  %div.21 = sdiv i32 %39, 2
  %add.22 = add i32 %div.21, -2
  store i32 %add.22, i32* %.preg_I4_4_64, align 8
  br label %L16130

fb25:                                             ; preds = %L16130
  %40 = load i32, i32* %.preg_I4_4_72
  %cmp11 = icmp eq i32 %40, 0
  br i1 %cmp11, label %tb26, label %L16386

tb26:                                             ; preds = %fb25
  br label %L17922

L16386:                                           ; preds = %L17922, %fb25
  ret i32 0

L17922:                                           ; preds = %tb26, %L16642
  %41 = bitcast i8* getelementptr inbounds ([55 x i8], [55 x i8]* @.str.3, i32 0, i32 0) to i8*
  %42 = load i32, i32* %.preg_I4_4_71
  %call4 = call i32 (i8*, ...) @printf(i8* %41, i32 %42)
  br label %L16386
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define i32 @_Z10checkPrimei(i32 %n_4) {
entry:
  %.preg_I4_4_69 = alloca i32
  %.preg_I4_4_67 = alloca i32
  %.preg_I4_4_61 = alloca i32
  %.preg_I4_4_60 = alloca i32
  %.preg_I4_4_59 = alloca i32
  %.preg_I4_4_58 = alloca i32
  %.preg_I4_4_57 = alloca i32
  %.preg_I4_4_56 = alloca i32
  %.preg_I4_4_55 = alloca i32
  %.preg_I4_4_54 = alloca i32
  %.preg_I4_4_68 = alloca i32
  %.preg_I4_4_65 = alloca i32
  %.preg_I4_4_49 = alloca i32
  %.preg_I4_4_62 = alloca i32
  %.preg_I4_4_53 = alloca i32
  %.preg_I4_4_66 = alloca i32
  %flag_6 = alloca i32, align 4
  %i_5 = alloca i32, align 4
  %n_4.addr = alloca i32, align 4
  %old_frame_pointer_11.addr = alloca i64, align 8
  %return_address_12.addr = alloca i64, align 8
  store i32 %n_4, i32* %n_4.addr, align 4
  store i32 %n_4, i32* %.preg_I4_4_66, align 8
  %0 = load i32, i32* %.preg_I4_4_66
  %div = sdiv i32 %0, 2
  store i32 %div, i32* %.preg_I4_4_53, align 8
  %1 = load i32, i32* %.preg_I4_4_53
  %add = add i32 %1, -2
  store i32 %add, i32* %.preg_I4_4_62, align 8
  %2 = load i32, i32* %.preg_I4_4_62
  %cmp12 = icmp sge i32 %2, 0
  br i1 %cmp12, label %tb, label %L2818

tb:                                               ; preds = %entry
  store i32 0, i32* %.preg_I4_4_49, align 8
  %3 = load i32, i32* %.preg_I4_4_53
  %add.1 = add i32 %3, -1
  %div.2 = sdiv i32 %add.1, 8
  %mul = mul i32 %div.2, 8
  store i32 %mul, i32* %.preg_I4_4_65, align 8
  %4 = load i32, i32* %.preg_I4_4_65
  %cmp13 = icmp sgt i32 %4, 0
  br i1 %cmp13, label %tb3, label %L9986

L2818:                                            ; preds = %entry
  store i32 1, i32* %.preg_I4_4_67, align 8
  br label %L9730

tb3:                                              ; preds = %tb
  %5 = load i32, i32* %.preg_I4_4_65
  %add.4 = add i32 %5, 7
  %div.5 = sdiv i32 %add.4, 8
  store i32 %div.5, i32* %.preg_I4_4_68, align 8
  store i32 2, i32* %.preg_I4_4_54, align 8
  store i32 3, i32* %.preg_I4_4_55, align 8
  store i32 4, i32* %.preg_I4_4_56, align 8
  store i32 5, i32* %.preg_I4_4_57, align 8
  store i32 6, i32* %.preg_I4_4_58, align 8
  store i32 7, i32* %.preg_I4_4_59, align 8
  store i32 8, i32* %.preg_I4_4_60, align 8
  store i32 9, i32* %.preg_I4_4_61, align 8
  store i32 1, i32* %.preg_I4_4_67, align 8
  br label %L3586

L9986:                                            ; preds = %tb
  store i32 1, i32* %.preg_I4_4_67, align 8
  br label %L6402

L3586:                                            ; preds = %L9218, %tb3
  %6 = load i32, i32* %.preg_I4_4_66
  %7 = load i32, i32* %.preg_I4_4_54
  %srem = srem i32 %6, %7
  %cmp14 = icmp eq i32 %srem, 0
  br i1 %cmp14, label %tb6, label %L7426

tb6:                                              ; preds = %L3586
  store i32 0, i32* %.preg_I4_4_67, align 8
  br label %L7426

L7426:                                            ; preds = %tb6, %L3586
  %8 = load i32, i32* %.preg_I4_4_66
  %9 = load i32, i32* %.preg_I4_4_55
  %srem.7 = srem i32 %8, %9
  %cmp15 = icmp eq i32 %srem.7, 0
  br i1 %cmp15, label %tb8, label %L7682

tb8:                                              ; preds = %L7426
  store i32 0, i32* %.preg_I4_4_67, align 8
  br label %L7682

L7682:                                            ; preds = %tb8, %L7426
  %10 = load i32, i32* %.preg_I4_4_66
  %11 = load i32, i32* %.preg_I4_4_56
  %srem.9 = srem i32 %10, %11
  %cmp16 = icmp eq i32 %srem.9, 0
  br i1 %cmp16, label %tb10, label %L7938

tb10:                                             ; preds = %L7682
  store i32 0, i32* %.preg_I4_4_67, align 8
  br label %L7938

L7938:                                            ; preds = %tb10, %L7682
  %12 = load i32, i32* %.preg_I4_4_66
  %13 = load i32, i32* %.preg_I4_4_57
  %srem.11 = srem i32 %12, %13
  %cmp17 = icmp eq i32 %srem.11, 0
  br i1 %cmp17, label %tb12, label %L8194

tb12:                                             ; preds = %L7938
  store i32 0, i32* %.preg_I4_4_67, align 8
  br label %L8194

L8194:                                            ; preds = %tb12, %L7938
  %14 = load i32, i32* %.preg_I4_4_66
  %15 = load i32, i32* %.preg_I4_4_58
  %srem.13 = srem i32 %14, %15
  %cmp18 = icmp eq i32 %srem.13, 0
  br i1 %cmp18, label %tb14, label %L8450

tb14:                                             ; preds = %L8194
  store i32 0, i32* %.preg_I4_4_67, align 8
  br label %L8450

L8450:                                            ; preds = %tb14, %L8194
  %16 = load i32, i32* %.preg_I4_4_66
  %17 = load i32, i32* %.preg_I4_4_59
  %srem.15 = srem i32 %16, %17
  %cmp19 = icmp eq i32 %srem.15, 0
  br i1 %cmp19, label %tb16, label %L8706

tb16:                                             ; preds = %L8450
  store i32 0, i32* %.preg_I4_4_67, align 8
  br label %L8706

L8706:                                            ; preds = %tb16, %L8450
  %18 = load i32, i32* %.preg_I4_4_66
  %19 = load i32, i32* %.preg_I4_4_60
  %srem.17 = srem i32 %18, %19
  %cmp20 = icmp eq i32 %srem.17, 0
  br i1 %cmp20, label %tb18, label %L8962

tb18:                                             ; preds = %L8706
  store i32 0, i32* %.preg_I4_4_67, align 8
  br label %L8962

L8962:                                            ; preds = %tb18, %L8706
  %20 = load i32, i32* %.preg_I4_4_66
  %21 = load i32, i32* %.preg_I4_4_61
  %srem.19 = srem i32 %20, %21
  %cmp21 = icmp eq i32 %srem.19, 0
  br i1 %cmp21, label %tb20, label %L9218

tb20:                                             ; preds = %L8962
  store i32 0, i32* %.preg_I4_4_67, align 8
  br label %L9218

L9218:                                            ; preds = %tb20, %L8962
  %22 = load i32, i32* %.preg_I4_4_49
  %add.21 = add i32 %22, 8
  store i32 %add.21, i32* %.preg_I4_4_49, align 8
  %23 = load i32, i32* %.preg_I4_4_61
  %add.22 = add i32 %23, 8
  store i32 %add.22, i32* %.preg_I4_4_61, align 8
  %24 = load i32, i32* %.preg_I4_4_60
  %add.23 = add i32 %24, 8
  store i32 %add.23, i32* %.preg_I4_4_60, align 8
  %25 = load i32, i32* %.preg_I4_4_59
  %add.24 = add i32 %25, 8
  store i32 %add.24, i32* %.preg_I4_4_59, align 8
  %26 = load i32, i32* %.preg_I4_4_58
  %add.25 = add i32 %26, 8
  store i32 %add.25, i32* %.preg_I4_4_58, align 8
  %27 = load i32, i32* %.preg_I4_4_57
  %add.26 = add i32 %27, 8
  store i32 %add.26, i32* %.preg_I4_4_57, align 8
  %28 = load i32, i32* %.preg_I4_4_56
  %add.27 = add i32 %28, 8
  store i32 %add.27, i32* %.preg_I4_4_56, align 8
  %29 = load i32, i32* %.preg_I4_4_55
  %add.28 = add i32 %29, 8
  store i32 %add.28, i32* %.preg_I4_4_55, align 8
  %30 = load i32, i32* %.preg_I4_4_54
  %add.29 = add i32 %30, 8
  store i32 %add.29, i32* %.preg_I4_4_54, align 8
  %31 = load i32, i32* %.preg_I4_4_49
  %32 = load i32, i32* %.preg_I4_4_65
  %cmp22 = icmp slt i32 %31, %32
  br i1 %cmp22, label %L3586, label %fb

fb:                                               ; preds = %L9218
  %33 = load i32, i32* %.preg_I4_4_49
  %34 = load i32, i32* %.preg_I4_4_62
  %cmp23 = icmp sle i32 %33, %34
  br i1 %cmp23, label %tb30, label %L6146

tb30:                                             ; preds = %fb
  br label %L6402

L6146:                                            ; preds = %fb38, %fb
  br label %L9730

L6402:                                            ; preds = %tb30, %L9986
  %35 = load i32, i32* %.preg_I4_4_53
  %36 = load i32, i32* %.preg_I4_4_49
  %add.31 = sub i32 %35, %36
  %add.32 = add i32 %add.31, -1
  store i32 %add.32, i32* %.preg_I4_4_69, align 8
  %37 = load i32, i32* %.preg_I4_4_49
  %add.33 = add i32 %37, 2
  store i32 %add.33, i32* %.preg_I4_4_54, align 8
  br label %L6658

L6658:                                            ; preds = %L9474, %L6402
  %38 = load i32, i32* %.preg_I4_4_66
  %39 = load i32, i32* %.preg_I4_4_54
  %srem.34 = srem i32 %38, %39
  %cmp24 = icmp eq i32 %srem.34, 0
  br i1 %cmp24, label %tb35, label %L9474

tb35:                                             ; preds = %L6658
  store i32 0, i32* %.preg_I4_4_67, align 8
  br label %L9474

L9474:                                            ; preds = %tb35, %L6658
  %40 = load i32, i32* %.preg_I4_4_49
  %add.36 = add i32 %40, 1
  store i32 %add.36, i32* %.preg_I4_4_49, align 8
  %41 = load i32, i32* %.preg_I4_4_54
  %add.37 = add i32 %41, 1
  store i32 %add.37, i32* %.preg_I4_4_54, align 8
  %42 = load i32, i32* %.preg_I4_4_49
  %43 = load i32, i32* %.preg_I4_4_62
  %cmp25 = icmp sle i32 %42, %43
  br i1 %cmp25, label %L6658, label %fb38

fb38:                                             ; preds = %L9474
  br label %L6146

L9730:                                            ; preds = %L6146, %L2818
  %44 = load i32, i32* %.preg_I4_4_67
  ret i32 %44
}
