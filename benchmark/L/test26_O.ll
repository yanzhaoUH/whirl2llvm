; ModuleID = 'test26_O.bc'

@.str = private constant [19 x i8] c"Enter an integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [20 x i8] c"%d is a palindrome.\00", align 1
@.str.3 = private constant [24 x i8] c"%d is not a palindrome.\00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_50 = alloca i32
  %.preg_I4_4_55 = alloca i32
  %.preg_I4_4_51 = alloca i32
  %.preg_I4_4_54 = alloca i32
  %_temp_dummy10_8 = alloca i32, align 4
  %_temp_dummy11_9 = alloca i32, align 4
  %_temp_dummy12_10 = alloca i32, align 4
  %_temp_dummy13_11 = alloca i32, align 4
  %_temp_ehpit0_12 = alloca i32, align 4
  %n_4 = alloca i32, align 4
  %old_frame_pointer_17.addr = alloca i64, align 8
  %originalInteger_7 = alloca i32, align 4
  %remainder_6 = alloca i32, align 4
  %return_address_18.addr = alloca i64, align 8
  %reversedInteger_5 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = load i32, i32* %n_4, align 4
  store i32 %3, i32* %.preg_I4_4_54, align 8
  %4 = load i32, i32* %.preg_I4_4_54
  store i32 %4, i32* %.preg_I4_4_51, align 8
  %5 = load i32, i32* %.preg_I4_4_51
  %cmp1 = icmp ne i32 %5, 0
  br i1 %cmp1, label %tb, label %L4354

tb:                                               ; preds = %entry
  store i32 0, i32* %.preg_I4_4_55, align 8
  br label %L3330

L4354:                                            ; preds = %entry
  br label %L4610

L3330:                                            ; preds = %L3330, %tb
  %6 = load i32, i32* %.preg_I4_4_54
  %lowPart_4 = alloca i32, align 4
  %div = sdiv i32 %6, 10
  %srem = srem i32 %6, 10
  store i32 %div, i32* %lowPart_4, align 4
  store i32 %srem, i32* %.preg_I4_4_50, align 8
  %7 = load i32, i32* %.preg_I4_4_50
  %8 = load i32, i32* %.preg_I4_4_55
  %mul = mul i32 %8, 10
  %add = add i32 %7, %mul
  store i32 %add, i32* %.preg_I4_4_55, align 8
  %9 = load i32, i32* %lowPart_4
  store i32 %9, i32* %.preg_I4_4_54, align 8
  %10 = load i32, i32* %.preg_I4_4_54
  %cmp2 = icmp ne i32 %10, 0
  br i1 %cmp2, label %L3330, label %fb

fb:                                               ; preds = %L3330
  %11 = load i32, i32* %.preg_I4_4_54
  store i32 %11, i32* %n_4, align 4
  %12 = load i32, i32* %.preg_I4_4_55
  %13 = load i32, i32* %.preg_I4_4_51
  %cmp3 = icmp eq i32 %12, %13
  br i1 %cmp3, label %tb1, label %L3842

tb1:                                              ; preds = %fb
  br label %L4610

L3842:                                            ; preds = %fb
  %14 = bitcast i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.3, i32 0, i32 0) to i8*
  %15 = load i32, i32* %.preg_I4_4_51
  %call4 = call i32 (i8*, ...) @printf(i8* %14, i32 %15)
  br label %L4098

L4610:                                            ; preds = %tb1, %L4354
  %16 = bitcast i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.2, i32 0, i32 0) to i8*
  %17 = load i32, i32* %.preg_I4_4_51
  %call3 = call i32 (i8*, ...) @printf(i8* %16, i32 %17)
  br label %L4098

L4098:                                            ; preds = %L4610, %L3842
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
