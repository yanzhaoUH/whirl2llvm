; ModuleID = 'test337.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.str = type { i8, i64, double*, [3 x i32] }

@.str = private unnamed_addr constant [4 x i8] c"%s\0A\00", align 1
@.str.1 = private unnamed_addr constant [7 x i8] c"test 1\00", align 1
@.str.2 = private unnamed_addr constant [9 x i8] c"test 1 1\00", align 1
@main.array = private unnamed_addr constant [2 x i8*] [i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.1, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.2, i32 0, i32 0)], align 16
@.str.3 = private unnamed_addr constant [5 x i8] c"%s \0A\00", align 1
@.str.4 = private unnamed_addr constant [7 x i8] c"test 2\00", align 1
@.str.5 = private unnamed_addr constant [4 x i8] c"%d\0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @f1(i8** %a) #0 {
entry:
  %a.addr = alloca i8**, align 8
  store i8** %a, i8*** %a.addr, align 8
  %0 = load i8**, i8*** %a.addr, align 8
  %arrayidx = getelementptr inbounds i8*, i8** %0, i64 1
  %1 = load i8*, i8** %arrayidx, align 8
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0), i8* %1)
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %array = alloca [2 x i8*], align 16
  %ap = alloca i8*, align 8
  %aa = alloca [3 x i32], align 4
  %p = alloca i32*, align 8
  %farray = alloca [3 x double], align 16
  %str1 = alloca %struct.str, align 8
  %strp = alloca %struct.str*, align 8
  store i32 0, i32* %retval
  %0 = bitcast [2 x i8*]* %array to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %0, i8* bitcast ([2 x i8*]* @main.array to i8*), i64 16, i32 16, i1 false)
  %arrayidx = getelementptr inbounds [2 x i8*], [2 x i8*]* %array, i32 0, i64 1
  %1 = load i8*, i8** %arrayidx, align 8
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.3, i32 0, i32 0), i8* %1)
  store i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.4, i32 0, i32 0), i8** %ap, align 8
  %2 = load i8*, i8** %ap, align 8
  %call1 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0), i8* %2)
  %arraydecay = getelementptr inbounds [2 x i8*], [2 x i8*]* %array, i32 0, i32 0
  %call2 = call i32 @f1(i8** %arraydecay)
  %arraydecay3 = getelementptr inbounds [3 x i32], [3 x i32]* %aa, i32 0, i32 0
  store i32* %arraydecay3, i32** %p, align 8
  %a = getelementptr inbounds %struct.str, %struct.str* %str1, i32 0, i32 1
  store i64 100, i64* %a, align 8
  %array123 = getelementptr inbounds %struct.str, %struct.str* %str1, i32 0, i32 3
  %arrayidx4 = getelementptr inbounds [3 x i32], [3 x i32]* %array123, i32 0, i64 1
  store i32 33, i32* %arrayidx4, align 4
  %arraydecay5 = getelementptr inbounds [3 x double], [3 x double]* %farray, i32 0, i32 0
  %fp = getelementptr inbounds %struct.str, %struct.str* %str1, i32 0, i32 2
  store double* %arraydecay5, double** %fp, align 8
  %array1236 = getelementptr inbounds %struct.str, %struct.str* %str1, i32 0, i32 3
  %arrayidx7 = getelementptr inbounds [3 x i32], [3 x i32]* %array1236, i32 0, i64 1
  %3 = load i32, i32* %arrayidx7, align 4
  %call8 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.5, i32 0, i32 0), i32 %3)
  store %struct.str* %str1, %struct.str** %strp, align 8
  %4 = load %struct.str*, %struct.str** %strp, align 8
  %array1239 = getelementptr inbounds %struct.str, %struct.str* %4, i32 0, i32 3
  %arrayidx10 = getelementptr inbounds [3 x i32], [3 x i32]* %array1239, i32 0, i64 1
  %5 = load i32, i32* %arrayidx10, align 4
  %call11 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.5, i32 0, i32 0), i32 %5)
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture, i8* nocapture readonly, i64, i32, i1) #2

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
