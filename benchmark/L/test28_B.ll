; ModuleID = 'test28.bc'

@.str = private constant [30 x i8] c"Enter two numbers(intevals): \00", align 1
@.str.1 = private constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private constant [38 x i8] c"Prime numbers between %d and %d are: \00", align 1
@.str.3 = private constant [4 x i8] c"%d \00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_8 = alloca i32, align 4
  %_temp_dummy11_9 = alloca i32, align 4
  %_temp_dummy12_10 = alloca i32, align 4
  %_temp_dummy13_11 = alloca i32, align 4
  %flag_7 = alloca i32, align 4
  %i_6 = alloca i32, align 4
  %n1_4 = alloca i32, align 4
  %n2_5 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n1_4 to i32*
  %3 = bitcast i32* %n2_5 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2, i32* %3)
  %4 = bitcast i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.2, i32 0, i32 0) to i8*
  %5 = load i32, i32* %n1_4, align 4
  %6 = load i32, i32* %n2_5, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %4, i32 %5, i32 %6)
  br label %while.cond

while.cond:                                       ; preds = %if.end6, %entry
  %7 = load i32, i32* %n1_4, align 4
  %8 = load i32, i32* %n2_5, align 4
  %cmp1 = icmp slt i32 %7, %8
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  store i32 0, i32* %flag_7, align 4
  store i32 2, i32* %i_6, align 4
  br label %while.cond1

while.end:                                        ; preds = %while.cond
  ret i32 0

while.cond1:                                      ; preds = %if.end, %while.body
  %9 = load i32, i32* %n1_4, align 4
  %div = sdiv i32 %9, 2
  %10 = load i32, i32* %i_6, align 4
  %cmp2 = icmp sge i32 %div, %10
  br i1 %cmp2, label %while.body2, label %while.end3

while.body2:                                      ; preds = %while.cond1
  %11 = load i32, i32* %n1_4, align 4
  %12 = load i32, i32* %i_6, align 4
  %srem = srem i32 %11, %12
  %cmp3 = icmp eq i32 %srem, 0
  br i1 %cmp3, label %if.then, label %if.else

while.end3:                                       ; preds = %if.then, %while.cond1
  br label %LABEL_258

if.then:                                          ; preds = %while.body2
  store i32 1, i32* %flag_7, align 4
  br label %while.end3

if.else:                                          ; preds = %while.body2
  br label %if.end

if.end:                                           ; preds = %if.else
  %13 = load i32, i32* %i_6, align 4
  %add = add i32 %13, 1
  store i32 %add, i32* %i_6, align 4
  br label %while.cond1

LABEL_258:                                        ; preds = %while.end3
  %14 = load i32, i32* %flag_7, align 4
  %cmp4 = icmp eq i32 %14, 0
  br i1 %cmp4, label %if.then4, label %if.else5

if.then4:                                         ; preds = %LABEL_258
  %15 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.3, i32 0, i32 0) to i8*
  %16 = load i32, i32* %n1_4, align 4
  %call4 = call i32 (i8*, ...) @printf(i8* %15, i32 %16)
  br label %if.end6

if.else5:                                         ; preds = %LABEL_258
  br label %if.end6

if.end6:                                          ; preds = %if.else5, %if.then4
  %17 = load i32, i32* %n1_4, align 4
  %add.7 = add i32 %17, 1
  store i32 %add.7, i32* %n1_4, align 4
  br label %while.cond
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
