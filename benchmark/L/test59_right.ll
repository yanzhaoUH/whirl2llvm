; ModuleID = 'test59.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [21 x i8] c"Enter first string: \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%s\00", align 1
@.str.2 = private unnamed_addr constant [22 x i8] c"Enter second string: \00", align 1
@.str.3 = private unnamed_addr constant [24 x i8] c"After concatenation: %s\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %s1 = alloca [100 x i8], align 16
  %s2 = alloca [100 x i8], align 16
  %i = alloca i8, align 1
  %j = alloca i8, align 1
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str, i32 0, i32 0))
  %arraydecay = getelementptr inbounds [100 x i8], [100 x i8]* %s1, i32 0, i32 0
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0), i8* %arraydecay)
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.2, i32 0, i32 0))
  %arraydecay3 = getelementptr inbounds [100 x i8], [100 x i8]* %s2, i32 0, i32 0
  %call4 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0), i8* %arraydecay3)
  store i8 0, i8* %i, align 1
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i8, i8* %i, align 1
  %idxprom = sext i8 %0 to i64
  %arrayidx = getelementptr inbounds [100 x i8], [100 x i8]* %s1, i32 0, i64 %idxprom
  %1 = load i8, i8* %arrayidx, align 1
  %conv = sext i8 %1 to i32
  %cmp = icmp ne i32 %conv, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %2 = load i8, i8* %i, align 1
  %inc = add i8 %2, 1
  store i8 %inc, i8* %i, align 1
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i8 0, i8* %j, align 1
  br label %for.cond.6

for.cond.6:                                       ; preds = %for.inc.17, %for.end
  %3 = load i8, i8* %j, align 1
  %idxprom7 = sext i8 %3 to i64
  %arrayidx8 = getelementptr inbounds [100 x i8], [100 x i8]* %s2, i32 0, i64 %idxprom7
  %4 = load i8, i8* %arrayidx8, align 1
  %conv9 = sext i8 %4 to i32
  %cmp10 = icmp ne i32 %conv9, 0
  br i1 %cmp10, label %for.body.12, label %for.end.20

for.body.12:                                      ; preds = %for.cond.6
  %5 = load i8, i8* %j, align 1
  %idxprom13 = sext i8 %5 to i64
  %arrayidx14 = getelementptr inbounds [100 x i8], [100 x i8]* %s2, i32 0, i64 %idxprom13
  %6 = load i8, i8* %arrayidx14, align 1
  %7 = load i8, i8* %i, align 1
  %idxprom15 = sext i8 %7 to i64
  %arrayidx16 = getelementptr inbounds [100 x i8], [100 x i8]* %s1, i32 0, i64 %idxprom15
  store i8 %6, i8* %arrayidx16, align 1
  br label %for.inc.17

for.inc.17:                                       ; preds = %for.body.12
  %8 = load i8, i8* %j, align 1
  %inc18 = add i8 %8, 1
  store i8 %inc18, i8* %j, align 1
  %9 = load i8, i8* %i, align 1
  %inc19 = add i8 %9, 1
  store i8 %inc19, i8* %i, align 1
  br label %for.cond.6

for.end.20:                                       ; preds = %for.cond.6
  %10 = load i8, i8* %i, align 1
  %idxprom21 = sext i8 %10 to i64
  %arrayidx22 = getelementptr inbounds [100 x i8], [100 x i8]* %s1, i32 0, i64 %idxprom21
  store i8 0, i8* %arrayidx22, align 1
  %arraydecay23 = getelementptr inbounds [100 x i8], [100 x i8]* %s1, i32 0, i32 0
  %call24 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.3, i32 0, i32 0), i8* %arraydecay23)
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @__isoc99_scanf(i8*, ...) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
