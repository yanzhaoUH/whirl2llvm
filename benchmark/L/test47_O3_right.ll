; ModuleID = 'test47.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [17 x i8] c"Enter elements: \00", align 1
@.str.1 = private unnamed_addr constant [27 x i8] c"\0AStandard Deviation = %.6f\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i64 0, i64 0)) #2
  %call3 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.1, i64 0, i64 0), double 0x4006FA6EA0000000) #2
  ret i32 0
}

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #1

; Function Attrs: nounwind uwtable
define float @calculateSD(float* nocapture readonly %data) #0 {
entry:
  %0 = load float, float* %data, align 4, !tbaa !1
  %add = fadd float %0, 0.000000e+00
  %arrayidx.1 = getelementptr inbounds float, float* %data, i64 1
  %1 = load float, float* %arrayidx.1, align 4, !tbaa !1
  %add.1 = fadd float %add, %1
  %arrayidx.2 = getelementptr inbounds float, float* %data, i64 2
  %2 = load float, float* %arrayidx.2, align 4, !tbaa !1
  %add.2 = fadd float %add.1, %2
  %arrayidx.3 = getelementptr inbounds float, float* %data, i64 3
  %3 = load float, float* %arrayidx.3, align 4, !tbaa !1
  %add.3 = fadd float %add.2, %3
  %arrayidx.4 = getelementptr inbounds float, float* %data, i64 4
  %4 = load float, float* %arrayidx.4, align 4, !tbaa !1
  %add.4 = fadd float %add.3, %4
  %arrayidx.5 = getelementptr inbounds float, float* %data, i64 5
  %5 = load float, float* %arrayidx.5, align 4, !tbaa !1
  %add.5 = fadd float %add.4, %5
  %arrayidx.6 = getelementptr inbounds float, float* %data, i64 6
  %6 = load float, float* %arrayidx.6, align 4, !tbaa !1
  %add.6 = fadd float %add.5, %6
  %arrayidx.7 = getelementptr inbounds float, float* %data, i64 7
  %7 = load float, float* %arrayidx.7, align 4, !tbaa !1
  %add.7 = fadd float %add.6, %7
  %arrayidx.8 = getelementptr inbounds float, float* %data, i64 8
  %8 = load float, float* %arrayidx.8, align 4, !tbaa !1
  %add.8 = fadd float %add.7, %8
  %arrayidx.9 = getelementptr inbounds float, float* %data, i64 9
  %9 = load float, float* %arrayidx.9, align 4, !tbaa !1
  %add.9 = fadd float %add.8, %9
  %div = fdiv float %add.9, 1.000000e+01
  %sub = fsub float %0, %div
  %conv = fpext float %sub to double
  %pow2 = fmul double %conv, %conv
  %add7 = fadd double %pow2, 0.000000e+00
  %conv8 = fptrunc double %add7 to float
  %sub.1 = fsub float %1, %div
  %conv.1 = fpext float %sub.1 to double
  %pow2.1 = fmul double %conv.1, %conv.1
  %conv6.1 = fpext float %conv8 to double
  %add7.1 = fadd double %conv6.1, %pow2.1
  %conv8.1 = fptrunc double %add7.1 to float
  %sub.2 = fsub float %2, %div
  %conv.2 = fpext float %sub.2 to double
  %pow2.2 = fmul double %conv.2, %conv.2
  %conv6.2 = fpext float %conv8.1 to double
  %add7.2 = fadd double %conv6.2, %pow2.2
  %conv8.2 = fptrunc double %add7.2 to float
  %sub.3 = fsub float %3, %div
  %conv.3 = fpext float %sub.3 to double
  %pow2.3 = fmul double %conv.3, %conv.3
  %conv6.3 = fpext float %conv8.2 to double
  %add7.3 = fadd double %conv6.3, %pow2.3
  %conv8.3 = fptrunc double %add7.3 to float
  %sub.4 = fsub float %4, %div
  %conv.4 = fpext float %sub.4 to double
  %pow2.4 = fmul double %conv.4, %conv.4
  %conv6.4 = fpext float %conv8.3 to double
  %add7.4 = fadd double %conv6.4, %pow2.4
  %conv8.4 = fptrunc double %add7.4 to float
  %sub.5 = fsub float %5, %div
  %conv.5 = fpext float %sub.5 to double
  %pow2.5 = fmul double %conv.5, %conv.5
  %conv6.5 = fpext float %conv8.4 to double
  %add7.5 = fadd double %conv6.5, %pow2.5
  %conv8.5 = fptrunc double %add7.5 to float
  %sub.6 = fsub float %6, %div
  %conv.6 = fpext float %sub.6 to double
  %pow2.6 = fmul double %conv.6, %conv.6
  %conv6.6 = fpext float %conv8.5 to double
  %add7.6 = fadd double %conv6.6, %pow2.6
  %conv8.6 = fptrunc double %add7.6 to float
  %sub.7 = fsub float %7, %div
  %conv.7 = fpext float %sub.7 to double
  %pow2.7 = fmul double %conv.7, %conv.7
  %conv6.7 = fpext float %conv8.6 to double
  %add7.7 = fadd double %conv6.7, %pow2.7
  %conv8.7 = fptrunc double %add7.7 to float
  %sub.8 = fsub float %8, %div
  %conv.8 = fpext float %sub.8 to double
  %pow2.8 = fmul double %conv.8, %conv.8
  %conv6.8 = fpext float %conv8.7 to double
  %add7.8 = fadd double %conv6.8, %pow2.8
  %conv8.8 = fptrunc double %add7.8 to float
  %sub.9 = fsub float %9, %div
  %conv.9 = fpext float %sub.9 to double
  %pow2.9 = fmul double %conv.9, %conv.9
  %conv6.9 = fpext float %conv8.8 to double
  %add7.9 = fadd double %conv6.9, %pow2.9
  %conv8.9 = fptrunc double %add7.9 to float
  %div12 = fdiv float %conv8.9, 1.000000e+01
  %sqrtf = tail call float @sqrtf(float %div12) #1
  ret float %sqrtf
}

declare float @sqrtf(float)

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"float", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
