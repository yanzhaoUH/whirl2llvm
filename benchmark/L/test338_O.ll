; ModuleID = 'test338_O.bc'

%struct.str = type { i8, double, [3 x i32], %struct.str_child, i32 }
%struct.str_child = type { i32, double*, i8 }

@.str = private constant [4 x i8] c"%d\0A\00", align 1
@.str.1 = private constant [12 x i8] c"test 1 = %c\00", align 1
@_LIB_VERSION_55 = common global i32 0, align 4
@signgam_56 = common global i32 0, align 4

define i32 @main() {
entry:
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_10 = alloca i32, align 4
  %_temp_ehpit0_11 = alloca i32, align 4
  %ccc_9 = alloca i32, align 4
  %dd_5 = alloca double, align 8
  %old_frame_pointer_16.addr = alloca i64, align 8
  %return_address_17.addr = alloca i64, align 8
  %str1_4 = alloca %struct.str, align 4
  %str_array_8 = alloca [10 x %struct.str], align 8
  %0 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0, i32 33)
  %1 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0) to i8*
  %call2 = call i32 (i8*, ...) @printf(i8* %1, i32 66)
  %2 = bitcast i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %2, i32 97)
  ret i32 0
}

declare i32 @printf(i8*, ...)
