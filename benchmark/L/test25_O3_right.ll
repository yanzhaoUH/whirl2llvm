; ModuleID = 'test25.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [22 x i8] c"Enter a base number: \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.2 = private unnamed_addr constant [21 x i8] c"Enter an exponent: 4\00", align 1
@.str.3 = private unnamed_addr constant [14 x i8] c"Answer = %lld\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %base = alloca i32, align 4
  %exponent = alloca i32, align 4
  %0 = bitcast i32* %base to i8*
  call void @llvm.lifetime.start(i64 4, i8* %0) #1
  %1 = bitcast i32* %exponent to i8*
  call void @llvm.lifetime.start(i64 4, i8* %1) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %base) #1
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.2, i64 0, i64 0)) #1
  %call3 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %exponent) #1
  %.pr = load i32, i32* %exponent, align 4, !tbaa !1
  %cmp.7 = icmp eq i32 %.pr, 0
  br i1 %cmp.7, label %while.end, label %while.body.lr.ph

while.body.lr.ph:                                 ; preds = %entry
  %2 = load i32, i32* %base, align 4, !tbaa !1
  %conv = sext i32 %2 to i64
  %3 = add i32 %.pr, -1
  %xtraiter = and i32 %.pr, 7
  %lcmp.mod = icmp eq i32 %xtraiter, 0
  br i1 %lcmp.mod, label %while.body.lr.ph.split, label %while.body.prol.preheader

while.body.prol.preheader:                        ; preds = %while.body.lr.ph
  br label %while.body.prol

while.body.prol:                                  ; preds = %while.body.prol.preheader, %while.body.prol
  %result.08.prol = phi i64 [ %mul.prol, %while.body.prol ], [ 1, %while.body.prol.preheader ]
  %4 = phi i32 [ %dec.prol, %while.body.prol ], [ %.pr, %while.body.prol.preheader ]
  %prol.iter = phi i32 [ %prol.iter.sub, %while.body.prol ], [ %xtraiter, %while.body.prol.preheader ]
  %mul.prol = mul nsw i64 %conv, %result.08.prol
  %dec.prol = add nsw i32 %4, -1
  %prol.iter.sub = add i32 %prol.iter, -1
  %prol.iter.cmp = icmp eq i32 %prol.iter.sub, 0
  br i1 %prol.iter.cmp, label %while.body.lr.ph.split.loopexit, label %while.body.prol, !llvm.loop !5

while.body.lr.ph.split.loopexit:                  ; preds = %while.body.prol
  %dec.prol.lcssa = phi i32 [ %dec.prol, %while.body.prol ]
  %mul.prol.lcssa = phi i64 [ %mul.prol, %while.body.prol ]
  br label %while.body.lr.ph.split

while.body.lr.ph.split:                           ; preds = %while.body.lr.ph.split.loopexit, %while.body.lr.ph
  %mul.lcssa.unr = phi i64 [ undef, %while.body.lr.ph ], [ %mul.prol.lcssa, %while.body.lr.ph.split.loopexit ]
  %result.08.unr = phi i64 [ 1, %while.body.lr.ph ], [ %mul.prol.lcssa, %while.body.lr.ph.split.loopexit ]
  %.unr = phi i32 [ %.pr, %while.body.lr.ph ], [ %dec.prol.lcssa, %while.body.lr.ph.split.loopexit ]
  %5 = icmp ult i32 %3, 7
  br i1 %5, label %while.cond.while.end_crit_edge, label %while.body.lr.ph.split.split

while.body.lr.ph.split.split:                     ; preds = %while.body.lr.ph.split
  br label %while.body

while.body:                                       ; preds = %while.body, %while.body.lr.ph.split.split
  %result.08 = phi i64 [ %result.08.unr, %while.body.lr.ph.split.split ], [ %mul.7, %while.body ]
  %6 = phi i32 [ %.unr, %while.body.lr.ph.split.split ], [ %dec.7, %while.body ]
  %mul = mul nsw i64 %conv, %result.08
  %mul.1 = mul nsw i64 %conv, %mul
  %mul.2 = mul nsw i64 %conv, %mul.1
  %mul.3 = mul nsw i64 %conv, %mul.2
  %mul.4 = mul nsw i64 %conv, %mul.3
  %mul.5 = mul nsw i64 %conv, %mul.4
  %mul.6 = mul nsw i64 %conv, %mul.5
  %mul.7 = mul nsw i64 %conv, %mul.6
  %dec.7 = add nsw i32 %6, -8
  %cmp.7.9 = icmp eq i32 %dec.7, 0
  br i1 %cmp.7.9, label %while.cond.while.end_crit_edge.unr-lcssa, label %while.body

while.cond.while.end_crit_edge.unr-lcssa:         ; preds = %while.body
  %mul.7.lcssa = phi i64 [ %mul.7, %while.body ]
  br label %while.cond.while.end_crit_edge

while.cond.while.end_crit_edge:                   ; preds = %while.body.lr.ph.split, %while.cond.while.end_crit_edge.unr-lcssa
  %mul.lcssa = phi i64 [ %mul.lcssa.unr, %while.body.lr.ph.split ], [ %mul.7.lcssa, %while.cond.while.end_crit_edge.unr-lcssa ]
  store i32 0, i32* %exponent, align 4, !tbaa !1
  br label %while.end

while.end:                                        ; preds = %while.cond.while.end_crit_edge, %entry
  %result.0.lcssa = phi i64 [ %mul.lcssa, %while.cond.while.end_crit_edge ], [ 1, %entry ]
  %call4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.3, i64 0, i64 0), i64 %result.0.lcssa) #1
  call void @llvm.lifetime.end(i64 4, i8* %1) #1
  call void @llvm.lifetime.end(i64 4, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = distinct !{!5, !6}
!6 = !{!"llvm.loop.unroll.disable"}
