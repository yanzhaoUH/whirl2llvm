; ModuleID = 'test20.bc'

@.str = private constant [21 x i8] c"Enter two integers: \00", align 1
@.str.1 = private constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private constant [25 x i8] c"G.C.D of %d and %d is %d\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_8 = alloca i32, align 4
  %_temp_dummy11_9 = alloca i32, align 4
  %_temp_dummy12_10 = alloca i32, align 4
  %gcd_7 = alloca i32, align 4
  %i_6 = alloca i32, align 4
  %n1_4 = alloca i32, align 4
  %n2_5 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n1_4 to i32*
  %3 = bitcast i32* %n2_5 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2, i32* %3)
  store i32 1, i32* %i_6, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %4 = load i32, i32* %i_6, align 4
  %5 = load i32, i32* %n1_4, align 4
  %cmp1 = icmp sle i32 %4, %5
  br i1 %cmp1, label %land.rhs, label %land.end

while.body:                                       ; preds = %land.end
  %6 = load i32, i32* %n1_4, align 4
  %7 = load i32, i32* %i_6, align 4
  %srem = srem i32 %6, %7
  %cmp3 = icmp eq i32 %srem, 0
  br i1 %cmp3, label %land.rhs1, label %land.end2

while.end:                                        ; preds = %land.end
  %8 = bitcast i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.2, i32 0, i32 0) to i8*
  %9 = load i32, i32* %n1_4, align 4
  %10 = load i32, i32* %n2_5, align 4
  %11 = load i32, i32* %gcd_7, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %8, i32 %9, i32 %10, i32 %11)
  ret i32 0

land.rhs:                                         ; preds = %while.cond
  %12 = load i32, i32* %i_6, align 4
  %13 = load i32, i32* %n2_5, align 4
  %cmp2 = icmp sle i32 %12, %13
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %14 = phi i1 [ false, %while.cond ], [ %cmp2, %land.rhs ]
  br i1 %14, label %while.body, label %while.end

if.then:                                          ; preds = %land.end2
  %15 = load i32, i32* %i_6, align 4
  store i32 %15, i32* %gcd_7, align 4
  br label %if.end

if.else:                                          ; preds = %land.end2
  br label %if.end

land.rhs1:                                        ; preds = %while.body
  %16 = load i32, i32* %n2_5, align 4
  %17 = load i32, i32* %i_6, align 4
  %srem.3 = srem i32 %16, %17
  %cmp4 = icmp eq i32 %srem.3, 0
  br label %land.end2

land.end2:                                        ; preds = %land.rhs1, %while.body
  %18 = phi i1 [ false, %while.body ], [ %cmp4, %land.rhs1 ]
  br i1 %18, label %if.then, label %if.else

if.end:                                           ; preds = %if.else, %if.then
  %19 = load i32, i32* %i_6, align 4
  %add = add i32 %19, 1
  store i32 %add, i32* %i_6, align 4
  br label %while.cond
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
