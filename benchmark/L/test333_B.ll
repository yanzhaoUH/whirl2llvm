; ModuleID = 'test333.bc'

@.str = private constant [21 x i8] c"go into the default\0A\00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1

define i32 @main() {
entry:
  %_temp__switch_index0_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_10 = alloca i32, align 4
  %a_4 = alloca i32, align 4
  %b_5 = alloca i32, align 4
  %i_8 = alloca i32, align 4
  %sum_9 = alloca i32, align 4
  store i32 1, i32* %a_4, align 4
  store i32 0, i32* %b_5, align 4
  %0 = load i32, i32* %a_4, align 4
  store i32 %0, i32* %_temp__switch_index0_6, align 4
  %1 = load i32, i32* %_temp__switch_index0_6, align 4
  switch i32 %1, label %sw.default [
    i32 0, label %sw.block0
    i32 1, label %sw.block1
  ]

sw.block0:                                        ; preds = %entry
  store i32 2, i32* %b_5, align 4
  br label %sw.epilog

sw.block1:                                        ; preds = %entry
  store i32 10, i32* %b_5, align 4
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %2 = bitcast i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %2)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.block1, %sw.block0
  store i32 0, i32* %i_8, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %sw.epilog
  %3 = load i32, i32* %i_8, align 4
  %cmp1 = icmp sle i32 %3, 4
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = load i32, i32* %a_4, align 4
  %5 = load i32, i32* %i_8, align 4
  %add = add i32 %4, %5
  store i32 %add, i32* %a_4, align 4
  %6 = load i32, i32* %i_8, align 4
  %add.1 = add i32 %6, 1
  store i32 %add.1, i32* %i_8, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond2

while.cond2:                                      ; preds = %while.body3, %while.end
  %7 = load i32, i32* %b_5, align 4
  %cmp2 = icmp sle i32 %7, 14
  br i1 %cmp2, label %while.body3, label %while.end4

while.body3:                                      ; preds = %while.cond2
  %8 = load i32, i32* %b_5, align 4
  %add.5 = add i32 %8, 1
  store i32 %add.5, i32* %b_5, align 4
  br label %while.cond2

while.end4:                                       ; preds = %while.cond2
  br label %do.body

do.cond:                                          ; preds = %do.body
  %9 = load i32, i32* %a_4, align 4
  %cmp3 = icmp sle i32 %9, 19
  br i1 %cmp3, label %do.body, label %do.end

do.body:                                          ; preds = %do.cond, %while.end4
  %10 = load i32, i32* %a_4, align 4
  %add.6 = add i32 %10, 1
  store i32 %add.6, i32* %a_4, align 4
  br label %do.cond

do.end:                                           ; preds = %do.cond
  %11 = load i32, i32* %a_4, align 4
  %12 = load i32, i32* %b_5, align 4
  %call2 = call i32 @_Z3Sumii(i32 %11, i32 %12)
  store i32 %call2, i32* %sum_9, align 4
  %13 = load i32, i32* %sum_9, align 4
  %cmp4 = icmp sgt i32 %13, 0
  br i1 %cmp4, label %if.then, label %if.else

if.then:                                          ; preds = %do.end
  %14 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %15 = load i32, i32* %sum_9, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %14, i32 %15)
  br label %if.end

if.else:                                          ; preds = %do.end
  store i32 0, i32* %sum_9, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret i32 0
}

declare i32 @printf(i8*, ...)

define i32 @_Z3Sumii(i32 %x_4, i32 %y_5) {
entry:
  %x_4.addr = alloca i32, align 4
  %y_5.addr = alloca i32, align 4
  store i32 %x_4, i32* %x_4.addr, align 4
  store i32 %y_5, i32* %y_5.addr, align 4
  %0 = load i32, i32* %x_4.addr, align 4
  %1 = load i32, i32* %y_5.addr, align 4
  %add = add i32 %0, %1
  ret i32 %add
}
