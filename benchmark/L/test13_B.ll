; ModuleID = 'test13.bc'

@.str = private constant [15 x i8] c"Enter a year: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [19 x i8] c"%d is a leap year.\00", align 1
@.str.3 = private constant [23 x i8] c"%d is not a leap year.\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_dummy11_6 = alloca i32, align 4
  %_temp_dummy12_7 = alloca i32, align 4
  %_temp_dummy13_8 = alloca i32, align 4
  %_temp_dummy14_9 = alloca i32, align 4
  %_temp_dummy15_10 = alloca i32, align 4
  %year_4 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %year_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = load i32, i32* %year_4, align 4
  %and = and i32 %3, 3
  %cmp1 = icmp eq i32 %and, 0
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load i32, i32* %year_4, align 4
  %srem = srem i32 %4, 100
  %cmp2 = icmp eq i32 %srem, 0
  br i1 %cmp2, label %if.then1, label %if.else2

if.else:                                          ; preds = %entry
  %5 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.3, i32 0, i32 0) to i8*
  %6 = load i32, i32* %year_4, align 4
  %call6 = call i32 (i8*, ...) @printf(i8* %5, i32 %6)
  br label %if.end7

if.then1:                                         ; preds = %if.then
  %7 = load i32, i32* %year_4, align 4
  %srem.5 = srem i32 %7, 400
  %cmp3 = icmp eq i32 %srem.5, 0
  br i1 %cmp3, label %if.then3, label %if.else4

if.else2:                                         ; preds = %if.then
  %8 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.2, i32 0, i32 0) to i8*
  %9 = load i32, i32* %year_4, align 4
  %call5 = call i32 (i8*, ...) @printf(i8* %8, i32 %9)
  br label %if.end6

if.then3:                                         ; preds = %if.then1
  %10 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.2, i32 0, i32 0) to i8*
  %11 = load i32, i32* %year_4, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %10, i32 %11)
  br label %if.end

if.else4:                                         ; preds = %if.then1
  %12 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.3, i32 0, i32 0) to i8*
  %13 = load i32, i32* %year_4, align 4
  %call4 = call i32 (i8*, ...) @printf(i8* %12, i32 %13)
  br label %if.end

if.end:                                           ; preds = %if.else4, %if.then3
  br label %if.end6

if.end6:                                          ; preds = %if.end, %if.else2
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %if.else
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
