; ModuleID = 'test30.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [31 x i8] c"Enter two numbers(intervals): \00", align 1
@.str.1 = private unnamed_addr constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private unnamed_addr constant [41 x i8] c"Armstrong numbers between %d an %d are: \00", align 1
@.str.3 = private unnamed_addr constant [4 x i8] c"%d \00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %n1 = alloca i32, align 4
  %n2 = alloca i32, align 4
  %i = alloca i32, align 4
  %temp1 = alloca i32, align 4
  %temp2 = alloca i32, align 4
  %remainder = alloca i32, align 4
  %n = alloca i32, align 4
  %result = alloca i32, align 4
  store i32 0, i32* %retval
  store i32 0, i32* %n, align 4
  store i32 0, i32* %result, align 4
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str, i32 0, i32 0))
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0), i32* %n1, i32* %n2)
  %0 = load i32, i32* %n1, align 4
  %1 = load i32, i32* %n2, align 4
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.2, i32 0, i32 0), i32 %0, i32 %1)
  %2 = load i32, i32* %n1, align 4
  %add = add nsw i32 %2, 1
  store i32 %add, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %n2, align 4
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load i32, i32* %i, align 4
  store i32 %5, i32* %temp2, align 4
  %6 = load i32, i32* %i, align 4
  store i32 %6, i32* %temp1, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %for.body
  %7 = load i32, i32* %temp1, align 4
  %cmp3 = icmp ne i32 %7, 0
  br i1 %cmp3, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %8 = load i32, i32* %temp1, align 4
  %div = sdiv i32 %8, 10
  store i32 %div, i32* %temp1, align 4
  %9 = load i32, i32* %n, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %n, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond.4

while.cond.4:                                     ; preds = %while.body.6, %while.end
  %10 = load i32, i32* %temp2, align 4
  %cmp5 = icmp ne i32 %10, 0
  br i1 %cmp5, label %while.body.6, label %while.end.13

while.body.6:                                     ; preds = %while.cond.4
  %11 = load i32, i32* %temp2, align 4
  %rem = srem i32 %11, 10
  store i32 %rem, i32* %remainder, align 4
  %12 = load i32, i32* %remainder, align 4
  %conv = sitofp i32 %12 to double
  %13 = load i32, i32* %n, align 4
  %conv7 = sitofp i32 %13 to double
  %call8 = call double @pow(double %conv, double %conv7) #3
  %14 = load i32, i32* %result, align 4
  %conv9 = sitofp i32 %14 to double
  %add10 = fadd double %conv9, %call8
  %conv11 = fptosi double %add10 to i32
  store i32 %conv11, i32* %result, align 4
  %15 = load i32, i32* %temp2, align 4
  %div12 = sdiv i32 %15, 10
  store i32 %div12, i32* %temp2, align 4
  br label %while.cond.4

while.end.13:                                     ; preds = %while.cond.4
  %16 = load i32, i32* %result, align 4
  %17 = load i32, i32* %i, align 4
  %cmp14 = icmp eq i32 %16, %17
  br i1 %cmp14, label %if.then, label %if.end

if.then:                                          ; preds = %while.end.13
  %18 = load i32, i32* %i, align 4
  %call16 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.3, i32 0, i32 0), i32 %18)
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end.13
  store i32 0, i32* %n, align 4
  store i32 0, i32* %result, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %19 = load i32, i32* %i, align 4
  %inc17 = add nsw i32 %19, 1
  store i32 %inc17, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @__isoc99_scanf(i8*, ...) #1

; Function Attrs: nounwind
declare double @pow(double, double) #2

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
