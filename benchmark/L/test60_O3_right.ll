; ModuleID = 'test60.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [18 x i8] c"Enter string s1: \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%s\00", align 1
@.str.2 = private unnamed_addr constant [14 x i8] c"String s2: %s\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %s1 = alloca [100 x i8], align 16
  %s2 = alloca [100 x i8], align 16
  %0 = getelementptr inbounds [100 x i8], [100 x i8]* %s1, i64 0, i64 0
  call void @llvm.lifetime.start(i64 100, i8* %0) #1
  %1 = getelementptr inbounds [100 x i8], [100 x i8]* %s2, i64 0, i64 0
  call void @llvm.lifetime.start(i64 100, i8* %1) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i8* %0) #1
  %2 = load i8, i8* %0, align 16, !tbaa !1
  %cmp.15 = icmp eq i8 %2, 0
  br i1 %cmp.15, label %for.end, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.body
  %arrayidx818 = phi i8* [ %arrayidx8, %for.body ], [ %1, %for.body.preheader ]
  %3 = phi i8 [ %4, %for.body ], [ %2, %for.body.preheader ]
  %i.017 = phi i8 [ %inc, %for.body ], [ 0, %for.body.preheader ]
  store i8 %3, i8* %arrayidx818, align 1, !tbaa !1
  %inc = add i8 %i.017, 1
  %idxprom = sext i8 %inc to i64
  %arrayidx = getelementptr inbounds [100 x i8], [100 x i8]* %s1, i64 0, i64 %idxprom
  %4 = load i8, i8* %arrayidx, align 1, !tbaa !1
  %cmp = icmp eq i8 %4, 0
  %arrayidx8 = getelementptr inbounds [100 x i8], [100 x i8]* %s2, i64 0, i64 %idxprom
  br i1 %cmp, label %for.end.loopexit, label %for.body

for.end.loopexit:                                 ; preds = %for.body
  %arrayidx8.lcssa23 = phi i8* [ %arrayidx8, %for.body ]
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  %arrayidx8.lcssa = phi i8* [ %1, %entry ], [ %arrayidx8.lcssa23, %for.end.loopexit ]
  store i8 0, i8* %arrayidx8.lcssa, align 1, !tbaa !1
  %call10 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.2, i64 0, i64 0), i8* %1) #1
  call void @llvm.lifetime.end(i64 100, i8* %1) #1
  call void @llvm.lifetime.end(i64 100, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"omnipotent char", !3, i64 0}
!3 = !{!"Simple C/C++ TBAA"}
