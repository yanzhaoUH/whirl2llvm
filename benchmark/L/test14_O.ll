; ModuleID = 'test14_O.bc'

@.str = private constant [17 x i8] c"Enter a number: \00", align 1
@.str.1 = private constant [4 x i8] c"%lf\00", align 1
@.str.2 = private constant [15 x i8] c"You entered 0.\00", align 1
@.str.3 = private constant [31 x i8] c"You entered a negative number.\00", align 1
@.str.4 = private constant [31 x i8] c"You entered a positive number.\00", align 1

define i32 @main() {
entry:
  %.preg_F8_11_50 = alloca double
  %.preg_F8_11_49 = alloca double
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_dummy11_6 = alloca i32, align 4
  %_temp_dummy12_7 = alloca i32, align 4
  %_temp_dummy13_8 = alloca i32, align 4
  %_temp_dummy14_9 = alloca i32, align 4
  %_temp_ehpit0_10 = alloca i32, align 4
  %number_4 = alloca double, align 8
  %old_frame_pointer_15.addr = alloca i64, align 8
  %return_address_16.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast double* %number_4 to double*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, double* %2)
  %3 = load double, double* %number_4, align 8
  store double %3, double* %.preg_F8_11_49, align 8
  store double 0.000000e+00, double* %.preg_F8_11_50, align 8
  %4 = load double, double* %.preg_F8_11_49
  %5 = load double, double* %.preg_F8_11_50
  %cmp1 = fcmp ole double %4, %5
  br i1 %cmp1, label %tb, label %L2306

tb:                                               ; preds = %entry
  %6 = load double, double* %.preg_F8_11_49
  %7 = load double, double* %.preg_F8_11_50
  %cmp2 = fcmp oeq double %6, %7
  br i1 %cmp2, label %tb1, label %L2562

L2306:                                            ; preds = %entry
  %8 = bitcast i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.4, i32 0, i32 0) to i8*
  %call5 = call i32 (i8*, ...) @printf(i8* %8)
  br label %L3074

tb1:                                              ; preds = %tb
  %9 = bitcast i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %9)
  br label %L2818

L2562:                                            ; preds = %tb
  %10 = bitcast i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.3, i32 0, i32 0) to i8*
  %call4 = call i32 (i8*, ...) @printf(i8* %10)
  br label %L2818

L2818:                                            ; preds = %L2562, %tb1
  br label %L3074

L3074:                                            ; preds = %L2818, %L2306
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
