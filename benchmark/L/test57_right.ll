; ModuleID = 'test57.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [17 x i8] c"Enter a string: \00", align 1
@.str.1 = private unnamed_addr constant [16 x i8] c"Output String: \00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %line = alloca [150 x i8], align 16
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i32 0, i32 0))
  %arraydecay = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i32 0
  %call1 = call i32 (i8*, ...) bitcast (i32 (...)* @gets to i32 (i8*, ...)*)(i8* %arraydecay)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc.42, %entry
  %0 = load i32, i32* %i, align 4
  %idxprom = sext i32 %0 to i64
  %arrayidx = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom
  %1 = load i8, i8* %arrayidx, align 1
  %conv = sext i8 %1 to i32
  %cmp = icmp ne i32 %conv, 0
  br i1 %cmp, label %for.body, label %for.end.44

for.body:                                         ; preds = %for.cond
  br label %while.cond

while.cond:                                       ; preds = %for.end, %for.body
  %2 = load i32, i32* %i, align 4
  %idxprom3 = sext i32 %2 to i64
  %arrayidx4 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom3
  %3 = load i8, i8* %arrayidx4, align 1
  %conv5 = sext i8 %3 to i32
  %cmp6 = icmp sge i32 %conv5, 97
  br i1 %cmp6, label %land.lhs.true, label %lor.lhs.false

land.lhs.true:                                    ; preds = %while.cond
  %4 = load i32, i32* %i, align 4
  %idxprom8 = sext i32 %4 to i64
  %arrayidx9 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom8
  %5 = load i8, i8* %arrayidx9, align 1
  %conv10 = sext i8 %5 to i32
  %cmp11 = icmp sle i32 %conv10, 122
  br i1 %cmp11, label %lor.end, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true, %while.cond
  %6 = load i32, i32* %i, align 4
  %idxprom13 = sext i32 %6 to i64
  %arrayidx14 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom13
  %7 = load i8, i8* %arrayidx14, align 1
  %conv15 = sext i8 %7 to i32
  %cmp16 = icmp sge i32 %conv15, 65
  br i1 %cmp16, label %land.lhs.true.18, label %lor.rhs

land.lhs.true.18:                                 ; preds = %lor.lhs.false
  %8 = load i32, i32* %i, align 4
  %idxprom19 = sext i32 %8 to i64
  %arrayidx20 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom19
  %9 = load i8, i8* %arrayidx20, align 1
  %conv21 = sext i8 %9 to i32
  %cmp22 = icmp sle i32 %conv21, 90
  br i1 %cmp22, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %land.lhs.true.18, %lor.lhs.false
  %10 = load i32, i32* %i, align 4
  %idxprom24 = sext i32 %10 to i64
  %arrayidx25 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom24
  %11 = load i8, i8* %arrayidx25, align 1
  %conv26 = sext i8 %11 to i32
  %cmp27 = icmp eq i32 %conv26, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %land.lhs.true.18, %land.lhs.true
  %12 = phi i1 [ true, %land.lhs.true.18 ], [ true, %land.lhs.true ], [ %cmp27, %lor.rhs ]
  %lnot = xor i1 %12, true
  br i1 %lnot, label %while.body, label %while.end

while.body:                                       ; preds = %lor.end
  %13 = load i32, i32* %i, align 4
  store i32 %13, i32* %j, align 4
  br label %for.cond.29

for.cond.29:                                      ; preds = %for.inc, %while.body
  %14 = load i32, i32* %j, align 4
  %idxprom30 = sext i32 %14 to i64
  %arrayidx31 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom30
  %15 = load i8, i8* %arrayidx31, align 1
  %conv32 = sext i8 %15 to i32
  %cmp33 = icmp ne i32 %conv32, 0
  br i1 %cmp33, label %for.body.35, label %for.end

for.body.35:                                      ; preds = %for.cond.29
  %16 = load i32, i32* %j, align 4
  %add = add nsw i32 %16, 1
  %idxprom36 = sext i32 %add to i64
  %arrayidx37 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom36
  %17 = load i8, i8* %arrayidx37, align 1
  %18 = load i32, i32* %j, align 4
  %idxprom38 = sext i32 %18 to i64
  %arrayidx39 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom38
  store i8 %17, i8* %arrayidx39, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body.35
  %19 = load i32, i32* %j, align 4
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond.29

for.end:                                          ; preds = %for.cond.29
  %20 = load i32, i32* %j, align 4
  %idxprom40 = sext i32 %20 to i64
  %arrayidx41 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom40
  store i8 0, i8* %arrayidx41, align 1
  br label %while.cond

while.end:                                        ; preds = %lor.end
  br label %for.inc.42

for.inc.42:                                       ; preds = %while.end
  %21 = load i32, i32* %i, align 4
  %inc43 = add nsw i32 %21, 1
  store i32 %inc43, i32* %i, align 4
  br label %for.cond

for.end.44:                                       ; preds = %for.cond
  %call45 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.1, i32 0, i32 0))
  %arraydecay46 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i32 0
  %call47 = call i32 @puts(i8* %arraydecay46)
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @gets(...) #1

declare i32 @puts(i8*) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
