; ModuleID = 'test27.bc'

@.str = private constant [27 x i8] c"Enter a positive integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [22 x i8] c"%d is a prime number.\00", align 1
@.str.3 = private constant [26 x i8] c"%d is not a prime number.\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %flag_6 = alloca i32, align 4
  %i_5 = alloca i32, align 4
  %n_4 = alloca i32, align 4
  store i32 0, i32* %flag_6, align 4
  %0 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  store i32 2, i32* %i_5, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %3 = load i32, i32* %n_4, align 4
  %div = sdiv i32 %3, 2
  %4 = load i32, i32* %i_5, align 4
  %cmp1 = icmp sge i32 %div, %4
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = load i32, i32* %n_4, align 4
  %6 = load i32, i32* %i_5, align 4
  %srem = srem i32 %5, %6
  %cmp2 = icmp eq i32 %srem, 0
  br i1 %cmp2, label %if.then, label %if.else

while.end:                                        ; preds = %if.then, %while.cond
  br label %LABEL_258

if.then:                                          ; preds = %while.body
  store i32 1, i32* %flag_6, align 4
  br label %while.end

if.else:                                          ; preds = %while.body
  br label %if.end

if.end:                                           ; preds = %if.else
  %7 = load i32, i32* %i_5, align 4
  %add = add i32 %7, 1
  store i32 %add, i32* %i_5, align 4
  br label %while.cond

LABEL_258:                                        ; preds = %while.end
  %8 = load i32, i32* %flag_6, align 4
  %cmp3 = icmp eq i32 %8, 0
  br i1 %cmp3, label %if.then1, label %if.else2

if.then1:                                         ; preds = %LABEL_258
  %9 = bitcast i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.2, i32 0, i32 0) to i8*
  %10 = load i32, i32* %n_4, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %9, i32 %10)
  br label %if.end3

if.else2:                                         ; preds = %LABEL_258
  %11 = bitcast i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.3, i32 0, i32 0) to i8*
  %12 = load i32, i32* %n_4, align 4
  %call4 = call i32 (i8*, ...) @printf(i8* %11, i32 %12)
  br label %if.end3

if.end3:                                          ; preds = %if.else2, %if.then1
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
