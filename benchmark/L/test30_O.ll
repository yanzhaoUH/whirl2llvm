; ModuleID = 'test30_O.bc'

@.str = private constant [31 x i8] c"Enter two numbers(intervals): \00", align 1
@.str.1 = private constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private constant [41 x i8] c"Armstrong numbers between %d an %d are: \00", align 1
@.str.3 = private constant [4 x i8] c"%d \00", align 1
@_LIB_VERSION_58 = common global i32 0, align 4
@signgam_59 = common global i32 0, align 4

define i32 @main() {
entry:
  %.preg_F8_11_49 = alloca double
  %.preg_I4_4_57 = alloca i32
  %.preg_I4_4_65 = alloca i32
  %.preg_F8_11_58 = alloca double
  %.preg_U4_8_60 = alloca i32
  %.preg_I4_4_51 = alloca i32
  %.preg_I4_4_63 = alloca i32
  %.preg_I4_4_62 = alloca i32
  %.preg_I4_4_55 = alloca i32
  %.preg_I4_4_54 = alloca i32
  %.preg_I4_4_61 = alloca i32
  %.preg_I4_4_66 = alloca i32
  %_temp_dummy10_12 = alloca i32, align 4
  %_temp_dummy11_13 = alloca i32, align 4
  %_temp_dummy12_14 = alloca i32, align 4
  %_temp_dummy13_15 = alloca i32, align 4
  %_temp_ehpit0_16 = alloca i32, align 4
  %i_6 = alloca i32, align 4
  %n1_4 = alloca i32, align 4
  %n2_5 = alloca i32, align 4
  %n_10 = alloca i32, align 4
  %old_frame_pointer_21.addr = alloca i64, align 8
  %remainder_9 = alloca i32, align 4
  %result_11 = alloca i32, align 4
  %return_address_22.addr = alloca i64, align 8
  %temp1_7 = alloca i32, align 4
  %temp2_8 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n1_4 to i32*
  %3 = bitcast i32* %n2_5 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2, i32* %3)
  %4 = bitcast i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.2, i32 0, i32 0) to i8*
  %5 = load i32, i32* %n1_4, align 4
  %6 = load i32, i32* %n2_5, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %4, i32 %5, i32 %6)
  %7 = load i32, i32* %n1_4, align 4
  store i32 %7, i32* %.preg_I4_4_66, align 8
  %8 = load i32, i32* %.preg_I4_4_66
  %add = add i32 %8, 1
  store i32 %add, i32* %.preg_I4_4_61, align 8
  store i32 0, i32* %.preg_I4_4_54, align 8
  %9 = load i32, i32* %n2_5, align 4
  %10 = load i32, i32* %.preg_I4_4_66
  %add.1 = sub i32 %9, %10
  %add.2 = add i32 %add.1, -1
  %cmp1 = icmp sgt i32 %add.2, 0
  br i1 %cmp1, label %tb, label %L5890

tb:                                               ; preds = %entry
  %11 = load i32, i32* %.preg_I4_4_61
  store i32 %11, i32* %.preg_I4_4_55, align 8
  br label %L6402

L5890:                                            ; preds = %fb16, %entry
  ret i32 0

L6402:                                            ; preds = %L9730, %tb
  %12 = load i32, i32* %.preg_I4_4_55
  store i32 %12, i32* %.preg_I4_4_62, align 8
  %13 = load i32, i32* %.preg_I4_4_55
  store i32 %13, i32* %.preg_I4_4_63, align 8
  store i32 0, i32* %.preg_I4_4_51, align 8
  %14 = load i32, i32* %.preg_I4_4_55
  %cmp2 = icmp ne i32 %14, 0
  %conv3 = zext i1 %cmp2 to i32
  store i32 %conv3, i32* %.preg_U4_8_60, align 8
  %15 = load i32, i32* %.preg_U4_8_60
  %conv = trunc i32 %15 to i1
  br i1 %conv, label %tb3, label %L6658

tb3:                                              ; preds = %L6402
  br label %L7170

L6658:                                            ; preds = %fb, %L6402
  %16 = load i32, i32* %.preg_U4_8_60
  %conv6 = trunc i32 %16 to i1
  br i1 %conv6, label %tb5, label %L9986

L7170:                                            ; preds = %L7170, %tb3
  %17 = load i32, i32* %.preg_I4_4_63
  %div = sdiv i32 %17, 10
  store i32 %div, i32* %.preg_I4_4_63, align 8
  %18 = load i32, i32* %.preg_I4_4_51
  %add.4 = add i32 %18, 1
  store i32 %add.4, i32* %.preg_I4_4_51, align 8
  %19 = load i32, i32* %.preg_I4_4_63
  %cmp3 = icmp ne i32 %19, 0
  br i1 %cmp3, label %L7170, label %fb

fb:                                               ; preds = %L7170
  br label %L6658

tb5:                                              ; preds = %L6658
  %20 = load i32, i32* %.preg_I4_4_51
  %conv9 = sitofp i32 %20 to double
  store double %conv9, double* %.preg_F8_11_58, align 8
  store i32 0, i32* %.preg_I4_4_65, align 8
  br label %L8194

L9986:                                            ; preds = %L6658
  br label %L10242

L8194:                                            ; preds = %L8194, %tb5
  %21 = load i32, i32* %.preg_I4_4_62
  %lowPart_4 = alloca i32, align 4
  %div.7 = sdiv i32 %21, 10
  %srem = srem i32 %21, 10
  store i32 %div.7, i32* %lowPart_4, align 4
  store i32 %srem, i32* %.preg_I4_4_57, align 8
  %22 = load i32, i32* %.preg_I4_4_57
  %conv98 = sitofp i32 %22 to double
  %23 = load double, double* %.preg_F8_11_58
  %call4 = call double @pow(double %conv98, double %23)
  store double %call4, double* %.preg_F8_11_49, align 8
  %24 = load i32, i32* %.preg_I4_4_65
  %conv99 = sitofp i32 %24 to double
  %25 = load double, double* %.preg_F8_11_49
  %add.10 = fadd double %conv99, %25
  %fp2int = fptosi double %add.10 to i32
  store i32 %fp2int, i32* %.preg_I4_4_65, align 8
  %26 = load i32, i32* %lowPart_4
  store i32 %26, i32* %.preg_I4_4_62, align 8
  %27 = load i32, i32* %.preg_I4_4_62
  %cmp4 = icmp ne i32 %27, 0
  br i1 %cmp4, label %L8194, label %fb11

fb11:                                             ; preds = %L8194
  %28 = load i32, i32* %.preg_I4_4_65
  %29 = load i32, i32* %.preg_I4_4_55
  %cmp5 = icmp eq i32 %28, %29
  br i1 %cmp5, label %tb12, label %L9730

tb12:                                             ; preds = %fb11
  br label %L10242

L9730:                                            ; preds = %L10242, %fb11
  %30 = load i32, i32* %.preg_I4_4_54
  %add.13 = add i32 %30, 1
  store i32 %add.13, i32* %.preg_I4_4_54, align 8
  %31 = load i32, i32* %.preg_I4_4_55
  %add.14 = add i32 %31, 1
  store i32 %add.14, i32* %.preg_I4_4_55, align 8
  %32 = load i32, i32* %.preg_I4_4_54
  %33 = load i32, i32* %n2_5, align 4
  %34 = load i32, i32* %.preg_I4_4_61
  %add.15 = sub i32 %33, %34
  %cmp6 = icmp slt i32 %32, %add.15
  br i1 %cmp6, label %L6402, label %fb16

L10242:                                           ; preds = %tb12, %L9986
  %35 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.3, i32 0, i32 0) to i8*
  %36 = load i32, i32* %.preg_I4_4_55
  %call5 = call i32 (i8*, ...) @printf(i8* %35, i32 %36)
  br label %L9730

fb16:                                             ; preds = %L9730
  br label %L5890
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

declare double @pow(double, double)
