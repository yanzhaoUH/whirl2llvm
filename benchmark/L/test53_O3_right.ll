; ModuleID = 'test53.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [32 x i8] c"Enter a, b and c respectively: \00", align 1
@.str.1 = private unnamed_addr constant [9 x i8] c"%d %d %d\00", align 1
@.str.3 = private unnamed_addr constant [24 x i8] c"a = %d \0Ab = %d \0Ac = %d\0A\00", align 1
@.str.5 = private unnamed_addr constant [23 x i8] c"a = %d \0Ab = %d \0Ac = %d\00", align 1
@str = private unnamed_addr constant [23 x i8] c"Value before swapping:\00"
@str.6 = private unnamed_addr constant [22 x i8] c"Value after swapping:\00"

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %a = alloca i32, align 4
  %b = alloca i32, align 4
  %c = alloca i32, align 4
  %0 = bitcast i32* %a to i8*
  call void @llvm.lifetime.start(i64 4, i8* %0) #1
  %1 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start(i64 4, i8* %1) #1
  %2 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start(i64 4, i8* %2) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %a, i32* nonnull %b, i32* nonnull %c) #1
  %puts = call i32 @puts(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @str, i64 0, i64 0))
  %3 = load i32, i32* %a, align 4, !tbaa !1
  %4 = load i32, i32* %b, align 4, !tbaa !1
  %5 = load i32, i32* %c, align 4, !tbaa !1
  %call3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.3, i64 0, i64 0), i32 %3, i32 %4, i32 %5) #1
  %6 = load i32, i32* %b, align 4, !tbaa !1
  %7 = load i32, i32* %a, align 4, !tbaa !1
  store i32 %7, i32* %b, align 4, !tbaa !1
  %8 = load i32, i32* %c, align 4, !tbaa !1
  store i32 %8, i32* %a, align 4, !tbaa !1
  store i32 %6, i32* %c, align 4, !tbaa !1
  %puts6 = call i32 @puts(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @str.6, i64 0, i64 0))
  %9 = load i32, i32* %a, align 4, !tbaa !1
  %10 = load i32, i32* %b, align 4, !tbaa !1
  %11 = load i32, i32* %c, align 4, !tbaa !1
  %call5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.5, i64 0, i64 0), i32 %9, i32 %10, i32 %11) #1
  call void @llvm.lifetime.end(i64 4, i8* %2) #1
  call void @llvm.lifetime.end(i64 4, i8* %1) #1
  call void @llvm.lifetime.end(i64 4, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind uwtable
define void @cyclicSwap(i32* nocapture %a, i32* nocapture %b, i32* nocapture %c) #0 {
entry:
  %0 = load i32, i32* %b, align 4, !tbaa !1
  %1 = load i32, i32* %a, align 4, !tbaa !1
  store i32 %1, i32* %b, align 4, !tbaa !1
  %2 = load i32, i32* %c, align 4, !tbaa !1
  store i32 %2, i32* %a, align 4, !tbaa !1
  store i32 %0, i32* %c, align 4, !tbaa !1
  ret void
}

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @puts(i8* nocapture readonly) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
