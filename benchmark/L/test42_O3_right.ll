; ModuleID = 'test42.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str.3 = private unnamed_addr constant [3 x i8] c"%c\00", align 1
@.str.4 = private unnamed_addr constant [24 x i8] c"Enter a binary number: \00", align 1
@.str.5 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.6 = private unnamed_addr constant [27 x i8] c"%d in binary = %d in octal\00", align 1
@.str.7 = private unnamed_addr constant [23 x i8] c"Enter a octal number: \00", align 1
@.str.8 = private unnamed_addr constant [27 x i8] c"%d in octal = %d in binary\00", align 1
@str = private unnamed_addr constant [14 x i8] c"Instructions:\00"
@str.9 = private unnamed_addr constant [50 x i8] c"1. Enter alphabet 'o' to convert binary to octal.\00"
@str.10 = private unnamed_addr constant [50 x i8] c"2. Enter alphabet 'b' to convert octal to binary.\00"

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %n = alloca i32, align 4
  %c = alloca i8, align 1
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start(i64 4, i8* %0) #1
  call void @llvm.lifetime.start(i64 1, i8* nonnull %c) #1
  %puts = tail call i32 @puts(i8* getelementptr inbounds ([14 x i8], [14 x i8]* @str, i64 0, i64 0))
  %puts25 = tail call i32 @puts(i8* getelementptr inbounds ([50 x i8], [50 x i8]* @str.9, i64 0, i64 0))
  %puts26 = tail call i32 @puts(i8* getelementptr inbounds ([50 x i8], [50 x i8]* @str.10, i64 0, i64 0))
  %call3 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.3, i64 0, i64 0), i8* nonnull %c) #1
  %1 = load i8, i8* %c, align 1, !tbaa !1
  switch i8 %1, label %if.end [
    i8 111, label %if.then
    i8 79, label %if.then
  ]

if.then:                                          ; preds = %entry, %entry
  %call8 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.4, i64 0, i64 0)) #1
  %call9 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.5, i64 0, i64 0), i32* nonnull %n) #1
  %2 = load i32, i32* %n, align 4, !tbaa !4
  %cmp.30.i = icmp eq i32 %2, 0
  br i1 %cmp.30.i, label %binary_octal.exit, label %while.body.i.preheader

while.body.i.preheader:                           ; preds = %if.then
  br label %while.body.i

while.cond.4.preheader.i:                         ; preds = %while.body.i
  %conv3.i.lcssa = phi i32 [ %conv3.i, %while.body.i ]
  %cmp5.26.i = icmp eq i32 %conv3.i.lcssa, 0
  br i1 %cmp5.26.i, label %binary_octal.exit, label %while.body.7.i.preheader

while.body.7.i.preheader:                         ; preds = %while.cond.4.preheader.i
  br label %while.body.7.i

while.body.i:                                     ; preds = %while.body.i.preheader, %while.body.i
  %i.033.i = phi i32 [ %inc.i, %while.body.i ], [ 0, %while.body.i.preheader ]
  %decimal.032.i = phi i32 [ %conv3.i, %while.body.i ], [ 0, %while.body.i.preheader ]
  %n.addr.031.i = phi i32 [ %div.i, %while.body.i ], [ %2, %while.body.i.preheader ]
  %rem.i = srem i32 %n.addr.031.i, 10
  %conv.i = sitofp i32 %rem.i to double
  %3 = call double @ldexp(double 1.000000e+00, i32 %i.033.i) #1
  %mul.i = fmul double %conv.i, %3
  %conv2.i = sitofp i32 %decimal.032.i to double
  %add.i = fadd double %conv2.i, %mul.i
  %conv3.i = fptosi double %add.i to i32
  %inc.i = add nuw nsw i32 %i.033.i, 1
  %div.i = sdiv i32 %n.addr.031.i, 10
  %n.addr.031.off.i = add i32 %n.addr.031.i, 9
  %4 = icmp ult i32 %n.addr.031.off.i, 19
  br i1 %4, label %while.cond.4.preheader.i, label %while.body.i

while.body.7.i:                                   ; preds = %while.body.7.i.preheader, %while.body.7.i
  %i.129.i = phi i32 [ %mul12.i, %while.body.7.i ], [ 1, %while.body.7.i.preheader ]
  %decimal.128.i = phi i32 [ %div11.i, %while.body.7.i ], [ %conv3.i.lcssa, %while.body.7.i.preheader ]
  %octal.027.i = phi i32 [ %add10.i, %while.body.7.i ], [ 0, %while.body.7.i.preheader ]
  %rem8.i = srem i32 %decimal.128.i, 8
  %mul9.i = mul nsw i32 %rem8.i, %i.129.i
  %add10.i = add nsw i32 %mul9.i, %octal.027.i
  %div11.i = sdiv i32 %decimal.128.i, 8
  %mul12.i = mul nsw i32 %i.129.i, 10
  %decimal.128.off.i = add i32 %decimal.128.i, 7
  %5 = icmp ult i32 %decimal.128.off.i, 15
  br i1 %5, label %binary_octal.exit.loopexit, label %while.body.7.i

binary_octal.exit.loopexit:                       ; preds = %while.body.7.i
  %add10.i.lcssa = phi i32 [ %add10.i, %while.body.7.i ]
  br label %binary_octal.exit

binary_octal.exit:                                ; preds = %binary_octal.exit.loopexit, %if.then, %while.cond.4.preheader.i
  %octal.0.lcssa.i = phi i32 [ 0, %while.cond.4.preheader.i ], [ 0, %if.then ], [ %add10.i.lcssa, %binary_octal.exit.loopexit ]
  %call11 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.6, i64 0, i64 0), i32 %2, i32 %octal.0.lcssa.i) #1
  %.pr = load i8, i8* %c, align 1, !tbaa !1
  br label %if.end

if.end:                                           ; preds = %entry, %binary_octal.exit
  %6 = phi i8 [ %1, %entry ], [ %.pr, %binary_octal.exit ]
  switch i8 %6, label %if.end.24 [
    i8 98, label %if.then.19
    i8 66, label %if.then.19
  ]

if.then.19:                                       ; preds = %if.end, %if.end
  %call20 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.7, i64 0, i64 0)) #1
  %call21 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.5, i64 0, i64 0), i32* nonnull %n) #1
  %7 = load i32, i32* %n, align 4, !tbaa !4
  %cmp.30.i.27 = icmp eq i32 %7, 0
  br i1 %cmp.30.i.27, label %octal_binary.exit, label %while.body.i.42.preheader

while.body.i.42.preheader:                        ; preds = %if.then.19
  br label %while.body.i.42

while.cond.4.preheader.i.29:                      ; preds = %while.body.i.42
  %conv3.i.38.lcssa = phi i32 [ %conv3.i.38, %while.body.i.42 ]
  %cmp5.26.i.28 = icmp eq i32 %conv3.i.38.lcssa, 0
  br i1 %cmp5.26.i.28, label %octal_binary.exit, label %while.body.7.i.49.preheader

while.body.7.i.49.preheader:                      ; preds = %while.cond.4.preheader.i.29
  br label %while.body.7.i.49

while.body.i.42:                                  ; preds = %while.body.i.42.preheader, %while.body.i.42
  %i.033.i.30 = phi i32 [ %inc.i.39, %while.body.i.42 ], [ 0, %while.body.i.42.preheader ]
  %decimal.032.i.31 = phi i32 [ %conv3.i.38, %while.body.i.42 ], [ 0, %while.body.i.42.preheader ]
  %n.addr.031.i.32 = phi i32 [ %div.i.40, %while.body.i.42 ], [ %7, %while.body.i.42.preheader ]
  %rem.i.33 = srem i32 %n.addr.031.i.32, 10
  %conv.i.34 = sitofp i32 %rem.i.33 to double
  %conv1.i = sitofp i32 %i.033.i.30 to double
  %call.i = call double @pow(double 8.000000e+00, double %conv1.i) #1
  %mul.i.35 = fmul double %conv.i.34, %call.i
  %conv2.i.36 = sitofp i32 %decimal.032.i.31 to double
  %add.i.37 = fadd double %conv2.i.36, %mul.i.35
  %conv3.i.38 = fptosi double %add.i.37 to i32
  %inc.i.39 = add nuw nsw i32 %i.033.i.30, 1
  %div.i.40 = sdiv i32 %n.addr.031.i.32, 10
  %n.addr.031.off.i.41 = add i32 %n.addr.031.i.32, 9
  %8 = icmp ult i32 %n.addr.031.off.i.41, 19
  br i1 %8, label %while.cond.4.preheader.i.29, label %while.body.i.42

while.body.7.i.49:                                ; preds = %while.body.7.i.49.preheader, %while.body.7.i.49
  %i.129.i.43 = phi i32 [ %mul12.i.48, %while.body.7.i.49 ], [ 1, %while.body.7.i.49.preheader ]
  %binary.028.i = phi i32 [ %add10.i.46, %while.body.7.i.49 ], [ 0, %while.body.7.i.49.preheader ]
  %decimal.127.i = phi i32 [ %div11.i.47, %while.body.7.i.49 ], [ %conv3.i.38.lcssa, %while.body.7.i.49.preheader ]
  %rem8.i.44 = srem i32 %decimal.127.i, 2
  %mul9.i.45 = mul nsw i32 %rem8.i.44, %i.129.i.43
  %add10.i.46 = add nsw i32 %mul9.i.45, %binary.028.i
  %div11.i.47 = sdiv i32 %decimal.127.i, 2
  %mul12.i.48 = mul nsw i32 %i.129.i.43, 10
  %decimal.127.off.i = add i32 %decimal.127.i, 1
  %9 = icmp ult i32 %decimal.127.off.i, 3
  br i1 %9, label %octal_binary.exit.loopexit, label %while.body.7.i.49

octal_binary.exit.loopexit:                       ; preds = %while.body.7.i.49
  %add10.i.46.lcssa = phi i32 [ %add10.i.46, %while.body.7.i.49 ]
  br label %octal_binary.exit

octal_binary.exit:                                ; preds = %octal_binary.exit.loopexit, %if.then.19, %while.cond.4.preheader.i.29
  %binary.0.lcssa.i = phi i32 [ 0, %while.cond.4.preheader.i.29 ], [ 0, %if.then.19 ], [ %add10.i.46.lcssa, %octal_binary.exit.loopexit ]
  %call23 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.8, i64 0, i64 0), i32 %7, i32 %binary.0.lcssa.i) #1
  br label %if.end.24

if.end.24:                                        ; preds = %if.end, %octal_binary.exit
  call void @llvm.lifetime.end(i64 1, i8* nonnull %c) #1
  call void @llvm.lifetime.end(i64 4, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind uwtable
define i32 @binary_octal(i32 %n) #0 {
entry:
  %cmp.30 = icmp eq i32 %n, 0
  br i1 %cmp.30, label %while.end.13, label %while.body.preheader

while.body.preheader:                             ; preds = %entry
  br label %while.body

while.cond.4.preheader:                           ; preds = %while.body
  %conv3.lcssa = phi i32 [ %conv3, %while.body ]
  %cmp5.26 = icmp eq i32 %conv3.lcssa, 0
  br i1 %cmp5.26, label %while.end.13, label %while.body.7.preheader

while.body.7.preheader:                           ; preds = %while.cond.4.preheader
  br label %while.body.7

while.body:                                       ; preds = %while.body.preheader, %while.body
  %i.033 = phi i32 [ %inc, %while.body ], [ 0, %while.body.preheader ]
  %decimal.032 = phi i32 [ %conv3, %while.body ], [ 0, %while.body.preheader ]
  %n.addr.031 = phi i32 [ %div, %while.body ], [ %n, %while.body.preheader ]
  %rem = srem i32 %n.addr.031, 10
  %conv = sitofp i32 %rem to double
  %0 = tail call double @ldexp(double 1.000000e+00, i32 %i.033) #1
  %mul = fmul double %conv, %0
  %conv2 = sitofp i32 %decimal.032 to double
  %add = fadd double %conv2, %mul
  %conv3 = fptosi double %add to i32
  %inc = add nuw nsw i32 %i.033, 1
  %div = sdiv i32 %n.addr.031, 10
  %n.addr.031.off = add i32 %n.addr.031, 9
  %1 = icmp ult i32 %n.addr.031.off, 19
  br i1 %1, label %while.cond.4.preheader, label %while.body

while.body.7:                                     ; preds = %while.body.7.preheader, %while.body.7
  %i.129 = phi i32 [ %mul12, %while.body.7 ], [ 1, %while.body.7.preheader ]
  %decimal.128 = phi i32 [ %div11, %while.body.7 ], [ %conv3.lcssa, %while.body.7.preheader ]
  %octal.027 = phi i32 [ %add10, %while.body.7 ], [ 0, %while.body.7.preheader ]
  %rem8 = srem i32 %decimal.128, 8
  %mul9 = mul nsw i32 %rem8, %i.129
  %add10 = add nsw i32 %mul9, %octal.027
  %div11 = sdiv i32 %decimal.128, 8
  %mul12 = mul nsw i32 %i.129, 10
  %decimal.128.off = add i32 %decimal.128, 7
  %2 = icmp ult i32 %decimal.128.off, 15
  br i1 %2, label %while.end.13.loopexit, label %while.body.7

while.end.13.loopexit:                            ; preds = %while.body.7
  %add10.lcssa = phi i32 [ %add10, %while.body.7 ]
  br label %while.end.13

while.end.13:                                     ; preds = %while.end.13.loopexit, %entry, %while.cond.4.preheader
  %octal.0.lcssa = phi i32 [ 0, %while.cond.4.preheader ], [ 0, %entry ], [ %add10.lcssa, %while.end.13.loopexit ]
  ret i32 %octal.0.lcssa
}

; Function Attrs: nounwind uwtable
define i32 @octal_binary(i32 %n) #0 {
entry:
  %cmp.30 = icmp eq i32 %n, 0
  br i1 %cmp.30, label %while.end.13, label %while.body.preheader

while.body.preheader:                             ; preds = %entry
  br label %while.body

while.cond.4.preheader:                           ; preds = %while.body
  %conv3.lcssa = phi i32 [ %conv3, %while.body ]
  %cmp5.26 = icmp eq i32 %conv3.lcssa, 0
  br i1 %cmp5.26, label %while.end.13, label %while.body.7.preheader

while.body.7.preheader:                           ; preds = %while.cond.4.preheader
  br label %while.body.7

while.body:                                       ; preds = %while.body.preheader, %while.body
  %i.033 = phi i32 [ %inc, %while.body ], [ 0, %while.body.preheader ]
  %decimal.032 = phi i32 [ %conv3, %while.body ], [ 0, %while.body.preheader ]
  %n.addr.031 = phi i32 [ %div, %while.body ], [ %n, %while.body.preheader ]
  %rem = srem i32 %n.addr.031, 10
  %conv = sitofp i32 %rem to double
  %conv1 = sitofp i32 %i.033 to double
  %call = tail call double @pow(double 8.000000e+00, double %conv1) #1
  %mul = fmul double %conv, %call
  %conv2 = sitofp i32 %decimal.032 to double
  %add = fadd double %conv2, %mul
  %conv3 = fptosi double %add to i32
  %inc = add nuw nsw i32 %i.033, 1
  %div = sdiv i32 %n.addr.031, 10
  %n.addr.031.off = add i32 %n.addr.031, 9
  %0 = icmp ult i32 %n.addr.031.off, 19
  br i1 %0, label %while.cond.4.preheader, label %while.body

while.body.7:                                     ; preds = %while.body.7.preheader, %while.body.7
  %i.129 = phi i32 [ %mul12, %while.body.7 ], [ 1, %while.body.7.preheader ]
  %binary.028 = phi i32 [ %add10, %while.body.7 ], [ 0, %while.body.7.preheader ]
  %decimal.127 = phi i32 [ %div11, %while.body.7 ], [ %conv3.lcssa, %while.body.7.preheader ]
  %rem8 = srem i32 %decimal.127, 2
  %mul9 = mul nsw i32 %rem8, %i.129
  %add10 = add nsw i32 %mul9, %binary.028
  %div11 = sdiv i32 %decimal.127, 2
  %mul12 = mul nsw i32 %i.129, 10
  %decimal.127.off = add i32 %decimal.127, 1
  %1 = icmp ult i32 %decimal.127.off, 3
  br i1 %1, label %while.end.13.loopexit, label %while.body.7

while.end.13.loopexit:                            ; preds = %while.body.7
  %add10.lcssa = phi i32 [ %add10, %while.body.7 ]
  br label %while.end.13

while.end.13:                                     ; preds = %while.end.13.loopexit, %entry, %while.cond.4.preheader
  %binary.0.lcssa = phi i32 [ 0, %while.cond.4.preheader ], [ 0, %entry ], [ %add10.lcssa, %while.end.13.loopexit ]
  ret i32 %binary.0.lcssa
}

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare double @pow(double, double) #2

; Function Attrs: nounwind
declare i32 @puts(i8* nocapture readonly) #1

declare double @ldexp(double, i32)

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"omnipotent char", !3, i64 0}
!3 = !{!"Simple C/C++ TBAA"}
!4 = !{!5, !5, i64 0}
!5 = !{!"int", !2, i64 0}
