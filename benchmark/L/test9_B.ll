; ModuleID = 'test9.bc'

@.str = private constant [19 x i8] c"Enter an integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [12 x i8] c"%d is even.\00", align 1
@.str.3 = private constant [11 x i8] c"%d is odd.\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_dummy11_6 = alloca i32, align 4
  %_temp_dummy12_7 = alloca i32, align 4
  %_temp_dummy13_8 = alloca i32, align 4
  %number_4 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %number_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = load i32, i32* %number_4, align 4
  %and = and i32 %3, 1
  %cmp1 = icmp eq i32 %and, 0
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = bitcast i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.2, i32 0, i32 0) to i8*
  %5 = load i32, i32* %number_4, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %4, i32 %5)
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = bitcast i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.3, i32 0, i32 0) to i8*
  %7 = load i32, i32* %number_4, align 4
  %call4 = call i32 (i8*, ...) @printf(i8* %6, i32 %7)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
