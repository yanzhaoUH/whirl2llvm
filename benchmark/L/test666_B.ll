; ModuleID = 'test666.bc'

@.str = private constant [15 x i8] c"hello world 1\0A\00", align 1
@.str.1 = private constant [15 x i8] c"hello world 2\0A\00", align 1
@_LIB_VERSION_54 = common global i32 0, align 4
@signgam_55 = common global i32 0, align 4

define i32 @main() {
entry:
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %i_4 = alloca i32, align 4
  %j_5 = alloca i32, align 4
  store i32 3, i32* %i_4, align 4
  store i32 4, i32* %j_5, align 4
  %0 = load i32, i32* %i_4, align 4
  %cmp1 = icmp sgt i32 %0, 1
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %j_5, align 4
  %cmp2 = icmp sgt i32 %1, 3
  br i1 %cmp2, label %if.then1, label %if.else2

if.else:                                          ; preds = %entry
  br label %if.end3

if.then1:                                         ; preds = %if.then
  %2 = bitcast i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %2)
  br label %if.end

if.else2:                                         ; preds = %if.then
  br label %if.end

if.end:                                           ; preds = %if.else2, %if.then1
  br label %if.end3

if.end3:                                          ; preds = %if.end, %if.else
  %3 = load i32, i32* %j_5, align 4
  %cmp3 = icmp sgt i32 %3, 1
  br i1 %cmp3, label %if.then4, label %if.else5

if.then4:                                         ; preds = %if.end3
  %4 = bitcast i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call2 = call i32 (i8*, ...) @printf(i8* %4)
  br label %if.end6

if.else5:                                         ; preds = %if.end3
  br label %if.end6

if.end6:                                          ; preds = %if.else5, %if.then4
  ret i32 0
}

declare i32 @printf(i8*, ...)
