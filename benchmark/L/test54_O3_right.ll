; ModuleID = 'test54.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [43 x i8] c"Enter total number of elements(1 to 100): \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.2 = private unnamed_addr constant [31 x i8] c"Error!!! memory not allocated.\00", align 1
@.str.4 = private unnamed_addr constant [18 x i8] c"Enter Number %d: \00", align 1
@.str.5 = private unnamed_addr constant [23 x i8] c"Largest element = %.2f\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %num = alloca i32, align 4
  %0 = bitcast i32* %num to i8*
  call void @llvm.lifetime.start(i64 4, i8* %0) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([43 x i8], [43 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %num) #1
  %1 = load i32, i32* %num, align 4, !tbaa !1
  %conv = sext i32 %1 to i64
  %call2 = call noalias i8* @calloc(i64 %conv, i64 4) #1
  %2 = bitcast i8* %call2 to float*
  %cmp = icmp eq i8* %call2, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.2, i64 0, i64 0)) #1
  call void @exit(i32 0) #4
  unreachable

if.end:                                           ; preds = %entry
  %putchar = call i32 @putchar(i32 10) #1
  %3 = load i32, i32* %num, align 4, !tbaa !1
  %cmp6.44 = icmp sgt i32 %3, 0
  br i1 %cmp6.44, label %for.body.preheader, label %for.end.24

for.body.preheader:                               ; preds = %if.end
  br label %for.body

for.cond.10.preheader:                            ; preds = %for.body
  %.lcssa57 = phi i32 [ %12, %for.body ]
  %.pre = load float, float* %2, align 4, !tbaa !5
  %cmp11.42 = icmp sgt i32 %.lcssa57, 1
  br i1 %cmp11.42, label %for.body.13.lr.ph, label %for.end.24

for.body.13.lr.ph:                                ; preds = %for.cond.10.preheader
  %4 = sext i32 %.lcssa57 to i64
  %5 = and i32 %.lcssa57, 1
  %lcmp.mod = icmp eq i32 %5, 0
  br i1 %lcmp.mod, label %for.body.13.prol, label %for.body.13.lr.ph.split

for.body.13.prol:                                 ; preds = %for.body.13.lr.ph
  %add.ptr15.prol = getelementptr inbounds i8, i8* %call2, i64 4
  %6 = bitcast i8* %add.ptr15.prol to float*
  %7 = load float, float* %6, align 4, !tbaa !5
  %cmp16.prol = fcmp olt float %.pre, %7
  br i1 %cmp16.prol, label %if.then.18.prol, label %for.inc.22.prol

if.then.18.prol:                                  ; preds = %for.body.13.prol
  store float %7, float* %2, align 4, !tbaa !5
  br label %for.inc.22.prol

for.inc.22.prol:                                  ; preds = %if.then.18.prol, %for.body.13.prol
  %8 = phi float [ %.pre, %for.body.13.prol ], [ %7, %if.then.18.prol ]
  br label %for.body.13.lr.ph.split

for.body.13.lr.ph.split:                          ; preds = %for.inc.22.prol, %for.body.13.lr.ph
  %indvars.iv.unr = phi i64 [ 1, %for.body.13.lr.ph ], [ 2, %for.inc.22.prol ]
  %.unr = phi float [ %.pre, %for.body.13.lr.ph ], [ %8, %for.inc.22.prol ]
  %.lcssa54.unr = phi float [ undef, %for.body.13.lr.ph ], [ %8, %for.inc.22.prol ]
  %9 = icmp eq i32 %.lcssa57, 2
  br i1 %9, label %for.end.24.loopexit, label %for.body.13.lr.ph.split.split

for.body.13.lr.ph.split.split:                    ; preds = %for.body.13.lr.ph.split
  br label %for.body.13

for.body:                                         ; preds = %for.body.preheader, %for.body
  %indvars.iv47 = phi i64 [ %indvars.iv.next48, %for.body ], [ 0, %for.body.preheader ]
  %indvars.iv.next48 = add nuw nsw i64 %indvars.iv47, 1
  %10 = trunc i64 %indvars.iv.next48 to i32
  %call8 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.4, i64 0, i64 0), i32 %10) #1
  %11 = trunc i64 %indvars.iv47 to i32
  %conv9 = sitofp i32 %11 to float
  %add.ptr = getelementptr inbounds float, float* %2, i64 %indvars.iv47
  store float %conv9, float* %add.ptr, align 4, !tbaa !5
  %12 = load i32, i32* %num, align 4, !tbaa !1
  %13 = sext i32 %12 to i64
  %cmp6 = icmp slt i64 %indvars.iv.next48, %13
  br i1 %cmp6, label %for.body, label %for.cond.10.preheader

for.body.13:                                      ; preds = %for.inc.22.1, %for.body.13.lr.ph.split.split
  %indvars.iv = phi i64 [ %indvars.iv.unr, %for.body.13.lr.ph.split.split ], [ %indvars.iv.next.1, %for.inc.22.1 ]
  %14 = phi float [ %.unr, %for.body.13.lr.ph.split.split ], [ %18, %for.inc.22.1 ]
  %add.ptr15 = getelementptr inbounds float, float* %2, i64 %indvars.iv
  %15 = load float, float* %add.ptr15, align 4, !tbaa !5
  %cmp16 = fcmp olt float %14, %15
  br i1 %cmp16, label %if.then.18, label %for.inc.22

if.then.18:                                       ; preds = %for.body.13
  store float %15, float* %2, align 4, !tbaa !5
  br label %for.inc.22

for.inc.22:                                       ; preds = %for.body.13, %if.then.18
  %16 = phi float [ %14, %for.body.13 ], [ %15, %if.then.18 ]
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %add.ptr15.1 = getelementptr inbounds float, float* %2, i64 %indvars.iv.next
  %17 = load float, float* %add.ptr15.1, align 4, !tbaa !5
  %cmp16.1 = fcmp olt float %16, %17
  br i1 %cmp16.1, label %if.then.18.1, label %for.inc.22.1

for.end.24.loopexit.unr-lcssa:                    ; preds = %for.inc.22.1
  %.lcssa56 = phi float [ %18, %for.inc.22.1 ]
  br label %for.end.24.loopexit

for.end.24.loopexit:                              ; preds = %for.body.13.lr.ph.split, %for.end.24.loopexit.unr-lcssa
  %.lcssa54 = phi float [ %.lcssa54.unr, %for.body.13.lr.ph.split ], [ %.lcssa56, %for.end.24.loopexit.unr-lcssa ]
  br label %for.end.24

for.end.24:                                       ; preds = %for.end.24.loopexit, %if.end, %for.cond.10.preheader
  %.lcssa = phi float [ %.pre, %for.cond.10.preheader ], [ 0.000000e+00, %if.end ], [ %.lcssa54, %for.end.24.loopexit ]
  %conv25 = fpext float %.lcssa to double
  %call26 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.5, i64 0, i64 0), double %conv25) #1
  call void @llvm.lifetime.end(i64 4, i8* %0) #1
  ret i32 0

if.then.18.1:                                     ; preds = %for.inc.22
  store float %17, float* %2, align 4, !tbaa !5
  br label %for.inc.22.1

for.inc.22.1:                                     ; preds = %if.then.18.1, %for.inc.22
  %18 = phi float [ %16, %for.inc.22 ], [ %17, %if.then.18.1 ]
  %indvars.iv.next.1 = add nsw i64 %indvars.iv, 2
  %cmp11.1 = icmp slt i64 %indvars.iv.next.1, %4
  br i1 %cmp11.1, label %for.body.13, label %for.end.24.loopexit.unr-lcssa
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare noalias i8* @calloc(i64, i64) #2

; Function Attrs: noreturn nounwind
declare void @exit(i32) #3

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @putchar(i32) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noreturn nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = !{!6, !6, i64 0}
!6 = !{!"float", !3, i64 0}
