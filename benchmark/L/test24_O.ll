; ModuleID = 'test24_O.bc'

@.str = private constant [19 x i8] c"Enter an integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [21 x i8] c"Reversed Number = %d\00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_50 = alloca i32
  %.preg_I4_4_53 = alloca i32
  %.preg_I4_4_52 = alloca i32
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_ehpit0_10 = alloca i32, align 4
  %n_4 = alloca i32, align 4
  %old_frame_pointer_15.addr = alloca i64, align 8
  %remainder_6 = alloca i32, align 4
  %return_address_16.addr = alloca i64, align 8
  %reversedNumber_5 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = load i32, i32* %n_4, align 4
  store i32 %3, i32* %.preg_I4_4_52, align 8
  %4 = load i32, i32* %.preg_I4_4_52
  %cmp1 = icmp ne i32 %4, 0
  br i1 %cmp1, label %tb, label %L2818

tb:                                               ; preds = %entry
  store i32 0, i32* %.preg_I4_4_53, align 8
  br label %L2306

L2818:                                            ; preds = %entry
  store i32 0, i32* %.preg_I4_4_53, align 8
  br label %L1794

L2306:                                            ; preds = %L2306, %tb
  %5 = load i32, i32* %.preg_I4_4_52
  %lowPart_4 = alloca i32, align 4
  %div = sdiv i32 %5, 10
  %srem = srem i32 %5, 10
  store i32 %div, i32* %lowPart_4, align 4
  store i32 %srem, i32* %.preg_I4_4_50, align 8
  %6 = load i32, i32* %.preg_I4_4_50
  %7 = load i32, i32* %.preg_I4_4_53
  %mul = mul i32 %7, 10
  %add = add i32 %6, %mul
  store i32 %add, i32* %.preg_I4_4_53, align 8
  %8 = load i32, i32* %lowPart_4
  store i32 %8, i32* %.preg_I4_4_52, align 8
  %9 = load i32, i32* %.preg_I4_4_52
  %cmp2 = icmp ne i32 %9, 0
  br i1 %cmp2, label %L2306, label %fb

fb:                                               ; preds = %L2306
  %10 = load i32, i32* %.preg_I4_4_52
  store i32 %10, i32* %n_4, align 4
  br label %L1794

L1794:                                            ; preds = %fb, %L2818
  %11 = bitcast i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.2, i32 0, i32 0) to i8*
  %12 = load i32, i32* %.preg_I4_4_53
  %call3 = call i32 (i8*, ...) @printf(i8* %11, i32 %12)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
