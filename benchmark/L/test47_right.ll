; ModuleID = 'test47.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [17 x i8] c"Enter elements: \00", align 1
@.str.1 = private unnamed_addr constant [27 x i8] c"\0AStandard Deviation = %.6f\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %i = alloca i32, align 4
  %data = alloca [10 x float], align 16
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 10
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %add = add nsw i32 %1, 2
  %conv = sitofp i32 %add to float
  %2 = load i32, i32* %i, align 4
  %idxprom = sext i32 %2 to i64
  %arrayidx = getelementptr inbounds [10 x float], [10 x float]* %data, i32 0, i64 %idxprom
  store float %conv, float* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %3 = load i32, i32* %i, align 4
  %inc = add nsw i32 %3, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay = getelementptr inbounds [10 x float], [10 x float]* %data, i32 0, i32 0
  %call1 = call float @calculateSD(float* %arraydecay)
  %conv2 = fpext float %call1 to double
  %call3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.1, i32 0, i32 0), double %conv2)
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

; Function Attrs: nounwind uwtable
define float @calculateSD(float* %data) #0 {
entry:
  %data.addr = alloca float*, align 8
  %sum = alloca float, align 4
  %mean = alloca float, align 4
  %standardDeviation = alloca float, align 4
  %i = alloca i32, align 4
  store float* %data, float** %data.addr, align 8
  store float 0.000000e+00, float* %sum, align 4
  store float 0.000000e+00, float* %standardDeviation, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 10
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %idxprom = sext i32 %1 to i64
  %2 = load float*, float** %data.addr, align 8
  %arrayidx = getelementptr inbounds float, float* %2, i64 %idxprom
  %3 = load float, float* %arrayidx, align 4
  %4 = load float, float* %sum, align 4
  %add = fadd float %4, %3
  store float %add, float* %sum, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %6 = load float, float* %sum, align 4
  %div = fdiv float %6, 1.000000e+01
  store float %div, float* %mean, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond.1

for.cond.1:                                       ; preds = %for.inc.9, %for.end
  %7 = load i32, i32* %i, align 4
  %cmp2 = icmp slt i32 %7, 10
  br i1 %cmp2, label %for.body.3, label %for.end.11

for.body.3:                                       ; preds = %for.cond.1
  %8 = load i32, i32* %i, align 4
  %idxprom4 = sext i32 %8 to i64
  %9 = load float*, float** %data.addr, align 8
  %arrayidx5 = getelementptr inbounds float, float* %9, i64 %idxprom4
  %10 = load float, float* %arrayidx5, align 4
  %11 = load float, float* %mean, align 4
  %sub = fsub float %10, %11
  %conv = fpext float %sub to double
  %call = call double @pow(double %conv, double 2.000000e+00) #3
  %12 = load float, float* %standardDeviation, align 4
  %conv6 = fpext float %12 to double
  %add7 = fadd double %conv6, %call
  %conv8 = fptrunc double %add7 to float
  store float %conv8, float* %standardDeviation, align 4
  br label %for.inc.9

for.inc.9:                                        ; preds = %for.body.3
  %13 = load i32, i32* %i, align 4
  %inc10 = add nsw i32 %13, 1
  store i32 %inc10, i32* %i, align 4
  br label %for.cond.1

for.end.11:                                       ; preds = %for.cond.1
  %14 = load float, float* %standardDeviation, align 4
  %div12 = fdiv float %14, 1.000000e+01
  %conv13 = fpext float %div12 to double
  %call14 = call double @sqrt(double %conv13) #3
  %conv15 = fptrunc double %call14 to float
  ret float %conv15
}

; Function Attrs: nounwind
declare double @pow(double, double) #2

; Function Attrs: nounwind
declare double @sqrt(double) #2

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
