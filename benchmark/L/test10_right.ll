; ModuleID = 'test10.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [20 x i8] c"Enter an alphabet: \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%c\00", align 1
@.str.2 = private unnamed_addr constant [15 x i8] c"%c is a vowel.\00", align 1
@.str.3 = private unnamed_addr constant [19 x i8] c"%c is a consonant.\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %c = alloca i8, align 1
  %isLowercaseVowel = alloca i32, align 4
  %isUppercaseVowel = alloca i32, align 4
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0))
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0), i8* %c)
  %0 = load i8, i8* %c, align 1
  %conv = sext i8 %0 to i32
  %cmp = icmp eq i32 %conv, 97
  br i1 %cmp, label %lor.end, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i8, i8* %c, align 1
  %conv3 = sext i8 %1 to i32
  %cmp4 = icmp eq i32 %conv3, 101
  br i1 %cmp4, label %lor.end, label %lor.lhs.false.6

lor.lhs.false.6:                                  ; preds = %lor.lhs.false
  %2 = load i8, i8* %c, align 1
  %conv7 = sext i8 %2 to i32
  %cmp8 = icmp eq i32 %conv7, 105
  br i1 %cmp8, label %lor.end, label %lor.lhs.false.10

lor.lhs.false.10:                                 ; preds = %lor.lhs.false.6
  %3 = load i8, i8* %c, align 1
  %conv11 = sext i8 %3 to i32
  %cmp12 = icmp eq i32 %conv11, 111
  br i1 %cmp12, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %lor.lhs.false.10
  %4 = load i8, i8* %c, align 1
  %conv14 = sext i8 %4 to i32
  %cmp15 = icmp eq i32 %conv14, 117
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %lor.lhs.false.10, %lor.lhs.false.6, %lor.lhs.false, %entry
  %5 = phi i1 [ true, %lor.lhs.false.10 ], [ true, %lor.lhs.false.6 ], [ true, %lor.lhs.false ], [ true, %entry ], [ %cmp15, %lor.rhs ]
  %lor.ext = zext i1 %5 to i32
  store i32 %lor.ext, i32* %isLowercaseVowel, align 4
  %6 = load i8, i8* %c, align 1
  %conv17 = sext i8 %6 to i32
  %cmp18 = icmp eq i32 %conv17, 65
  br i1 %cmp18, label %lor.end.36, label %lor.lhs.false.20

lor.lhs.false.20:                                 ; preds = %lor.end
  %7 = load i8, i8* %c, align 1
  %conv21 = sext i8 %7 to i32
  %cmp22 = icmp eq i32 %conv21, 69
  br i1 %cmp22, label %lor.end.36, label %lor.lhs.false.24

lor.lhs.false.24:                                 ; preds = %lor.lhs.false.20
  %8 = load i8, i8* %c, align 1
  %conv25 = sext i8 %8 to i32
  %cmp26 = icmp eq i32 %conv25, 73
  br i1 %cmp26, label %lor.end.36, label %lor.lhs.false.28

lor.lhs.false.28:                                 ; preds = %lor.lhs.false.24
  %9 = load i8, i8* %c, align 1
  %conv29 = sext i8 %9 to i32
  %cmp30 = icmp eq i32 %conv29, 79
  br i1 %cmp30, label %lor.end.36, label %lor.rhs.32

lor.rhs.32:                                       ; preds = %lor.lhs.false.28
  %10 = load i8, i8* %c, align 1
  %conv33 = sext i8 %10 to i32
  %cmp34 = icmp eq i32 %conv33, 85
  br label %lor.end.36

lor.end.36:                                       ; preds = %lor.rhs.32, %lor.lhs.false.28, %lor.lhs.false.24, %lor.lhs.false.20, %lor.end
  %11 = phi i1 [ true, %lor.lhs.false.28 ], [ true, %lor.lhs.false.24 ], [ true, %lor.lhs.false.20 ], [ true, %lor.end ], [ %cmp34, %lor.rhs.32 ]
  %lor.ext37 = zext i1 %11 to i32
  store i32 %lor.ext37, i32* %isUppercaseVowel, align 4
  %12 = load i32, i32* %isLowercaseVowel, align 4
  %tobool = icmp ne i32 %12, 0
  br i1 %tobool, label %if.then, label %lor.lhs.false.38

lor.lhs.false.38:                                 ; preds = %lor.end.36
  %13 = load i32, i32* %isUppercaseVowel, align 4
  %tobool39 = icmp ne i32 %13, 0
  br i1 %tobool39, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false.38, %lor.end.36
  %14 = load i8, i8* %c, align 1
  %conv40 = sext i8 %14 to i32
  %call41 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i32 0, i32 0), i32 %conv40)
  br label %if.end

if.else:                                          ; preds = %lor.lhs.false.38
  %15 = load i8, i8* %c, align 1
  %conv42 = sext i8 %15 to i32
  %call43 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.3, i32 0, i32 0), i32 %conv42)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @__isoc99_scanf(i8*, ...) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
