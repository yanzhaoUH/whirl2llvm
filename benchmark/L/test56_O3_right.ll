; ModuleID = 'test56.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [25 x i8] c"Enter a line of string: \00", align 1
@.str.1 = private unnamed_addr constant [6 x i8] c"%[^\0A]\00", align 1
@.str.2 = private unnamed_addr constant [11 x i8] c"Vowels: %d\00", align 1
@.str.3 = private unnamed_addr constant [16 x i8] c"\0AConsonants: %d\00", align 1
@.str.4 = private unnamed_addr constant [12 x i8] c"\0ADigits: %d\00", align 1
@.str.5 = private unnamed_addr constant [18 x i8] c"\0AWhite spaces: %d\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %line = alloca [150 x i8], align 16
  %0 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i64 0, i64 0
  call void @llvm.lifetime.start(i64 150, i8* %0) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i64 0, i64 0), i8* %0) #1
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %indvars.iv = phi i64 [ %indvars.iv.next, %for.inc ], [ 0, %entry ]
  %vowels.0 = phi i32 [ %vowels.1, %for.inc ], [ 0, %entry ]
  %consonants.0 = phi i32 [ %consonants.1, %for.inc ], [ 0, %entry ]
  %digits.0 = phi i32 [ %digits.1, %for.inc ], [ 0, %entry ]
  %spaces.0 = phi i32 [ %spaces.1, %for.inc ], [ 0, %entry ]
  %arrayidx = getelementptr inbounds [150 x i8], [150 x i8]* %line, i64 0, i64 %indvars.iv
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !1
  switch i8 %1, label %if.else [
    i8 0, label %for.end
    i8 97, label %if.then
    i8 101, label %if.then
    i8 105, label %if.then
    i8 111, label %if.then
    i8 117, label %if.then
    i8 65, label %if.then
    i8 69, label %if.then
    i8 73, label %if.then
    i8 79, label %if.then
    i8 85, label %if.then
  ]

if.then:                                          ; preds = %for.cond, %for.cond, %for.cond, %for.cond, %for.cond, %for.cond, %for.cond, %for.cond, %for.cond, %for.cond
  %inc = add nsw i32 %vowels.0, 1
  br label %for.inc

if.else:                                          ; preds = %for.cond
  %2 = and i8 %1, -33
  %3 = add i8 %2, -65
  %4 = icmp ult i8 %3, 26
  br i1 %4, label %if.then.83, label %if.else.85

if.then.83:                                       ; preds = %if.else
  %inc84 = add nsw i32 %consonants.0, 1
  br label %for.inc

if.else.85:                                       ; preds = %if.else
  %.off145 = add i8 %1, -48
  %5 = icmp ult i8 %.off145, 10
  br i1 %5, label %if.then.97, label %if.else.99

if.then.97:                                       ; preds = %if.else.85
  %inc98 = add nsw i32 %digits.0, 1
  br label %for.inc

if.else.99:                                       ; preds = %if.else.85
  %cmp103 = icmp eq i8 %1, 32
  %inc106 = zext i1 %cmp103 to i32
  %inc106.spaces.0 = add nsw i32 %inc106, %spaces.0
  br label %for.inc

for.inc:                                          ; preds = %if.else.99, %if.then, %if.then.97, %if.then.83
  %vowels.1 = phi i32 [ %inc, %if.then ], [ %vowels.0, %if.then.83 ], [ %vowels.0, %if.then.97 ], [ %vowels.0, %if.else.99 ]
  %consonants.1 = phi i32 [ %consonants.0, %if.then ], [ %inc84, %if.then.83 ], [ %consonants.0, %if.then.97 ], [ %consonants.0, %if.else.99 ]
  %digits.1 = phi i32 [ %digits.0, %if.then ], [ %digits.0, %if.then.83 ], [ %inc98, %if.then.97 ], [ %digits.0, %if.else.99 ]
  %spaces.1 = phi i32 [ %spaces.0, %if.then ], [ %spaces.0, %if.then.83 ], [ %spaces.0, %if.then.97 ], [ %inc106.spaces.0, %if.else.99 ]
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %spaces.0.lcssa = phi i32 [ %spaces.0, %for.cond ]
  %digits.0.lcssa = phi i32 [ %digits.0, %for.cond ]
  %consonants.0.lcssa = phi i32 [ %consonants.0, %for.cond ]
  %vowels.0.lcssa = phi i32 [ %vowels.0, %for.cond ]
  %call111 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.2, i64 0, i64 0), i32 %vowels.0.lcssa) #1
  %call112 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.3, i64 0, i64 0), i32 %consonants.0.lcssa) #1
  %call113 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.4, i64 0, i64 0), i32 %digits.0.lcssa) #1
  %call114 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.5, i64 0, i64 0), i32 %spaces.0.lcssa) #1
  call void @llvm.lifetime.end(i64 150, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"omnipotent char", !3, i64 0}
!3 = !{!"Simple C/C++ TBAA"}
