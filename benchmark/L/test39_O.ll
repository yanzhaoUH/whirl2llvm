; ModuleID = 'test39_O.bc'

%struct._temp_dummy13 = type {}

@.str = private constant [30 x i8] c"Enter two positive integers: \00", align 1
@.str.1 = private constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private constant [26 x i8] c"G.C.D of %d and %d is %d.\00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_49 = alloca i32
  %.preg_I4_4_50 = alloca i32
  %.preg_I4_4_54 = alloca i32
  %.preg_I4_4_53 = alloca i32
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %_temp_dummy13_14 = alloca i32, align 4
  %_temp_dummy13_17 = alloca %struct._temp_dummy13, align 4
  %_temp_ehpit0_18 = alloca i32, align 4
  %n1_12.addr = alloca i32, align 4
  %n1_15 = alloca i32, align 4
  %n1_4 = alloca i32, align 4
  %n2_13.addr = alloca i32, align 4
  %n2_16 = alloca i32, align 4
  %n2_5 = alloca i32, align 4
  %old_frame_pointer_23.addr = alloca i64, align 8
  %return_address_24.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n1_4 to i32*
  %3 = bitcast i32* %n2_5 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2, i32* %3)
  %4 = load i32, i32* %n1_4, align 4
  store i32 %4, i32* %.preg_I4_4_53, align 8
  %5 = load i32, i32* %n2_5, align 4
  store i32 %5, i32* %.preg_I4_4_54, align 8
  %6 = load i32, i32* %.preg_I4_4_54
  %cmp1 = icmp ne i32 %6, 0
  br i1 %cmp1, label %tb, label %L1794

tb:                                               ; preds = %entry
  %7 = load i32, i32* %.preg_I4_4_54
  %8 = load i32, i32* %.preg_I4_4_53
  %9 = load i32, i32* %.preg_I4_4_54
  %srem = srem i32 %8, %9
  %call3 = call i32 @_Z3hcfii(i32 %7, i32 %srem)
  store i32 %call3, i32* %.preg_I4_4_50, align 8
  %10 = load i32, i32* %.preg_I4_4_50
  store i32 %10, i32* %.preg_I4_4_49, align 8
  %11 = load i32, i32* %n1_4, align 4
  store i32 %11, i32* %.preg_I4_4_53, align 8
  %12 = load i32, i32* %n2_5, align 4
  store i32 %12, i32* %.preg_I4_4_54, align 8
  br label %L258

L1794:                                            ; preds = %entry
  %13 = load i32, i32* %.preg_I4_4_53
  store i32 %13, i32* %.preg_I4_4_49, align 8
  br label %L258

L258:                                             ; preds = %L1794, %tb
  %14 = bitcast i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.2, i32 0, i32 0) to i8*
  %15 = load i32, i32* %.preg_I4_4_53
  %16 = load i32, i32* %.preg_I4_4_54
  %17 = load i32, i32* %.preg_I4_4_49
  %call4 = call i32 (i8*, ...) @printf(i8* %14, i32 %15, i32 %16, i32 %17)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define i32 @_Z3hcfii(i32 %n1_4, i32 %n2_5) {
entry:
  %.preg_I4_4_49 = alloca i32
  %.preg_I4_4_51 = alloca i32
  %.preg_I4_4_50 = alloca i32
  %_temp_dummy13_6 = alloca i32, align 4
  %_temp_ehpit1_7 = alloca i32, align 4
  %n1_4.addr = alloca i32, align 4
  %n2_5.addr = alloca i32, align 4
  %old_frame_pointer_12.addr = alloca i64, align 8
  %return_address_13.addr = alloca i64, align 8
  store i32 %n1_4, i32* %n1_4.addr, align 4
  store i32 %n2_5, i32* %n2_5.addr, align 4
  store i32 %n1_4, i32* %.preg_I4_4_50, align 8
  store i32 %n2_5, i32* %.preg_I4_4_51, align 8
  %0 = load i32, i32* %.preg_I4_4_51
  %cmp2 = icmp ne i32 %0, 0
  br i1 %cmp2, label %tb, label %L770

tb:                                               ; preds = %entry
  %1 = load i32, i32* %.preg_I4_4_51
  %2 = load i32, i32* %.preg_I4_4_50
  %3 = load i32, i32* %.preg_I4_4_51
  %srem = srem i32 %2, %3
  %call5 = call i32 @_Z3hcfii(i32 %1, i32 %srem)
  store i32 %call5, i32* %.preg_I4_4_49, align 8
  %4 = load i32, i32* %.preg_I4_4_49
  ret i32 %4

L770:                                             ; preds = %entry
  %5 = load i32, i32* %.preg_I4_4_50
  ret i32 %5
}
