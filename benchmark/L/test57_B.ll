; ModuleID = 'test57.bc'

@.str = private constant [17 x i8] c"Enter a string: \00", align 1
@.str.1 = private constant [16 x i8] c"Output String: \00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %i_5 = alloca i32, align 4
  %j_6 = alloca i32, align 4
  %line_4 = alloca [150 x i8], align 1
  %0 = bitcast i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %arrayAddress = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i32 0
  %1 = bitcast i8* %arrayAddress to i8*
  %call2 = call i8* @gets(i8* %1)
  store i32 0, i32* %i_5, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.end4, %entry
  %2 = load i32, i32* %i_5, align 4
  %conv3 = sext i32 %2 to i64
  %arrayidx = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv3
  %3 = load i8, i8* %arrayidx, align 1
  %conv31 = sext i8 %3 to i32
  %cmp1 = icmp ne i32 %conv31, 0
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  br label %while.cond2

while.end:                                        ; preds = %while.cond
  %4 = bitcast i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %4)
  %arrayAddress38 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i32 0
  %5 = bitcast i8* %arrayAddress38 to i8*
  %call4 = call i32 @puts(i8* %5)
  ret i32 0

while.cond2:                                      ; preds = %while.end22, %while.body
  %6 = load i32, i32* %i_5, align 4
  %conv37 = sext i32 %6 to i64
  %arrayidx8 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv37
  %7 = load i8, i8* %arrayidx8, align 1
  %conv39 = sext i8 %7 to i32
  %add = sub i32 %conv39, 97
  %conv = trunc i32 %add to i8
  %conv310 = sext i8 %conv to i32
  %cmp2 = icmp ugt i32 %conv310, 25
  br i1 %cmp2, label %land.rhs5, label %land.end6

while.body3:                                      ; preds = %land.end
  %8 = load i32, i32* %i_5, align 4
  store i32 %8, i32* %j_6, align 4
  br label %while.cond20

while.end4:                                       ; preds = %land.end
  %9 = load i32, i32* %i_5, align 4
  %add.37 = add i32 %9, 1
  store i32 %add.37, i32* %i_5, align 4
  br label %while.cond

land.rhs:                                         ; preds = %land.end6
  %10 = load i32, i32* %i_5, align 4
  %conv317 = sext i32 %10 to i64
  %arrayidx18 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv317
  %11 = load i8, i8* %arrayidx18, align 1
  %conv319 = sext i8 %11 to i32
  %cmp4 = icmp ne i32 %conv319, 0
  br label %land.end

land.end:                                         ; preds = %land.end6, %land.rhs
  %12 = phi i1 [ false, %land.end6 ], [ %cmp4, %land.rhs ]
  br i1 %12, label %while.body3, label %while.end4

land.rhs5:                                        ; preds = %while.cond2
  %13 = load i32, i32* %i_5, align 4
  %conv311 = sext i32 %13 to i64
  %arrayidx12 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv311
  %14 = load i8, i8* %arrayidx12, align 1
  %conv313 = sext i8 %14 to i32
  %add.14 = sub i32 %conv313, 65
  %conv15 = trunc i32 %add.14 to i8
  %conv316 = sext i8 %conv15 to i32
  %cmp3 = icmp ugt i32 %conv316, 25
  br label %land.end6

land.end6:                                        ; preds = %land.rhs5, %while.cond2
  %15 = phi i1 [ false, %while.cond2 ], [ %cmp3, %land.rhs5 ]
  br i1 %15, label %land.rhs, label %land.end

while.cond20:                                     ; preds = %while.body21, %while.body3
  %16 = load i32, i32* %j_6, align 4
  %conv323 = sext i32 %16 to i64
  %arrayidx24 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv323
  %17 = load i8, i8* %arrayidx24, align 1
  %conv325 = sext i8 %17 to i32
  %cmp5 = icmp ne i32 %conv325, 0
  br i1 %cmp5, label %while.body21, label %while.end22

while.body21:                                     ; preds = %while.cond20
  %18 = load i32, i32* %j_6, align 4
  %conv326 = sext i32 %18 to i64
  %arrayidx27 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv326
  %19 = load i32, i32* %j_6, align 4
  %add.28 = add i32 %19, 1
  %arrayidx29 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i32 %add.28
  %20 = load i8, i8* %arrayidx29, align 1
  %conv330 = sext i8 %20 to i32
  %conv32 = trunc i32 %conv330 to i8
  store i8 %conv32, i8* %arrayidx27, align 1
  %21 = load i32, i32* %j_6, align 4
  %add.33 = add i32 %21, 1
  store i32 %add.33, i32* %j_6, align 4
  br label %while.cond20

while.end22:                                      ; preds = %while.cond20
  %22 = load i32, i32* %j_6, align 4
  %conv334 = sext i32 %22 to i64
  %arrayidx35 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv334
  %conv36 = trunc i32 0 to i8
  store i8 %conv36, i8* %arrayidx35, align 1
  br label %while.cond2
}

declare i32 @printf(i8*, ...)

declare i8* @gets(i8*)

declare i32 @puts(i8*)
