; ModuleID = 'test49.bc'

@.str = private constant [41 x i8] c"Enter rows and column for first matrix: \00", align 1
@.str.1 = private constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private constant [42 x i8] c"Enter rows and column for second matrix: \00", align 1
@.str.3 = private constant [60 x i8] c"Error! column of first matrix not equal to row of second.\0A\0A\00", align 1
@.str.4 = private constant [30 x i8] c"\0AEnter elements of matrix 1:\0A\00", align 1
@.str.5 = private constant [23 x i8] c"Enter elements a%d%d: \00", align 1
@.str.6 = private constant [30 x i8] c"\0AEnter elements of matrix 2:\0A\00", align 1
@.str.7 = private constant [23 x i8] c"Enter elements b%d%d: \00", align 1
@.str.8 = private constant [17 x i8] c"\0AOutput Matrix:\0A\00", align 1
@.str.9 = private constant [5 x i8] c"%d  \00", align 1
@.str.10 = private constant [3 x i8] c"\0A\0A\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_14 = alloca i32, align 4
  %_temp_dummy110_24 = alloca i32, align 4
  %_temp_dummy111_25 = alloca i32, align 4
  %_temp_dummy112_26 = alloca i32, align 4
  %_temp_dummy113_27 = alloca i32, align 4
  %_temp_dummy114_28 = alloca i32, align 4
  %_temp_dummy115_29 = alloca i32, align 4
  %_temp_dummy11_15 = alloca i32, align 4
  %_temp_dummy12_16 = alloca i32, align 4
  %_temp_dummy13_17 = alloca i32, align 4
  %_temp_dummy14_18 = alloca i32, align 4
  %_temp_dummy15_19 = alloca i32, align 4
  %_temp_dummy16_20 = alloca i32, align 4
  %_temp_dummy17_21 = alloca i32, align 4
  %_temp_dummy18_22 = alloca i32, align 4
  %_temp_dummy19_23 = alloca i32, align 4
  %a_4 = alloca [10 x [10 x i32]], align 4
  %b_5 = alloca [10 x [10 x i32]], align 4
  %c1_8 = alloca i32, align 4
  %c2_10 = alloca i32, align 4
  %i_11 = alloca i32, align 4
  %j_12 = alloca i32, align 4
  %k_13 = alloca i32, align 4
  %r1_7 = alloca i32, align 4
  %r2_9 = alloca i32, align 4
  %result_6 = alloca [10 x [10 x i32]], align 4
  %0 = bitcast i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %r1_7 to i32*
  %3 = bitcast i32* %c1_8 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2, i32* %3)
  %4 = bitcast i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %4)
  %5 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %6 = bitcast i32* %r2_9 to i32*
  %7 = bitcast i32* %c2_10 to i32*
  %call4 = call i32 (i8*, ...) @scanf(i8* %5, i32* %6, i32* %7)
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %8 = load i32, i32* %c1_8, align 4
  %9 = load i32, i32* %r2_9, align 4
  %cmp1 = icmp ne i32 %8, %9
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %10 = bitcast i8* getelementptr inbounds ([60 x i8], [60 x i8]* @.str.3, i32 0, i32 0) to i8*
  %call5 = call i32 (i8*, ...) @printf(i8* %10)
  %11 = bitcast i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str, i32 0, i32 0) to i8*
  %call6 = call i32 (i8*, ...) @printf(i8* %11)
  %12 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %13 = bitcast i32* %r1_7 to i32*
  %14 = bitcast i32* %c1_8 to i32*
  %call7 = call i32 (i8*, ...) @scanf(i8* %12, i32* %13, i32* %14)
  %15 = bitcast i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call8 = call i32 (i8*, ...) @printf(i8* %15)
  %16 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %17 = bitcast i32* %r2_9 to i32*
  %18 = bitcast i32* %c2_10 to i32*
  %call9 = call i32 (i8*, ...) @scanf(i8* %16, i32* %17, i32* %18)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %19 = bitcast i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.4, i32 0, i32 0) to i8*
  %call10 = call i32 (i8*, ...) @printf(i8* %19)
  store i32 0, i32* %i_11, align 4
  br label %while.cond1

while.cond1:                                      ; preds = %while.end6, %while.end
  %20 = load i32, i32* %i_11, align 4
  %21 = load i32, i32* %r1_7, align 4
  %cmp2 = icmp slt i32 %20, %21
  br i1 %cmp2, label %while.body2, label %while.end3

while.body2:                                      ; preds = %while.cond1
  store i32 0, i32* %j_12, align 4
  br label %while.cond4

while.end3:                                       ; preds = %while.cond1
  %22 = bitcast i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.6, i32 0, i32 0) to i8*
  %call12 = call i32 (i8*, ...) @printf(i8* %22)
  store i32 0, i32* %i_11, align 4
  br label %while.cond14

while.cond4:                                      ; preds = %while.body5, %while.body2
  %23 = load i32, i32* %j_12, align 4
  %24 = load i32, i32* %c1_8, align 4
  %cmp3 = icmp slt i32 %23, %24
  br i1 %cmp3, label %while.body5, label %while.end6

while.body5:                                      ; preds = %while.cond4
  %25 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.5, i32 0, i32 0) to i8*
  %26 = load i32, i32* %i_11, align 4
  %add = add i32 %26, 1
  %27 = load i32, i32* %j_12, align 4
  %add.7 = add i32 %27, 1
  %call11 = call i32 (i8*, ...) @printf(i8* %25, i32 %add, i32 %add.7)
  %28 = load i32, i32* %i_11, align 4
  %conv3 = sext i32 %28 to i64
  %arrayidx = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %a_4, i32 0, i64 %conv3
  %29 = load i32, i32* %j_12, align 4
  %conv38 = sext i32 %29 to i64
  %arrayidx9 = getelementptr [10 x i32], [10 x i32]* %arrayidx, i32 0, i64 %conv38
  %30 = load i32, i32* %i_11, align 4
  %add.10 = add i32 %30, 1
  %31 = load i32, i32* %j_12, align 4
  %add.11 = add i32 %31, 1
  %mul = mul i32 %add.10, %add.11
  store i32 %mul, i32* %arrayidx9, align 4
  %32 = load i32, i32* %j_12, align 4
  %add.12 = add i32 %32, 1
  store i32 %add.12, i32* %j_12, align 4
  br label %while.cond4

while.end6:                                       ; preds = %while.cond4
  %33 = load i32, i32* %i_11, align 4
  %add.13 = add i32 %33, 1
  store i32 %add.13, i32* %i_11, align 4
  br label %while.cond1

while.cond14:                                     ; preds = %while.end19, %while.end3
  %34 = load i32, i32* %i_11, align 4
  %35 = load i32, i32* %r2_9, align 4
  %cmp4 = icmp slt i32 %34, %35
  br i1 %cmp4, label %while.body15, label %while.end16

while.body15:                                     ; preds = %while.cond14
  store i32 0, i32* %j_12, align 4
  br label %while.cond17

while.end16:                                      ; preds = %while.cond14
  store i32 0, i32* %i_11, align 4
  br label %while.cond31

while.cond17:                                     ; preds = %while.body18, %while.body15
  %36 = load i32, i32* %j_12, align 4
  %37 = load i32, i32* %c2_10, align 4
  %cmp5 = icmp slt i32 %36, %37
  br i1 %cmp5, label %while.body18, label %while.end19

while.body18:                                     ; preds = %while.cond17
  %38 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.7, i32 0, i32 0) to i8*
  %39 = load i32, i32* %i_11, align 4
  %add.20 = add i32 %39, 1
  %40 = load i32, i32* %j_12, align 4
  %add.21 = add i32 %40, 1
  %call13 = call i32 (i8*, ...) @printf(i8* %38, i32 %add.20, i32 %add.21)
  %41 = load i32, i32* %i_11, align 4
  %conv322 = sext i32 %41 to i64
  %arrayidx23 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %b_5, i32 0, i64 %conv322
  %42 = load i32, i32* %j_12, align 4
  %conv324 = sext i32 %42 to i64
  %arrayidx25 = getelementptr [10 x i32], [10 x i32]* %arrayidx23, i32 0, i64 %conv324
  %43 = load i32, i32* %i_11, align 4
  %add.26 = add i32 %43, 1
  %44 = load i32, i32* %j_12, align 4
  %add.27 = add i32 %44, 1
  %mul.28 = mul i32 %add.26, %add.27
  store i32 %mul.28, i32* %arrayidx25, align 4
  %45 = load i32, i32* %j_12, align 4
  %add.29 = add i32 %45, 1
  store i32 %add.29, i32* %j_12, align 4
  br label %while.cond17

while.end19:                                      ; preds = %while.cond17
  %46 = load i32, i32* %i_11, align 4
  %add.30 = add i32 %46, 1
  store i32 %add.30, i32* %i_11, align 4
  br label %while.cond14

while.cond31:                                     ; preds = %while.end36, %while.end16
  %47 = load i32, i32* %i_11, align 4
  %48 = load i32, i32* %r1_7, align 4
  %cmp6 = icmp slt i32 %47, %48
  br i1 %cmp6, label %while.body32, label %while.end33

while.body32:                                     ; preds = %while.cond31
  store i32 0, i32* %j_12, align 4
  br label %while.cond34

while.end33:                                      ; preds = %while.cond31
  store i32 0, i32* %i_11, align 4
  br label %while.cond43

while.cond34:                                     ; preds = %while.body35, %while.body32
  %49 = load i32, i32* %j_12, align 4
  %50 = load i32, i32* %c2_10, align 4
  %cmp7 = icmp slt i32 %49, %50
  br i1 %cmp7, label %while.body35, label %while.end36

while.body35:                                     ; preds = %while.cond34
  %51 = load i32, i32* %i_11, align 4
  %conv337 = sext i32 %51 to i64
  %arrayidx38 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %result_6, i32 0, i64 %conv337
  %52 = load i32, i32* %j_12, align 4
  %conv339 = sext i32 %52 to i64
  %arrayidx40 = getelementptr [10 x i32], [10 x i32]* %arrayidx38, i32 0, i64 %conv339
  store i32 0, i32* %arrayidx40, align 4
  %53 = load i32, i32* %j_12, align 4
  %add.41 = add i32 %53, 1
  store i32 %add.41, i32* %j_12, align 4
  br label %while.cond34

while.end36:                                      ; preds = %while.cond34
  %54 = load i32, i32* %i_11, align 4
  %add.42 = add i32 %54, 1
  store i32 %add.42, i32* %i_11, align 4
  br label %while.cond31

while.cond43:                                     ; preds = %while.end48, %while.end33
  %55 = load i32, i32* %i_11, align 4
  %56 = load i32, i32* %r1_7, align 4
  %cmp8 = icmp slt i32 %55, %56
  br i1 %cmp8, label %while.body44, label %while.end45

while.body44:                                     ; preds = %while.cond43
  store i32 0, i32* %j_12, align 4
  br label %while.cond46

while.end45:                                      ; preds = %while.cond43
  %57 = bitcast i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.8, i32 0, i32 0) to i8*
  %call14 = call i32 (i8*, ...) @printf(i8* %57)
  store i32 0, i32* %i_11, align 4
  br label %while.cond73

while.cond46:                                     ; preds = %while.end51, %while.body44
  %58 = load i32, i32* %j_12, align 4
  %59 = load i32, i32* %c2_10, align 4
  %cmp9 = icmp slt i32 %58, %59
  br i1 %cmp9, label %while.body47, label %while.end48

while.body47:                                     ; preds = %while.cond46
  store i32 0, i32* %k_13, align 4
  br label %while.cond49

while.end48:                                      ; preds = %while.cond46
  %60 = load i32, i32* %i_11, align 4
  %add.72 = add i32 %60, 1
  store i32 %add.72, i32* %i_11, align 4
  br label %while.cond43

while.cond49:                                     ; preds = %while.body50, %while.body47
  %61 = load i32, i32* %k_13, align 4
  %62 = load i32, i32* %c1_8, align 4
  %cmp10 = icmp slt i32 %61, %62
  br i1 %cmp10, label %while.body50, label %while.end51

while.body50:                                     ; preds = %while.cond49
  %63 = load i32, i32* %i_11, align 4
  %conv352 = sext i32 %63 to i64
  %arrayidx53 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %result_6, i32 0, i64 %conv352
  %64 = load i32, i32* %j_12, align 4
  %conv354 = sext i32 %64 to i64
  %arrayidx55 = getelementptr [10 x i32], [10 x i32]* %arrayidx53, i32 0, i64 %conv354
  %65 = load i32, i32* %i_11, align 4
  %conv356 = sext i32 %65 to i64
  %arrayidx57 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %result_6, i32 0, i64 %conv356
  %66 = load i32, i32* %j_12, align 4
  %conv358 = sext i32 %66 to i64
  %arrayidx59 = getelementptr [10 x i32], [10 x i32]* %arrayidx57, i32 0, i64 %conv358
  %67 = load i32, i32* %arrayidx59, align 4
  %68 = load i32, i32* %i_11, align 4
  %conv360 = sext i32 %68 to i64
  %arrayidx61 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %a_4, i32 0, i64 %conv360
  %69 = load i32, i32* %k_13, align 4
  %conv362 = sext i32 %69 to i64
  %arrayidx63 = getelementptr [10 x i32], [10 x i32]* %arrayidx61, i32 0, i64 %conv362
  %70 = load i32, i32* %arrayidx63, align 4
  %71 = load i32, i32* %k_13, align 4
  %conv364 = sext i32 %71 to i64
  %arrayidx65 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %b_5, i32 0, i64 %conv364
  %72 = load i32, i32* %j_12, align 4
  %conv366 = sext i32 %72 to i64
  %arrayidx67 = getelementptr [10 x i32], [10 x i32]* %arrayidx65, i32 0, i64 %conv366
  %73 = load i32, i32* %arrayidx67, align 4
  %mul.68 = mul i32 %70, %73
  %add.69 = add i32 %67, %mul.68
  store i32 %add.69, i32* %arrayidx55, align 4
  %74 = load i32, i32* %k_13, align 4
  %add.70 = add i32 %74, 1
  store i32 %add.70, i32* %k_13, align 4
  br label %while.cond49

while.end51:                                      ; preds = %while.cond49
  %75 = load i32, i32* %j_12, align 4
  %add.71 = add i32 %75, 1
  store i32 %add.71, i32* %j_12, align 4
  br label %while.cond46

while.cond73:                                     ; preds = %while.end78, %while.end45
  %76 = load i32, i32* %i_11, align 4
  %77 = load i32, i32* %r1_7, align 4
  %cmp11 = icmp slt i32 %76, %77
  br i1 %cmp11, label %while.body74, label %while.end75

while.body74:                                     ; preds = %while.cond73
  store i32 0, i32* %j_12, align 4
  br label %while.cond76

while.end75:                                      ; preds = %while.cond73
  ret i32 0

while.cond76:                                     ; preds = %if.end, %while.body74
  %78 = load i32, i32* %j_12, align 4
  %79 = load i32, i32* %c2_10, align 4
  %cmp12 = icmp slt i32 %78, %79
  br i1 %cmp12, label %while.body77, label %while.end78

while.body77:                                     ; preds = %while.cond76
  %80 = bitcast i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.9, i32 0, i32 0) to i8*
  %81 = load i32, i32* %i_11, align 4
  %conv379 = sext i32 %81 to i64
  %arrayidx80 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %result_6, i32 0, i64 %conv379
  %82 = load i32, i32* %j_12, align 4
  %conv381 = sext i32 %82 to i64
  %arrayidx82 = getelementptr [10 x i32], [10 x i32]* %arrayidx80, i32 0, i64 %conv381
  %83 = load i32, i32* %arrayidx82, align 4
  %call15 = call i32 (i8*, ...) @printf(i8* %80, i32 %83)
  %84 = load i32, i32* %c2_10, align 4
  %add.83 = sub i32 %84, 1
  %85 = load i32, i32* %j_12, align 4
  %cmp13 = icmp eq i32 %add.83, %85
  br i1 %cmp13, label %if.then, label %if.else

while.end78:                                      ; preds = %while.cond76
  %86 = load i32, i32* %i_11, align 4
  %add.85 = add i32 %86, 1
  store i32 %add.85, i32* %i_11, align 4
  br label %while.cond73

if.then:                                          ; preds = %while.body77
  %87 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.10, i32 0, i32 0) to i8*
  %call16 = call i32 (i8*, ...) @printf(i8* %87)
  br label %if.end

if.else:                                          ; preds = %while.body77
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %88 = load i32, i32* %j_12, align 4
  %add.84 = add i32 %88, 1
  store i32 %add.84, i32* %j_12, align 4
  br label %while.cond76
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
