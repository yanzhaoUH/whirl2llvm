; ModuleID = 'test25.bc'

@.str = private constant [22 x i8] c"Enter a base number: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [21 x i8] c"Enter an exponent: 4\00", align 1
@.str.3 = private constant [14 x i8] c"Answer = %lld\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %_temp_dummy14_11 = alloca i32, align 4
  %base_4 = alloca i32, align 4
  %exponent_5 = alloca i32, align 4
  %result_6 = alloca i64, align 8
  store i64 1, i64* %result_6, align 8
  %0 = bitcast i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %base_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = bitcast i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %3)
  %4 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %5 = bitcast i32* %exponent_5 to i32*
  %call4 = call i32 (i8*, ...) @scanf(i8* %4, i32* %5)
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %6 = load i32, i32* %exponent_5, align 4
  %cmp1 = icmp ne i32 %6, 0
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = load i32, i32* %base_4, align 4
  %conv3 = sext i32 %7 to i64
  %8 = load i64, i64* %result_6, align 8
  %mul = mul i64 %conv3, %8
  store i64 %mul, i64* %result_6, align 8
  %9 = load i32, i32* %exponent_5, align 4
  %add = sub i32 %9, 1
  store i32 %add, i32* %exponent_5, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %10 = bitcast i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.3, i32 0, i32 0) to i8*
  %11 = load i64, i64* %result_6, align 8
  %call5 = call i32 (i8*, ...) @printf(i8* %10, i64 %11)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
