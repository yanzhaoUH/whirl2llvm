; ModuleID = 'test58.bc'

@.str = private constant [17 x i8] c"Enter a string: \00", align 1
@.str.1 = private constant [3 x i8] c"%s\00", align 1
@.str.2 = private constant [21 x i8] c"Length of string: %d\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %i_5 = alloca i8, align 1
  %s_4 = alloca [1000 x i8], align 1
  %0 = bitcast i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %arrayAddress = getelementptr [1000 x i8], [1000 x i8]* %s_4, i32 0, i32 0
  %2 = bitcast i8* %arrayAddress to i8*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i8* %2)
  %conv = trunc i32 0 to i8
  store i8 %conv, i8* %i_5, align 1
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %3 = load i8, i8* %i_5, align 1
  %arrayidx = getelementptr [1000 x i8], [1000 x i8]* %s_4, i32 0, i8 %3
  %4 = load i8, i8* %arrayidx, align 1
  %conv3 = sext i8 %4 to i32
  %cmp1 = icmp ne i32 %conv3, 0
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = load i8, i8* %i_5, align 1
  %conv31 = sext i8 %5 to i32
  %add = add i32 %conv31, 1
  %conv2 = trunc i32 %add to i8
  store i8 %conv2, i8* %i_5, align 1
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %6 = bitcast i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.2, i32 0, i32 0) to i8*
  %7 = load i8, i8* %i_5, align 1
  %conv33 = sext i8 %7 to i32
  %call3 = call i32 (i8*, ...) @printf(i8* %6, i32 %conv33)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
