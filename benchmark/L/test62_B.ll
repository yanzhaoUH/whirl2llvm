; ModuleID = 'test62.bc'

%struct.student = type { [50 x i8], i32, float }

@.str = private constant [20 x i8] c"Enter information:\0A\00", align 1
@.str.1 = private constant [13 x i8] c"Enter name: \00", align 1
@.str.2 = private constant [3 x i8] c"%s\00", align 1
@.str.3 = private constant [20 x i8] c"Enter roll number: \00", align 1
@.str.4 = private constant [3 x i8] c"%d\00", align 1
@.str.5 = private constant [14 x i8] c"Enter marks: \00", align 1
@.str.6 = private constant [3 x i8] c"%f\00", align 1
@.str.7 = private constant [25 x i8] c"Displaying Information:\0A\00", align 1
@.str.8 = private constant [7 x i8] c"Name: \00", align 1
@.str.9 = private constant [17 x i8] c"Roll number: %d\0A\00", align 1
@.str.10 = private constant [13 x i8] c"Marks: %.1f\0A\00", align 1
@g_i_66 = common global i32 0, align 4
@s_56 = common global %struct.student zeroinitializer, align 4

define i32 @main() {
entry:
  %_temp_dummy10_4 = alloca i32, align 4
  %_temp_dummy110_14 = alloca i32, align 4
  %_temp_dummy111_15 = alloca i32, align 4
  %_temp_dummy11_5 = alloca i32, align 4
  %_temp_dummy12_6 = alloca i32, align 4
  %_temp_dummy13_7 = alloca i32, align 4
  %_temp_dummy14_8 = alloca i32, align 4
  %_temp_dummy15_9 = alloca i32, align 4
  %_temp_dummy16_10 = alloca i32, align 4
  %_temp_dummy17_11 = alloca i32, align 4
  %_temp_dummy18_12 = alloca i32, align 4
  %_temp_dummy19_13 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call2 = call i32 (i8*, ...) @printf(i8* %1)
  %2 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0) to i8*
  %name = getelementptr %struct.student, %struct.student* @s_56, i32 0, i32 0
  %roll = getelementptr %struct.student, %struct.student* @s_56, i32 0, i32 1
  %marks = getelementptr %struct.student, %struct.student* @s_56, i32 0, i32 2
  %3 = bitcast [50 x i8]* %name to i8*
  %call3 = call i32 (i8*, ...) @scanf(i8* %2, i8* %3)
  %4 = bitcast i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.3, i32 0, i32 0) to i8*
  %call4 = call i32 (i8*, ...) @printf(i8* %4)
  %5 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.4, i32 0, i32 0) to i8*
  %name1 = getelementptr %struct.student, %struct.student* @s_56, i32 0, i32 0
  %roll2 = getelementptr %struct.student, %struct.student* @s_56, i32 0, i32 1
  %marks3 = getelementptr %struct.student, %struct.student* @s_56, i32 0, i32 2
  %6 = bitcast i32* %roll2 to i32*
  %call5 = call i32 (i8*, ...) @scanf(i8* %5, i32* %6)
  %7 = bitcast i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.5, i32 0, i32 0) to i8*
  %call6 = call i32 (i8*, ...) @printf(i8* %7)
  %8 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.6, i32 0, i32 0) to i8*
  %name4 = getelementptr %struct.student, %struct.student* @s_56, i32 0, i32 0
  %roll5 = getelementptr %struct.student, %struct.student* @s_56, i32 0, i32 1
  %marks6 = getelementptr %struct.student, %struct.student* @s_56, i32 0, i32 2
  %9 = bitcast float* %marks6 to float*
  %call7 = call i32 (i8*, ...) @scanf(i8* %8, float* %9)
  %10 = bitcast i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.7, i32 0, i32 0) to i8*
  %call8 = call i32 (i8*, ...) @printf(i8* %10)
  %11 = bitcast i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.8, i32 0, i32 0) to i8*
  %call9 = call i32 (i8*, ...) @printf(i8* %11)
  %name7 = getelementptr %struct.student, %struct.student* @s_56, i32 0, i32 0
  %roll8 = getelementptr %struct.student, %struct.student* @s_56, i32 0, i32 1
  %marks9 = getelementptr %struct.student, %struct.student* @s_56, i32 0, i32 2
  %12 = bitcast [50 x i8]* %name7 to i8*
  %call10 = call i32 @puts(i8* %12)
  %13 = bitcast i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.9, i32 0, i32 0) to i8*
  %name10 = getelementptr %struct.student, %struct.student* @s_56, i32 0, i32 0
  %roll11 = getelementptr %struct.student, %struct.student* @s_56, i32 0, i32 1
  %marks12 = getelementptr %struct.student, %struct.student* @s_56, i32 0, i32 2
  %14 = load i32, i32* %roll11, align 4
  %call11 = call i32 (i8*, ...) @printf(i8* %13, i32 %14)
  %15 = bitcast i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.10, i32 0, i32 0) to i8*
  %name13 = getelementptr %struct.student, %struct.student* @s_56, i32 0, i32 0
  %roll14 = getelementptr %struct.student, %struct.student* @s_56, i32 0, i32 1
  %marks15 = getelementptr %struct.student, %struct.student* @s_56, i32 0, i32 2
  %16 = load float, float* %marks15, align 4
  %convDouble = fpext float %16 to double
  %call12 = call i32 (i8*, ...) @printf(i8* %15, double %convDouble)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

declare i32 @puts(i8*)
