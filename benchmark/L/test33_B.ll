; ModuleID = 'test33.bc'

@.str = private constant [31 x i8] c"Enter an operator (+, -, *,): \00", align 1
@.str.1 = private constant [3 x i8] c"%c\00", align 1
@.str.2 = private constant [21 x i8] c"Enter two operands: \00", align 1
@.str.3 = private constant [8 x i8] c"%lf %lf\00", align 1
@.str.4 = private constant [22 x i8] c"%.1lf + %.1lf = %.1lf\00", align 1
@.str.5 = private constant [22 x i8] c"%.1lf - %.1lf = %.1lf\00", align 1
@.str.6 = private constant [22 x i8] c"%.1lf * %.1lf = %.1lf\00", align 1
@.str.7 = private constant [22 x i8] c"%.1lf / %.1lf = %.1lf\00", align 1
@.str.8 = private constant [31 x i8] c"Error! operator is not correct\00", align 1

define i32 @main() {
entry:
  %_temp__switch_index4_11 = alloca i32, align 4
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %_temp_dummy15_12 = alloca i32, align 4
  %_temp_dummy16_13 = alloca i32, align 4
  %_temp_dummy17_14 = alloca i32, align 4
  %_temp_dummy18_15 = alloca i32, align 4
  %_temp_dummy19_16 = alloca i32, align 4
  %firstNumber_5 = alloca double, align 8
  %op_4 = alloca i8, align 1
  %secondNumber_6 = alloca double, align 8
  %0 = bitcast i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i8* %op_4 to i8*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i8* %2)
  %3 = bitcast i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %3)
  %4 = bitcast i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.3, i32 0, i32 0) to i8*
  %5 = bitcast double* %firstNumber_5 to double*
  %6 = bitcast double* %secondNumber_6 to double*
  %call4 = call i32 (i8*, ...) @scanf(i8* %4, double* %5, double* %6)
  %7 = load i8, i8* %op_4, align 1
  %conv3 = sext i8 %7 to i32
  store i32 %conv3, i32* %_temp__switch_index4_11, align 4
  %8 = load i32, i32* %_temp__switch_index4_11, align 4
  switch i32 %8, label %sw.default [
    i32 43, label %sw.block0
    i32 45, label %sw.block1
    i32 42, label %sw.block2
    i32 47, label %sw.block3
  ]

sw.block0:                                        ; preds = %entry
  %9 = bitcast i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.4, i32 0, i32 0) to i8*
  %10 = load double, double* %firstNumber_5, align 8
  %11 = load double, double* %secondNumber_6, align 8
  %12 = load double, double* %firstNumber_5, align 8
  %13 = load double, double* %secondNumber_6, align 8
  %add = fadd double %12, %13
  %call5 = call i32 (i8*, ...) @printf(i8* %9, double %10, double %11, double %add)
  br label %sw.epilog

sw.block1:                                        ; preds = %entry
  %14 = bitcast i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.5, i32 0, i32 0) to i8*
  %15 = load double, double* %firstNumber_5, align 8
  %16 = load double, double* %secondNumber_6, align 8
  %17 = load double, double* %firstNumber_5, align 8
  %18 = load double, double* %secondNumber_6, align 8
  %add.1 = fsub double %17, %18
  %call6 = call i32 (i8*, ...) @printf(i8* %14, double %15, double %16, double %add.1)
  br label %sw.epilog

sw.block2:                                        ; preds = %entry
  %19 = bitcast i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.6, i32 0, i32 0) to i8*
  %20 = load double, double* %firstNumber_5, align 8
  %21 = load double, double* %secondNumber_6, align 8
  %22 = load double, double* %firstNumber_5, align 8
  %23 = load double, double* %secondNumber_6, align 8
  %mul = fmul double %22, %23
  %call7 = call i32 (i8*, ...) @printf(i8* %19, double %20, double %21, double %mul)
  br label %sw.epilog

sw.block3:                                        ; preds = %entry
  %24 = bitcast i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.7, i32 0, i32 0) to i8*
  %25 = load double, double* %firstNumber_5, align 8
  %26 = load double, double* %secondNumber_6, align 8
  %27 = load double, double* %firstNumber_5, align 8
  %28 = load double, double* %firstNumber_5, align 8
  %SDiv = fdiv double %27, %28
  %call8 = call i32 (i8*, ...) @printf(i8* %24, double %25, double %26, double %SDiv)
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %29 = bitcast i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.8, i32 0, i32 0) to i8*
  %call9 = call i32 (i8*, ...) @printf(i8* %29)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.block3, %sw.block2, %sw.block1, %sw.block0
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
