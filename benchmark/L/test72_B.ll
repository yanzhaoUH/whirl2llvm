; ModuleID = 'test72.bc'

@.str = private constant [24 x i8] c"Enter the value of num\0A\00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [30 x i8] c"Sum of all odd numbers  = %d\0A\00", align 1
@.str.3 = private constant [30 x i8] c"Sum of all even numbers = %d\0A\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_8 = alloca i32, align 4
  %_temp_dummy11_9 = alloca i32, align 4
  %_temp_dummy12_10 = alloca i32, align 4
  %_temp_dummy13_11 = alloca i32, align 4
  %even_sum_7 = alloca i32, align 4
  %i_4 = alloca i32, align 4
  %num_5 = alloca i32, align 4
  %odd_sum_6 = alloca i32, align 4
  store i32 0, i32* %odd_sum_6, align 4
  store i32 0, i32* %even_sum_7, align 4
  %0 = bitcast i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %num_5 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  store i32 1, i32* %i_4, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %3 = load i32, i32* %i_4, align 4
  %4 = load i32, i32* %num_5, align 4
  %cmp1 = icmp sle i32 %3, %4
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = load i32, i32* %i_4, align 4
  %and = and i32 %5, 1
  %cmp2 = icmp eq i32 %and, 0
  br i1 %cmp2, label %if.then, label %if.else

while.end:                                        ; preds = %while.cond
  %6 = bitcast i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.2, i32 0, i32 0) to i8*
  %7 = load i32, i32* %odd_sum_6, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %6, i32 %7)
  %8 = bitcast i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.3, i32 0, i32 0) to i8*
  %9 = load i32, i32* %even_sum_7, align 4
  %call4 = call i32 (i8*, ...) @printf(i8* %8, i32 %9)
  ret i32 0

if.then:                                          ; preds = %while.body
  %10 = load i32, i32* %even_sum_7, align 4
  %11 = load i32, i32* %i_4, align 4
  %add = add i32 %10, %11
  store i32 %add, i32* %even_sum_7, align 4
  br label %if.end

if.else:                                          ; preds = %while.body
  %12 = load i32, i32* %odd_sum_6, align 4
  %13 = load i32, i32* %i_4, align 4
  %add.1 = add i32 %12, %13
  store i32 %add.1, i32* %odd_sum_6, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %14 = load i32, i32* %i_4, align 4
  %add.2 = add i32 %14, 1
  store i32 %add.2, i32* %i_4, align 4
  br label %while.cond
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
