; ModuleID = 'test64.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.complex = type { float, float }

@.str = private unnamed_addr constant [25 x i8] c"For 1st complex number \0A\00", align 1
@.str.1 = private unnamed_addr constant [45 x i8] c"Enter real and imaginary part respectively:\0A\00", align 1
@.str.2 = private unnamed_addr constant [6 x i8] c"%f %f\00", align 1
@.str.3 = private unnamed_addr constant [26 x i8] c"\0AFor 2nd complex number \0A\00", align 1
@.str.4 = private unnamed_addr constant [19 x i8] c"Sum = %.1f + %.1fi\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %n1 = alloca %struct.complex, align 4
  %n2 = alloca %struct.complex, align 4
  %temp = alloca %struct.complex, align 4
  %coerce = alloca %struct.complex, align 4
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str, i32 0, i32 0))
  %call1 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([45 x i8], [45 x i8]* @.str.1, i32 0, i32 0))
  %real = getelementptr inbounds %struct.complex, %struct.complex* %n1, i32 0, i32 0
  %imag = getelementptr inbounds %struct.complex, %struct.complex* %n1, i32 0, i32 1
  %call2 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.2, i32 0, i32 0), float* %real, float* %imag)
  %call3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.3, i32 0, i32 0))
  %call4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([45 x i8], [45 x i8]* @.str.1, i32 0, i32 0))
  %real5 = getelementptr inbounds %struct.complex, %struct.complex* %n2, i32 0, i32 0
  %imag6 = getelementptr inbounds %struct.complex, %struct.complex* %n2, i32 0, i32 1
  %call7 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.2, i32 0, i32 0), float* %real5, float* %imag6)
  %0 = bitcast %struct.complex* %n1 to <2 x float>*
  %1 = load <2 x float>, <2 x float>* %0, align 4
  %2 = bitcast %struct.complex* %n2 to <2 x float>*
  %3 = load <2 x float>, <2 x float>* %2, align 4
  %call8 = call <2 x float> @add(<2 x float> %1, <2 x float> %3)
  %4 = bitcast %struct.complex* %coerce to <2 x float>*
  store <2 x float> %call8, <2 x float>* %4, align 4
  %5 = bitcast %struct.complex* %temp to i8*
  %6 = bitcast %struct.complex* %coerce to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %5, i8* %6, i64 8, i32 4, i1 false)
  %real9 = getelementptr inbounds %struct.complex, %struct.complex* %temp, i32 0, i32 0
  %7 = load float, float* %real9, align 4
  %conv = fpext float %7 to double
  %imag10 = getelementptr inbounds %struct.complex, %struct.complex* %temp, i32 0, i32 1
  %8 = load float, float* %imag10, align 4
  %conv11 = fpext float %8 to double
  %call12 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.4, i32 0, i32 0), double %conv, double %conv11)
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @__isoc99_scanf(i8*, ...) #1

; Function Attrs: nounwind uwtable
define <2 x float> @add(<2 x float> %n1.coerce, <2 x float> %n2.coerce) #0 {
entry:
  %retval = alloca %struct.complex, align 4
  %n1 = alloca %struct.complex, align 8
  %n2 = alloca %struct.complex, align 8
  %temp = alloca %struct.complex, align 4
  %0 = bitcast %struct.complex* %n1 to <2 x float>*
  store <2 x float> %n1.coerce, <2 x float>* %0, align 8
  %1 = bitcast %struct.complex* %n2 to <2 x float>*
  store <2 x float> %n2.coerce, <2 x float>* %1, align 8
  %real = getelementptr inbounds %struct.complex, %struct.complex* %n1, i32 0, i32 0
  %2 = load float, float* %real, align 4
  %real1 = getelementptr inbounds %struct.complex, %struct.complex* %n2, i32 0, i32 0
  %3 = load float, float* %real1, align 4
  %add = fadd float %2, %3
  %real2 = getelementptr inbounds %struct.complex, %struct.complex* %temp, i32 0, i32 0
  store float %add, float* %real2, align 4
  %imag = getelementptr inbounds %struct.complex, %struct.complex* %n1, i32 0, i32 1
  %4 = load float, float* %imag, align 4
  %imag3 = getelementptr inbounds %struct.complex, %struct.complex* %n2, i32 0, i32 1
  %5 = load float, float* %imag3, align 4
  %add4 = fadd float %4, %5
  %imag5 = getelementptr inbounds %struct.complex, %struct.complex* %temp, i32 0, i32 1
  store float %add4, float* %imag5, align 4
  %6 = bitcast %struct.complex* %retval to i8*
  %7 = bitcast %struct.complex* %temp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %6, i8* %7, i64 8, i32 4, i1 false)
  %8 = bitcast %struct.complex* %retval to <2 x float>*
  %9 = load <2 x float>, <2 x float>* %8, align 4
  ret <2 x float> %9
}

; Function Attrs: nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture, i8* nocapture readonly, i64, i32, i1) #2

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
