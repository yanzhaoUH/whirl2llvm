; ModuleID = 'test23.bc'

@.str = private constant [19 x i8] c"Enter an integer: \00", align 1
@.str.1 = private constant [5 x i8] c"%lld\00", align 1
@.str.2 = private constant [21 x i8] c"Number of digits: %d\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %count_5 = alloca i32, align 4
  %n_4 = alloca i64, align 8
  store i32 0, i32* %count_5, align 4
  %0 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i64* %n_4 to i64*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i64* %2)
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %3 = load i64, i64* %n_4, align 8
  %cmp1 = icmp ne i64 %3, 0
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = load i64, i64* %n_4, align 8
  %div = sdiv i64 %4, 10
  store i64 %div, i64* %n_4, align 8
  %5 = load i32, i32* %count_5, align 4
  %add = add i32 %5, 1
  store i32 %add, i32* %count_5, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %6 = bitcast i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.2, i32 0, i32 0) to i8*
  %7 = load i32, i32* %count_5, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %6, i32 %7)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
