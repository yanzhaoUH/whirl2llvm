; ModuleID = 'test27_O.bc'

@.str = private constant [27 x i8] c"Enter a positive integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [22 x i8] c"%d is a prime number.\00", align 1
@.str.3 = private constant [26 x i8] c"%d is not a prime number.\00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_52 = alloca i32
  %.preg_I4_4_55 = alloca i32
  %.preg_I4_4_53 = alloca i32
  %.preg_I4_4_51 = alloca i32
  %.preg_I4_4_54 = alloca i32
  %.preg_I4_4_49 = alloca i32
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %_temp_ehpit0_11 = alloca i32, align 4
  %flag_6 = alloca i32, align 4
  %i_5 = alloca i32, align 4
  %n_4 = alloca i32, align 4
  %old_frame_pointer_16.addr = alloca i64, align 8
  %return_address_17.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  store i32 0, i32* %.preg_I4_4_49, align 8
  %3 = load i32, i32* %n_4, align 4
  store i32 %3, i32* %.preg_I4_4_54, align 8
  %4 = load i32, i32* %.preg_I4_4_54
  %div = sdiv i32 %4, 2
  store i32 %div, i32* %.preg_I4_4_51, align 8
  %5 = load i32, i32* %.preg_I4_4_51
  %add = add i32 %5, -2
  store i32 %add, i32* %.preg_I4_4_53, align 8
  %6 = load i32, i32* %.preg_I4_4_53
  %cmp1 = icmp sge i32 %6, 0
  br i1 %cmp1, label %tb, label %L4098

tb:                                               ; preds = %entry
  %7 = load i32, i32* %.preg_I4_4_51
  %add.1 = add i32 %7, -1
  store i32 %add.1, i32* %.preg_I4_4_55, align 8
  store i32 2, i32* %.preg_I4_4_52, align 8
  br label %L4610

L4098:                                            ; preds = %fb, %entry
  %8 = bitcast i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.2, i32 0, i32 0) to i8*
  %9 = load i32, i32* %.preg_I4_4_54
  %call3 = call i32 (i8*, ...) @printf(i8* %8, i32 %9)
  br label %L5890

L4610:                                            ; preds = %L4866, %tb
  %10 = load i32, i32* %.preg_I4_4_54
  %11 = load i32, i32* %.preg_I4_4_52
  %srem = srem i32 %10, %11
  %cmp2 = icmp eq i32 %srem, 0
  br i1 %cmp2, label %tb2, label %L4866

tb2:                                              ; preds = %L4610
  br label %L5378

L4866:                                            ; preds = %L4610
  %12 = load i32, i32* %.preg_I4_4_49
  %add.3 = add i32 %12, 1
  store i32 %add.3, i32* %.preg_I4_4_49, align 8
  %13 = load i32, i32* %.preg_I4_4_52
  %add.4 = add i32 %13, 1
  store i32 %add.4, i32* %.preg_I4_4_52, align 8
  %14 = load i32, i32* %.preg_I4_4_49
  %15 = load i32, i32* %.preg_I4_4_53
  %cmp3 = icmp sle i32 %14, %15
  br i1 %cmp3, label %L4610, label %fb

L5378:                                            ; preds = %tb2
  %16 = bitcast i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.3, i32 0, i32 0) to i8*
  %17 = load i32, i32* %.preg_I4_4_54
  %call4 = call i32 (i8*, ...) @printf(i8* %16, i32 %17)
  br label %L5890

fb:                                               ; preds = %L4866
  br label %L4098

L5890:                                            ; preds = %L5378, %L4098
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
