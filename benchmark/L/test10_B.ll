; ModuleID = 'test10.bc'

@.str = private constant [20 x i8] c"Enter an alphabet: \00", align 1
@.str.1 = private constant [3 x i8] c"%c\00", align 1
@.str.2 = private constant [15 x i8] c"%c is a vowel.\00", align 1
@.str.3 = private constant [19 x i8] c"%c is a consonant.\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %c_4 = alloca i8, align 1
  %isLowercaseVowel_5 = alloca i32, align 4
  %isUppercaseVowel_6 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i8* %c_4 to i8*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i8* %2)
  %3 = load i8, i8* %c_4, align 1
  %conv3 = sext i8 %3 to i32
  %cmp1 = icmp eq i32 %conv3, 97
  br i1 %cmp1, label %lor.end6, label %lor.rhs5

lor.rhs:                                          ; preds = %lor.end2
  %4 = load i8, i8* %c_4, align 1
  %conv310 = sext i8 %4 to i32
  %cmp5 = icmp eq i32 %conv310, 117
  br label %lor.end

lor.end:                                          ; preds = %lor.end2, %lor.rhs
  %5 = phi i1 [ true, %lor.end2 ], [ %cmp5, %lor.rhs ]
  %conv = zext i1 %5 to i32
  store i32 %conv, i32* %isLowercaseVowel_5, align 4
  %6 = load i8, i8* %c_4, align 1
  %conv319 = sext i8 %6 to i32
  %cmp6 = icmp eq i32 %conv319, 65
  br i1 %cmp6, label %lor.end18, label %lor.rhs17

lor.rhs1:                                         ; preds = %lor.end4
  %7 = load i8, i8* %c_4, align 1
  %conv39 = sext i8 %7 to i32
  %cmp4 = icmp eq i32 %conv39, 111
  br label %lor.end2

lor.end2:                                         ; preds = %lor.end4, %lor.rhs1
  %8 = phi i1 [ true, %lor.end4 ], [ %cmp4, %lor.rhs1 ]
  br i1 %8, label %lor.end, label %lor.rhs

lor.rhs3:                                         ; preds = %lor.end6
  %9 = load i8, i8* %c_4, align 1
  %conv38 = sext i8 %9 to i32
  %cmp3 = icmp eq i32 %conv38, 105
  br label %lor.end4

lor.end4:                                         ; preds = %lor.end6, %lor.rhs3
  %10 = phi i1 [ true, %lor.end6 ], [ %cmp3, %lor.rhs3 ]
  br i1 %10, label %lor.end2, label %lor.rhs1

lor.rhs5:                                         ; preds = %entry
  %11 = load i8, i8* %c_4, align 1
  %conv37 = sext i8 %11 to i32
  %cmp2 = icmp eq i32 %conv37, 101
  br label %lor.end6

lor.end6:                                         ; preds = %lor.rhs5, %entry
  %12 = phi i1 [ true, %entry ], [ %cmp2, %lor.rhs5 ]
  br i1 %12, label %lor.end4, label %lor.rhs3

lor.rhs11:                                        ; preds = %lor.end14
  %13 = load i8, i8* %c_4, align 1
  %conv323 = sext i8 %13 to i32
  %cmp10 = icmp eq i32 %conv323, 85
  br label %lor.end12

lor.end12:                                        ; preds = %lor.end14, %lor.rhs11
  %14 = phi i1 [ true, %lor.end14 ], [ %cmp10, %lor.rhs11 ]
  %conv24 = zext i1 %14 to i32
  store i32 %conv24, i32* %isUppercaseVowel_6, align 4
  %15 = load i32, i32* %isLowercaseVowel_5, align 4
  %16 = load i32, i32* %isUppercaseVowel_6, align 4
  %or = or i32 %15, %16
  %cmp11 = icmp ne i32 %or, 0
  br i1 %cmp11, label %if.then, label %if.else

lor.rhs13:                                        ; preds = %lor.end16
  %17 = load i8, i8* %c_4, align 1
  %conv322 = sext i8 %17 to i32
  %cmp9 = icmp eq i32 %conv322, 79
  br label %lor.end14

lor.end14:                                        ; preds = %lor.end16, %lor.rhs13
  %18 = phi i1 [ true, %lor.end16 ], [ %cmp9, %lor.rhs13 ]
  br i1 %18, label %lor.end12, label %lor.rhs11

lor.rhs15:                                        ; preds = %lor.end18
  %19 = load i8, i8* %c_4, align 1
  %conv321 = sext i8 %19 to i32
  %cmp8 = icmp eq i32 %conv321, 73
  br label %lor.end16

lor.end16:                                        ; preds = %lor.end18, %lor.rhs15
  %20 = phi i1 [ true, %lor.end18 ], [ %cmp8, %lor.rhs15 ]
  br i1 %20, label %lor.end14, label %lor.rhs13

lor.rhs17:                                        ; preds = %lor.end
  %21 = load i8, i8* %c_4, align 1
  %conv320 = sext i8 %21 to i32
  %cmp7 = icmp eq i32 %conv320, 69
  br label %lor.end18

lor.end18:                                        ; preds = %lor.rhs17, %lor.end
  %22 = phi i1 [ true, %lor.end ], [ %cmp7, %lor.rhs17 ]
  br i1 %22, label %lor.end16, label %lor.rhs15

if.then:                                          ; preds = %lor.end12
  %23 = bitcast i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i32 0, i32 0) to i8*
  %24 = load i8, i8* %c_4, align 1
  %conv325 = sext i8 %24 to i32
  %call3 = call i32 (i8*, ...) @printf(i8* %23, i32 %conv325)
  br label %if.end

if.else:                                          ; preds = %lor.end12
  %25 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.3, i32 0, i32 0) to i8*
  %26 = load i8, i8* %c_4, align 1
  %conv326 = sext i8 %26 to i32
  %call4 = call i32 (i8*, ...) @printf(i8* %25, i32 %conv326)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
