; ModuleID = 'test335.bc'

%struct.sTest = type { i32, float }

@.str = private constant [4 x i8] c"%d\0A\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %a_4 = alloca [4 x i32], align 4
  %p_8 = alloca i32*, align 4
  %s1_6 = alloca %struct.sTest, align 4
  %arrayidx = getelementptr [4 x i32], [4 x i32]* %a_4, i32 0, i32 2
  store i32 26, i32* %arrayidx, align 4
  %0 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0) to i8*
  %arrayidx1 = getelementptr [4 x i32], [4 x i32]* %a_4, i32 0, i32 2
  %1 = load i32, i32* %arrayidx1, align 4
  %call1 = call i32 (i8*, ...) @printf(i8* %0, i32 %1)
  %mI = getelementptr %struct.sTest, %struct.sTest* %s1_6, i32 0, i32 0
  %mF = getelementptr %struct.sTest, %struct.sTest* %s1_6, i32 0, i32 1
  store i32 10, i32* %mI, align 4
  %2 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0) to i8*
  %mI2 = getelementptr %struct.sTest, %struct.sTest* %s1_6, i32 0, i32 0
  %mF3 = getelementptr %struct.sTest, %struct.sTest* %s1_6, i32 0, i32 1
  %3 = load i32, i32* %mI2, align 4
  %call2 = call i32 (i8*, ...) @printf(i8* %2, i32 %3)
  %arrayAddress = getelementptr [4 x i32], [4 x i32]* %a_4, i32 0, i32 0
  store i32* %arrayAddress, i32** %p_8, align 8
  %4 = load i32*, i32** %p_8, align 8
  store i32 66, i32* %4, align 4
  %5 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0) to i8*
  %arrayidx4 = getelementptr [4 x i32], [4 x i32]* %a_4, i32 0, i32 0
  %6 = load i32, i32* %arrayidx4, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %5, i32 %6)
  ret i32 0
}

declare i32 @printf(i8*, ...)
