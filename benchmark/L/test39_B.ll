; ModuleID = 'test39.bc'

@.str = private constant [30 x i8] c"Enter two positive integers: \00", align 1
@.str.1 = private constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private constant [26 x i8] c"G.C.D of %d and %d is %d.\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %n1_4 = alloca i32, align 4
  %n2_5 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n1_4 to i32*
  %3 = bitcast i32* %n2_5 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2, i32* %3)
  %4 = bitcast i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.2, i32 0, i32 0) to i8*
  %5 = load i32, i32* %n1_4, align 4
  %6 = load i32, i32* %n2_5, align 4
  %7 = load i32, i32* %n1_4, align 4
  %8 = load i32, i32* %n2_5, align 4
  %call3 = call i32 @_Z3hcfii(i32 %7, i32 %8)
  %call4 = call i32 (i8*, ...) @printf(i8* %4, i32 %5, i32 %6, i32 %call3)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define i32 @_Z3hcfii(i32 %n1_4, i32 %n2_5) {
entry:
  %_temp_dummy13_6 = alloca i32, align 4
  %n1_4.addr = alloca i32, align 4
  %n2_5.addr = alloca i32, align 4
  store i32 %n1_4, i32* %n1_4.addr, align 4
  store i32 %n2_5, i32* %n2_5.addr, align 4
  %0 = load i32, i32* %n2_5.addr, align 4
  %cmp1 = icmp ne i32 %0, 0
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %n2_5.addr, align 4
  %2 = load i32, i32* %n1_4.addr, align 4
  %3 = load i32, i32* %n2_5.addr, align 4
  %srem = srem i32 %2, %3
  %call5 = call i32 @_Z3hcfii(i32 %1, i32 %srem)
  ret i32 %call5

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %n1_4.addr, align 4
  ret i32 %4
}
