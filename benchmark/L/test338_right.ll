; ModuleID = 'test338.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.str = type { i8, double, [3 x i32], %struct.str_child, i32 }
%struct.str_child = type { i32, double*, i8 }

@.str = private unnamed_addr constant [4 x i8] c"%d\0A\00", align 1
@.str.1 = private unnamed_addr constant [12 x i8] c"test 1 = %c\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %str1 = alloca %struct.str, align 8
  %dd = alloca double, align 8
  %str_array = alloca [10 x %struct.str], align 16
  %ccc = alloca i32, align 4
  store i32 0, i32* %retval
  %end = getelementptr inbounds %struct.str, %struct.str* %str1, i32 0, i32 4
  store i32 2, i32* %end, align 4
  store double 1.000000e+00, double* %dd, align 8
  %sch1 = getelementptr inbounds %struct.str, %struct.str* %str1, i32 0, i32 3
  %b_child = getelementptr inbounds %struct.str_child, %struct.str_child* %sch1, i32 0, i32 1
  store double* %dd, double** %b_child, align 8
  %sch11 = getelementptr inbounds %struct.str, %struct.str* %str1, i32 0, i32 3
  %a_child = getelementptr inbounds %struct.str_child, %struct.str_child* %sch11, i32 0, i32 0
  store i32 33, i32* %a_child, align 4
  %array123 = getelementptr inbounds %struct.str, %struct.str* %str1, i32 0, i32 2
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %array123, i32 0, i64 1
  store i32 66, i32* %arrayidx, align 4
  %sch12 = getelementptr inbounds %struct.str, %struct.str* %str1, i32 0, i32 3
  %a_child3 = getelementptr inbounds %struct.str_child, %struct.str_child* %sch12, i32 0, i32 0
  %0 = load i32, i32* %a_child3, align 4
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0), i32 %0)
  %array1234 = getelementptr inbounds %struct.str, %struct.str* %str1, i32 0, i32 2
  %arrayidx5 = getelementptr inbounds [3 x i32], [3 x i32]* %array1234, i32 0, i64 1
  %1 = load i32, i32* %arrayidx5, align 4
  %call6 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0), i32 %1)
  store i32 97, i32* %ccc, align 4
  %2 = load i32, i32* %ccc, align 4
  %conv = trunc i32 %2 to i8
  %arrayidx7 = getelementptr inbounds [10 x %struct.str], [10 x %struct.str]* %str_array, i32 0, i64 1
  %c = getelementptr inbounds %struct.str, %struct.str* %arrayidx7, i32 0, i32 0
  store i8 %conv, i8* %c, align 1
  %arrayidx8 = getelementptr inbounds [10 x %struct.str], [10 x %struct.str]* %str_array, i32 0, i64 1
  %fp = getelementptr inbounds %struct.str, %struct.str* %arrayidx8, i32 0, i32 1
  store double 1.000000e+00, double* %fp, align 8
  %3 = load i32, i32* %ccc, align 4
  %conv9 = trunc i32 %3 to i8
  %arrayidx10 = getelementptr inbounds [10 x %struct.str], [10 x %struct.str]* %str_array, i32 0, i64 1
  %sch111 = getelementptr inbounds %struct.str, %struct.str* %arrayidx10, i32 0, i32 3
  %c_child = getelementptr inbounds %struct.str_child, %struct.str_child* %sch111, i32 0, i32 2
  store i8 %conv9, i8* %c_child, align 1
  %arrayidx12 = getelementptr inbounds [10 x %struct.str], [10 x %struct.str]* %str_array, i32 0, i64 1
  %sch113 = getelementptr inbounds %struct.str, %struct.str* %arrayidx12, i32 0, i32 3
  %c_child14 = getelementptr inbounds %struct.str_child, %struct.str_child* %sch113, i32 0, i32 2
  %4 = load i8, i8* %c_child14, align 1
  %conv15 = sext i8 %4 to i32
  %call16 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i32 0, i32 0), i32 %conv15)
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
