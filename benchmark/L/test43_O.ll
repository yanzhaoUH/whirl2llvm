; ModuleID = 'test43_O.bc'

%struct._temp_dummy12 = type {}
%struct._temp_dummy13 = type {}
%struct._temp_dummy14 = type {}

@.str = private constant [19 x i8] c"Enter a sentence: \00", align 1
@.str.1 = private constant [3 x i8] c"%c\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_4 = alloca i32, align 4
  %_temp_dummy11_5 = alloca i32, align 4
  %_temp_dummy12_10 = alloca i32, align 4
  %_temp_dummy12_13 = alloca %struct._temp_dummy12, align 4
  %_temp_dummy13_11 = alloca i32, align 4
  %_temp_dummy13_14 = alloca %struct._temp_dummy13, align 4
  %_temp_dummy14_12 = alloca i32, align 4
  %_temp_dummy14_15 = alloca %struct._temp_dummy14, align 4
  %_temp_ehpit0_16 = alloca i32, align 4
  %c_9 = alloca i8, align 1
  %old_frame_pointer_21.addr = alloca i64, align 8
  %return_address_22.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i8* %c_9 to i8*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i8* %2)
  %3 = load i8, i8* %c_9, align 1
  %conv3 = sext i8 %3 to i32
  %cmp1 = icmp ne i32 %conv3, 10
  br i1 %cmp1, label %tb, label %L1538

tb:                                               ; preds = %entry
  call void @_Z7Reversev()
  %4 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %5 = load i8, i8* %c_9, align 1
  %conv31 = sext i8 %5 to i32
  %call4 = call i32 (i8*, ...) @printf(i8* %4, i32 %conv31)
  br label %L1538

L1538:                                            ; preds = %tb, %entry
  ret i32 0
}

declare i32 @printf(i8*, ...)

define void @_Z7Reversev() {
entry:
  %_temp_dummy12_5 = alloca i32, align 4
  %_temp_dummy13_6 = alloca i32, align 4
  %_temp_dummy14_7 = alloca i32, align 4
  %_temp_ehpit1_8 = alloca i32, align 4
  %c_4 = alloca i8, align 1
  %old_frame_pointer_13.addr = alloca i64, align 8
  %return_address_14.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %1 = bitcast i8* %c_4 to i8*
  %call5 = call i32 (i8*, ...) @scanf(i8* %0, i8* %1)
  %2 = load i8, i8* %c_4, align 1
  %conv3 = sext i8 %2 to i32
  %cmp2 = icmp ne i32 %conv3, 10
  br i1 %cmp2, label %tb, label %L1538

tb:                                               ; preds = %entry
  call void @_Z7Reversev()
  %3 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %4 = load i8, i8* %c_4, align 1
  %conv31 = sext i8 %4 to i32
  %call7 = call i32 (i8*, ...) @printf(i8* %3, i32 %conv31)
  br label %L1538

L1538:                                            ; preds = %tb, %entry
  ret void
}

declare i32 @scanf(i8*, ...)
