; ModuleID = 'test340.bc'

@.str = private constant [3 x i8] c"%d\00", align 1
@.str.1 = private constant [6 x i8] c"hello\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %i_4 = alloca i32, align 4
  %j_6 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str, i32 0, i32 0) to i8*
  %1 = bitcast i32* %i_4 to i32*
  %call1 = call i32 (i8*, ...) @scanf(i8* %0, i32* %1)
  %2 = load i32, i32* %i_4, align 4
  store i32 %2, i32* %j_6, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %3 = load i32, i32* %j_6, align 4
  %cmp1 = icmp sle i32 %3, 9
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str, i32 0, i32 0) to i8*
  %5 = load i32, i32* %j_6, align 4
  %call2 = call i32 (i8*, ...) @printf(i8* %4, i32 %5)
  %6 = load i32, i32* %j_6, align 4
  %add = add i32 %6, 1
  store i32 %add, i32* %j_6, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %7 = load i32, i32* %j_6, align 4
  %add.1 = add i32 %7, 10
  store i32 %add.1, i32* %j_6, align 4
  %8 = load i32, i32* %j_6, align 4
  %cmp2 = icmp sgt i32 %8, 100
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %while.end
  %9 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %9)
  br label %if.end

if.else:                                          ; preds = %while.end
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret i32 0
}

declare i32 @scanf(i8*, ...)

declare i32 @printf(i8*, ...)
