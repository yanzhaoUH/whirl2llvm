; ModuleID = 'test2_O.bc'

@.str = private constant [21 x i8] c"Enter two integers: \00", align 1
@.str.1 = private constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private constant [13 x i8] c"%d + %d = %d\00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_50 = alloca i32
  %.preg_I4_4_49 = alloca i32
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_ehpit0_10 = alloca i32, align 4
  %firstNumber_4 = alloca i32, align 4
  %old_frame_pointer_15.addr = alloca i64, align 8
  %return_address_16.addr = alloca i64, align 8
  %secondNumber_5 = alloca i32, align 4
  %sumOfTwoNumbers_6 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %firstNumber_4 to i32*
  %3 = bitcast i32* %secondNumber_5 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2, i32* %3)
  %4 = load i32, i32* %secondNumber_5, align 4
  store i32 %4, i32* %.preg_I4_4_49, align 8
  %5 = load i32, i32* %firstNumber_4, align 4
  store i32 %5, i32* %.preg_I4_4_50, align 8
  %6 = bitcast i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.2, i32 0, i32 0) to i8*
  %7 = load i32, i32* %.preg_I4_4_50
  %8 = load i32, i32* %.preg_I4_4_49
  %9 = load i32, i32* %.preg_I4_4_49
  %10 = load i32, i32* %.preg_I4_4_50
  %add = add i32 %9, %10
  %call3 = call i32 (i8*, ...) @printf(i8* %6, i32 %7, i32 %8, i32 %add)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
