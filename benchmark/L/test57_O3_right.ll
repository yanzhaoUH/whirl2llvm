; ModuleID = 'test57.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [17 x i8] c"Enter a string: \00", align 1
@.str.1 = private unnamed_addr constant [16 x i8] c"Output String: \00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %line = alloca [150 x i8], align 16
  %0 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i64 0, i64 0
  call void @llvm.lifetime.start(i64 150, i8* %0) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) bitcast (i32 (...)* @gets to i32 (i8*, ...)*)(i8* %0) #1
  %1 = load i8, i8* %0, align 16, !tbaa !1
  %cmp.70 = icmp eq i8 %1, 0
  br i1 %cmp.70, label %for.end.44, label %while.cond.preheader.preheader

while.cond.preheader.preheader:                   ; preds = %entry
  br label %while.cond.preheader

while.cond.preheader:                             ; preds = %while.cond.preheader.preheader, %for.inc.42
  %indvars.iv77 = phi i64 [ %indvars.iv.next78, %for.inc.42 ], [ 0, %while.cond.preheader.preheader ]
  %2 = phi i8 [ %10, %for.inc.42 ], [ %1, %while.cond.preheader.preheader ]
  %arrayidx73 = phi i8* [ %arrayidx, %for.inc.42 ], [ %0, %while.cond.preheader.preheader ]
  %.off.68 = add i8 %2, -97
  %3 = icmp ult i8 %.off.68, 26
  br i1 %3, label %for.inc.42, label %lor.lhs.false.lr.ph

lor.lhs.false.lr.ph:                              ; preds = %while.cond.preheader
  %arrayidx31.64 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i64 0, i64 %indvars.iv77
  br label %lor.lhs.false

lor.lhs.false:                                    ; preds = %lor.lhs.false.lr.ph, %for.end
  %4 = phi i8 [ %2, %lor.lhs.false.lr.ph ], [ %.pr, %for.end ]
  %.off62 = add i8 %4, -65
  %5 = icmp ult i8 %.off62, 26
  %cmp27 = icmp eq i8 %4, 0
  %or.cond61 = or i1 %cmp27, %5
  br i1 %or.cond61, label %for.inc.42.loopexit, label %for.cond.29.preheader

for.cond.29.preheader:                            ; preds = %lor.lhs.false
  %6 = load i8, i8* %arrayidx31.64, align 1, !tbaa !1
  %cmp33.65 = icmp eq i8 %6, 0
  br i1 %cmp33.65, label %for.end, label %for.body.35.preheader

for.body.35.preheader:                            ; preds = %for.cond.29.preheader
  br label %for.body.35

for.body.35:                                      ; preds = %for.body.35.preheader, %for.body.35
  %indvars.iv75 = phi i64 [ %indvars.iv.next76, %for.body.35 ], [ %indvars.iv77, %for.body.35.preheader ]
  %arrayidx3167 = phi i8* [ %arrayidx37, %for.body.35 ], [ %arrayidx31.64, %for.body.35.preheader ]
  %indvars.iv.next76 = add nuw nsw i64 %indvars.iv75, 1
  %arrayidx37 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i64 0, i64 %indvars.iv.next76
  %7 = load i8, i8* %arrayidx37, align 1, !tbaa !1
  store i8 %7, i8* %arrayidx3167, align 1, !tbaa !1
  %8 = load i8, i8* %arrayidx37, align 1, !tbaa !1
  %cmp33 = icmp eq i8 %8, 0
  br i1 %cmp33, label %for.end.loopexit, label %for.body.35

for.end.loopexit:                                 ; preds = %for.body.35
  %arrayidx37.lcssa = phi i8* [ %arrayidx37, %for.body.35 ]
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %for.cond.29.preheader
  %arrayidx31.lcssa = phi i8* [ %arrayidx31.64, %for.cond.29.preheader ], [ %arrayidx37.lcssa, %for.end.loopexit ]
  store i8 0, i8* %arrayidx31.lcssa, align 1, !tbaa !1
  %.pr = load i8, i8* %arrayidx73, align 1, !tbaa !1
  %.off = add i8 %.pr, -97
  %9 = icmp ult i8 %.off, 26
  br i1 %9, label %for.inc.42.loopexit, label %lor.lhs.false

for.inc.42.loopexit:                              ; preds = %lor.lhs.false, %for.end
  br label %for.inc.42

for.inc.42:                                       ; preds = %for.inc.42.loopexit, %while.cond.preheader
  %indvars.iv.next78 = add nuw nsw i64 %indvars.iv77, 1
  %arrayidx = getelementptr inbounds [150 x i8], [150 x i8]* %line, i64 0, i64 %indvars.iv.next78
  %10 = load i8, i8* %arrayidx, align 1, !tbaa !1
  %cmp = icmp eq i8 %10, 0
  br i1 %cmp, label %for.end.44.loopexit, label %while.cond.preheader

for.end.44.loopexit:                              ; preds = %for.inc.42
  br label %for.end.44

for.end.44:                                       ; preds = %for.end.44.loopexit, %entry
  %call45 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.1, i64 0, i64 0)) #1
  %call47 = call i32 @puts(i8* %0) #1
  call void @llvm.lifetime.end(i64 150, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @gets(...) #2

; Function Attrs: nounwind
declare i32 @puts(i8* nocapture readonly) #2

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"omnipotent char", !3, i64 0}
!3 = !{!"Simple C/C++ TBAA"}
