; ModuleID = 'test44.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [20 x i8] c"Enter base number: \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.2 = private unnamed_addr constant [39 x i8] c"Enter power number(positive integer): \00", align 1
@.str.3 = private unnamed_addr constant [11 x i8] c"%d^%d = %d\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %base = alloca i32, align 4
  %exp = alloca i32, align 4
  %0 = bitcast i32* %base to i8*
  call void @llvm.lifetime.start(i64 4, i8* %0) #1
  %1 = bitcast i32* %exp to i8*
  call void @llvm.lifetime.start(i64 4, i8* %1) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %base) #1
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.2, i64 0, i64 0)) #1
  %call3 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %exp) #1
  %2 = load i32, i32* %base, align 4, !tbaa !1
  %3 = load i32, i32* %exp, align 4, !tbaa !1
  %cmp.3.i = icmp eq i32 %3, 1
  br i1 %cmp.3.i, label %power.exit, label %overflow.checked

overflow.checked:                                 ; preds = %entry
  %4 = add i32 %3, -1
  %end.idx = add i32 %3, -1
  %n.vec = and i32 %4, -8
  %cmp.zero = icmp eq i32 %n.vec, 0
  %ind.end = sub i32 %3, %n.vec
  %5 = insertelement <4 x i32> <i32 undef, i32 1, i32 1, i32 1>, i32 %2, i32 0
  br i1 %cmp.zero, label %middle.block, label %vector.ph

vector.ph:                                        ; preds = %overflow.checked
  %broadcast.splatinsert8 = insertelement <4 x i32> undef, i32 %2, i32 0
  %broadcast.splat9 = shufflevector <4 x i32> %broadcast.splatinsert8, <4 x i32> undef, <4 x i32> zeroinitializer
  %6 = add i32 %3, -9
  %7 = lshr i32 %6, 3
  %8 = add nuw nsw i32 %7, 1
  %xtraiter = and i32 %8, 7
  %lcmp.mod = icmp eq i32 %xtraiter, 0
  br i1 %lcmp.mod, label %vector.ph.split, label %vector.body.prol.preheader

vector.body.prol.preheader:                       ; preds = %vector.ph
  br label %vector.body.prol

vector.body.prol:                                 ; preds = %vector.body.prol.preheader, %vector.body.prol
  %index.prol = phi i32 [ %index.next.prol, %vector.body.prol ], [ 0, %vector.body.prol.preheader ]
  %vec.phi.prol = phi <4 x i32> [ %9, %vector.body.prol ], [ %5, %vector.body.prol.preheader ]
  %vec.phi7.prol = phi <4 x i32> [ %10, %vector.body.prol ], [ <i32 1, i32 1, i32 1, i32 1>, %vector.body.prol.preheader ]
  %prol.iter = phi i32 [ %prol.iter.sub, %vector.body.prol ], [ %xtraiter, %vector.body.prol.preheader ]
  %9 = mul nsw <4 x i32> %vec.phi.prol, %broadcast.splat9
  %10 = mul nsw <4 x i32> %vec.phi7.prol, %broadcast.splat9
  %index.next.prol = add i32 %index.prol, 8
  %prol.iter.sub = add i32 %prol.iter, -1
  %prol.iter.cmp = icmp eq i32 %prol.iter.sub, 0
  br i1 %prol.iter.cmp, label %vector.ph.split.loopexit, label %vector.body.prol, !llvm.loop !5

vector.ph.split.loopexit:                         ; preds = %vector.body.prol
  %index.next.prol.lcssa = phi i32 [ %index.next.prol, %vector.body.prol ]
  %.lcssa18 = phi <4 x i32> [ %10, %vector.body.prol ]
  %.lcssa17 = phi <4 x i32> [ %9, %vector.body.prol ]
  br label %vector.ph.split

vector.ph.split:                                  ; preds = %vector.ph.split.loopexit, %vector.ph
  %.lcssa14.unr = phi <4 x i32> [ undef, %vector.ph ], [ %.lcssa18, %vector.ph.split.loopexit ]
  %.lcssa.unr = phi <4 x i32> [ undef, %vector.ph ], [ %.lcssa17, %vector.ph.split.loopexit ]
  %index.unr = phi i32 [ 0, %vector.ph ], [ %index.next.prol.lcssa, %vector.ph.split.loopexit ]
  %vec.phi.unr = phi <4 x i32> [ %5, %vector.ph ], [ %.lcssa17, %vector.ph.split.loopexit ]
  %vec.phi7.unr = phi <4 x i32> [ <i32 1, i32 1, i32 1, i32 1>, %vector.ph ], [ %.lcssa18, %vector.ph.split.loopexit ]
  %11 = icmp ult i32 %6, 56
  br i1 %11, label %middle.block.loopexit, label %vector.ph.split.split

vector.ph.split.split:                            ; preds = %vector.ph.split
  br label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.ph.split.split
  %index = phi i32 [ %index.unr, %vector.ph.split.split ], [ %index.next.7, %vector.body ]
  %vec.phi = phi <4 x i32> [ %vec.phi.unr, %vector.ph.split.split ], [ %26, %vector.body ]
  %vec.phi7 = phi <4 x i32> [ %vec.phi7.unr, %vector.ph.split.split ], [ %27, %vector.body ]
  %12 = mul nsw <4 x i32> %vec.phi, %broadcast.splat9
  %13 = mul nsw <4 x i32> %vec.phi7, %broadcast.splat9
  %14 = mul nsw <4 x i32> %12, %broadcast.splat9
  %15 = mul nsw <4 x i32> %13, %broadcast.splat9
  %16 = mul nsw <4 x i32> %14, %broadcast.splat9
  %17 = mul nsw <4 x i32> %15, %broadcast.splat9
  %18 = mul nsw <4 x i32> %16, %broadcast.splat9
  %19 = mul nsw <4 x i32> %17, %broadcast.splat9
  %20 = mul nsw <4 x i32> %18, %broadcast.splat9
  %21 = mul nsw <4 x i32> %19, %broadcast.splat9
  %22 = mul nsw <4 x i32> %20, %broadcast.splat9
  %23 = mul nsw <4 x i32> %21, %broadcast.splat9
  %24 = mul nsw <4 x i32> %22, %broadcast.splat9
  %25 = mul nsw <4 x i32> %23, %broadcast.splat9
  %26 = mul nsw <4 x i32> %24, %broadcast.splat9
  %27 = mul nsw <4 x i32> %25, %broadcast.splat9
  %index.next.7 = add i32 %index, 64
  %28 = icmp eq i32 %index.next.7, %n.vec
  br i1 %28, label %middle.block.loopexit.unr-lcssa, label %vector.body, !llvm.loop !7

middle.block.loopexit.unr-lcssa:                  ; preds = %vector.body
  %.lcssa16 = phi <4 x i32> [ %27, %vector.body ]
  %.lcssa15 = phi <4 x i32> [ %26, %vector.body ]
  br label %middle.block.loopexit

middle.block.loopexit:                            ; preds = %vector.ph.split, %middle.block.loopexit.unr-lcssa
  %.lcssa14 = phi <4 x i32> [ %.lcssa14.unr, %vector.ph.split ], [ %.lcssa16, %middle.block.loopexit.unr-lcssa ]
  %.lcssa = phi <4 x i32> [ %.lcssa.unr, %vector.ph.split ], [ %.lcssa15, %middle.block.loopexit.unr-lcssa ]
  br label %middle.block

middle.block:                                     ; preds = %middle.block.loopexit, %overflow.checked
  %resume.val = phi i32 [ %3, %overflow.checked ], [ %ind.end, %middle.block.loopexit ]
  %new.indc.resume.val = phi i32 [ 0, %overflow.checked ], [ %n.vec, %middle.block.loopexit ]
  %rdx.vec.exit.phi = phi <4 x i32> [ %5, %overflow.checked ], [ %.lcssa, %middle.block.loopexit ]
  %rdx.vec.exit.phi10 = phi <4 x i32> [ <i32 1, i32 1, i32 1, i32 1>, %overflow.checked ], [ %.lcssa14, %middle.block.loopexit ]
  %bin.rdx = mul <4 x i32> %rdx.vec.exit.phi10, %rdx.vec.exit.phi
  %rdx.shuf = shufflevector <4 x i32> %bin.rdx, <4 x i32> undef, <4 x i32> <i32 2, i32 3, i32 undef, i32 undef>
  %bin.rdx11 = mul <4 x i32> %bin.rdx, %rdx.shuf
  %rdx.shuf12 = shufflevector <4 x i32> %bin.rdx11, <4 x i32> undef, <4 x i32> <i32 1, i32 undef, i32 undef, i32 undef>
  %bin.rdx13 = mul <4 x i32> %bin.rdx11, %rdx.shuf12
  %29 = extractelement <4 x i32> %bin.rdx13, i32 0
  %cmp.n = icmp eq i32 %end.idx, %new.indc.resume.val
  br i1 %cmp.n, label %power.exit, label %if.then.i.preheader

if.then.i.preheader:                              ; preds = %middle.block
  br label %if.then.i

if.then.i:                                        ; preds = %if.then.i.preheader, %if.then.i
  %exp.tr5.i = phi i32 [ %sub.i, %if.then.i ], [ %resume.val, %if.then.i.preheader ]
  %accumulator.tr4.i = phi i32 [ %mul.i, %if.then.i ], [ %29, %if.then.i.preheader ]
  %sub.i = add nsw i32 %exp.tr5.i, -1
  %mul.i = mul nsw i32 %accumulator.tr4.i, %2
  %cmp.i = icmp eq i32 %sub.i, 1
  br i1 %cmp.i, label %power.exit.loopexit, label %if.then.i, !llvm.loop !10

power.exit.loopexit:                              ; preds = %if.then.i
  %mul.i.lcssa = phi i32 [ %mul.i, %if.then.i ]
  br label %power.exit

power.exit:                                       ; preds = %power.exit.loopexit, %middle.block, %entry
  %accumulator.tr.lcssa.i = phi i32 [ %2, %entry ], [ %29, %middle.block ], [ %mul.i.lcssa, %power.exit.loopexit ]
  %call5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.3, i64 0, i64 0), i32 %2, i32 %3, i32 %accumulator.tr.lcssa.i) #1
  call void @llvm.lifetime.end(i64 4, i8* %1) #1
  call void @llvm.lifetime.end(i64 4, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind readnone uwtable
define i32 @power(i32 %base, i32 %exp) #3 {
entry:
  %cmp.3 = icmp eq i32 %exp, 1
  br i1 %cmp.3, label %return, label %overflow.checked

overflow.checked:                                 ; preds = %entry
  %0 = add i32 %exp, -1
  %end.idx = add i32 %exp, -1
  %n.vec = and i32 %0, -8
  %cmp.zero = icmp eq i32 %n.vec, 0
  %ind.end = sub i32 %exp, %n.vec
  %1 = insertelement <4 x i32> <i32 undef, i32 1, i32 1, i32 1>, i32 %base, i32 0
  br i1 %cmp.zero, label %middle.block, label %vector.ph

vector.ph:                                        ; preds = %overflow.checked
  %broadcast.splatinsert8 = insertelement <4 x i32> undef, i32 %base, i32 0
  %broadcast.splat9 = shufflevector <4 x i32> %broadcast.splatinsert8, <4 x i32> undef, <4 x i32> zeroinitializer
  %2 = add i32 %exp, -9
  %3 = lshr i32 %2, 3
  %4 = add nuw nsw i32 %3, 1
  %xtraiter = and i32 %4, 7
  %lcmp.mod = icmp eq i32 %xtraiter, 0
  br i1 %lcmp.mod, label %vector.ph.split, label %vector.body.prol.preheader

vector.body.prol.preheader:                       ; preds = %vector.ph
  br label %vector.body.prol

vector.body.prol:                                 ; preds = %vector.body.prol.preheader, %vector.body.prol
  %index.prol = phi i32 [ %index.next.prol, %vector.body.prol ], [ 0, %vector.body.prol.preheader ]
  %vec.phi.prol = phi <4 x i32> [ %5, %vector.body.prol ], [ %1, %vector.body.prol.preheader ]
  %vec.phi7.prol = phi <4 x i32> [ %6, %vector.body.prol ], [ <i32 1, i32 1, i32 1, i32 1>, %vector.body.prol.preheader ]
  %prol.iter = phi i32 [ %prol.iter.sub, %vector.body.prol ], [ %xtraiter, %vector.body.prol.preheader ]
  %5 = mul nsw <4 x i32> %vec.phi.prol, %broadcast.splat9
  %6 = mul nsw <4 x i32> %vec.phi7.prol, %broadcast.splat9
  %index.next.prol = add i32 %index.prol, 8
  %prol.iter.sub = add i32 %prol.iter, -1
  %prol.iter.cmp = icmp eq i32 %prol.iter.sub, 0
  br i1 %prol.iter.cmp, label %vector.ph.split.loopexit, label %vector.body.prol, !llvm.loop !12

vector.ph.split.loopexit:                         ; preds = %vector.body.prol
  %index.next.prol.lcssa = phi i32 [ %index.next.prol, %vector.body.prol ]
  %.lcssa18 = phi <4 x i32> [ %6, %vector.body.prol ]
  %.lcssa17 = phi <4 x i32> [ %5, %vector.body.prol ]
  br label %vector.ph.split

vector.ph.split:                                  ; preds = %vector.ph.split.loopexit, %vector.ph
  %.lcssa14.unr = phi <4 x i32> [ undef, %vector.ph ], [ %.lcssa18, %vector.ph.split.loopexit ]
  %.lcssa.unr = phi <4 x i32> [ undef, %vector.ph ], [ %.lcssa17, %vector.ph.split.loopexit ]
  %index.unr = phi i32 [ 0, %vector.ph ], [ %index.next.prol.lcssa, %vector.ph.split.loopexit ]
  %vec.phi.unr = phi <4 x i32> [ %1, %vector.ph ], [ %.lcssa17, %vector.ph.split.loopexit ]
  %vec.phi7.unr = phi <4 x i32> [ <i32 1, i32 1, i32 1, i32 1>, %vector.ph ], [ %.lcssa18, %vector.ph.split.loopexit ]
  %7 = icmp ult i32 %2, 56
  br i1 %7, label %middle.block.loopexit, label %vector.ph.split.split

vector.ph.split.split:                            ; preds = %vector.ph.split
  br label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.ph.split.split
  %index = phi i32 [ %index.unr, %vector.ph.split.split ], [ %index.next.7, %vector.body ]
  %vec.phi = phi <4 x i32> [ %vec.phi.unr, %vector.ph.split.split ], [ %22, %vector.body ]
  %vec.phi7 = phi <4 x i32> [ %vec.phi7.unr, %vector.ph.split.split ], [ %23, %vector.body ]
  %8 = mul nsw <4 x i32> %vec.phi, %broadcast.splat9
  %9 = mul nsw <4 x i32> %vec.phi7, %broadcast.splat9
  %10 = mul nsw <4 x i32> %8, %broadcast.splat9
  %11 = mul nsw <4 x i32> %9, %broadcast.splat9
  %12 = mul nsw <4 x i32> %10, %broadcast.splat9
  %13 = mul nsw <4 x i32> %11, %broadcast.splat9
  %14 = mul nsw <4 x i32> %12, %broadcast.splat9
  %15 = mul nsw <4 x i32> %13, %broadcast.splat9
  %16 = mul nsw <4 x i32> %14, %broadcast.splat9
  %17 = mul nsw <4 x i32> %15, %broadcast.splat9
  %18 = mul nsw <4 x i32> %16, %broadcast.splat9
  %19 = mul nsw <4 x i32> %17, %broadcast.splat9
  %20 = mul nsw <4 x i32> %18, %broadcast.splat9
  %21 = mul nsw <4 x i32> %19, %broadcast.splat9
  %22 = mul nsw <4 x i32> %20, %broadcast.splat9
  %23 = mul nsw <4 x i32> %21, %broadcast.splat9
  %index.next.7 = add i32 %index, 64
  %24 = icmp eq i32 %index.next.7, %n.vec
  br i1 %24, label %middle.block.loopexit.unr-lcssa, label %vector.body, !llvm.loop !13

middle.block.loopexit.unr-lcssa:                  ; preds = %vector.body
  %.lcssa16 = phi <4 x i32> [ %23, %vector.body ]
  %.lcssa15 = phi <4 x i32> [ %22, %vector.body ]
  br label %middle.block.loopexit

middle.block.loopexit:                            ; preds = %vector.ph.split, %middle.block.loopexit.unr-lcssa
  %.lcssa14 = phi <4 x i32> [ %.lcssa14.unr, %vector.ph.split ], [ %.lcssa16, %middle.block.loopexit.unr-lcssa ]
  %.lcssa = phi <4 x i32> [ %.lcssa.unr, %vector.ph.split ], [ %.lcssa15, %middle.block.loopexit.unr-lcssa ]
  br label %middle.block

middle.block:                                     ; preds = %middle.block.loopexit, %overflow.checked
  %resume.val = phi i32 [ %exp, %overflow.checked ], [ %ind.end, %middle.block.loopexit ]
  %new.indc.resume.val = phi i32 [ 0, %overflow.checked ], [ %n.vec, %middle.block.loopexit ]
  %rdx.vec.exit.phi = phi <4 x i32> [ %1, %overflow.checked ], [ %.lcssa, %middle.block.loopexit ]
  %rdx.vec.exit.phi10 = phi <4 x i32> [ <i32 1, i32 1, i32 1, i32 1>, %overflow.checked ], [ %.lcssa14, %middle.block.loopexit ]
  %bin.rdx = mul <4 x i32> %rdx.vec.exit.phi10, %rdx.vec.exit.phi
  %rdx.shuf = shufflevector <4 x i32> %bin.rdx, <4 x i32> undef, <4 x i32> <i32 2, i32 3, i32 undef, i32 undef>
  %bin.rdx11 = mul <4 x i32> %bin.rdx, %rdx.shuf
  %rdx.shuf12 = shufflevector <4 x i32> %bin.rdx11, <4 x i32> undef, <4 x i32> <i32 1, i32 undef, i32 undef, i32 undef>
  %bin.rdx13 = mul <4 x i32> %bin.rdx11, %rdx.shuf12
  %25 = extractelement <4 x i32> %bin.rdx13, i32 0
  %cmp.n = icmp eq i32 %end.idx, %new.indc.resume.val
  br i1 %cmp.n, label %return, label %if.then.preheader

if.then.preheader:                                ; preds = %middle.block
  br label %if.then

if.then:                                          ; preds = %if.then.preheader, %if.then
  %exp.tr5 = phi i32 [ %sub, %if.then ], [ %resume.val, %if.then.preheader ]
  %accumulator.tr4 = phi i32 [ %mul, %if.then ], [ %25, %if.then.preheader ]
  %sub = add nsw i32 %exp.tr5, -1
  %mul = mul nsw i32 %accumulator.tr4, %base
  %cmp = icmp eq i32 %sub, 1
  br i1 %cmp, label %return.loopexit, label %if.then, !llvm.loop !14

return.loopexit:                                  ; preds = %if.then
  %mul.lcssa = phi i32 [ %mul, %if.then ]
  br label %return

return:                                           ; preds = %return.loopexit, %middle.block, %entry
  %accumulator.tr.lcssa = phi i32 [ %base, %entry ], [ %25, %middle.block ], [ %mul.lcssa, %return.loopexit ]
  ret i32 %accumulator.tr.lcssa
}

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readnone uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = distinct !{!5, !6}
!6 = !{!"llvm.loop.unroll.disable"}
!7 = distinct !{!7, !8, !9}
!8 = !{!"llvm.loop.vectorize.width", i32 1}
!9 = !{!"llvm.loop.interleave.count", i32 1}
!10 = distinct !{!10, !11, !8, !9}
!11 = !{!"llvm.loop.unroll.runtime.disable"}
!12 = distinct !{!12, !6}
!13 = distinct !{!13, !8, !9}
!14 = distinct !{!14, !11, !8, !9}
