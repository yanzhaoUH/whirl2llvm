; ModuleID = 'test62.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.student = type { [50 x i8], i32, float }

@.str.1 = private unnamed_addr constant [13 x i8] c"Enter name: \00", align 1
@.str.2 = private unnamed_addr constant [3 x i8] c"%s\00", align 1
@s = common global %struct.student zeroinitializer, align 4
@.str.3 = private unnamed_addr constant [20 x i8] c"Enter roll number: \00", align 1
@.str.4 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.5 = private unnamed_addr constant [14 x i8] c"Enter marks: \00", align 1
@.str.6 = private unnamed_addr constant [3 x i8] c"%f\00", align 1
@.str.8 = private unnamed_addr constant [7 x i8] c"Name: \00", align 1
@.str.9 = private unnamed_addr constant [17 x i8] c"Roll number: %d\0A\00", align 1
@.str.10 = private unnamed_addr constant [13 x i8] c"Marks: %.1f\0A\00", align 1
@g_i = common global i32 0, align 4
@str = private unnamed_addr constant [19 x i8] c"Enter information:\00"
@str.11 = private unnamed_addr constant [24 x i8] c"Displaying Information:\00"

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %puts = tail call i32 @puts(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @str, i64 0, i64 0)) #2
  %call1 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.1, i64 0, i64 0)) #2
  %call2 = tail call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i64 0, i64 0), i8* getelementptr inbounds (%struct.student, %struct.student* @s, i64 0, i32 0, i64 0)) #2
  %call3 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.3, i64 0, i64 0)) #2
  %call4 = tail call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.4, i64 0, i64 0), i32* getelementptr inbounds (%struct.student, %struct.student* @s, i64 0, i32 1)) #2
  %call5 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.5, i64 0, i64 0)) #2
  %call6 = tail call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.6, i64 0, i64 0), float* getelementptr inbounds (%struct.student, %struct.student* @s, i64 0, i32 2)) #2
  %puts12 = tail call i32 @puts(i8* getelementptr inbounds ([24 x i8], [24 x i8]* @str.11, i64 0, i64 0)) #2
  %call8 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.8, i64 0, i64 0)) #2
  %call9 = tail call i32 @puts(i8* getelementptr inbounds (%struct.student, %struct.student* @s, i64 0, i32 0, i64 0)) #2
  %0 = load i32, i32* getelementptr inbounds (%struct.student, %struct.student* @s, i64 0, i32 1), align 4, !tbaa !1
  %call10 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.9, i64 0, i64 0), i32 %0) #2
  %1 = load float, float* getelementptr inbounds (%struct.student, %struct.student* @s, i64 0, i32 2), align 4, !tbaa !7
  %conv = fpext float %1 to double
  %call11 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.10, i64 0, i64 0), double %conv) #2
  ret i32 0
}

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #1

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #1

; Function Attrs: nounwind
declare i32 @puts(i8* nocapture readonly) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !5, i64 52}
!2 = !{!"student", !3, i64 0, !5, i64 52, !6, i64 56}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = !{!"int", !3, i64 0}
!6 = !{!"float", !3, i64 0}
!7 = !{!2, !6, i64 56}
