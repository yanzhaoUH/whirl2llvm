; ModuleID = 'test15_O.bc'

@.str = private constant [20 x i8] c"Enter a character: \00", align 1
@.str.1 = private constant [3 x i8] c"%c\00", align 1
@.str.2 = private constant [19 x i8] c"%c is an alphabet.\00", align 1
@.str.3 = private constant [23 x i8] c"%c is not an alphabet.\00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_49 = alloca i32
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_dummy11_6 = alloca i32, align 4
  %_temp_dummy12_7 = alloca i32, align 4
  %_temp_dummy13_8 = alloca i32, align 4
  %_temp_ehpit0_9 = alloca i32, align 4
  %c_4 = alloca i8, align 1
  %old_frame_pointer_14.addr = alloca i64, align 8
  %return_address_15.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i8* %c_4 to i8*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i8* %2)
  %3 = load i8, i8* %c_4, align 1
  %conv3 = sext i8 %3 to i32
  store i32 %conv3, i32* %.preg_I4_4_49, align 8
  %4 = load i32, i32* %.preg_I4_4_49
  %add = add i32 %4, -97
  %conv = trunc i32 %add to i8
  %conv31 = sext i8 %conv to i32
  %cmp1 = icmp ule i32 %conv31, 25
  br i1 %cmp1, label %tb, label %L1282

tb:                                               ; preds = %entry
  %5 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.2, i32 0, i32 0) to i8*
  %6 = load i32, i32* %.preg_I4_4_49
  %call3 = call i32 (i8*, ...) @printf(i8* %5, i32 %6)
  br label %L1538

L1282:                                            ; preds = %entry
  %7 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.3, i32 0, i32 0) to i8*
  %8 = load i32, i32* %.preg_I4_4_49
  %call4 = call i32 (i8*, ...) @printf(i8* %7, i32 %8)
  br label %L1538

L1538:                                            ; preds = %L1282, %tb
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
