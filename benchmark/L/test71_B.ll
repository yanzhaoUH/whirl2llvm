; ModuleID = 'test71.bc'

@.str = private constant [20 x i8] c"Enter an integer : \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [23 x i8] c"%d is an even integer\0A\00", align 1
@.str.3 = private constant [22 x i8] c"%d is an odd integer\0A\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %_temp_dummy13_9 = alloca i32, align 4
  %ival_4 = alloca i32, align 4
  %remainder_5 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %ival_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = load i32, i32* %ival_4, align 4
  %srem = srem i32 %3, 2
  store i32 %srem, i32* %remainder_5, align 4
  %4 = load i32, i32* %remainder_5, align 4
  %cmp1 = icmp eq i32 %4, 0
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.2, i32 0, i32 0) to i8*
  %6 = load i32, i32* %ival_4, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %5, i32 %6)
  br label %if.end

if.else:                                          ; preds = %entry
  %7 = bitcast i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.3, i32 0, i32 0) to i8*
  %8 = load i32, i32* %ival_4, align 4
  %call4 = call i32 (i8*, ...) @printf(i8* %7, i32 %8)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
