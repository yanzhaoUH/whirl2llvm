; ModuleID = 'test4.bc'

@.str = private constant [20 x i8] c"Enter a character: \00", align 1
@.str.1 = private constant [3 x i8] c"%c\00", align 1
@.str.2 = private constant [23 x i8] c"ASCII value of %c = %d\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_dummy11_6 = alloca i32, align 4
  %_temp_dummy12_7 = alloca i32, align 4
  %c_4 = alloca i8, align 1
  %0 = bitcast i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i8* %c_4 to i8*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i8* %2)
  %3 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.2, i32 0, i32 0) to i8*
  %4 = load i8, i8* %c_4, align 1
  %conv3 = sext i8 %4 to i32
  %5 = load i8, i8* %c_4, align 1
  %conv31 = sext i8 %5 to i32
  %call3 = call i32 (i8*, ...) @printf(i8* %3, i32 %conv3, i32 %conv31)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
