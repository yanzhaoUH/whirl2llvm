; ModuleID = 'test35.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [27 x i8] c"Enter a positive integer: \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.2 = private unnamed_addr constant [23 x i8] c"%d is a prime number.\0A\00", align 1
@.str.3 = private unnamed_addr constant [27 x i8] c"%d is not a prime number.\0A\00", align 1
@.str.4 = private unnamed_addr constant [27 x i8] c"%d is an Armstrong number.\00", align 1
@.str.5 = private unnamed_addr constant [31 x i8] c"%d is not an Armstrong number.\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %n = alloca i32, align 4
  %flag = alloca i32, align 4
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0))
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0), i32* %n)
  %0 = load i32, i32* %n, align 4
  %call2 = call i32 @checkPrimeNumber(i32 %0)
  store i32 %call2, i32* %flag, align 4
  %1 = load i32, i32* %flag, align 4
  %cmp = icmp eq i32 %1, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %n, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.2, i32 0, i32 0), i32 %2)
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load i32, i32* %n, align 4
  %call4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.3, i32 0, i32 0), i32 %3)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %4 = load i32, i32* %n, align 4
  %call5 = call i32 @checkArmstrongNumber(i32 %4)
  store i32 %call5, i32* %flag, align 4
  %5 = load i32, i32* %flag, align 4
  %cmp6 = icmp eq i32 %5, 1
  br i1 %cmp6, label %if.then.7, label %if.else.9

if.then.7:                                        ; preds = %if.end
  %6 = load i32, i32* %n, align 4
  %call8 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.4, i32 0, i32 0), i32 %6)
  br label %if.end.11

if.else.9:                                        ; preds = %if.end
  %7 = load i32, i32* %n, align 4
  %call10 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.5, i32 0, i32 0), i32 %7)
  br label %if.end.11

if.end.11:                                        ; preds = %if.else.9, %if.then.7
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @__isoc99_scanf(i8*, ...) #1

; Function Attrs: nounwind uwtable
define i32 @checkPrimeNumber(i32 %n) #0 {
entry:
  %n.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %flag = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32 1, i32* %flag, align 4
  store i32 2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %n.addr, align 4
  %div = sdiv i32 %1, 2
  %cmp = icmp sle i32 %0, %div
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %n.addr, align 4
  %3 = load i32, i32* %i, align 4
  %rem = srem i32 %2, %3
  %cmp1 = icmp eq i32 %rem, 0
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i32 0, i32* %flag, align 4
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %5 = load i32, i32* %flag, align 4
  ret i32 %5
}

; Function Attrs: nounwind uwtable
define i32 @checkArmstrongNumber(i32 %number) #0 {
entry:
  %number.addr = alloca i32, align 4
  %originalNumber = alloca i32, align 4
  %remainder = alloca i32, align 4
  %result = alloca i32, align 4
  %n = alloca i32, align 4
  %flag = alloca i32, align 4
  store i32 %number, i32* %number.addr, align 4
  store i32 0, i32* %result, align 4
  store i32 0, i32* %n, align 4
  %0 = load i32, i32* %number.addr, align 4
  store i32 %0, i32* %originalNumber, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32, i32* %originalNumber, align 4
  %cmp = icmp ne i32 %1, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load i32, i32* %originalNumber, align 4
  %div = sdiv i32 %2, 10
  store i32 %div, i32* %originalNumber, align 4
  %3 = load i32, i32* %n, align 4
  %inc = add nsw i32 %3, 1
  store i32 %inc, i32* %n, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i32, i32* %number.addr, align 4
  store i32 %4, i32* %originalNumber, align 4
  br label %while.cond.1

while.cond.1:                                     ; preds = %while.body.3, %while.end
  %5 = load i32, i32* %originalNumber, align 4
  %cmp2 = icmp ne i32 %5, 0
  br i1 %cmp2, label %while.body.3, label %while.end.8

while.body.3:                                     ; preds = %while.cond.1
  %6 = load i32, i32* %originalNumber, align 4
  %rem = srem i32 %6, 10
  store i32 %rem, i32* %remainder, align 4
  %7 = load i32, i32* %remainder, align 4
  %conv = sitofp i32 %7 to double
  %8 = load i32, i32* %n, align 4
  %conv4 = sitofp i32 %8 to double
  %call = call double @pow(double %conv, double %conv4) #3
  %9 = load i32, i32* %result, align 4
  %conv5 = sitofp i32 %9 to double
  %add = fadd double %conv5, %call
  %conv6 = fptosi double %add to i32
  store i32 %conv6, i32* %result, align 4
  %10 = load i32, i32* %originalNumber, align 4
  %div7 = sdiv i32 %10, 10
  store i32 %div7, i32* %originalNumber, align 4
  br label %while.cond.1

while.end.8:                                      ; preds = %while.cond.1
  %11 = load i32, i32* %result, align 4
  %12 = load i32, i32* %number.addr, align 4
  %cmp9 = icmp eq i32 %11, %12
  br i1 %cmp9, label %if.then, label %if.else

if.then:                                          ; preds = %while.end.8
  store i32 1, i32* %flag, align 4
  br label %if.end

if.else:                                          ; preds = %while.end.8
  store i32 0, i32* %flag, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %13 = load i32, i32* %flag, align 4
  ret i32 %13
}

; Function Attrs: nounwind
declare double @pow(double, double) #2

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
