; ModuleID = 'test67.bc'

%struct.course = type { i32, [30 x i8] }

@.str = private constant [26 x i8] c"Enter number of records: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [51 x i8] c"Enter name of the subject and marks respectively:\0A\00", align 1
@.str.3 = private constant [6 x i8] c"%s %d\00", align 1
@.str.4 = private constant [25 x i8] c"Displaying Information:\0A\00", align 1
@.str.5 = private constant [7 x i8] c"%s\09%d\0A\00", align 1

define i32 @main() {
entry:
  %_temp__casttmp2_9 = alloca %struct.course*, align 4
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %_temp_dummy14_11 = alloca i32, align 4
  %_temp_dummy15_12 = alloca i32, align 4
  %_temp_dummy16_13 = alloca i32, align 4
  %i_5 = alloca i32, align 4
  %noOfRecords_6 = alloca i32, align 4
  %ptr_4 = alloca %struct.course*, align 4
  %0 = bitcast i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %noOfRecords_6 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = load i32, i32* %noOfRecords_6, align 4
  %conv3 = sext i32 %3 to i64
  %mul = mul i64 %conv3, 36
  %call3 = call i8* @malloc(i64 %mul)
  %4 = bitcast i8* %call3 to %struct.course*
  store %struct.course* %4, %struct.course** %_temp__casttmp2_9, align 8
  %5 = load %struct.course*, %struct.course** %_temp__casttmp2_9, align 8
  store %struct.course* %5, %struct.course** %ptr_4, align 8
  store i32 0, i32* %i_5, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %6 = load i32, i32* %i_5, align 4
  %7 = load i32, i32* %noOfRecords_6, align 4
  %cmp1 = icmp slt i32 %6, %7
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %8 = bitcast i8* getelementptr inbounds ([51 x i8], [51 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call4 = call i32 (i8*, ...) @printf(i8* %8)
  %9 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.3, i32 0, i32 0) to i8*
  %10 = load i32, i32* %i_5, align 4
  %conv31 = sext i32 %10 to i64
  %mul.2 = mul i64 %conv31, 36
  %11 = load %struct.course*, %struct.course** %ptr_4, align 8
  %12 = ptrtoint %struct.course* %11 to i64
  %add = add i64 %mul.2, %12
  %add.3 = add i64 %add, 4
  %13 = inttoptr i64 %add.3 to [30 x i8]*
  %14 = load i32, i32* %i_5, align 4
  %conv34 = sext i32 %14 to i64
  %mul.5 = mul i64 %conv34, 36
  %15 = load %struct.course*, %struct.course** %ptr_4, align 8
  %16 = ptrtoint %struct.course* %15 to i64
  %add.6 = add i64 %mul.5, %16
  %add.7 = add i64 %add.6, 0
  %17 = inttoptr i64 %add.7 to i32*
  %call5 = call i32 (i8*, ...) @scanf(i8* %9, [30 x i8]* %13, i32* %17)
  %18 = load i32, i32* %i_5, align 4
  %add.8 = add i32 %18, 1
  store i32 %add.8, i32* %i_5, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %19 = bitcast i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.4, i32 0, i32 0) to i8*
  %call6 = call i32 (i8*, ...) @printf(i8* %19)
  store i32 0, i32* %i_5, align 4
  br label %while.cond9

while.cond9:                                      ; preds = %while.body10, %while.end
  %20 = load i32, i32* %i_5, align 4
  %21 = load i32, i32* %noOfRecords_6, align 4
  %cmp2 = icmp slt i32 %20, %21
  br i1 %cmp2, label %while.body10, label %while.end11

while.body10:                                     ; preds = %while.cond9
  %22 = bitcast i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.5, i32 0, i32 0) to i8*
  %23 = load i32, i32* %i_5, align 4
  %conv312 = sext i32 %23 to i64
  %mul.13 = mul i64 %conv312, 36
  %24 = load %struct.course*, %struct.course** %ptr_4, align 8
  %25 = ptrtoint %struct.course* %24 to i64
  %add.14 = add i64 %mul.13, %25
  %add.15 = add i64 %add.14, 4
  %26 = inttoptr i64 %add.15 to i8*
  %27 = load i32, i32* %i_5, align 4
  %conv316 = sext i32 %27 to i64
  %mul.17 = mul i64 %conv316, 36
  %28 = load %struct.course*, %struct.course** %ptr_4, align 8
  %29 = ptrtoint %struct.course* %28 to i64
  %add.18 = add i64 %mul.17, %29
  %30 = inttoptr i64 %add.18 to %struct.course*
  %marks = getelementptr %struct.course, %struct.course* %30, i32 0, i32 0
  %subject = getelementptr %struct.course, %struct.course* %30, i32 0, i32 1
  %31 = load i32, i32* %marks, align 4
  %call7 = call i32 (i8*, ...) @printf(i8* %22, i8* %26, i32 %31)
  %32 = load i32, i32* %i_5, align 4
  %add.19 = add i32 %32, 1
  store i32 %add.19, i32* %i_5, align 4
  br label %while.cond9

while.end11:                                      ; preds = %while.cond9
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

declare i8* @malloc(i64)
