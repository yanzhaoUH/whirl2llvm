; ModuleID = 'test334.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [20 x i8] c"%c %d %ld %f %lf %u\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %ch = alloca i8, align 1
  %shorti = alloca i16, align 2
  %bi = alloca i32, align 4
  %longi = alloca i64, align 8
  %eui = alloca i32, align 4
  %cf = alloca float, align 4
  %dd = alloca double, align 8
  store i8 97, i8* %ch, align 1
  store i16 2, i16* %shorti, align 2
  store i32 4, i32* %bi, align 4
  store i64 100, i64* %longi, align 8
  store i32 4, i32* %eui, align 4
  store float 1.000000e+00, float* %cf, align 4
  store double 2.000000e+00, double* %dd, align 8
  %0 = load i8, i8* %ch, align 1
  %conv = sext i8 %0 to i32
  %1 = load i32, i32* %bi, align 4
  %2 = load i64, i64* %longi, align 8
  %3 = load float, float* %cf, align 4
  %conv1 = fpext float %3 to double
  %4 = load double, double* %dd, align 8
  %5 = load i32, i32* %eui, align 4
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0), i32 %conv, i32 %1, i64 %2, double %conv1, double %4, i32 %5)
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
