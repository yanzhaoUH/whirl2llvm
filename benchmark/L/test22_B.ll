; ModuleID = 'test22.bc'

@.str = private constant [4 x i8] c"%c \00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_5 = alloca i32, align 4
  %c_4 = alloca i8, align 1
  %conv = trunc i32 65 to i8
  store i8 %conv, i8* %c_4, align 1
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i8, i8* %c_4, align 1
  %conv3 = sext i8 %0 to i32
  %cmp1 = icmp sle i32 %conv3, 90
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0) to i8*
  %2 = load i8, i8* %c_4, align 1
  %conv31 = sext i8 %2 to i32
  %call1 = call i32 (i8*, ...) @printf(i8* %1, i32 %conv31)
  %3 = load i8, i8* %c_4, align 1
  %conv32 = sext i8 %3 to i32
  %add = add i32 %conv32, 1
  %conv4 = trunc i32 %add to i8
  store i8 %conv4, i8* %c_4, align 1
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret i32 0
}

declare i32 @printf(i8*, ...)
