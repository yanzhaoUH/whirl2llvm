; ModuleID = 'test63.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.Distance = type { i32, float }

@.str.1 = private unnamed_addr constant [13 x i8] c"Enter feet: \00", align 1
@.str.2 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@d1 = common global %struct.Distance zeroinitializer, align 4
@.str.3 = private unnamed_addr constant [13 x i8] c"Enter inch: \00", align 1
@.str.4 = private unnamed_addr constant [3 x i8] c"%f\00", align 1
@d2 = common global %struct.Distance zeroinitializer, align 4
@sumOfDistances = common global %struct.Distance zeroinitializer, align 4
@.str.6 = private unnamed_addr constant [30 x i8] c"\0ASum of distances = %d'-%.1f\22\00", align 1
@str = private unnamed_addr constant [35 x i8] c"Enter information for 1st distance\00"
@str.7 = private unnamed_addr constant [36 x i8] c"\0AEnter information for 2nd distance\00"

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %puts = tail call i32 @puts(i8* getelementptr inbounds ([35 x i8], [35 x i8]* @str, i64 0, i64 0))
  %call1 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.1, i64 0, i64 0)) #2
  %call2 = tail call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i64 0, i64 0), i32* getelementptr inbounds (%struct.Distance, %struct.Distance* @d1, i64 0, i32 0)) #2
  %call3 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.3, i64 0, i64 0)) #2
  %call4 = tail call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.4, i64 0, i64 0), float* getelementptr inbounds (%struct.Distance, %struct.Distance* @d1, i64 0, i32 1)) #2
  %puts16 = tail call i32 @puts(i8* getelementptr inbounds ([36 x i8], [36 x i8]* @str.7, i64 0, i64 0))
  %call6 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.1, i64 0, i64 0)) #2
  %call7 = tail call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i64 0, i64 0), i32* getelementptr inbounds (%struct.Distance, %struct.Distance* @d2, i64 0, i32 0)) #2
  %call8 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.3, i64 0, i64 0)) #2
  %call9 = tail call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.4, i64 0, i64 0), float* getelementptr inbounds (%struct.Distance, %struct.Distance* @d2, i64 0, i32 1)) #2
  %0 = load i32, i32* getelementptr inbounds (%struct.Distance, %struct.Distance* @d1, i64 0, i32 0), align 4, !tbaa !1
  %1 = load i32, i32* getelementptr inbounds (%struct.Distance, %struct.Distance* @d2, i64 0, i32 0), align 4, !tbaa !1
  %add = add nsw i32 %1, %0
  store i32 %add, i32* getelementptr inbounds (%struct.Distance, %struct.Distance* @sumOfDistances, i64 0, i32 0), align 4, !tbaa !1
  %2 = load float, float* getelementptr inbounds (%struct.Distance, %struct.Distance* @d1, i64 0, i32 1), align 4, !tbaa !7
  %3 = load float, float* getelementptr inbounds (%struct.Distance, %struct.Distance* @d2, i64 0, i32 1), align 4, !tbaa !7
  %add10 = fadd float %2, %3
  store float %add10, float* getelementptr inbounds (%struct.Distance, %struct.Distance* @sumOfDistances, i64 0, i32 1), align 4, !tbaa !7
  %cmp = fcmp ogt float %add10, 1.200000e+01
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %conv13 = fadd float %add10, -1.200000e+01
  store float %conv13, float* getelementptr inbounds (%struct.Distance, %struct.Distance* @sumOfDistances, i64 0, i32 1), align 4, !tbaa !7
  %inc = add nsw i32 %add, 1
  store i32 %inc, i32* getelementptr inbounds (%struct.Distance, %struct.Distance* @sumOfDistances, i64 0, i32 0), align 4, !tbaa !1
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = phi float [ %conv13, %if.then ], [ %add10, %entry ]
  %5 = phi i32 [ %inc, %if.then ], [ %add, %entry ]
  %conv14 = fpext float %4 to double
  %call15 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.6, i64 0, i64 0), i32 %5, double %conv14) #2
  ret i32 0
}

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #1

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #1

; Function Attrs: nounwind
declare i32 @puts(i8* nocapture readonly) #2

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !3, i64 0}
!2 = !{!"Distance", !3, i64 0, !6, i64 4}
!3 = !{!"int", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!"float", !4, i64 0}
!7 = !{!2, !6, i64 4}
