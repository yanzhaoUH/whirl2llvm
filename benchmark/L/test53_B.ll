; ModuleID = 'test53.bc'

@.str = private constant [32 x i8] c"Enter a, b and c respectively: \00", align 1
@.str.1 = private constant [9 x i8] c"%d %d %d\00", align 1
@.str.2 = private constant [24 x i8] c"Value before swapping:\0A\00", align 1
@.str.3 = private constant [24 x i8] c"a = %d \0Ab = %d \0Ac = %d\0A\00", align 1
@.str.4 = private constant [23 x i8] c"Value after swapping:\0A\00", align 1
@.str.5 = private constant [23 x i8] c"a = %d \0Ab = %d \0Ac = %d\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %_temp_dummy14_11 = alloca i32, align 4
  %_temp_dummy15_12 = alloca i32, align 4
  %a_4 = alloca i32, align 4
  %b_5 = alloca i32, align 4
  %c_6 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %a_4 to i32*
  %3 = bitcast i32* %b_5 to i32*
  %4 = bitcast i32* %c_6 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2, i32* %3, i32* %4)
  %5 = bitcast i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %5)
  %6 = bitcast i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.3, i32 0, i32 0) to i8*
  %7 = load i32, i32* %a_4, align 4
  %8 = load i32, i32* %b_5, align 4
  %9 = load i32, i32* %c_6, align 4
  %call4 = call i32 (i8*, ...) @printf(i8* %6, i32 %7, i32 %8, i32 %9)
  %10 = bitcast i32* %a_4 to i32*
  %11 = bitcast i32* %b_5 to i32*
  %12 = bitcast i32* %c_6 to i32*
  call void @_Z10cyclicSwapPiS_S_(i32* %10, i32* %11, i32* %12)
  %13 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.4, i32 0, i32 0) to i8*
  %call6 = call i32 (i8*, ...) @printf(i8* %13)
  %14 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.5, i32 0, i32 0) to i8*
  %15 = load i32, i32* %a_4, align 4
  %16 = load i32, i32* %b_5, align 4
  %17 = load i32, i32* %c_6, align 4
  %call7 = call i32 (i8*, ...) @printf(i8* %14, i32 %15, i32 %16, i32 %17)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define void @_Z10cyclicSwapPiS_S_(i32* %a_4, i32* %b_5, i32* %c_6) {
entry:
  %a_4.addr = alloca i32*, align 4
  %b_5.addr = alloca i32*, align 4
  %c_6.addr = alloca i32*, align 4
  %temp_7 = alloca i32, align 4
  store i32* %a_4, i32** %a_4.addr, align 4
  store i32* %b_5, i32** %b_5.addr, align 4
  store i32* %c_6, i32** %c_6.addr, align 4
  %0 = load i32*, i32** %b_5.addr, align 8
  %1 = load i32, i32* %0, align 4
  store i32 %1, i32* %temp_7, align 4
  %2 = load i32*, i32** %b_5.addr, align 8
  %3 = load i32*, i32** %a_4.addr, align 8
  %4 = load i32, i32* %3, align 4
  store i32 %4, i32* %2, align 4
  %5 = load i32*, i32** %a_4.addr, align 8
  %6 = load i32*, i32** %c_6.addr, align 8
  %7 = load i32, i32* %6, align 4
  store i32 %7, i32* %5, align 4
  %8 = load i32*, i32** %c_6.addr, align 8
  %9 = load i32, i32* %temp_7, align 4
  store i32 %9, i32* %8, align 4
  ret void
}
