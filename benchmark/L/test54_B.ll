; ModuleID = 'test54.bc'

@.str = private constant [43 x i8] c"Enter total number of elements(1 to 100): \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [31 x i8] c"Error!!! memory not allocated.\00", align 1
@.str.3 = private constant [2 x i8] c"\0A\00", align 1
@.str.4 = private constant [18 x i8] c"Enter Number %d: \00", align 1
@.str.5 = private constant [23 x i8] c"Largest element = %.2f\00", align 1

define i32 @main() {
entry:
  %_temp__casttmp2_9 = alloca float*, align 4
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %_temp_dummy14_11 = alloca i32, align 4
  %_temp_dummy15_12 = alloca i32, align 4
  %_temp_dummy16_13 = alloca i32, align 4
  %data_6 = alloca float*, align 4
  %i_4 = alloca i32, align 4
  %num_5 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([43 x i8], [43 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %num_5 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = load i32, i32* %num_5, align 4
  %conv3 = sext i32 %3 to i64
  %call3 = call i8* @calloc(i64 %conv3, i64 4)
  %4 = bitcast i8* %call3 to float*
  store float* %4, float** %_temp__casttmp2_9, align 8
  %5 = load float*, float** %_temp__casttmp2_9, align 8
  store float* %5, float** %data_6, align 8
  %6 = load float*, float** %data_6, align 8
  %7 = ptrtoint float* %6 to i64
  %cmp1 = icmp eq i64 %7, 0
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %8 = bitcast i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call4 = call i32 (i8*, ...) @printf(i8* %8)
  call void @exit(i32 0)
  br label %if.end

if.else:                                          ; preds = %entry
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %9 = bitcast i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.3, i32 0, i32 0) to i8*
  %call6 = call i32 (i8*, ...) @printf(i8* %9)
  store i32 0, i32* %i_4, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end
  %10 = load i32, i32* %i_4, align 4
  %11 = load i32, i32* %num_5, align 4
  %cmp2 = icmp slt i32 %10, %11
  br i1 %cmp2, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %12 = bitcast i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.4, i32 0, i32 0) to i8*
  %13 = load i32, i32* %i_4, align 4
  %add = add i32 %13, 1
  %call7 = call i32 (i8*, ...) @printf(i8* %12, i32 %add)
  %14 = load i32, i32* %i_4, align 4
  %conv31 = sext i32 %14 to i64
  %mul = mul i64 %conv31, 4
  %15 = load float*, float** %data_6, align 8
  %16 = ptrtoint float* %15 to i64
  %add.2 = add i64 %mul, %16
  %17 = load i32, i32* %i_4, align 4
  %conv9 = sitofp i32 %17 to float
  %18 = inttoptr i64 %add.2 to float*
  store float %conv9, float* %18, align 4
  %19 = load i32, i32* %i_4, align 4
  %add.3 = add i32 %19, 1
  store i32 %add.3, i32* %i_4, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 1, i32* %i_4, align 4
  br label %while.cond4

while.cond4:                                      ; preds = %if.end15, %while.end
  %20 = load i32, i32* %i_4, align 4
  %21 = load i32, i32* %num_5, align 4
  %cmp3 = icmp slt i32 %20, %21
  br i1 %cmp3, label %while.body5, label %while.end6

while.body5:                                      ; preds = %while.cond4
  %22 = load float*, float** %data_6, align 8
  %23 = load float, float* %22, align 4
  %24 = load i32, i32* %i_4, align 4
  %conv39 = sext i32 %24 to i64
  %mul.10 = mul i64 %conv39, 4
  %25 = load float*, float** %data_6, align 8
  %26 = ptrtoint float* %25 to i64
  %add.11 = add i64 %mul.10, %26
  %27 = inttoptr i64 %add.11 to float*
  %28 = load float, float* %27, align 4
  %cmp4 = fcmp olt float %23, %28
  br i1 %cmp4, label %if.then7, label %if.else8

while.end6:                                       ; preds = %while.cond4
  %29 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.5, i32 0, i32 0) to i8*
  %30 = load float*, float** %data_6, align 8
  %31 = load float, float* %30, align 4
  %convDouble = fpext float %31 to double
  %call8 = call i32 (i8*, ...) @printf(i8* %29, double %convDouble)
  ret i32 0

if.then7:                                         ; preds = %while.body5
  %32 = load float*, float** %data_6, align 8
  %33 = load i32, i32* %i_4, align 4
  %conv312 = sext i32 %33 to i64
  %mul.13 = mul i64 %conv312, 4
  %34 = load float*, float** %data_6, align 8
  %35 = ptrtoint float* %34 to i64
  %add.14 = add i64 %mul.13, %35
  %36 = inttoptr i64 %add.14 to float*
  %37 = load float, float* %36, align 4
  store float %37, float* %32, align 4
  br label %if.end15

if.else8:                                         ; preds = %while.body5
  br label %if.end15

if.end15:                                         ; preds = %if.else8, %if.then7
  %38 = load i32, i32* %i_4, align 4
  %add.16 = add i32 %38, 1
  store i32 %add.16, i32* %i_4, align 4
  br label %while.cond4
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

declare i8* @calloc(i64, i64)

declare void @exit(i32)
