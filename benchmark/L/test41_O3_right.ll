; ModuleID = 'test41.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str.3 = private unnamed_addr constant [3 x i8] c"%c\00", align 1
@.str.4 = private unnamed_addr constant [24 x i8] c"Enter an octal number: \00", align 1
@.str.5 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.6 = private unnamed_addr constant [28 x i8] c"%d in octal = %d in decimal\00", align 1
@.str.7 = private unnamed_addr constant [25 x i8] c"Enter a decimal number: \00", align 1
@.str.8 = private unnamed_addr constant [28 x i8] c"%d in decimal = %d in octal\00", align 1
@str = private unnamed_addr constant [14 x i8] c"Instructions:\00"
@str.9 = private unnamed_addr constant [51 x i8] c"1. Enter alphabet 'o' to convert decimal to octal.\00"
@str.10 = private unnamed_addr constant [51 x i8] c"2. Enter alphabet 'd' to convert octal to decimal.\00"

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %n = alloca i32, align 4
  %c = alloca i8, align 1
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start(i64 4, i8* %0) #1
  call void @llvm.lifetime.start(i64 1, i8* nonnull %c) #1
  %puts = tail call i32 @puts(i8* getelementptr inbounds ([14 x i8], [14 x i8]* @str, i64 0, i64 0))
  %puts25 = tail call i32 @puts(i8* getelementptr inbounds ([51 x i8], [51 x i8]* @str.9, i64 0, i64 0))
  %puts26 = tail call i32 @puts(i8* getelementptr inbounds ([51 x i8], [51 x i8]* @str.10, i64 0, i64 0))
  %call3 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.3, i64 0, i64 0), i8* nonnull %c) #1
  %1 = load i8, i8* %c, align 1, !tbaa !1
  switch i8 %1, label %if.end [
    i8 100, label %if.then
    i8 68, label %if.then
  ]

if.then:                                          ; preds = %entry, %entry
  %call8 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.4, i64 0, i64 0)) #1
  %call9 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.5, i64 0, i64 0), i32* nonnull %n) #1
  %2 = load i32, i32* %n, align 4, !tbaa !4
  %cmp.12.i = icmp eq i32 %2, 0
  br i1 %cmp.12.i, label %octal_decimal.exit, label %while.body.i.preheader

while.body.i.preheader:                           ; preds = %if.then
  br label %while.body.i

while.body.i:                                     ; preds = %while.body.i.preheader, %while.body.i
  %n.addr.015.i = phi i32 [ %div.i, %while.body.i ], [ %2, %while.body.i.preheader ]
  %i.014.i = phi i32 [ %inc.i, %while.body.i ], [ 0, %while.body.i.preheader ]
  %decimal.013.i = phi i32 [ %conv4.i, %while.body.i ], [ 0, %while.body.i.preheader ]
  %rem1.i = srem i32 %n.addr.015.i, 10
  %div.i = sdiv i32 %n.addr.015.i, 10
  %conv.i = sitofp i32 %rem1.i to double
  %conv2.i = sitofp i32 %i.014.i to double
  %call.i = call double @pow(double 8.000000e+00, double %conv2.i) #1
  %mul.i = fmul double %conv.i, %call.i
  %conv3.i = sitofp i32 %decimal.013.i to double
  %add.i = fadd double %conv3.i, %mul.i
  %conv4.i = fptosi double %add.i to i32
  %inc.i = add nuw nsw i32 %i.014.i, 1
  %n.addr.015.off.i = add i32 %n.addr.015.i, 9
  %3 = icmp ult i32 %n.addr.015.off.i, 19
  br i1 %3, label %octal_decimal.exit.loopexit, label %while.body.i

octal_decimal.exit.loopexit:                      ; preds = %while.body.i
  %conv4.i.lcssa = phi i32 [ %conv4.i, %while.body.i ]
  br label %octal_decimal.exit

octal_decimal.exit:                               ; preds = %octal_decimal.exit.loopexit, %if.then
  %decimal.0.lcssa.i = phi i32 [ 0, %if.then ], [ %conv4.i.lcssa, %octal_decimal.exit.loopexit ]
  %call11 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.6, i64 0, i64 0), i32 %2, i32 %decimal.0.lcssa.i) #1
  %.pr = load i8, i8* %c, align 1, !tbaa !1
  br label %if.end

if.end:                                           ; preds = %entry, %octal_decimal.exit
  %4 = phi i8 [ %1, %entry ], [ %.pr, %octal_decimal.exit ]
  switch i8 %4, label %if.end.24 [
    i8 111, label %if.then.19
    i8 79, label %if.then.19
  ]

if.then.19:                                       ; preds = %if.end, %if.end
  %call20 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.7, i64 0, i64 0)) #1
  %call21 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.5, i64 0, i64 0), i32* nonnull %n) #1
  %5 = load i32, i32* %n, align 4, !tbaa !4
  %cmp.10.i = icmp eq i32 %5, 0
  br i1 %cmp.10.i, label %decimal_octal.exit, label %while.body.i.31.preheader

while.body.i.31.preheader:                        ; preds = %if.then.19
  br label %while.body.i.31

while.body.i.31:                                  ; preds = %while.body.i.31.preheader, %while.body.i.31
  %octal.013.i = phi i32 [ %add.i.30, %while.body.i.31 ], [ 0, %while.body.i.31.preheader ]
  %i.012.i = phi i32 [ %mul2.i, %while.body.i.31 ], [ 1, %while.body.i.31.preheader ]
  %n.addr.011.i = phi i32 [ %div.i.28, %while.body.i.31 ], [ %5, %while.body.i.31.preheader ]
  %rem1.i.27 = srem i32 %n.addr.011.i, 8
  %div.i.28 = sdiv i32 %n.addr.011.i, 8
  %mul.i.29 = mul nsw i32 %rem1.i.27, %i.012.i
  %add.i.30 = add nsw i32 %mul.i.29, %octal.013.i
  %mul2.i = mul nsw i32 %i.012.i, 10
  %n.addr.011.off.i = add i32 %n.addr.011.i, 7
  %6 = icmp ult i32 %n.addr.011.off.i, 15
  br i1 %6, label %decimal_octal.exit.loopexit, label %while.body.i.31

decimal_octal.exit.loopexit:                      ; preds = %while.body.i.31
  %add.i.30.lcssa = phi i32 [ %add.i.30, %while.body.i.31 ]
  br label %decimal_octal.exit

decimal_octal.exit:                               ; preds = %decimal_octal.exit.loopexit, %if.then.19
  %octal.0.lcssa.i = phi i32 [ 0, %if.then.19 ], [ %add.i.30.lcssa, %decimal_octal.exit.loopexit ]
  %call23 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.8, i64 0, i64 0), i32 %5, i32 %octal.0.lcssa.i) #1
  br label %if.end.24

if.end.24:                                        ; preds = %if.end, %decimal_octal.exit
  call void @llvm.lifetime.end(i64 1, i8* nonnull %c) #1
  call void @llvm.lifetime.end(i64 4, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind uwtable
define i32 @octal_decimal(i32 %n) #0 {
entry:
  %cmp.12 = icmp eq i32 %n, 0
  br i1 %cmp.12, label %while.end, label %while.body.preheader

while.body.preheader:                             ; preds = %entry
  br label %while.body

while.body:                                       ; preds = %while.body.preheader, %while.body
  %n.addr.015 = phi i32 [ %div, %while.body ], [ %n, %while.body.preheader ]
  %i.014 = phi i32 [ %inc, %while.body ], [ 0, %while.body.preheader ]
  %decimal.013 = phi i32 [ %conv4, %while.body ], [ 0, %while.body.preheader ]
  %rem1 = srem i32 %n.addr.015, 10
  %div = sdiv i32 %n.addr.015, 10
  %conv = sitofp i32 %rem1 to double
  %conv2 = sitofp i32 %i.014 to double
  %call = tail call double @pow(double 8.000000e+00, double %conv2) #1
  %mul = fmul double %conv, %call
  %conv3 = sitofp i32 %decimal.013 to double
  %add = fadd double %conv3, %mul
  %conv4 = fptosi double %add to i32
  %inc = add nuw nsw i32 %i.014, 1
  %n.addr.015.off = add i32 %n.addr.015, 9
  %0 = icmp ult i32 %n.addr.015.off, 19
  br i1 %0, label %while.end.loopexit, label %while.body

while.end.loopexit:                               ; preds = %while.body
  %conv4.lcssa = phi i32 [ %conv4, %while.body ]
  br label %while.end

while.end:                                        ; preds = %while.end.loopexit, %entry
  %decimal.0.lcssa = phi i32 [ 0, %entry ], [ %conv4.lcssa, %while.end.loopexit ]
  ret i32 %decimal.0.lcssa
}

; Function Attrs: nounwind readnone uwtable
define i32 @decimal_octal(i32 %n) #3 {
entry:
  %cmp.10 = icmp eq i32 %n, 0
  br i1 %cmp.10, label %while.end, label %while.body.preheader

while.body.preheader:                             ; preds = %entry
  br label %while.body

while.body:                                       ; preds = %while.body.preheader, %while.body
  %octal.013 = phi i32 [ %add, %while.body ], [ 0, %while.body.preheader ]
  %i.012 = phi i32 [ %mul2, %while.body ], [ 1, %while.body.preheader ]
  %n.addr.011 = phi i32 [ %div, %while.body ], [ %n, %while.body.preheader ]
  %rem1 = srem i32 %n.addr.011, 8
  %div = sdiv i32 %n.addr.011, 8
  %mul = mul nsw i32 %rem1, %i.012
  %add = add nsw i32 %mul, %octal.013
  %mul2 = mul nsw i32 %i.012, 10
  %n.addr.011.off = add i32 %n.addr.011, 7
  %0 = icmp ult i32 %n.addr.011.off, 15
  br i1 %0, label %while.end.loopexit, label %while.body

while.end.loopexit:                               ; preds = %while.body
  %add.lcssa = phi i32 [ %add, %while.body ]
  br label %while.end

while.end:                                        ; preds = %while.end.loopexit, %entry
  %octal.0.lcssa = phi i32 [ 0, %entry ], [ %add.lcssa, %while.end.loopexit ]
  ret i32 %octal.0.lcssa
}

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare double @pow(double, double) #2

; Function Attrs: nounwind
declare i32 @puts(i8* nocapture readonly) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readnone uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"omnipotent char", !3, i64 0}
!3 = !{!"Simple C/C++ TBAA"}
!4 = !{!5, !5, i64 0}
!5 = !{!"int", !2, i64 0}
