; ModuleID = 'test10_O.bc'

@.str = private constant [20 x i8] c"Enter an alphabet: \00", align 1
@.str.1 = private constant [3 x i8] c"%c\00", align 1
@.str.2 = private constant [15 x i8] c"%c is a vowel.\00", align 1
@.str.3 = private constant [19 x i8] c"%c is a consonant.\00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_51 = alloca i32
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %_temp_ehpit0_11 = alloca i32, align 4
  %c_4 = alloca i8, align 1
  %isLowercaseVowel_5 = alloca i32, align 4
  %isUppercaseVowel_6 = alloca i32, align 4
  %old_frame_pointer_16.addr = alloca i64, align 8
  %return_address_17.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i8* %c_4 to i8*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i8* %2)
  %3 = load i8, i8* %c_4, align 1
  %conv3 = sext i8 %3 to i32
  store i32 %conv3, i32* %.preg_I4_4_51, align 8
  %4 = load i32, i32* %.preg_I4_4_51
  %cmp1 = icmp eq i32 %4, 65
  br i1 %cmp1, label %L13570, label %fb

L13570:                                           ; preds = %entry
  br label %L8450

fb:                                               ; preds = %entry
  %5 = load i32, i32* %.preg_I4_4_51
  %cmp2 = icmp eq i32 %5, 69
  br i1 %cmp2, label %L13826, label %fb1

L13826:                                           ; preds = %fb
  br label %L8450

fb1:                                              ; preds = %fb
  %6 = load i32, i32* %.preg_I4_4_51
  %cmp3 = icmp eq i32 %6, 73
  br i1 %cmp3, label %L14082, label %fb2

L14082:                                           ; preds = %fb1
  br label %L8450

fb2:                                              ; preds = %fb1
  %7 = load i32, i32* %.preg_I4_4_51
  %cmp4 = icmp eq i32 %7, 79
  br i1 %cmp4, label %L8450, label %fb3

L8450:                                            ; preds = %tb, %fb2, %L14082, %L13826, %L13570
  br label %L11778

fb3:                                              ; preds = %fb2
  %8 = load i32, i32* %.preg_I4_4_51
  %cmp5 = icmp eq i32 %8, 85
  br i1 %cmp5, label %tb, label %L8194

tb:                                               ; preds = %fb3
  br label %L8450

L8194:                                            ; preds = %fb3
  br label %L12034

L11778:                                           ; preds = %L8450
  %9 = load i32, i32* %.preg_I4_4_51
  %cmp6 = icmp eq i32 %9, 97
  br i1 %cmp6, label %L14594, label %fb4

L12034:                                           ; preds = %L8194
  %10 = load i32, i32* %.preg_I4_4_51
  %cmp11 = icmp eq i32 %10, 97
  br i1 %cmp11, label %L15618, label %fb9

L14594:                                           ; preds = %L11778
  br label %L9986

fb4:                                              ; preds = %L11778
  %11 = load i32, i32* %.preg_I4_4_51
  %cmp7 = icmp eq i32 %11, 101
  br i1 %cmp7, label %L14850, label %fb5

L14850:                                           ; preds = %fb4
  br label %L9986

fb5:                                              ; preds = %fb4
  %12 = load i32, i32* %.preg_I4_4_51
  %cmp8 = icmp eq i32 %12, 105
  br i1 %cmp8, label %L15106, label %fb6

L15106:                                           ; preds = %fb5
  br label %L9986

fb6:                                              ; preds = %fb5
  %13 = load i32, i32* %.preg_I4_4_51
  %cmp9 = icmp eq i32 %13, 111
  br i1 %cmp9, label %L15362, label %fb7

L15362:                                           ; preds = %fb6
  br label %L9986

fb7:                                              ; preds = %fb6
  %14 = load i32, i32* %.preg_I4_4_51
  %cmp10 = icmp eq i32 %14, 117
  br i1 %cmp10, label %tb8, label %L12290

tb8:                                              ; preds = %fb7
  br label %L9986

L12290:                                           ; preds = %fb7
  br label %L11522

L9986:                                            ; preds = %tb13, %fb11, %L16130, %L15874, %L15618, %tb8, %L15362, %L15106, %L14850, %L14594
  br label %L11522

L15618:                                           ; preds = %L12034
  br label %L9986

L15874:                                           ; preds = %fb9
  br label %L9986

L16130:                                           ; preds = %fb10
  br label %L9986

L11522:                                           ; preds = %L9986, %L12290
  %15 = bitcast i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i32 0, i32 0) to i8*
  %16 = load i32, i32* %.preg_I4_4_51
  %call3 = call i32 (i8*, ...) @printf(i8* %15, i32 %16)
  br label %L7426

fb9:                                              ; preds = %L12034
  %17 = load i32, i32* %.preg_I4_4_51
  %cmp12 = icmp eq i32 %17, 101
  br i1 %cmp12, label %L15874, label %fb10

fb10:                                             ; preds = %fb9
  %18 = load i32, i32* %.preg_I4_4_51
  %cmp13 = icmp eq i32 %18, 105
  br i1 %cmp13, label %L16130, label %fb11

fb11:                                             ; preds = %fb10
  %19 = load i32, i32* %.preg_I4_4_51
  %cmp14 = icmp eq i32 %19, 111
  br i1 %cmp14, label %L9986, label %fb12

fb12:                                             ; preds = %fb11
  %20 = load i32, i32* %.preg_I4_4_51
  %cmp15 = icmp eq i32 %20, 117
  br i1 %cmp15, label %tb13, label %L12546

tb13:                                             ; preds = %fb12
  br label %L9986

L12546:                                           ; preds = %fb12
  br label %L7682

L7682:                                            ; preds = %L12546
  %21 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.3, i32 0, i32 0) to i8*
  %22 = load i32, i32* %.preg_I4_4_51
  %call4 = call i32 (i8*, ...) @printf(i8* %21, i32 %22)
  br label %L7426

L7426:                                            ; preds = %L7682, %L11522
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
