; ModuleID = 'test339.bc'

@.str = private constant [7 x i8] c"test1\0A\00", align 1
@.str.1 = private constant [6 x i8] c"hello\00", align 1
@.str.2 = private constant [4 x i8] c"%s\0A\00", align 1
@.str.3 = private constant [7 x i8] c"test2\0A\00", align 1
@.str.4 = private constant [4 x i8] c"%d\0A\00", align 1
@.str.5 = private constant [7 x i8] c"test3\0A\00", align 1
@.str.6 = private constant [7 x i8] c"test4\0A\00", align 1
@.str.7 = private constant [4 x i8] c"%c\0A\00", align 1
@.str.8 = private constant [4 x i8] c"%f\0A\00", align 1
@_LIB_VERSION_64 = common global i32 0, align 4
@g_c_62 = common global i8 0, align 1
@signgam_65 = common global i32 0, align 4
@array.init = private constant [3 x i32] [i32 1, i32 2, i32 3], align 4
@array.init.9 = private constant [3 x i8] c"abc", align 4
@array.init.10 = private constant [3 x float] [float 1.000000e+00, float 2.000000e+00, float 3.000000e+00], align 4
@array.init.11 = private constant [3 x double] [double 1.000000e+00, double 2.000000e+00, double 3.000000e+00], align 4

define i32 @main() {
entry:
  %_temp__casttmp1_8 = alloca i8*, align 1
  %_temp_dummy10_4 = alloca i32, align 4
  %_temp_dummy110_23 = alloca i32, align 4
  %_temp_dummy111_26 = alloca i32, align 4
  %_temp_dummy112_29 = alloca i32, align 4
  %_temp_dummy113_30 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %_temp_dummy14_12 = alloca i32, align 4
  %_temp_dummy15_14 = alloca i32, align 4
  %_temp_dummy16_15 = alloca i32, align 4
  %_temp_dummy17_16 = alloca i32, align 4
  %_temp_dummy18_19 = alloca i32, align 4
  %_temp_dummy19_20 = alloca i32, align 4
  %a.init_18 = alloca [3 x i32], align 4
  %a_17 = alloca [3 x i32], align 4
  %ap_5 = alloca i8*, align 1
  %b_6 = alloca [10 x i8], align 1
  %c.init_22 = alloca [3 x i8], align 1
  %c_21 = alloca [3 x i8], align 1
  %cp_7 = alloca i8*, align 1
  %d.init_28 = alloca [3 x double], align 8
  %d_27 = alloca [3 x double], align 8
  %f.init_25 = alloca [3 x float], align 4
  %f_24 = alloca [3 x float], align 4
  %n_11 = alloca i32, align 4
  %n_13 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  store i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0), i8** %ap_5, align 8
  %arrayAddress = getelementptr [10 x i8], [10 x i8]* %b_6, i32 0, i32 0
  %1 = bitcast i8* %arrayAddress to i8*
  %2 = load i8*, i8** %ap_5, align 8
  %3 = bitcast i8* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %1, i8* %3, i64 6, i32 8, i1 false)
  %4 = bitcast i8* %1 to i8*
  store i8* %4, i8** %_temp__casttmp1_8, align 8
  %5 = load i8*, i8** %_temp__casttmp1_8, align 8
  store i8* %5, i8** %cp_7, align 8
  %6 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.2, i32 0, i32 0) to i8*
  %7 = load i8*, i8** %cp_7, align 8
  %8 = bitcast i8* %7 to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %6, i8* %8)
  %9 = bitcast i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.3, i32 0, i32 0) to i8*
  %call4 = call i32 (i8*, ...) @printf(i8* %9)
  store i32 2, i32* %n_11, align 4
  %10 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.4, i32 0, i32 0) to i8*
  %11 = load i32, i32* %n_11, align 4
  %call5 = call i32 (i8*, ...) @printf(i8* %10, i32 %11)
  store i32 33, i32* %n_13, align 4
  %12 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.4, i32 0, i32 0) to i8*
  %13 = load i32, i32* %n_13, align 4
  %call6 = call i32 (i8*, ...) @printf(i8* %12, i32 %13)
  %14 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.4, i32 0, i32 0) to i8*
  %15 = load i32, i32* %n_11, align 4
  %call7 = call i32 (i8*, ...) @printf(i8* %14, i32 %15)
  %16 = bitcast i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.5, i32 0, i32 0) to i8*
  %call8 = call i32 (i8*, ...) @printf(i8* %16)
  %17 = bitcast [3 x i32]* %a_17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %17, i8* bitcast ([3 x i32]* @array.init to i8*), i64 12, i32 4, i1 false)
  %18 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.4, i32 0, i32 0) to i8*
  %arrayidx = getelementptr [3 x i32], [3 x i32]* %a_17, i32 0, i32 1
  %19 = load i32, i32* %arrayidx, align 4
  %call9 = call i32 (i8*, ...) @printf(i8* %18, i32 %19)
  %20 = bitcast i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.6, i32 0, i32 0) to i8*
  %call10 = call i32 (i8*, ...) @printf(i8* %20)
  %21 = bitcast [3 x i8]* %c_21 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %21, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @array.init.9, i32 0, i32 0), i64 3, i32 1, i1 false)
  %22 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.7, i32 0, i32 0) to i8*
  %arrayidx1 = getelementptr [3 x i8], [3 x i8]* %c_21, i32 0, i32 1
  %23 = load i8, i8* %arrayidx1, align 1
  %conv3 = sext i8 %23 to i32
  %call11 = call i32 (i8*, ...) @printf(i8* %22, i32 %conv3)
  %24 = bitcast [3 x float]* %f_24 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %24, i8* bitcast ([3 x float]* @array.init.10 to i8*), i64 12, i32 4, i1 false)
  %25 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.8, i32 0, i32 0) to i8*
  %arrayidx2 = getelementptr [3 x float], [3 x float]* %f_24, i32 0, i32 1
  %26 = load float, float* %arrayidx2, align 4
  %convDouble = fpext float %26 to double
  %call12 = call i32 (i8*, ...) @printf(i8* %25, double %convDouble)
  %27 = bitcast [3 x double]* %d_27 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %27, i8* bitcast ([3 x double]* @array.init.11 to i8*), i64 24, i32 8, i1 false)
  %28 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.8, i32 0, i32 0) to i8*
  %arrayidx3 = getelementptr [3 x double], [3 x double]* %d_27, i32 0, i32 1
  %29 = load double, double* %arrayidx3, align 8
  %call13 = call i32 (i8*, ...) @printf(i8* %28, double %29)
  %30 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.8, i32 0, i32 0) to i8*
  %arrayidx4 = getelementptr [3 x double], [3 x double]* %d_27, i32 0, i32 1
  %31 = load double, double* %arrayidx4, align 8
  %call14 = call double @_Z2f1d(double %31)
  %call15 = call i32 (i8*, ...) @printf(i8* %30, double %call14)
  ret i32 0
}

declare i32 @printf(i8*, ...)

define double @_Z2f1d(double %i_4) {
entry:
  %_temp___save_sqrt15_6 = alloca double, align 8
  %_temp___sqrt_arg14_5 = alloca double, align 8
  %i_4.addr = alloca double, align 8
  store double %i_4, double* %i_4.addr, align 8
  %0 = load double, double* %i_4.addr, align 8
  store double %0, double* %_temp___sqrt_arg14_5, align 8
  %1 = load double, double* %_temp___sqrt_arg14_5, align 8
  %call7 = call double @sqrt(double %1)
  store double %call7, double* %_temp___save_sqrt15_6, align 8
  %2 = load double, double* %_temp___save_sqrt15_6, align 8
  %3 = load double, double* %_temp___save_sqrt15_6, align 8
  %cmp1 = fcmp one double %2, %3
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load double, double* %_temp___sqrt_arg14_5, align 8
  %call16 = call double @sqrt(double %4)
  store double %call16, double* %_temp___save_sqrt15_6, align 8
  br label %if.end

if.else:                                          ; preds = %entry
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %5 = load double, double* %_temp___save_sqrt15_6, align 8
  ret double %5
}

declare double @sqrt(double)

; Function Attrs: nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture, i8* nocapture readonly, i64, i32, i1) #0

attributes #0 = { nounwind }
