; ModuleID = 'test34.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [30 x i8] c"Enter two positive integers: \00", align 1
@.str.1 = private unnamed_addr constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private unnamed_addr constant [41 x i8] c"Prime numberbers between %d and %d are: \00", align 1
@.str.3 = private unnamed_addr constant [4 x i8] c"%d \00", align 1

; Function Attrs: nounwind readnone uwtable
define i32 @checkPrimeNumber(i32 %n) #0 {
entry:
  %div = sdiv i32 %n, 2
  %cmp.7 = icmp slt i32 %n, 4
  br i1 %cmp.7, label %for.end, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  br label %for.body

for.cond:                                         ; preds = %for.body
  %inc = add nuw nsw i32 %j.08, 1
  %cmp = icmp slt i32 %j.08, %div
  br i1 %cmp, label %for.body, label %for.end.loopexit

for.body:                                         ; preds = %for.body.preheader, %for.cond
  %j.08 = phi i32 [ %inc, %for.cond ], [ 2, %for.body.preheader ]
  %rem = srem i32 %n, %j.08
  %cmp1 = icmp eq i32 %rem, 0
  br i1 %cmp1, label %for.end.loopexit, label %for.cond

for.end.loopexit:                                 ; preds = %for.body, %for.cond
  %flag.0.ph = phi i32 [ 1, %for.cond ], [ 0, %for.body ]
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  %flag.0 = phi i32 [ 1, %entry ], [ %flag.0.ph, %for.end.loopexit ]
  ret i32 %flag.0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

; Function Attrs: nounwind uwtable
define i32 @main() #2 {
entry:
  %n1 = alloca i32, align 4
  %n2 = alloca i32, align 4
  %0 = bitcast i32* %n1 to i8*
  call void @llvm.lifetime.start(i64 4, i8* %0) #1
  %1 = bitcast i32* %n2 to i8*
  call void @llvm.lifetime.start(i64 4, i8* %1) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %n1, i32* nonnull %n2) #1
  %2 = load i32, i32* %n1, align 4, !tbaa !1
  %3 = load i32, i32* %n2, align 4, !tbaa !1
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.2, i64 0, i64 0), i32 %2, i32 %3) #1
  %4 = load i32, i32* %n1, align 4, !tbaa !1
  %i.0.14 = add nsw i32 %4, 1
  %5 = load i32, i32* %n2, align 4, !tbaa !1
  %cmp.15 = icmp slt i32 %i.0.14, %5
  br i1 %cmp.15, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.cond.backedge
  %6 = phi i32 [ %7, %for.cond.backedge ], [ %5, %for.body.preheader ]
  %i.016 = phi i32 [ %i.0, %for.cond.backedge ], [ %i.0.14, %for.body.preheader ]
  %div.i = sdiv i32 %i.016, 2
  %cmp.7.i = icmp slt i32 %i.016, 4
  br i1 %cmp.7.i, label %if.then, label %for.body.i.preheader

for.body.i.preheader:                             ; preds = %for.body
  br label %for.body.i

for.cond.i:                                       ; preds = %for.body.i
  %inc.i = add nuw nsw i32 %j.08.i, 1
  %cmp.i = icmp slt i32 %j.08.i, %div.i
  br i1 %cmp.i, label %for.body.i, label %if.then.loopexit

for.body.i:                                       ; preds = %for.body.i.preheader, %for.cond.i
  %j.08.i = phi i32 [ %inc.i, %for.cond.i ], [ 2, %for.body.i.preheader ]
  %rem.i = srem i32 %i.016, %j.08.i
  %cmp1.i = icmp eq i32 %rem.i, 0
  br i1 %cmp1.i, label %for.cond.backedge.loopexit, label %for.cond.i

if.then.loopexit:                                 ; preds = %for.cond.i
  br label %if.then

if.then:                                          ; preds = %if.then.loopexit, %for.body
  %call5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.3, i64 0, i64 0), i32 %i.016) #1
  %.pre = load i32, i32* %n2, align 4, !tbaa !1
  br label %for.cond.backedge

for.cond.backedge.loopexit:                       ; preds = %for.body.i
  br label %for.cond.backedge

for.cond.backedge:                                ; preds = %for.cond.backedge.loopexit, %if.then
  %7 = phi i32 [ %.pre, %if.then ], [ %6, %for.cond.backedge.loopexit ]
  %i.0 = add nsw i32 %i.016, 1
  %cmp = icmp slt i32 %i.0, %7
  br i1 %cmp, label %for.body, label %for.end.loopexit

for.end.loopexit:                                 ; preds = %for.cond.backedge
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  call void @llvm.lifetime.end(i64 4, i8* %1) #1
  call void @llvm.lifetime.end(i64 4, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #3

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #3

attributes #0 = { nounwind readnone uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
