; ModuleID = 'test34.bc'

@.str = private constant [30 x i8] c"Enter two positive integers: \00", align 1
@.str.1 = private constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private constant [41 x i8] c"Prime numberbers between %d and %d are: \00", align 1
@.str.3 = private constant [4 x i8] c"%d \00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_8 = alloca i32, align 4
  %_temp_dummy11_9 = alloca i32, align 4
  %_temp_dummy12_10 = alloca i32, align 4
  %_temp_dummy13_11 = alloca i32, align 4
  %flag_7 = alloca i32, align 4
  %i_6 = alloca i32, align 4
  %n1_4 = alloca i32, align 4
  %n2_5 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n1_4 to i32*
  %3 = bitcast i32* %n2_5 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2, i32* %3)
  %4 = bitcast i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.2, i32 0, i32 0) to i8*
  %5 = load i32, i32* %n1_4, align 4
  %6 = load i32, i32* %n2_5, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %4, i32 %5, i32 %6)
  %7 = load i32, i32* %n1_4, align 4
  %add = add i32 %7, 1
  store i32 %add, i32* %i_6, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %8 = load i32, i32* %i_6, align 4
  %9 = load i32, i32* %n2_5, align 4
  %cmp1 = icmp slt i32 %8, %9
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %10 = load i32, i32* %i_6, align 4
  %call4 = call i32 @_Z16checkPrimeNumberi(i32 %10)
  store i32 %call4, i32* %flag_7, align 4
  %11 = load i32, i32* %flag_7, align 4
  %cmp2 = icmp eq i32 %11, 1
  br i1 %cmp2, label %if.then, label %if.else

while.end:                                        ; preds = %while.cond
  ret i32 0

if.then:                                          ; preds = %while.body
  %12 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.3, i32 0, i32 0) to i8*
  %13 = load i32, i32* %i_6, align 4
  %call5 = call i32 (i8*, ...) @printf(i8* %12, i32 %13)
  br label %if.end

if.else:                                          ; preds = %while.body
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %14 = load i32, i32* %i_6, align 4
  %add.1 = add i32 %14, 1
  store i32 %add.1, i32* %i_6, align 4
  br label %while.cond
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define i32 @_Z16checkPrimeNumberi(i32 %n_4) {
entry:
  %flag_6 = alloca i32, align 4
  %j_5 = alloca i32, align 4
  %n_4.addr = alloca i32, align 4
  store i32 %n_4, i32* %n_4.addr, align 4
  store i32 1, i32* %flag_6, align 4
  store i32 2, i32* %j_5, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %0 = load i32, i32* %n_4.addr, align 4
  %div = sdiv i32 %0, 2
  %1 = load i32, i32* %j_5, align 4
  %cmp3 = icmp sge i32 %div, %1
  br i1 %cmp3, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load i32, i32* %n_4.addr, align 4
  %3 = load i32, i32* %j_5, align 4
  %srem = srem i32 %2, %3
  %cmp4 = icmp eq i32 %srem, 0
  br i1 %cmp4, label %if.then, label %if.else

while.end:                                        ; preds = %if.then, %while.cond
  br label %LABEL_258

if.then:                                          ; preds = %while.body
  store i32 0, i32* %flag_6, align 4
  br label %while.end

if.else:                                          ; preds = %while.body
  br label %if.end

if.end:                                           ; preds = %if.else
  %4 = load i32, i32* %j_5, align 4
  %add = add i32 %4, 1
  store i32 %add, i32* %j_5, align 4
  br label %while.cond

LABEL_258:                                        ; preds = %while.end
  %5 = load i32, i32* %flag_6, align 4
  ret i32 %5
}
