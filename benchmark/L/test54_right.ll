; ModuleID = 'test54.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [43 x i8] c"Enter total number of elements(1 to 100): \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.2 = private unnamed_addr constant [31 x i8] c"Error!!! memory not allocated.\00", align 1
@.str.3 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.4 = private unnamed_addr constant [18 x i8] c"Enter Number %d: \00", align 1
@.str.5 = private unnamed_addr constant [23 x i8] c"Largest element = %.2f\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %i = alloca i32, align 4
  %num = alloca i32, align 4
  %data = alloca float*, align 8
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([43 x i8], [43 x i8]* @.str, i32 0, i32 0))
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0), i32* %num)
  %0 = load i32, i32* %num, align 4
  %conv = sext i32 %0 to i64
  %call2 = call noalias i8* @calloc(i64 %conv, i64 4) #4
  %1 = bitcast i8* %call2 to float*
  store float* %1, float** %data, align 8
  %2 = load float*, float** %data, align 8
  %cmp = icmp eq float* %2, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.2, i32 0, i32 0))
  call void @exit(i32 0) #5
  unreachable

if.end:                                           ; preds = %entry
  %call5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.3, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num, align 4
  %cmp6 = icmp slt i32 %3, %4
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load i32, i32* %i, align 4
  %add = add nsw i32 %5, 1
  %call8 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.4, i32 0, i32 0), i32 %add)
  %6 = load i32, i32* %i, align 4
  %conv9 = sitofp i32 %6 to float
  %7 = load float*, float** %data, align 8
  %8 = load i32, i32* %i, align 4
  %idx.ext = sext i32 %8 to i64
  %add.ptr = getelementptr inbounds float, float* %7, i64 %idx.ext
  store float %conv9, float* %add.ptr, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 1, i32* %i, align 4
  br label %for.cond.10

for.cond.10:                                      ; preds = %for.inc.22, %for.end
  %10 = load i32, i32* %i, align 4
  %11 = load i32, i32* %num, align 4
  %cmp11 = icmp slt i32 %10, %11
  br i1 %cmp11, label %for.body.13, label %for.end.24

for.body.13:                                      ; preds = %for.cond.10
  %12 = load float*, float** %data, align 8
  %13 = load float, float* %12, align 4
  %14 = load float*, float** %data, align 8
  %15 = load i32, i32* %i, align 4
  %idx.ext14 = sext i32 %15 to i64
  %add.ptr15 = getelementptr inbounds float, float* %14, i64 %idx.ext14
  %16 = load float, float* %add.ptr15, align 4
  %cmp16 = fcmp olt float %13, %16
  br i1 %cmp16, label %if.then.18, label %if.end.21

if.then.18:                                       ; preds = %for.body.13
  %17 = load float*, float** %data, align 8
  %18 = load i32, i32* %i, align 4
  %idx.ext19 = sext i32 %18 to i64
  %add.ptr20 = getelementptr inbounds float, float* %17, i64 %idx.ext19
  %19 = load float, float* %add.ptr20, align 4
  %20 = load float*, float** %data, align 8
  store float %19, float* %20, align 4
  br label %if.end.21

if.end.21:                                        ; preds = %if.then.18, %for.body.13
  br label %for.inc.22

for.inc.22:                                       ; preds = %if.end.21
  %21 = load i32, i32* %i, align 4
  %inc23 = add nsw i32 %21, 1
  store i32 %inc23, i32* %i, align 4
  br label %for.cond.10

for.end.24:                                       ; preds = %for.cond.10
  %22 = load float*, float** %data, align 8
  %23 = load float, float* %22, align 4
  %conv25 = fpext float %23 to double
  %call26 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.5, i32 0, i32 0), double %conv25)
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @__isoc99_scanf(i8*, ...) #1

; Function Attrs: nounwind
declare noalias i8* @calloc(i64, i64) #2

; Function Attrs: noreturn nounwind
declare void @exit(i32) #3

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }
attributes #5 = { noreturn nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
