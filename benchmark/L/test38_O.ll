; ModuleID = 'test38_O.bc'

%struct._temp_dummy13 = type {}

@.str = private constant [27 x i8] c"Enter a positive integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [22 x i8] c"Factorial of %d = %ld\00", align 1

define i32 @main() {
entry:
  %.preg_I8_5_49 = alloca i64
  %.preg_I8_5_50 = alloca i64
  %.preg_I4_4_53 = alloca i32
  %.preg_I4_4_55 = alloca i32
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_dummy11_6 = alloca i32, align 4
  %_temp_dummy12_7 = alloca i32, align 4
  %_temp_dummy13_12 = alloca i32, align 4
  %_temp_dummy13_14 = alloca %struct._temp_dummy13, align 4
  %_temp_ehpit0_15 = alloca i32, align 4
  %n_11.addr = alloca i32, align 4
  %n_13 = alloca i32, align 4
  %n_4 = alloca i32, align 4
  %old_frame_pointer_20.addr = alloca i64, align 8
  %return_address_21.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = load i32, i32* %n_4, align 4
  store i32 %3, i32* %.preg_I4_4_55, align 8
  %4 = load i32, i32* %.preg_I4_4_55
  store i32 %4, i32* %.preg_I4_4_53, align 8
  %5 = load i32, i32* %.preg_I4_4_53
  %cmp1 = icmp sgt i32 %5, 0
  br i1 %cmp1, label %tb, label %L1794

tb:                                               ; preds = %entry
  %6 = load i32, i32* %.preg_I4_4_53
  %add = add i32 %6, -1
  %call3 = call i64 @_Z15multiplyNumbersi(i32 %add)
  store i64 %call3, i64* %.preg_I8_5_50, align 8
  %7 = load i32, i32* %.preg_I4_4_53
  %conv6 = sext i32 %7 to i64
  %8 = load i64, i64* %.preg_I8_5_50
  %mul = mul i64 %conv6, %8
  store i64 %mul, i64* %.preg_I8_5_49, align 8
  %9 = load i32, i32* %n_4, align 4
  store i32 %9, i32* %.preg_I4_4_55, align 8
  br label %L258

L1794:                                            ; preds = %entry
  %conv3 = zext i32 1 to i64
  store i64 %conv3, i64* %.preg_I8_5_49, align 8
  br label %L258

L258:                                             ; preds = %L1794, %tb
  %10 = bitcast i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.2, i32 0, i32 0) to i8*
  %11 = load i32, i32* %.preg_I4_4_55
  %12 = load i64, i64* %.preg_I8_5_49
  %call4 = call i32 (i8*, ...) @printf(i8* %10, i32 %11, i64 %12)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define i64 @_Z15multiplyNumbersi(i32 %n_4) {
entry:
  %.preg_I8_5_49 = alloca i64
  %.preg_I4_4_50 = alloca i32
  %_temp_dummy13_5 = alloca i32, align 4
  %_temp_ehpit1_6 = alloca i32, align 4
  %n_4.addr = alloca i32, align 4
  %old_frame_pointer_11.addr = alloca i64, align 8
  %return_address_12.addr = alloca i64, align 8
  store i32 %n_4, i32* %n_4.addr, align 4
  store i32 %n_4, i32* %.preg_I4_4_50, align 8
  %0 = load i32, i32* %.preg_I4_4_50
  %cmp2 = icmp sgt i32 %0, 0
  br i1 %cmp2, label %tb, label %L770

tb:                                               ; preds = %entry
  %1 = load i32, i32* %.preg_I4_4_50
  %add = add i32 %1, -1
  %call5 = call i64 @_Z15multiplyNumbersi(i32 %add)
  store i64 %call5, i64* %.preg_I8_5_49, align 8
  %2 = load i32, i32* %.preg_I4_4_50
  %conv6 = sext i32 %2 to i64
  %3 = load i64, i64* %.preg_I8_5_49
  %mul = mul i64 %conv6, %3
  ret i64 %mul

L770:                                             ; preds = %entry
  %conv3 = zext i32 1 to i64
  ret i64 %conv3
}
