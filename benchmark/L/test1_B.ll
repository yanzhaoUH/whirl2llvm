; ModuleID = 'test1.bc'

@.str = private constant [19 x i8] c"Enter an integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [16 x i8] c"You entered: %d\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_dummy11_6 = alloca i32, align 4
  %_temp_dummy12_7 = alloca i32, align 4
  %number_4 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %number_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = bitcast i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.2, i32 0, i32 0) to i8*
  %4 = load i32, i32* %number_4, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %3, i32 %4)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
