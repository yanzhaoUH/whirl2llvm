; ModuleID = 'test48.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [43 x i8] c"Enter number of rows (between 1 and 100): \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.2 = private unnamed_addr constant [46 x i8] c"Enter number of columns (between 1 and 100): \00", align 1
@.str.4 = private unnamed_addr constant [22 x i8] c"Enter element a%d%d: \00", align 1
@.str.7 = private unnamed_addr constant [6 x i8] c"%d   \00", align 1
@str = private unnamed_addr constant [31 x i8] c"\0AEnter elements of 1st matrix:\00"
@str.9 = private unnamed_addr constant [30 x i8] c"Enter elements of 2nd matrix:\00"
@str.10 = private unnamed_addr constant [25 x i8] c"\0ASum of two matrix is: \0A\00"
@str.11 = private unnamed_addr constant [2 x i8] c"\0A\00"

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %a = alloca [100 x [100 x i32]], align 16
  %b = alloca [100 x [100 x i32]], align 16
  %sum = alloca [100 x [100 x i32]], align 16
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start(i64 4, i8* %0) #1
  %1 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start(i64 4, i8* %1) #1
  %2 = bitcast [100 x [100 x i32]]* %a to i8*
  call void @llvm.lifetime.start(i64 40000, i8* %2) #1
  %3 = bitcast [100 x [100 x i32]]* %b to i8*
  call void @llvm.lifetime.start(i64 40000, i8* %3) #1
  %4 = bitcast [100 x [100 x i32]]* %sum to i8*
  call void @llvm.lifetime.start(i64 40000, i8* %4) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([43 x i8], [43 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %r) #1
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.2, i64 0, i64 0)) #1
  %call3 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %c) #1
  %puts = call i32 @puts(i8* getelementptr inbounds ([31 x i8], [31 x i8]* @str, i64 0, i64 0))
  %5 = load i32, i32* %r, align 4, !tbaa !1
  %cmp.138 = icmp sgt i32 %5, 0
  br i1 %cmp.138, label %for.cond.5.preheader.lr.ph, label %for.end.16

for.cond.5.preheader.lr.ph:                       ; preds = %entry
  %.pre = load i32, i32* %c, align 4, !tbaa !1
  br label %for.cond.5.preheader

for.cond.5.for.cond.loopexit_crit_edge:           ; preds = %for.body.7
  %.lcssa180 = phi i32 [ %15, %for.body.7 ]
  %.pre156 = load i32, i32* %r, align 4, !tbaa !1
  br label %for.cond.loopexit

for.cond.loopexit:                                ; preds = %for.cond.5.for.cond.loopexit_crit_edge, %for.cond.5.preheader
  %6 = phi i32 [ %.pre156, %for.cond.5.for.cond.loopexit_crit_edge ], [ %9, %for.cond.5.preheader ]
  %7 = phi i32 [ %.lcssa180, %for.cond.5.for.cond.loopexit_crit_edge ], [ %10, %for.cond.5.preheader ]
  %8 = sext i32 %6 to i64
  %cmp = icmp slt i64 %indvars.iv.next151, %8
  br i1 %cmp, label %for.cond.5.preheader, label %for.end.16.loopexit

for.cond.5.preheader:                             ; preds = %for.cond.5.preheader.lr.ph, %for.cond.loopexit
  %9 = phi i32 [ %5, %for.cond.5.preheader.lr.ph ], [ %6, %for.cond.loopexit ]
  %10 = phi i32 [ %.pre, %for.cond.5.preheader.lr.ph ], [ %7, %for.cond.loopexit ]
  %indvars.iv150 = phi i64 [ 0, %for.cond.5.preheader.lr.ph ], [ %indvars.iv.next151, %for.cond.loopexit ]
  %cmp6.135 = icmp sgt i32 %10, 0
  %indvars.iv.next151 = add nuw nsw i64 %indvars.iv150, 1
  br i1 %cmp6.135, label %for.body.7.preheader, label %for.cond.loopexit

for.body.7.preheader:                             ; preds = %for.cond.5.preheader
  %11 = trunc i64 %indvars.iv.next151 to i32
  br label %for.body.7

for.body.7:                                       ; preds = %for.body.7.preheader, %for.body.7
  %indvars.iv147 = phi i64 [ %indvars.iv.next148, %for.body.7 ], [ 0, %for.body.7.preheader ]
  %indvars.iv.next148 = add nuw nsw i64 %indvars.iv147, 1
  %12 = trunc i64 %indvars.iv.next148 to i32
  %call9 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.4, i64 0, i64 0), i32 %11, i32 %12) #1
  %13 = mul nsw i64 %indvars.iv.next148, %indvars.iv.next151
  %arrayidx13 = getelementptr inbounds [100 x [100 x i32]], [100 x [100 x i32]]* %a, i64 0, i64 %indvars.iv150, i64 %indvars.iv147
  %14 = trunc i64 %13 to i32
  store i32 %14, i32* %arrayidx13, align 4, !tbaa !1
  %15 = load i32, i32* %c, align 4, !tbaa !1
  %16 = sext i32 %15 to i64
  %cmp6 = icmp slt i64 %indvars.iv.next148, %16
  br i1 %cmp6, label %for.body.7, label %for.cond.5.for.cond.loopexit_crit_edge

for.end.16.loopexit:                              ; preds = %for.cond.loopexit
  br label %for.end.16

for.end.16:                                       ; preds = %for.end.16.loopexit, %entry
  %puts120 = call i32 @puts(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @str.9, i64 0, i64 0))
  %17 = load i32, i32* %r, align 4, !tbaa !1
  %cmp19.133 = icmp sgt i32 %17, 0
  br i1 %cmp19.133, label %for.cond.21.preheader.lr.ph, label %for.end.64

for.cond.21.preheader.lr.ph:                      ; preds = %for.end.16
  %.pre157 = load i32, i32* %c, align 4, !tbaa !1
  br label %for.cond.21.preheader

for.cond.21.for.cond.18.loopexit_crit_edge:       ; preds = %for.body.23
  %.lcssa178 = phi i32 [ %84, %for.body.23 ]
  %.pre158 = load i32, i32* %r, align 4, !tbaa !1
  br label %for.cond.18.loopexit

for.cond.18.loopexit:                             ; preds = %for.cond.21.for.cond.18.loopexit_crit_edge, %for.cond.21.preheader
  %18 = phi i32 [ %.pre158, %for.cond.21.for.cond.18.loopexit_crit_edge ], [ %21, %for.cond.21.preheader ]
  %19 = phi i32 [ %.lcssa178, %for.cond.21.for.cond.18.loopexit_crit_edge ], [ %22, %for.cond.21.preheader ]
  %20 = sext i32 %18 to i64
  %cmp19 = icmp slt i64 %indvars.iv.next146, %20
  br i1 %cmp19, label %for.cond.21.preheader, label %for.cond.40.preheader

for.cond.21.preheader:                            ; preds = %for.cond.21.preheader.lr.ph, %for.cond.18.loopexit
  %21 = phi i32 [ %17, %for.cond.21.preheader.lr.ph ], [ %18, %for.cond.18.loopexit ]
  %22 = phi i32 [ %.pre157, %for.cond.21.preheader.lr.ph ], [ %19, %for.cond.18.loopexit ]
  %indvars.iv145 = phi i64 [ 0, %for.cond.21.preheader.lr.ph ], [ %indvars.iv.next146, %for.cond.18.loopexit ]
  %cmp22.131 = icmp sgt i32 %22, 0
  %indvars.iv.next146 = add nuw nsw i64 %indvars.iv145, 1
  br i1 %cmp22.131, label %for.body.23.preheader, label %for.cond.18.loopexit

for.body.23.preheader:                            ; preds = %for.cond.21.preheader
  %23 = trunc i64 %indvars.iv.next146 to i32
  br label %for.body.23

for.cond.40.preheader:                            ; preds = %for.cond.18.loopexit
  %.lcssa179 = phi i32 [ %18, %for.cond.18.loopexit ]
  %cmp41.129 = icmp sgt i32 %.lcssa179, 0
  br i1 %cmp41.129, label %for.cond.43.preheader.lr.ph, label %for.end.64

for.cond.43.preheader.lr.ph:                      ; preds = %for.cond.40.preheader
  %24 = load i32, i32* %c, align 4, !tbaa !1
  %cmp44.127 = icmp sgt i32 %24, 0
  br i1 %cmp44.127, label %for.cond.43.preheader.lr.ph.split.us, label %for.end.64

for.cond.43.preheader.lr.ph.split.us:             ; preds = %for.cond.43.preheader.lr.ph
  %25 = sext i32 %.lcssa179 to i64
  %26 = add i32 %24, -1
  %27 = zext i32 %26 to i64
  %28 = add nuw nsw i64 %27, 1
  %29 = zext i32 %26 to i64
  %30 = add nuw nsw i64 %29, 1
  %31 = and i64 %30, 8589934584
  %32 = add nsw i64 %31, -8
  %33 = lshr exact i64 %32, 3
  %end.idx = add nuw nsw i64 %27, 1
  %n.vec = and i64 %28, 8589934584
  %cmp.zero = icmp eq i64 %n.vec, 0
  %34 = and i64 %33, 1
  %lcmp.mod = icmp eq i64 %34, 0
  %35 = icmp eq i64 %33, 0
  br label %overflow.checked

for.inc.62.us.loopexit:                           ; preds = %for.body.45.us
  br label %for.inc.62.us

for.inc.62.us:                                    ; preds = %for.inc.62.us.loopexit, %middle.block
  %indvars.iv.next155 = add nuw nsw i64 %indvars.iv154, 1
  %cmp41.us = icmp slt i64 %indvars.iv.next155, %25
  br i1 %cmp41.us, label %overflow.checked, label %for.end.64.loopexit

for.body.45.us:                                   ; preds = %for.body.45.us.preheader, %for.body.45.us
  %indvars.iv152 = phi i64 [ %indvars.iv.next153, %for.body.45.us ], [ %resume.val, %for.body.45.us.preheader ]
  %arrayidx49.us = getelementptr inbounds [100 x [100 x i32]], [100 x [100 x i32]]* %a, i64 0, i64 %indvars.iv154, i64 %indvars.iv152
  %36 = load i32, i32* %arrayidx49.us, align 4, !tbaa !1
  %arrayidx53.us = getelementptr inbounds [100 x [100 x i32]], [100 x [100 x i32]]* %b, i64 0, i64 %indvars.iv154, i64 %indvars.iv152
  %37 = load i32, i32* %arrayidx53.us, align 4, !tbaa !1
  %add54.us = add nsw i32 %37, %36
  %arrayidx58.us = getelementptr inbounds [100 x [100 x i32]], [100 x [100 x i32]]* %sum, i64 0, i64 %indvars.iv154, i64 %indvars.iv152
  store i32 %add54.us, i32* %arrayidx58.us, align 4, !tbaa !1
  %indvars.iv.next153 = add nuw nsw i64 %indvars.iv152, 1
  %lftr.wideiv = trunc i64 %indvars.iv.next153 to i32
  %exitcond = icmp eq i32 %lftr.wideiv, %24
  br i1 %exitcond, label %for.inc.62.us.loopexit, label %for.body.45.us, !llvm.loop !5

overflow.checked:                                 ; preds = %for.inc.62.us, %for.cond.43.preheader.lr.ph.split.us
  %indvars.iv154 = phi i64 [ %indvars.iv.next155, %for.inc.62.us ], [ 0, %for.cond.43.preheader.lr.ph.split.us ]
  br i1 %cmp.zero, label %middle.block, label %vector.body.preheader

vector.body.preheader:                            ; preds = %overflow.checked
  br i1 %lcmp.mod, label %vector.body.prol, label %vector.body.preheader.split

vector.body.prol:                                 ; preds = %vector.body.preheader
  %38 = getelementptr inbounds [100 x [100 x i32]], [100 x [100 x i32]]* %a, i64 0, i64 %indvars.iv154, i64 0
  %39 = bitcast i32* %38 to <4 x i32>*
  %wide.load.prol = load <4 x i32>, <4 x i32>* %39, align 16, !tbaa !1
  %40 = getelementptr [100 x [100 x i32]], [100 x [100 x i32]]* %a, i64 0, i64 %indvars.iv154, i64 4
  %41 = bitcast i32* %40 to <4 x i32>*
  %wide.load170.prol = load <4 x i32>, <4 x i32>* %41, align 16, !tbaa !1
  %42 = getelementptr inbounds [100 x [100 x i32]], [100 x [100 x i32]]* %b, i64 0, i64 %indvars.iv154, i64 0
  %43 = bitcast i32* %42 to <4 x i32>*
  %wide.load171.prol = load <4 x i32>, <4 x i32>* %43, align 16, !tbaa !1
  %44 = getelementptr [100 x [100 x i32]], [100 x [100 x i32]]* %b, i64 0, i64 %indvars.iv154, i64 4
  %45 = bitcast i32* %44 to <4 x i32>*
  %wide.load172.prol = load <4 x i32>, <4 x i32>* %45, align 16, !tbaa !1
  %46 = add nsw <4 x i32> %wide.load171.prol, %wide.load.prol
  %47 = add nsw <4 x i32> %wide.load172.prol, %wide.load170.prol
  %48 = getelementptr inbounds [100 x [100 x i32]], [100 x [100 x i32]]* %sum, i64 0, i64 %indvars.iv154, i64 0
  %49 = bitcast i32* %48 to <4 x i32>*
  store <4 x i32> %46, <4 x i32>* %49, align 16, !tbaa !1
  %50 = getelementptr [100 x [100 x i32]], [100 x [100 x i32]]* %sum, i64 0, i64 %indvars.iv154, i64 4
  %51 = bitcast i32* %50 to <4 x i32>*
  store <4 x i32> %47, <4 x i32>* %51, align 16, !tbaa !1
  br label %vector.body.preheader.split

vector.body.preheader.split:                      ; preds = %vector.body.prol, %vector.body.preheader
  %index.unr = phi i64 [ 0, %vector.body.preheader ], [ 8, %vector.body.prol ]
  br i1 %35, label %middle.block.loopexit, label %vector.body.preheader.split.split

vector.body.preheader.split.split:                ; preds = %vector.body.preheader.split
  br label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.body.preheader.split.split
  %index = phi i64 [ %index.unr, %vector.body.preheader.split.split ], [ %index.next.1, %vector.body ]
  %52 = getelementptr inbounds [100 x [100 x i32]], [100 x [100 x i32]]* %a, i64 0, i64 %indvars.iv154, i64 %index
  %53 = bitcast i32* %52 to <4 x i32>*
  %wide.load = load <4 x i32>, <4 x i32>* %53, align 16, !tbaa !1
  %54 = getelementptr i32, i32* %52, i64 4
  %55 = bitcast i32* %54 to <4 x i32>*
  %wide.load170 = load <4 x i32>, <4 x i32>* %55, align 16, !tbaa !1
  %56 = getelementptr inbounds [100 x [100 x i32]], [100 x [100 x i32]]* %b, i64 0, i64 %indvars.iv154, i64 %index
  %57 = bitcast i32* %56 to <4 x i32>*
  %wide.load171 = load <4 x i32>, <4 x i32>* %57, align 16, !tbaa !1
  %58 = getelementptr i32, i32* %56, i64 4
  %59 = bitcast i32* %58 to <4 x i32>*
  %wide.load172 = load <4 x i32>, <4 x i32>* %59, align 16, !tbaa !1
  %60 = add nsw <4 x i32> %wide.load171, %wide.load
  %61 = add nsw <4 x i32> %wide.load172, %wide.load170
  %62 = getelementptr inbounds [100 x [100 x i32]], [100 x [100 x i32]]* %sum, i64 0, i64 %indvars.iv154, i64 %index
  %63 = bitcast i32* %62 to <4 x i32>*
  store <4 x i32> %60, <4 x i32>* %63, align 16, !tbaa !1
  %64 = getelementptr i32, i32* %62, i64 4
  %65 = bitcast i32* %64 to <4 x i32>*
  store <4 x i32> %61, <4 x i32>* %65, align 16, !tbaa !1
  %index.next = add i64 %index, 8
  %66 = getelementptr inbounds [100 x [100 x i32]], [100 x [100 x i32]]* %a, i64 0, i64 %indvars.iv154, i64 %index.next
  %67 = bitcast i32* %66 to <4 x i32>*
  %wide.load.1 = load <4 x i32>, <4 x i32>* %67, align 16, !tbaa !1
  %68 = getelementptr i32, i32* %66, i64 4
  %69 = bitcast i32* %68 to <4 x i32>*
  %wide.load170.1 = load <4 x i32>, <4 x i32>* %69, align 16, !tbaa !1
  %70 = getelementptr inbounds [100 x [100 x i32]], [100 x [100 x i32]]* %b, i64 0, i64 %indvars.iv154, i64 %index.next
  %71 = bitcast i32* %70 to <4 x i32>*
  %wide.load171.1 = load <4 x i32>, <4 x i32>* %71, align 16, !tbaa !1
  %72 = getelementptr i32, i32* %70, i64 4
  %73 = bitcast i32* %72 to <4 x i32>*
  %wide.load172.1 = load <4 x i32>, <4 x i32>* %73, align 16, !tbaa !1
  %74 = add nsw <4 x i32> %wide.load171.1, %wide.load.1
  %75 = add nsw <4 x i32> %wide.load172.1, %wide.load170.1
  %76 = getelementptr inbounds [100 x [100 x i32]], [100 x [100 x i32]]* %sum, i64 0, i64 %indvars.iv154, i64 %index.next
  %77 = bitcast i32* %76 to <4 x i32>*
  store <4 x i32> %74, <4 x i32>* %77, align 16, !tbaa !1
  %78 = getelementptr i32, i32* %76, i64 4
  %79 = bitcast i32* %78 to <4 x i32>*
  store <4 x i32> %75, <4 x i32>* %79, align 16, !tbaa !1
  %index.next.1 = add i64 %index, 16
  %80 = icmp eq i64 %index.next.1, %n.vec
  br i1 %80, label %middle.block.loopexit.unr-lcssa, label %vector.body, !llvm.loop !9

middle.block.loopexit.unr-lcssa:                  ; preds = %vector.body
  br label %middle.block.loopexit

middle.block.loopexit:                            ; preds = %vector.body.preheader.split, %middle.block.loopexit.unr-lcssa
  br label %middle.block

middle.block:                                     ; preds = %middle.block.loopexit, %overflow.checked
  %resume.val = phi i64 [ 0, %overflow.checked ], [ %n.vec, %middle.block.loopexit ]
  %cmp.n = icmp eq i64 %end.idx, %resume.val
  br i1 %cmp.n, label %for.inc.62.us, label %for.body.45.us.preheader

for.body.45.us.preheader:                         ; preds = %middle.block
  br label %for.body.45.us

for.body.23:                                      ; preds = %for.body.23.preheader, %for.body.23
  %indvars.iv142 = phi i64 [ %indvars.iv.next143, %for.body.23 ], [ 0, %for.body.23.preheader ]
  %indvars.iv.next143 = add nuw nsw i64 %indvars.iv142, 1
  %81 = trunc i64 %indvars.iv.next143 to i32
  %call26 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.4, i64 0, i64 0), i32 %23, i32 %81) #1
  %82 = mul nsw i64 %indvars.iv.next143, %indvars.iv.next146
  %arrayidx33 = getelementptr inbounds [100 x [100 x i32]], [100 x [100 x i32]]* %b, i64 0, i64 %indvars.iv145, i64 %indvars.iv142
  %83 = trunc i64 %82 to i32
  store i32 %83, i32* %arrayidx33, align 4, !tbaa !1
  %84 = load i32, i32* %c, align 4, !tbaa !1
  %85 = sext i32 %84 to i64
  %cmp22 = icmp slt i64 %indvars.iv.next143, %85
  br i1 %cmp22, label %for.body.23, label %for.cond.21.for.cond.18.loopexit_crit_edge

for.end.64.loopexit:                              ; preds = %for.inc.62.us
  br label %for.end.64

for.end.64:                                       ; preds = %for.end.64.loopexit, %for.end.16, %for.cond.43.preheader.lr.ph, %for.cond.40.preheader
  %puts121 = call i32 @puts(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @str.10, i64 0, i64 0))
  %86 = load i32, i32* %r, align 4, !tbaa !1
  %cmp67.125 = icmp sgt i32 %86, 0
  br i1 %cmp67.125, label %for.cond.69.preheader.lr.ph, label %for.end.84

for.cond.69.preheader.lr.ph:                      ; preds = %for.end.64
  %.pre159 = load i32, i32* %c, align 4, !tbaa !1
  br label %for.cond.69.preheader

for.cond.69.preheader:                            ; preds = %for.cond.69.preheader.lr.ph, %for.inc.82
  %87 = phi i32 [ %86, %for.cond.69.preheader.lr.ph ], [ %94, %for.inc.82 ]
  %88 = phi i32 [ %.pre159, %for.cond.69.preheader.lr.ph ], [ %95, %for.inc.82 ]
  %indvars.iv140 = phi i64 [ 0, %for.cond.69.preheader.lr.ph ], [ %indvars.iv.next141, %for.inc.82 ]
  %cmp70.123 = icmp sgt i32 %88, 0
  br i1 %cmp70.123, label %for.body.71.preheader, label %for.inc.82

for.body.71.preheader:                            ; preds = %for.cond.69.preheader
  br label %for.body.71

for.body.71:                                      ; preds = %for.body.71.preheader, %for.inc.79
  %indvars.iv = phi i64 [ %indvars.iv.next, %for.inc.79 ], [ 0, %for.body.71.preheader ]
  %arrayidx75 = getelementptr inbounds [100 x [100 x i32]], [100 x [100 x i32]]* %sum, i64 0, i64 %indvars.iv140, i64 %indvars.iv
  %89 = load i32, i32* %arrayidx75, align 4, !tbaa !1
  %call76 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.7, i64 0, i64 0), i32 %89) #1
  %90 = load i32, i32* %c, align 4, !tbaa !1
  %sub = add nsw i32 %90, -1
  %91 = trunc i64 %indvars.iv to i32
  %cmp77 = icmp eq i32 %91, %sub
  br i1 %cmp77, label %if.then, label %for.inc.79

if.then:                                          ; preds = %for.body.71
  %puts122 = call i32 @puts(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @str.11, i64 0, i64 0))
  %.pre160 = load i32, i32* %c, align 4, !tbaa !1
  br label %for.inc.79

for.inc.79:                                       ; preds = %for.body.71, %if.then
  %92 = phi i32 [ %90, %for.body.71 ], [ %.pre160, %if.then ]
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %93 = sext i32 %92 to i64
  %cmp70 = icmp slt i64 %indvars.iv.next, %93
  br i1 %cmp70, label %for.body.71, label %for.cond.69.for.inc.82_crit_edge

for.cond.69.for.inc.82_crit_edge:                 ; preds = %for.inc.79
  %.lcssa = phi i32 [ %92, %for.inc.79 ]
  %.pre161 = load i32, i32* %r, align 4, !tbaa !1
  br label %for.inc.82

for.inc.82:                                       ; preds = %for.cond.69.for.inc.82_crit_edge, %for.cond.69.preheader
  %94 = phi i32 [ %.pre161, %for.cond.69.for.inc.82_crit_edge ], [ %87, %for.cond.69.preheader ]
  %95 = phi i32 [ %.lcssa, %for.cond.69.for.inc.82_crit_edge ], [ %88, %for.cond.69.preheader ]
  %indvars.iv.next141 = add nuw nsw i64 %indvars.iv140, 1
  %96 = sext i32 %94 to i64
  %cmp67 = icmp slt i64 %indvars.iv.next141, %96
  br i1 %cmp67, label %for.cond.69.preheader, label %for.end.84.loopexit

for.end.84.loopexit:                              ; preds = %for.inc.82
  br label %for.end.84

for.end.84:                                       ; preds = %for.end.84.loopexit, %for.end.64
  call void @llvm.lifetime.end(i64 40000, i8* %4) #1
  call void @llvm.lifetime.end(i64 40000, i8* %3) #1
  call void @llvm.lifetime.end(i64 40000, i8* %2) #1
  call void @llvm.lifetime.end(i64 4, i8* %1) #1
  call void @llvm.lifetime.end(i64 4, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @puts(i8* nocapture readonly) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = distinct !{!5, !6, !7, !8}
!6 = !{!"llvm.loop.unroll.runtime.disable"}
!7 = !{!"llvm.loop.vectorize.width", i32 1}
!8 = !{!"llvm.loop.interleave.count", i32 1}
!9 = distinct !{!9, !7, !8}
