; ModuleID = 'test15.bc'

@.str = private constant [20 x i8] c"Enter a character: \00", align 1
@.str.1 = private constant [3 x i8] c"%c\00", align 1
@.str.2 = private constant [19 x i8] c"%c is an alphabet.\00", align 1
@.str.3 = private constant [23 x i8] c"%c is not an alphabet.\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_dummy11_6 = alloca i32, align 4
  %_temp_dummy12_7 = alloca i32, align 4
  %_temp_dummy13_8 = alloca i32, align 4
  %c_4 = alloca i8, align 1
  %0 = bitcast i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i8* %c_4 to i8*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i8* %2)
  %3 = load i8, i8* %c_4, align 1
  %conv3 = sext i8 %3 to i32
  %add = sub i32 %conv3, 97
  %conv = trunc i32 %add to i8
  %conv31 = sext i8 %conv to i32
  %cmp1 = icmp ule i32 %conv31, 25
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.2, i32 0, i32 0) to i8*
  %5 = load i8, i8* %c_4, align 1
  %conv32 = sext i8 %5 to i32
  %call3 = call i32 (i8*, ...) @printf(i8* %4, i32 %conv32)
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.3, i32 0, i32 0) to i8*
  %7 = load i8, i8* %c_4, align 1
  %conv33 = sext i8 %7 to i32
  %call4 = call i32 (i8*, ...) @printf(i8* %6, i32 %conv33)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
