; ModuleID = 'test44.bc'

@.str = private constant [20 x i8] c"Enter base number: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [39 x i8] c"Enter power number(positive integer): \00", align 1
@.str.3 = private constant [11 x i8] c"%d^%d = %d\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %_temp_dummy13_9 = alloca i32, align 4
  %_temp_dummy14_10 = alloca i32, align 4
  %base_4 = alloca i32, align 4
  %exp_5 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %base_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = bitcast i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %3)
  %4 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %5 = bitcast i32* %exp_5 to i32*
  %call4 = call i32 (i8*, ...) @scanf(i8* %4, i32* %5)
  %6 = bitcast i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.3, i32 0, i32 0) to i8*
  %7 = load i32, i32* %base_4, align 4
  %8 = load i32, i32* %exp_5, align 4
  %9 = load i32, i32* %base_4, align 4
  %10 = load i32, i32* %exp_5, align 4
  %call5 = call i32 @_Z5powerii(i32 %9, i32 %10)
  %call6 = call i32 (i8*, ...) @printf(i8* %6, i32 %7, i32 %8, i32 %call5)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define i32 @_Z5powerii(i32 %base_4, i32 %exp_5) {
entry:
  %_temp_dummy15_6 = alloca i32, align 4
  %base_4.addr = alloca i32, align 4
  %exp_5.addr = alloca i32, align 4
  store i32 %base_4, i32* %base_4.addr, align 4
  store i32 %exp_5, i32* %exp_5.addr, align 4
  %0 = load i32, i32* %exp_5.addr, align 4
  %cmp1 = icmp ne i32 %0, 1
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %base_4.addr, align 4
  %2 = load i32, i32* %exp_5.addr, align 4
  %add = sub i32 %2, 1
  %call7 = call i32 @_Z5powerii(i32 %1, i32 %add)
  %3 = load i32, i32* %base_4.addr, align 4
  %mul = mul i32 %call7, %3
  ret i32 %mul

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %base_4.addr, align 4
  ret i32 %4
}
