; ModuleID = 'test29.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [30 x i8] c"Enter a three digit integer: \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.2 = private unnamed_addr constant [27 x i8] c"%d is an Armstrong number.\00", align 1
@.str.3 = private unnamed_addr constant [31 x i8] c"%d is not an Armstrong number.\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %number = alloca i32, align 4
  %originalNumber = alloca i32, align 4
  %remainder = alloca i32, align 4
  %result = alloca i32, align 4
  store i32 0, i32* %retval
  store i32 0, i32* %result, align 4
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str, i32 0, i32 0))
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0), i32* %number)
  %0 = load i32, i32* %number, align 4
  store i32 %0, i32* %originalNumber, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32, i32* %originalNumber, align 4
  %cmp = icmp ne i32 %1, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load i32, i32* %originalNumber, align 4
  %rem = srem i32 %2, 10
  store i32 %rem, i32* %remainder, align 4
  %3 = load i32, i32* %remainder, align 4
  %4 = load i32, i32* %remainder, align 4
  %mul = mul nsw i32 %3, %4
  %5 = load i32, i32* %remainder, align 4
  %mul2 = mul nsw i32 %mul, %5
  %6 = load i32, i32* %result, align 4
  %add = add nsw i32 %6, %mul2
  store i32 %add, i32* %result, align 4
  %7 = load i32, i32* %originalNumber, align 4
  %div = sdiv i32 %7, 10
  store i32 %div, i32* %originalNumber, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %8 = load i32, i32* %result, align 4
  %9 = load i32, i32* %number, align 4
  %cmp3 = icmp eq i32 %8, %9
  br i1 %cmp3, label %if.then, label %if.else

if.then:                                          ; preds = %while.end
  %10 = load i32, i32* %number, align 4
  %call4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.2, i32 0, i32 0), i32 %10)
  br label %if.end

if.else:                                          ; preds = %while.end
  %11 = load i32, i32* %number, align 4
  %call5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.3, i32 0, i32 0), i32 %11)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @__isoc99_scanf(i8*, ...) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
