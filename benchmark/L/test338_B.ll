; ModuleID = 'test338.bc'

%struct.str = type { i8, double, [3 x i32], %struct.str_child, i32 }
%struct.str_child = type { i32, double*, i8 }

@.str = private constant [4 x i8] c"%d\0A\00", align 1
@.str.1 = private constant [12 x i8] c"test 1 = %c\00", align 1
@_LIB_VERSION_55 = common global i32 0, align 4
@signgam_56 = common global i32 0, align 4

define i32 @main() {
entry:
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_10 = alloca i32, align 4
  %ccc_9 = alloca i32, align 4
  %dd_5 = alloca double, align 8
  %str1_4 = alloca %struct.str, align 4
  %str_array_8 = alloca [10 x %struct.str], align 8
  %c = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 0
  %fp = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 1
  %array123 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 2
  %sch1 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 3
  %a_child = getelementptr %struct.str_child, %struct.str_child* %sch1, i32 0, i32 0
  %b_child = getelementptr %struct.str_child, %struct.str_child* %sch1, i32 0, i32 1
  %c_child = getelementptr %struct.str_child, %struct.str_child* %sch1, i32 0, i32 2
  %end = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 4
  store i32 2, i32* %end, align 4
  store double 1.000000e+00, double* %dd_5, align 8
  %c1 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 0
  %fp2 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 1
  %array1233 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 2
  %sch14 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 3
  %a_child5 = getelementptr %struct.str_child, %struct.str_child* %sch14, i32 0, i32 0
  %b_child6 = getelementptr %struct.str_child, %struct.str_child* %sch14, i32 0, i32 1
  %c_child7 = getelementptr %struct.str_child, %struct.str_child* %sch14, i32 0, i32 2
  %end8 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 4
  store double* %dd_5, double** %b_child6, align 8
  %c9 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 0
  %fp10 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 1
  %array12311 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 2
  %sch112 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 3
  %a_child13 = getelementptr %struct.str_child, %struct.str_child* %sch112, i32 0, i32 0
  %b_child14 = getelementptr %struct.str_child, %struct.str_child* %sch112, i32 0, i32 1
  %c_child15 = getelementptr %struct.str_child, %struct.str_child* %sch112, i32 0, i32 2
  %end16 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 4
  store i32 33, i32* %a_child13, align 4
  %c17 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 0
  %fp18 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 1
  %array12319 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 2
  %sch120 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 3
  %a_child21 = getelementptr %struct.str_child, %struct.str_child* %sch120, i32 0, i32 0
  %b_child22 = getelementptr %struct.str_child, %struct.str_child* %sch120, i32 0, i32 1
  %c_child23 = getelementptr %struct.str_child, %struct.str_child* %sch120, i32 0, i32 2
  %end24 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 4
  %arrayidx = getelementptr [3 x i32], [3 x i32]* %array12319, i32 0, i64 1
  store i32 66, i32* %arrayidx, align 4
  %0 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0) to i8*
  %c25 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 0
  %fp26 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 1
  %array12327 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 2
  %sch128 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 3
  %a_child29 = getelementptr %struct.str_child, %struct.str_child* %sch128, i32 0, i32 0
  %b_child30 = getelementptr %struct.str_child, %struct.str_child* %sch128, i32 0, i32 1
  %c_child31 = getelementptr %struct.str_child, %struct.str_child* %sch128, i32 0, i32 2
  %end32 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 4
  %1 = load i32, i32* %a_child29, align 4
  %call1 = call i32 (i8*, ...) @printf(i8* %0, i32 %1)
  %2 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0) to i8*
  %c33 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 0
  %fp34 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 1
  %array12335 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 2
  %sch136 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 3
  %a_child37 = getelementptr %struct.str_child, %struct.str_child* %sch136, i32 0, i32 0
  %b_child38 = getelementptr %struct.str_child, %struct.str_child* %sch136, i32 0, i32 1
  %c_child39 = getelementptr %struct.str_child, %struct.str_child* %sch136, i32 0, i32 2
  %end40 = getelementptr %struct.str, %struct.str* %str1_4, i32 0, i32 4
  %arrayidx41 = getelementptr [3 x i32], [3 x i32]* %array12335, i32 0, i64 1
  %3 = load i32, i32* %arrayidx41, align 4
  %call2 = call i32 (i8*, ...) @printf(i8* %2, i32 %3)
  store i32 97, i32* %ccc_9, align 4
  %arrayidx42 = getelementptr [10 x %struct.str], [10 x %struct.str]* %str_array_8, i32 0, i32 1
  %4 = load i32, i32* %ccc_9, align 4
  %conv = trunc i32 %4 to i8
  %c43 = getelementptr %struct.str, %struct.str* %arrayidx42, i32 0, i32 0
  %fp44 = getelementptr %struct.str, %struct.str* %arrayidx42, i32 0, i32 1
  %array12345 = getelementptr %struct.str, %struct.str* %arrayidx42, i32 0, i32 2
  %sch146 = getelementptr %struct.str, %struct.str* %arrayidx42, i32 0, i32 3
  %a_child47 = getelementptr %struct.str_child, %struct.str_child* %sch146, i32 0, i32 0
  %b_child48 = getelementptr %struct.str_child, %struct.str_child* %sch146, i32 0, i32 1
  %c_child49 = getelementptr %struct.str_child, %struct.str_child* %sch146, i32 0, i32 2
  %end50 = getelementptr %struct.str, %struct.str* %arrayidx42, i32 0, i32 4
  store i8 %conv, i8* %c43, align 1
  %arrayidx51 = getelementptr [10 x %struct.str], [10 x %struct.str]* %str_array_8, i32 0, i32 1
  %c52 = getelementptr %struct.str, %struct.str* %arrayidx51, i32 0, i32 0
  %fp53 = getelementptr %struct.str, %struct.str* %arrayidx51, i32 0, i32 1
  %array12354 = getelementptr %struct.str, %struct.str* %arrayidx51, i32 0, i32 2
  %sch155 = getelementptr %struct.str, %struct.str* %arrayidx51, i32 0, i32 3
  %a_child56 = getelementptr %struct.str_child, %struct.str_child* %sch155, i32 0, i32 0
  %b_child57 = getelementptr %struct.str_child, %struct.str_child* %sch155, i32 0, i32 1
  %c_child58 = getelementptr %struct.str_child, %struct.str_child* %sch155, i32 0, i32 2
  %end59 = getelementptr %struct.str, %struct.str* %arrayidx51, i32 0, i32 4
  store double 1.000000e+00, double* %fp53, align 8
  %arrayidx60 = getelementptr [10 x %struct.str], [10 x %struct.str]* %str_array_8, i32 0, i32 1
  %5 = load i32, i32* %ccc_9, align 4
  %conv61 = trunc i32 %5 to i8
  %c62 = getelementptr %struct.str, %struct.str* %arrayidx60, i32 0, i32 0
  %fp63 = getelementptr %struct.str, %struct.str* %arrayidx60, i32 0, i32 1
  %array12364 = getelementptr %struct.str, %struct.str* %arrayidx60, i32 0, i32 2
  %sch165 = getelementptr %struct.str, %struct.str* %arrayidx60, i32 0, i32 3
  %a_child66 = getelementptr %struct.str_child, %struct.str_child* %sch165, i32 0, i32 0
  %b_child67 = getelementptr %struct.str_child, %struct.str_child* %sch165, i32 0, i32 1
  %c_child68 = getelementptr %struct.str_child, %struct.str_child* %sch165, i32 0, i32 2
  %end69 = getelementptr %struct.str, %struct.str* %arrayidx60, i32 0, i32 4
  store i8 %conv61, i8* %c_child68, align 1
  %6 = bitcast i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i32 0, i32 0) to i8*
  %arrayidx70 = getelementptr [10 x %struct.str], [10 x %struct.str]* %str_array_8, i32 0, i32 1
  %c71 = getelementptr %struct.str, %struct.str* %arrayidx70, i32 0, i32 0
  %fp72 = getelementptr %struct.str, %struct.str* %arrayidx70, i32 0, i32 1
  %array12373 = getelementptr %struct.str, %struct.str* %arrayidx70, i32 0, i32 2
  %sch174 = getelementptr %struct.str, %struct.str* %arrayidx70, i32 0, i32 3
  %a_child75 = getelementptr %struct.str_child, %struct.str_child* %sch174, i32 0, i32 0
  %b_child76 = getelementptr %struct.str_child, %struct.str_child* %sch174, i32 0, i32 1
  %c_child77 = getelementptr %struct.str_child, %struct.str_child* %sch174, i32 0, i32 2
  %end78 = getelementptr %struct.str, %struct.str* %arrayidx70, i32 0, i32 4
  %7 = load i8, i8* %c_child77, align 1
  %conv3 = sext i8 %7 to i32
  %call3 = call i32 (i8*, ...) @printf(i8* %6, i32 %conv3)
  ret i32 0
}

declare i32 @printf(i8*, ...)
