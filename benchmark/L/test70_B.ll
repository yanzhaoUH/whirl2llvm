; ModuleID = 'test70.bc'

@.str = private constant [27 x i8] c"Enter the number of rows: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [3 x i8] c"* \00", align 1
@.str.3 = private constant [2 x i8] c"\0A\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %i_4 = alloca i32, align 4
  %j_5 = alloca i32, align 4
  %rows_6 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %rows_6 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  store i32 1, i32* %i_4, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.end3, %entry
  %3 = load i32, i32* %i_4, align 4
  %4 = load i32, i32* %rows_6, align 4
  %cmp1 = icmp sle i32 %3, %4
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  store i32 1, i32* %j_5, align 4
  br label %while.cond1

while.end:                                        ; preds = %while.cond
  ret i32 0

while.cond1:                                      ; preds = %while.body2, %while.body
  %5 = load i32, i32* %j_5, align 4
  %6 = load i32, i32* %i_4, align 4
  %cmp2 = icmp sle i32 %5, %6
  br i1 %cmp2, label %while.body2, label %while.end3

while.body2:                                      ; preds = %while.cond1
  %7 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %7)
  %8 = load i32, i32* %j_5, align 4
  %add = add i32 %8, 1
  store i32 %add, i32* %j_5, align 4
  br label %while.cond1

while.end3:                                       ; preds = %while.cond1
  %9 = bitcast i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.3, i32 0, i32 0) to i8*
  %call4 = call i32 (i8*, ...) @printf(i8* %9)
  %10 = load i32, i32* %i_4, align 4
  %add.4 = add i32 %10, 1
  store i32 %add.4, i32* %i_4, align 4
  br label %while.cond
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
