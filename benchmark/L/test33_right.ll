; ModuleID = 'test33.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [31 x i8] c"Enter an operator (+, -, *,): \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%c\00", align 1
@.str.2 = private unnamed_addr constant [21 x i8] c"Enter two operands: \00", align 1
@.str.3 = private unnamed_addr constant [8 x i8] c"%lf %lf\00", align 1
@.str.4 = private unnamed_addr constant [22 x i8] c"%.1lf + %.1lf = %.1lf\00", align 1
@.str.5 = private unnamed_addr constant [22 x i8] c"%.1lf - %.1lf = %.1lf\00", align 1
@.str.6 = private unnamed_addr constant [22 x i8] c"%.1lf * %.1lf = %.1lf\00", align 1
@.str.7 = private unnamed_addr constant [22 x i8] c"%.1lf / %.1lf = %.1lf\00", align 1
@.str.8 = private unnamed_addr constant [31 x i8] c"Error! operator is not correct\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %op = alloca i8, align 1
  %firstNumber = alloca double, align 8
  %secondNumber = alloca double, align 8
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str, i32 0, i32 0))
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0), i8* %op)
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.2, i32 0, i32 0))
  %call3 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.3, i32 0, i32 0), double* %firstNumber, double* %secondNumber)
  %0 = load i8, i8* %op, align 1
  %conv = sext i8 %0 to i32
  switch i32 %conv, label %sw.default [
    i32 43, label %sw.bb
    i32 45, label %sw.bb.5
    i32 42, label %sw.bb.7
    i32 47, label %sw.bb.9
  ]

sw.bb:                                            ; preds = %entry
  %1 = load double, double* %firstNumber, align 8
  %2 = load double, double* %secondNumber, align 8
  %3 = load double, double* %firstNumber, align 8
  %4 = load double, double* %secondNumber, align 8
  %add = fadd double %3, %4
  %call4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.4, i32 0, i32 0), double %1, double %2, double %add)
  br label %sw.epilog

sw.bb.5:                                          ; preds = %entry
  %5 = load double, double* %firstNumber, align 8
  %6 = load double, double* %secondNumber, align 8
  %7 = load double, double* %firstNumber, align 8
  %8 = load double, double* %secondNumber, align 8
  %sub = fsub double %7, %8
  %call6 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.5, i32 0, i32 0), double %5, double %6, double %sub)
  br label %sw.epilog

sw.bb.7:                                          ; preds = %entry
  %9 = load double, double* %firstNumber, align 8
  %10 = load double, double* %secondNumber, align 8
  %11 = load double, double* %firstNumber, align 8
  %12 = load double, double* %secondNumber, align 8
  %mul = fmul double %11, %12
  %call8 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.6, i32 0, i32 0), double %9, double %10, double %mul)
  br label %sw.epilog

sw.bb.9:                                          ; preds = %entry
  %13 = load double, double* %firstNumber, align 8
  %14 = load double, double* %secondNumber, align 8
  %15 = load double, double* %firstNumber, align 8
  %16 = load double, double* %firstNumber, align 8
  %div = fdiv double %15, %16
  %call10 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.7, i32 0, i32 0), double %13, double %14, double %div)
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %call11 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.8, i32 0, i32 0))
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb.9, %sw.bb.7, %sw.bb.5, %sw.bb
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @__isoc99_scanf(i8*, ...) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
