; ModuleID = 'test17.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [19 x i8] c"Enter an integer: \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.2 = private unnamed_addr constant [53 x i8] c"Error! Factorial of a negative number doesn't exist.\00", align 1
@.str.3 = private unnamed_addr constant [23 x i8] c"Factorial of %d = %llu\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %n = alloca i32, align 4
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start(i64 4, i8* %0) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %n) #1
  %1 = load i32, i32* %n, align 4, !tbaa !1
  %cmp = icmp slt i32 %1, 0
  br i1 %cmp, label %if.then, label %for.cond.preheader

for.cond.preheader:                               ; preds = %entry
  %cmp3.10 = icmp slt i32 %1, 1
  br i1 %cmp3.10, label %for.end, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %for.cond.preheader
  %2 = sext i32 %1 to i64
  %3 = sext i32 %1 to i64
  %4 = add nsw i64 %3, -1
  %xtraiter = and i64 %3, 7
  %lcmp.mod = icmp eq i64 %xtraiter, 0
  br i1 %lcmp.mod, label %for.body.lr.ph.split, label %for.body.prol.preheader

for.body.prol.preheader:                          ; preds = %for.body.lr.ph
  br label %for.body.prol

for.body.prol:                                    ; preds = %for.body.prol.preheader, %for.body.prol
  %indvars.iv.prol = phi i64 [ %indvars.iv.next.prol, %for.body.prol ], [ 1, %for.body.prol.preheader ]
  %factorial.012.prol = phi i64 [ %mul.prol, %for.body.prol ], [ 1, %for.body.prol.preheader ]
  %prol.iter = phi i64 [ %prol.iter.sub, %for.body.prol ], [ %xtraiter, %for.body.prol.preheader ]
  %mul.prol = mul i64 %factorial.012.prol, %indvars.iv.prol
  %indvars.iv.next.prol = add nuw nsw i64 %indvars.iv.prol, 1
  %prol.iter.sub = add i64 %prol.iter, -1
  %prol.iter.cmp = icmp eq i64 %prol.iter.sub, 0
  br i1 %prol.iter.cmp, label %for.body.lr.ph.split.loopexit, label %for.body.prol, !llvm.loop !5

for.body.lr.ph.split.loopexit:                    ; preds = %for.body.prol
  %indvars.iv.next.prol.lcssa = phi i64 [ %indvars.iv.next.prol, %for.body.prol ]
  %mul.prol.lcssa = phi i64 [ %mul.prol, %for.body.prol ]
  br label %for.body.lr.ph.split

for.body.lr.ph.split:                             ; preds = %for.body.lr.ph.split.loopexit, %for.body.lr.ph
  %indvars.iv.unr = phi i64 [ 1, %for.body.lr.ph ], [ %indvars.iv.next.prol.lcssa, %for.body.lr.ph.split.loopexit ]
  %factorial.012.unr = phi i64 [ 1, %for.body.lr.ph ], [ %mul.prol.lcssa, %for.body.lr.ph.split.loopexit ]
  %mul.lcssa.unr = phi i64 [ undef, %for.body.lr.ph ], [ %mul.prol.lcssa, %for.body.lr.ph.split.loopexit ]
  %5 = icmp ult i64 %4, 7
  br i1 %5, label %for.end.loopexit, label %for.body.lr.ph.split.split

for.body.lr.ph.split.split:                       ; preds = %for.body.lr.ph.split
  br label %for.body

if.then:                                          ; preds = %entry
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.2, i64 0, i64 0)) #1
  br label %if.end

for.body:                                         ; preds = %for.body, %for.body.lr.ph.split.split
  %indvars.iv = phi i64 [ %indvars.iv.unr, %for.body.lr.ph.split.split ], [ %indvars.iv.next.7, %for.body ]
  %factorial.012 = phi i64 [ %factorial.012.unr, %for.body.lr.ph.split.split ], [ %mul.7, %for.body ]
  %mul = mul i64 %factorial.012, %indvars.iv
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %mul.1 = mul i64 %mul, %indvars.iv.next
  %indvars.iv.next.1 = add nsw i64 %indvars.iv, 2
  %mul.2 = mul i64 %mul.1, %indvars.iv.next.1
  %indvars.iv.next.2 = add nsw i64 %indvars.iv, 3
  %mul.3 = mul i64 %mul.2, %indvars.iv.next.2
  %indvars.iv.next.3 = add nsw i64 %indvars.iv, 4
  %mul.4 = mul i64 %mul.3, %indvars.iv.next.3
  %indvars.iv.next.4 = add nsw i64 %indvars.iv, 5
  %mul.5 = mul i64 %mul.4, %indvars.iv.next.4
  %indvars.iv.next.5 = add nsw i64 %indvars.iv, 6
  %mul.6 = mul i64 %mul.5, %indvars.iv.next.5
  %indvars.iv.next.6 = add nsw i64 %indvars.iv, 7
  %mul.7 = mul i64 %mul.6, %indvars.iv.next.6
  %indvars.iv.next.7 = add nsw i64 %indvars.iv, 8
  %cmp3.7 = icmp slt i64 %indvars.iv.next.6, %2
  br i1 %cmp3.7, label %for.body, label %for.end.loopexit.unr-lcssa

for.end.loopexit.unr-lcssa:                       ; preds = %for.body
  %mul.7.lcssa = phi i64 [ %mul.7, %for.body ]
  br label %for.end.loopexit

for.end.loopexit:                                 ; preds = %for.body.lr.ph.split, %for.end.loopexit.unr-lcssa
  %mul.lcssa = phi i64 [ %mul.lcssa.unr, %for.body.lr.ph.split ], [ %mul.7.lcssa, %for.end.loopexit.unr-lcssa ]
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %for.cond.preheader
  %factorial.0.lcssa = phi i64 [ 1, %for.cond.preheader ], [ %mul.lcssa, %for.end.loopexit ]
  %call4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.3, i64 0, i64 0), i32 %1, i64 %factorial.0.lcssa) #1
  br label %if.end

if.end:                                           ; preds = %for.end, %if.then
  call void @llvm.lifetime.end(i64 4, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = distinct !{!5, !6}
!6 = !{!"llvm.loop.unroll.disable"}
