; ModuleID = 'test48.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [43 x i8] c"Enter number of rows (between 1 and 100): \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.2 = private unnamed_addr constant [46 x i8] c"Enter number of columns (between 1 and 100): \00", align 1
@.str.3 = private unnamed_addr constant [32 x i8] c"\0AEnter elements of 1st matrix:\0A\00", align 1
@.str.4 = private unnamed_addr constant [22 x i8] c"Enter element a%d%d: \00", align 1
@.str.5 = private unnamed_addr constant [31 x i8] c"Enter elements of 2nd matrix:\0A\00", align 1
@.str.6 = private unnamed_addr constant [26 x i8] c"\0ASum of two matrix is: \0A\0A\00", align 1
@.str.7 = private unnamed_addr constant [6 x i8] c"%d   \00", align 1
@.str.8 = private unnamed_addr constant [3 x i8] c"\0A\0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %a = alloca [100 x [100 x i32]], align 16
  %b = alloca [100 x [100 x i32]], align 16
  %sum = alloca [100 x [100 x i32]], align 16
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([43 x i8], [43 x i8]* @.str, i32 0, i32 0))
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0), i32* %r)
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.2, i32 0, i32 0))
  %call3 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0), i32* %c)
  %call4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.3, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc.14, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %r, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end.16

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4
  br label %for.cond.5

for.cond.5:                                       ; preds = %for.inc, %for.body
  %2 = load i32, i32* %j, align 4
  %3 = load i32, i32* %c, align 4
  %cmp6 = icmp slt i32 %2, %3
  br i1 %cmp6, label %for.body.7, label %for.end

for.body.7:                                       ; preds = %for.cond.5
  %4 = load i32, i32* %i, align 4
  %add = add nsw i32 %4, 1
  %5 = load i32, i32* %j, align 4
  %add8 = add nsw i32 %5, 1
  %call9 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.4, i32 0, i32 0), i32 %add, i32 %add8)
  %6 = load i32, i32* %i, align 4
  %add10 = add nsw i32 %6, 1
  %7 = load i32, i32* %j, align 4
  %add11 = add nsw i32 %7, 1
  %mul = mul nsw i32 %add10, %add11
  %8 = load i32, i32* %j, align 4
  %idxprom = sext i32 %8 to i64
  %9 = load i32, i32* %i, align 4
  %idxprom12 = sext i32 %9 to i64
  %arrayidx = getelementptr inbounds [100 x [100 x i32]], [100 x [100 x i32]]* %a, i32 0, i64 %idxprom12
  %arrayidx13 = getelementptr inbounds [100 x i32], [100 x i32]* %arrayidx, i32 0, i64 %idxprom
  store i32 %mul, i32* %arrayidx13, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body.7
  %10 = load i32, i32* %j, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond.5

for.end:                                          ; preds = %for.cond.5
  br label %for.inc.14

for.inc.14:                                       ; preds = %for.end
  %11 = load i32, i32* %i, align 4
  %inc15 = add nsw i32 %11, 1
  store i32 %inc15, i32* %i, align 4
  br label %for.cond

for.end.16:                                       ; preds = %for.cond
  %call17 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.5, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond.18

for.cond.18:                                      ; preds = %for.inc.37, %for.end.16
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %r, align 4
  %cmp19 = icmp slt i32 %12, %13
  br i1 %cmp19, label %for.body.20, label %for.end.39

for.body.20:                                      ; preds = %for.cond.18
  store i32 0, i32* %j, align 4
  br label %for.cond.21

for.cond.21:                                      ; preds = %for.inc.34, %for.body.20
  %14 = load i32, i32* %j, align 4
  %15 = load i32, i32* %c, align 4
  %cmp22 = icmp slt i32 %14, %15
  br i1 %cmp22, label %for.body.23, label %for.end.36

for.body.23:                                      ; preds = %for.cond.21
  %16 = load i32, i32* %i, align 4
  %add24 = add nsw i32 %16, 1
  %17 = load i32, i32* %j, align 4
  %add25 = add nsw i32 %17, 1
  %call26 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.4, i32 0, i32 0), i32 %add24, i32 %add25)
  %18 = load i32, i32* %i, align 4
  %add27 = add nsw i32 %18, 1
  %19 = load i32, i32* %j, align 4
  %add28 = add nsw i32 %19, 1
  %mul29 = mul nsw i32 %add27, %add28
  %20 = load i32, i32* %j, align 4
  %idxprom30 = sext i32 %20 to i64
  %21 = load i32, i32* %i, align 4
  %idxprom31 = sext i32 %21 to i64
  %arrayidx32 = getelementptr inbounds [100 x [100 x i32]], [100 x [100 x i32]]* %b, i32 0, i64 %idxprom31
  %arrayidx33 = getelementptr inbounds [100 x i32], [100 x i32]* %arrayidx32, i32 0, i64 %idxprom30
  store i32 %mul29, i32* %arrayidx33, align 4
  br label %for.inc.34

for.inc.34:                                       ; preds = %for.body.23
  %22 = load i32, i32* %j, align 4
  %inc35 = add nsw i32 %22, 1
  store i32 %inc35, i32* %j, align 4
  br label %for.cond.21

for.end.36:                                       ; preds = %for.cond.21
  br label %for.inc.37

for.inc.37:                                       ; preds = %for.end.36
  %23 = load i32, i32* %i, align 4
  %inc38 = add nsw i32 %23, 1
  store i32 %inc38, i32* %i, align 4
  br label %for.cond.18

for.end.39:                                       ; preds = %for.cond.18
  store i32 0, i32* %i, align 4
  br label %for.cond.40

for.cond.40:                                      ; preds = %for.inc.62, %for.end.39
  %24 = load i32, i32* %i, align 4
  %25 = load i32, i32* %r, align 4
  %cmp41 = icmp slt i32 %24, %25
  br i1 %cmp41, label %for.body.42, label %for.end.64

for.body.42:                                      ; preds = %for.cond.40
  store i32 0, i32* %j, align 4
  br label %for.cond.43

for.cond.43:                                      ; preds = %for.inc.59, %for.body.42
  %26 = load i32, i32* %j, align 4
  %27 = load i32, i32* %c, align 4
  %cmp44 = icmp slt i32 %26, %27
  br i1 %cmp44, label %for.body.45, label %for.end.61

for.body.45:                                      ; preds = %for.cond.43
  %28 = load i32, i32* %j, align 4
  %idxprom46 = sext i32 %28 to i64
  %29 = load i32, i32* %i, align 4
  %idxprom47 = sext i32 %29 to i64
  %arrayidx48 = getelementptr inbounds [100 x [100 x i32]], [100 x [100 x i32]]* %a, i32 0, i64 %idxprom47
  %arrayidx49 = getelementptr inbounds [100 x i32], [100 x i32]* %arrayidx48, i32 0, i64 %idxprom46
  %30 = load i32, i32* %arrayidx49, align 4
  %31 = load i32, i32* %j, align 4
  %idxprom50 = sext i32 %31 to i64
  %32 = load i32, i32* %i, align 4
  %idxprom51 = sext i32 %32 to i64
  %arrayidx52 = getelementptr inbounds [100 x [100 x i32]], [100 x [100 x i32]]* %b, i32 0, i64 %idxprom51
  %arrayidx53 = getelementptr inbounds [100 x i32], [100 x i32]* %arrayidx52, i32 0, i64 %idxprom50
  %33 = load i32, i32* %arrayidx53, align 4
  %add54 = add nsw i32 %30, %33
  %34 = load i32, i32* %j, align 4
  %idxprom55 = sext i32 %34 to i64
  %35 = load i32, i32* %i, align 4
  %idxprom56 = sext i32 %35 to i64
  %arrayidx57 = getelementptr inbounds [100 x [100 x i32]], [100 x [100 x i32]]* %sum, i32 0, i64 %idxprom56
  %arrayidx58 = getelementptr inbounds [100 x i32], [100 x i32]* %arrayidx57, i32 0, i64 %idxprom55
  store i32 %add54, i32* %arrayidx58, align 4
  br label %for.inc.59

for.inc.59:                                       ; preds = %for.body.45
  %36 = load i32, i32* %j, align 4
  %inc60 = add nsw i32 %36, 1
  store i32 %inc60, i32* %j, align 4
  br label %for.cond.43

for.end.61:                                       ; preds = %for.cond.43
  br label %for.inc.62

for.inc.62:                                       ; preds = %for.end.61
  %37 = load i32, i32* %i, align 4
  %inc63 = add nsw i32 %37, 1
  store i32 %inc63, i32* %i, align 4
  br label %for.cond.40

for.end.64:                                       ; preds = %for.cond.40
  %call65 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.6, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond.66

for.cond.66:                                      ; preds = %for.inc.82, %for.end.64
  %38 = load i32, i32* %i, align 4
  %39 = load i32, i32* %r, align 4
  %cmp67 = icmp slt i32 %38, %39
  br i1 %cmp67, label %for.body.68, label %for.end.84

for.body.68:                                      ; preds = %for.cond.66
  store i32 0, i32* %j, align 4
  br label %for.cond.69

for.cond.69:                                      ; preds = %for.inc.79, %for.body.68
  %40 = load i32, i32* %j, align 4
  %41 = load i32, i32* %c, align 4
  %cmp70 = icmp slt i32 %40, %41
  br i1 %cmp70, label %for.body.71, label %for.end.81

for.body.71:                                      ; preds = %for.cond.69
  %42 = load i32, i32* %j, align 4
  %idxprom72 = sext i32 %42 to i64
  %43 = load i32, i32* %i, align 4
  %idxprom73 = sext i32 %43 to i64
  %arrayidx74 = getelementptr inbounds [100 x [100 x i32]], [100 x [100 x i32]]* %sum, i32 0, i64 %idxprom73
  %arrayidx75 = getelementptr inbounds [100 x i32], [100 x i32]* %arrayidx74, i32 0, i64 %idxprom72
  %44 = load i32, i32* %arrayidx75, align 4
  %call76 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.7, i32 0, i32 0), i32 %44)
  %45 = load i32, i32* %j, align 4
  %46 = load i32, i32* %c, align 4
  %sub = sub nsw i32 %46, 1
  %cmp77 = icmp eq i32 %45, %sub
  br i1 %cmp77, label %if.then, label %if.end

if.then:                                          ; preds = %for.body.71
  %call78 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.8, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body.71
  br label %for.inc.79

for.inc.79:                                       ; preds = %if.end
  %47 = load i32, i32* %j, align 4
  %inc80 = add nsw i32 %47, 1
  store i32 %inc80, i32* %j, align 4
  br label %for.cond.69

for.end.81:                                       ; preds = %for.cond.69
  br label %for.inc.82

for.inc.82:                                       ; preds = %for.end.81
  %48 = load i32, i32* %i, align 4
  %inc83 = add nsw i32 %48, 1
  store i32 %inc83, i32* %i, align 4
  br label %for.cond.66

for.end.84:                                       ; preds = %for.cond.66
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @__isoc99_scanf(i8*, ...) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
