; ModuleID = 'test33.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [31 x i8] c"Enter an operator (+, -, *,): \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%c\00", align 1
@.str.2 = private unnamed_addr constant [21 x i8] c"Enter two operands: \00", align 1
@.str.3 = private unnamed_addr constant [8 x i8] c"%lf %lf\00", align 1
@.str.4 = private unnamed_addr constant [22 x i8] c"%.1lf + %.1lf = %.1lf\00", align 1
@.str.5 = private unnamed_addr constant [22 x i8] c"%.1lf - %.1lf = %.1lf\00", align 1
@.str.6 = private unnamed_addr constant [22 x i8] c"%.1lf * %.1lf = %.1lf\00", align 1
@.str.7 = private unnamed_addr constant [22 x i8] c"%.1lf / %.1lf = %.1lf\00", align 1
@.str.8 = private unnamed_addr constant [31 x i8] c"Error! operator is not correct\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %op = alloca i8, align 1
  %firstNumber = alloca double, align 8
  %secondNumber = alloca double, align 8
  call void @llvm.lifetime.start(i64 1, i8* nonnull %op) #1
  %0 = bitcast double* %firstNumber to i8*
  call void @llvm.lifetime.start(i64 8, i8* %0) #1
  %1 = bitcast double* %secondNumber to i8*
  call void @llvm.lifetime.start(i64 8, i8* %1) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i8* nonnull %op) #1
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.2, i64 0, i64 0)) #1
  %call3 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.3, i64 0, i64 0), double* nonnull %firstNumber, double* nonnull %secondNumber) #1
  %2 = load i8, i8* %op, align 1, !tbaa !1
  %conv = sext i8 %2 to i32
  switch i32 %conv, label %sw.default [
    i32 43, label %sw.bb
    i32 45, label %sw.bb.5
    i32 42, label %sw.bb.7
    i32 47, label %sw.bb.9
  ]

sw.bb:                                            ; preds = %entry
  %3 = load double, double* %firstNumber, align 8, !tbaa !4
  %4 = load double, double* %secondNumber, align 8, !tbaa !4
  %add = fadd double %3, %4
  %call4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.4, i64 0, i64 0), double %3, double %4, double %add) #1
  br label %sw.epilog

sw.bb.5:                                          ; preds = %entry
  %5 = load double, double* %firstNumber, align 8, !tbaa !4
  %6 = load double, double* %secondNumber, align 8, !tbaa !4
  %sub = fsub double %5, %6
  %call6 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.5, i64 0, i64 0), double %5, double %6, double %sub) #1
  br label %sw.epilog

sw.bb.7:                                          ; preds = %entry
  %7 = load double, double* %firstNumber, align 8, !tbaa !4
  %8 = load double, double* %secondNumber, align 8, !tbaa !4
  %mul = fmul double %7, %8
  %call8 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.6, i64 0, i64 0), double %7, double %8, double %mul) #1
  br label %sw.epilog

sw.bb.9:                                          ; preds = %entry
  %9 = load double, double* %firstNumber, align 8, !tbaa !4
  %10 = load double, double* %secondNumber, align 8, !tbaa !4
  %div = fdiv double %9, %9
  %call10 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.7, i64 0, i64 0), double %9, double %10, double %div) #1
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %call11 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.8, i64 0, i64 0)) #1
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb.9, %sw.bb.7, %sw.bb.5, %sw.bb
  call void @llvm.lifetime.end(i64 8, i8* %1) #1
  call void @llvm.lifetime.end(i64 8, i8* %0) #1
  call void @llvm.lifetime.end(i64 1, i8* nonnull %op) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"omnipotent char", !3, i64 0}
!3 = !{!"Simple C/C++ TBAA"}
!4 = !{!5, !5, i64 0}
!5 = !{!"double", !2, i64 0}
