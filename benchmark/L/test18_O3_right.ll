; ModuleID = 'test18.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [19 x i8] c"Enter an integer: \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.2 = private unnamed_addr constant [15 x i8] c"%d * %d = %d \0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %n = alloca i32, align 4
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start(i64 4, i8* %0) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %n) #1
  %1 = load i32, i32* %n, align 4, !tbaa !1
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i64 0, i64 0), i32 %1, i32 1, i32 %1) #1
  %2 = load i32, i32* %n, align 4, !tbaa !1
  %mul.1 = shl nsw i32 %2, 1
  %call2.1 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i64 0, i64 0), i32 %2, i32 2, i32 %mul.1) #1
  %3 = load i32, i32* %n, align 4, !tbaa !1
  %mul.2 = mul nsw i32 %3, 3
  %call2.2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i64 0, i64 0), i32 %3, i32 3, i32 %mul.2) #1
  %4 = load i32, i32* %n, align 4, !tbaa !1
  %mul.3 = shl nsw i32 %4, 2
  %call2.3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i64 0, i64 0), i32 %4, i32 4, i32 %mul.3) #1
  %5 = load i32, i32* %n, align 4, !tbaa !1
  %mul.4 = mul nsw i32 %5, 5
  %call2.4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i64 0, i64 0), i32 %5, i32 5, i32 %mul.4) #1
  %6 = load i32, i32* %n, align 4, !tbaa !1
  %mul.5 = mul nsw i32 %6, 6
  %call2.5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i64 0, i64 0), i32 %6, i32 6, i32 %mul.5) #1
  %7 = load i32, i32* %n, align 4, !tbaa !1
  %mul.6 = mul nsw i32 %7, 7
  %call2.6 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i64 0, i64 0), i32 %7, i32 7, i32 %mul.6) #1
  %8 = load i32, i32* %n, align 4, !tbaa !1
  %mul.7 = shl nsw i32 %8, 3
  %call2.7 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i64 0, i64 0), i32 %8, i32 8, i32 %mul.7) #1
  %9 = load i32, i32* %n, align 4, !tbaa !1
  %mul.8 = mul nsw i32 %9, 9
  %call2.8 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i64 0, i64 0), i32 %9, i32 9, i32 %mul.8) #1
  %10 = load i32, i32* %n, align 4, !tbaa !1
  %mul.9 = mul nsw i32 %10, 10
  %call2.9 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i64 0, i64 0), i32 %10, i32 10, i32 %mul.9) #1
  call void @llvm.lifetime.end(i64 4, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
