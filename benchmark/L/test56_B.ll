; ModuleID = 'test56.bc'

@.str = private constant [25 x i8] c"Enter a line of string: \00", align 1
@.str.1 = private constant [6 x i8] c"%[^\0A]\00", align 1
@.str.2 = private constant [11 x i8] c"Vowels: %d\00", align 1
@.str.3 = private constant [16 x i8] c"\0AConsonants: %d\00", align 1
@.str.4 = private constant [12 x i8] c"\0ADigits: %d\00", align 1
@.str.5 = private constant [18 x i8] c"\0AWhite spaces: %d\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_10 = alloca i32, align 4
  %_temp_dummy11_11 = alloca i32, align 4
  %_temp_dummy12_12 = alloca i32, align 4
  %_temp_dummy13_13 = alloca i32, align 4
  %_temp_dummy14_14 = alloca i32, align 4
  %_temp_dummy15_15 = alloca i32, align 4
  %consonants_7 = alloca i32, align 4
  %digits_8 = alloca i32, align 4
  %i_5 = alloca i32, align 4
  %line_4 = alloca [150 x i8], align 1
  %spaces_9 = alloca i32, align 4
  %vowels_6 = alloca i32, align 4
  store i32 0, i32* %spaces_9, align 4
  %0 = load i32, i32* %spaces_9, align 4
  store i32 %0, i32* %digits_8, align 4
  %1 = load i32, i32* %digits_8, align 4
  store i32 %1, i32* %consonants_7, align 4
  %2 = load i32, i32* %consonants_7, align 4
  store i32 %2, i32* %vowels_6, align 4
  %3 = bitcast i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %3)
  %4 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %arrayAddress = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i32 0
  %5 = bitcast i8* %arrayAddress to i8*
  %call2 = call i32 (i8*, ...) @scanf(i8* %4, i8* %5)
  store i32 0, i32* %i_5, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %6 = load i32, i32* %i_5, align 4
  %conv3 = sext i32 %6 to i64
  %arrayidx = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv3
  %7 = load i8, i8* %arrayidx, align 1
  %conv31 = sext i8 %7 to i32
  %cmp1 = icmp ne i32 %conv31, 0
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %8 = load i32, i32* %i_5, align 4
  %conv318 = sext i32 %8 to i64
  %arrayidx19 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv318
  %9 = load i8, i8* %arrayidx19, align 1
  %conv320 = sext i8 %9 to i32
  %cmp2 = icmp eq i32 %conv320, 97
  br i1 %cmp2, label %lor.end17, label %lor.rhs16

while.end:                                        ; preds = %while.cond
  %10 = bitcast i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.2, i32 0, i32 0) to i8*
  %11 = load i32, i32* %vowels_6, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %10, i32 %11)
  %12 = bitcast i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.3, i32 0, i32 0) to i8*
  %13 = load i32, i32* %consonants_7, align 4
  %call4 = call i32 (i8*, ...) @printf(i8* %12, i32 %13)
  %14 = bitcast i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.4, i32 0, i32 0) to i8*
  %15 = load i32, i32* %digits_8, align 4
  %call5 = call i32 (i8*, ...) @printf(i8* %14, i32 %15)
  %16 = bitcast i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.5, i32 0, i32 0) to i8*
  %17 = load i32, i32* %spaces_9, align 4
  %call6 = call i32 (i8*, ...) @printf(i8* %16, i32 %17)
  ret i32 0

if.then:                                          ; preds = %lor.end
  %18 = load i32, i32* %vowels_6, align 4
  %add = add i32 %18, 1
  store i32 %add, i32* %vowels_6, align 4
  br label %if.end

if.else:                                          ; preds = %lor.end
  %19 = load i32, i32* %i_5, align 4
  %conv352 = sext i32 %19 to i64
  %arrayidx53 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv352
  %20 = load i8, i8* %arrayidx53, align 1
  %conv354 = sext i8 %20 to i32
  %add.55 = sub i32 %conv354, 97
  %conv = trunc i32 %add.55 to i8
  %conv356 = sext i8 %conv to i32
  %cmp12 = icmp ule i32 %conv356, 25
  br i1 %cmp12, label %lor.end51, label %lor.rhs50

lor.rhs:                                          ; preds = %lor.end3
  %21 = load i32, i32* %i_5, align 4
  %conv345 = sext i32 %21 to i64
  %arrayidx46 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv345
  %22 = load i8, i8* %arrayidx46, align 1
  %conv347 = sext i8 %22 to i32
  %cmp11 = icmp eq i32 %conv347, 85
  br label %lor.end

lor.end:                                          ; preds = %lor.end3, %lor.rhs
  %23 = phi i1 [ true, %lor.end3 ], [ %cmp11, %lor.rhs ]
  br i1 %23, label %if.then, label %if.else

lor.rhs2:                                         ; preds = %lor.end5
  %24 = load i32, i32* %i_5, align 4
  %conv342 = sext i32 %24 to i64
  %arrayidx43 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv342
  %25 = load i8, i8* %arrayidx43, align 1
  %conv344 = sext i8 %25 to i32
  %cmp10 = icmp eq i32 %conv344, 79
  br label %lor.end3

lor.end3:                                         ; preds = %lor.end5, %lor.rhs2
  %26 = phi i1 [ true, %lor.end5 ], [ %cmp10, %lor.rhs2 ]
  br i1 %26, label %lor.end, label %lor.rhs

lor.rhs4:                                         ; preds = %lor.end7
  %27 = load i32, i32* %i_5, align 4
  %conv339 = sext i32 %27 to i64
  %arrayidx40 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv339
  %28 = load i8, i8* %arrayidx40, align 1
  %conv341 = sext i8 %28 to i32
  %cmp9 = icmp eq i32 %conv341, 73
  br label %lor.end5

lor.end5:                                         ; preds = %lor.end7, %lor.rhs4
  %29 = phi i1 [ true, %lor.end7 ], [ %cmp9, %lor.rhs4 ]
  br i1 %29, label %lor.end3, label %lor.rhs2

lor.rhs6:                                         ; preds = %lor.end9
  %30 = load i32, i32* %i_5, align 4
  %conv336 = sext i32 %30 to i64
  %arrayidx37 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv336
  %31 = load i8, i8* %arrayidx37, align 1
  %conv338 = sext i8 %31 to i32
  %cmp8 = icmp eq i32 %conv338, 69
  br label %lor.end7

lor.end7:                                         ; preds = %lor.end9, %lor.rhs6
  %32 = phi i1 [ true, %lor.end9 ], [ %cmp8, %lor.rhs6 ]
  br i1 %32, label %lor.end5, label %lor.rhs4

lor.rhs8:                                         ; preds = %lor.end11
  %33 = load i32, i32* %i_5, align 4
  %conv333 = sext i32 %33 to i64
  %arrayidx34 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv333
  %34 = load i8, i8* %arrayidx34, align 1
  %conv335 = sext i8 %34 to i32
  %cmp7 = icmp eq i32 %conv335, 65
  br label %lor.end9

lor.end9:                                         ; preds = %lor.end11, %lor.rhs8
  %35 = phi i1 [ true, %lor.end11 ], [ %cmp7, %lor.rhs8 ]
  br i1 %35, label %lor.end7, label %lor.rhs6

lor.rhs10:                                        ; preds = %lor.end13
  %36 = load i32, i32* %i_5, align 4
  %conv330 = sext i32 %36 to i64
  %arrayidx31 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv330
  %37 = load i8, i8* %arrayidx31, align 1
  %conv332 = sext i8 %37 to i32
  %cmp6 = icmp eq i32 %conv332, 117
  br label %lor.end11

lor.end11:                                        ; preds = %lor.end13, %lor.rhs10
  %38 = phi i1 [ true, %lor.end13 ], [ %cmp6, %lor.rhs10 ]
  br i1 %38, label %lor.end9, label %lor.rhs8

lor.rhs12:                                        ; preds = %lor.end15
  %39 = load i32, i32* %i_5, align 4
  %conv327 = sext i32 %39 to i64
  %arrayidx28 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv327
  %40 = load i8, i8* %arrayidx28, align 1
  %conv329 = sext i8 %40 to i32
  %cmp5 = icmp eq i32 %conv329, 111
  br label %lor.end13

lor.end13:                                        ; preds = %lor.end15, %lor.rhs12
  %41 = phi i1 [ true, %lor.end15 ], [ %cmp5, %lor.rhs12 ]
  br i1 %41, label %lor.end11, label %lor.rhs10

lor.rhs14:                                        ; preds = %lor.end17
  %42 = load i32, i32* %i_5, align 4
  %conv324 = sext i32 %42 to i64
  %arrayidx25 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv324
  %43 = load i8, i8* %arrayidx25, align 1
  %conv326 = sext i8 %43 to i32
  %cmp4 = icmp eq i32 %conv326, 105
  br label %lor.end15

lor.end15:                                        ; preds = %lor.end17, %lor.rhs14
  %44 = phi i1 [ true, %lor.end17 ], [ %cmp4, %lor.rhs14 ]
  br i1 %44, label %lor.end13, label %lor.rhs12

lor.rhs16:                                        ; preds = %while.body
  %45 = load i32, i32* %i_5, align 4
  %conv321 = sext i32 %45 to i64
  %arrayidx22 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv321
  %46 = load i8, i8* %arrayidx22, align 1
  %conv323 = sext i8 %46 to i32
  %cmp3 = icmp eq i32 %conv323, 101
  br label %lor.end17

lor.end17:                                        ; preds = %lor.rhs16, %while.body
  %47 = phi i1 [ true, %while.body ], [ %cmp3, %lor.rhs16 ]
  br i1 %47, label %lor.end15, label %lor.rhs14

if.end:                                           ; preds = %if.end64, %if.then
  %48 = load i32, i32* %i_5, align 4
  %add.82 = add i32 %48, 1
  store i32 %add.82, i32* %i_5, align 4
  br label %while.cond

if.then48:                                        ; preds = %lor.end51
  %49 = load i32, i32* %consonants_7, align 4
  %add.63 = add i32 %49, 1
  store i32 %add.63, i32* %consonants_7, align 4
  br label %if.end64

if.else49:                                        ; preds = %lor.end51
  %50 = load i32, i32* %i_5, align 4
  %conv367 = sext i32 %50 to i64
  %arrayidx68 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv367
  %51 = load i8, i8* %arrayidx68, align 1
  %conv369 = sext i8 %51 to i32
  %add.70 = sub i32 %conv369, 48
  %conv71 = trunc i32 %add.70 to i8
  %conv372 = sext i8 %conv71 to i32
  %cmp14 = icmp ule i32 %conv372, 9
  br i1 %cmp14, label %if.then65, label %if.else66

lor.rhs50:                                        ; preds = %if.else
  %52 = load i32, i32* %i_5, align 4
  %conv357 = sext i32 %52 to i64
  %arrayidx58 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv357
  %53 = load i8, i8* %arrayidx58, align 1
  %conv359 = sext i8 %53 to i32
  %add.60 = sub i32 %conv359, 65
  %conv61 = trunc i32 %add.60 to i8
  %conv362 = sext i8 %conv61 to i32
  %cmp13 = icmp ule i32 %conv362, 25
  br label %lor.end51

lor.end51:                                        ; preds = %lor.rhs50, %if.else
  %54 = phi i1 [ true, %if.else ], [ %cmp13, %lor.rhs50 ]
  br i1 %54, label %if.then48, label %if.else49

if.end64:                                         ; preds = %if.end74, %if.then48
  br label %if.end

if.then65:                                        ; preds = %if.else49
  %55 = load i32, i32* %digits_8, align 4
  %add.73 = add i32 %55, 1
  store i32 %add.73, i32* %digits_8, align 4
  br label %if.end74

if.else66:                                        ; preds = %if.else49
  %56 = load i32, i32* %i_5, align 4
  %conv377 = sext i32 %56 to i64
  %arrayidx78 = getelementptr [150 x i8], [150 x i8]* %line_4, i32 0, i64 %conv377
  %57 = load i8, i8* %arrayidx78, align 1
  %conv379 = sext i8 %57 to i32
  %cmp15 = icmp eq i32 %conv379, 32
  br i1 %cmp15, label %if.then75, label %if.else76

if.end74:                                         ; preds = %if.end81, %if.then65
  br label %if.end64

if.then75:                                        ; preds = %if.else66
  %58 = load i32, i32* %spaces_9, align 4
  %add.80 = add i32 %58, 1
  store i32 %add.80, i32* %spaces_9, align 4
  br label %if.end81

if.else76:                                        ; preds = %if.else66
  br label %if.end81

if.end81:                                         ; preds = %if.else76, %if.then75
  br label %if.end74
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
