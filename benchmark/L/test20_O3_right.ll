; ModuleID = 'test20.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [21 x i8] c"Enter two integers: \00", align 1
@.str.1 = private unnamed_addr constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private unnamed_addr constant [25 x i8] c"G.C.D of %d and %d is %d\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %n1 = alloca i32, align 4
  %n2 = alloca i32, align 4
  %0 = bitcast i32* %n1 to i8*
  call void @llvm.lifetime.start(i64 4, i8* %0) #1
  %1 = bitcast i32* %n2 to i8*
  call void @llvm.lifetime.start(i64 4, i8* %1) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %n1, i32* nonnull %n2) #1
  %2 = load i32, i32* %n1, align 4, !tbaa !1
  %3 = load i32, i32* %n2, align 4, !tbaa !1
  %cmp2.15 = icmp sgt i32 %3, 0
  %not.cmp.16 = icmp sgt i32 %2, 0
  %4 = and i1 %not.cmp.16, %cmp2.15
  br i1 %4, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.inc
  %gcd.018 = phi i32 [ %gcd.1, %for.inc ], [ undef, %for.body.preheader ]
  %i.017 = phi i32 [ %inc, %for.inc ], [ 1, %for.body.preheader ]
  %rem = srem i32 %2, %i.017
  %cmp3 = icmp eq i32 %rem, 0
  br i1 %cmp3, label %land.lhs.true, label %for.inc

land.lhs.true:                                    ; preds = %for.body
  %rem4 = srem i32 %3, %i.017
  %cmp5 = icmp eq i32 %rem4, 0
  %i.0.gcd.0 = select i1 %cmp5, i32 %i.017, i32 %gcd.018
  br label %for.inc

for.inc:                                          ; preds = %land.lhs.true, %for.body
  %gcd.1 = phi i32 [ %gcd.018, %for.body ], [ %i.0.gcd.0, %land.lhs.true ]
  %inc = add nuw nsw i32 %i.017, 1
  %cmp2 = icmp slt i32 %i.017, %3
  %not.cmp = icmp slt i32 %i.017, %2
  %5 = and i1 %not.cmp, %cmp2
  br i1 %5, label %for.body, label %for.end.loopexit

for.end.loopexit:                                 ; preds = %for.inc
  %gcd.1.lcssa = phi i32 [ %gcd.1, %for.inc ]
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  %gcd.0.lcssa = phi i32 [ undef, %entry ], [ %gcd.1.lcssa, %for.end.loopexit ]
  %call6 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.2, i64 0, i64 0), i32 %2, i32 %3, i32 %gcd.0.lcssa) #1
  call void @llvm.lifetime.end(i64 4, i8* %1) #1
  call void @llvm.lifetime.end(i64 4, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
