; ModuleID = 'test13_O.bc'

@.str = private constant [15 x i8] c"Enter a year: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [19 x i8] c"%d is a leap year.\00", align 1
@.str.3 = private constant [23 x i8] c"%d is not a leap year.\00", align 1

define i32 @main() {
entry:
  %.preg_U4_8_49 = alloca i32
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_dummy11_6 = alloca i32, align 4
  %_temp_dummy12_7 = alloca i32, align 4
  %_temp_dummy13_8 = alloca i32, align 4
  %_temp_dummy14_9 = alloca i32, align 4
  %_temp_dummy15_10 = alloca i32, align 4
  %_temp_ehpit0_11 = alloca i32, align 4
  %old_frame_pointer_16.addr = alloca i64, align 8
  %return_address_17.addr = alloca i64, align 8
  %year_4 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %year_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = load i32, i32* %year_4, align 4
  store i32 %3, i32* %.preg_U4_8_49, align 8
  %4 = load i32, i32* %.preg_U4_8_49
  %and = and i32 %4, 3
  %cmp1 = icmp eq i32 %and, 0
  br i1 %cmp1, label %tb, label %L3330

tb:                                               ; preds = %entry
  %5 = load i32, i32* %.preg_U4_8_49
  %srem = srem i32 %5, 100
  %cmp2 = icmp eq i32 %srem, 0
  br i1 %cmp2, label %tb1, label %L3586

L3330:                                            ; preds = %entry
  %6 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.3, i32 0, i32 0) to i8*
  %7 = load i32, i32* %.preg_U4_8_49
  %call6 = call i32 (i8*, ...) @printf(i8* %6, i32 %7)
  br label %L4610

tb1:                                              ; preds = %tb
  %8 = load i32, i32* %.preg_U4_8_49
  %srem.2 = srem i32 %8, 400
  %cmp3 = icmp eq i32 %srem.2, 0
  br i1 %cmp3, label %tb3, label %L3842

L3586:                                            ; preds = %tb
  %9 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.2, i32 0, i32 0) to i8*
  %10 = load i32, i32* %.preg_U4_8_49
  %call5 = call i32 (i8*, ...) @printf(i8* %9, i32 %10)
  br label %L4354

tb3:                                              ; preds = %tb1
  %11 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.2, i32 0, i32 0) to i8*
  %12 = load i32, i32* %.preg_U4_8_49
  %call3 = call i32 (i8*, ...) @printf(i8* %11, i32 %12)
  br label %L4098

L3842:                                            ; preds = %tb1
  %13 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.3, i32 0, i32 0) to i8*
  %14 = load i32, i32* %.preg_U4_8_49
  %call4 = call i32 (i8*, ...) @printf(i8* %13, i32 %14)
  br label %L4098

L4098:                                            ; preds = %L3842, %tb3
  br label %L4354

L4354:                                            ; preds = %L4098, %L3586
  br label %L4610

L4610:                                            ; preds = %L4354, %L3330
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
