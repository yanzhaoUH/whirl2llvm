; ModuleID = 'test11_O.bc'

@.str = private constant [12 x i8] c"%lf %lf %lf\00", align 1
@.str.1 = private constant [28 x i8] c"%.2f is the largest number.\00", align 1

define i32 @main() {
entry:
  %.preg_F8_11_51 = alloca double
  %.preg_F8_11_50 = alloca double
  %.preg_F8_11_49 = alloca double
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %_temp_ehpit0_11 = alloca i32, align 4
  %n1_4 = alloca double, align 8
  %n2_5 = alloca double, align 8
  %n3_6 = alloca double, align 8
  %old_frame_pointer_16.addr = alloca i64, align 8
  %return_address_17.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0) to i8*
  %1 = bitcast double* %n1_4 to double*
  %2 = bitcast double* %n2_5 to double*
  %3 = bitcast double* %n3_6 to double*
  %call1 = call i32 (i8*, ...) @scanf(i8* %0, double* %1, double* %2, double* %3)
  %4 = load double, double* %n3_6, align 8
  store double %4, double* %.preg_F8_11_49, align 8
  %5 = load double, double* %n2_5, align 8
  store double %5, double* %.preg_F8_11_50, align 8
  %6 = load double, double* %n1_4, align 8
  store double %6, double* %.preg_F8_11_51, align 8
  %7 = load double, double* %.preg_F8_11_51
  %8 = load double, double* %.preg_F8_11_50
  %cmp1 = fcmp oge double %7, %8
  br i1 %cmp1, label %tb, label %L7170

tb:                                               ; preds = %entry
  %9 = load double, double* %.preg_F8_11_51
  %10 = load double, double* %.preg_F8_11_49
  %cmp2 = fcmp oge double %9, %10
  br i1 %cmp2, label %tb1, label %L5634

L7170:                                            ; preds = %entry
  br label %L5634

tb1:                                              ; preds = %tb
  %11 = bitcast i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.1, i32 0, i32 0) to i8*
  %12 = load double, double* %.preg_F8_11_51
  %call2 = call i32 (i8*, ...) @printf(i8* %11, double %12)
  %13 = load double, double* %n3_6, align 8
  store double %13, double* %.preg_F8_11_49, align 8
  %14 = load double, double* %n2_5, align 8
  store double %14, double* %.preg_F8_11_50, align 8
  %15 = load double, double* %n1_4, align 8
  store double %15, double* %.preg_F8_11_51, align 8
  br label %L5634

L5634:                                            ; preds = %tb1, %L7170, %tb
  %16 = load double, double* %.preg_F8_11_51
  %17 = load double, double* %.preg_F8_11_50
  %cmp3 = fcmp ole double %16, %17
  br i1 %cmp3, label %tb2, label %L7682

tb2:                                              ; preds = %L5634
  %18 = load double, double* %.preg_F8_11_50
  %19 = load double, double* %.preg_F8_11_49
  %cmp4 = fcmp oge double %18, %19
  br i1 %cmp4, label %tb3, label %L6146

L7682:                                            ; preds = %L5634
  br label %L6146

tb3:                                              ; preds = %tb2
  %20 = bitcast i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.1, i32 0, i32 0) to i8*
  %21 = load double, double* %.preg_F8_11_50
  %call3 = call i32 (i8*, ...) @printf(i8* %20, double %21)
  %22 = load double, double* %n3_6, align 8
  store double %22, double* %.preg_F8_11_49, align 8
  %23 = load double, double* %n1_4, align 8
  store double %23, double* %.preg_F8_11_51, align 8
  br label %L6146

L6146:                                            ; preds = %tb3, %L7682, %tb2
  %24 = load double, double* %.preg_F8_11_51
  %25 = load double, double* %.preg_F8_11_49
  %cmp5 = fcmp ole double %24, %25
  br i1 %cmp5, label %tb4, label %L8194

tb4:                                              ; preds = %L6146
  %26 = load double, double* %n2_5, align 8
  %27 = load double, double* %.preg_F8_11_49
  %cmp6 = fcmp ole double %26, %27
  br i1 %cmp6, label %tb5, label %L6658

L8194:                                            ; preds = %L6146
  br label %L6658

tb5:                                              ; preds = %tb4
  %28 = bitcast i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.1, i32 0, i32 0) to i8*
  %29 = load double, double* %.preg_F8_11_49
  %call4 = call i32 (i8*, ...) @printf(i8* %28, double %29)
  br label %L6658

L6658:                                            ; preds = %tb5, %L8194, %tb4
  ret i32 0
}

declare i32 @scanf(i8*, ...)

declare i32 @printf(i8*, ...)
