; ModuleID = 'test51.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [41 x i8] c"Enter rows and column for first matrix: \00", align 1
@.str.1 = private unnamed_addr constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private unnamed_addr constant [42 x i8] c"Enter rows and column for second matrix: \00", align 1
@.str.4 = private unnamed_addr constant [5 x i8] c"%d%d\00", align 1
@.str.6 = private unnamed_addr constant [23 x i8] c"Enter elements a%d%d: \00", align 1
@.str.8 = private unnamed_addr constant [23 x i8] c"Enter elements b%d%d: \00", align 1
@.str.10 = private unnamed_addr constant [5 x i8] c"%d  \00", align 1
@str = private unnamed_addr constant [58 x i8] c"Error! column of first matrix not equal to row of second.\00"
@str.12 = private unnamed_addr constant [29 x i8] c"\0AEnter elements of matrix 1:\00"
@str.13 = private unnamed_addr constant [29 x i8] c"\0AEnter elements of matrix 2:\00"
@str.14 = private unnamed_addr constant [16 x i8] c"\0AOutput Matrix:\00"
@str.15 = private unnamed_addr constant [2 x i8] c"\0A\00"

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %firstMatrix = alloca [10 x [10 x i32]], align 16
  %secondMatrix = alloca [10 x [10 x i32]], align 16
  %mult = alloca [10 x [10 x i32]], align 16
  %rowFirst = alloca i32, align 4
  %columnFirst = alloca i32, align 4
  %rowSecond = alloca i32, align 4
  %columnSecond = alloca i32, align 4
  %0 = bitcast [10 x [10 x i32]]* %firstMatrix to i8*
  call void @llvm.lifetime.start(i64 400, i8* %0) #1
  %1 = bitcast [10 x [10 x i32]]* %secondMatrix to i8*
  call void @llvm.lifetime.start(i64 400, i8* %1) #1
  %2 = bitcast [10 x [10 x i32]]* %mult to i8*
  call void @llvm.lifetime.start(i64 400, i8* %2) #1
  %3 = bitcast i32* %rowFirst to i8*
  call void @llvm.lifetime.start(i64 4, i8* %3) #1
  %4 = bitcast i32* %columnFirst to i8*
  call void @llvm.lifetime.start(i64 4, i8* %4) #1
  %5 = bitcast i32* %rowSecond to i8*
  call void @llvm.lifetime.start(i64 4, i8* %5) #1
  %6 = bitcast i32* %columnSecond to i8*
  call void @llvm.lifetime.start(i64 4, i8* %6) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %rowFirst, i32* nonnull %columnFirst) #1
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.2, i64 0, i64 0)) #1
  %call3 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %rowSecond, i32* nonnull %columnSecond) #1
  %7 = load i32, i32* %columnFirst, align 4, !tbaa !1
  %8 = load i32, i32* %rowSecond, align 4, !tbaa !1
  %cmp.29 = icmp eq i32 %7, %8
  br i1 %cmp.29, label %while.end, label %while.body.preheader

while.body.preheader:                             ; preds = %entry
  br label %while.body

while.body:                                       ; preds = %while.body.preheader, %while.body
  %puts = call i32 @puts(i8* getelementptr inbounds ([58 x i8], [58 x i8]* @str, i64 0, i64 0))
  %call5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str, i64 0, i64 0)) #1
  %call6 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.4, i64 0, i64 0), i32* nonnull %rowFirst, i32* nonnull %columnFirst) #1
  %call7 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.2, i64 0, i64 0)) #1
  %call8 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.4, i64 0, i64 0), i32* nonnull %rowSecond, i32* nonnull %columnSecond) #1
  %9 = load i32, i32* %columnFirst, align 4, !tbaa !1
  %10 = load i32, i32* %rowSecond, align 4, !tbaa !1
  %cmp = icmp eq i32 %9, %10
  br i1 %cmp, label %while.end.loopexit, label %while.body

while.end.loopexit:                               ; preds = %while.body
  %.lcssa68 = phi i32 [ %9, %while.body ]
  br label %while.end

while.end:                                        ; preds = %while.end.loopexit, %entry
  %.lcssa28 = phi i32 [ %7, %entry ], [ %.lcssa68, %while.end.loopexit ]
  %11 = load i32, i32* %rowFirst, align 4, !tbaa !1
  %12 = load i32, i32* %columnSecond, align 4, !tbaa !1
  %puts.i = call i32 @puts(i8* getelementptr inbounds ([29 x i8], [29 x i8]* @str.12, i64 0, i64 0)) #1
  %cmp.62.i = icmp sgt i32 %11, 0
  %cmp2.59.i = icmp sgt i32 %.lcssa28, 0
  %or.cond.i = and i1 %cmp2.59.i, %cmp.62.i
  br i1 %or.cond.i, label %for.body.3.lr.ph.us.i.preheader, label %for.end.12.i

for.body.3.lr.ph.us.i.preheader:                  ; preds = %while.end
  br label %for.body.3.lr.ph.us.i

for.body.3.us.i:                                  ; preds = %for.body.3.lr.ph.us.i, %for.body.3.us.i
  %indvars.iv69.i = phi i64 [ 0, %for.body.3.lr.ph.us.i ], [ %indvars.iv.next70.i, %for.body.3.us.i ]
  %indvars.iv.next70.i = add nuw nsw i64 %indvars.iv69.i, 1
  %13 = trunc i64 %indvars.iv.next70.i to i32
  %call5.us.i = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.6, i64 0, i64 0), i32 %16, i32 %13) #1
  %14 = mul nuw nsw i64 %indvars.iv.next70.i, %indvars.iv.next75.i
  %arrayidx9.us.i = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %firstMatrix, i64 0, i64 %indvars.iv74.i, i64 %indvars.iv69.i
  %15 = trunc i64 %14 to i32
  store i32 %15, i32* %arrayidx9.us.i, align 4, !tbaa !1
  %exitcond44 = icmp eq i32 %13, %.lcssa28
  br i1 %exitcond44, label %for.cond.loopexit.us.i, label %for.body.3.us.i

for.cond.loopexit.us.i:                           ; preds = %for.body.3.us.i
  %exitcond46 = icmp eq i32 %16, %11
  br i1 %exitcond46, label %for.end.12.i.loopexit, label %for.body.3.lr.ph.us.i

for.body.3.lr.ph.us.i:                            ; preds = %for.body.3.lr.ph.us.i.preheader, %for.cond.loopexit.us.i
  %indvars.iv74.i = phi i64 [ %indvars.iv.next75.i, %for.cond.loopexit.us.i ], [ 0, %for.body.3.lr.ph.us.i.preheader ]
  %indvars.iv.next75.i = add nuw nsw i64 %indvars.iv74.i, 1
  %16 = trunc i64 %indvars.iv.next75.i to i32
  br label %for.body.3.us.i

for.end.12.i.loopexit:                            ; preds = %for.cond.loopexit.us.i
  br label %for.end.12.i

for.end.12.i:                                     ; preds = %for.end.12.i.loopexit, %while.end
  %puts54.i = call i32 @puts(i8* getelementptr inbounds ([29 x i8], [29 x i8]* @str.13, i64 0, i64 0)) #1
  %cmp15.57.i = icmp sgt i32 %.lcssa28, 0
  %cmp18.55.i = icmp sgt i32 %12, 0
  %or.cond80.i = and i1 %cmp15.57.i, %cmp18.55.i
  br i1 %or.cond80.i, label %for.body.19.lr.ph.us.i.preheader, label %enterData.exit

for.body.19.lr.ph.us.i.preheader:                 ; preds = %for.end.12.i
  br label %for.body.19.lr.ph.us.i

for.body.19.us.i:                                 ; preds = %for.body.19.lr.ph.us.i, %for.body.19.us.i
  %indvars.iv.i = phi i64 [ 0, %for.body.19.lr.ph.us.i ], [ %indvars.iv.next.i, %for.body.19.us.i ]
  %indvars.iv.next.i = add nuw nsw i64 %indvars.iv.i, 1
  %17 = trunc i64 %indvars.iv.next.i to i32
  %call22.us.i = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.8, i64 0, i64 0), i32 %20, i32 %17) #1
  %18 = mul nuw nsw i64 %indvars.iv.next.i, %indvars.iv.next66.i
  %arrayidx29.us.i = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %secondMatrix, i64 0, i64 %indvars.iv65.i, i64 %indvars.iv.i
  %19 = trunc i64 %18 to i32
  store i32 %19, i32* %arrayidx29.us.i, align 4, !tbaa !1
  %exitcond40 = icmp eq i32 %17, %12
  br i1 %exitcond40, label %for.cond.14.loopexit.us.i, label %for.body.19.us.i

for.cond.14.loopexit.us.i:                        ; preds = %for.body.19.us.i
  %exitcond42 = icmp eq i32 %20, %.lcssa28
  br i1 %exitcond42, label %enterData.exit.loopexit, label %for.body.19.lr.ph.us.i

for.body.19.lr.ph.us.i:                           ; preds = %for.body.19.lr.ph.us.i.preheader, %for.cond.14.loopexit.us.i
  %indvars.iv65.i = phi i64 [ %indvars.iv.next66.i, %for.cond.14.loopexit.us.i ], [ 0, %for.body.19.lr.ph.us.i.preheader ]
  %indvars.iv.next66.i = add nuw nsw i64 %indvars.iv65.i, 1
  %20 = trunc i64 %indvars.iv.next66.i to i32
  br label %for.body.19.us.i

enterData.exit.loopexit:                          ; preds = %for.cond.14.loopexit.us.i
  br label %enterData.exit

enterData.exit:                                   ; preds = %enterData.exit.loopexit, %for.end.12.i
  %21 = load i32, i32* %rowFirst, align 4, !tbaa !1
  %22 = load i32, i32* %columnFirst, align 4, !tbaa !1
  %23 = load i32, i32* %columnSecond, align 4, !tbaa !1
  %cmp.88.i = icmp sgt i32 %21, 0
  %cmp2.86.i = icmp sgt i32 %23, 0
  %or.cond.i.17 = and i1 %cmp.88.i, %cmp2.86.i
  br i1 %or.cond.i.17, label %for.cond.1.preheader.lr.ph.split.us.i, label %multiplyMatrices.exit

for.cond.1.preheader.lr.ph.split.us.i:            ; preds = %enterData.exit
  %24 = add i32 %23, -1
  %25 = zext i32 %24 to i64
  %26 = shl nuw nsw i64 %25, 2
  %27 = add nuw nsw i64 %26, 4
  %28 = add i32 %21, -1
  %xtraiter = and i32 %21, 7
  %lcmp.mod = icmp eq i32 %xtraiter, 0
  br i1 %lcmp.mod, label %for.cond.1.preheader.lr.ph.split.us.i.split, label %for.inc.6.us.i.prol.preheader

for.inc.6.us.i.prol.preheader:                    ; preds = %for.cond.1.preheader.lr.ph.split.us.i
  br label %for.inc.6.us.i.prol

for.inc.6.us.i.prol:                              ; preds = %for.inc.6.us.i.prol.preheader, %for.inc.6.us.i.prol
  %indvar.i.prol = phi i64 [ %indvar.next.i.prol, %for.inc.6.us.i.prol ], [ 0, %for.inc.6.us.i.prol.preheader ]
  %prol.iter = phi i32 [ %prol.iter.sub, %for.inc.6.us.i.prol ], [ %xtraiter, %for.inc.6.us.i.prol.preheader ]
  %scevgep.i.prol = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %mult, i64 0, i64 %indvar.i.prol, i64 0
  %scevgep102.i.prol = bitcast i32* %scevgep.i.prol to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep102.i.prol, i8 0, i64 %27, i32 8, i1 false) #1
  %indvar.next.i.prol = add nuw nsw i64 %indvar.i.prol, 1
  %prol.iter.sub = add i32 %prol.iter, -1
  %prol.iter.cmp = icmp eq i32 %prol.iter.sub, 0
  br i1 %prol.iter.cmp, label %for.cond.1.preheader.lr.ph.split.us.i.split.loopexit, label %for.inc.6.us.i.prol, !llvm.loop !5

for.cond.1.preheader.lr.ph.split.us.i.split.loopexit: ; preds = %for.inc.6.us.i.prol
  %indvar.next.i.prol.lcssa = phi i64 [ %indvar.next.i.prol, %for.inc.6.us.i.prol ]
  br label %for.cond.1.preheader.lr.ph.split.us.i.split

for.cond.1.preheader.lr.ph.split.us.i.split:      ; preds = %for.cond.1.preheader.lr.ph.split.us.i.split.loopexit, %for.cond.1.preheader.lr.ph.split.us.i
  %indvar.i.unr = phi i64 [ 0, %for.cond.1.preheader.lr.ph.split.us.i ], [ %indvar.next.i.prol.lcssa, %for.cond.1.preheader.lr.ph.split.us.i.split.loopexit ]
  %29 = icmp ult i32 %28, 7
  br i1 %29, label %for.cond.15.preheader.lr.ph.us.i.preheader, label %for.cond.1.preheader.lr.ph.split.us.i.split.split

for.cond.1.preheader.lr.ph.split.us.i.split.split: ; preds = %for.cond.1.preheader.lr.ph.split.us.i.split
  br label %for.inc.6.us.i

for.inc.6.us.i:                                   ; preds = %for.inc.6.us.i, %for.cond.1.preheader.lr.ph.split.us.i.split.split
  %indvar.i = phi i64 [ %indvar.i.unr, %for.cond.1.preheader.lr.ph.split.us.i.split.split ], [ %indvar.next.i.7, %for.inc.6.us.i ]
  %scevgep.i = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %mult, i64 0, i64 %indvar.i, i64 0
  %scevgep102.i = bitcast i32* %scevgep.i to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep102.i, i8 0, i64 %27, i32 8, i1 false) #1
  %indvar.next.i = add nuw nsw i64 %indvar.i, 1
  %scevgep.i.1 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %mult, i64 0, i64 %indvar.next.i, i64 0
  %scevgep102.i.1 = bitcast i32* %scevgep.i.1 to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep102.i.1, i8 0, i64 %27, i32 8, i1 false) #1
  %indvar.next.i.1 = add nsw i64 %indvar.i, 2
  %scevgep.i.2 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %mult, i64 0, i64 %indvar.next.i.1, i64 0
  %scevgep102.i.2 = bitcast i32* %scevgep.i.2 to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep102.i.2, i8 0, i64 %27, i32 8, i1 false) #1
  %indvar.next.i.2 = add nsw i64 %indvar.i, 3
  %scevgep.i.3 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %mult, i64 0, i64 %indvar.next.i.2, i64 0
  %scevgep102.i.3 = bitcast i32* %scevgep.i.3 to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep102.i.3, i8 0, i64 %27, i32 8, i1 false) #1
  %indvar.next.i.3 = add nsw i64 %indvar.i, 4
  %scevgep.i.4 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %mult, i64 0, i64 %indvar.next.i.3, i64 0
  %scevgep102.i.4 = bitcast i32* %scevgep.i.4 to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep102.i.4, i8 0, i64 %27, i32 8, i1 false) #1
  %indvar.next.i.4 = add nsw i64 %indvar.i, 5
  %scevgep.i.5 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %mult, i64 0, i64 %indvar.next.i.4, i64 0
  %scevgep102.i.5 = bitcast i32* %scevgep.i.5 to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep102.i.5, i8 0, i64 %27, i32 8, i1 false) #1
  %indvar.next.i.5 = add nsw i64 %indvar.i, 6
  %scevgep.i.6 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %mult, i64 0, i64 %indvar.next.i.5, i64 0
  %scevgep102.i.6 = bitcast i32* %scevgep.i.6 to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep102.i.6, i8 0, i64 %27, i32 8, i1 false) #1
  %indvar.next.i.6 = add nsw i64 %indvar.i, 7
  %scevgep.i.7 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %mult, i64 0, i64 %indvar.next.i.6, i64 0
  %scevgep102.i.7 = bitcast i32* %scevgep.i.7 to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep102.i.7, i8 0, i64 %27, i32 8, i1 false) #1
  %indvar.next.i.7 = add nsw i64 %indvar.i, 8
  %lftr.wideiv37.7 = trunc i64 %indvar.next.i.7 to i32
  %exitcond38.7 = icmp eq i32 %lftr.wideiv37.7, %21
  br i1 %exitcond38.7, label %for.cond.15.preheader.lr.ph.us.i.preheader.unr-lcssa, label %for.inc.6.us.i

for.cond.15.preheader.lr.ph.us.i.preheader.unr-lcssa: ; preds = %for.inc.6.us.i
  br label %for.cond.15.preheader.lr.ph.us.i.preheader

for.cond.15.preheader.lr.ph.us.i.preheader:       ; preds = %for.cond.1.preheader.lr.ph.split.us.i.split, %for.cond.15.preheader.lr.ph.us.i.preheader.unr-lcssa
  %cmp16.60.i = icmp sgt i32 %22, 0
  br i1 %cmp16.60.i, label %for.body.17.lr.ph.us.us.i.preheader.us.preheader, label %multiplyMatrices.exit

for.body.17.lr.ph.us.us.i.preheader.us.preheader: ; preds = %for.cond.15.preheader.lr.ph.us.i.preheader
  %30 = add i32 %22, -1
  %31 = zext i32 %30 to i64
  %32 = add nuw nsw i64 %31, 1
  %end.idx = add nuw nsw i64 %31, 1
  %n.vec = and i64 %32, 8589934584
  %cmp.zero = icmp eq i64 %n.vec, 0
  br label %for.body.17.lr.ph.us.us.i.preheader.us

for.body.17.lr.ph.us.us.i.us:                     ; preds = %for.body.17.lr.ph.us.us.i.preheader.us, %for.inc.33.us.us.i.us
  %indvars.iv90.i.us = phi i64 [ %indvars.iv.next91.i.us, %for.inc.33.us.us.i.us ], [ 0, %for.body.17.lr.ph.us.us.i.preheader.us ]
  %arrayidx29.us.us.i.us = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %mult, i64 0, i64 %indvars.iv94.i.us, i64 %indvars.iv90.i.us
  %.pre.i.us = load i32, i32* %arrayidx29.us.us.i.us, align 4, !tbaa !1
  %33 = insertelement <4 x i32> <i32 undef, i32 0, i32 0, i32 0>, i32 %.pre.i.us, i32 0
  br i1 %cmp.zero, label %middle.block, label %vector.body.preheader

vector.body.preheader:                            ; preds = %for.body.17.lr.ph.us.us.i.us
  br label %vector.body

vector.body:                                      ; preds = %vector.body.preheader, %vector.body
  %index = phi i64 [ %index.next, %vector.body ], [ 0, %vector.body.preheader ]
  %vec.phi = phi <4 x i32> [ %71, %vector.body ], [ %33, %vector.body.preheader ]
  %vec.phi54 = phi <4 x i32> [ %72, %vector.body ], [ zeroinitializer, %vector.body.preheader ]
  %broadcast.splatinsert = insertelement <4 x i64> undef, i64 %index, i32 0
  %broadcast.splat = shufflevector <4 x i64> %broadcast.splatinsert, <4 x i64> undef, <4 x i32> zeroinitializer
  %induction = add <4 x i64> %broadcast.splat, <i64 0, i64 1, i64 2, i64 3>
  %induction55 = add <4 x i64> %broadcast.splat, <i64 4, i64 5, i64 6, i64 7>
  %34 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %firstMatrix, i64 0, i64 %indvars.iv94.i.us, i64 %index
  %35 = extractelement <4 x i64> %induction, i32 1
  %36 = extractelement <4 x i64> %induction, i32 2
  %37 = extractelement <4 x i64> %induction, i32 3
  %38 = extractelement <4 x i64> %induction55, i32 0
  %39 = extractelement <4 x i64> %induction55, i32 1
  %40 = extractelement <4 x i64> %induction55, i32 2
  %41 = extractelement <4 x i64> %induction55, i32 3
  %42 = bitcast i32* %34 to <4 x i32>*
  %wide.load = load <4 x i32>, <4 x i32>* %42, align 8, !tbaa !1
  %43 = getelementptr i32, i32* %34, i64 4
  %44 = bitcast i32* %43 to <4 x i32>*
  %wide.load56 = load <4 x i32>, <4 x i32>* %44, align 8, !tbaa !1
  %45 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %secondMatrix, i64 0, i64 %index, i64 %indvars.iv90.i.us
  %46 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %secondMatrix, i64 0, i64 %35, i64 %indvars.iv90.i.us
  %47 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %secondMatrix, i64 0, i64 %36, i64 %indvars.iv90.i.us
  %48 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %secondMatrix, i64 0, i64 %37, i64 %indvars.iv90.i.us
  %49 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %secondMatrix, i64 0, i64 %38, i64 %indvars.iv90.i.us
  %50 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %secondMatrix, i64 0, i64 %39, i64 %indvars.iv90.i.us
  %51 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %secondMatrix, i64 0, i64 %40, i64 %indvars.iv90.i.us
  %52 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %secondMatrix, i64 0, i64 %41, i64 %indvars.iv90.i.us
  %53 = load i32, i32* %45, align 4, !tbaa !1
  %54 = insertelement <4 x i32> undef, i32 %53, i32 0
  %55 = load i32, i32* %46, align 4, !tbaa !1
  %56 = insertelement <4 x i32> %54, i32 %55, i32 1
  %57 = load i32, i32* %47, align 4, !tbaa !1
  %58 = insertelement <4 x i32> %56, i32 %57, i32 2
  %59 = load i32, i32* %48, align 4, !tbaa !1
  %60 = insertelement <4 x i32> %58, i32 %59, i32 3
  %61 = load i32, i32* %49, align 4, !tbaa !1
  %62 = insertelement <4 x i32> undef, i32 %61, i32 0
  %63 = load i32, i32* %50, align 4, !tbaa !1
  %64 = insertelement <4 x i32> %62, i32 %63, i32 1
  %65 = load i32, i32* %51, align 4, !tbaa !1
  %66 = insertelement <4 x i32> %64, i32 %65, i32 2
  %67 = load i32, i32* %52, align 4, !tbaa !1
  %68 = insertelement <4 x i32> %66, i32 %67, i32 3
  %69 = mul nsw <4 x i32> %60, %wide.load
  %70 = mul nsw <4 x i32> %68, %wide.load56
  %71 = add nsw <4 x i32> %69, %vec.phi
  %72 = add nsw <4 x i32> %70, %vec.phi54
  %index.next = add i64 %index, 8
  %73 = icmp eq i64 %index.next, %n.vec
  br i1 %73, label %middle.block.loopexit, label %vector.body, !llvm.loop !7

middle.block.loopexit:                            ; preds = %vector.body
  %.lcssa66 = phi <4 x i32> [ %72, %vector.body ]
  %.lcssa = phi <4 x i32> [ %71, %vector.body ]
  br label %middle.block

middle.block:                                     ; preds = %middle.block.loopexit, %for.body.17.lr.ph.us.us.i.us
  %resume.val = phi i64 [ 0, %for.body.17.lr.ph.us.us.i.us ], [ %n.vec, %middle.block.loopexit ]
  %rdx.vec.exit.phi = phi <4 x i32> [ %33, %for.body.17.lr.ph.us.us.i.us ], [ %.lcssa, %middle.block.loopexit ]
  %rdx.vec.exit.phi59 = phi <4 x i32> [ zeroinitializer, %for.body.17.lr.ph.us.us.i.us ], [ %.lcssa66, %middle.block.loopexit ]
  %bin.rdx = add <4 x i32> %rdx.vec.exit.phi59, %rdx.vec.exit.phi
  %rdx.shuf = shufflevector <4 x i32> %bin.rdx, <4 x i32> undef, <4 x i32> <i32 2, i32 3, i32 undef, i32 undef>
  %bin.rdx60 = add <4 x i32> %bin.rdx, %rdx.shuf
  %rdx.shuf61 = shufflevector <4 x i32> %bin.rdx60, <4 x i32> undef, <4 x i32> <i32 1, i32 undef, i32 undef, i32 undef>
  %bin.rdx62 = add <4 x i32> %bin.rdx60, %rdx.shuf61
  %74 = extractelement <4 x i32> %bin.rdx62, i32 0
  %cmp.n = icmp eq i64 %end.idx, %resume.val
  br i1 %cmp.n, label %for.inc.33.us.us.i.us, label %for.body.17.us.us.i.us.preheader

for.body.17.us.us.i.us.preheader:                 ; preds = %middle.block
  br label %for.body.17.us.us.i.us

for.body.17.us.us.i.us:                           ; preds = %for.body.17.us.us.i.us.preheader, %for.body.17.us.us.i.us
  %75 = phi i32 [ %add.us.us.i.us, %for.body.17.us.us.i.us ], [ %74, %for.body.17.us.us.i.us.preheader ]
  %indvars.iv.i.18.us = phi i64 [ %indvars.iv.next.i.19.us, %for.body.17.us.us.i.us ], [ %resume.val, %for.body.17.us.us.i.us.preheader ]
  %arrayidx21.us.us.i.us = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %firstMatrix, i64 0, i64 %indvars.iv94.i.us, i64 %indvars.iv.i.18.us
  %76 = load i32, i32* %arrayidx21.us.us.i.us, align 4, !tbaa !1
  %arrayidx25.us.us.i.us = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %secondMatrix, i64 0, i64 %indvars.iv.i.18.us, i64 %indvars.iv90.i.us
  %77 = load i32, i32* %arrayidx25.us.us.i.us, align 4, !tbaa !1
  %mul.us.us.i.us = mul nsw i32 %77, %76
  %add.us.us.i.us = add nsw i32 %mul.us.us.i.us, %75
  %indvars.iv.next.i.19.us = add nuw nsw i64 %indvars.iv.i.18.us, 1
  %lftr.wideiv47 = trunc i64 %indvars.iv.next.i.19.us to i32
  %exitcond48 = icmp eq i32 %lftr.wideiv47, %22
  br i1 %exitcond48, label %for.inc.33.us.us.i.us.loopexit, label %for.body.17.us.us.i.us, !llvm.loop !10

for.inc.33.us.us.i.us.loopexit:                   ; preds = %for.body.17.us.us.i.us
  %add.us.us.i.us.lcssa67 = phi i32 [ %add.us.us.i.us, %for.body.17.us.us.i.us ]
  br label %for.inc.33.us.us.i.us

for.inc.33.us.us.i.us:                            ; preds = %for.inc.33.us.us.i.us.loopexit, %middle.block
  %add.us.us.i.us.lcssa = phi i32 [ %74, %middle.block ], [ %add.us.us.i.us.lcssa67, %for.inc.33.us.us.i.us.loopexit ]
  store i32 %add.us.us.i.us.lcssa, i32* %arrayidx29.us.us.i.us, align 4, !tbaa !1
  %indvars.iv.next91.i.us = add nuw nsw i64 %indvars.iv90.i.us, 1
  %lftr.wideiv49 = trunc i64 %indvars.iv.next91.i.us to i32
  %exitcond50 = icmp eq i32 %lftr.wideiv49, %23
  br i1 %exitcond50, label %for.inc.36.us.i.us, label %for.body.17.lr.ph.us.us.i.us

for.inc.36.us.i.us:                               ; preds = %for.inc.33.us.us.i.us
  %indvars.iv.next95.i.us = add nuw nsw i64 %indvars.iv94.i.us, 1
  %lftr.wideiv51 = trunc i64 %indvars.iv.next95.i.us to i32
  %exitcond52 = icmp eq i32 %lftr.wideiv51, %21
  br i1 %exitcond52, label %multiplyMatrices.exit.loopexit, label %for.body.17.lr.ph.us.us.i.preheader.us

for.body.17.lr.ph.us.us.i.preheader.us:           ; preds = %for.body.17.lr.ph.us.us.i.preheader.us.preheader, %for.inc.36.us.i.us
  %indvars.iv94.i.us = phi i64 [ %indvars.iv.next95.i.us, %for.inc.36.us.i.us ], [ 0, %for.body.17.lr.ph.us.us.i.preheader.us.preheader ]
  br label %for.body.17.lr.ph.us.us.i.us

multiplyMatrices.exit.loopexit:                   ; preds = %for.inc.36.us.i.us
  br label %multiplyMatrices.exit

multiplyMatrices.exit:                            ; preds = %multiplyMatrices.exit.loopexit, %for.cond.15.preheader.lr.ph.us.i.preheader, %enterData.exit
  %puts.i.21 = call i32 @puts(i8* getelementptr inbounds ([16 x i8], [16 x i8]* @str.14, i64 0, i64 0)) #1
  br i1 %cmp.88.i, label %for.cond.1.preheader.lr.ph.i, label %display.exit

for.cond.1.preheader.lr.ph.i:                     ; preds = %multiplyMatrices.exit
  %sub.i = add nsw i32 %23, -1
  br i1 %cmp2.86.i, label %for.body.3.lr.ph.us.i.27.preheader, label %display.exit

for.body.3.lr.ph.us.i.27.preheader:               ; preds = %for.cond.1.preheader.lr.ph.i
  br label %for.body.3.lr.ph.us.i.27

for.inc.9.us.i:                                   ; preds = %for.inc.us.i
  %indvars.iv.next26.i = add nuw nsw i64 %indvars.iv25.i, 1
  %lftr.wideiv33 = trunc i64 %indvars.iv.next26.i to i32
  %exitcond34 = icmp eq i32 %lftr.wideiv33, %21
  br i1 %exitcond34, label %display.exit.loopexit, label %for.body.3.lr.ph.us.i.27

for.body.3.us.i.23:                               ; preds = %for.body.3.lr.ph.us.i.27, %for.inc.us.i
  %indvars.iv.i.22 = phi i64 [ 0, %for.body.3.lr.ph.us.i.27 ], [ %indvars.iv.next.i.24, %for.inc.us.i ]
  %arrayidx5.us.i = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %mult, i64 0, i64 %indvars.iv25.i, i64 %indvars.iv.i.22
  %78 = load i32, i32* %arrayidx5.us.i, align 4, !tbaa !1
  %call6.us.i = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.10, i64 0, i64 0), i32 %78) #1
  %79 = trunc i64 %indvars.iv.i.22 to i32
  %cmp7.us.i = icmp eq i32 %79, %sub.i
  br i1 %cmp7.us.i, label %if.then.us.i, label %for.inc.us.i

if.then.us.i:                                     ; preds = %for.body.3.us.i.23
  %puts20.us.i = call i32 @puts(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @str.15, i64 0, i64 0)) #1
  br label %for.inc.us.i

for.inc.us.i:                                     ; preds = %if.then.us.i, %for.body.3.us.i.23
  %indvars.iv.next.i.24 = add nuw nsw i64 %indvars.iv.i.22, 1
  %lftr.wideiv = trunc i64 %indvars.iv.next.i.24 to i32
  %exitcond = icmp eq i32 %lftr.wideiv, %23
  br i1 %exitcond, label %for.inc.9.us.i, label %for.body.3.us.i.23

for.body.3.lr.ph.us.i.27:                         ; preds = %for.body.3.lr.ph.us.i.27.preheader, %for.inc.9.us.i
  %indvars.iv25.i = phi i64 [ %indvars.iv.next26.i, %for.inc.9.us.i ], [ 0, %for.body.3.lr.ph.us.i.27.preheader ]
  br label %for.body.3.us.i.23

display.exit.loopexit:                            ; preds = %for.inc.9.us.i
  br label %display.exit

display.exit:                                     ; preds = %display.exit.loopexit, %multiplyMatrices.exit, %for.cond.1.preheader.lr.ph.i
  call void @llvm.lifetime.end(i64 4, i8* %6) #1
  call void @llvm.lifetime.end(i64 4, i8* %5) #1
  call void @llvm.lifetime.end(i64 4, i8* %4) #1
  call void @llvm.lifetime.end(i64 4, i8* %3) #1
  call void @llvm.lifetime.end(i64 400, i8* %2) #1
  call void @llvm.lifetime.end(i64 400, i8* %1) #1
  call void @llvm.lifetime.end(i64 400, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind uwtable
define void @enterData([10 x i32]* nocapture %firstMatrix, [10 x i32]* nocapture %secondMatrix, i32 %rowFirst, i32 %columnFirst, i32 %rowSecond, i32 %columnSecond) #0 {
entry:
  %puts = tail call i32 @puts(i8* getelementptr inbounds ([29 x i8], [29 x i8]* @str.12, i64 0, i64 0))
  %cmp.62 = icmp sgt i32 %rowFirst, 0
  %cmp2.59 = icmp sgt i32 %columnFirst, 0
  %or.cond = and i1 %cmp.62, %cmp2.59
  br i1 %or.cond, label %for.body.3.lr.ph.us.preheader, label %for.end.12

for.body.3.lr.ph.us.preheader:                    ; preds = %entry
  br label %for.body.3.lr.ph.us

for.body.3.us:                                    ; preds = %for.body.3.us, %for.body.3.lr.ph.us
  %indvars.iv69 = phi i64 [ 0, %for.body.3.lr.ph.us ], [ %indvars.iv.next70, %for.body.3.us ]
  %indvars.iv.next70 = add nuw nsw i64 %indvars.iv69, 1
  %0 = trunc i64 %indvars.iv.next70 to i32
  %call5.us = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.6, i64 0, i64 0), i32 %3, i32 %0) #1
  %1 = mul nuw nsw i64 %indvars.iv.next70, %indvars.iv.next75
  %arrayidx9.us = getelementptr inbounds [10 x i32], [10 x i32]* %firstMatrix, i64 %indvars.iv74, i64 %indvars.iv69
  %2 = trunc i64 %1 to i32
  store i32 %2, i32* %arrayidx9.us, align 4, !tbaa !1
  %exitcond73 = icmp eq i32 %0, %columnFirst
  br i1 %exitcond73, label %for.cond.loopexit.us, label %for.body.3.us

for.cond.loopexit.us:                             ; preds = %for.body.3.us
  %exitcond77 = icmp eq i32 %3, %rowFirst
  br i1 %exitcond77, label %for.end.12.loopexit, label %for.body.3.lr.ph.us

for.body.3.lr.ph.us:                              ; preds = %for.body.3.lr.ph.us.preheader, %for.cond.loopexit.us
  %indvars.iv74 = phi i64 [ %indvars.iv.next75, %for.cond.loopexit.us ], [ 0, %for.body.3.lr.ph.us.preheader ]
  %indvars.iv.next75 = add nuw nsw i64 %indvars.iv74, 1
  %3 = trunc i64 %indvars.iv.next75 to i32
  br label %for.body.3.us

for.end.12.loopexit:                              ; preds = %for.cond.loopexit.us
  br label %for.end.12

for.end.12:                                       ; preds = %for.end.12.loopexit, %entry
  %puts54 = tail call i32 @puts(i8* getelementptr inbounds ([29 x i8], [29 x i8]* @str.13, i64 0, i64 0))
  %cmp15.57 = icmp sgt i32 %rowSecond, 0
  %cmp18.55 = icmp sgt i32 %columnSecond, 0
  %or.cond80 = and i1 %cmp15.57, %cmp18.55
  br i1 %or.cond80, label %for.body.19.lr.ph.us.preheader, label %for.end.35

for.body.19.lr.ph.us.preheader:                   ; preds = %for.end.12
  br label %for.body.19.lr.ph.us

for.body.19.us:                                   ; preds = %for.body.19.us, %for.body.19.lr.ph.us
  %indvars.iv = phi i64 [ 0, %for.body.19.lr.ph.us ], [ %indvars.iv.next, %for.body.19.us ]
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %4 = trunc i64 %indvars.iv.next to i32
  %call22.us = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.8, i64 0, i64 0), i32 %7, i32 %4) #1
  %5 = mul nuw nsw i64 %indvars.iv.next, %indvars.iv.next66
  %arrayidx29.us = getelementptr inbounds [10 x i32], [10 x i32]* %secondMatrix, i64 %indvars.iv65, i64 %indvars.iv
  %6 = trunc i64 %5 to i32
  store i32 %6, i32* %arrayidx29.us, align 4, !tbaa !1
  %exitcond = icmp eq i32 %4, %columnSecond
  br i1 %exitcond, label %for.cond.14.loopexit.us, label %for.body.19.us

for.cond.14.loopexit.us:                          ; preds = %for.body.19.us
  %exitcond68 = icmp eq i32 %7, %rowSecond
  br i1 %exitcond68, label %for.end.35.loopexit, label %for.body.19.lr.ph.us

for.body.19.lr.ph.us:                             ; preds = %for.body.19.lr.ph.us.preheader, %for.cond.14.loopexit.us
  %indvars.iv65 = phi i64 [ %indvars.iv.next66, %for.cond.14.loopexit.us ], [ 0, %for.body.19.lr.ph.us.preheader ]
  %indvars.iv.next66 = add nuw nsw i64 %indvars.iv65, 1
  %7 = trunc i64 %indvars.iv.next66 to i32
  br label %for.body.19.us

for.end.35.loopexit:                              ; preds = %for.cond.14.loopexit.us
  br label %for.end.35

for.end.35:                                       ; preds = %for.end.35.loopexit, %for.end.12
  ret void
}

; Function Attrs: nounwind uwtable
define void @multiplyMatrices([10 x i32]* nocapture readonly %firstMatrix, [10 x i32]* nocapture readonly %secondMatrix, [10 x i32]* nocapture %mult, i32 %rowFirst, i32 %columnFirst, i32 %rowSecond, i32 %columnSecond) #0 {
entry:
  %cmp.88 = icmp sgt i32 %rowFirst, 0
  %cmp2.86 = icmp sgt i32 %columnSecond, 0
  %or.cond = and i1 %cmp.88, %cmp2.86
  br i1 %or.cond, label %for.cond.1.preheader.lr.ph.split.us, label %for.end.38

for.cond.1.preheader.lr.ph.split.us:              ; preds = %entry
  %0 = add i32 %columnSecond, -1
  %1 = zext i32 %0 to i64
  %2 = shl nuw nsw i64 %1, 2
  %3 = add nuw nsw i64 %2, 4
  %4 = add i32 %rowFirst, -1
  %xtraiter108 = and i32 %rowFirst, 7
  %lcmp.mod109 = icmp eq i32 %xtraiter108, 0
  br i1 %lcmp.mod109, label %for.cond.1.preheader.lr.ph.split.us.split, label %for.inc.6.us.prol.preheader

for.inc.6.us.prol.preheader:                      ; preds = %for.cond.1.preheader.lr.ph.split.us
  br label %for.inc.6.us.prol

for.inc.6.us.prol:                                ; preds = %for.inc.6.us.prol.preheader, %for.inc.6.us.prol
  %indvar.prol = phi i64 [ %indvar.next.prol, %for.inc.6.us.prol ], [ 0, %for.inc.6.us.prol.preheader ]
  %prol.iter = phi i32 [ %prol.iter.sub, %for.inc.6.us.prol ], [ %xtraiter108, %for.inc.6.us.prol.preheader ]
  %scevgep.prol = getelementptr [10 x i32], [10 x i32]* %mult, i64 %indvar.prol, i64 0
  %scevgep102.prol = bitcast i32* %scevgep.prol to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep102.prol, i8 0, i64 %3, i32 4, i1 false)
  %indvar.next.prol = add nuw nsw i64 %indvar.prol, 1
  %prol.iter.sub = add i32 %prol.iter, -1
  %prol.iter.cmp = icmp eq i32 %prol.iter.sub, 0
  br i1 %prol.iter.cmp, label %for.cond.1.preheader.lr.ph.split.us.split.loopexit, label %for.inc.6.us.prol, !llvm.loop !12

for.cond.1.preheader.lr.ph.split.us.split.loopexit: ; preds = %for.inc.6.us.prol
  %indvar.next.prol.lcssa = phi i64 [ %indvar.next.prol, %for.inc.6.us.prol ]
  br label %for.cond.1.preheader.lr.ph.split.us.split

for.cond.1.preheader.lr.ph.split.us.split:        ; preds = %for.cond.1.preheader.lr.ph.split.us.split.loopexit, %for.cond.1.preheader.lr.ph.split.us
  %indvar.unr = phi i64 [ 0, %for.cond.1.preheader.lr.ph.split.us ], [ %indvar.next.prol.lcssa, %for.cond.1.preheader.lr.ph.split.us.split.loopexit ]
  %5 = icmp ult i32 %4, 7
  br i1 %5, label %for.cond.9.preheader, label %for.cond.1.preheader.lr.ph.split.us.split.split

for.cond.1.preheader.lr.ph.split.us.split.split:  ; preds = %for.cond.1.preheader.lr.ph.split.us.split
  br label %for.inc.6.us

for.inc.6.us:                                     ; preds = %for.inc.6.us, %for.cond.1.preheader.lr.ph.split.us.split.split
  %indvar = phi i64 [ %indvar.unr, %for.cond.1.preheader.lr.ph.split.us.split.split ], [ %indvar.next.7, %for.inc.6.us ]
  %scevgep = getelementptr [10 x i32], [10 x i32]* %mult, i64 %indvar, i64 0
  %scevgep102 = bitcast i32* %scevgep to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep102, i8 0, i64 %3, i32 4, i1 false)
  %indvar.next = add nuw nsw i64 %indvar, 1
  %scevgep.1 = getelementptr [10 x i32], [10 x i32]* %mult, i64 %indvar.next, i64 0
  %scevgep102.1 = bitcast i32* %scevgep.1 to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep102.1, i8 0, i64 %3, i32 4, i1 false)
  %indvar.next.1 = add nsw i64 %indvar, 2
  %scevgep.2 = getelementptr [10 x i32], [10 x i32]* %mult, i64 %indvar.next.1, i64 0
  %scevgep102.2 = bitcast i32* %scevgep.2 to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep102.2, i8 0, i64 %3, i32 4, i1 false)
  %indvar.next.2 = add nsw i64 %indvar, 3
  %scevgep.3 = getelementptr [10 x i32], [10 x i32]* %mult, i64 %indvar.next.2, i64 0
  %scevgep102.3 = bitcast i32* %scevgep.3 to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep102.3, i8 0, i64 %3, i32 4, i1 false)
  %indvar.next.3 = add nsw i64 %indvar, 4
  %scevgep.4 = getelementptr [10 x i32], [10 x i32]* %mult, i64 %indvar.next.3, i64 0
  %scevgep102.4 = bitcast i32* %scevgep.4 to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep102.4, i8 0, i64 %3, i32 4, i1 false)
  %indvar.next.4 = add nsw i64 %indvar, 5
  %scevgep.5 = getelementptr [10 x i32], [10 x i32]* %mult, i64 %indvar.next.4, i64 0
  %scevgep102.5 = bitcast i32* %scevgep.5 to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep102.5, i8 0, i64 %3, i32 4, i1 false)
  %indvar.next.5 = add nsw i64 %indvar, 6
  %scevgep.6 = getelementptr [10 x i32], [10 x i32]* %mult, i64 %indvar.next.5, i64 0
  %scevgep102.6 = bitcast i32* %scevgep.6 to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep102.6, i8 0, i64 %3, i32 4, i1 false)
  %indvar.next.6 = add nsw i64 %indvar, 7
  %scevgep.7 = getelementptr [10 x i32], [10 x i32]* %mult, i64 %indvar.next.6, i64 0
  %scevgep102.7 = bitcast i32* %scevgep.7 to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep102.7, i8 0, i64 %3, i32 4, i1 false)
  %indvar.next.7 = add nsw i64 %indvar, 8
  %lftr.wideiv103.7 = trunc i64 %indvar.next.7 to i32
  %exitcond104.7 = icmp eq i32 %lftr.wideiv103.7, %rowFirst
  br i1 %exitcond104.7, label %for.cond.9.preheader.unr-lcssa, label %for.inc.6.us

for.cond.9.preheader.unr-lcssa:                   ; preds = %for.inc.6.us
  br label %for.cond.9.preheader

for.cond.9.preheader:                             ; preds = %for.cond.1.preheader.lr.ph.split.us.split, %for.cond.9.preheader.unr-lcssa
  br i1 %cmp.88, label %for.cond.12.preheader.lr.ph, label %for.end.38

for.cond.12.preheader.lr.ph:                      ; preds = %for.cond.9.preheader
  %cmp13.62 = icmp sgt i32 %columnSecond, 0
  %cmp16.60 = icmp sgt i32 %columnFirst, 0
  br i1 %cmp13.62, label %for.cond.15.preheader.lr.ph.us.preheader, label %for.end.38

for.cond.15.preheader.lr.ph.us.preheader:         ; preds = %for.cond.12.preheader.lr.ph
  %xtraiter = and i32 %columnFirst, 1
  %lcmp.mod = icmp eq i32 %xtraiter, 0
  %6 = icmp eq i32 %columnFirst, 1
  br label %for.cond.15.preheader.lr.ph.us

for.inc.36.us.loopexit:                           ; preds = %for.inc.33.us.us
  br label %for.inc.36.us

for.inc.36.us:                                    ; preds = %for.inc.36.us.loopexit, %for.cond.15.preheader.lr.ph.us
  %indvars.iv.next95 = add nuw nsw i64 %indvars.iv94, 1
  %lftr.wideiv96 = trunc i64 %indvars.iv.next95 to i32
  %exitcond97 = icmp eq i32 %lftr.wideiv96, %rowFirst
  br i1 %exitcond97, label %for.end.38.loopexit, label %for.cond.15.preheader.lr.ph.us

for.cond.15.preheader.lr.ph.us:                   ; preds = %for.cond.15.preheader.lr.ph.us.preheader, %for.inc.36.us
  %indvars.iv94 = phi i64 [ %indvars.iv.next95, %for.inc.36.us ], [ 0, %for.cond.15.preheader.lr.ph.us.preheader ]
  br i1 %cmp16.60, label %for.body.17.lr.ph.us.us.preheader, label %for.inc.36.us

for.body.17.lr.ph.us.us.preheader:                ; preds = %for.cond.15.preheader.lr.ph.us
  %arrayidx21.us.us.prol = getelementptr inbounds [10 x i32], [10 x i32]* %firstMatrix, i64 %indvars.iv94, i64 0
  br label %for.body.17.lr.ph.us.us

for.inc.33.us.us.unr-lcssa:                       ; preds = %for.body.17.us.us
  br label %for.inc.33.us.us

for.inc.33.us.us:                                 ; preds = %for.body.17.lr.ph.us.us.split, %for.inc.33.us.us.unr-lcssa
  %indvars.iv.next91 = add nuw nsw i64 %indvars.iv90, 1
  %lftr.wideiv92 = trunc i64 %indvars.iv.next91 to i32
  %exitcond93 = icmp eq i32 %lftr.wideiv92, %columnSecond
  br i1 %exitcond93, label %for.inc.36.us.loopexit, label %for.body.17.lr.ph.us.us

for.body.17.lr.ph.us.us:                          ; preds = %for.body.17.lr.ph.us.us.preheader, %for.inc.33.us.us
  %indvars.iv90 = phi i64 [ %indvars.iv.next91, %for.inc.33.us.us ], [ 0, %for.body.17.lr.ph.us.us.preheader ]
  %arrayidx29.us.us = getelementptr inbounds [10 x i32], [10 x i32]* %mult, i64 %indvars.iv94, i64 %indvars.iv90
  %.pre = load i32, i32* %arrayidx29.us.us, align 4, !tbaa !1
  br i1 %lcmp.mod, label %for.body.17.lr.ph.us.us.split, label %for.body.17.us.us.prol

for.body.17.us.us.prol:                           ; preds = %for.body.17.lr.ph.us.us
  %7 = load i32, i32* %arrayidx21.us.us.prol, align 4, !tbaa !1
  %arrayidx25.us.us.prol = getelementptr inbounds [10 x i32], [10 x i32]* %secondMatrix, i64 0, i64 %indvars.iv90
  %8 = load i32, i32* %arrayidx25.us.us.prol, align 4, !tbaa !1
  %mul.us.us.prol = mul nsw i32 %8, %7
  %add.us.us.prol = add nsw i32 %.pre, %mul.us.us.prol
  store i32 %add.us.us.prol, i32* %arrayidx29.us.us, align 4, !tbaa !1
  br label %for.body.17.lr.ph.us.us.split

for.body.17.lr.ph.us.us.split:                    ; preds = %for.body.17.lr.ph.us.us, %for.body.17.us.us.prol
  %.unr = phi i32 [ %.pre, %for.body.17.lr.ph.us.us ], [ %add.us.us.prol, %for.body.17.us.us.prol ]
  %indvars.iv.unr = phi i64 [ 0, %for.body.17.lr.ph.us.us ], [ 1, %for.body.17.us.us.prol ]
  br i1 %6, label %for.inc.33.us.us, label %for.body.17.lr.ph.us.us.split.split

for.body.17.lr.ph.us.us.split.split:              ; preds = %for.body.17.lr.ph.us.us.split
  br label %for.body.17.us.us

for.body.17.us.us:                                ; preds = %for.body.17.us.us, %for.body.17.lr.ph.us.us.split.split
  %9 = phi i32 [ %.unr, %for.body.17.lr.ph.us.us.split.split ], [ %add.us.us.1, %for.body.17.us.us ]
  %indvars.iv = phi i64 [ %indvars.iv.unr, %for.body.17.lr.ph.us.us.split.split ], [ %indvars.iv.next.1, %for.body.17.us.us ]
  %arrayidx21.us.us = getelementptr inbounds [10 x i32], [10 x i32]* %firstMatrix, i64 %indvars.iv94, i64 %indvars.iv
  %10 = load i32, i32* %arrayidx21.us.us, align 4, !tbaa !1
  %arrayidx25.us.us = getelementptr inbounds [10 x i32], [10 x i32]* %secondMatrix, i64 %indvars.iv, i64 %indvars.iv90
  %11 = load i32, i32* %arrayidx25.us.us, align 4, !tbaa !1
  %mul.us.us = mul nsw i32 %11, %10
  %add.us.us = add nsw i32 %9, %mul.us.us
  store i32 %add.us.us, i32* %arrayidx29.us.us, align 4, !tbaa !1
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %arrayidx21.us.us.1 = getelementptr inbounds [10 x i32], [10 x i32]* %firstMatrix, i64 %indvars.iv94, i64 %indvars.iv.next
  %12 = load i32, i32* %arrayidx21.us.us.1, align 4, !tbaa !1
  %arrayidx25.us.us.1 = getelementptr inbounds [10 x i32], [10 x i32]* %secondMatrix, i64 %indvars.iv.next, i64 %indvars.iv90
  %13 = load i32, i32* %arrayidx25.us.us.1, align 4, !tbaa !1
  %mul.us.us.1 = mul nsw i32 %13, %12
  %add.us.us.1 = add nsw i32 %add.us.us, %mul.us.us.1
  store i32 %add.us.us.1, i32* %arrayidx29.us.us, align 4, !tbaa !1
  %indvars.iv.next.1 = add nsw i64 %indvars.iv, 2
  %lftr.wideiv.1 = trunc i64 %indvars.iv.next.1 to i32
  %exitcond.1 = icmp eq i32 %lftr.wideiv.1, %columnFirst
  br i1 %exitcond.1, label %for.inc.33.us.us.unr-lcssa, label %for.body.17.us.us

for.end.38.loopexit:                              ; preds = %for.inc.36.us
  br label %for.end.38

for.end.38:                                       ; preds = %for.end.38.loopexit, %entry, %for.cond.12.preheader.lr.ph, %for.cond.9.preheader
  ret void
}

; Function Attrs: nounwind uwtable
define void @display([10 x i32]* nocapture readonly %mult, i32 %rowFirst, i32 %columnSecond) #0 {
entry:
  %puts = tail call i32 @puts(i8* getelementptr inbounds ([16 x i8], [16 x i8]* @str.14, i64 0, i64 0))
  %cmp.23 = icmp sgt i32 %rowFirst, 0
  br i1 %cmp.23, label %for.cond.1.preheader.lr.ph, label %for.end.11

for.cond.1.preheader.lr.ph:                       ; preds = %entry
  %cmp2.21 = icmp sgt i32 %columnSecond, 0
  %sub = add nsw i32 %columnSecond, -1
  br i1 %cmp2.21, label %for.body.3.lr.ph.us.preheader, label %for.end.11

for.body.3.lr.ph.us.preheader:                    ; preds = %for.cond.1.preheader.lr.ph
  br label %for.body.3.lr.ph.us

for.inc.9.us:                                     ; preds = %for.inc.us
  %indvars.iv.next26 = add nuw nsw i64 %indvars.iv25, 1
  %lftr.wideiv27 = trunc i64 %indvars.iv.next26 to i32
  %exitcond28 = icmp eq i32 %lftr.wideiv27, %rowFirst
  br i1 %exitcond28, label %for.end.11.loopexit, label %for.body.3.lr.ph.us

for.body.3.us:                                    ; preds = %for.inc.us, %for.body.3.lr.ph.us
  %indvars.iv = phi i64 [ 0, %for.body.3.lr.ph.us ], [ %indvars.iv.next, %for.inc.us ]
  %arrayidx5.us = getelementptr inbounds [10 x i32], [10 x i32]* %mult, i64 %indvars.iv25, i64 %indvars.iv
  %0 = load i32, i32* %arrayidx5.us, align 4, !tbaa !1
  %call6.us = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.10, i64 0, i64 0), i32 %0) #1
  %1 = trunc i64 %indvars.iv to i32
  %cmp7.us = icmp eq i32 %1, %sub
  br i1 %cmp7.us, label %if.then.us, label %for.inc.us

if.then.us:                                       ; preds = %for.body.3.us
  %puts20.us = tail call i32 @puts(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @str.15, i64 0, i64 0))
  br label %for.inc.us

for.inc.us:                                       ; preds = %if.then.us, %for.body.3.us
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %lftr.wideiv = trunc i64 %indvars.iv.next to i32
  %exitcond = icmp eq i32 %lftr.wideiv, %columnSecond
  br i1 %exitcond, label %for.inc.9.us, label %for.body.3.us

for.body.3.lr.ph.us:                              ; preds = %for.body.3.lr.ph.us.preheader, %for.inc.9.us
  %indvars.iv25 = phi i64 [ %indvars.iv.next26, %for.inc.9.us ], [ 0, %for.body.3.lr.ph.us.preheader ]
  br label %for.body.3.us

for.end.11.loopexit:                              ; preds = %for.inc.9.us
  br label %for.end.11

for.end.11:                                       ; preds = %for.end.11.loopexit, %for.cond.1.preheader.lr.ph, %entry
  ret void
}

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @puts(i8* nocapture readonly) #1

; Function Attrs: nounwind
declare void @llvm.memset.p0i8.i64(i8* nocapture, i8, i64, i32, i1) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = distinct !{!5, !6}
!6 = !{!"llvm.loop.unroll.disable"}
!7 = distinct !{!7, !8, !9}
!8 = !{!"llvm.loop.vectorize.width", i32 1}
!9 = !{!"llvm.loop.interleave.count", i32 1}
!10 = distinct !{!10, !11, !8, !9}
!11 = !{!"llvm.loop.unroll.runtime.disable"}
!12 = distinct !{!12, !6}
