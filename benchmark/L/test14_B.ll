; ModuleID = 'test14.bc'

@.str = private constant [17 x i8] c"Enter a number: \00", align 1
@.str.1 = private constant [4 x i8] c"%lf\00", align 1
@.str.2 = private constant [15 x i8] c"You entered 0.\00", align 1
@.str.3 = private constant [31 x i8] c"You entered a negative number.\00", align 1
@.str.4 = private constant [31 x i8] c"You entered a positive number.\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_dummy11_6 = alloca i32, align 4
  %_temp_dummy12_7 = alloca i32, align 4
  %_temp_dummy13_8 = alloca i32, align 4
  %_temp_dummy14_9 = alloca i32, align 4
  %number_4 = alloca double, align 8
  %0 = bitcast i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast double* %number_4 to double*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, double* %2)
  %3 = load double, double* %number_4, align 8
  %cmp1 = fcmp ole double %3, 0.000000e+00
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load double, double* %number_4, align 8
  %cmp2 = fcmp oeq double %4, 0.000000e+00
  br i1 %cmp2, label %if.then1, label %if.else2

if.else:                                          ; preds = %entry
  %5 = bitcast i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.4, i32 0, i32 0) to i8*
  %call5 = call i32 (i8*, ...) @printf(i8* %5)
  br label %if.end3

if.then1:                                         ; preds = %if.then
  %6 = bitcast i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %6)
  br label %if.end

if.else2:                                         ; preds = %if.then
  %7 = bitcast i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.3, i32 0, i32 0) to i8*
  %call4 = call i32 (i8*, ...) @printf(i8* %7)
  br label %if.end

if.end:                                           ; preds = %if.else2, %if.then1
  br label %if.end3

if.end3:                                          ; preds = %if.end, %if.else
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
