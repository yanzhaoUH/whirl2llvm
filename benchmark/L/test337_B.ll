; ModuleID = 'test337.bc'

%struct.str = type { i8, i64, double*, [3 x i32] }

@.str = private constant [7 x i8] c"test 1\00", align 1
@.str.1 = private constant [9 x i8] c"test 1 1\00", align 1
@.str.2 = private constant [5 x i8] c"%s \0A\00", align 1
@.str.3 = private constant [7 x i8] c"test 2\00", align 1
@.str.4 = private constant [4 x i8] c"%s\0A\00", align 1
@.str.5 = private constant [4 x i8] c"%d\0A\00", align 1
@_LIB_VERSION_59 = common global i32 0, align 4
@signgam_60 = common global i32 0, align 4

define i32 @main() {
entry:
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %_temp_dummy13_13 = alloca i32, align 4
  %_temp_dummy14_15 = alloca i32, align 4
  %aa_9 = alloca [3 x i32], align 4
  %ap_6 = alloca i8*, align 1
  %array_4 = alloca [2 x i8*], align 8
  %farray_11 = alloca [3 x double], align 8
  %p_10 = alloca i32*, align 4
  %str1_12 = alloca %struct.str, align 4
  %strp_14 = alloca %struct.str*, align 8
  %arrayidx = getelementptr [2 x i8*], [2 x i8*]* %array_4, i32 0, i64 0
  store i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str, i32 0, i32 0), i8** %arrayidx, align 8
  %arrayidx1 = getelementptr [2 x i8*], [2 x i8*]* %array_4, i32 0, i64 1
  store i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.1, i32 0, i32 0), i8** %arrayidx1, align 8
  %0 = bitcast i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i32 0, i32 0) to i8*
  %arrayidx2 = getelementptr [2 x i8*], [2 x i8*]* %array_4, i32 0, i32 1
  %1 = load i8*, i8** %arrayidx2, align 8
  %2 = bitcast i8* %1 to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0, i8* %2)
  store i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.3, i32 0, i32 0), i8** %ap_6, align 8
  %3 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.4, i32 0, i32 0) to i8*
  %4 = load i8*, i8** %ap_6, align 8
  %5 = bitcast i8* %4 to i8*
  %call2 = call i32 (i8*, ...) @printf(i8* %3, i8* %5)
  %arrayAddress = getelementptr [2 x i8*], [2 x i8*]* %array_4, i32 0, i32 0
  %6 = bitcast i8** %arrayAddress to i8**
  %call3 = call i32 @_Z2f1PPc(i8** %6)
  %arrayAddress3 = getelementptr [3 x i32], [3 x i32]* %aa_9, i32 0, i32 0
  store i32* %arrayAddress3, i32** %p_10, align 8
  %c = getelementptr %struct.str, %struct.str* %str1_12, i32 0, i32 0
  %a = getelementptr %struct.str, %struct.str* %str1_12, i32 0, i32 1
  %fp = getelementptr %struct.str, %struct.str* %str1_12, i32 0, i32 2
  %array123 = getelementptr %struct.str, %struct.str* %str1_12, i32 0, i32 3
  store i64 100, i64* %a, align 8
  %c4 = getelementptr %struct.str, %struct.str* %str1_12, i32 0, i32 0
  %a5 = getelementptr %struct.str, %struct.str* %str1_12, i32 0, i32 1
  %fp6 = getelementptr %struct.str, %struct.str* %str1_12, i32 0, i32 2
  %array1237 = getelementptr %struct.str, %struct.str* %str1_12, i32 0, i32 3
  %arrayidx8 = getelementptr [3 x i32], [3 x i32]* %array1237, i32 0, i64 1
  store i32 33, i32* %arrayidx8, align 4
  %arrayAddress9 = getelementptr [3 x double], [3 x double]* %farray_11, i32 0, i32 0
  %c10 = getelementptr %struct.str, %struct.str* %str1_12, i32 0, i32 0
  %a11 = getelementptr %struct.str, %struct.str* %str1_12, i32 0, i32 1
  %fp12 = getelementptr %struct.str, %struct.str* %str1_12, i32 0, i32 2
  %array12313 = getelementptr %struct.str, %struct.str* %str1_12, i32 0, i32 3
  store double* %arrayAddress9, double** %fp12, align 8
  %7 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.5, i32 0, i32 0) to i8*
  %c14 = getelementptr %struct.str, %struct.str* %str1_12, i32 0, i32 0
  %a15 = getelementptr %struct.str, %struct.str* %str1_12, i32 0, i32 1
  %fp16 = getelementptr %struct.str, %struct.str* %str1_12, i32 0, i32 2
  %array12317 = getelementptr %struct.str, %struct.str* %str1_12, i32 0, i32 3
  %arrayidx18 = getelementptr [3 x i32], [3 x i32]* %array12317, i32 0, i64 1
  %8 = load i32, i32* %arrayidx18, align 4
  %call4 = call i32 (i8*, ...) @printf(i8* %7, i32 %8)
  store %struct.str* %str1_12, %struct.str** %strp_14, align 8
  %9 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.5, i32 0, i32 0) to i8*
  %10 = load %struct.str*, %struct.str** %strp_14, align 8
  %11 = ptrtoint %struct.str* %10 to i64
  %add = add i64 %11, 24
  %12 = inttoptr i64 %add to i32*
  %arrayidx19 = getelementptr i32, i32* %12, i32 1
  %13 = load i32, i32* %arrayidx19, align 4
  %call5 = call i32 (i8*, ...) @printf(i8* %9, i32 %13)
  ret i32 0
}

declare i32 @printf(i8*, ...)

define i32 @_Z2f1PPc(i8** %a_4) {
entry:
  %_temp_dummy15_5 = alloca i32, align 4
  %a_4.addr = alloca i8**, align 8
  store i8** %a_4, i8*** %a_4.addr, align 8
  %0 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.4, i32 0, i32 0) to i8*
  %1 = load i8**, i8*** %a_4.addr, align 8
  %pointeridx = getelementptr i8*, i8** %1, i64 1
  %2 = load i8*, i8** %pointeridx, align 8
  %3 = bitcast i8* %2 to i8*
  %call6 = call i32 (i8*, ...) @printf(i8* %0, i8* %3)
  ret i32 0
}
