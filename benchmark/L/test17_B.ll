; ModuleID = 'test17.bc'

@.str = private constant [19 x i8] c"Enter an integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [53 x i8] c"Error! Factorial of a negative number doesn't exist.\00", align 1
@.str.3 = private constant [23 x i8] c"Factorial of %d = %llu\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %factorial_6 = alloca i64, align 8
  %i_5 = alloca i32, align 4
  %n_4 = alloca i32, align 4
  store i64 1, i64* %factorial_6, align 8
  %0 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = load i32, i32* %n_4, align 4
  %cmp1 = icmp slt i32 %3, 0
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = bitcast i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %4)
  br label %if.end

if.else:                                          ; preds = %entry
  store i32 1, i32* %i_5, align 4
  br label %while.cond

if.end:                                           ; preds = %while.end, %if.then
  ret i32 0

while.cond:                                       ; preds = %while.body, %if.else
  %5 = load i32, i32* %i_5, align 4
  %6 = load i32, i32* %n_4, align 4
  %cmp2 = icmp sle i32 %5, %6
  br i1 %cmp2, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = load i32, i32* %i_5, align 4
  %conv3 = sext i32 %7 to i64
  %8 = load i64, i64* %factorial_6, align 8
  %mul = mul i64 %conv3, %8
  store i64 %mul, i64* %factorial_6, align 8
  %9 = load i32, i32* %i_5, align 4
  %add = add i32 %9, 1
  store i32 %add, i32* %i_5, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %10 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.3, i32 0, i32 0) to i8*
  %11 = load i32, i32* %n_4, align 4
  %12 = load i64, i64* %factorial_6, align 8
  %call4 = call i32 (i8*, ...) @printf(i8* %10, i32 %11, i64 %12)
  br label %if.end
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
