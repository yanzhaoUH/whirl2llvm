; ModuleID = 'test46.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [43 x i8] c"Enter total number of elements(1 to 100): \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.3 = private unnamed_addr constant [19 x i8] c"Enter Number %d\0A: \00", align 1
@.str.5 = private unnamed_addr constant [3 x i8] c"%f\00", align 1
@.str.6 = private unnamed_addr constant [23 x i8] c"Largest element = %.2f\00", align 1
@str = private unnamed_addr constant [11 x i8] c"reach here\00"

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %n = alloca i32, align 4
  %arr = alloca [100 x float], align 16
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start(i64 4, i8* %0) #1
  %1 = bitcast [100 x float]* %arr to i8*
  call void @llvm.lifetime.start(i64 400, i8* %1) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([43 x i8], [43 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %n) #1
  %putchar = call i32 @putchar(i32 10) #1
  %2 = load i32, i32* %n, align 4, !tbaa !1
  %cmp.37 = icmp sgt i32 %2, 0
  br i1 %cmp.37, label %for.body.preheader, label %entry.for.end_crit_edge

for.body.preheader:                               ; preds = %entry
  br label %for.body

entry.for.end_crit_edge:                          ; preds = %entry
  %.pre45 = getelementptr inbounds [100 x float], [100 x float]* %arr, i64 0, i64 0
  br label %for.end

for.body:                                         ; preds = %for.body.preheader, %for.body
  %indvars.iv40 = phi i64 [ %indvars.iv.next41, %for.body ], [ 0, %for.body.preheader ]
  %3 = trunc i64 %indvars.iv40 to i32
  %call3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.3, i64 0, i64 0), i32 %3) #1
  %conv = sitofp i32 %3 to float
  %arrayidx = getelementptr inbounds [100 x float], [100 x float]* %arr, i64 0, i64 %indvars.iv40
  store float %conv, float* %arrayidx, align 4, !tbaa !5
  %indvars.iv.next41 = add nuw nsw i64 %indvars.iv40, 1
  %4 = load i32, i32* %n, align 4, !tbaa !1
  %5 = sext i32 %4 to i64
  %cmp = icmp slt i64 %indvars.iv.next41, %5
  br i1 %cmp, label %for.body, label %for.cond.for.end_crit_edge

for.cond.for.end_crit_edge:                       ; preds = %for.body
  %arrayidx5.phi.trans.insert = getelementptr inbounds [100 x float], [100 x float]* %arr, i64 0, i64 11
  %.pre = load float, float* %arrayidx5.phi.trans.insert, align 4, !tbaa !5
  %arrayidx12.phi.trans.insert = getelementptr inbounds [100 x float], [100 x float]* %arr, i64 0, i64 0
  %.pre42 = load float, float* %arrayidx12.phi.trans.insert, align 16, !tbaa !5
  %phitmp = fpext float %.pre to double
  br label %for.end

for.end:                                          ; preds = %entry.for.end_crit_edge, %for.cond.for.end_crit_edge
  %arrayidx12.pre-phi = phi float* [ %.pre45, %entry.for.end_crit_edge ], [ %arrayidx12.phi.trans.insert, %for.cond.for.end_crit_edge ]
  %6 = phi float [ undef, %entry.for.end_crit_edge ], [ %.pre42, %for.cond.for.end_crit_edge ]
  %7 = phi double [ undef, %entry.for.end_crit_edge ], [ %phitmp, %for.cond.for.end_crit_edge ]
  %puts = call i32 @puts(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @str, i64 0, i64 0))
  %call7 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.5, i64 0, i64 0), double %7) #1
  %8 = load i32, i32* %n, align 4, !tbaa !1
  %cmp9.35 = icmp sgt i32 %8, 1
  br i1 %cmp9.35, label %for.body.11.lr.ph, label %for.end.22

for.body.11.lr.ph:                                ; preds = %for.end
  %9 = sext i32 %8 to i64
  %10 = and i32 %8, 1
  %lcmp.mod = icmp eq i32 %10, 0
  br i1 %lcmp.mod, label %for.body.11.prol, label %for.body.11.lr.ph.split

for.body.11.prol:                                 ; preds = %for.body.11.lr.ph
  %arrayidx14.prol = getelementptr inbounds [100 x float], [100 x float]* %arr, i64 0, i64 1
  %11 = load float, float* %arrayidx14.prol, align 4, !tbaa !5
  %cmp15.prol = fcmp olt float %6, %11
  br i1 %cmp15.prol, label %if.then.prol, label %for.inc.20.prol

if.then.prol:                                     ; preds = %for.body.11.prol
  store float %11, float* %arrayidx12.pre-phi, align 16, !tbaa !5
  br label %for.inc.20.prol

for.inc.20.prol:                                  ; preds = %if.then.prol, %for.body.11.prol
  %12 = phi float [ %11, %if.then.prol ], [ %6, %for.body.11.prol ]
  br label %for.body.11.lr.ph.split

for.body.11.lr.ph.split:                          ; preds = %for.inc.20.prol, %for.body.11.lr.ph
  %indvars.iv.unr = phi i64 [ 1, %for.body.11.lr.ph ], [ 2, %for.inc.20.prol ]
  %.pre43.unr = phi float [ %6, %for.body.11.lr.ph ], [ %12, %for.inc.20.prol ]
  %.lcssa48.unr = phi float [ undef, %for.body.11.lr.ph ], [ %12, %for.inc.20.prol ]
  %13 = icmp eq i32 %8, 2
  br i1 %13, label %for.end.22.loopexit, label %for.body.11.lr.ph.split.split

for.body.11.lr.ph.split.split:                    ; preds = %for.body.11.lr.ph.split
  br label %for.body.11

for.body.11:                                      ; preds = %for.inc.20.1, %for.body.11.lr.ph.split.split
  %indvars.iv = phi i64 [ %indvars.iv.unr, %for.body.11.lr.ph.split.split ], [ %indvars.iv.next.1, %for.inc.20.1 ]
  %.pre43 = phi float [ %.pre43.unr, %for.body.11.lr.ph.split.split ], [ %17, %for.inc.20.1 ]
  %arrayidx14 = getelementptr inbounds [100 x float], [100 x float]* %arr, i64 0, i64 %indvars.iv
  %14 = load float, float* %arrayidx14, align 4, !tbaa !5
  %cmp15 = fcmp olt float %.pre43, %14
  br i1 %cmp15, label %if.then, label %for.inc.20

if.then:                                          ; preds = %for.body.11
  store float %14, float* %arrayidx12.pre-phi, align 16, !tbaa !5
  br label %for.inc.20

for.inc.20:                                       ; preds = %for.body.11, %if.then
  %15 = phi float [ %14, %if.then ], [ %.pre43, %for.body.11 ]
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %arrayidx14.1 = getelementptr inbounds [100 x float], [100 x float]* %arr, i64 0, i64 %indvars.iv.next
  %16 = load float, float* %arrayidx14.1, align 4, !tbaa !5
  %cmp15.1 = fcmp olt float %15, %16
  br i1 %cmp15.1, label %if.then.1, label %for.inc.20.1

for.end.22.loopexit.unr-lcssa:                    ; preds = %for.inc.20.1
  %.lcssa49 = phi float [ %17, %for.inc.20.1 ]
  br label %for.end.22.loopexit

for.end.22.loopexit:                              ; preds = %for.body.11.lr.ph.split, %for.end.22.loopexit.unr-lcssa
  %.lcssa48 = phi float [ %.lcssa48.unr, %for.body.11.lr.ph.split ], [ %.lcssa49, %for.end.22.loopexit.unr-lcssa ]
  br label %for.end.22

for.end.22:                                       ; preds = %for.end.22.loopexit, %for.end
  %.lcssa = phi float [ %6, %for.end ], [ %.lcssa48, %for.end.22.loopexit ]
  %conv24 = fpext float %.lcssa to double
  %call25 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.6, i64 0, i64 0), double %conv24) #1
  call void @llvm.lifetime.end(i64 400, i8* %1) #1
  call void @llvm.lifetime.end(i64 4, i8* %0) #1
  ret i32 0

if.then.1:                                        ; preds = %for.inc.20
  store float %16, float* %arrayidx12.pre-phi, align 16, !tbaa !5
  br label %for.inc.20.1

for.inc.20.1:                                     ; preds = %if.then.1, %for.inc.20
  %17 = phi float [ %16, %if.then.1 ], [ %15, %for.inc.20 ]
  %indvars.iv.next.1 = add nsw i64 %indvars.iv, 2
  %cmp9.1 = icmp slt i64 %indvars.iv.next.1, %9
  br i1 %cmp9.1, label %for.body.11, label %for.end.22.loopexit.unr-lcssa
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @putchar(i32) #1

; Function Attrs: nounwind
declare i32 @puts(i8* nocapture readonly) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = !{!6, !6, i64 0}
!6 = !{!"float", !3, i64 0}
