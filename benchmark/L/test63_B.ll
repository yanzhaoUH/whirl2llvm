; ModuleID = 'test63.bc'

%struct.Distance = type { i32, float }

@.str = private constant [36 x i8] c"Enter information for 1st distance\0A\00", align 1
@.str.1 = private constant [13 x i8] c"Enter feet: \00", align 1
@.str.2 = private constant [3 x i8] c"%d\00", align 1
@.str.3 = private constant [13 x i8] c"Enter inch: \00", align 1
@.str.4 = private constant [3 x i8] c"%f\00", align 1
@.str.5 = private constant [37 x i8] c"\0AEnter information for 2nd distance\0A\00", align 1
@.str.6 = private constant [30 x i8] c"\0ASum of distances = %d'-%.1f\22\00", align 1
@d1_56 = common global %struct.Distance zeroinitializer, align 4
@d2_60 = common global %struct.Distance zeroinitializer, align 4
@sumOfDistances_61 = common global %struct.Distance zeroinitializer, align 4

define i32 @main() {
entry:
  %_temp_dummy10_4 = alloca i32, align 4
  %_temp_dummy110_14 = alloca i32, align 4
  %_temp_dummy11_5 = alloca i32, align 4
  %_temp_dummy12_6 = alloca i32, align 4
  %_temp_dummy13_7 = alloca i32, align 4
  %_temp_dummy14_8 = alloca i32, align 4
  %_temp_dummy15_9 = alloca i32, align 4
  %_temp_dummy16_10 = alloca i32, align 4
  %_temp_dummy17_11 = alloca i32, align 4
  %_temp_dummy18_12 = alloca i32, align 4
  %_temp_dummy19_13 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call2 = call i32 (i8*, ...) @printf(i8* %1)
  %2 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0) to i8*
  %feet = getelementptr %struct.Distance, %struct.Distance* @d1_56, i32 0, i32 0
  %inch = getelementptr %struct.Distance, %struct.Distance* @d1_56, i32 0, i32 1
  %3 = bitcast i32* %feet to i32*
  %call3 = call i32 (i8*, ...) @scanf(i8* %2, i32* %3)
  %4 = bitcast i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.3, i32 0, i32 0) to i8*
  %call4 = call i32 (i8*, ...) @printf(i8* %4)
  %5 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.4, i32 0, i32 0) to i8*
  %feet1 = getelementptr %struct.Distance, %struct.Distance* @d1_56, i32 0, i32 0
  %inch2 = getelementptr %struct.Distance, %struct.Distance* @d1_56, i32 0, i32 1
  %6 = bitcast float* %inch2 to float*
  %call5 = call i32 (i8*, ...) @scanf(i8* %5, float* %6)
  %7 = bitcast i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.5, i32 0, i32 0) to i8*
  %call6 = call i32 (i8*, ...) @printf(i8* %7)
  %8 = bitcast i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call7 = call i32 (i8*, ...) @printf(i8* %8)
  %9 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0) to i8*
  %feet3 = getelementptr %struct.Distance, %struct.Distance* @d2_60, i32 0, i32 0
  %inch4 = getelementptr %struct.Distance, %struct.Distance* @d2_60, i32 0, i32 1
  %10 = bitcast i32* %feet3 to i32*
  %call8 = call i32 (i8*, ...) @scanf(i8* %9, i32* %10)
  %11 = bitcast i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.3, i32 0, i32 0) to i8*
  %call9 = call i32 (i8*, ...) @printf(i8* %11)
  %12 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.4, i32 0, i32 0) to i8*
  %feet5 = getelementptr %struct.Distance, %struct.Distance* @d2_60, i32 0, i32 0
  %inch6 = getelementptr %struct.Distance, %struct.Distance* @d2_60, i32 0, i32 1
  %13 = bitcast float* %inch6 to float*
  %call10 = call i32 (i8*, ...) @scanf(i8* %12, float* %13)
  %feet7 = getelementptr %struct.Distance, %struct.Distance* @d1_56, i32 0, i32 0
  %inch8 = getelementptr %struct.Distance, %struct.Distance* @d1_56, i32 0, i32 1
  %14 = load i32, i32* %feet7, align 4
  %feet9 = getelementptr %struct.Distance, %struct.Distance* @d2_60, i32 0, i32 0
  %inch10 = getelementptr %struct.Distance, %struct.Distance* @d2_60, i32 0, i32 1
  %15 = load i32, i32* %feet9, align 4
  %add = add i32 %14, %15
  %feet11 = getelementptr %struct.Distance, %struct.Distance* @sumOfDistances_61, i32 0, i32 0
  %inch12 = getelementptr %struct.Distance, %struct.Distance* @sumOfDistances_61, i32 0, i32 1
  store i32 %add, i32* %feet11, align 4
  %feet13 = getelementptr %struct.Distance, %struct.Distance* @d1_56, i32 0, i32 0
  %inch14 = getelementptr %struct.Distance, %struct.Distance* @d1_56, i32 0, i32 1
  %16 = load float, float* %inch14, align 4
  %feet15 = getelementptr %struct.Distance, %struct.Distance* @d2_60, i32 0, i32 0
  %inch16 = getelementptr %struct.Distance, %struct.Distance* @d2_60, i32 0, i32 1
  %17 = load float, float* %inch16, align 4
  %add.17 = fadd float %16, %17
  %feet18 = getelementptr %struct.Distance, %struct.Distance* @sumOfDistances_61, i32 0, i32 0
  %inch19 = getelementptr %struct.Distance, %struct.Distance* @sumOfDistances_61, i32 0, i32 1
  store float %add.17, float* %inch19, align 4
  %feet20 = getelementptr %struct.Distance, %struct.Distance* @sumOfDistances_61, i32 0, i32 0
  %inch21 = getelementptr %struct.Distance, %struct.Distance* @sumOfDistances_61, i32 0, i32 1
  %18 = load float, float* %inch21, align 4
  %cmp1 = fcmp ogt float %18, 1.200000e+01
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %feet22 = getelementptr %struct.Distance, %struct.Distance* @sumOfDistances_61, i32 0, i32 0
  %inch23 = getelementptr %struct.Distance, %struct.Distance* @sumOfDistances_61, i32 0, i32 1
  %19 = load float, float* %inch23, align 4
  %add.24 = fsub float %19, 1.200000e+01
  %feet25 = getelementptr %struct.Distance, %struct.Distance* @sumOfDistances_61, i32 0, i32 0
  %inch26 = getelementptr %struct.Distance, %struct.Distance* @sumOfDistances_61, i32 0, i32 1
  store float %add.24, float* %inch26, align 4
  %feet27 = getelementptr %struct.Distance, %struct.Distance* @sumOfDistances_61, i32 0, i32 0
  %inch28 = getelementptr %struct.Distance, %struct.Distance* @sumOfDistances_61, i32 0, i32 1
  %20 = load i32, i32* %feet27, align 4
  %add.29 = add i32 %20, 1
  %feet30 = getelementptr %struct.Distance, %struct.Distance* @sumOfDistances_61, i32 0, i32 0
  %inch31 = getelementptr %struct.Distance, %struct.Distance* @sumOfDistances_61, i32 0, i32 1
  store i32 %add.29, i32* %feet30, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %21 = bitcast i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.6, i32 0, i32 0) to i8*
  %feet32 = getelementptr %struct.Distance, %struct.Distance* @sumOfDistances_61, i32 0, i32 0
  %inch33 = getelementptr %struct.Distance, %struct.Distance* @sumOfDistances_61, i32 0, i32 1
  %22 = load i32, i32* %feet32, align 4
  %feet34 = getelementptr %struct.Distance, %struct.Distance* @sumOfDistances_61, i32 0, i32 0
  %inch35 = getelementptr %struct.Distance, %struct.Distance* @sumOfDistances_61, i32 0, i32 1
  %23 = load float, float* %inch35, align 4
  %convDouble = fpext float %23 to double
  %call11 = call i32 (i8*, ...) @printf(i8* %21, i32 %22, double %convDouble)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
