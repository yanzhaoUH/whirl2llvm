; ModuleID = 'test49.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [41 x i8] c"Enter rows and column for first matrix: \00", align 1
@.str.1 = private unnamed_addr constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private unnamed_addr constant [42 x i8] c"Enter rows and column for second matrix: \00", align 1
@.str.3 = private unnamed_addr constant [60 x i8] c"Error! column of first matrix not equal to row of second.\0A\0A\00", align 1
@.str.4 = private unnamed_addr constant [30 x i8] c"\0AEnter elements of matrix 1:\0A\00", align 1
@.str.5 = private unnamed_addr constant [23 x i8] c"Enter elements a%d%d: \00", align 1
@.str.6 = private unnamed_addr constant [30 x i8] c"\0AEnter elements of matrix 2:\0A\00", align 1
@.str.7 = private unnamed_addr constant [23 x i8] c"Enter elements b%d%d: \00", align 1
@.str.8 = private unnamed_addr constant [17 x i8] c"\0AOutput Matrix:\0A\00", align 1
@.str.9 = private unnamed_addr constant [5 x i8] c"%d  \00", align 1
@.str.10 = private unnamed_addr constant [3 x i8] c"\0A\0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %a = alloca [10 x [10 x i32]], align 16
  %b = alloca [10 x [10 x i32]], align 16
  %result = alloca [10 x [10 x i32]], align 16
  %r1 = alloca i32, align 4
  %c1 = alloca i32, align 4
  %r2 = alloca i32, align 4
  %c2 = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str, i32 0, i32 0))
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0), i32* %r1, i32* %c1)
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.2, i32 0, i32 0))
  %call3 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0), i32* %r2, i32* %c2)
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %c1, align 4
  %1 = load i32, i32* %r2, align 4
  %cmp = icmp ne i32 %0, %1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([60 x i8], [60 x i8]* @.str.3, i32 0, i32 0))
  %call5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str, i32 0, i32 0))
  %call6 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0), i32* %r1, i32* %c1)
  %call7 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.2, i32 0, i32 0))
  %call8 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0), i32* %r2, i32* %c2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %call9 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.4, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc.20, %while.end
  %2 = load i32, i32* %i, align 4
  %3 = load i32, i32* %r1, align 4
  %cmp10 = icmp slt i32 %2, %3
  br i1 %cmp10, label %for.body, label %for.end.22

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4
  br label %for.cond.11

for.cond.11:                                      ; preds = %for.inc, %for.body
  %4 = load i32, i32* %j, align 4
  %5 = load i32, i32* %c1, align 4
  %cmp12 = icmp slt i32 %4, %5
  br i1 %cmp12, label %for.body.13, label %for.end

for.body.13:                                      ; preds = %for.cond.11
  %6 = load i32, i32* %i, align 4
  %add = add nsw i32 %6, 1
  %7 = load i32, i32* %j, align 4
  %add14 = add nsw i32 %7, 1
  %call15 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.5, i32 0, i32 0), i32 %add, i32 %add14)
  %8 = load i32, i32* %i, align 4
  %add16 = add nsw i32 %8, 1
  %9 = load i32, i32* %j, align 4
  %add17 = add nsw i32 %9, 1
  %mul = mul nsw i32 %add16, %add17
  %10 = load i32, i32* %j, align 4
  %idxprom = sext i32 %10 to i64
  %11 = load i32, i32* %i, align 4
  %idxprom18 = sext i32 %11 to i64
  %arrayidx = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %a, i32 0, i64 %idxprom18
  %arrayidx19 = getelementptr inbounds [10 x i32], [10 x i32]* %arrayidx, i32 0, i64 %idxprom
  store i32 %mul, i32* %arrayidx19, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body.13
  %12 = load i32, i32* %j, align 4
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond.11

for.end:                                          ; preds = %for.cond.11
  br label %for.inc.20

for.inc.20:                                       ; preds = %for.end
  %13 = load i32, i32* %i, align 4
  %inc21 = add nsw i32 %13, 1
  store i32 %inc21, i32* %i, align 4
  br label %for.cond

for.end.22:                                       ; preds = %for.cond
  %call23 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.6, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond.24

for.cond.24:                                      ; preds = %for.inc.43, %for.end.22
  %14 = load i32, i32* %i, align 4
  %15 = load i32, i32* %r2, align 4
  %cmp25 = icmp slt i32 %14, %15
  br i1 %cmp25, label %for.body.26, label %for.end.45

for.body.26:                                      ; preds = %for.cond.24
  store i32 0, i32* %j, align 4
  br label %for.cond.27

for.cond.27:                                      ; preds = %for.inc.40, %for.body.26
  %16 = load i32, i32* %j, align 4
  %17 = load i32, i32* %c2, align 4
  %cmp28 = icmp slt i32 %16, %17
  br i1 %cmp28, label %for.body.29, label %for.end.42

for.body.29:                                      ; preds = %for.cond.27
  %18 = load i32, i32* %i, align 4
  %add30 = add nsw i32 %18, 1
  %19 = load i32, i32* %j, align 4
  %add31 = add nsw i32 %19, 1
  %call32 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.7, i32 0, i32 0), i32 %add30, i32 %add31)
  %20 = load i32, i32* %i, align 4
  %add33 = add nsw i32 %20, 1
  %21 = load i32, i32* %j, align 4
  %add34 = add nsw i32 %21, 1
  %mul35 = mul nsw i32 %add33, %add34
  %22 = load i32, i32* %j, align 4
  %idxprom36 = sext i32 %22 to i64
  %23 = load i32, i32* %i, align 4
  %idxprom37 = sext i32 %23 to i64
  %arrayidx38 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %b, i32 0, i64 %idxprom37
  %arrayidx39 = getelementptr inbounds [10 x i32], [10 x i32]* %arrayidx38, i32 0, i64 %idxprom36
  store i32 %mul35, i32* %arrayidx39, align 4
  br label %for.inc.40

for.inc.40:                                       ; preds = %for.body.29
  %24 = load i32, i32* %j, align 4
  %inc41 = add nsw i32 %24, 1
  store i32 %inc41, i32* %j, align 4
  br label %for.cond.27

for.end.42:                                       ; preds = %for.cond.27
  br label %for.inc.43

for.inc.43:                                       ; preds = %for.end.42
  %25 = load i32, i32* %i, align 4
  %inc44 = add nsw i32 %25, 1
  store i32 %inc44, i32* %i, align 4
  br label %for.cond.24

for.end.45:                                       ; preds = %for.cond.24
  store i32 0, i32* %i, align 4
  br label %for.cond.46

for.cond.46:                                      ; preds = %for.inc.59, %for.end.45
  %26 = load i32, i32* %i, align 4
  %27 = load i32, i32* %r1, align 4
  %cmp47 = icmp slt i32 %26, %27
  br i1 %cmp47, label %for.body.48, label %for.end.61

for.body.48:                                      ; preds = %for.cond.46
  store i32 0, i32* %j, align 4
  br label %for.cond.49

for.cond.49:                                      ; preds = %for.inc.56, %for.body.48
  %28 = load i32, i32* %j, align 4
  %29 = load i32, i32* %c2, align 4
  %cmp50 = icmp slt i32 %28, %29
  br i1 %cmp50, label %for.body.51, label %for.end.58

for.body.51:                                      ; preds = %for.cond.49
  %30 = load i32, i32* %j, align 4
  %idxprom52 = sext i32 %30 to i64
  %31 = load i32, i32* %i, align 4
  %idxprom53 = sext i32 %31 to i64
  %arrayidx54 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %result, i32 0, i64 %idxprom53
  %arrayidx55 = getelementptr inbounds [10 x i32], [10 x i32]* %arrayidx54, i32 0, i64 %idxprom52
  store i32 0, i32* %arrayidx55, align 4
  br label %for.inc.56

for.inc.56:                                       ; preds = %for.body.51
  %32 = load i32, i32* %j, align 4
  %inc57 = add nsw i32 %32, 1
  store i32 %inc57, i32* %j, align 4
  br label %for.cond.49

for.end.58:                                       ; preds = %for.cond.49
  br label %for.inc.59

for.inc.59:                                       ; preds = %for.end.58
  %33 = load i32, i32* %i, align 4
  %inc60 = add nsw i32 %33, 1
  store i32 %inc60, i32* %i, align 4
  br label %for.cond.46

for.end.61:                                       ; preds = %for.cond.46
  store i32 0, i32* %i, align 4
  br label %for.cond.62

for.cond.62:                                      ; preds = %for.inc.91, %for.end.61
  %34 = load i32, i32* %i, align 4
  %35 = load i32, i32* %r1, align 4
  %cmp63 = icmp slt i32 %34, %35
  br i1 %cmp63, label %for.body.64, label %for.end.93

for.body.64:                                      ; preds = %for.cond.62
  store i32 0, i32* %j, align 4
  br label %for.cond.65

for.cond.65:                                      ; preds = %for.inc.88, %for.body.64
  %36 = load i32, i32* %j, align 4
  %37 = load i32, i32* %c2, align 4
  %cmp66 = icmp slt i32 %36, %37
  br i1 %cmp66, label %for.body.67, label %for.end.90

for.body.67:                                      ; preds = %for.cond.65
  store i32 0, i32* %k, align 4
  br label %for.cond.68

for.cond.68:                                      ; preds = %for.inc.85, %for.body.67
  %38 = load i32, i32* %k, align 4
  %39 = load i32, i32* %c1, align 4
  %cmp69 = icmp slt i32 %38, %39
  br i1 %cmp69, label %for.body.70, label %for.end.87

for.body.70:                                      ; preds = %for.cond.68
  %40 = load i32, i32* %k, align 4
  %idxprom71 = sext i32 %40 to i64
  %41 = load i32, i32* %i, align 4
  %idxprom72 = sext i32 %41 to i64
  %arrayidx73 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %a, i32 0, i64 %idxprom72
  %arrayidx74 = getelementptr inbounds [10 x i32], [10 x i32]* %arrayidx73, i32 0, i64 %idxprom71
  %42 = load i32, i32* %arrayidx74, align 4
  %43 = load i32, i32* %j, align 4
  %idxprom75 = sext i32 %43 to i64
  %44 = load i32, i32* %k, align 4
  %idxprom76 = sext i32 %44 to i64
  %arrayidx77 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %b, i32 0, i64 %idxprom76
  %arrayidx78 = getelementptr inbounds [10 x i32], [10 x i32]* %arrayidx77, i32 0, i64 %idxprom75
  %45 = load i32, i32* %arrayidx78, align 4
  %mul79 = mul nsw i32 %42, %45
  %46 = load i32, i32* %j, align 4
  %idxprom80 = sext i32 %46 to i64
  %47 = load i32, i32* %i, align 4
  %idxprom81 = sext i32 %47 to i64
  %arrayidx82 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %result, i32 0, i64 %idxprom81
  %arrayidx83 = getelementptr inbounds [10 x i32], [10 x i32]* %arrayidx82, i32 0, i64 %idxprom80
  %48 = load i32, i32* %arrayidx83, align 4
  %add84 = add nsw i32 %48, %mul79
  store i32 %add84, i32* %arrayidx83, align 4
  br label %for.inc.85

for.inc.85:                                       ; preds = %for.body.70
  %49 = load i32, i32* %k, align 4
  %inc86 = add nsw i32 %49, 1
  store i32 %inc86, i32* %k, align 4
  br label %for.cond.68

for.end.87:                                       ; preds = %for.cond.68
  br label %for.inc.88

for.inc.88:                                       ; preds = %for.end.87
  %50 = load i32, i32* %j, align 4
  %inc89 = add nsw i32 %50, 1
  store i32 %inc89, i32* %j, align 4
  br label %for.cond.65

for.end.90:                                       ; preds = %for.cond.65
  br label %for.inc.91

for.inc.91:                                       ; preds = %for.end.90
  %51 = load i32, i32* %i, align 4
  %inc92 = add nsw i32 %51, 1
  store i32 %inc92, i32* %i, align 4
  br label %for.cond.62

for.end.93:                                       ; preds = %for.cond.62
  %call94 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.8, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond.95

for.cond.95:                                      ; preds = %for.inc.111, %for.end.93
  %52 = load i32, i32* %i, align 4
  %53 = load i32, i32* %r1, align 4
  %cmp96 = icmp slt i32 %52, %53
  br i1 %cmp96, label %for.body.97, label %for.end.113

for.body.97:                                      ; preds = %for.cond.95
  store i32 0, i32* %j, align 4
  br label %for.cond.98

for.cond.98:                                      ; preds = %for.inc.108, %for.body.97
  %54 = load i32, i32* %j, align 4
  %55 = load i32, i32* %c2, align 4
  %cmp99 = icmp slt i32 %54, %55
  br i1 %cmp99, label %for.body.100, label %for.end.110

for.body.100:                                     ; preds = %for.cond.98
  %56 = load i32, i32* %j, align 4
  %idxprom101 = sext i32 %56 to i64
  %57 = load i32, i32* %i, align 4
  %idxprom102 = sext i32 %57 to i64
  %arrayidx103 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %result, i32 0, i64 %idxprom102
  %arrayidx104 = getelementptr inbounds [10 x i32], [10 x i32]* %arrayidx103, i32 0, i64 %idxprom101
  %58 = load i32, i32* %arrayidx104, align 4
  %call105 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.9, i32 0, i32 0), i32 %58)
  %59 = load i32, i32* %j, align 4
  %60 = load i32, i32* %c2, align 4
  %sub = sub nsw i32 %60, 1
  %cmp106 = icmp eq i32 %59, %sub
  br i1 %cmp106, label %if.then, label %if.end

if.then:                                          ; preds = %for.body.100
  %call107 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.10, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body.100
  br label %for.inc.108

for.inc.108:                                      ; preds = %if.end
  %61 = load i32, i32* %j, align 4
  %inc109 = add nsw i32 %61, 1
  store i32 %inc109, i32* %j, align 4
  br label %for.cond.98

for.end.110:                                      ; preds = %for.cond.98
  br label %for.inc.111

for.inc.111:                                      ; preds = %for.end.110
  %62 = load i32, i32* %i, align 4
  %inc112 = add nsw i32 %62, 1
  store i32 %inc112, i32* %i, align 4
  br label %for.cond.95

for.end.113:                                      ; preds = %for.cond.95
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @__isoc99_scanf(i8*, ...) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
