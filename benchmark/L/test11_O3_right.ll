; ModuleID = 'test11.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [12 x i8] c"%lf %lf %lf\00", align 1
@.str.1 = private unnamed_addr constant [28 x i8] c"%.2f is the largest number.\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %n1 = alloca double, align 8
  %n2 = alloca double, align 8
  %n3 = alloca double, align 8
  %0 = bitcast double* %n1 to i8*
  call void @llvm.lifetime.start(i64 8, i8* %0) #1
  %1 = bitcast double* %n2 to i8*
  call void @llvm.lifetime.start(i64 8, i8* %1) #1
  %2 = bitcast double* %n3 to i8*
  call void @llvm.lifetime.start(i64 8, i8* %2) #1
  %call = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i64 0, i64 0), double* nonnull %n1, double* nonnull %n2, double* nonnull %n3) #1
  %3 = load double, double* %n1, align 8, !tbaa !1
  %4 = load double, double* %n2, align 8, !tbaa !1
  %cmp = fcmp ult double %3, %4
  %5 = load double, double* %n3, align 8
  %cmp1 = fcmp ult double %3, %5
  %or.cond = or i1 %cmp, %cmp1
  br i1 %or.cond, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.1, i64 0, i64 0), double %3) #1
  %.pre = load double, double* %n2, align 8, !tbaa !1
  %.pre17 = load double, double* %n1, align 8, !tbaa !1
  %.pre18 = load double, double* %n3, align 8
  br label %if.end

if.end:                                           ; preds = %entry, %if.then
  %6 = phi double [ %5, %entry ], [ %.pre18, %if.then ]
  %7 = phi double [ %3, %entry ], [ %.pre17, %if.then ]
  %8 = phi double [ %4, %entry ], [ %.pre, %if.then ]
  %cmp3 = fcmp ult double %8, %7
  %cmp5 = fcmp ult double %8, %6
  %or.cond15 = or i1 %cmp3, %cmp5
  br i1 %or.cond15, label %if.end.8, label %if.then.6

if.then.6:                                        ; preds = %if.end
  %call7 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.1, i64 0, i64 0), double %8) #1
  %.pre19 = load double, double* %n3, align 8, !tbaa !1
  %.pre20 = load double, double* %n1, align 8, !tbaa !1
  %.pre21 = load double, double* %n2, align 8
  br label %if.end.8

if.end.8:                                         ; preds = %if.end, %if.then.6
  %9 = phi double [ %8, %if.end ], [ %.pre21, %if.then.6 ]
  %10 = phi double [ %7, %if.end ], [ %.pre20, %if.then.6 ]
  %11 = phi double [ %6, %if.end ], [ %.pre19, %if.then.6 ]
  %cmp9 = fcmp ult double %11, %10
  %cmp11 = fcmp ult double %11, %9
  %or.cond16 = or i1 %cmp9, %cmp11
  br i1 %or.cond16, label %if.end.14, label %if.then.12

if.then.12:                                       ; preds = %if.end.8
  %call13 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.1, i64 0, i64 0), double %11) #1
  br label %if.end.14

if.end.14:                                        ; preds = %if.end.8, %if.then.12
  call void @llvm.lifetime.end(i64 8, i8* %2) #1
  call void @llvm.lifetime.end(i64 8, i8* %1) #1
  call void @llvm.lifetime.end(i64 8, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"double", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
