; ModuleID = 'test36.bc'

@.str = private constant [27 x i8] c"Enter a positive integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [14 x i8] c"%d = %d + %d\0A\00", align 1
@.str.3 = private constant [55 x i8] c"%d can't be expressed as the sum of two prime numbers.\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %flag_6 = alloca i32, align 4
  %i_5 = alloca i32, align 4
  %n_4 = alloca i32, align 4
  store i32 0, i32* %flag_6, align 4
  %0 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  store i32 2, i32* %i_5, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end4, %entry
  %3 = load i32, i32* %n_4, align 4
  %div = sdiv i32 %3, 2
  %4 = load i32, i32* %i_5, align 4
  %cmp1 = icmp sge i32 %div, %4
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = load i32, i32* %i_5, align 4
  %call3 = call i32 @_Z10checkPrimei(i32 %5)
  %cmp2 = icmp eq i32 %call3, 1
  br i1 %cmp2, label %if.then, label %if.else

while.end:                                        ; preds = %while.cond
  %6 = load i32, i32* %flag_6, align 4
  %cmp4 = icmp eq i32 %6, 0
  br i1 %cmp4, label %if.then6, label %if.else7

if.then:                                          ; preds = %while.body
  %7 = load i32, i32* %n_4, align 4
  %8 = load i32, i32* %i_5, align 4
  %add = sub i32 %7, %8
  %call4 = call i32 @_Z10checkPrimei(i32 %add)
  %cmp3 = icmp eq i32 %call4, 1
  br i1 %cmp3, label %if.then1, label %if.else2

if.else:                                          ; preds = %while.body
  br label %if.end4

if.then1:                                         ; preds = %if.then
  %9 = bitcast i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.2, i32 0, i32 0) to i8*
  %10 = load i32, i32* %n_4, align 4
  %11 = load i32, i32* %i_5, align 4
  %12 = load i32, i32* %n_4, align 4
  %13 = load i32, i32* %i_5, align 4
  %add.3 = sub i32 %12, %13
  %call5 = call i32 (i8*, ...) @printf(i8* %9, i32 %10, i32 %11, i32 %add.3)
  store i32 1, i32* %flag_6, align 4
  br label %if.end

if.else2:                                         ; preds = %if.then
  br label %if.end

if.end:                                           ; preds = %if.else2, %if.then1
  br label %if.end4

if.end4:                                          ; preds = %if.end, %if.else
  %14 = load i32, i32* %i_5, align 4
  %add.5 = add i32 %14, 1
  store i32 %add.5, i32* %i_5, align 4
  br label %while.cond

if.then6:                                         ; preds = %while.end
  %15 = bitcast i8* getelementptr inbounds ([55 x i8], [55 x i8]* @.str.3, i32 0, i32 0) to i8*
  %16 = load i32, i32* %n_4, align 4
  %call6 = call i32 (i8*, ...) @printf(i8* %15, i32 %16)
  br label %if.end8

if.else7:                                         ; preds = %while.end
  br label %if.end8

if.end8:                                          ; preds = %if.else7, %if.then6
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define i32 @_Z10checkPrimei(i32 %n_4) {
entry:
  %flag_6 = alloca i32, align 4
  %i_5 = alloca i32, align 4
  %n_4.addr = alloca i32, align 4
  store i32 %n_4, i32* %n_4.addr, align 4
  store i32 1, i32* %flag_6, align 4
  store i32 2, i32* %i_5, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %0 = load i32, i32* %n_4.addr, align 4
  %div = sdiv i32 %0, 2
  %1 = load i32, i32* %i_5, align 4
  %cmp5 = icmp sge i32 %div, %1
  br i1 %cmp5, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load i32, i32* %n_4.addr, align 4
  %3 = load i32, i32* %i_5, align 4
  %srem = srem i32 %2, %3
  %cmp6 = icmp eq i32 %srem, 0
  br i1 %cmp6, label %if.then, label %if.else

while.end:                                        ; preds = %while.cond
  %4 = load i32, i32* %flag_6, align 4
  ret i32 %4

if.then:                                          ; preds = %while.body
  store i32 0, i32* %flag_6, align 4
  br label %if.end

if.else:                                          ; preds = %while.body
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %5 = load i32, i32* %i_5, align 4
  %add = add i32 %5, 1
  store i32 %add, i32* %i_5, align 4
  br label %while.cond
}
