; ModuleID = 'test59.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [21 x i8] c"Enter first string: \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%s\00", align 1
@.str.2 = private unnamed_addr constant [22 x i8] c"Enter second string: \00", align 1
@.str.3 = private unnamed_addr constant [24 x i8] c"After concatenation: %s\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %s1 = alloca [100 x i8], align 16
  %s2 = alloca [100 x i8], align 16
  %0 = getelementptr inbounds [100 x i8], [100 x i8]* %s1, i64 0, i64 0
  call void @llvm.lifetime.start(i64 100, i8* %0) #1
  %1 = getelementptr inbounds [100 x i8], [100 x i8]* %s2, i64 0, i64 0
  call void @llvm.lifetime.start(i64 100, i8* %1) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i8* %0) #1
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.2, i64 0, i64 0)) #1
  %call4 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i8* %1) #1
  br label %for.cond

for.cond:                                         ; preds = %for.cond, %entry
  %i.0 = phi i8 [ 0, %entry ], [ %inc, %for.cond ]
  %idxprom = sext i8 %i.0 to i64
  %arrayidx = getelementptr inbounds [100 x i8], [100 x i8]* %s1, i64 0, i64 %idxprom
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !1
  %cmp = icmp eq i8 %2, 0
  %inc = add i8 %i.0, 1
  br i1 %cmp, label %for.cond.6.preheader, label %for.cond

for.cond.6.preheader:                             ; preds = %for.cond
  %arrayidx.lcssa = phi i8* [ %arrayidx, %for.cond ]
  %i.0.lcssa = phi i8 [ %i.0, %for.cond ]
  %3 = load i8, i8* %1, align 16, !tbaa !1
  %cmp10.32 = icmp eq i8 %3, 0
  br i1 %cmp10.32, label %for.end.20, label %for.body.12.preheader

for.body.12.preheader:                            ; preds = %for.cond.6.preheader
  br label %for.body.12

for.body.12:                                      ; preds = %for.body.12.preheader, %for.body.12
  %arrayidx2237 = phi i8* [ %arrayidx22, %for.body.12 ], [ %arrayidx.lcssa, %for.body.12.preheader ]
  %4 = phi i8 [ %5, %for.body.12 ], [ %3, %for.body.12.preheader ]
  %j.036 = phi i8 [ %inc18, %for.body.12 ], [ 0, %for.body.12.preheader ]
  %i.135 = phi i8 [ %inc19, %for.body.12 ], [ %i.0.lcssa, %for.body.12.preheader ]
  store i8 %4, i8* %arrayidx2237, align 1, !tbaa !1
  %inc18 = add i8 %j.036, 1
  %inc19 = add i8 %i.135, 1
  %idxprom7 = sext i8 %inc18 to i64
  %arrayidx8 = getelementptr inbounds [100 x i8], [100 x i8]* %s2, i64 0, i64 %idxprom7
  %5 = load i8, i8* %arrayidx8, align 1, !tbaa !1
  %cmp10 = icmp eq i8 %5, 0
  %idxprom21 = sext i8 %inc19 to i64
  %arrayidx22 = getelementptr inbounds [100 x i8], [100 x i8]* %s1, i64 0, i64 %idxprom21
  br i1 %cmp10, label %for.end.20.loopexit, label %for.body.12

for.end.20.loopexit:                              ; preds = %for.body.12
  %arrayidx22.lcssa42 = phi i8* [ %arrayidx22, %for.body.12 ]
  br label %for.end.20

for.end.20:                                       ; preds = %for.end.20.loopexit, %for.cond.6.preheader
  %arrayidx22.lcssa = phi i8* [ %arrayidx.lcssa, %for.cond.6.preheader ], [ %arrayidx22.lcssa42, %for.end.20.loopexit ]
  store i8 0, i8* %arrayidx22.lcssa, align 1, !tbaa !1
  %call24 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.3, i64 0, i64 0), i8* %0) #1
  call void @llvm.lifetime.end(i64 100, i8* %1) #1
  call void @llvm.lifetime.end(i64 100, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"omnipotent char", !3, i64 0}
!3 = !{!"Simple C/C++ TBAA"}
