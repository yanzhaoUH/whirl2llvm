; ModuleID = 'test50.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [35 x i8] c"Enter rows and columns of matrix: \00", align 1
@.str.1 = private unnamed_addr constant [6 x i8] c"%d %d\00", align 1
@.str.3 = private unnamed_addr constant [22 x i8] c"Enter element a%d%d: \00", align 1
@.str.5 = private unnamed_addr constant [5 x i8] c"%d  \00", align 1
@str = private unnamed_addr constant [27 x i8] c"\0AEnter elements of matrix:\00"
@str.8 = private unnamed_addr constant [18 x i8] c"\0AEntered Matrix: \00"
@str.9 = private unnamed_addr constant [22 x i8] c"\0ATranspose of Matrix:\00"
@str.11 = private unnamed_addr constant [2 x i8] c"\0A\00"

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %a = alloca [10 x [10 x i32]], align 16
  %transpose = alloca [10 x [10 x i32]], align 16
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %0 = bitcast [10 x [10 x i32]]* %a to i8*
  call void @llvm.lifetime.start(i64 400, i8* %0) #1
  %1 = bitcast [10 x [10 x i32]]* %transpose to i8*
  call void @llvm.lifetime.start(i64 400, i8* %1) #1
  %2 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start(i64 4, i8* %2) #1
  %3 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start(i64 4, i8* %3) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %r, i32* nonnull %c) #1
  %puts = call i32 @puts(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @str, i64 0, i64 0))
  %4 = load i32, i32* %r, align 4, !tbaa !1
  %cmp.127 = icmp sgt i32 %4, 0
  br i1 %cmp.127, label %for.cond.3.preheader.lr.ph, label %for.end.14

for.cond.3.preheader.lr.ph:                       ; preds = %entry
  %.pre = load i32, i32* %c, align 4, !tbaa !1
  br label %for.cond.3.preheader

for.cond.3.for.cond.loopexit_crit_edge:           ; preds = %for.body.5
  %.lcssa167 = phi i32 [ %14, %for.body.5 ]
  %.pre144 = load i32, i32* %r, align 4, !tbaa !1
  br label %for.cond.loopexit

for.cond.loopexit:                                ; preds = %for.cond.3.for.cond.loopexit_crit_edge, %for.cond.3.preheader
  %5 = phi i32 [ %.pre144, %for.cond.3.for.cond.loopexit_crit_edge ], [ %8, %for.cond.3.preheader ]
  %6 = phi i32 [ %.lcssa167, %for.cond.3.for.cond.loopexit_crit_edge ], [ %9, %for.cond.3.preheader ]
  %7 = sext i32 %5 to i64
  %cmp = icmp slt i64 %indvars.iv.next139, %7
  br i1 %cmp, label %for.cond.3.preheader, label %for.end.14.loopexit

for.cond.3.preheader:                             ; preds = %for.cond.3.preheader.lr.ph, %for.cond.loopexit
  %8 = phi i32 [ %4, %for.cond.3.preheader.lr.ph ], [ %5, %for.cond.loopexit ]
  %9 = phi i32 [ %.pre, %for.cond.3.preheader.lr.ph ], [ %6, %for.cond.loopexit ]
  %indvars.iv138 = phi i64 [ 0, %for.cond.3.preheader.lr.ph ], [ %indvars.iv.next139, %for.cond.loopexit ]
  %cmp4.125 = icmp sgt i32 %9, 0
  %indvars.iv.next139 = add nuw nsw i64 %indvars.iv138, 1
  br i1 %cmp4.125, label %for.body.5.preheader, label %for.cond.loopexit

for.body.5.preheader:                             ; preds = %for.cond.3.preheader
  %10 = trunc i64 %indvars.iv.next139 to i32
  br label %for.body.5

for.body.5:                                       ; preds = %for.body.5.preheader, %for.body.5
  %indvars.iv135 = phi i64 [ %indvars.iv.next136, %for.body.5 ], [ 0, %for.body.5.preheader ]
  %indvars.iv.next136 = add nuw nsw i64 %indvars.iv135, 1
  %11 = trunc i64 %indvars.iv.next136 to i32
  %call7 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.3, i64 0, i64 0), i32 %10, i32 %11) #1
  %12 = mul nsw i64 %indvars.iv.next136, %indvars.iv.next139
  %arrayidx11 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %a, i64 0, i64 %indvars.iv138, i64 %indvars.iv135
  %13 = trunc i64 %12 to i32
  store i32 %13, i32* %arrayidx11, align 4, !tbaa !1
  %14 = load i32, i32* %c, align 4, !tbaa !1
  %15 = sext i32 %14 to i64
  %cmp4 = icmp slt i64 %indvars.iv.next136, %15
  br i1 %cmp4, label %for.body.5, label %for.cond.3.for.cond.loopexit_crit_edge

for.end.14.loopexit:                              ; preds = %for.cond.loopexit
  br label %for.end.14

for.end.14:                                       ; preds = %for.end.14.loopexit, %entry
  %puts109 = call i32 @puts(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @str.8, i64 0, i64 0))
  %16 = load i32, i32* %r, align 4, !tbaa !1
  %cmp17.123 = icmp sgt i32 %16, 0
  br i1 %cmp17.123, label %for.cond.19.preheader.lr.ph, label %for.end.54

for.cond.19.preheader.lr.ph:                      ; preds = %for.end.14
  %.pre145 = load i32, i32* %c, align 4, !tbaa !1
  br label %for.cond.19.preheader

for.cond.19.preheader:                            ; preds = %for.cond.19.preheader.lr.ph, %for.inc.32
  %17 = phi i32 [ %16, %for.cond.19.preheader.lr.ph ], [ %58, %for.inc.32 ]
  %18 = phi i32 [ %.pre145, %for.cond.19.preheader.lr.ph ], [ %59, %for.inc.32 ]
  %indvars.iv133 = phi i64 [ 0, %for.cond.19.preheader.lr.ph ], [ %indvars.iv.next134, %for.inc.32 ]
  %cmp20.121 = icmp sgt i32 %18, 0
  br i1 %cmp20.121, label %for.body.21.preheader, label %for.inc.32

for.body.21.preheader:                            ; preds = %for.cond.19.preheader
  br label %for.body.21

for.cond.35.preheader:                            ; preds = %for.inc.32
  %.lcssa166 = phi i32 [ %58, %for.inc.32 ]
  %cmp36.119 = icmp sgt i32 %.lcssa166, 0
  br i1 %cmp36.119, label %for.cond.38.preheader.lr.ph, label %for.end.54

for.cond.38.preheader.lr.ph:                      ; preds = %for.cond.35.preheader
  %19 = load i32, i32* %c, align 4, !tbaa !1
  %cmp39.117 = icmp sgt i32 %19, 0
  br i1 %cmp39.117, label %for.cond.38.preheader.lr.ph.split.us, label %for.end.54

for.cond.38.preheader.lr.ph.split.us:             ; preds = %for.cond.38.preheader.lr.ph
  %20 = sext i32 %.lcssa166 to i64
  %21 = add i32 %19, -1
  %22 = zext i32 %21 to i64
  %23 = add nuw nsw i64 %22, 1
  %end.idx = add nuw nsw i64 %22, 1
  %n.vec = and i64 %23, 8589934584
  %cmp.zero = icmp eq i64 %n.vec, 0
  br label %overflow.checked

for.inc.52.us.loopexit:                           ; preds = %for.body.40.us
  br label %for.inc.52.us

for.inc.52.us:                                    ; preds = %for.inc.52.us.loopexit, %middle.block
  %indvars.iv.next143 = add nuw nsw i64 %indvars.iv142, 1
  %cmp36.us = icmp slt i64 %indvars.iv.next143, %20
  br i1 %cmp36.us, label %overflow.checked, label %for.end.54.loopexit

for.body.40.us:                                   ; preds = %for.body.40.us.preheader, %for.body.40.us
  %indvars.iv140 = phi i64 [ %indvars.iv.next141, %for.body.40.us ], [ %resume.val, %for.body.40.us.preheader ]
  %arrayidx44.us = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %a, i64 0, i64 %indvars.iv142, i64 %indvars.iv140
  %24 = load i32, i32* %arrayidx44.us, align 4, !tbaa !1
  %arrayidx48.us = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %transpose, i64 0, i64 %indvars.iv140, i64 %indvars.iv142
  store i32 %24, i32* %arrayidx48.us, align 4, !tbaa !1
  %indvars.iv.next141 = add nuw nsw i64 %indvars.iv140, 1
  %lftr.wideiv = trunc i64 %indvars.iv.next141 to i32
  %exitcond = icmp eq i32 %lftr.wideiv, %19
  br i1 %exitcond, label %for.inc.52.us.loopexit, label %for.body.40.us, !llvm.loop !5

overflow.checked:                                 ; preds = %for.inc.52.us, %for.cond.38.preheader.lr.ph.split.us
  %indvars.iv142 = phi i64 [ %indvars.iv.next143, %for.inc.52.us ], [ 0, %for.cond.38.preheader.lr.ph.split.us ]
  br i1 %cmp.zero, label %middle.block, label %vector.body.preheader

vector.body.preheader:                            ; preds = %overflow.checked
  br label %vector.body

vector.body:                                      ; preds = %vector.body.preheader, %vector.body
  %index = phi i64 [ %index.next, %vector.body ], [ 0, %vector.body.preheader ]
  %broadcast.splatinsert = insertelement <4 x i64> undef, i64 %index, i32 0
  %broadcast.splat = shufflevector <4 x i64> %broadcast.splatinsert, <4 x i64> undef, <4 x i32> zeroinitializer
  %induction = add <4 x i64> %broadcast.splat, <i64 0, i64 1, i64 2, i64 3>
  %induction158 = add <4 x i64> %broadcast.splat, <i64 4, i64 5, i64 6, i64 7>
  %25 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %a, i64 0, i64 %indvars.iv142, i64 %index
  %26 = extractelement <4 x i64> %induction, i32 1
  %27 = extractelement <4 x i64> %induction, i32 2
  %28 = extractelement <4 x i64> %induction, i32 3
  %29 = extractelement <4 x i64> %induction158, i32 0
  %30 = extractelement <4 x i64> %induction158, i32 1
  %31 = extractelement <4 x i64> %induction158, i32 2
  %32 = extractelement <4 x i64> %induction158, i32 3
  %33 = bitcast i32* %25 to <4 x i32>*
  %wide.load = load <4 x i32>, <4 x i32>* %33, align 8, !tbaa !1
  %34 = getelementptr i32, i32* %25, i64 4
  %35 = bitcast i32* %34 to <4 x i32>*
  %wide.load159 = load <4 x i32>, <4 x i32>* %35, align 8, !tbaa !1
  %36 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %transpose, i64 0, i64 %index, i64 %indvars.iv142
  %37 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %transpose, i64 0, i64 %26, i64 %indvars.iv142
  %38 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %transpose, i64 0, i64 %27, i64 %indvars.iv142
  %39 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %transpose, i64 0, i64 %28, i64 %indvars.iv142
  %40 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %transpose, i64 0, i64 %29, i64 %indvars.iv142
  %41 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %transpose, i64 0, i64 %30, i64 %indvars.iv142
  %42 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %transpose, i64 0, i64 %31, i64 %indvars.iv142
  %43 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %transpose, i64 0, i64 %32, i64 %indvars.iv142
  %44 = extractelement <4 x i32> %wide.load, i32 0
  store i32 %44, i32* %36, align 4, !tbaa !1
  %45 = extractelement <4 x i32> %wide.load, i32 1
  store i32 %45, i32* %37, align 4, !tbaa !1
  %46 = extractelement <4 x i32> %wide.load, i32 2
  store i32 %46, i32* %38, align 4, !tbaa !1
  %47 = extractelement <4 x i32> %wide.load, i32 3
  store i32 %47, i32* %39, align 4, !tbaa !1
  %48 = extractelement <4 x i32> %wide.load159, i32 0
  store i32 %48, i32* %40, align 4, !tbaa !1
  %49 = extractelement <4 x i32> %wide.load159, i32 1
  store i32 %49, i32* %41, align 4, !tbaa !1
  %50 = extractelement <4 x i32> %wide.load159, i32 2
  store i32 %50, i32* %42, align 4, !tbaa !1
  %51 = extractelement <4 x i32> %wide.load159, i32 3
  store i32 %51, i32* %43, align 4, !tbaa !1
  %index.next = add i64 %index, 8
  %52 = icmp eq i64 %index.next, %n.vec
  br i1 %52, label %middle.block.loopexit, label %vector.body, !llvm.loop !9

middle.block.loopexit:                            ; preds = %vector.body
  br label %middle.block

middle.block:                                     ; preds = %middle.block.loopexit, %overflow.checked
  %resume.val = phi i64 [ 0, %overflow.checked ], [ %n.vec, %middle.block.loopexit ]
  %cmp.n = icmp eq i64 %end.idx, %resume.val
  br i1 %cmp.n, label %for.inc.52.us, label %for.body.40.us.preheader

for.body.40.us.preheader:                         ; preds = %middle.block
  br label %for.body.40.us

for.body.21:                                      ; preds = %for.body.21.preheader, %for.inc.29
  %indvars.iv131 = phi i64 [ %indvars.iv.next132, %for.inc.29 ], [ 0, %for.body.21.preheader ]
  %arrayidx25 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %a, i64 0, i64 %indvars.iv133, i64 %indvars.iv131
  %53 = load i32, i32* %arrayidx25, align 4, !tbaa !1
  %call26 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.5, i64 0, i64 0), i32 %53) #1
  %54 = load i32, i32* %c, align 4, !tbaa !1
  %sub = add nsw i32 %54, -1
  %55 = trunc i64 %indvars.iv131 to i32
  %cmp27 = icmp eq i32 %55, %sub
  br i1 %cmp27, label %if.then, label %for.inc.29

if.then:                                          ; preds = %for.body.21
  %puts112 = call i32 @puts(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @str.11, i64 0, i64 0))
  %.pre146 = load i32, i32* %c, align 4, !tbaa !1
  br label %for.inc.29

for.inc.29:                                       ; preds = %for.body.21, %if.then
  %56 = phi i32 [ %54, %for.body.21 ], [ %.pre146, %if.then ]
  %indvars.iv.next132 = add nuw nsw i64 %indvars.iv131, 1
  %57 = sext i32 %56 to i64
  %cmp20 = icmp slt i64 %indvars.iv.next132, %57
  br i1 %cmp20, label %for.body.21, label %for.cond.19.for.inc.32_crit_edge

for.cond.19.for.inc.32_crit_edge:                 ; preds = %for.inc.29
  %.lcssa165 = phi i32 [ %56, %for.inc.29 ]
  %.pre147 = load i32, i32* %r, align 4, !tbaa !1
  br label %for.inc.32

for.inc.32:                                       ; preds = %for.cond.19.for.inc.32_crit_edge, %for.cond.19.preheader
  %58 = phi i32 [ %.pre147, %for.cond.19.for.inc.32_crit_edge ], [ %17, %for.cond.19.preheader ]
  %59 = phi i32 [ %.lcssa165, %for.cond.19.for.inc.32_crit_edge ], [ %18, %for.cond.19.preheader ]
  %indvars.iv.next134 = add nuw nsw i64 %indvars.iv133, 1
  %60 = sext i32 %58 to i64
  %cmp17 = icmp slt i64 %indvars.iv.next134, %60
  br i1 %cmp17, label %for.cond.19.preheader, label %for.cond.35.preheader

for.end.54.loopexit:                              ; preds = %for.inc.52.us
  br label %for.end.54

for.end.54:                                       ; preds = %for.end.54.loopexit, %for.end.14, %for.cond.38.preheader.lr.ph, %for.cond.35.preheader
  %puts110 = call i32 @puts(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @str.9, i64 0, i64 0))
  %61 = load i32, i32* %c, align 4, !tbaa !1
  %cmp57.115 = icmp sgt i32 %61, 0
  br i1 %cmp57.115, label %for.cond.59.preheader.lr.ph, label %for.end.77

for.cond.59.preheader.lr.ph:                      ; preds = %for.end.54
  %.pre148 = load i32, i32* %r, align 4, !tbaa !1
  br label %for.cond.59.preheader

for.cond.59.preheader:                            ; preds = %for.cond.59.preheader.lr.ph, %for.inc.75
  %62 = phi i32 [ %61, %for.cond.59.preheader.lr.ph ], [ %69, %for.inc.75 ]
  %63 = phi i32 [ %.pre148, %for.cond.59.preheader.lr.ph ], [ %70, %for.inc.75 ]
  %indvars.iv129 = phi i64 [ 0, %for.cond.59.preheader.lr.ph ], [ %indvars.iv.next130, %for.inc.75 ]
  %cmp60.113 = icmp sgt i32 %63, 0
  br i1 %cmp60.113, label %for.body.61.preheader, label %for.inc.75

for.body.61.preheader:                            ; preds = %for.cond.59.preheader
  br label %for.body.61

for.body.61:                                      ; preds = %for.body.61.preheader, %for.inc.72
  %indvars.iv = phi i64 [ %indvars.iv.next, %for.inc.72 ], [ 0, %for.body.61.preheader ]
  %arrayidx65 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %transpose, i64 0, i64 %indvars.iv129, i64 %indvars.iv
  %64 = load i32, i32* %arrayidx65, align 4, !tbaa !1
  %call66 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.5, i64 0, i64 0), i32 %64) #1
  %65 = load i32, i32* %r, align 4, !tbaa !1
  %sub67 = add nsw i32 %65, -1
  %66 = trunc i64 %indvars.iv to i32
  %cmp68 = icmp eq i32 %66, %sub67
  br i1 %cmp68, label %if.then.69, label %for.inc.72

if.then.69:                                       ; preds = %for.body.61
  %puts111 = call i32 @puts(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @str.11, i64 0, i64 0))
  %.pre149 = load i32, i32* %r, align 4, !tbaa !1
  br label %for.inc.72

for.inc.72:                                       ; preds = %for.body.61, %if.then.69
  %67 = phi i32 [ %65, %for.body.61 ], [ %.pre149, %if.then.69 ]
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %68 = sext i32 %67 to i64
  %cmp60 = icmp slt i64 %indvars.iv.next, %68
  br i1 %cmp60, label %for.body.61, label %for.cond.59.for.inc.75_crit_edge

for.cond.59.for.inc.75_crit_edge:                 ; preds = %for.inc.72
  %.lcssa = phi i32 [ %67, %for.inc.72 ]
  %.pre150 = load i32, i32* %c, align 4, !tbaa !1
  br label %for.inc.75

for.inc.75:                                       ; preds = %for.cond.59.for.inc.75_crit_edge, %for.cond.59.preheader
  %69 = phi i32 [ %.pre150, %for.cond.59.for.inc.75_crit_edge ], [ %62, %for.cond.59.preheader ]
  %70 = phi i32 [ %.lcssa, %for.cond.59.for.inc.75_crit_edge ], [ %63, %for.cond.59.preheader ]
  %indvars.iv.next130 = add nuw nsw i64 %indvars.iv129, 1
  %71 = sext i32 %69 to i64
  %cmp57 = icmp slt i64 %indvars.iv.next130, %71
  br i1 %cmp57, label %for.cond.59.preheader, label %for.end.77.loopexit

for.end.77.loopexit:                              ; preds = %for.inc.75
  br label %for.end.77

for.end.77:                                       ; preds = %for.end.77.loopexit, %for.end.54
  call void @llvm.lifetime.end(i64 4, i8* %3) #1
  call void @llvm.lifetime.end(i64 4, i8* %2) #1
  call void @llvm.lifetime.end(i64 400, i8* %1) #1
  call void @llvm.lifetime.end(i64 400, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @puts(i8* nocapture readonly) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = distinct !{!5, !6, !7, !8}
!6 = !{!"llvm.loop.unroll.runtime.disable"}
!7 = !{!"llvm.loop.vectorize.width", i32 1}
!8 = !{!"llvm.loop.interleave.count", i32 1}
!9 = distinct !{!9, !7, !8}
