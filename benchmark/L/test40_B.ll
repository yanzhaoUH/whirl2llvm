; ModuleID = 'test40.bc'

@.str = private constant [24 x i8] c"Enter a binary number: \00", align 1
@.str.1 = private constant [5 x i8] c"%lld\00", align 1
@.str.2 = private constant [31 x i8] c"%lld in binary = %d in decimal\00", align 1
@_LIB_VERSION_59 = common global i32 0, align 4
@signgam_60 = common global i32 0, align 4

define i32 @main() {
entry:
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_dummy11_6 = alloca i32, align 4
  %_temp_dummy12_7 = alloca i32, align 4
  %n_4 = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i64* %n_4 to i64*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i64* %2)
  %3 = bitcast i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.2, i32 0, i32 0) to i8*
  %4 = load i64, i64* %n_4, align 8
  %5 = load i64, i64* %n_4, align 8
  %call3 = call i32 @_Z22ConvertBinaryToDecimalx(i64 %5)
  %call4 = call i32 (i8*, ...) @printf(i8* %3, i64 %4, i32 %call3)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define i32 @_Z22ConvertBinaryToDecimalx(i64 %n_4) {
entry:
  %decimalNumber_5 = alloca i32, align 4
  %i_6 = alloca i32, align 4
  %n_4.addr = alloca i64, align 8
  %remainder_7 = alloca i32, align 4
  store i64 %n_4, i64* %n_4.addr, align 8
  store i32 0, i32* %decimalNumber_5, align 4
  store i32 0, i32* %i_6, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i64, i64* %n_4.addr, align 8
  %cmp1 = icmp ne i64 %0, 0
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load i64, i64* %n_4.addr, align 8
  %srem = srem i64 %1, 10
  %conv = trunc i64 %srem to i32
  store i32 %conv, i32* %remainder_7, align 4
  %2 = load i64, i64* %n_4.addr, align 8
  %div = sdiv i64 %2, 10
  store i64 %div, i64* %n_4.addr, align 8
  %3 = load i32, i32* %decimalNumber_5, align 4
  %conv9 = sitofp i32 %3 to double
  %4 = load i32, i32* %remainder_7, align 4
  %conv91 = sitofp i32 %4 to double
  %5 = load i32, i32* %i_6, align 4
  %conv92 = sitofp i32 %5 to double
  %call5 = call double @pow(double 2.000000e+00, double %conv92)
  %mul = fmul double %conv91, %call5
  %add = fadd double %conv9, %mul
  %fp2int = fptosi double %add to i32
  store i32 %fp2int, i32* %decimalNumber_5, align 4
  %6 = load i32, i32* %i_6, align 4
  %add.3 = add i32 %6, 1
  store i32 %add.3, i32* %i_6, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %7 = load i32, i32* %decimalNumber_5, align 4
  ret i32 %7
}

declare double @pow(double, double)
