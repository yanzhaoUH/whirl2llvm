; ModuleID = 'test72.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str.1 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.2 = private unnamed_addr constant [30 x i8] c"Sum of all odd numbers  = %d\0A\00", align 1
@.str.3 = private unnamed_addr constant [30 x i8] c"Sum of all even numbers = %d\0A\00", align 1
@str = private unnamed_addr constant [23 x i8] c"Enter the value of num\00"

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %num = alloca i32, align 4
  %0 = bitcast i32* %num to i8*
  call void @llvm.lifetime.start(i64 4, i8* %0) #1
  %puts = tail call i32 @puts(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @str, i64 0, i64 0))
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %num) #1
  %1 = load i32, i32* %num, align 4, !tbaa !1
  %cmp.16 = icmp slt i32 %1, 1
  br i1 %cmp.16, label %for.end, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  %backedge.overflow = icmp eq i32 %1, 0
  br i1 %backedge.overflow, label %for.body.preheader39, label %overflow.checked

overflow.checked:                                 ; preds = %for.body.preheader
  %end.idx = add i32 %1, 1
  %n.vec = and i32 %1, -8
  %end.idx.rnd.down = or i32 %n.vec, 1
  %cmp.zero = icmp eq i32 %end.idx.rnd.down, 1
  br i1 %cmp.zero, label %middle.block, label %vector.body.preheader

vector.body.preheader:                            ; preds = %overflow.checked
  br label %vector.body

vector.body:                                      ; preds = %vector.body.preheader, %vector.body
  %index = phi i32 [ %index.next, %vector.body ], [ 1, %vector.body.preheader ]
  %vec.phi = phi <4 x i32> [ %12, %vector.body ], [ zeroinitializer, %vector.body.preheader ]
  %vec.phi21 = phi <4 x i32> [ %13, %vector.body ], [ zeroinitializer, %vector.body.preheader ]
  %vec.phi22 = phi <4 x i32> [ %8, %vector.body ], [ zeroinitializer, %vector.body.preheader ]
  %vec.phi23 = phi <4 x i32> [ %9, %vector.body ], [ zeroinitializer, %vector.body.preheader ]
  %broadcast.splatinsert = insertelement <4 x i32> undef, i32 %index, i32 0
  %broadcast.splat = shufflevector <4 x i32> %broadcast.splatinsert, <4 x i32> undef, <4 x i32> zeroinitializer
  %induction = add <4 x i32> %broadcast.splat, <i32 0, i32 1, i32 2, i32 3>
  %induction24 = add <4 x i32> %broadcast.splat, <i32 4, i32 5, i32 6, i32 7>
  %2 = and <4 x i32> %induction, <i32 1, i32 1, i32 1, i32 1>
  %3 = and <4 x i32> %induction24, <i32 1, i32 1, i32 1, i32 1>
  %4 = icmp eq <4 x i32> %2, zeroinitializer
  %5 = icmp eq <4 x i32> %3, zeroinitializer
  %6 = select <4 x i1> %4, <4 x i32> zeroinitializer, <4 x i32> %induction
  %7 = select <4 x i1> %5, <4 x i32> zeroinitializer, <4 x i32> %induction24
  %8 = add nsw <4 x i32> %6, %vec.phi22
  %9 = add nsw <4 x i32> %7, %vec.phi23
  %10 = select <4 x i1> %4, <4 x i32> %induction, <4 x i32> zeroinitializer
  %11 = select <4 x i1> %5, <4 x i32> %induction24, <4 x i32> zeroinitializer
  %12 = add nsw <4 x i32> %10, %vec.phi
  %13 = add nsw <4 x i32> %11, %vec.phi21
  %index.next = add i32 %index, 8
  %14 = icmp eq i32 %index.next, %end.idx.rnd.down
  br i1 %14, label %middle.block.loopexit, label %vector.body, !llvm.loop !5

middle.block.loopexit:                            ; preds = %vector.body
  %.lcssa45 = phi <4 x i32> [ %13, %vector.body ]
  %.lcssa44 = phi <4 x i32> [ %12, %vector.body ]
  %.lcssa43 = phi <4 x i32> [ %9, %vector.body ]
  %.lcssa = phi <4 x i32> [ %8, %vector.body ]
  br label %middle.block

middle.block:                                     ; preds = %middle.block.loopexit, %overflow.checked
  %resume.val = phi i32 [ 1, %overflow.checked ], [ %end.idx.rnd.down, %middle.block.loopexit ]
  %rdx.vec.exit.phi = phi <4 x i32> [ zeroinitializer, %overflow.checked ], [ %.lcssa44, %middle.block.loopexit ]
  %rdx.vec.exit.phi27 = phi <4 x i32> [ zeroinitializer, %overflow.checked ], [ %.lcssa45, %middle.block.loopexit ]
  %rdx.vec.exit.phi31 = phi <4 x i32> [ zeroinitializer, %overflow.checked ], [ %.lcssa, %middle.block.loopexit ]
  %rdx.vec.exit.phi32 = phi <4 x i32> [ zeroinitializer, %overflow.checked ], [ %.lcssa43, %middle.block.loopexit ]
  %bin.rdx33 = add <4 x i32> %rdx.vec.exit.phi32, %rdx.vec.exit.phi31
  %rdx.shuf34 = shufflevector <4 x i32> %bin.rdx33, <4 x i32> undef, <4 x i32> <i32 2, i32 3, i32 undef, i32 undef>
  %bin.rdx35 = add <4 x i32> %bin.rdx33, %rdx.shuf34
  %rdx.shuf36 = shufflevector <4 x i32> %bin.rdx35, <4 x i32> undef, <4 x i32> <i32 1, i32 undef, i32 undef, i32 undef>
  %bin.rdx37 = add <4 x i32> %bin.rdx35, %rdx.shuf36
  %15 = extractelement <4 x i32> %bin.rdx37, i32 0
  %bin.rdx = add <4 x i32> %rdx.vec.exit.phi27, %rdx.vec.exit.phi
  %rdx.shuf = shufflevector <4 x i32> %bin.rdx, <4 x i32> undef, <4 x i32> <i32 2, i32 3, i32 undef, i32 undef>
  %bin.rdx28 = add <4 x i32> %bin.rdx, %rdx.shuf
  %rdx.shuf29 = shufflevector <4 x i32> %bin.rdx28, <4 x i32> undef, <4 x i32> <i32 1, i32 undef, i32 undef, i32 undef>
  %bin.rdx30 = add <4 x i32> %bin.rdx28, %rdx.shuf29
  %16 = extractelement <4 x i32> %bin.rdx30, i32 0
  %cmp.n = icmp eq i32 %end.idx, %resume.val
  br i1 %cmp.n, label %for.end, label %for.body.preheader39

for.body.preheader39:                             ; preds = %middle.block, %for.body.preheader
  %even_sum.019.ph = phi i32 [ 0, %for.body.preheader ], [ %16, %middle.block ]
  %odd_sum.018.ph = phi i32 [ 0, %for.body.preheader ], [ %15, %middle.block ]
  %i.017.ph = phi i32 [ 1, %for.body.preheader ], [ %resume.val, %middle.block ]
  br label %for.body

for.body:                                         ; preds = %for.body.preheader39, %for.body
  %even_sum.019 = phi i32 [ %even_sum.1, %for.body ], [ %even_sum.019.ph, %for.body.preheader39 ]
  %odd_sum.018 = phi i32 [ %odd_sum.1, %for.body ], [ %odd_sum.018.ph, %for.body.preheader39 ]
  %i.017 = phi i32 [ %inc, %for.body ], [ %i.017.ph, %for.body.preheader39 ]
  %rem15 = and i32 %i.017, 1
  %cmp2 = icmp eq i32 %rem15, 0
  %add3 = select i1 %cmp2, i32 0, i32 %i.017
  %odd_sum.1 = add nsw i32 %add3, %odd_sum.018
  %add = select i1 %cmp2, i32 %i.017, i32 0
  %even_sum.1 = add nsw i32 %add, %even_sum.019
  %inc = add nuw nsw i32 %i.017, 1
  %cmp = icmp slt i32 %i.017, %1
  br i1 %cmp, label %for.body, label %for.end.loopexit, !llvm.loop !8

for.end.loopexit:                                 ; preds = %for.body
  %even_sum.1.lcssa = phi i32 [ %even_sum.1, %for.body ]
  %odd_sum.1.lcssa = phi i32 [ %odd_sum.1, %for.body ]
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %middle.block, %entry
  %even_sum.0.lcssa = phi i32 [ 0, %entry ], [ %16, %middle.block ], [ %even_sum.1.lcssa, %for.end.loopexit ]
  %odd_sum.0.lcssa = phi i32 [ 0, %entry ], [ %15, %middle.block ], [ %odd_sum.1.lcssa, %for.end.loopexit ]
  %call4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.2, i64 0, i64 0), i32 %odd_sum.0.lcssa) #1
  %call5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.3, i64 0, i64 0), i32 %even_sum.0.lcssa) #1
  call void @llvm.lifetime.end(i64 4, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @puts(i8* nocapture readonly) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = distinct !{!5, !6, !7}
!6 = !{!"llvm.loop.vectorize.width", i32 1}
!7 = !{!"llvm.loop.interleave.count", i32 1}
!8 = distinct !{!8, !9, !6, !7}
!9 = !{!"llvm.loop.unroll.runtime.disable"}
