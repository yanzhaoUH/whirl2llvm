; ModuleID = 'test67.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.course = type { i32, [30 x i8] }

@.str = private unnamed_addr constant [26 x i8] c"Enter number of records: \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.3 = private unnamed_addr constant [6 x i8] c"%s %d\00", align 1
@.str.5 = private unnamed_addr constant [7 x i8] c"%s\09%d\0A\00", align 1
@str = private unnamed_addr constant [24 x i8] c"Displaying Information:\00"
@str.6 = private unnamed_addr constant [50 x i8] c"Enter name of the subject and marks respectively:\00"

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %noOfRecords = alloca i32, align 4
  %0 = bitcast i32* %noOfRecords to i8*
  call void @llvm.lifetime.start(i64 4, i8* %0) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %noOfRecords) #1
  %1 = load i32, i32* %noOfRecords, align 4, !tbaa !1
  %conv = sext i32 %1 to i64
  %mul = mul nsw i64 %conv, 36
  %call2 = call noalias i8* @malloc(i64 %mul) #1
  %2 = bitcast i8* %call2 to %struct.course*
  %cmp.38 = icmp sgt i32 %1, 0
  br i1 %cmp.38, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %entry
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.body
  %indvars.iv40 = phi i64 [ %indvars.iv.next41, %for.body ], [ 0, %for.body.preheader ]
  %puts35 = call i32 @puts(i8* getelementptr inbounds ([50 x i8], [50 x i8]* @str.6, i64 0, i64 0))
  %subject = getelementptr inbounds %struct.course, %struct.course* %2, i64 %indvars.iv40, i32 1
  %marks = getelementptr inbounds %struct.course, %struct.course* %2, i64 %indvars.iv40, i32 0
  %call7 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.3, i64 0, i64 0), [30 x i8]* %subject, i32* %marks) #1
  %indvars.iv.next41 = add nuw nsw i64 %indvars.iv40, 1
  %3 = load i32, i32* %noOfRecords, align 4, !tbaa !1
  %4 = sext i32 %3 to i64
  %cmp = icmp slt i64 %indvars.iv.next41, %4
  br i1 %cmp, label %for.body, label %for.end.loopexit

for.end.loopexit:                                 ; preds = %for.body
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  %puts = call i32 @puts(i8* getelementptr inbounds ([24 x i8], [24 x i8]* @str, i64 0, i64 0))
  %5 = load i32, i32* %noOfRecords, align 4, !tbaa !1
  %cmp10.36 = icmp sgt i32 %5, 0
  br i1 %cmp10.36, label %for.body.12.preheader, label %for.end.22

for.body.12.preheader:                            ; preds = %for.end
  br label %for.body.12

for.body.12:                                      ; preds = %for.body.12.preheader, %for.body.12
  %indvars.iv = phi i64 [ %indvars.iv.next, %for.body.12 ], [ 0, %for.body.12.preheader ]
  %arraydecay = getelementptr inbounds %struct.course, %struct.course* %2, i64 %indvars.iv, i32 1, i64 0
  %marks18 = getelementptr inbounds %struct.course, %struct.course* %2, i64 %indvars.iv, i32 0
  %6 = load i32, i32* %marks18, align 4, !tbaa !5
  %call19 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.5, i64 0, i64 0), i8* %arraydecay, i32 %6) #1
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %7 = load i32, i32* %noOfRecords, align 4, !tbaa !1
  %8 = sext i32 %7 to i64
  %cmp10 = icmp slt i64 %indvars.iv.next, %8
  br i1 %cmp10, label %for.body.12, label %for.end.22.loopexit

for.end.22.loopexit:                              ; preds = %for.body.12
  br label %for.end.22

for.end.22:                                       ; preds = %for.end.22.loopexit, %for.end
  call void @llvm.lifetime.end(i64 4, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) #2

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @puts(i8* nocapture readonly) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = !{!6, !2, i64 0}
!6 = !{!"course", !2, i64 0, !3, i64 4}
