; ModuleID = 'test34_O.bc'

@.str = private constant [30 x i8] c"Enter two positive integers: \00", align 1
@.str.1 = private constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private constant [41 x i8] c"Prime numberbers between %d and %d are: \00", align 1
@.str.3 = private constant [4 x i8] c"%d \00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_55 = alloca i32
  %.preg_I4_4_61 = alloca i32
  %.preg_I4_4_57 = alloca i32
  %.preg_I4_4_56 = alloca i32
  %.preg_I4_4_50 = alloca i32
  %.preg_I4_4_54 = alloca i32
  %.preg_I4_4_59 = alloca i32
  %.preg_I4_4_53 = alloca i32
  %.preg_I4_4_58 = alloca i32
  %.preg_I4_4_60 = alloca i32
  %_temp_dummy10_8 = alloca i32, align 4
  %_temp_dummy11_9 = alloca i32, align 4
  %_temp_dummy12_10 = alloca i32, align 4
  %_temp_dummy13_11 = alloca i32, align 4
  %_temp_ehpit0_19 = alloca i32, align 4
  %flag_17 = alloca i32, align 4
  %flag_7 = alloca i32, align 4
  %i_6 = alloca i32, align 4
  %j_16 = alloca i32, align 4
  %n1_4 = alloca i32, align 4
  %n2_5 = alloca i32, align 4
  %n_15.addr = alloca i32, align 4
  %n_18 = alloca i32, align 4
  %old_frame_pointer_24.addr = alloca i64, align 8
  %return_address_25.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n1_4 to i32*
  %3 = bitcast i32* %n2_5 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2, i32* %3)
  %4 = bitcast i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.2, i32 0, i32 0) to i8*
  %5 = load i32, i32* %n1_4, align 4
  %6 = load i32, i32* %n2_5, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %4, i32 %5, i32 %6)
  %7 = load i32, i32* %n1_4, align 4
  store i32 %7, i32* %.preg_I4_4_60, align 8
  %8 = load i32, i32* %.preg_I4_4_60
  %add = add i32 %8, 1
  store i32 %add, i32* %.preg_I4_4_58, align 8
  store i32 0, i32* %.preg_I4_4_53, align 8
  %9 = load i32, i32* %n2_5, align 4
  store i32 %9, i32* %.preg_I4_4_59, align 8
  %10 = load i32, i32* %.preg_I4_4_59
  %11 = load i32, i32* %.preg_I4_4_60
  %add.1 = sub i32 %10, %11
  %add.2 = add i32 %add.1, -1
  %cmp1 = icmp sgt i32 %add.2, 0
  br i1 %cmp1, label %tb, label %L5634

tb:                                               ; preds = %entry
  %12 = load i32, i32* %.preg_I4_4_58
  store i32 %12, i32* %.preg_I4_4_54, align 8
  br label %L6146

L5634:                                            ; preds = %fb12, %entry
  ret i32 0

L6146:                                            ; preds = %L8706, %tb
  store i32 0, i32* %.preg_I4_4_50, align 8
  %13 = load i32, i32* %.preg_I4_4_54
  %div = sdiv i32 %13, 2
  store i32 %div, i32* %.preg_I4_4_56, align 8
  %14 = load i32, i32* %.preg_I4_4_56
  %add.3 = add i32 %14, -2
  store i32 %add.3, i32* %.preg_I4_4_57, align 8
  %15 = load i32, i32* %.preg_I4_4_57
  %cmp2 = icmp sge i32 %15, 0
  br i1 %cmp2, label %tb4, label %L6402

tb4:                                              ; preds = %L6146
  %16 = load i32, i32* %.preg_I4_4_56
  %add.5 = add i32 %16, -1
  store i32 %add.5, i32* %.preg_I4_4_61, align 8
  store i32 2, i32* %.preg_I4_4_55, align 8
  br label %L6914

L6402:                                            ; preds = %fb, %L6146
  %17 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.3, i32 0, i32 0) to i8*
  %18 = load i32, i32* %.preg_I4_4_54
  %call4 = call i32 (i8*, ...) @printf(i8* %17, i32 %18)
  %19 = load i32, i32* %n2_5, align 4
  store i32 %19, i32* %.preg_I4_4_59, align 8
  br label %L8706

L6914:                                            ; preds = %L7170, %tb4
  %20 = load i32, i32* %.preg_I4_4_54
  %21 = load i32, i32* %.preg_I4_4_55
  %srem = srem i32 %20, %21
  %cmp3 = icmp eq i32 %srem, 0
  br i1 %cmp3, label %tb6, label %L7170

tb6:                                              ; preds = %L6914
  br label %L8706

L7170:                                            ; preds = %L6914
  %22 = load i32, i32* %.preg_I4_4_50
  %add.7 = add i32 %22, 1
  store i32 %add.7, i32* %.preg_I4_4_50, align 8
  %23 = load i32, i32* %.preg_I4_4_55
  %add.8 = add i32 %23, 1
  store i32 %add.8, i32* %.preg_I4_4_55, align 8
  %24 = load i32, i32* %.preg_I4_4_50
  %25 = load i32, i32* %.preg_I4_4_57
  %cmp4 = icmp sle i32 %24, %25
  br i1 %cmp4, label %L6914, label %fb

L8706:                                            ; preds = %tb6, %L6402
  %26 = load i32, i32* %.preg_I4_4_53
  %add.9 = add i32 %26, 1
  store i32 %add.9, i32* %.preg_I4_4_53, align 8
  %27 = load i32, i32* %.preg_I4_4_54
  %add.10 = add i32 %27, 1
  store i32 %add.10, i32* %.preg_I4_4_54, align 8
  %28 = load i32, i32* %.preg_I4_4_53
  %29 = load i32, i32* %.preg_I4_4_59
  %30 = load i32, i32* %.preg_I4_4_58
  %add.11 = sub i32 %29, %30
  %cmp5 = icmp slt i32 %28, %add.11
  br i1 %cmp5, label %L6146, label %fb12

fb:                                               ; preds = %L7170
  br label %L6402

fb12:                                             ; preds = %L8706
  br label %L5634
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define i32 @_Z16checkPrimeNumberi(i32 %n_4) {
entry:
  %.preg_I4_4_55 = alloca i32
  %.preg_I4_4_52 = alloca i32
  %.preg_I4_4_56 = alloca i32
  %.preg_I4_4_53 = alloca i32
  %.preg_I4_4_51 = alloca i32
  %.preg_I4_4_49 = alloca i32
  %.preg_I4_4_54 = alloca i32
  %flag_6 = alloca i32, align 4
  %j_5 = alloca i32, align 4
  %n_4.addr = alloca i32, align 4
  %old_frame_pointer_11.addr = alloca i64, align 8
  %return_address_12.addr = alloca i64, align 8
  store i32 %n_4, i32* %n_4.addr, align 4
  store i32 %n_4, i32* %.preg_I4_4_54, align 8
  store i32 0, i32* %.preg_I4_4_49, align 8
  %0 = load i32, i32* %.preg_I4_4_54
  %div = sdiv i32 %0, 2
  store i32 %div, i32* %.preg_I4_4_51, align 8
  %1 = load i32, i32* %.preg_I4_4_51
  %add = add i32 %1, -2
  store i32 %add, i32* %.preg_I4_4_53, align 8
  %2 = load i32, i32* %.preg_I4_4_53
  %cmp6 = icmp sge i32 %2, 0
  br i1 %cmp6, label %tb, label %L3074

tb:                                               ; preds = %entry
  %3 = load i32, i32* %.preg_I4_4_51
  %add.1 = add i32 %3, -1
  store i32 %add.1, i32* %.preg_I4_4_56, align 8
  store i32 2, i32* %.preg_I4_4_52, align 8
  br label %L3586

L3074:                                            ; preds = %fb, %entry
  store i32 1, i32* %.preg_I4_4_55, align 8
  br label %L258

L3586:                                            ; preds = %L3842, %tb
  %4 = load i32, i32* %.preg_I4_4_54
  %5 = load i32, i32* %.preg_I4_4_52
  %srem = srem i32 %4, %5
  %cmp7 = icmp eq i32 %srem, 0
  br i1 %cmp7, label %tb2, label %L3842

tb2:                                              ; preds = %L3586
  store i32 0, i32* %.preg_I4_4_55, align 8
  br label %L258

L3842:                                            ; preds = %L3586
  %6 = load i32, i32* %.preg_I4_4_49
  %add.3 = add i32 %6, 1
  store i32 %add.3, i32* %.preg_I4_4_49, align 8
  %7 = load i32, i32* %.preg_I4_4_52
  %add.4 = add i32 %7, 1
  store i32 %add.4, i32* %.preg_I4_4_52, align 8
  %8 = load i32, i32* %.preg_I4_4_49
  %9 = load i32, i32* %.preg_I4_4_53
  %cmp8 = icmp sle i32 %8, %9
  br i1 %cmp8, label %L3586, label %fb

L258:                                             ; preds = %tb2, %L3074
  %10 = load i32, i32* %.preg_I4_4_55
  ret i32 %10

fb:                                               ; preds = %L3842
  br label %L3074
}
