; ModuleID = 'test47.bc'

@.str = private constant [17 x i8] c"Enter elements: \00", align 1
@.str.1 = private constant [27 x i8] c"\0AStandard Deviation = %.6f\00", align 1
@_LIB_VERSION_60 = common global i32 0, align 4
@signgam_61 = common global i32 0, align 4

define i32 @main() {
entry:
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %data_5 = alloca [10 x float], align 4
  %i_4 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  store i32 0, i32* %i_4, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32, i32* %i_4, align 4
  %cmp1 = icmp sle i32 %1, 9
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load i32, i32* %i_4, align 4
  %conv3 = sext i32 %2 to i64
  %arrayidx = getelementptr [10 x float], [10 x float]* %data_5, i32 0, i64 %conv3
  %3 = load i32, i32* %i_4, align 4
  %add = add i32 %3, 2
  %conv9 = sitofp i32 %add to float
  store float %conv9, float* %arrayidx, align 4
  %4 = load i32, i32* %i_4, align 4
  %add.1 = add i32 %4, 1
  store i32 %add.1, i32* %i_4, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %5 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.1, i32 0, i32 0) to i8*
  %arrayAddress = getelementptr [10 x float], [10 x float]* %data_5, i32 0, i32 0
  %6 = bitcast float* %arrayAddress to float*
  %call2 = call float @_Z11calculateSDPf(float* %6)
  %convDouble = fpext float %call2 to double
  %call3 = call i32 (i8*, ...) @printf(i8* %5, double %convDouble)
  ret i32 0
}

declare i32 @printf(i8*, ...)

define float @_Z11calculateSDPf(float* %data_4) {
entry:
  %_temp___save_sqrt3_10 = alloca float, align 4
  %_temp___sqrt_arg2_9 = alloca float, align 4
  %data_4.addr = alloca float*, align 4
  %i_8 = alloca i32, align 4
  %mean_6 = alloca float, align 4
  %standardDeviation_7 = alloca float, align 4
  %sum_5 = alloca float, align 4
  store float* %data_4, float** %data_4.addr, align 4
  store float 0.000000e+00, float* %sum_5, align 4
  store float 0.000000e+00, float* %standardDeviation_7, align 4
  store i32 0, i32* %i_8, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %i_8, align 4
  %cmp2 = icmp sle i32 %0, 9
  br i1 %cmp2, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load i32, i32* %i_8, align 4
  %conv3 = sext i32 %1 to i64
  %mul = mul i64 %conv3, 4
  %2 = load float*, float** %data_4.addr, align 8
  %3 = ptrtoint float* %2 to i64
  %add = add i64 %mul, %3
  %4 = inttoptr i64 %add to float*
  %5 = load float, float* %4, align 4
  %6 = load float, float* %sum_5, align 4
  %add.1 = fadd float %5, %6
  store float %add.1, float* %sum_5, align 4
  %7 = load i32, i32* %i_8, align 4
  %add.2 = add i32 %7, 1
  store i32 %add.2, i32* %i_8, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %8 = load float, float* %sum_5, align 4
  %SDiv = fdiv float %8, 1.000000e+01
  store float %SDiv, float* %mean_6, align 4
  store i32 0, i32* %i_8, align 4
  br label %while.cond3

while.cond3:                                      ; preds = %while.body4, %while.end
  %9 = load i32, i32* %i_8, align 4
  %cmp3 = icmp sle i32 %9, 9
  br i1 %cmp3, label %while.body4, label %while.end5

while.body4:                                      ; preds = %while.cond3
  %10 = load float, float* %standardDeviation_7, align 4
  %convDouble = fpext float %10 to double
  %11 = load i32, i32* %i_8, align 4
  %conv36 = sext i32 %11 to i64
  %mul.7 = mul i64 %conv36, 4
  %12 = load float*, float** %data_4.addr, align 8
  %13 = ptrtoint float* %12 to i64
  %add.8 = add i64 %mul.7, %13
  %14 = inttoptr i64 %add.8 to float*
  %15 = load float, float* %14, align 4
  %16 = load float, float* %mean_6, align 4
  %add.9 = fsub float %15, %16
  %convDouble10 = fpext float %add.9 to double
  %call4 = call double @pow(double %convDouble10, double 2.000000e+00)
  %add.11 = fadd double %convDouble, %call4
  %convDouble12 = fptrunc double %add.11 to float
  store float %convDouble12, float* %standardDeviation_7, align 4
  %17 = load i32, i32* %i_8, align 4
  %add.13 = add i32 %17, 1
  store i32 %add.13, i32* %i_8, align 4
  br label %while.cond3

while.end5:                                       ; preds = %while.cond3
  %18 = load float, float* %standardDeviation_7, align 4
  %SDiv.14 = fdiv float %18, 1.000000e+01
  store float %SDiv.14, float* %_temp___sqrt_arg2_9, align 4
  %19 = load float, float* %_temp___sqrt_arg2_9, align 4
  %call7 = call float @sqrtf(float %19)
  store float %call7, float* %_temp___save_sqrt3_10, align 4
  %20 = load float, float* %_temp___save_sqrt3_10, align 4
  %21 = load float, float* %_temp___save_sqrt3_10, align 4
  %cmp4 = fcmp one float %20, %21
  br i1 %cmp4, label %if.then, label %if.else

if.then:                                          ; preds = %while.end5
  %22 = load float, float* %_temp___sqrt_arg2_9, align 4
  %call5 = call float @sqrtf(float %22)
  store float %call5, float* %_temp___save_sqrt3_10, align 4
  br label %if.end

if.else:                                          ; preds = %while.end5
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %23 = load float, float* %_temp___save_sqrt3_10, align 4
  ret float %23
}

declare double @pow(double, double)

declare float @sqrtf(float)
