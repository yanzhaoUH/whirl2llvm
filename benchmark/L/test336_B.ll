; ModuleID = 'test336.bc'

@.str = private constant [21 x i8] c"%d %d %d %d %d %d %d\00", align 1
@.str.1 = private constant [6 x i8] c"%f %f\00", align 1
@_LIB_VERSION_58 = common global i32 0, align 4
@signgam_59 = common global i32 0, align 4

define i32 @main() {
entry:
  %_temp_dummy10_34 = alloca i32, align 4
  %_temp_dummy11_35 = alloca i32, align 4
  %c1_4 = alloca i8, align 1
  %c2_5 = alloca i8, align 1
  %c3_6 = alloca i8, align 1
  %d1_31 = alloca double, align 8
  %d2_32 = alloca double, align 8
  %d3_33 = alloca double, align 8
  %f1_28 = alloca float, align 4
  %f2_29 = alloca float, align 4
  %f3_30 = alloca float, align 4
  %i1_13 = alloca i32, align 4
  %i2_14 = alloca i32, align 4
  %i3_15 = alloca i32, align 4
  %l1_16 = alloca i64, align 8
  %l2_17 = alloca i64, align 8
  %l3_18 = alloca i64, align 8
  %ll1_19 = alloca i64, align 8
  %ll2_20 = alloca i64, align 8
  %ll3_21 = alloca i64, align 8
  %s1_7 = alloca i16, align 2
  %s2_8 = alloca i16, align 2
  %s3_9 = alloca i16, align 2
  %ui1_22 = alloca i32, align 4
  %ui2_23 = alloca i32, align 4
  %ui3_24 = alloca i32, align 4
  %ul1_25 = alloca i64, align 8
  %ul2_26 = alloca i64, align 8
  %ul3_27 = alloca i64, align 8
  %us1_10 = alloca i16, align 2
  %us2_11 = alloca i16, align 2
  %us3_12 = alloca i16, align 2
  %conv = trunc i32 1 to i8
  store i8 %conv, i8* %c1_4, align 1
  %conv1 = trunc i32 2 to i8
  store i8 %conv1, i8* %c2_5, align 1
  %0 = load i8, i8* %c1_4, align 1
  %conv3 = sext i8 %0 to i32
  %1 = load i8, i8* %c2_5, align 1
  %conv32 = sext i8 %1 to i32
  %mul = mul i32 %conv3, %conv32
  %conv4 = trunc i32 %mul to i8
  store i8 %conv4, i8* %c3_6, align 1
  %conv5 = trunc i32 1 to i16
  store i16 %conv5, i16* %s1_7, align 2
  %conv6 = trunc i32 2 to i16
  store i16 %conv6, i16* %s2_8, align 2
  %2 = load i16, i16* %s1_7, align 2
  %conv37 = sext i16 %2 to i32
  %3 = load i16, i16* %s2_8, align 2
  %conv38 = sext i16 %3 to i32
  %mul.9 = mul i32 %conv37, %conv38
  %conv10 = trunc i32 %mul.9 to i16
  store i16 %conv10, i16* %s3_9, align 2
  %conv11 = trunc i32 1 to i16
  store i16 %conv11, i16* %us1_10, align 2
  %conv12 = trunc i32 2 to i16
  store i16 %conv12, i16* %us2_11, align 2
  %4 = load i16, i16* %us1_10, align 2
  %conv313 = zext i16 %4 to i32
  %5 = load i16, i16* %us2_11, align 2
  %conv314 = zext i16 %5 to i32
  %mul.15 = mul i32 %conv313, %conv314
  %conv16 = trunc i32 %mul.15 to i16
  store i16 %conv16, i16* %us3_12, align 2
  store i32 1, i32* %i1_13, align 4
  store i32 2, i32* %i2_14, align 4
  %6 = load i32, i32* %i1_13, align 4
  %7 = load i32, i32* %i2_14, align 4
  %mul.17 = mul i32 %6, %7
  store i32 %mul.17, i32* %i3_15, align 4
  store i64 1, i64* %l1_16, align 8
  store i64 2, i64* %l2_17, align 8
  %8 = load i64, i64* %l1_16, align 8
  %9 = load i64, i64* %l2_17, align 8
  %mul.18 = mul i64 %8, %9
  store i64 %mul.18, i64* %l3_18, align 8
  store i64 1, i64* %ll1_19, align 8
  store i64 2, i64* %ll2_20, align 8
  %10 = load i64, i64* %ll1_19, align 8
  %11 = load i64, i64* %ll2_20, align 8
  %mul.19 = mul i64 %10, %11
  store i64 %mul.19, i64* %ll3_21, align 8
  store i32 1, i32* %ui1_22, align 4
  store i32 1, i32* %ui2_23, align 4
  %12 = load i32, i32* %ui1_22, align 4
  %13 = load i32, i32* %ui2_23, align 4
  %mul.20 = mul i32 %12, %13
  store i32 %mul.20, i32* %ui3_24, align 4
  store i64 1, i64* %ul1_25, align 8
  store i64 1, i64* %ul2_26, align 8
  %14 = load i64, i64* %ul1_25, align 8
  %15 = load i64, i64* %ul2_26, align 8
  %mul.21 = mul i64 %14, %15
  store i64 %mul.21, i64* %ul3_27, align 8
  store float 1.000000e+00, float* %f1_28, align 4
  store float 2.000000e+00, float* %f2_29, align 4
  %16 = load float, float* %f1_28, align 4
  %17 = load float, float* %f2_29, align 4
  %mul.22 = fmul float %16, %17
  store float %mul.22, float* %f3_30, align 4
  store double 1.000000e+00, double* %d1_31, align 8
  store double 2.000000e+00, double* %d2_32, align 8
  %18 = load double, double* %d1_31, align 8
  %19 = load double, double* %d2_32, align 8
  %mul.23 = fmul double %18, %19
  store double %mul.23, double* %d3_33, align 8
  %20 = bitcast i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str, i32 0, i32 0) to i8*
  %21 = load i8, i8* %c3_6, align 1
  %conv324 = sext i8 %21 to i32
  %22 = load i16, i16* %s3_9, align 2
  %conv325 = sext i16 %22 to i32
  %23 = load i32, i32* %i3_15, align 4
  %24 = load i64, i64* %l3_18, align 8
  %25 = load i64, i64* %ll3_21, align 8
  %26 = load i32, i32* %ui3_24, align 4
  %27 = load i64, i64* %ul3_27, align 8
  %call1 = call i32 (i8*, ...) @printf(i8* %20, i32 %conv324, i32 %conv325, i32 %23, i64 %24, i64 %25, i32 %26, i64 %27)
  %28 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %29 = load float, float* %f3_30, align 4
  %convDouble = fpext float %29 to double
  %30 = load double, double* %d3_33, align 8
  %call2 = call i32 (i8*, ...) @printf(i8* %28, double %convDouble, double %30)
  ret i32 0
}

declare i32 @printf(i8*, ...)
