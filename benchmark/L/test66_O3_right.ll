; ModuleID = 'test66.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.student = type { [50 x i8], i32, float }

@s = common global [10 x %struct.student] zeroinitializer, align 16
@.str.1 = private unnamed_addr constant [21 x i8] c"\0AFor roll number%d,\0A\00", align 1
@.str.2 = private unnamed_addr constant [13 x i8] c"Enter name: \00", align 1
@.str.3 = private unnamed_addr constant [3 x i8] c"%s\00", align 1
@.str.4 = private unnamed_addr constant [14 x i8] c"Enter marks: \00", align 1
@.str.5 = private unnamed_addr constant [3 x i8] c"%f\00", align 1
@.str.8 = private unnamed_addr constant [18 x i8] c"\0ARoll number: %d\0A\00", align 1
@.str.9 = private unnamed_addr constant [7 x i8] c"Name: \00", align 1
@.str.10 = private unnamed_addr constant [12 x i8] c"Marks: %.1f\00", align 1
@str = private unnamed_addr constant [31 x i8] c"Enter information of students:\00"
@str.11 = private unnamed_addr constant [25 x i8] c"Displaying Information:\0A\00"

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %puts = tail call i32 @puts(i8* getelementptr inbounds ([31 x i8], [31 x i8]* @str, i64 0, i64 0)) #2
  store i32 1, i32* getelementptr inbounds ([10 x %struct.student], [10 x %struct.student]* @s, i64 0, i64 0, i32 1), align 4, !tbaa !1
  %call4 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.1, i64 0, i64 0), i32 1) #2
  %call5 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.2, i64 0, i64 0)) #2
  %call8 = tail call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.3, i64 0, i64 0), i8* getelementptr inbounds ([10 x %struct.student], [10 x %struct.student]* @s, i64 0, i64 0, i32 0, i64 0)) #2
  %call9 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.4, i64 0, i64 0)) #2
  %call12 = tail call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.5, i64 0, i64 0), float* getelementptr inbounds ([10 x %struct.student], [10 x %struct.student]* @s, i64 0, i64 0, i32 2)) #2
  %putchar47 = tail call i32 @putchar(i32 10) #2
  store i32 2, i32* getelementptr inbounds ([10 x %struct.student], [10 x %struct.student]* @s, i64 0, i64 1, i32 1), align 4, !tbaa !1
  %call4.1 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.1, i64 0, i64 0), i32 2) #2
  %call5.1 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.2, i64 0, i64 0)) #2
  %call8.1 = tail call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.3, i64 0, i64 0), i8* getelementptr inbounds ([10 x %struct.student], [10 x %struct.student]* @s, i64 0, i64 1, i32 0, i64 0)) #2
  %call9.1 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.4, i64 0, i64 0)) #2
  %call12.1 = tail call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.5, i64 0, i64 0), float* getelementptr inbounds ([10 x %struct.student], [10 x %struct.student]* @s, i64 0, i64 1, i32 2)) #2
  %putchar47.1 = tail call i32 @putchar(i32 10) #2
  %puts46 = tail call i32 @puts(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @str.11, i64 0, i64 0)) #2
  %call19 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.8, i64 0, i64 0), i32 1) #2
  %call20 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.9, i64 0, i64 0)) #2
  %call25 = tail call i32 @puts(i8* getelementptr inbounds ([10 x %struct.student], [10 x %struct.student]* @s, i64 0, i64 0, i32 0, i64 0)) #2
  %0 = load float, float* getelementptr inbounds ([10 x %struct.student], [10 x %struct.student]* @s, i64 0, i64 0, i32 2), align 8, !tbaa !7
  %conv = fpext float %0 to double
  %call29 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.10, i64 0, i64 0), double %conv) #2
  %putchar = tail call i32 @putchar(i32 10) #2
  %call19.1 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.8, i64 0, i64 0), i32 2) #2
  %call20.1 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.9, i64 0, i64 0)) #2
  %call25.1 = tail call i32 @puts(i8* getelementptr inbounds ([10 x %struct.student], [10 x %struct.student]* @s, i64 0, i64 1, i32 0, i64 0)) #2
  %1 = load float, float* getelementptr inbounds ([10 x %struct.student], [10 x %struct.student]* @s, i64 0, i64 1, i32 2), align 4, !tbaa !7
  %conv.1 = fpext float %1 to double
  %call29.1 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.10, i64 0, i64 0), double %conv.1) #2
  %putchar.1 = tail call i32 @putchar(i32 10) #2
  ret i32 0
}

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #1

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #1

; Function Attrs: nounwind
declare i32 @puts(i8* nocapture readonly) #1

; Function Attrs: nounwind
declare i32 @putchar(i32) #2

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !5, i64 52}
!2 = !{!"student", !3, i64 0, !5, i64 52, !6, i64 56}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = !{!"int", !3, i64 0}
!6 = !{!"float", !3, i64 0}
!7 = !{!2, !6, i64 56}
