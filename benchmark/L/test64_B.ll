; ModuleID = 'test64.bc'

%struct.complex = type { float, float }

@.str = private constant [25 x i8] c"For 1st complex number \0A\00", align 1
@.str.1 = private constant [45 x i8] c"Enter real and imaginary part respectively:\0A\00", align 1
@.str.2 = private constant [6 x i8] c"%f %f\00", align 1
@.str.3 = private constant [26 x i8] c"\0AFor 2nd complex number \0A\00", align 1
@.str.4 = private constant [19 x i8] c"Sum = %.1f + %.1fi\00", align 1

define i32 @main() {
entry:
  %.anon_1_13 = alloca %struct.complex, align 4
  %_temp_.Mreturn._Z3add7complexS_6_14 = alloca %struct.complex, align 4
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %_temp_dummy14_11 = alloca i32, align 4
  %_temp_dummy15_12 = alloca i32, align 4
  %_temp_dummy17_15 = alloca i32, align 4
  %n1_4 = alloca %struct.complex, align 4
  %n2_5 = alloca %struct.complex, align 4
  %temp_6 = alloca %struct.complex, align 4
  %0 = bitcast i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([45 x i8], [45 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call2 = call i32 (i8*, ...) @printf(i8* %1)
  %2 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.2, i32 0, i32 0) to i8*
  %real = getelementptr %struct.complex, %struct.complex* %n1_4, i32 0, i32 0
  %imag = getelementptr %struct.complex, %struct.complex* %n1_4, i32 0, i32 1
  %3 = bitcast float* %real to float*
  %real1 = getelementptr %struct.complex, %struct.complex* %n1_4, i32 0, i32 0
  %imag2 = getelementptr %struct.complex, %struct.complex* %n1_4, i32 0, i32 1
  %4 = bitcast float* %imag2 to float*
  %call3 = call i32 (i8*, ...) @scanf(i8* %2, float* %3, float* %4)
  %5 = bitcast i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.3, i32 0, i32 0) to i8*
  %call4 = call i32 (i8*, ...) @printf(i8* %5)
  %6 = bitcast i8* getelementptr inbounds ([45 x i8], [45 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call5 = call i32 (i8*, ...) @printf(i8* %6)
  %7 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.2, i32 0, i32 0) to i8*
  %real3 = getelementptr %struct.complex, %struct.complex* %n2_5, i32 0, i32 0
  %imag4 = getelementptr %struct.complex, %struct.complex* %n2_5, i32 0, i32 1
  %8 = bitcast float* %real3 to float*
  %real5 = getelementptr %struct.complex, %struct.complex* %n2_5, i32 0, i32 0
  %imag6 = getelementptr %struct.complex, %struct.complex* %n2_5, i32 0, i32 1
  %9 = bitcast float* %imag6 to float*
  %call6 = call i32 (i8*, ...) @scanf(i8* %7, float* %8, float* %9)
  %10 = load %struct.complex, %struct.complex* %n1_4
  %11 = load %struct.complex, %struct.complex* %n2_5
  %call7 = call %struct.complex @_Z3add7complexS_(%struct.complex %10, %struct.complex %11)
  store %struct.complex %call7, %struct.complex* %_temp_.Mreturn._Z3add7complexS_6_14
  %12 = load %struct.complex, %struct.complex* %_temp_.Mreturn._Z3add7complexS_6_14
  store %struct.complex %12, %struct.complex* %.anon_1_13
  %13 = load %struct.complex, %struct.complex* %.anon_1_13
  store %struct.complex %13, %struct.complex* %temp_6
  %14 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.4, i32 0, i32 0) to i8*
  %real7 = getelementptr %struct.complex, %struct.complex* %temp_6, i32 0, i32 0
  %imag8 = getelementptr %struct.complex, %struct.complex* %temp_6, i32 0, i32 1
  %15 = load float, float* %real7, align 4
  %convDouble = fpext float %15 to double
  %real9 = getelementptr %struct.complex, %struct.complex* %temp_6, i32 0, i32 0
  %imag10 = getelementptr %struct.complex, %struct.complex* %temp_6, i32 0, i32 1
  %16 = load float, float* %imag10, align 4
  %convDouble11 = fpext float %16 to double
  %call8 = call i32 (i8*, ...) @printf(i8* %14, double %convDouble, double %convDouble11)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define %struct.complex @_Z3add7complexS_(%struct.complex %n1_4, %struct.complex %n2_5) {
entry:
  %.anon_2_7 = alloca %struct.complex, align 4
  %n1_4.addr = alloca %struct.complex, align 4
  %n2_5.addr = alloca %struct.complex, align 4
  %temp_6 = alloca %struct.complex, align 4
  store %struct.complex %n1_4, %struct.complex* %n1_4.addr, align 4
  store %struct.complex %n2_5, %struct.complex* %n2_5.addr, align 4
  %real = getelementptr %struct.complex, %struct.complex* %n1_4.addr, i32 0, i32 0
  %imag = getelementptr %struct.complex, %struct.complex* %n1_4.addr, i32 0, i32 1
  %0 = load float, float* %real, align 4
  %real1 = getelementptr %struct.complex, %struct.complex* %n2_5.addr, i32 0, i32 0
  %imag2 = getelementptr %struct.complex, %struct.complex* %n2_5.addr, i32 0, i32 1
  %1 = load float, float* %real1, align 4
  %add = fadd float %0, %1
  %real3 = getelementptr %struct.complex, %struct.complex* %temp_6, i32 0, i32 0
  %imag4 = getelementptr %struct.complex, %struct.complex* %temp_6, i32 0, i32 1
  store float %add, float* %real3, align 4
  %real5 = getelementptr %struct.complex, %struct.complex* %n1_4.addr, i32 0, i32 0
  %imag6 = getelementptr %struct.complex, %struct.complex* %n1_4.addr, i32 0, i32 1
  %2 = load float, float* %imag6, align 4
  %real7 = getelementptr %struct.complex, %struct.complex* %n2_5.addr, i32 0, i32 0
  %imag8 = getelementptr %struct.complex, %struct.complex* %n2_5.addr, i32 0, i32 1
  %3 = load float, float* %imag8, align 4
  %add.9 = fadd float %2, %3
  %real10 = getelementptr %struct.complex, %struct.complex* %temp_6, i32 0, i32 0
  %imag11 = getelementptr %struct.complex, %struct.complex* %temp_6, i32 0, i32 1
  store float %add.9, float* %imag11, align 4
  %4 = load %struct.complex, %struct.complex* %temp_6
  ret %struct.complex %4
}
