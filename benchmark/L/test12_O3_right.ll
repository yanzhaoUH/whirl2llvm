; ModuleID = 'test12.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [32 x i8] c"Enter coefficients a, b and c: \00", align 1
@.str.1 = private unnamed_addr constant [12 x i8] c"%lf %lf %lf\00", align 1
@.str.2 = private unnamed_addr constant [32 x i8] c"root1 = %.2lf and root2 = %.2lf\00", align 1
@.str.3 = private unnamed_addr constant [23 x i8] c"root1 = root2 = %.2lf;\00", align 1
@.str.4 = private unnamed_addr constant [44 x i8] c"root1 = %.2lf+%.2lfi and root2 = %.2f-%.2fi\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %a = alloca double, align 8
  %b = alloca double, align 8
  %c = alloca double, align 8
  %0 = bitcast double* %a to i8*
  call void @llvm.lifetime.start(i64 8, i8* %0) #1
  %1 = bitcast double* %b to i8*
  call void @llvm.lifetime.start(i64 8, i8* %1) #1
  %2 = bitcast double* %c to i8*
  call void @llvm.lifetime.start(i64 8, i8* %2) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i64 0, i64 0), double* nonnull %a, double* nonnull %b, double* nonnull %c) #1
  %3 = load double, double* %b, align 8, !tbaa !1
  %mul = fmul double %3, %3
  %4 = load double, double* %a, align 8, !tbaa !1
  %mul2 = fmul double %4, 4.000000e+00
  %5 = load double, double* %c, align 8, !tbaa !1
  %mul3 = fmul double %mul2, %5
  %sub = fsub double %mul, %mul3
  %cmp = fcmp ogt double %sub, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %call5 = call double @sqrt(double %4) #1
  %add = fsub double %call5, %3
  %6 = load double, double* %a, align 8, !tbaa !1
  %mul6 = fmul double %6, 2.000000e+00
  %div = fdiv double %add, %mul6
  %7 = load double, double* %b, align 8, !tbaa !1
  %sub7 = fsub double -0.000000e+00, %7
  %call8 = call double @sqrt(double %sub) #1
  %sub9 = fsub double %sub7, %call8
  %8 = load double, double* %a, align 8, !tbaa !1
  %mul10 = fmul double %8, 2.000000e+00
  %div11 = fdiv double %sub9, %mul10
  %call12 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.2, i64 0, i64 0), double %div, double %div11) #1
  br label %if.end.28

if.else:                                          ; preds = %entry
  %cmp13 = fcmp oeq double %sub, 0.000000e+00
  %sub15 = fsub double -0.000000e+00, %3
  %mul16 = fmul double %4, 2.000000e+00
  %div17 = fdiv double %sub15, %mul16
  br i1 %cmp13, label %if.then.14, label %if.else.19

if.then.14:                                       ; preds = %if.else
  %call18 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.3, i64 0, i64 0), double %div17) #1
  br label %if.end.28

if.else.19:                                       ; preds = %if.else
  %sub23 = fsub double -0.000000e+00, %sub
  %call24 = call double @sqrt(double %sub23) #1
  %9 = load double, double* %a, align 8, !tbaa !1
  %mul25 = fmul double %9, 2.000000e+00
  %div26 = fdiv double %call24, %mul25
  %call27 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([44 x i8], [44 x i8]* @.str.4, i64 0, i64 0), double %div17, double %div26, double %div17, double %div26) #1
  br label %if.end.28

if.end.28:                                        ; preds = %if.then.14, %if.else.19, %if.then
  call void @llvm.lifetime.end(i64 8, i8* %2) #1
  call void @llvm.lifetime.end(i64 8, i8* %1) #1
  call void @llvm.lifetime.end(i64 8, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare double @sqrt(double) #2

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"double", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
