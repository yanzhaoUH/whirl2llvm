; ModuleID = 'test55.bc'

@.str = private constant [17 x i8] c"Enter a string: \00", align 1
@.str.1 = private constant [42 x i8] c"Enter a character to find the frequency: \00", align 1
@.str.2 = private constant [4 x i8] c" %c\00", align 1
@.str.3 = private constant [21 x i8] c"Frequency of %c = %d\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_8 = alloca i32, align 4
  %_temp_dummy11_9 = alloca i32, align 4
  %_temp_dummy12_10 = alloca i32, align 4
  %_temp_dummy13_11 = alloca i32, align 4
  %_temp_dummy14_12 = alloca i32, align 4
  %ch_5 = alloca i8, align 1
  %frequency_7 = alloca i32, align 4
  %i_6 = alloca i32, align 4
  %str_4 = alloca [1000 x i8], align 1
  store i32 0, i32* %frequency_7, align 4
  %0 = bitcast i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %arrayAddress = getelementptr [1000 x i8], [1000 x i8]* %str_4, i32 0, i32 0
  %1 = bitcast i8* %arrayAddress to i8*
  %call2 = call i8* @gets(i8* %1)
  %2 = bitcast i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %2)
  %3 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.2, i32 0, i32 0) to i8*
  %4 = bitcast i8* %ch_5 to i8*
  %call4 = call i32 (i8*, ...) @scanf(i8* %3, i8* %4)
  store i32 0, i32* %i_6, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %5 = load i32, i32* %i_6, align 4
  %conv3 = sext i32 %5 to i64
  %arrayidx = getelementptr [1000 x i8], [1000 x i8]* %str_4, i32 0, i64 %conv3
  %6 = load i8, i8* %arrayidx, align 1
  %conv31 = sext i8 %6 to i32
  %cmp1 = icmp ne i32 %conv31, 0
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = load i32, i32* %i_6, align 4
  %conv32 = sext i32 %7 to i64
  %arrayidx3 = getelementptr [1000 x i8], [1000 x i8]* %str_4, i32 0, i64 %conv32
  %8 = load i8, i8* %arrayidx3, align 1
  %conv34 = sext i8 %8 to i32
  %9 = load i8, i8* %ch_5, align 1
  %conv35 = sext i8 %9 to i32
  %cmp2 = icmp eq i32 %conv34, %conv35
  br i1 %cmp2, label %if.then, label %if.else

while.end:                                        ; preds = %while.cond
  %10 = bitcast i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.3, i32 0, i32 0) to i8*
  %11 = load i8, i8* %ch_5, align 1
  %conv37 = sext i8 %11 to i32
  %12 = load i32, i32* %frequency_7, align 4
  %call5 = call i32 (i8*, ...) @printf(i8* %10, i32 %conv37, i32 %12)
  ret i32 0

if.then:                                          ; preds = %while.body
  %13 = load i32, i32* %frequency_7, align 4
  %add = add i32 %13, 1
  store i32 %add, i32* %frequency_7, align 4
  br label %if.end

if.else:                                          ; preds = %while.body
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %14 = load i32, i32* %i_6, align 4
  %add.6 = add i32 %14, 1
  store i32 %add.6, i32* %i_6, align 4
  br label %while.cond
}

declare i32 @printf(i8*, ...)

declare i8* @gets(i8*)

declare i32 @scanf(i8*, ...)
