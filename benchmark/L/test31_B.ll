; ModuleID = 'test31.bc'

@.str = private constant [27 x i8] c"Enter a positive integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [20 x i8] c"Factors of %d are: \00", align 1
@.str.3 = private constant [4 x i8] c"%d \00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %_temp_dummy13_9 = alloca i32, align 4
  %i_5 = alloca i32, align 4
  %number_4 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %number_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = bitcast i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.2, i32 0, i32 0) to i8*
  %4 = load i32, i32* %number_4, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %3, i32 %4)
  store i32 1, i32* %i_5, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %5 = load i32, i32* %i_5, align 4
  %6 = load i32, i32* %number_4, align 4
  %cmp1 = icmp sle i32 %5, %6
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = load i32, i32* %number_4, align 4
  %8 = load i32, i32* %i_5, align 4
  %srem = srem i32 %7, %8
  %cmp2 = icmp eq i32 %srem, 0
  br i1 %cmp2, label %if.then, label %if.else

while.end:                                        ; preds = %while.cond
  ret i32 0

if.then:                                          ; preds = %while.body
  %9 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.3, i32 0, i32 0) to i8*
  %10 = load i32, i32* %i_5, align 4
  %call4 = call i32 (i8*, ...) @printf(i8* %9, i32 %10)
  br label %if.end

if.else:                                          ; preds = %while.body
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %11 = load i32, i32* %i_5, align 4
  %add = add i32 %11, 1
  store i32 %add, i32* %i_5, align 4
  br label %while.cond
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
