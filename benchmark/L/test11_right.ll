; ModuleID = 'test11.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [12 x i8] c"%lf %lf %lf\00", align 1
@.str.1 = private unnamed_addr constant [28 x i8] c"%.2f is the largest number.\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %n1 = alloca double, align 8
  %n2 = alloca double, align 8
  %n3 = alloca double, align 8
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), double* %n1, double* %n2, double* %n3)
  %0 = load double, double* %n1, align 8
  %1 = load double, double* %n2, align 8
  %cmp = fcmp oge double %0, %1
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %2 = load double, double* %n1, align 8
  %3 = load double, double* %n3, align 8
  %cmp1 = fcmp oge double %2, %3
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %4 = load double, double* %n1, align 8
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.1, i32 0, i32 0), double %4)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  %5 = load double, double* %n2, align 8
  %6 = load double, double* %n1, align 8
  %cmp3 = fcmp oge double %5, %6
  br i1 %cmp3, label %land.lhs.true.4, label %if.end.8

land.lhs.true.4:                                  ; preds = %if.end
  %7 = load double, double* %n2, align 8
  %8 = load double, double* %n3, align 8
  %cmp5 = fcmp oge double %7, %8
  br i1 %cmp5, label %if.then.6, label %if.end.8

if.then.6:                                        ; preds = %land.lhs.true.4
  %9 = load double, double* %n2, align 8
  %call7 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.1, i32 0, i32 0), double %9)
  br label %if.end.8

if.end.8:                                         ; preds = %if.then.6, %land.lhs.true.4, %if.end
  %10 = load double, double* %n3, align 8
  %11 = load double, double* %n1, align 8
  %cmp9 = fcmp oge double %10, %11
  br i1 %cmp9, label %land.lhs.true.10, label %if.end.14

land.lhs.true.10:                                 ; preds = %if.end.8
  %12 = load double, double* %n3, align 8
  %13 = load double, double* %n2, align 8
  %cmp11 = fcmp oge double %12, %13
  br i1 %cmp11, label %if.then.12, label %if.end.14

if.then.12:                                       ; preds = %land.lhs.true.10
  %14 = load double, double* %n3, align 8
  %call13 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.1, i32 0, i32 0), double %14)
  br label %if.end.14

if.end.14:                                        ; preds = %if.then.12, %land.lhs.true.10, %if.end.8
  ret i32 0
}

declare i32 @__isoc99_scanf(i8*, ...) #1

declare i32 @printf(i8*, ...) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
