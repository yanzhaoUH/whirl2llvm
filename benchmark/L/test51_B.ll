; ModuleID = 'test51.bc'

@.str = private constant [41 x i8] c"Enter rows and column for first matrix: \00", align 1
@.str.1 = private constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private constant [42 x i8] c"Enter rows and column for second matrix: \00", align 1
@.str.3 = private constant [59 x i8] c"Error! column of first matrix not equal to row of second.\0A\00", align 1
@.str.4 = private constant [5 x i8] c"%d%d\00", align 1
@.str.5 = private constant [17 x i8] c"\0AOutput Matrix:\0A\00", align 1
@.str.6 = private constant [5 x i8] c"%d  \00", align 1
@.str.7 = private constant [3 x i8] c"\0A\0A\00", align 1
@.str.8 = private constant [30 x i8] c"\0AEnter elements of matrix 1:\0A\00", align 1
@.str.9 = private constant [23 x i8] c"Enter elements a%d%d: \00", align 1
@.str.10 = private constant [30 x i8] c"\0AEnter elements of matrix 2:\0A\00", align 1
@.str.11 = private constant [23 x i8] c"Enter elements b%d%d: \00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_14 = alloca i32, align 4
  %_temp_dummy110_24 = alloca i32, align 4
  %_temp_dummy11_15 = alloca i32, align 4
  %_temp_dummy12_16 = alloca i32, align 4
  %_temp_dummy13_17 = alloca i32, align 4
  %_temp_dummy14_18 = alloca i32, align 4
  %_temp_dummy15_19 = alloca i32, align 4
  %_temp_dummy16_20 = alloca i32, align 4
  %_temp_dummy17_21 = alloca i32, align 4
  %_temp_dummy18_22 = alloca i32, align 4
  %_temp_dummy19_23 = alloca i32, align 4
  %columnFirst_8 = alloca i32, align 4
  %columnSecond_10 = alloca i32, align 4
  %firstMatrix_4 = alloca [10 x [10 x i32]], align 4
  %i_11 = alloca i32, align 4
  %j_12 = alloca i32, align 4
  %k_13 = alloca i32, align 4
  %mult_6 = alloca [10 x [10 x i32]], align 4
  %rowFirst_7 = alloca i32, align 4
  %rowSecond_9 = alloca i32, align 4
  %secondMatrix_5 = alloca [10 x [10 x i32]], align 4
  %0 = bitcast i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %rowFirst_7 to i32*
  %3 = bitcast i32* %columnFirst_8 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2, i32* %3)
  %4 = bitcast i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %4)
  %5 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %6 = bitcast i32* %rowSecond_9 to i32*
  %7 = bitcast i32* %columnSecond_10 to i32*
  %call4 = call i32 (i8*, ...) @scanf(i8* %5, i32* %6, i32* %7)
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %8 = load i32, i32* %columnFirst_8, align 4
  %9 = load i32, i32* %rowSecond_9, align 4
  %cmp1 = icmp ne i32 %8, %9
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %10 = bitcast i8* getelementptr inbounds ([59 x i8], [59 x i8]* @.str.3, i32 0, i32 0) to i8*
  %call5 = call i32 (i8*, ...) @printf(i8* %10)
  %11 = bitcast i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str, i32 0, i32 0) to i8*
  %call6 = call i32 (i8*, ...) @printf(i8* %11)
  %12 = bitcast i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.4, i32 0, i32 0) to i8*
  %13 = bitcast i32* %rowFirst_7 to i32*
  %14 = bitcast i32* %columnFirst_8 to i32*
  %call7 = call i32 (i8*, ...) @scanf(i8* %12, i32* %13, i32* %14)
  %15 = bitcast i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call8 = call i32 (i8*, ...) @printf(i8* %15)
  %16 = bitcast i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.4, i32 0, i32 0) to i8*
  %17 = bitcast i32* %rowSecond_9 to i32*
  %18 = bitcast i32* %columnSecond_10 to i32*
  %call9 = call i32 (i8*, ...) @scanf(i8* %16, i32* %17, i32* %18)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %arrayAddress = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %firstMatrix_4, i32 0, i32 0
  %19 = bitcast [10 x i32]* %arrayAddress to [10 x i32]*
  %arrayAddress1 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %secondMatrix_5, i32 0, i32 0
  %20 = bitcast [10 x i32]* %arrayAddress1 to [10 x i32]*
  %21 = load i32, i32* %rowFirst_7, align 4
  %22 = load i32, i32* %columnFirst_8, align 4
  %23 = load i32, i32* %rowSecond_9, align 4
  %24 = load i32, i32* %columnSecond_10, align 4
  call void @_Z9enterDataPA10_iS0_iiii([10 x i32]* %19, [10 x i32]* %20, i32 %21, i32 %22, i32 %23, i32 %24)
  %arrayAddress2 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %firstMatrix_4, i32 0, i32 0
  %25 = bitcast [10 x i32]* %arrayAddress2 to [10 x i32]*
  %arrayAddress3 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %secondMatrix_5, i32 0, i32 0
  %26 = bitcast [10 x i32]* %arrayAddress3 to [10 x i32]*
  %arrayAddress4 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %mult_6, i32 0, i32 0
  %27 = bitcast [10 x i32]* %arrayAddress4 to [10 x i32]*
  %28 = load i32, i32* %rowFirst_7, align 4
  %29 = load i32, i32* %columnFirst_8, align 4
  %30 = load i32, i32* %rowSecond_9, align 4
  %31 = load i32, i32* %columnSecond_10, align 4
  call void @_Z16multiplyMatricesPA10_iS0_S0_iiii([10 x i32]* %25, [10 x i32]* %26, [10 x i32]* %27, i32 %28, i32 %29, i32 %30, i32 %31)
  %arrayAddress5 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %mult_6, i32 0, i32 0
  %32 = bitcast [10 x i32]* %arrayAddress5 to [10 x i32]*
  %33 = load i32, i32* %rowFirst_7, align 4
  %34 = load i32, i32* %columnSecond_10, align 4
  call void @_Z7displayPA10_iii([10 x i32]* %32, i32 %33, i32 %34)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define void @_Z9enterDataPA10_iS0_iiii([10 x i32]* %firstMatrix_4, [10 x i32]* %secondMatrix_5, i32 %rowFirst_6, i32 %columnFirst_7, i32 %rowSecond_8, i32 %columnSecond_9) {
entry:
  %_temp_dummy114_12 = alloca i32, align 4
  %_temp_dummy115_13 = alloca i32, align 4
  %_temp_dummy116_14 = alloca i32, align 4
  %_temp_dummy117_15 = alloca i32, align 4
  %columnFirst_7.addr = alloca i32, align 4
  %columnSecond_9.addr = alloca i32, align 4
  %firstMatrix_4.addr = alloca [10 x i32]*, align 4
  %i_10 = alloca i32, align 4
  %j_11 = alloca i32, align 4
  %rowFirst_6.addr = alloca i32, align 4
  %rowSecond_8.addr = alloca i32, align 4
  %secondMatrix_5.addr = alloca [10 x i32]*, align 4
  store [10 x i32]* %firstMatrix_4, [10 x i32]** %firstMatrix_4.addr, align 4
  store [10 x i32]* %secondMatrix_5, [10 x i32]** %secondMatrix_5.addr, align 4
  store i32 %rowFirst_6, i32* %rowFirst_6.addr, align 4
  store i32 %columnFirst_7, i32* %columnFirst_7.addr, align 4
  store i32 %rowSecond_8, i32* %rowSecond_8.addr, align 4
  store i32 %columnSecond_9, i32* %columnSecond_9.addr, align 4
  %0 = bitcast i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.8, i32 0, i32 0) to i8*
  %call16 = call i32 (i8*, ...) @printf(i8* %0)
  store i32 0, i32* %i_10, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.end3, %entry
  %1 = load i32, i32* %i_10, align 4
  %2 = load i32, i32* %rowFirst_6.addr, align 4
  %cmp10 = icmp slt i32 %1, %2
  br i1 %cmp10, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  store i32 0, i32* %j_11, align 4
  br label %while.cond1

while.end:                                        ; preds = %while.cond
  %3 = bitcast i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.10, i32 0, i32 0) to i8*
  %call18 = call i32 (i8*, ...) @printf(i8* %3)
  store i32 0, i32* %i_10, align 4
  br label %while.cond12

while.cond1:                                      ; preds = %while.body2, %while.body
  %4 = load i32, i32* %j_11, align 4
  %5 = load i32, i32* %columnFirst_7.addr, align 4
  %cmp11 = icmp slt i32 %4, %5
  br i1 %cmp11, label %while.body2, label %while.end3

while.body2:                                      ; preds = %while.cond1
  %6 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.9, i32 0, i32 0) to i8*
  %7 = load i32, i32* %i_10, align 4
  %add = add i32 %7, 1
  %8 = load i32, i32* %j_11, align 4
  %add.4 = add i32 %8, 1
  %call17 = call i32 (i8*, ...) @printf(i8* %6, i32 %add, i32 %add.4)
  %9 = load i32, i32* %j_11, align 4
  %conv3 = sext i32 %9 to i64
  %10 = load i32, i32* %i_10, align 4
  %conv35 = sext i32 %10 to i64
  %mul = mul i64 %conv35, 40
  %11 = load [10 x i32]*, [10 x i32]** %firstMatrix_4.addr, align 8
  %12 = ptrtoint [10 x i32]* %11 to i64
  %add.6 = add i64 %mul, %12
  %13 = inttoptr i64 %add.6 to i32*
  %arrayidx = getelementptr i32, i32* %13, i64 %conv3
  %14 = load i32, i32* %i_10, align 4
  %add.7 = add i32 %14, 1
  %15 = load i32, i32* %j_11, align 4
  %add.8 = add i32 %15, 1
  %mul.9 = mul i32 %add.7, %add.8
  store i32 %mul.9, i32* %arrayidx, align 4
  %16 = load i32, i32* %j_11, align 4
  %add.10 = add i32 %16, 1
  store i32 %add.10, i32* %j_11, align 4
  br label %while.cond1

while.end3:                                       ; preds = %while.cond1
  %17 = load i32, i32* %i_10, align 4
  %add.11 = add i32 %17, 1
  store i32 %add.11, i32* %i_10, align 4
  br label %while.cond

while.cond12:                                     ; preds = %while.end17, %while.end
  %18 = load i32, i32* %i_10, align 4
  %19 = load i32, i32* %rowSecond_8.addr, align 4
  %cmp12 = icmp slt i32 %18, %19
  br i1 %cmp12, label %while.body13, label %while.end14

while.body13:                                     ; preds = %while.cond12
  store i32 0, i32* %j_11, align 4
  br label %while.cond15

while.end14:                                      ; preds = %while.cond12
  ret void

while.cond15:                                     ; preds = %while.body16, %while.body13
  %20 = load i32, i32* %j_11, align 4
  %21 = load i32, i32* %columnSecond_9.addr, align 4
  %cmp13 = icmp slt i32 %20, %21
  br i1 %cmp13, label %while.body16, label %while.end17

while.body16:                                     ; preds = %while.cond15
  %22 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.11, i32 0, i32 0) to i8*
  %23 = load i32, i32* %i_10, align 4
  %add.18 = add i32 %23, 1
  %24 = load i32, i32* %j_11, align 4
  %add.19 = add i32 %24, 1
  %call19 = call i32 (i8*, ...) @printf(i8* %22, i32 %add.18, i32 %add.19)
  %25 = load i32, i32* %j_11, align 4
  %conv320 = sext i32 %25 to i64
  %26 = load i32, i32* %i_10, align 4
  %conv321 = sext i32 %26 to i64
  %mul.22 = mul i64 %conv321, 40
  %27 = load [10 x i32]*, [10 x i32]** %secondMatrix_5.addr, align 8
  %28 = ptrtoint [10 x i32]* %27 to i64
  %add.23 = add i64 %mul.22, %28
  %29 = inttoptr i64 %add.23 to i32*
  %arrayidx24 = getelementptr i32, i32* %29, i64 %conv320
  %30 = load i32, i32* %i_10, align 4
  %add.25 = add i32 %30, 1
  %31 = load i32, i32* %j_11, align 4
  %add.26 = add i32 %31, 1
  %mul.27 = mul i32 %add.25, %add.26
  store i32 %mul.27, i32* %arrayidx24, align 4
  %32 = load i32, i32* %j_11, align 4
  %add.28 = add i32 %32, 1
  store i32 %add.28, i32* %j_11, align 4
  br label %while.cond15

while.end17:                                      ; preds = %while.cond15
  %33 = load i32, i32* %i_10, align 4
  %add.29 = add i32 %33, 1
  store i32 %add.29, i32* %i_10, align 4
  br label %while.cond12
}

define void @_Z16multiplyMatricesPA10_iS0_S0_iiii([10 x i32]* %firstMatrix_4, [10 x i32]* %secondMatrix_5, [10 x i32]* %mult_6, i32 %rowFirst_7, i32 %columnFirst_8, i32 %rowSecond_9, i32 %columnSecond_10) {
entry:
  %columnFirst_8.addr = alloca i32, align 4
  %columnSecond_10.addr = alloca i32, align 4
  %firstMatrix_4.addr = alloca [10 x i32]*, align 4
  %i_11 = alloca i32, align 4
  %j_12 = alloca i32, align 4
  %k_13 = alloca i32, align 4
  %mult_6.addr = alloca [10 x i32]*, align 4
  %rowFirst_7.addr = alloca i32, align 4
  %rowSecond_9.addr = alloca i32, align 4
  %secondMatrix_5.addr = alloca [10 x i32]*, align 4
  store [10 x i32]* %firstMatrix_4, [10 x i32]** %firstMatrix_4.addr, align 4
  store [10 x i32]* %secondMatrix_5, [10 x i32]** %secondMatrix_5.addr, align 4
  store [10 x i32]* %mult_6, [10 x i32]** %mult_6.addr, align 4
  store i32 %rowFirst_7, i32* %rowFirst_7.addr, align 4
  store i32 %columnFirst_8, i32* %columnFirst_8.addr, align 4
  store i32 %rowSecond_9, i32* %rowSecond_9.addr, align 4
  store i32 %columnSecond_10, i32* %columnSecond_10.addr, align 4
  store i32 0, i32* %i_11, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.end3, %entry
  %0 = load i32, i32* %i_11, align 4
  %1 = load i32, i32* %rowFirst_7.addr, align 4
  %cmp5 = icmp slt i32 %0, %1
  br i1 %cmp5, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  store i32 0, i32* %j_12, align 4
  br label %while.cond1

while.end:                                        ; preds = %while.cond
  store i32 0, i32* %i_11, align 4
  br label %while.cond7

while.cond1:                                      ; preds = %while.body2, %while.body
  %2 = load i32, i32* %j_12, align 4
  %3 = load i32, i32* %columnSecond_10.addr, align 4
  %cmp6 = icmp slt i32 %2, %3
  br i1 %cmp6, label %while.body2, label %while.end3

while.body2:                                      ; preds = %while.cond1
  %4 = load i32, i32* %j_12, align 4
  %conv3 = sext i32 %4 to i64
  %5 = load i32, i32* %i_11, align 4
  %conv34 = sext i32 %5 to i64
  %mul = mul i64 %conv34, 40
  %6 = load [10 x i32]*, [10 x i32]** %mult_6.addr, align 8
  %7 = ptrtoint [10 x i32]* %6 to i64
  %add = add i64 %mul, %7
  %8 = inttoptr i64 %add to i32*
  %arrayidx = getelementptr i32, i32* %8, i64 %conv3
  store i32 0, i32* %arrayidx, align 4
  %9 = load i32, i32* %j_12, align 4
  %add.5 = add i32 %9, 1
  store i32 %add.5, i32* %j_12, align 4
  br label %while.cond1

while.end3:                                       ; preds = %while.cond1
  %10 = load i32, i32* %i_11, align 4
  %add.6 = add i32 %10, 1
  store i32 %add.6, i32* %i_11, align 4
  br label %while.cond

while.cond7:                                      ; preds = %while.end12, %while.end
  %11 = load i32, i32* %i_11, align 4
  %12 = load i32, i32* %rowFirst_7.addr, align 4
  %cmp7 = icmp slt i32 %11, %12
  br i1 %cmp7, label %while.body8, label %while.end9

while.body8:                                      ; preds = %while.cond7
  store i32 0, i32* %j_12, align 4
  br label %while.cond10

while.end9:                                       ; preds = %while.cond7
  ret void

while.cond10:                                     ; preds = %while.end15, %while.body8
  %13 = load i32, i32* %j_12, align 4
  %14 = load i32, i32* %columnSecond_10.addr, align 4
  %cmp8 = icmp slt i32 %13, %14
  br i1 %cmp8, label %while.body11, label %while.end12

while.body11:                                     ; preds = %while.cond10
  store i32 0, i32* %k_13, align 4
  br label %while.cond13

while.end12:                                      ; preds = %while.cond10
  %15 = load i32, i32* %i_11, align 4
  %add.40 = add i32 %15, 1
  store i32 %add.40, i32* %i_11, align 4
  br label %while.cond7

while.cond13:                                     ; preds = %while.body14, %while.body11
  %16 = load i32, i32* %k_13, align 4
  %17 = load i32, i32* %columnFirst_8.addr, align 4
  %cmp9 = icmp slt i32 %16, %17
  br i1 %cmp9, label %while.body14, label %while.end15

while.body14:                                     ; preds = %while.cond13
  %18 = load i32, i32* %j_12, align 4
  %conv316 = sext i32 %18 to i64
  %19 = load i32, i32* %i_11, align 4
  %conv317 = sext i32 %19 to i64
  %mul.18 = mul i64 %conv317, 40
  %20 = load [10 x i32]*, [10 x i32]** %mult_6.addr, align 8
  %21 = ptrtoint [10 x i32]* %20 to i64
  %add.19 = add i64 %mul.18, %21
  %22 = inttoptr i64 %add.19 to i32*
  %arrayidx20 = getelementptr i32, i32* %22, i64 %conv316
  %23 = load i32, i32* %j_12, align 4
  %conv321 = sext i32 %23 to i64
  %24 = load i32, i32* %i_11, align 4
  %conv322 = sext i32 %24 to i64
  %mul.23 = mul i64 %conv322, 40
  %25 = load [10 x i32]*, [10 x i32]** %mult_6.addr, align 8
  %26 = ptrtoint [10 x i32]* %25 to i64
  %add.24 = add i64 %mul.23, %26
  %27 = inttoptr i64 %add.24 to i32*
  %arrayidx25 = getelementptr i32, i32* %27, i64 %conv321
  %28 = load i32, i32* %arrayidx25, align 4
  %29 = load i32, i32* %k_13, align 4
  %conv326 = sext i32 %29 to i64
  %30 = load i32, i32* %i_11, align 4
  %conv327 = sext i32 %30 to i64
  %mul.28 = mul i64 %conv327, 40
  %31 = load [10 x i32]*, [10 x i32]** %firstMatrix_4.addr, align 8
  %32 = ptrtoint [10 x i32]* %31 to i64
  %add.29 = add i64 %mul.28, %32
  %33 = inttoptr i64 %add.29 to i32*
  %arrayidx30 = getelementptr i32, i32* %33, i64 %conv326
  %34 = load i32, i32* %arrayidx30, align 4
  %35 = load i32, i32* %j_12, align 4
  %conv331 = sext i32 %35 to i64
  %36 = load i32, i32* %k_13, align 4
  %conv332 = sext i32 %36 to i64
  %mul.33 = mul i64 %conv332, 40
  %37 = load [10 x i32]*, [10 x i32]** %secondMatrix_5.addr, align 8
  %38 = ptrtoint [10 x i32]* %37 to i64
  %add.34 = add i64 %mul.33, %38
  %39 = inttoptr i64 %add.34 to i32*
  %arrayidx35 = getelementptr i32, i32* %39, i64 %conv331
  %40 = load i32, i32* %arrayidx35, align 4
  %mul.36 = mul i32 %34, %40
  %add.37 = add i32 %28, %mul.36
  store i32 %add.37, i32* %arrayidx20, align 4
  %41 = load i32, i32* %k_13, align 4
  %add.38 = add i32 %41, 1
  store i32 %add.38, i32* %k_13, align 4
  br label %while.cond13

while.end15:                                      ; preds = %while.cond13
  %42 = load i32, i32* %j_12, align 4
  %add.39 = add i32 %42, 1
  store i32 %add.39, i32* %j_12, align 4
  br label %while.cond10
}

define void @_Z7displayPA10_iii([10 x i32]* %mult_4, i32 %rowFirst_5, i32 %columnSecond_6) {
entry:
  %_temp_dummy111_9 = alloca i32, align 4
  %_temp_dummy112_10 = alloca i32, align 4
  %_temp_dummy113_11 = alloca i32, align 4
  %columnSecond_6.addr = alloca i32, align 4
  %i_7 = alloca i32, align 4
  %j_8 = alloca i32, align 4
  %mult_4.addr = alloca [10 x i32]*, align 4
  %rowFirst_5.addr = alloca i32, align 4
  store [10 x i32]* %mult_4, [10 x i32]** %mult_4.addr, align 4
  store i32 %rowFirst_5, i32* %rowFirst_5.addr, align 4
  store i32 %columnSecond_6, i32* %columnSecond_6.addr, align 4
  %0 = bitcast i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.5, i32 0, i32 0) to i8*
  %call13 = call i32 (i8*, ...) @printf(i8* %0)
  store i32 0, i32* %i_7, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.end3, %entry
  %1 = load i32, i32* %i_7, align 4
  %2 = load i32, i32* %rowFirst_5.addr, align 4
  %cmp2 = icmp slt i32 %1, %2
  br i1 %cmp2, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  store i32 0, i32* %j_8, align 4
  br label %while.cond1

while.end:                                        ; preds = %while.cond
  ret void

while.cond1:                                      ; preds = %if.end, %while.body
  %3 = load i32, i32* %j_8, align 4
  %4 = load i32, i32* %columnSecond_6.addr, align 4
  %cmp3 = icmp slt i32 %3, %4
  br i1 %cmp3, label %while.body2, label %while.end3

while.body2:                                      ; preds = %while.cond1
  %5 = bitcast i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.6, i32 0, i32 0) to i8*
  %6 = load i32, i32* %j_8, align 4
  %conv3 = sext i32 %6 to i64
  %7 = load i32, i32* %i_7, align 4
  %conv34 = sext i32 %7 to i64
  %mul = mul i64 %conv34, 40
  %8 = load [10 x i32]*, [10 x i32]** %mult_4.addr, align 8
  %9 = ptrtoint [10 x i32]* %8 to i64
  %add = add i64 %mul, %9
  %10 = inttoptr i64 %add to i32*
  %arrayidx = getelementptr i32, i32* %10, i64 %conv3
  %11 = load i32, i32* %arrayidx, align 4
  %call14 = call i32 (i8*, ...) @printf(i8* %5, i32 %11)
  %12 = load i32, i32* %columnSecond_6.addr, align 4
  %add.5 = sub i32 %12, 1
  %13 = load i32, i32* %j_8, align 4
  %cmp4 = icmp eq i32 %add.5, %13
  br i1 %cmp4, label %if.then, label %if.else

while.end3:                                       ; preds = %while.cond1
  %14 = load i32, i32* %i_7, align 4
  %add.7 = add i32 %14, 1
  store i32 %add.7, i32* %i_7, align 4
  br label %while.cond

if.then:                                          ; preds = %while.body2
  %15 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.7, i32 0, i32 0) to i8*
  %call15 = call i32 (i8*, ...) @printf(i8* %15)
  br label %if.end

if.else:                                          ; preds = %while.body2
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %16 = load i32, i32* %j_8, align 4
  %add.6 = add i32 %16, 1
  store i32 %add.6, i32* %j_8, align 4
  br label %while.cond1
}
