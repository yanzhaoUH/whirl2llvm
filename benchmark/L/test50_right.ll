; ModuleID = 'test50.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [35 x i8] c"Enter rows and columns of matrix: \00", align 1
@.str.1 = private unnamed_addr constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private unnamed_addr constant [28 x i8] c"\0AEnter elements of matrix:\0A\00", align 1
@.str.3 = private unnamed_addr constant [22 x i8] c"Enter element a%d%d: \00", align 1
@.str.4 = private unnamed_addr constant [19 x i8] c"\0AEntered Matrix: \0A\00", align 1
@.str.5 = private unnamed_addr constant [5 x i8] c"%d  \00", align 1
@.str.6 = private unnamed_addr constant [3 x i8] c"\0A\0A\00", align 1
@.str.7 = private unnamed_addr constant [23 x i8] c"\0ATranspose of Matrix:\0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %a = alloca [10 x [10 x i32]], align 16
  %transpose = alloca [10 x [10 x i32]], align 16
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str, i32 0, i32 0))
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0), i32* %r, i32* %c)
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.2, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc.12, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %r, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end.14

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4
  br label %for.cond.3

for.cond.3:                                       ; preds = %for.inc, %for.body
  %2 = load i32, i32* %j, align 4
  %3 = load i32, i32* %c, align 4
  %cmp4 = icmp slt i32 %2, %3
  br i1 %cmp4, label %for.body.5, label %for.end

for.body.5:                                       ; preds = %for.cond.3
  %4 = load i32, i32* %i, align 4
  %add = add nsw i32 %4, 1
  %5 = load i32, i32* %j, align 4
  %add6 = add nsw i32 %5, 1
  %call7 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.3, i32 0, i32 0), i32 %add, i32 %add6)
  %6 = load i32, i32* %i, align 4
  %add8 = add nsw i32 %6, 1
  %7 = load i32, i32* %j, align 4
  %add9 = add nsw i32 %7, 1
  %mul = mul nsw i32 %add8, %add9
  %8 = load i32, i32* %j, align 4
  %idxprom = sext i32 %8 to i64
  %9 = load i32, i32* %i, align 4
  %idxprom10 = sext i32 %9 to i64
  %arrayidx = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %a, i32 0, i64 %idxprom10
  %arrayidx11 = getelementptr inbounds [10 x i32], [10 x i32]* %arrayidx, i32 0, i64 %idxprom
  store i32 %mul, i32* %arrayidx11, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body.5
  %10 = load i32, i32* %j, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond.3

for.end:                                          ; preds = %for.cond.3
  br label %for.inc.12

for.inc.12:                                       ; preds = %for.end
  %11 = load i32, i32* %i, align 4
  %inc13 = add nsw i32 %11, 1
  store i32 %inc13, i32* %i, align 4
  br label %for.cond

for.end.14:                                       ; preds = %for.cond
  %call15 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.4, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond.16

for.cond.16:                                      ; preds = %for.inc.32, %for.end.14
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %r, align 4
  %cmp17 = icmp slt i32 %12, %13
  br i1 %cmp17, label %for.body.18, label %for.end.34

for.body.18:                                      ; preds = %for.cond.16
  store i32 0, i32* %j, align 4
  br label %for.cond.19

for.cond.19:                                      ; preds = %for.inc.29, %for.body.18
  %14 = load i32, i32* %j, align 4
  %15 = load i32, i32* %c, align 4
  %cmp20 = icmp slt i32 %14, %15
  br i1 %cmp20, label %for.body.21, label %for.end.31

for.body.21:                                      ; preds = %for.cond.19
  %16 = load i32, i32* %j, align 4
  %idxprom22 = sext i32 %16 to i64
  %17 = load i32, i32* %i, align 4
  %idxprom23 = sext i32 %17 to i64
  %arrayidx24 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %a, i32 0, i64 %idxprom23
  %arrayidx25 = getelementptr inbounds [10 x i32], [10 x i32]* %arrayidx24, i32 0, i64 %idxprom22
  %18 = load i32, i32* %arrayidx25, align 4
  %call26 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.5, i32 0, i32 0), i32 %18)
  %19 = load i32, i32* %j, align 4
  %20 = load i32, i32* %c, align 4
  %sub = sub nsw i32 %20, 1
  %cmp27 = icmp eq i32 %19, %sub
  br i1 %cmp27, label %if.then, label %if.end

if.then:                                          ; preds = %for.body.21
  %call28 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.6, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body.21
  br label %for.inc.29

for.inc.29:                                       ; preds = %if.end
  %21 = load i32, i32* %j, align 4
  %inc30 = add nsw i32 %21, 1
  store i32 %inc30, i32* %j, align 4
  br label %for.cond.19

for.end.31:                                       ; preds = %for.cond.19
  br label %for.inc.32

for.inc.32:                                       ; preds = %for.end.31
  %22 = load i32, i32* %i, align 4
  %inc33 = add nsw i32 %22, 1
  store i32 %inc33, i32* %i, align 4
  br label %for.cond.16

for.end.34:                                       ; preds = %for.cond.16
  store i32 0, i32* %i, align 4
  br label %for.cond.35

for.cond.35:                                      ; preds = %for.inc.52, %for.end.34
  %23 = load i32, i32* %i, align 4
  %24 = load i32, i32* %r, align 4
  %cmp36 = icmp slt i32 %23, %24
  br i1 %cmp36, label %for.body.37, label %for.end.54

for.body.37:                                      ; preds = %for.cond.35
  store i32 0, i32* %j, align 4
  br label %for.cond.38

for.cond.38:                                      ; preds = %for.inc.49, %for.body.37
  %25 = load i32, i32* %j, align 4
  %26 = load i32, i32* %c, align 4
  %cmp39 = icmp slt i32 %25, %26
  br i1 %cmp39, label %for.body.40, label %for.end.51

for.body.40:                                      ; preds = %for.cond.38
  %27 = load i32, i32* %j, align 4
  %idxprom41 = sext i32 %27 to i64
  %28 = load i32, i32* %i, align 4
  %idxprom42 = sext i32 %28 to i64
  %arrayidx43 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %a, i32 0, i64 %idxprom42
  %arrayidx44 = getelementptr inbounds [10 x i32], [10 x i32]* %arrayidx43, i32 0, i64 %idxprom41
  %29 = load i32, i32* %arrayidx44, align 4
  %30 = load i32, i32* %i, align 4
  %idxprom45 = sext i32 %30 to i64
  %31 = load i32, i32* %j, align 4
  %idxprom46 = sext i32 %31 to i64
  %arrayidx47 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %transpose, i32 0, i64 %idxprom46
  %arrayidx48 = getelementptr inbounds [10 x i32], [10 x i32]* %arrayidx47, i32 0, i64 %idxprom45
  store i32 %29, i32* %arrayidx48, align 4
  br label %for.inc.49

for.inc.49:                                       ; preds = %for.body.40
  %32 = load i32, i32* %j, align 4
  %inc50 = add nsw i32 %32, 1
  store i32 %inc50, i32* %j, align 4
  br label %for.cond.38

for.end.51:                                       ; preds = %for.cond.38
  br label %for.inc.52

for.inc.52:                                       ; preds = %for.end.51
  %33 = load i32, i32* %i, align 4
  %inc53 = add nsw i32 %33, 1
  store i32 %inc53, i32* %i, align 4
  br label %for.cond.35

for.end.54:                                       ; preds = %for.cond.35
  %call55 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.7, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond.56

for.cond.56:                                      ; preds = %for.inc.75, %for.end.54
  %34 = load i32, i32* %i, align 4
  %35 = load i32, i32* %c, align 4
  %cmp57 = icmp slt i32 %34, %35
  br i1 %cmp57, label %for.body.58, label %for.end.77

for.body.58:                                      ; preds = %for.cond.56
  store i32 0, i32* %j, align 4
  br label %for.cond.59

for.cond.59:                                      ; preds = %for.inc.72, %for.body.58
  %36 = load i32, i32* %j, align 4
  %37 = load i32, i32* %r, align 4
  %cmp60 = icmp slt i32 %36, %37
  br i1 %cmp60, label %for.body.61, label %for.end.74

for.body.61:                                      ; preds = %for.cond.59
  %38 = load i32, i32* %j, align 4
  %idxprom62 = sext i32 %38 to i64
  %39 = load i32, i32* %i, align 4
  %idxprom63 = sext i32 %39 to i64
  %arrayidx64 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %transpose, i32 0, i64 %idxprom63
  %arrayidx65 = getelementptr inbounds [10 x i32], [10 x i32]* %arrayidx64, i32 0, i64 %idxprom62
  %40 = load i32, i32* %arrayidx65, align 4
  %call66 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.5, i32 0, i32 0), i32 %40)
  %41 = load i32, i32* %j, align 4
  %42 = load i32, i32* %r, align 4
  %sub67 = sub nsw i32 %42, 1
  %cmp68 = icmp eq i32 %41, %sub67
  br i1 %cmp68, label %if.then.69, label %if.end.71

if.then.69:                                       ; preds = %for.body.61
  %call70 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.6, i32 0, i32 0))
  br label %if.end.71

if.end.71:                                        ; preds = %if.then.69, %for.body.61
  br label %for.inc.72

for.inc.72:                                       ; preds = %if.end.71
  %43 = load i32, i32* %j, align 4
  %inc73 = add nsw i32 %43, 1
  store i32 %inc73, i32* %j, align 4
  br label %for.cond.59

for.end.74:                                       ; preds = %for.cond.59
  br label %for.inc.75

for.inc.75:                                       ; preds = %for.end.74
  %44 = load i32, i32* %i, align 4
  %inc76 = add nsw i32 %44, 1
  store i32 %inc76, i32* %i, align 4
  br label %for.cond.56

for.end.77:                                       ; preds = %for.cond.56
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @__isoc99_scanf(i8*, ...) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
