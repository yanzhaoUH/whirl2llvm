; ModuleID = 'test29_O.bc'

@.str = private constant [30 x i8] c"Enter a three digit integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [27 x i8] c"%d is an Armstrong number.\00", align 1
@.str.3 = private constant [31 x i8] c"%d is not an Armstrong number.\00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_51 = alloca i32
  %.preg_I4_4_50 = alloca i32
  %.preg_I4_4_55 = alloca i32
  %.preg_I4_4_52 = alloca i32
  %.preg_I4_4_54 = alloca i32
  %_temp_dummy10_8 = alloca i32, align 4
  %_temp_dummy11_9 = alloca i32, align 4
  %_temp_dummy12_10 = alloca i32, align 4
  %_temp_dummy13_11 = alloca i32, align 4
  %_temp_ehpit0_12 = alloca i32, align 4
  %number_4 = alloca i32, align 4
  %old_frame_pointer_17.addr = alloca i64, align 8
  %originalNumber_5 = alloca i32, align 4
  %remainder_6 = alloca i32, align 4
  %result_7 = alloca i32, align 4
  %return_address_18.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %number_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = load i32, i32* %number_4, align 4
  store i32 %3, i32* %.preg_I4_4_54, align 8
  %4 = load i32, i32* %.preg_I4_4_54
  store i32 %4, i32* %.preg_I4_4_52, align 8
  %5 = load i32, i32* %.preg_I4_4_54
  %cmp1 = icmp ne i32 %5, 0
  br i1 %cmp1, label %tb, label %L4354

tb:                                               ; preds = %entry
  store i32 0, i32* %.preg_I4_4_55, align 8
  br label %L3330

L4354:                                            ; preds = %entry
  br label %L4610

L3330:                                            ; preds = %L3330, %tb
  %6 = load i32, i32* %.preg_I4_4_52
  %lowPart_4 = alloca i32, align 4
  %div = sdiv i32 %6, 10
  %srem = srem i32 %6, 10
  store i32 %div, i32* %lowPart_4, align 4
  store i32 %srem, i32* %.preg_I4_4_50, align 8
  %7 = load i32, i32* %.preg_I4_4_50
  store i32 %7, i32* %.preg_I4_4_51, align 8
  %8 = load i32, i32* %.preg_I4_4_55
  %9 = load i32, i32* %.preg_I4_4_51
  %10 = load i32, i32* %.preg_I4_4_51
  %11 = load i32, i32* %.preg_I4_4_51
  %mul = mul i32 %10, %11
  %mul.1 = mul i32 %9, %mul
  %add = add i32 %8, %mul.1
  store i32 %add, i32* %.preg_I4_4_55, align 8
  %12 = load i32, i32* %lowPart_4
  store i32 %12, i32* %.preg_I4_4_52, align 8
  %13 = load i32, i32* %.preg_I4_4_52
  %cmp2 = icmp ne i32 %13, 0
  br i1 %cmp2, label %L3330, label %fb

fb:                                               ; preds = %L3330
  %14 = load i32, i32* %.preg_I4_4_54
  %15 = load i32, i32* %.preg_I4_4_55
  %cmp3 = icmp eq i32 %14, %15
  br i1 %cmp3, label %tb2, label %L3842

tb2:                                              ; preds = %fb
  br label %L4610

L3842:                                            ; preds = %fb
  %16 = bitcast i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.3, i32 0, i32 0) to i8*
  %17 = load i32, i32* %.preg_I4_4_54
  %call4 = call i32 (i8*, ...) @printf(i8* %16, i32 %17)
  br label %L4098

L4610:                                            ; preds = %tb2, %L4354
  %18 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.2, i32 0, i32 0) to i8*
  %19 = load i32, i32* %.preg_I4_4_54
  %call3 = call i32 (i8*, ...) @printf(i8* %18, i32 %19)
  br label %L4098

L4098:                                            ; preds = %L4610, %L3842
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
