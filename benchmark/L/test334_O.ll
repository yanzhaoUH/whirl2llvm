; ModuleID = 'test334_O.bc'

@.str = private constant [20 x i8] c"%c %d %ld %f %lf %u\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_11 = alloca i32, align 4
  %_temp_ehpit0_12 = alloca i32, align 4
  %bi_6 = alloca i32, align 4
  %cf_9 = alloca float, align 4
  %ch_4 = alloca i8, align 1
  %dd_10 = alloca double, align 8
  %eui_8 = alloca i32, align 4
  %longi_7 = alloca i64, align 8
  %old_frame_pointer_17.addr = alloca i64, align 8
  %return_address_18.addr = alloca i64, align 8
  %shorti_5 = alloca i16, align 2
  %0 = bitcast i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0, i32 97, i32 4, i32 100, double 1.000000e+00, double 2.000000e+00, i32 4)
  ret i32 0
}

declare i32 @printf(i8*, ...)
