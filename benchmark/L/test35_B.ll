; ModuleID = 'test35.bc'

@.str = private constant [27 x i8] c"Enter a positive integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [23 x i8] c"%d is a prime number.\0A\00", align 1
@.str.3 = private constant [27 x i8] c"%d is not a prime number.\0A\00", align 1
@.str.4 = private constant [27 x i8] c"%d is an Armstrong number.\00", align 1
@.str.5 = private constant [31 x i8] c"%d is not an Armstrong number.\00", align 1
@_LIB_VERSION_62 = common global i32 0, align 4
@signgam_63 = common global i32 0, align 4

define i32 @main() {
entry:
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %_temp_dummy13_9 = alloca i32, align 4
  %_temp_dummy14_10 = alloca i32, align 4
  %_temp_dummy15_11 = alloca i32, align 4
  %flag_5 = alloca i32, align 4
  %n_4 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = load i32, i32* %n_4, align 4
  %call3 = call i32 @_Z16checkPrimeNumberi(i32 %3)
  store i32 %call3, i32* %flag_5, align 4
  %4 = load i32, i32* %flag_5, align 4
  %cmp1 = icmp eq i32 %4, 1
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.2, i32 0, i32 0) to i8*
  %6 = load i32, i32* %n_4, align 4
  %call4 = call i32 (i8*, ...) @printf(i8* %5, i32 %6)
  br label %if.end

if.else:                                          ; preds = %entry
  %7 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.3, i32 0, i32 0) to i8*
  %8 = load i32, i32* %n_4, align 4
  %call5 = call i32 (i8*, ...) @printf(i8* %7, i32 %8)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %9 = load i32, i32* %n_4, align 4
  %call6 = call i32 @_Z20checkArmstrongNumberi(i32 %9)
  store i32 %call6, i32* %flag_5, align 4
  %10 = load i32, i32* %flag_5, align 4
  %cmp2 = icmp eq i32 %10, 1
  br i1 %cmp2, label %if.then1, label %if.else2

if.then1:                                         ; preds = %if.end
  %11 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.4, i32 0, i32 0) to i8*
  %12 = load i32, i32* %n_4, align 4
  %call7 = call i32 (i8*, ...) @printf(i8* %11, i32 %12)
  br label %if.end3

if.else2:                                         ; preds = %if.end
  %13 = bitcast i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.5, i32 0, i32 0) to i8*
  %14 = load i32, i32* %n_4, align 4
  %call8 = call i32 (i8*, ...) @printf(i8* %13, i32 %14)
  br label %if.end3

if.end3:                                          ; preds = %if.else2, %if.then1
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define i32 @_Z16checkPrimeNumberi(i32 %n_4) {
entry:
  %flag_6 = alloca i32, align 4
  %i_5 = alloca i32, align 4
  %n_4.addr = alloca i32, align 4
  store i32 %n_4, i32* %n_4.addr, align 4
  store i32 1, i32* %flag_6, align 4
  store i32 2, i32* %i_5, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %0 = load i32, i32* %n_4.addr, align 4
  %div = sdiv i32 %0, 2
  %1 = load i32, i32* %i_5, align 4
  %cmp6 = icmp sge i32 %div, %1
  br i1 %cmp6, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load i32, i32* %n_4.addr, align 4
  %3 = load i32, i32* %i_5, align 4
  %srem = srem i32 %2, %3
  %cmp7 = icmp eq i32 %srem, 0
  br i1 %cmp7, label %if.then, label %if.else

while.end:                                        ; preds = %if.then, %while.cond
  br label %LABEL_258

if.then:                                          ; preds = %while.body
  store i32 0, i32* %flag_6, align 4
  br label %while.end

if.else:                                          ; preds = %while.body
  br label %if.end

if.end:                                           ; preds = %if.else
  %4 = load i32, i32* %i_5, align 4
  %add = add i32 %4, 1
  store i32 %add, i32* %i_5, align 4
  br label %while.cond

LABEL_258:                                        ; preds = %while.end
  %5 = load i32, i32* %flag_6, align 4
  ret i32 %5
}

define i32 @_Z20checkArmstrongNumberi(i32 %number_4) {
entry:
  %flag_9 = alloca i32, align 4
  %n_8 = alloca i32, align 4
  %number_4.addr = alloca i32, align 4
  %originalNumber_5 = alloca i32, align 4
  %remainder_6 = alloca i32, align 4
  %result_7 = alloca i32, align 4
  store i32 %number_4, i32* %number_4.addr, align 4
  store i32 0, i32* %result_7, align 4
  store i32 0, i32* %n_8, align 4
  %0 = load i32, i32* %number_4.addr, align 4
  store i32 %0, i32* %originalNumber_5, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32, i32* %originalNumber_5, align 4
  %cmp3 = icmp ne i32 %1, 0
  br i1 %cmp3, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load i32, i32* %originalNumber_5, align 4
  %div = sdiv i32 %2, 10
  store i32 %div, i32* %originalNumber_5, align 4
  %3 = load i32, i32* %n_8, align 4
  %add = add i32 %3, 1
  store i32 %add, i32* %n_8, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i32, i32* %number_4.addr, align 4
  store i32 %4, i32* %originalNumber_5, align 4
  br label %while.cond1

while.cond1:                                      ; preds = %while.body2, %while.end
  %5 = load i32, i32* %originalNumber_5, align 4
  %cmp4 = icmp ne i32 %5, 0
  br i1 %cmp4, label %while.body2, label %while.end3

while.body2:                                      ; preds = %while.cond1
  %6 = load i32, i32* %originalNumber_5, align 4
  %srem = srem i32 %6, 10
  store i32 %srem, i32* %remainder_6, align 4
  %7 = load i32, i32* %result_7, align 4
  %conv9 = sitofp i32 %7 to double
  %8 = load i32, i32* %remainder_6, align 4
  %conv94 = sitofp i32 %8 to double
  %9 = load i32, i32* %n_8, align 4
  %conv95 = sitofp i32 %9 to double
  %call9 = call double @pow(double %conv94, double %conv95)
  %add.6 = fadd double %conv9, %call9
  %fp2int = fptosi double %add.6 to i32
  store i32 %fp2int, i32* %result_7, align 4
  %10 = load i32, i32* %originalNumber_5, align 4
  %div.7 = sdiv i32 %10, 10
  store i32 %div.7, i32* %originalNumber_5, align 4
  br label %while.cond1

while.end3:                                       ; preds = %while.cond1
  %11 = load i32, i32* %result_7, align 4
  %12 = load i32, i32* %number_4.addr, align 4
  %cmp5 = icmp eq i32 %11, %12
  br i1 %cmp5, label %if.then, label %if.else

if.then:                                          ; preds = %while.end3
  store i32 1, i32* %flag_9, align 4
  br label %if.end

if.else:                                          ; preds = %while.end3
  store i32 0, i32* %flag_9, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %13 = load i32, i32* %flag_9, align 4
  ret i32 %13
}

declare double @pow(double, double)
