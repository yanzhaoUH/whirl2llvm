; ModuleID = 'test59.bc'

@.str = private constant [21 x i8] c"Enter first string: \00", align 1
@.str.1 = private constant [3 x i8] c"%s\00", align 1
@.str.2 = private constant [22 x i8] c"Enter second string: \00", align 1
@.str.3 = private constant [24 x i8] c"After concatenation: %s\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_8 = alloca i32, align 4
  %_temp_dummy11_9 = alloca i32, align 4
  %_temp_dummy12_10 = alloca i32, align 4
  %_temp_dummy13_11 = alloca i32, align 4
  %_temp_dummy14_12 = alloca i32, align 4
  %i_6 = alloca i8, align 1
  %j_7 = alloca i8, align 1
  %s1_4 = alloca [100 x i8], align 1
  %s2_5 = alloca [100 x i8], align 1
  %0 = bitcast i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %arrayAddress = getelementptr [100 x i8], [100 x i8]* %s1_4, i32 0, i32 0
  %2 = bitcast i8* %arrayAddress to i8*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i8* %2)
  %3 = bitcast i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %3)
  %4 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %arrayAddress1 = getelementptr [100 x i8], [100 x i8]* %s2_5, i32 0, i32 0
  %5 = bitcast i8* %arrayAddress1 to i8*
  %call4 = call i32 (i8*, ...) @scanf(i8* %4, i8* %5)
  %conv = trunc i32 0 to i8
  store i8 %conv, i8* %i_6, align 1
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %6 = load i8, i8* %i_6, align 1
  %arrayidx = getelementptr [100 x i8], [100 x i8]* %s1_4, i32 0, i8 %6
  %7 = load i8, i8* %arrayidx, align 1
  %conv3 = sext i8 %7 to i32
  %cmp1 = icmp ne i32 %conv3, 0
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %8 = load i8, i8* %i_6, align 1
  %conv32 = sext i8 %8 to i32
  %add = add i32 %conv32, 1
  %conv4 = trunc i32 %add to i8
  store i8 %conv4, i8* %i_6, align 1
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %conv5 = trunc i32 0 to i8
  store i8 %conv5, i8* %j_7, align 1
  br label %while.cond6

while.cond6:                                      ; preds = %while.body7, %while.end
  %9 = load i8, i8* %j_7, align 1
  %arrayidx9 = getelementptr [100 x i8], [100 x i8]* %s2_5, i32 0, i8 %9
  %10 = load i8, i8* %arrayidx9, align 1
  %conv310 = sext i8 %10 to i32
  %cmp2 = icmp ne i32 %conv310, 0
  br i1 %cmp2, label %while.body7, label %while.end8

while.body7:                                      ; preds = %while.cond6
  %11 = load i8, i8* %i_6, align 1
  %arrayidx11 = getelementptr [100 x i8], [100 x i8]* %s1_4, i32 0, i8 %11
  %12 = load i8, i8* %j_7, align 1
  %arrayidx12 = getelementptr [100 x i8], [100 x i8]* %s2_5, i32 0, i8 %12
  %13 = load i8, i8* %arrayidx12, align 1
  %conv313 = sext i8 %13 to i32
  %conv14 = trunc i32 %conv313 to i8
  store i8 %conv14, i8* %arrayidx11, align 1
  %14 = load i8, i8* %j_7, align 1
  %conv315 = sext i8 %14 to i32
  %add.16 = add i32 %conv315, 1
  %conv17 = trunc i32 %add.16 to i8
  store i8 %conv17, i8* %j_7, align 1
  %15 = load i8, i8* %i_6, align 1
  %conv318 = sext i8 %15 to i32
  %add.19 = add i32 %conv318, 1
  %conv20 = trunc i32 %add.19 to i8
  store i8 %conv20, i8* %i_6, align 1
  br label %while.cond6

while.end8:                                       ; preds = %while.cond6
  %16 = load i8, i8* %i_6, align 1
  %arrayidx21 = getelementptr [100 x i8], [100 x i8]* %s1_4, i32 0, i8 %16
  %conv22 = trunc i32 0 to i8
  store i8 %conv22, i8* %arrayidx21, align 1
  %17 = bitcast i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.3, i32 0, i32 0) to i8*
  %arrayAddress23 = getelementptr [100 x i8], [100 x i8]* %s1_4, i32 0, i32 0
  %18 = bitcast i8* %arrayAddress23 to i8*
  %call5 = call i32 (i8*, ...) @printf(i8* %17, i8* %18)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
