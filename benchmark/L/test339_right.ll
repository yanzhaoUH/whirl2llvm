; ModuleID = 'test339.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@g_c = global i8 97, align 1
@.str = private unnamed_addr constant [7 x i8] c"test1\0A\00", align 1
@.str.1 = private unnamed_addr constant [6 x i8] c"hello\00", align 1
@.str.2 = private unnamed_addr constant [4 x i8] c"%s\0A\00", align 1
@.str.3 = private unnamed_addr constant [7 x i8] c"test2\0A\00", align 1
@.str.4 = private unnamed_addr constant [4 x i8] c"%d\0A\00", align 1
@.str.5 = private unnamed_addr constant [7 x i8] c"test3\0A\00", align 1
@main.a = private unnamed_addr constant [3 x i32] [i32 1, i32 2, i32 3], align 4
@.str.6 = private unnamed_addr constant [7 x i8] c"test4\0A\00", align 1
@main.c = private unnamed_addr constant [3 x i8] c"abc", align 1
@.str.7 = private unnamed_addr constant [4 x i8] c"%c\0A\00", align 1
@main.f = private unnamed_addr constant [3 x float] [float 1.000000e+00, float 2.000000e+00, float 3.000000e+00], align 4
@.str.8 = private unnamed_addr constant [4 x i8] c"%f\0A\00", align 1
@main.d = private unnamed_addr constant [3 x double] [double 1.000000e+00, double 2.000000e+00, double 3.000000e+00], align 16

; Function Attrs: nounwind uwtable
define double @f1(double %i) #0 {
entry:
  %i.addr = alloca double, align 8
  store double %i, double* %i.addr, align 8
  %0 = load double, double* %i.addr, align 8
  %call = call double @sqrt(double %0) #3
  ret double %call
}

; Function Attrs: nounwind
declare double @sqrt(double) #1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %ap = alloca i8*, align 8
  %b = alloca [10 x i8], align 1
  %cp = alloca i8*, align 8
  %n = alloca i32, align 4
  %n4 = alloca i32, align 4
  %a = alloca [3 x i32], align 4
  %c = alloca [3 x i8], align 1
  %f = alloca [3 x float], align 4
  %d = alloca [3 x double], align 16
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str, i32 0, i32 0))
  store i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0), i8** %ap, align 8
  %0 = bitcast [10 x i8]* %b to i8*
  %1 = load i8*, i8** %ap, align 8
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %0, i8* %1, i64 6, i32 1, i1 false)
  store i8* %0, i8** %cp, align 8
  %2 = load i8*, i8** %cp, align 8
  %call1 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.2, i32 0, i32 0), i8* %2)
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.3, i32 0, i32 0))
  store i32 2, i32* %n, align 4
  %3 = load i32, i32* %n, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.4, i32 0, i32 0), i32 %3)
  store i32 33, i32* %n4, align 4
  %4 = load i32, i32* %n4, align 4
  %call5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.4, i32 0, i32 0), i32 %4)
  %5 = load i32, i32* %n, align 4
  %call6 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.4, i32 0, i32 0), i32 %5)
  %call7 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.5, i32 0, i32 0))
  %6 = bitcast [3 x i32]* %a to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %6, i8* bitcast ([3 x i32]* @main.a to i8*), i64 12, i32 4, i1 false)
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %a, i32 0, i64 1
  %7 = load i32, i32* %arrayidx, align 4
  %call8 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.4, i32 0, i32 0), i32 %7)
  %call9 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.6, i32 0, i32 0))
  %8 = bitcast [3 x i8]* %c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %8, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @main.c, i32 0, i32 0), i64 3, i32 1, i1 false)
  %arrayidx10 = getelementptr inbounds [3 x i8], [3 x i8]* %c, i32 0, i64 1
  %9 = load i8, i8* %arrayidx10, align 1
  %conv = sext i8 %9 to i32
  %call11 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.7, i32 0, i32 0), i32 %conv)
  %10 = bitcast [3 x float]* %f to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %10, i8* bitcast ([3 x float]* @main.f to i8*), i64 12, i32 4, i1 false)
  %arrayidx12 = getelementptr inbounds [3 x float], [3 x float]* %f, i32 0, i64 1
  %11 = load float, float* %arrayidx12, align 4
  %conv13 = fpext float %11 to double
  %call14 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.8, i32 0, i32 0), double %conv13)
  %12 = bitcast [3 x double]* %d to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %12, i8* bitcast ([3 x double]* @main.d to i8*), i64 24, i32 16, i1 false)
  %arrayidx15 = getelementptr inbounds [3 x double], [3 x double]* %d, i32 0, i64 1
  %13 = load double, double* %arrayidx15, align 8
  %call16 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.8, i32 0, i32 0), double %13)
  %arrayidx17 = getelementptr inbounds [3 x double], [3 x double]* %d, i32 0, i64 1
  %14 = load double, double* %arrayidx17, align 8
  %call18 = call double @f1(double %14)
  %call19 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.8, i32 0, i32 0), double %call18)
  ret i32 0
}

declare i32 @printf(i8*, ...) #2

; Function Attrs: nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture, i8* nocapture readonly, i64, i32, i1) #3

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
