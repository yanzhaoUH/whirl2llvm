; ModuleID = 'test17_O.bc'

@.str = private constant [19 x i8] c"Enter an integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [53 x i8] c"Error! Factorial of a negative number doesn't exist.\00", align 1
@.str.3 = private constant [23 x i8] c"Factorial of %d = %llu\00", align 1

define i32 @main() {
entry:
  %.preg_U8_9_53 = alloca i64
  %.preg_U8_9_51 = alloca i64
  %.preg_I4_4_54 = alloca i32
  %.preg_I4_4_49 = alloca i32
  %.preg_I4_4_52 = alloca i32
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %_temp_ehpit0_11 = alloca i32, align 4
  %factorial_6 = alloca i64, align 8
  %i_5 = alloca i32, align 4
  %n_4 = alloca i32, align 4
  %old_frame_pointer_16.addr = alloca i64, align 8
  %return_address_17.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = load i32, i32* %n_4, align 4
  store i32 %3, i32* %.preg_I4_4_52, align 8
  %4 = load i32, i32* %.preg_I4_4_52
  %cmp1 = icmp slt i32 %4, 0
  br i1 %cmp1, label %tb, label %L2818

tb:                                               ; preds = %entry
  %5 = bitcast i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %5)
  br label %L4610

L2818:                                            ; preds = %entry
  %6 = load i32, i32* %.preg_I4_4_52
  %add = add i32 %6, -1
  %cmp2 = icmp sge i32 %add, 0
  br i1 %cmp2, label %tb1, label %L3074

L4610:                                            ; preds = %L4354, %tb
  ret i32 0

tb1:                                              ; preds = %L2818
  store i32 0, i32* %.preg_I4_4_49, align 8
  %7 = load i32, i32* %.preg_I4_4_52
  store i32 %7, i32* %.preg_I4_4_54, align 8
  %conv3 = zext i32 1 to i64
  store i64 %conv3, i64* %.preg_U8_9_51, align 8
  %conv32 = zext i32 1 to i64
  store i64 %conv32, i64* %.preg_U8_9_53, align 8
  br label %L3842

L3074:                                            ; preds = %L2818
  %conv36 = zext i32 1 to i64
  store i64 %conv36, i64* %.preg_U8_9_53, align 8
  br label %L4354

L3842:                                            ; preds = %L3842, %tb1
  %8 = load i64, i64* %.preg_U8_9_51
  %9 = load i64, i64* %.preg_U8_9_53
  %mul = mul i64 %8, %9
  store i64 %mul, i64* %.preg_U8_9_53, align 8
  %10 = load i32, i32* %.preg_I4_4_49
  %add.3 = add i32 %10, 1
  store i32 %add.3, i32* %.preg_I4_4_49, align 8
  %11 = load i64, i64* %.preg_U8_9_51
  %conv34 = zext i32 1 to i64
  %add.5 = add i64 %11, %conv34
  store i64 %add.5, i64* %.preg_U8_9_51, align 8
  %12 = load i32, i32* %.preg_I4_4_49
  %13 = load i32, i32* %.preg_I4_4_52
  %cmp3 = icmp slt i32 %12, %13
  br i1 %cmp3, label %L3842, label %fb

fb:                                               ; preds = %L3842
  br label %L4354

L4354:                                            ; preds = %fb, %L3074
  %14 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.3, i32 0, i32 0) to i8*
  %15 = load i32, i32* %.preg_I4_4_52
  %16 = load i64, i64* %.preg_U8_9_53
  %call4 = call i32 (i8*, ...) @printf(i8* %14, i32 %15, i64 %16)
  br label %L4610
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
