; ModuleID = 'test12.bc'

@.str = private constant [32 x i8] c"Enter coefficients a, b and c: \00", align 1
@.str.1 = private constant [12 x i8] c"%lf %lf %lf\00", align 1
@.str.2 = private constant [32 x i8] c"root1 = %.2lf and root2 = %.2lf\00", align 1
@.str.3 = private constant [23 x i8] c"root1 = root2 = %.2lf;\00", align 1
@.str.4 = private constant [44 x i8] c"root1 = %.2lf+%.2lfi and root2 = %.2f-%.2fi\00", align 1
@_LIB_VERSION_63 = common global i32 0, align 4
@signgam_64 = common global i32 0, align 4

define i32 @main() {
entry:
  %_temp___save_sqrt3_15 = alloca double, align 8
  %_temp___save_sqrt5_17 = alloca double, align 8
  %_temp___save_sqrt9_21 = alloca double, align 8
  %_temp___sqrt_arg2_14 = alloca double, align 8
  %_temp___sqrt_arg4_16 = alloca double, align 8
  %_temp___sqrt_arg8_20 = alloca double, align 8
  %_temp_dummy10_12 = alloca i32, align 4
  %_temp_dummy110_22 = alloca i32, align 4
  %_temp_dummy11_13 = alloca i32, align 4
  %_temp_dummy16_18 = alloca i32, align 4
  %_temp_dummy17_19 = alloca i32, align 4
  %a_4 = alloca double, align 8
  %b_5 = alloca double, align 8
  %c_6 = alloca double, align 8
  %determinant_7 = alloca double, align 8
  %imaginaryPart_11 = alloca double, align 8
  %realPart_10 = alloca double, align 8
  %root1_8 = alloca double, align 8
  %root2_9 = alloca double, align 8
  %0 = bitcast i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast double* %a_4 to double*
  %3 = bitcast double* %b_5 to double*
  %4 = bitcast double* %c_6 to double*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, double* %2, double* %3, double* %4)
  %5 = load double, double* %b_5, align 8
  %6 = load double, double* %b_5, align 8
  %mul = fmul double %5, %6
  %7 = load double, double* %a_4, align 8
  %mul.1 = fmul double %7, -4.000000e+00
  %8 = load double, double* %c_6, align 8
  %mul.2 = fmul double %mul.1, %8
  %add = fadd double %mul, %mul.2
  store double %add, double* %determinant_7, align 8
  %9 = load double, double* %determinant_7, align 8
  %cmp1 = fcmp ogt double %9, 0.000000e+00
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %10 = load double, double* %a_4, align 8
  store double %10, double* %_temp___sqrt_arg2_14, align 8
  %11 = load double, double* %_temp___sqrt_arg2_14, align 8
  %call7 = call double @sqrt(double %11)
  store double %call7, double* %_temp___save_sqrt3_15, align 8
  %12 = load double, double* %_temp___save_sqrt3_15, align 8
  %13 = load double, double* %_temp___save_sqrt3_15, align 8
  %cmp2 = fcmp one double %12, %13
  br i1 %cmp2, label %if.then3, label %if.else4

if.else:                                          ; preds = %entry
  %14 = load double, double* %determinant_7, align 8
  %cmp4 = fcmp oeq double %14, 0.000000e+00
  br i1 %cmp4, label %if.then15, label %if.else16

if.then3:                                         ; preds = %if.then
  %15 = load double, double* %_temp___sqrt_arg2_14, align 8
  %call3 = call double @sqrt(double %15)
  store double %call3, double* %_temp___save_sqrt3_15, align 8
  br label %if.end

if.else4:                                         ; preds = %if.then
  br label %if.end

if.end:                                           ; preds = %if.else4, %if.then3
  %16 = load double, double* %_temp___save_sqrt3_15, align 8
  %17 = load double, double* %b_5, align 8
  %add.5 = fsub double %16, %17
  %18 = load double, double* %a_4, align 8
  %mul.6 = fmul double %18, 2.000000e+00
  %SDiv = fdiv double %add.5, %mul.6
  store double %SDiv, double* %root1_8, align 8
  %19 = load double, double* %determinant_7, align 8
  store double %19, double* %_temp___sqrt_arg4_16, align 8
  %20 = load double, double* %_temp___sqrt_arg4_16, align 8
  %call77 = call double @sqrt(double %20)
  store double %call77, double* %_temp___save_sqrt5_17, align 8
  %21 = load double, double* %_temp___save_sqrt5_17, align 8
  %22 = load double, double* %_temp___save_sqrt5_17, align 8
  %cmp3 = fcmp one double %21, %22
  br i1 %cmp3, label %if.then8, label %if.else9

if.then8:                                         ; preds = %if.end
  %23 = load double, double* %_temp___sqrt_arg4_16, align 8
  %call4 = call double @sqrt(double %23)
  store double %call4, double* %_temp___save_sqrt5_17, align 8
  br label %if.end10

if.else9:                                         ; preds = %if.end
  br label %if.end10

if.end10:                                         ; preds = %if.else9, %if.then8
  %24 = load double, double* %b_5, align 8
  %sub = fsub double -0.000000e+00, %24
  %25 = load double, double* %_temp___save_sqrt5_17, align 8
  %add.11 = fsub double %sub, %25
  %26 = load double, double* %a_4, align 8
  %mul.12 = fmul double %26, 2.000000e+00
  %SDiv.13 = fdiv double %add.11, %mul.12
  store double %SDiv.13, double* %root2_9, align 8
  %27 = bitcast i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.2, i32 0, i32 0) to i8*
  %28 = load double, double* %root1_8, align 8
  %29 = load double, double* %root2_9, align 8
  %call5 = call i32 (i8*, ...) @printf(i8* %27, double %28, double %29)
  br label %if.end14

if.end14:                                         ; preds = %if.end19, %if.end10
  ret i32 0

if.then15:                                        ; preds = %if.else
  %30 = load double, double* %b_5, align 8
  %31 = load double, double* %a_4, align 8
  %mul.17 = fmul double %31, -2.000000e+00
  %SDiv.18 = fdiv double %30, %mul.17
  store double %SDiv.18, double* %root2_9, align 8
  %32 = load double, double* %root2_9, align 8
  store double %32, double* %root1_8, align 8
  %33 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.3, i32 0, i32 0) to i8*
  %34 = load double, double* %root1_8, align 8
  %call6 = call i32 (i8*, ...) @printf(i8* %33, double %34)
  br label %if.end19

if.else16:                                        ; preds = %if.else
  %35 = load double, double* %b_5, align 8
  %36 = load double, double* %a_4, align 8
  %mul.20 = fmul double %36, -2.000000e+00
  %SDiv.21 = fdiv double %35, %mul.20
  store double %SDiv.21, double* %realPart_10, align 8
  %37 = load double, double* %determinant_7, align 8
  %sub.22 = fsub double -0.000000e+00, %37
  store double %sub.22, double* %_temp___sqrt_arg8_20, align 8
  %38 = load double, double* %_temp___sqrt_arg8_20, align 8
  %call723 = call double @sqrt(double %38)
  store double %call723, double* %_temp___save_sqrt9_21, align 8
  %39 = load double, double* %_temp___save_sqrt9_21, align 8
  %40 = load double, double* %_temp___save_sqrt9_21, align 8
  %cmp5 = fcmp one double %39, %40
  br i1 %cmp5, label %if.then24, label %if.else25

if.end19:                                         ; preds = %if.end27, %if.then15
  br label %if.end14

if.then24:                                        ; preds = %if.else16
  %41 = load double, double* %_temp___sqrt_arg8_20, align 8
  %call726 = call double @sqrt(double %41)
  store double %call726, double* %_temp___save_sqrt9_21, align 8
  br label %if.end27

if.else25:                                        ; preds = %if.else16
  br label %if.end27

if.end27:                                         ; preds = %if.else25, %if.then24
  %42 = load double, double* %_temp___save_sqrt9_21, align 8
  %43 = load double, double* %a_4, align 8
  %mul.28 = fmul double %43, 2.000000e+00
  %SDiv.29 = fdiv double %42, %mul.28
  store double %SDiv.29, double* %imaginaryPart_11, align 8
  %44 = bitcast i8* getelementptr inbounds ([44 x i8], [44 x i8]* @.str.4, i32 0, i32 0) to i8*
  %45 = load double, double* %realPart_10, align 8
  %46 = load double, double* %imaginaryPart_11, align 8
  %47 = load double, double* %realPart_10, align 8
  %48 = load double, double* %imaginaryPart_11, align 8
  %call8 = call i32 (i8*, ...) @printf(i8* %44, double %45, double %46, double %47, double %48)
  br label %if.end19
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

declare double @sqrt(double)
