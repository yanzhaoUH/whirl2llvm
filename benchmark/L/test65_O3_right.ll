; ModuleID = 'test65.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.TIME = type { i32, i32, i32 }

@.str.1 = private unnamed_addr constant [48 x i8] c"Enter hours, minutes and seconds respectively: \00", align 1
@.str.2 = private unnamed_addr constant [9 x i8] c"%d %d %d\00", align 1
@.str.4 = private unnamed_addr constant [30 x i8] c"\0ATIME DIFFERENCE: %d:%d:%d - \00", align 1
@.str.5 = private unnamed_addr constant [10 x i8] c"%d:%d:%d \00", align 1
@.str.6 = private unnamed_addr constant [12 x i8] c"= %d:%d:%d\0A\00", align 1
@str = private unnamed_addr constant [19 x i8] c"Enter start time: \00"
@str.7 = private unnamed_addr constant [18 x i8] c"Enter stop time: \00"

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %startTime = alloca %struct.TIME, align 8
  %stopTime = alloca %struct.TIME, align 8
  %0 = bitcast %struct.TIME* %startTime to i8*
  call void @llvm.lifetime.start(i64 12, i8* %0) #1
  %1 = bitcast %struct.TIME* %stopTime to i8*
  call void @llvm.lifetime.start(i64 12, i8* %1) #1
  %puts = tail call i32 @puts(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @str, i64 0, i64 0))
  %call1 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.1, i64 0, i64 0)) #1
  %hours = getelementptr inbounds %struct.TIME, %struct.TIME* %startTime, i64 0, i32 2
  %minutes = getelementptr inbounds %struct.TIME, %struct.TIME* %startTime, i64 0, i32 1
  %seconds = getelementptr inbounds %struct.TIME, %struct.TIME* %startTime, i64 0, i32 0
  %call2 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.2, i64 0, i64 0), i32* %hours, i32* %minutes, i32* %seconds) #1
  %puts23 = call i32 @puts(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @str.7, i64 0, i64 0))
  %call4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.1, i64 0, i64 0)) #1
  %hours5 = getelementptr inbounds %struct.TIME, %struct.TIME* %stopTime, i64 0, i32 2
  %minutes6 = getelementptr inbounds %struct.TIME, %struct.TIME* %stopTime, i64 0, i32 1
  %seconds7 = getelementptr inbounds %struct.TIME, %struct.TIME* %stopTime, i64 0, i32 0
  %call8 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.2, i64 0, i64 0), i32* %hours5, i32* %minutes6, i32* %seconds7) #1
  %startTime.coerce.sroa.0.0..sroa_cast = bitcast %struct.TIME* %startTime to i64*
  %startTime.coerce.sroa.0.0.copyload = load i64, i64* %startTime.coerce.sroa.0.0..sroa_cast, align 8
  %startTime.coerce.sroa.2.0.copyload = load i32, i32* %hours, align 8
  %stopTime.coerce.sroa.0.0..sroa_cast = bitcast %struct.TIME* %stopTime to i64*
  %stopTime.coerce.sroa.0.0.copyload = load i64, i64* %stopTime.coerce.sroa.0.0..sroa_cast, align 8
  %stopTime.coerce.sroa.2.0.copyload = load i32, i32* %hours5, align 8
  %stop.sroa.0.0.extract.trunc.i = trunc i64 %stopTime.coerce.sroa.0.0.copyload to i32
  %start.sroa.0.0.extract.trunc.i = trunc i64 %startTime.coerce.sroa.0.0.copyload to i32
  %cmp.i = icmp sgt i32 %stop.sroa.0.0.extract.trunc.i, %start.sroa.0.0.extract.trunc.i
  br i1 %cmp.i, label %if.then.i, label %if.end.i

if.then.i:                                        ; preds = %entry
  %start.sroa.0.4.extract.shift51.i = add i64 %startTime.coerce.sroa.0.0.copyload, -4294967296
  %start.sroa.0.4.insert.shift.i = and i64 %start.sroa.0.4.extract.shift51.i, -4294967296
  %add.i = add i64 %startTime.coerce.sroa.0.0.copyload, 60
  %start.sroa.0.0.insert.ext.i = and i64 %add.i, 4294967295
  %start.sroa.0.0.insert.insert.i = or i64 %start.sroa.0.4.insert.shift.i, %start.sroa.0.0.insert.ext.i
  br label %if.end.i

if.end.i:                                         ; preds = %if.then.i, %entry
  %start.sroa.0.0.i = phi i64 [ %start.sroa.0.0.insert.insert.i, %if.then.i ], [ %startTime.coerce.sroa.0.0.copyload, %entry ]
  %start.sroa.0.0.extract.trunc35.i = trunc i64 %start.sroa.0.0.i to i32
  %sub.i = sub nsw i32 %start.sroa.0.0.extract.trunc35.i, %stop.sroa.0.0.extract.trunc.i
  %stop.sroa.0.4.extract.shift.i = lshr i64 %stopTime.coerce.sroa.0.0.copyload, 32
  %stop.sroa.0.4.extract.trunc.i = trunc i64 %stop.sroa.0.4.extract.shift.i to i32
  %start.sroa.0.4.extract.shift37.i = lshr i64 %start.sroa.0.0.i, 32
  %start.sroa.0.4.extract.trunc38.i = trunc i64 %start.sroa.0.4.extract.shift37.i to i32
  %cmp9.i = icmp sgt i32 %stop.sroa.0.4.extract.trunc.i, %start.sroa.0.4.extract.trunc38.i
  br i1 %cmp9.i, label %if.then.10.i, label %differenceBetweenTimePeriod.exit

if.then.10.i:                                     ; preds = %if.end.i
  %dec11.i = add nsw i32 %startTime.coerce.sroa.2.0.copyload, -1
  %add13.i = shl nuw i64 %start.sroa.0.4.extract.shift37.i, 32
  %start.sroa.0.4.insert.shift44.i = add i64 %add13.i, 257698037760
  %start.sroa.0.4.insert.mask45.i = and i64 %start.sroa.0.0.i, 4294967295
  %start.sroa.0.4.insert.insert46.i = or i64 %start.sroa.0.4.insert.shift44.i, %start.sroa.0.4.insert.mask45.i
  br label %differenceBetweenTimePeriod.exit

differenceBetweenTimePeriod.exit:                 ; preds = %if.end.i, %if.then.10.i
  %start.sroa.0.1.i = phi i64 [ %start.sroa.0.4.insert.insert46.i, %if.then.10.i ], [ %start.sroa.0.0.i, %if.end.i ]
  %start.sroa.11.0.i = phi i32 [ %dec11.i, %if.then.10.i ], [ %startTime.coerce.sroa.2.0.copyload, %if.end.i ]
  %start.sroa.0.4.extract.shift48.i = lshr i64 %start.sroa.0.1.i, 32
  %start.sroa.0.4.extract.trunc49.i = trunc i64 %start.sroa.0.4.extract.shift48.i to i32
  %sub17.i = sub nsw i32 %start.sroa.0.4.extract.trunc49.i, %stop.sroa.0.4.extract.trunc.i
  %sub21.i = sub nsw i32 %start.sroa.11.0.i, %stopTime.coerce.sroa.2.0.copyload
  %2 = load i32, i32* %minutes, align 4, !tbaa !1
  %call12 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.4, i64 0, i64 0), i32 %startTime.coerce.sroa.2.0.copyload, i32 %2, i32 %start.sroa.0.0.extract.trunc.i) #1
  %3 = load i32, i32* %hours5, align 8, !tbaa !6
  %4 = load i32, i32* %minutes6, align 4, !tbaa !1
  %5 = load i32, i32* %seconds7, align 8, !tbaa !7
  %call16 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.5, i64 0, i64 0), i32 %3, i32 %4, i32 %5) #1
  %call20 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.6, i64 0, i64 0), i32 %sub21.i, i32 %sub17.i, i32 %sub.i) #1
  call void @llvm.lifetime.end(i64 12, i8* %1) #1
  call void @llvm.lifetime.end(i64 12, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind uwtable
define void @differenceBetweenTimePeriod(i64 %start.coerce0, i32 %start.coerce1, i64 %stop.coerce0, i32 %stop.coerce1, %struct.TIME* nocapture %diff) #0 {
entry:
  %stop.sroa.0.0.extract.trunc = trunc i64 %stop.coerce0 to i32
  %start.sroa.0.0.extract.trunc = trunc i64 %start.coerce0 to i32
  %cmp = icmp sgt i32 %stop.sroa.0.0.extract.trunc, %start.sroa.0.0.extract.trunc
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %start.sroa.0.4.extract.shift51 = add i64 %start.coerce0, -4294967296
  %start.sroa.0.4.insert.shift = and i64 %start.sroa.0.4.extract.shift51, -4294967296
  %add = add i64 %start.coerce0, 60
  %start.sroa.0.0.insert.ext = and i64 %add, 4294967295
  %start.sroa.0.0.insert.insert = or i64 %start.sroa.0.4.insert.shift, %start.sroa.0.0.insert.ext
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %start.sroa.0.0 = phi i64 [ %start.sroa.0.0.insert.insert, %if.then ], [ %start.coerce0, %entry ]
  %start.sroa.0.0.extract.trunc35 = trunc i64 %start.sroa.0.0 to i32
  %sub = sub nsw i32 %start.sroa.0.0.extract.trunc35, %stop.sroa.0.0.extract.trunc
  %seconds6 = getelementptr inbounds %struct.TIME, %struct.TIME* %diff, i64 0, i32 0
  store i32 %sub, i32* %seconds6, align 4, !tbaa !7
  %stop.sroa.0.4.extract.shift = lshr i64 %stop.coerce0, 32
  %stop.sroa.0.4.extract.trunc = trunc i64 %stop.sroa.0.4.extract.shift to i32
  %start.sroa.0.4.extract.shift37 = lshr i64 %start.sroa.0.0, 32
  %start.sroa.0.4.extract.trunc38 = trunc i64 %start.sroa.0.4.extract.shift37 to i32
  %cmp9 = icmp sgt i32 %stop.sroa.0.4.extract.trunc, %start.sroa.0.4.extract.trunc38
  br i1 %cmp9, label %if.then.10, label %if.end.14

if.then.10:                                       ; preds = %if.end
  %dec11 = add nsw i32 %start.coerce1, -1
  %add13 = shl nuw i64 %start.sroa.0.4.extract.shift37, 32
  %start.sroa.0.4.insert.shift44 = add i64 %add13, 257698037760
  %start.sroa.0.4.insert.mask45 = and i64 %start.sroa.0.0, 4294967295
  %start.sroa.0.4.insert.insert46 = or i64 %start.sroa.0.4.insert.shift44, %start.sroa.0.4.insert.mask45
  br label %if.end.14

if.end.14:                                        ; preds = %if.then.10, %if.end
  %start.sroa.0.1 = phi i64 [ %start.sroa.0.4.insert.insert46, %if.then.10 ], [ %start.sroa.0.0, %if.end ]
  %start.sroa.11.0 = phi i32 [ %dec11, %if.then.10 ], [ %start.coerce1, %if.end ]
  %start.sroa.0.4.extract.shift48 = lshr i64 %start.sroa.0.1, 32
  %start.sroa.0.4.extract.trunc49 = trunc i64 %start.sroa.0.4.extract.shift48 to i32
  %sub17 = sub nsw i32 %start.sroa.0.4.extract.trunc49, %stop.sroa.0.4.extract.trunc
  %minutes18 = getelementptr inbounds %struct.TIME, %struct.TIME* %diff, i64 0, i32 1
  store i32 %sub17, i32* %minutes18, align 4, !tbaa !1
  %sub21 = sub nsw i32 %start.sroa.11.0, %stop.coerce1
  %hours22 = getelementptr inbounds %struct.TIME, %struct.TIME* %diff, i64 0, i32 2
  store i32 %sub21, i32* %hours22, align 4, !tbaa !6
  ret void
}

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @puts(i8* nocapture readonly) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !3, i64 4}
!2 = !{!"TIME", !3, i64 0, !3, i64 4, !3, i64 8}
!3 = !{!"int", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!2, !3, i64 8}
!7 = !{!2, !3, i64 0}
