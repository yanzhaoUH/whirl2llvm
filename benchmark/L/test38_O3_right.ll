; ModuleID = 'test38.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [27 x i8] c"Enter a positive integer: \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.2 = private unnamed_addr constant [22 x i8] c"Factorial of %d = %ld\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %n = alloca i32, align 4
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start(i64 4, i8* %0) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %n) #1
  %1 = load i32, i32* %n, align 4, !tbaa !1
  %cmp.3.i = icmp sgt i32 %1, 0
  br i1 %cmp.3.i, label %if.then.lr.ph.i, label %multiplyNumbers.exit

if.then.lr.ph.i:                                  ; preds = %entry
  %2 = sext i32 %1 to i64
  br label %if.then.i

if.then.i:                                        ; preds = %if.then.i, %if.then.lr.ph.i
  %indvars.iv.i = phi i64 [ %2, %if.then.lr.ph.i ], [ %indvars.iv.next.i, %if.then.i ]
  %accumulator.tr4.i = phi i64 [ 1, %if.then.lr.ph.i ], [ %mul.i, %if.then.i ]
  %indvars.iv.next.i = add nsw i64 %indvars.iv.i, -1
  %mul.i = mul nsw i64 %accumulator.tr4.i, %indvars.iv.i
  %cmp.i = icmp sgt i64 %indvars.iv.i, 1
  br i1 %cmp.i, label %if.then.i, label %multiplyNumbers.exit.loopexit

multiplyNumbers.exit.loopexit:                    ; preds = %if.then.i
  %mul.i.lcssa = phi i64 [ %mul.i, %if.then.i ]
  br label %multiplyNumbers.exit

multiplyNumbers.exit:                             ; preds = %multiplyNumbers.exit.loopexit, %entry
  %accumulator.tr.lcssa.i = phi i64 [ 1, %entry ], [ %mul.i.lcssa, %multiplyNumbers.exit.loopexit ]
  %call3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.2, i64 0, i64 0), i32 %1, i64 %accumulator.tr.lcssa.i) #1
  call void @llvm.lifetime.end(i64 4, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind readnone uwtable
define i64 @multiplyNumbers(i32 %n) #3 {
entry:
  %cmp.3 = icmp sgt i32 %n, 0
  br i1 %cmp.3, label %if.then.lr.ph, label %return

if.then.lr.ph:                                    ; preds = %entry
  %0 = sext i32 %n to i64
  br label %if.then

if.then:                                          ; preds = %if.then.lr.ph, %if.then
  %indvars.iv = phi i64 [ %0, %if.then.lr.ph ], [ %indvars.iv.next, %if.then ]
  %accumulator.tr4 = phi i64 [ 1, %if.then.lr.ph ], [ %mul, %if.then ]
  %indvars.iv.next = add nsw i64 %indvars.iv, -1
  %mul = mul nsw i64 %indvars.iv, %accumulator.tr4
  %cmp = icmp sgt i64 %indvars.iv, 1
  br i1 %cmp, label %if.then, label %return.loopexit

return.loopexit:                                  ; preds = %if.then
  %mul.lcssa = phi i64 [ %mul, %if.then ]
  br label %return

return:                                           ; preds = %return.loopexit, %entry
  %accumulator.tr.lcssa = phi i64 [ 1, %entry ], [ %mul.lcssa, %return.loopexit ]
  ret i64 %accumulator.tr.lcssa
}

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readnone uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
