; ModuleID = 'test19_O.bc'

@.str = private constant [28 x i8] c"Enter the number of terms: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [27 x i8] c"Fibonacci Series: %d, %d, \00", align 1
@.str.3 = private constant [5 x i8] c"%d, \00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_50 = alloca i32
  %.preg_I4_4_52 = alloca i32
  %.preg_I4_4_51 = alloca i32
  %.preg_I4_4_49 = alloca i32
  %_temp_dummy10_9 = alloca i32, align 4
  %_temp_dummy11_10 = alloca i32, align 4
  %_temp_dummy12_11 = alloca i32, align 4
  %_temp_dummy13_12 = alloca i32, align 4
  %_temp_ehpit0_13 = alloca i32, align 4
  %i_4 = alloca i32, align 4
  %n_5 = alloca i32, align 4
  %nextTerm_8 = alloca i32, align 4
  %old_frame_pointer_18.addr = alloca i64, align 8
  %return_address_19.addr = alloca i64, align 8
  %t1_6 = alloca i32, align 4
  %t2_7 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_5 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %3, i32 0, i32 1)
  store i32 0, i32* %.preg_I4_4_49, align 8
  %4 = load i32, i32* %n_5, align 4
  %add = add i32 %4, -3
  %cmp1 = icmp sge i32 %add, 0
  br i1 %cmp1, label %tb, label %L1794

tb:                                               ; preds = %entry
  store i32 1, i32* %.preg_I4_4_51, align 8
  store i32 0, i32* %.preg_I4_4_52, align 8
  br label %L2306

L1794:                                            ; preds = %fb, %entry
  ret i32 0

L2306:                                            ; preds = %L2306, %tb
  %5 = load i32, i32* %.preg_I4_4_51
  %6 = load i32, i32* %.preg_I4_4_52
  %add.1 = add i32 %5, %6
  store i32 %add.1, i32* %.preg_I4_4_50, align 8
  %7 = load i32, i32* %.preg_I4_4_51
  store i32 %7, i32* %.preg_I4_4_52, align 8
  %8 = load i32, i32* %.preg_I4_4_50
  store i32 %8, i32* %.preg_I4_4_51, align 8
  %9 = bitcast i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.3, i32 0, i32 0) to i8*
  %10 = load i32, i32* %.preg_I4_4_50
  %call4 = call i32 (i8*, ...) @printf(i8* %9, i32 %10)
  %11 = load i32, i32* %.preg_I4_4_49
  %add.2 = add i32 %11, 1
  store i32 %add.2, i32* %.preg_I4_4_49, align 8
  %12 = load i32, i32* %.preg_I4_4_49
  %13 = load i32, i32* %n_5, align 4
  %add.3 = add i32 %13, -3
  %cmp2 = icmp sle i32 %12, %add.3
  br i1 %cmp2, label %L2306, label %fb

fb:                                               ; preds = %L2306
  br label %L1794
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
