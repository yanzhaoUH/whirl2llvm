; ModuleID = 'test11.bc'

@.str = private constant [12 x i8] c"%lf %lf %lf\00", align 1
@.str.1 = private constant [28 x i8] c"%.2f is the largest number.\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %n1_4 = alloca double, align 8
  %n2_5 = alloca double, align 8
  %n3_6 = alloca double, align 8
  %0 = bitcast i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0) to i8*
  %1 = bitcast double* %n1_4 to double*
  %2 = bitcast double* %n2_5 to double*
  %3 = bitcast double* %n3_6 to double*
  %call1 = call i32 (i8*, ...) @scanf(i8* %0, double* %1, double* %2, double* %3)
  %4 = load double, double* %n1_4, align 8
  %5 = load double, double* %n2_5, align 8
  %cmp1 = fcmp oge double %4, %5
  br i1 %cmp1, label %land.rhs, label %land.end

if.then:                                          ; preds = %land.end
  %6 = bitcast i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.1, i32 0, i32 0) to i8*
  %7 = load double, double* %n1_4, align 8
  %call2 = call i32 (i8*, ...) @printf(i8* %6, double %7)
  br label %if.end

if.else:                                          ; preds = %land.end
  br label %if.end

land.rhs:                                         ; preds = %entry
  %8 = load double, double* %n1_4, align 8
  %9 = load double, double* %n3_6, align 8
  %cmp2 = fcmp oge double %8, %9
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %10 = phi i1 [ false, %entry ], [ %cmp2, %land.rhs ]
  br i1 %10, label %if.then, label %if.else

if.end:                                           ; preds = %if.else, %if.then
  %11 = load double, double* %n2_5, align 8
  %12 = load double, double* %n1_4, align 8
  %cmp3 = fcmp oge double %11, %12
  br i1 %cmp3, label %land.rhs3, label %land.end4

if.then1:                                         ; preds = %land.end4
  %13 = bitcast i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.1, i32 0, i32 0) to i8*
  %14 = load double, double* %n2_5, align 8
  %call3 = call i32 (i8*, ...) @printf(i8* %13, double %14)
  br label %if.end5

if.else2:                                         ; preds = %land.end4
  br label %if.end5

land.rhs3:                                        ; preds = %if.end
  %15 = load double, double* %n2_5, align 8
  %16 = load double, double* %n3_6, align 8
  %cmp4 = fcmp oge double %15, %16
  br label %land.end4

land.end4:                                        ; preds = %land.rhs3, %if.end
  %17 = phi i1 [ false, %if.end ], [ %cmp4, %land.rhs3 ]
  br i1 %17, label %if.then1, label %if.else2

if.end5:                                          ; preds = %if.else2, %if.then1
  %18 = load double, double* %n3_6, align 8
  %19 = load double, double* %n1_4, align 8
  %cmp5 = fcmp oge double %18, %19
  br i1 %cmp5, label %land.rhs8, label %land.end9

if.then6:                                         ; preds = %land.end9
  %20 = bitcast i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.1, i32 0, i32 0) to i8*
  %21 = load double, double* %n3_6, align 8
  %call4 = call i32 (i8*, ...) @printf(i8* %20, double %21)
  br label %if.end10

if.else7:                                         ; preds = %land.end9
  br label %if.end10

land.rhs8:                                        ; preds = %if.end5
  %22 = load double, double* %n3_6, align 8
  %23 = load double, double* %n2_5, align 8
  %cmp6 = fcmp oge double %22, %23
  br label %land.end9

land.end9:                                        ; preds = %land.rhs8, %if.end5
  %24 = phi i1 [ false, %if.end5 ], [ %cmp6, %land.rhs8 ]
  br i1 %24, label %if.then6, label %if.else7

if.end10:                                         ; preds = %if.else7, %if.then6
  ret i32 0
}

declare i32 @scanf(i8*, ...)

declare i32 @printf(i8*, ...)
