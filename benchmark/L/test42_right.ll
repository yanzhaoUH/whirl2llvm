; ModuleID = 'test42.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [15 x i8] c"Instructions:\0A\00", align 1
@.str.1 = private unnamed_addr constant [51 x i8] c"1. Enter alphabet 'o' to convert binary to octal.\0A\00", align 1
@.str.2 = private unnamed_addr constant [51 x i8] c"2. Enter alphabet 'b' to convert octal to binary.\0A\00", align 1
@.str.3 = private unnamed_addr constant [3 x i8] c"%c\00", align 1
@.str.4 = private unnamed_addr constant [24 x i8] c"Enter a binary number: \00", align 1
@.str.5 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.6 = private unnamed_addr constant [27 x i8] c"%d in binary = %d in octal\00", align 1
@.str.7 = private unnamed_addr constant [23 x i8] c"Enter a octal number: \00", align 1
@.str.8 = private unnamed_addr constant [27 x i8] c"%d in octal = %d in binary\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %n = alloca i32, align 4
  %c = alloca i8, align 1
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str, i32 0, i32 0))
  %call1 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([51 x i8], [51 x i8]* @.str.1, i32 0, i32 0))
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([51 x i8], [51 x i8]* @.str.2, i32 0, i32 0))
  %call3 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.3, i32 0, i32 0), i8* %c)
  %0 = load i8, i8* %c, align 1
  %conv = sext i8 %0 to i32
  %cmp = icmp eq i32 %conv, 111
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i8, i8* %c, align 1
  %conv5 = sext i8 %1 to i32
  %cmp6 = icmp eq i32 %conv5, 79
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %call8 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.4, i32 0, i32 0))
  %call9 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.5, i32 0, i32 0), i32* %n)
  %2 = load i32, i32* %n, align 4
  %3 = load i32, i32* %n, align 4
  %call10 = call i32 @binary_octal(i32 %3)
  %call11 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.6, i32 0, i32 0), i32 %2, i32 %call10)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  %4 = load i8, i8* %c, align 1
  %conv12 = sext i8 %4 to i32
  %cmp13 = icmp eq i32 %conv12, 98
  br i1 %cmp13, label %if.then.19, label %lor.lhs.false.15

lor.lhs.false.15:                                 ; preds = %if.end
  %5 = load i8, i8* %c, align 1
  %conv16 = sext i8 %5 to i32
  %cmp17 = icmp eq i32 %conv16, 66
  br i1 %cmp17, label %if.then.19, label %if.end.24

if.then.19:                                       ; preds = %lor.lhs.false.15, %if.end
  %call20 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.7, i32 0, i32 0))
  %call21 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.5, i32 0, i32 0), i32* %n)
  %6 = load i32, i32* %n, align 4
  %7 = load i32, i32* %n, align 4
  %call22 = call i32 @octal_binary(i32 %7)
  %call23 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.8, i32 0, i32 0), i32 %6, i32 %call22)
  br label %if.end.24

if.end.24:                                        ; preds = %if.then.19, %lor.lhs.false.15
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @__isoc99_scanf(i8*, ...) #1

; Function Attrs: nounwind uwtable
define i32 @binary_octal(i32 %n) #0 {
entry:
  %n.addr = alloca i32, align 4
  %octal = alloca i32, align 4
  %decimal = alloca i32, align 4
  %i = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32 0, i32* %octal, align 4
  store i32 0, i32* %decimal, align 4
  store i32 0, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %n.addr, align 4
  %cmp = icmp ne i32 %0, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load i32, i32* %n.addr, align 4
  %rem = srem i32 %1, 10
  %conv = sitofp i32 %rem to double
  %2 = load i32, i32* %i, align 4
  %conv1 = sitofp i32 %2 to double
  %call = call double @pow(double 2.000000e+00, double %conv1) #3
  %mul = fmul double %conv, %call
  %3 = load i32, i32* %decimal, align 4
  %conv2 = sitofp i32 %3 to double
  %add = fadd double %conv2, %mul
  %conv3 = fptosi double %add to i32
  store i32 %conv3, i32* %decimal, align 4
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  %5 = load i32, i32* %n.addr, align 4
  %div = sdiv i32 %5, 10
  store i32 %div, i32* %n.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 1, i32* %i, align 4
  br label %while.cond.4

while.cond.4:                                     ; preds = %while.body.7, %while.end
  %6 = load i32, i32* %decimal, align 4
  %cmp5 = icmp ne i32 %6, 0
  br i1 %cmp5, label %while.body.7, label %while.end.13

while.body.7:                                     ; preds = %while.cond.4
  %7 = load i32, i32* %decimal, align 4
  %rem8 = srem i32 %7, 8
  %8 = load i32, i32* %i, align 4
  %mul9 = mul nsw i32 %rem8, %8
  %9 = load i32, i32* %octal, align 4
  %add10 = add nsw i32 %9, %mul9
  store i32 %add10, i32* %octal, align 4
  %10 = load i32, i32* %decimal, align 4
  %div11 = sdiv i32 %10, 8
  store i32 %div11, i32* %decimal, align 4
  %11 = load i32, i32* %i, align 4
  %mul12 = mul nsw i32 %11, 10
  store i32 %mul12, i32* %i, align 4
  br label %while.cond.4

while.end.13:                                     ; preds = %while.cond.4
  %12 = load i32, i32* %octal, align 4
  ret i32 %12
}

; Function Attrs: nounwind uwtable
define i32 @octal_binary(i32 %n) #0 {
entry:
  %n.addr = alloca i32, align 4
  %decimal = alloca i32, align 4
  %binary = alloca i32, align 4
  %i = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32 0, i32* %decimal, align 4
  store i32 0, i32* %binary, align 4
  store i32 0, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %n.addr, align 4
  %cmp = icmp ne i32 %0, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load i32, i32* %n.addr, align 4
  %rem = srem i32 %1, 10
  %conv = sitofp i32 %rem to double
  %2 = load i32, i32* %i, align 4
  %conv1 = sitofp i32 %2 to double
  %call = call double @pow(double 8.000000e+00, double %conv1) #3
  %mul = fmul double %conv, %call
  %3 = load i32, i32* %decimal, align 4
  %conv2 = sitofp i32 %3 to double
  %add = fadd double %conv2, %mul
  %conv3 = fptosi double %add to i32
  store i32 %conv3, i32* %decimal, align 4
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  %5 = load i32, i32* %n.addr, align 4
  %div = sdiv i32 %5, 10
  store i32 %div, i32* %n.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 1, i32* %i, align 4
  br label %while.cond.4

while.cond.4:                                     ; preds = %while.body.7, %while.end
  %6 = load i32, i32* %decimal, align 4
  %cmp5 = icmp ne i32 %6, 0
  br i1 %cmp5, label %while.body.7, label %while.end.13

while.body.7:                                     ; preds = %while.cond.4
  %7 = load i32, i32* %decimal, align 4
  %rem8 = srem i32 %7, 2
  %8 = load i32, i32* %i, align 4
  %mul9 = mul nsw i32 %rem8, %8
  %9 = load i32, i32* %binary, align 4
  %add10 = add nsw i32 %9, %mul9
  store i32 %add10, i32* %binary, align 4
  %10 = load i32, i32* %decimal, align 4
  %div11 = sdiv i32 %10, 2
  store i32 %div11, i32* %decimal, align 4
  %11 = load i32, i32* %i, align 4
  %mul12 = mul nsw i32 %11, 10
  store i32 %mul12, i32* %i, align 4
  br label %while.cond.4

while.end.13:                                     ; preds = %while.cond.4
  %12 = load i32, i32* %binary, align 4
  ret i32 %12
}

; Function Attrs: nounwind
declare double @pow(double, double) #2

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
