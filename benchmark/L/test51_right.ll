; ModuleID = 'test51.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [41 x i8] c"Enter rows and column for first matrix: \00", align 1
@.str.1 = private unnamed_addr constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private unnamed_addr constant [42 x i8] c"Enter rows and column for second matrix: \00", align 1
@.str.3 = private unnamed_addr constant [59 x i8] c"Error! column of first matrix not equal to row of second.\0A\00", align 1
@.str.4 = private unnamed_addr constant [5 x i8] c"%d%d\00", align 1
@.str.5 = private unnamed_addr constant [30 x i8] c"\0AEnter elements of matrix 1:\0A\00", align 1
@.str.6 = private unnamed_addr constant [23 x i8] c"Enter elements a%d%d: \00", align 1
@.str.7 = private unnamed_addr constant [30 x i8] c"\0AEnter elements of matrix 2:\0A\00", align 1
@.str.8 = private unnamed_addr constant [23 x i8] c"Enter elements b%d%d: \00", align 1
@.str.9 = private unnamed_addr constant [17 x i8] c"\0AOutput Matrix:\0A\00", align 1
@.str.10 = private unnamed_addr constant [5 x i8] c"%d  \00", align 1
@.str.11 = private unnamed_addr constant [3 x i8] c"\0A\0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %firstMatrix = alloca [10 x [10 x i32]], align 16
  %secondMatrix = alloca [10 x [10 x i32]], align 16
  %mult = alloca [10 x [10 x i32]], align 16
  %rowFirst = alloca i32, align 4
  %columnFirst = alloca i32, align 4
  %rowSecond = alloca i32, align 4
  %columnSecond = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str, i32 0, i32 0))
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0), i32* %rowFirst, i32* %columnFirst)
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.2, i32 0, i32 0))
  %call3 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0), i32* %rowSecond, i32* %columnSecond)
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %columnFirst, align 4
  %1 = load i32, i32* %rowSecond, align 4
  %cmp = icmp ne i32 %0, %1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([59 x i8], [59 x i8]* @.str.3, i32 0, i32 0))
  %call5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str, i32 0, i32 0))
  %call6 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.4, i32 0, i32 0), i32* %rowFirst, i32* %columnFirst)
  %call7 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.2, i32 0, i32 0))
  %call8 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.4, i32 0, i32 0), i32* %rowSecond, i32* %columnSecond)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %arraydecay = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %firstMatrix, i32 0, i32 0
  %arraydecay9 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %secondMatrix, i32 0, i32 0
  %2 = load i32, i32* %rowFirst, align 4
  %3 = load i32, i32* %columnFirst, align 4
  %4 = load i32, i32* %rowSecond, align 4
  %5 = load i32, i32* %columnSecond, align 4
  call void @enterData([10 x i32]* %arraydecay, [10 x i32]* %arraydecay9, i32 %2, i32 %3, i32 %4, i32 %5)
  %arraydecay10 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %firstMatrix, i32 0, i32 0
  %arraydecay11 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %secondMatrix, i32 0, i32 0
  %arraydecay12 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %mult, i32 0, i32 0
  %6 = load i32, i32* %rowFirst, align 4
  %7 = load i32, i32* %columnFirst, align 4
  %8 = load i32, i32* %rowSecond, align 4
  %9 = load i32, i32* %columnSecond, align 4
  call void @multiplyMatrices([10 x i32]* %arraydecay10, [10 x i32]* %arraydecay11, [10 x i32]* %arraydecay12, i32 %6, i32 %7, i32 %8, i32 %9)
  %arraydecay13 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %mult, i32 0, i32 0
  %10 = load i32, i32* %rowFirst, align 4
  %11 = load i32, i32* %columnSecond, align 4
  call void @display([10 x i32]* %arraydecay13, i32 %10, i32 %11)
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @__isoc99_scanf(i8*, ...) #1

; Function Attrs: nounwind uwtable
define void @enterData([10 x i32]* %firstMatrix, [10 x i32]* %secondMatrix, i32 %rowFirst, i32 %columnFirst, i32 %rowSecond, i32 %columnSecond) #0 {
entry:
  %firstMatrix.addr = alloca [10 x i32]*, align 8
  %secondMatrix.addr = alloca [10 x i32]*, align 8
  %rowFirst.addr = alloca i32, align 4
  %columnFirst.addr = alloca i32, align 4
  %rowSecond.addr = alloca i32, align 4
  %columnSecond.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store [10 x i32]* %firstMatrix, [10 x i32]** %firstMatrix.addr, align 8
  store [10 x i32]* %secondMatrix, [10 x i32]** %secondMatrix.addr, align 8
  store i32 %rowFirst, i32* %rowFirst.addr, align 4
  store i32 %columnFirst, i32* %columnFirst.addr, align 4
  store i32 %rowSecond, i32* %rowSecond.addr, align 4
  store i32 %columnSecond, i32* %columnSecond.addr, align 4
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.5, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc.10, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %rowFirst.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end.12

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4
  br label %for.cond.1

for.cond.1:                                       ; preds = %for.inc, %for.body
  %2 = load i32, i32* %j, align 4
  %3 = load i32, i32* %columnFirst.addr, align 4
  %cmp2 = icmp slt i32 %2, %3
  br i1 %cmp2, label %for.body.3, label %for.end

for.body.3:                                       ; preds = %for.cond.1
  %4 = load i32, i32* %i, align 4
  %add = add nsw i32 %4, 1
  %5 = load i32, i32* %j, align 4
  %add4 = add nsw i32 %5, 1
  %call5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.6, i32 0, i32 0), i32 %add, i32 %add4)
  %6 = load i32, i32* %i, align 4
  %add6 = add nsw i32 %6, 1
  %7 = load i32, i32* %j, align 4
  %add7 = add nsw i32 %7, 1
  %mul = mul nsw i32 %add6, %add7
  %8 = load i32, i32* %j, align 4
  %idxprom = sext i32 %8 to i64
  %9 = load i32, i32* %i, align 4
  %idxprom8 = sext i32 %9 to i64
  %10 = load [10 x i32]*, [10 x i32]** %firstMatrix.addr, align 8
  %arrayidx = getelementptr inbounds [10 x i32], [10 x i32]* %10, i64 %idxprom8
  %arrayidx9 = getelementptr inbounds [10 x i32], [10 x i32]* %arrayidx, i32 0, i64 %idxprom
  store i32 %mul, i32* %arrayidx9, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body.3
  %11 = load i32, i32* %j, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond.1

for.end:                                          ; preds = %for.cond.1
  br label %for.inc.10

for.inc.10:                                       ; preds = %for.end
  %12 = load i32, i32* %i, align 4
  %inc11 = add nsw i32 %12, 1
  store i32 %inc11, i32* %i, align 4
  br label %for.cond

for.end.12:                                       ; preds = %for.cond
  %call13 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.7, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond.14

for.cond.14:                                      ; preds = %for.inc.33, %for.end.12
  %13 = load i32, i32* %i, align 4
  %14 = load i32, i32* %rowSecond.addr, align 4
  %cmp15 = icmp slt i32 %13, %14
  br i1 %cmp15, label %for.body.16, label %for.end.35

for.body.16:                                      ; preds = %for.cond.14
  store i32 0, i32* %j, align 4
  br label %for.cond.17

for.cond.17:                                      ; preds = %for.inc.30, %for.body.16
  %15 = load i32, i32* %j, align 4
  %16 = load i32, i32* %columnSecond.addr, align 4
  %cmp18 = icmp slt i32 %15, %16
  br i1 %cmp18, label %for.body.19, label %for.end.32

for.body.19:                                      ; preds = %for.cond.17
  %17 = load i32, i32* %i, align 4
  %add20 = add nsw i32 %17, 1
  %18 = load i32, i32* %j, align 4
  %add21 = add nsw i32 %18, 1
  %call22 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.8, i32 0, i32 0), i32 %add20, i32 %add21)
  %19 = load i32, i32* %i, align 4
  %add23 = add nsw i32 %19, 1
  %20 = load i32, i32* %j, align 4
  %add24 = add nsw i32 %20, 1
  %mul25 = mul nsw i32 %add23, %add24
  %21 = load i32, i32* %j, align 4
  %idxprom26 = sext i32 %21 to i64
  %22 = load i32, i32* %i, align 4
  %idxprom27 = sext i32 %22 to i64
  %23 = load [10 x i32]*, [10 x i32]** %secondMatrix.addr, align 8
  %arrayidx28 = getelementptr inbounds [10 x i32], [10 x i32]* %23, i64 %idxprom27
  %arrayidx29 = getelementptr inbounds [10 x i32], [10 x i32]* %arrayidx28, i32 0, i64 %idxprom26
  store i32 %mul25, i32* %arrayidx29, align 4
  br label %for.inc.30

for.inc.30:                                       ; preds = %for.body.19
  %24 = load i32, i32* %j, align 4
  %inc31 = add nsw i32 %24, 1
  store i32 %inc31, i32* %j, align 4
  br label %for.cond.17

for.end.32:                                       ; preds = %for.cond.17
  br label %for.inc.33

for.inc.33:                                       ; preds = %for.end.32
  %25 = load i32, i32* %i, align 4
  %inc34 = add nsw i32 %25, 1
  store i32 %inc34, i32* %i, align 4
  br label %for.cond.14

for.end.35:                                       ; preds = %for.cond.14
  ret void
}

; Function Attrs: nounwind uwtable
define void @multiplyMatrices([10 x i32]* %firstMatrix, [10 x i32]* %secondMatrix, [10 x i32]* %mult, i32 %rowFirst, i32 %columnFirst, i32 %rowSecond, i32 %columnSecond) #0 {
entry:
  %firstMatrix.addr = alloca [10 x i32]*, align 8
  %secondMatrix.addr = alloca [10 x i32]*, align 8
  %mult.addr = alloca [10 x i32]*, align 8
  %rowFirst.addr = alloca i32, align 4
  %columnFirst.addr = alloca i32, align 4
  %rowSecond.addr = alloca i32, align 4
  %columnSecond.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  store [10 x i32]* %firstMatrix, [10 x i32]** %firstMatrix.addr, align 8
  store [10 x i32]* %secondMatrix, [10 x i32]** %secondMatrix.addr, align 8
  store [10 x i32]* %mult, [10 x i32]** %mult.addr, align 8
  store i32 %rowFirst, i32* %rowFirst.addr, align 4
  store i32 %columnFirst, i32* %columnFirst.addr, align 4
  store i32 %rowSecond, i32* %rowSecond.addr, align 4
  store i32 %columnSecond, i32* %columnSecond.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc.6, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %rowFirst.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end.8

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4
  br label %for.cond.1

for.cond.1:                                       ; preds = %for.inc, %for.body
  %2 = load i32, i32* %j, align 4
  %3 = load i32, i32* %columnSecond.addr, align 4
  %cmp2 = icmp slt i32 %2, %3
  br i1 %cmp2, label %for.body.3, label %for.end

for.body.3:                                       ; preds = %for.cond.1
  %4 = load i32, i32* %j, align 4
  %idxprom = sext i32 %4 to i64
  %5 = load i32, i32* %i, align 4
  %idxprom4 = sext i32 %5 to i64
  %6 = load [10 x i32]*, [10 x i32]** %mult.addr, align 8
  %arrayidx = getelementptr inbounds [10 x i32], [10 x i32]* %6, i64 %idxprom4
  %arrayidx5 = getelementptr inbounds [10 x i32], [10 x i32]* %arrayidx, i32 0, i64 %idxprom
  store i32 0, i32* %arrayidx5, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body.3
  %7 = load i32, i32* %j, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond.1

for.end:                                          ; preds = %for.cond.1
  br label %for.inc.6

for.inc.6:                                        ; preds = %for.end
  %8 = load i32, i32* %i, align 4
  %inc7 = add nsw i32 %8, 1
  store i32 %inc7, i32* %i, align 4
  br label %for.cond

for.end.8:                                        ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond.9

for.cond.9:                                       ; preds = %for.inc.36, %for.end.8
  %9 = load i32, i32* %i, align 4
  %10 = load i32, i32* %rowFirst.addr, align 4
  %cmp10 = icmp slt i32 %9, %10
  br i1 %cmp10, label %for.body.11, label %for.end.38

for.body.11:                                      ; preds = %for.cond.9
  store i32 0, i32* %j, align 4
  br label %for.cond.12

for.cond.12:                                      ; preds = %for.inc.33, %for.body.11
  %11 = load i32, i32* %j, align 4
  %12 = load i32, i32* %columnSecond.addr, align 4
  %cmp13 = icmp slt i32 %11, %12
  br i1 %cmp13, label %for.body.14, label %for.end.35

for.body.14:                                      ; preds = %for.cond.12
  store i32 0, i32* %k, align 4
  br label %for.cond.15

for.cond.15:                                      ; preds = %for.inc.30, %for.body.14
  %13 = load i32, i32* %k, align 4
  %14 = load i32, i32* %columnFirst.addr, align 4
  %cmp16 = icmp slt i32 %13, %14
  br i1 %cmp16, label %for.body.17, label %for.end.32

for.body.17:                                      ; preds = %for.cond.15
  %15 = load i32, i32* %k, align 4
  %idxprom18 = sext i32 %15 to i64
  %16 = load i32, i32* %i, align 4
  %idxprom19 = sext i32 %16 to i64
  %17 = load [10 x i32]*, [10 x i32]** %firstMatrix.addr, align 8
  %arrayidx20 = getelementptr inbounds [10 x i32], [10 x i32]* %17, i64 %idxprom19
  %arrayidx21 = getelementptr inbounds [10 x i32], [10 x i32]* %arrayidx20, i32 0, i64 %idxprom18
  %18 = load i32, i32* %arrayidx21, align 4
  %19 = load i32, i32* %j, align 4
  %idxprom22 = sext i32 %19 to i64
  %20 = load i32, i32* %k, align 4
  %idxprom23 = sext i32 %20 to i64
  %21 = load [10 x i32]*, [10 x i32]** %secondMatrix.addr, align 8
  %arrayidx24 = getelementptr inbounds [10 x i32], [10 x i32]* %21, i64 %idxprom23
  %arrayidx25 = getelementptr inbounds [10 x i32], [10 x i32]* %arrayidx24, i32 0, i64 %idxprom22
  %22 = load i32, i32* %arrayidx25, align 4
  %mul = mul nsw i32 %18, %22
  %23 = load i32, i32* %j, align 4
  %idxprom26 = sext i32 %23 to i64
  %24 = load i32, i32* %i, align 4
  %idxprom27 = sext i32 %24 to i64
  %25 = load [10 x i32]*, [10 x i32]** %mult.addr, align 8
  %arrayidx28 = getelementptr inbounds [10 x i32], [10 x i32]* %25, i64 %idxprom27
  %arrayidx29 = getelementptr inbounds [10 x i32], [10 x i32]* %arrayidx28, i32 0, i64 %idxprom26
  %26 = load i32, i32* %arrayidx29, align 4
  %add = add nsw i32 %26, %mul
  store i32 %add, i32* %arrayidx29, align 4
  br label %for.inc.30

for.inc.30:                                       ; preds = %for.body.17
  %27 = load i32, i32* %k, align 4
  %inc31 = add nsw i32 %27, 1
  store i32 %inc31, i32* %k, align 4
  br label %for.cond.15

for.end.32:                                       ; preds = %for.cond.15
  br label %for.inc.33

for.inc.33:                                       ; preds = %for.end.32
  %28 = load i32, i32* %j, align 4
  %inc34 = add nsw i32 %28, 1
  store i32 %inc34, i32* %j, align 4
  br label %for.cond.12

for.end.35:                                       ; preds = %for.cond.12
  br label %for.inc.36

for.inc.36:                                       ; preds = %for.end.35
  %29 = load i32, i32* %i, align 4
  %inc37 = add nsw i32 %29, 1
  store i32 %inc37, i32* %i, align 4
  br label %for.cond.9

for.end.38:                                       ; preds = %for.cond.9
  ret void
}

; Function Attrs: nounwind uwtable
define void @display([10 x i32]* %mult, i32 %rowFirst, i32 %columnSecond) #0 {
entry:
  %mult.addr = alloca [10 x i32]*, align 8
  %rowFirst.addr = alloca i32, align 4
  %columnSecond.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store [10 x i32]* %mult, [10 x i32]** %mult.addr, align 8
  store i32 %rowFirst, i32* %rowFirst.addr, align 4
  store i32 %columnSecond, i32* %columnSecond.addr, align 4
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.9, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc.9, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %rowFirst.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end.11

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4
  br label %for.cond.1

for.cond.1:                                       ; preds = %for.inc, %for.body
  %2 = load i32, i32* %j, align 4
  %3 = load i32, i32* %columnSecond.addr, align 4
  %cmp2 = icmp slt i32 %2, %3
  br i1 %cmp2, label %for.body.3, label %for.end

for.body.3:                                       ; preds = %for.cond.1
  %4 = load i32, i32* %j, align 4
  %idxprom = sext i32 %4 to i64
  %5 = load i32, i32* %i, align 4
  %idxprom4 = sext i32 %5 to i64
  %6 = load [10 x i32]*, [10 x i32]** %mult.addr, align 8
  %arrayidx = getelementptr inbounds [10 x i32], [10 x i32]* %6, i64 %idxprom4
  %arrayidx5 = getelementptr inbounds [10 x i32], [10 x i32]* %arrayidx, i32 0, i64 %idxprom
  %7 = load i32, i32* %arrayidx5, align 4
  %call6 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.10, i32 0, i32 0), i32 %7)
  %8 = load i32, i32* %j, align 4
  %9 = load i32, i32* %columnSecond.addr, align 4
  %sub = sub nsw i32 %9, 1
  %cmp7 = icmp eq i32 %8, %sub
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %for.body.3
  %call8 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.11, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body.3
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %10 = load i32, i32* %j, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond.1

for.end:                                          ; preds = %for.cond.1
  br label %for.inc.9

for.inc.9:                                        ; preds = %for.end
  %11 = load i32, i32* %i, align 4
  %inc10 = add nsw i32 %11, 1
  store i32 %inc10, i32* %i, align 4
  br label %for.cond

for.end.11:                                       ; preds = %for.cond
  ret void
}

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
