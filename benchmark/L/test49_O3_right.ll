; ModuleID = 'test49.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [41 x i8] c"Enter rows and column for first matrix: \00", align 1
@.str.1 = private unnamed_addr constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private unnamed_addr constant [42 x i8] c"Enter rows and column for second matrix: \00", align 1
@.str.5 = private unnamed_addr constant [23 x i8] c"Enter elements a%d%d: \00", align 1
@.str.7 = private unnamed_addr constant [23 x i8] c"Enter elements b%d%d: \00", align 1
@.str.9 = private unnamed_addr constant [5 x i8] c"%d  \00", align 1
@str = private unnamed_addr constant [29 x i8] c"\0AEnter elements of matrix 1:\00"
@str.11 = private unnamed_addr constant [29 x i8] c"\0AEnter elements of matrix 2:\00"
@str.12 = private unnamed_addr constant [16 x i8] c"\0AOutput Matrix:\00"
@str.13 = private unnamed_addr constant [2 x i8] c"\0A\00"
@str.14 = private unnamed_addr constant [59 x i8] c"Error! column of first matrix not equal to row of second.\0A\00"

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %a = alloca [10 x [10 x i32]], align 16
  %b = alloca [10 x [10 x i32]], align 16
  %result = alloca [10 x [10 x i32]], align 16
  %r1 = alloca i32, align 4
  %c1 = alloca i32, align 4
  %r2 = alloca i32, align 4
  %c2 = alloca i32, align 4
  %0 = bitcast [10 x [10 x i32]]* %a to i8*
  call void @llvm.lifetime.start(i64 400, i8* %0) #1
  %1 = bitcast [10 x [10 x i32]]* %b to i8*
  call void @llvm.lifetime.start(i64 400, i8* %1) #1
  %2 = bitcast [10 x [10 x i32]]* %result to i8*
  call void @llvm.lifetime.start(i64 400, i8* %2) #1
  %3 = bitcast i32* %r1 to i8*
  call void @llvm.lifetime.start(i64 4, i8* %3) #1
  %4 = bitcast i32* %c1 to i8*
  call void @llvm.lifetime.start(i64 4, i8* %4) #1
  %5 = bitcast i32* %r2 to i8*
  call void @llvm.lifetime.start(i64 4, i8* %5) #1
  %6 = bitcast i32* %c2 to i8*
  call void @llvm.lifetime.start(i64 4, i8* %6) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %r1, i32* nonnull %c1) #1
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.2, i64 0, i64 0)) #1
  %call3 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %r2, i32* nonnull %c2) #1
  %7 = load i32, i32* %c1, align 4, !tbaa !1
  %8 = load i32, i32* %r2, align 4, !tbaa !1
  %cmp.206 = icmp eq i32 %7, %8
  br i1 %cmp.206, label %while.end, label %while.body.preheader

while.body.preheader:                             ; preds = %entry
  br label %while.body

while.body:                                       ; preds = %while.body.preheader, %while.body
  %puts160 = call i32 @puts(i8* getelementptr inbounds ([59 x i8], [59 x i8]* @str.14, i64 0, i64 0))
  %call5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str, i64 0, i64 0)) #1
  %call6 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %r1, i32* nonnull %c1) #1
  %call7 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.2, i64 0, i64 0)) #1
  %call8 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %r2, i32* nonnull %c2) #1
  %9 = load i32, i32* %c1, align 4, !tbaa !1
  %10 = load i32, i32* %r2, align 4, !tbaa !1
  %cmp = icmp eq i32 %9, %10
  br i1 %cmp, label %while.end.loopexit, label %while.body

while.end.loopexit:                               ; preds = %while.body
  br label %while.end

while.end:                                        ; preds = %while.end.loopexit, %entry
  %puts = call i32 @puts(i8* getelementptr inbounds ([29 x i8], [29 x i8]* @str, i64 0, i64 0))
  %11 = load i32, i32* %r1, align 4, !tbaa !1
  %cmp10.204 = icmp sgt i32 %11, 0
  br i1 %cmp10.204, label %for.cond.11.preheader.lr.ph, label %for.end.22

for.cond.11.preheader.lr.ph:                      ; preds = %while.end
  %.pre = load i32, i32* %c1, align 4, !tbaa !1
  br label %for.cond.11.preheader

for.cond.11.for.cond.loopexit_crit_edge:          ; preds = %for.body.13
  %.lcssa263 = phi i32 [ %21, %for.body.13 ]
  %.pre232 = load i32, i32* %r1, align 4, !tbaa !1
  br label %for.cond.loopexit

for.cond.loopexit:                                ; preds = %for.cond.11.for.cond.loopexit_crit_edge, %for.cond.11.preheader
  %12 = phi i32 [ %.pre232, %for.cond.11.for.cond.loopexit_crit_edge ], [ %15, %for.cond.11.preheader ]
  %13 = phi i32 [ %.lcssa263, %for.cond.11.for.cond.loopexit_crit_edge ], [ %16, %for.cond.11.preheader ]
  %14 = sext i32 %12 to i64
  %cmp10 = icmp slt i64 %indvars.iv.next218, %14
  br i1 %cmp10, label %for.cond.11.preheader, label %for.end.22.loopexit

for.cond.11.preheader:                            ; preds = %for.cond.11.preheader.lr.ph, %for.cond.loopexit
  %15 = phi i32 [ %11, %for.cond.11.preheader.lr.ph ], [ %12, %for.cond.loopexit ]
  %16 = phi i32 [ %.pre, %for.cond.11.preheader.lr.ph ], [ %13, %for.cond.loopexit ]
  %indvars.iv217 = phi i64 [ 0, %for.cond.11.preheader.lr.ph ], [ %indvars.iv.next218, %for.cond.loopexit ]
  %cmp12.201 = icmp sgt i32 %16, 0
  %indvars.iv.next218 = add nuw nsw i64 %indvars.iv217, 1
  br i1 %cmp12.201, label %for.body.13.preheader, label %for.cond.loopexit

for.body.13.preheader:                            ; preds = %for.cond.11.preheader
  %17 = trunc i64 %indvars.iv.next218 to i32
  br label %for.body.13

for.body.13:                                      ; preds = %for.body.13.preheader, %for.body.13
  %indvars.iv214 = phi i64 [ %indvars.iv.next215, %for.body.13 ], [ 0, %for.body.13.preheader ]
  %indvars.iv.next215 = add nuw nsw i64 %indvars.iv214, 1
  %18 = trunc i64 %indvars.iv.next215 to i32
  %call15 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.5, i64 0, i64 0), i32 %17, i32 %18) #1
  %19 = mul nsw i64 %indvars.iv.next215, %indvars.iv.next218
  %arrayidx19 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %a, i64 0, i64 %indvars.iv217, i64 %indvars.iv214
  %20 = trunc i64 %19 to i32
  store i32 %20, i32* %arrayidx19, align 4, !tbaa !1
  %21 = load i32, i32* %c1, align 4, !tbaa !1
  %22 = sext i32 %21 to i64
  %cmp12 = icmp slt i64 %indvars.iv.next215, %22
  br i1 %cmp12, label %for.body.13, label %for.cond.11.for.cond.loopexit_crit_edge

for.end.22.loopexit:                              ; preds = %for.cond.loopexit
  br label %for.end.22

for.end.22:                                       ; preds = %for.end.22.loopexit, %while.end
  %puts157 = call i32 @puts(i8* getelementptr inbounds ([29 x i8], [29 x i8]* @str.11, i64 0, i64 0))
  %23 = load i32, i32* %r2, align 4, !tbaa !1
  %cmp25.199 = icmp sgt i32 %23, 0
  br i1 %cmp25.199, label %for.cond.27.preheader.lr.ph, label %for.cond.46.preheader

for.cond.27.preheader.lr.ph:                      ; preds = %for.end.22
  %.pre233 = load i32, i32* %c2, align 4, !tbaa !1
  br label %for.cond.27.preheader

for.cond.27.for.cond.24.loopexit_crit_edge:       ; preds = %for.body.29
  %.lcssa262 = phi i32 [ %41, %for.body.29 ]
  %.pre234 = load i32, i32* %r2, align 4, !tbaa !1
  br label %for.cond.24.loopexit

for.cond.24.loopexit:                             ; preds = %for.cond.27.for.cond.24.loopexit_crit_edge, %for.cond.27.preheader
  %24 = phi i32 [ %.pre234, %for.cond.27.for.cond.24.loopexit_crit_edge ], [ %27, %for.cond.27.preheader ]
  %25 = phi i32 [ %.lcssa262, %for.cond.27.for.cond.24.loopexit_crit_edge ], [ %28, %for.cond.27.preheader ]
  %26 = sext i32 %24 to i64
  %cmp25 = icmp slt i64 %indvars.iv.next213, %26
  br i1 %cmp25, label %for.cond.27.preheader, label %for.cond.46.preheader.loopexit

for.cond.27.preheader:                            ; preds = %for.cond.27.preheader.lr.ph, %for.cond.24.loopexit
  %27 = phi i32 [ %23, %for.cond.27.preheader.lr.ph ], [ %24, %for.cond.24.loopexit ]
  %28 = phi i32 [ %.pre233, %for.cond.27.preheader.lr.ph ], [ %25, %for.cond.24.loopexit ]
  %indvars.iv212 = phi i64 [ 0, %for.cond.27.preheader.lr.ph ], [ %indvars.iv.next213, %for.cond.24.loopexit ]
  %cmp28.197 = icmp sgt i32 %28, 0
  %indvars.iv.next213 = add nuw nsw i64 %indvars.iv212, 1
  br i1 %cmp28.197, label %for.body.29.preheader, label %for.cond.24.loopexit

for.body.29.preheader:                            ; preds = %for.cond.27.preheader
  %29 = trunc i64 %indvars.iv.next213 to i32
  br label %for.body.29

for.cond.46.preheader.loopexit:                   ; preds = %for.cond.24.loopexit
  br label %for.cond.46.preheader

for.cond.46.preheader:                            ; preds = %for.cond.46.preheader.loopexit, %for.end.22
  %30 = load i32, i32* %r1, align 4, !tbaa !1
  %cmp47.195 = icmp sgt i32 %30, 0
  br i1 %cmp47.195, label %for.cond.49.preheader.lr.ph, label %for.end.93

for.cond.49.preheader.lr.ph:                      ; preds = %for.cond.46.preheader
  %31 = load i32, i32* %c2, align 4, !tbaa !1
  %cmp50.193 = icmp sgt i32 %31, 0
  br i1 %cmp50.193, label %for.cond.49.preheader.lr.ph.split.us, label %for.end.93

for.cond.49.preheader.lr.ph.split.us:             ; preds = %for.cond.49.preheader.lr.ph
  %32 = add i32 %31, -1
  %33 = zext i32 %32 to i64
  %34 = shl nuw nsw i64 %33, 2
  %35 = add nuw nsw i64 %34, 4
  %36 = add i32 %30, -1
  %xtraiter = and i32 %30, 3
  %lcmp.mod = icmp eq i32 %xtraiter, 0
  br i1 %lcmp.mod, label %for.cond.49.preheader.lr.ph.split.us.split, label %for.inc.59.us.prol.preheader

for.inc.59.us.prol.preheader:                     ; preds = %for.cond.49.preheader.lr.ph.split.us
  br label %for.inc.59.us.prol

for.inc.59.us.prol:                               ; preds = %for.inc.59.us.prol.preheader, %for.inc.59.us.prol
  %indvar.prol = phi i64 [ %indvar.next.prol, %for.inc.59.us.prol ], [ 0, %for.inc.59.us.prol.preheader ]
  %i.2196.us.prol = phi i32 [ %inc60.us.prol, %for.inc.59.us.prol ], [ 0, %for.inc.59.us.prol.preheader ]
  %prol.iter = phi i32 [ %prol.iter.sub, %for.inc.59.us.prol ], [ %xtraiter, %for.inc.59.us.prol.preheader ]
  %scevgep.prol = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %result, i64 0, i64 %indvar.prol, i64 0
  %scevgep231.prol = bitcast i32* %scevgep.prol to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep231.prol, i8 0, i64 %35, i32 8, i1 false)
  %inc60.us.prol = add nuw nsw i32 %i.2196.us.prol, 1
  %indvar.next.prol = add nuw nsw i64 %indvar.prol, 1
  %prol.iter.sub = add i32 %prol.iter, -1
  %prol.iter.cmp = icmp eq i32 %prol.iter.sub, 0
  br i1 %prol.iter.cmp, label %for.cond.49.preheader.lr.ph.split.us.split.loopexit, label %for.inc.59.us.prol, !llvm.loop !5

for.cond.49.preheader.lr.ph.split.us.split.loopexit: ; preds = %for.inc.59.us.prol
  %indvar.next.prol.lcssa = phi i64 [ %indvar.next.prol, %for.inc.59.us.prol ]
  %inc60.us.prol.lcssa = phi i32 [ %inc60.us.prol, %for.inc.59.us.prol ]
  br label %for.cond.49.preheader.lr.ph.split.us.split

for.cond.49.preheader.lr.ph.split.us.split:       ; preds = %for.cond.49.preheader.lr.ph.split.us.split.loopexit, %for.cond.49.preheader.lr.ph.split.us
  %indvar.unr = phi i64 [ 0, %for.cond.49.preheader.lr.ph.split.us ], [ %indvar.next.prol.lcssa, %for.cond.49.preheader.lr.ph.split.us.split.loopexit ]
  %i.2196.us.unr = phi i32 [ 0, %for.cond.49.preheader.lr.ph.split.us ], [ %inc60.us.prol.lcssa, %for.cond.49.preheader.lr.ph.split.us.split.loopexit ]
  %37 = icmp ult i32 %36, 3
  br i1 %37, label %for.cond.62.preheader, label %for.cond.49.preheader.lr.ph.split.us.split.split

for.cond.49.preheader.lr.ph.split.us.split.split: ; preds = %for.cond.49.preheader.lr.ph.split.us.split
  br label %for.inc.59.us

for.inc.59.us:                                    ; preds = %for.inc.59.us, %for.cond.49.preheader.lr.ph.split.us.split.split
  %indvar = phi i64 [ %indvar.unr, %for.cond.49.preheader.lr.ph.split.us.split.split ], [ %indvar.next.3, %for.inc.59.us ]
  %i.2196.us = phi i32 [ %i.2196.us.unr, %for.cond.49.preheader.lr.ph.split.us.split.split ], [ %inc60.us.3, %for.inc.59.us ]
  %scevgep = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %result, i64 0, i64 %indvar, i64 0
  %scevgep231 = bitcast i32* %scevgep to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep231, i8 0, i64 %35, i32 8, i1 false)
  %indvar.next = add nuw nsw i64 %indvar, 1
  %scevgep.1 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %result, i64 0, i64 %indvar.next, i64 0
  %scevgep231.1 = bitcast i32* %scevgep.1 to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep231.1, i8 0, i64 %35, i32 8, i1 false)
  %indvar.next.1 = add nsw i64 %indvar, 2
  %scevgep.2 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %result, i64 0, i64 %indvar.next.1, i64 0
  %scevgep231.2 = bitcast i32* %scevgep.2 to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep231.2, i8 0, i64 %35, i32 8, i1 false)
  %indvar.next.2 = add nsw i64 %indvar, 3
  %scevgep.3 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %result, i64 0, i64 %indvar.next.2, i64 0
  %scevgep231.3 = bitcast i32* %scevgep.3 to i8*
  call void @llvm.memset.p0i8.i64(i8* %scevgep231.3, i8 0, i64 %35, i32 8, i1 false)
  %inc60.us.3 = add nsw i32 %i.2196.us, 4
  %cmp47.us.3 = icmp slt i32 %inc60.us.3, %30
  %indvar.next.3 = add nsw i64 %indvar, 4
  br i1 %cmp47.us.3, label %for.inc.59.us, label %for.cond.62.preheader.unr-lcssa

for.body.29:                                      ; preds = %for.body.29.preheader, %for.body.29
  %indvars.iv209 = phi i64 [ %indvars.iv.next210, %for.body.29 ], [ 0, %for.body.29.preheader ]
  %indvars.iv.next210 = add nuw nsw i64 %indvars.iv209, 1
  %38 = trunc i64 %indvars.iv.next210 to i32
  %call32 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.7, i64 0, i64 0), i32 %29, i32 %38) #1
  %39 = mul nsw i64 %indvars.iv.next210, %indvars.iv.next213
  %arrayidx39 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %b, i64 0, i64 %indvars.iv212, i64 %indvars.iv209
  %40 = trunc i64 %39 to i32
  store i32 %40, i32* %arrayidx39, align 4, !tbaa !1
  %41 = load i32, i32* %c2, align 4, !tbaa !1
  %42 = sext i32 %41 to i64
  %cmp28 = icmp slt i64 %indvars.iv.next210, %42
  br i1 %cmp28, label %for.body.29, label %for.cond.27.for.cond.24.loopexit_crit_edge

for.cond.62.preheader.unr-lcssa:                  ; preds = %for.inc.59.us
  br label %for.cond.62.preheader

for.cond.62.preheader:                            ; preds = %for.cond.49.preheader.lr.ph.split.us.split, %for.cond.62.preheader.unr-lcssa
  br i1 %cmp47.195, label %for.cond.65.preheader.lr.ph, label %for.end.93

for.cond.65.preheader.lr.ph:                      ; preds = %for.cond.62.preheader
  %.pr = load i32, i32* %c2, align 4, !tbaa !1
  %cmp66.167 = icmp sgt i32 %.pr, 0
  %43 = load i32, i32* %c1, align 4, !tbaa !1
  %cmp69.165 = icmp sgt i32 %43, 0
  br i1 %cmp66.167, label %for.cond.65.preheader.lr.ph.split.us, label %for.end.93

for.cond.65.preheader.lr.ph.split.us:             ; preds = %for.cond.65.preheader.lr.ph
  %44 = sext i32 %30 to i64
  %45 = add i32 %43, -1
  %46 = zext i32 %45 to i64
  %47 = add nuw nsw i64 %46, 1
  %end.idx = add nuw nsw i64 %46, 1
  %n.vec = and i64 %47, 8589934584
  %cmp.zero = icmp eq i64 %n.vec, 0
  br label %for.cond.68.preheader.lr.ph.us

for.inc.91.us.loopexit:                           ; preds = %for.inc.88.us.us
  br label %for.inc.91.us

for.inc.91.us:                                    ; preds = %for.inc.91.us.loopexit, %for.cond.68.preheader.lr.ph.us
  %indvars.iv.next226 = add nuw nsw i64 %indvars.iv225, 1
  %cmp63.us = icmp slt i64 %indvars.iv.next226, %44
  br i1 %cmp63.us, label %for.cond.68.preheader.lr.ph.us, label %for.end.93.loopexit

for.cond.68.preheader.lr.ph.us:                   ; preds = %for.cond.65.preheader.lr.ph.split.us, %for.inc.91.us
  %indvars.iv225 = phi i64 [ %indvars.iv.next226, %for.inc.91.us ], [ 0, %for.cond.65.preheader.lr.ph.split.us ]
  br i1 %cmp69.165, label %for.body.70.lr.ph.us.us.preheader, label %for.inc.91.us

for.body.70.lr.ph.us.us.preheader:                ; preds = %for.cond.68.preheader.lr.ph.us
  br label %for.body.70.lr.ph.us.us

for.inc.88.us.us.loopexit:                        ; preds = %for.body.70.us.us
  %add84.us.us.lcssa261 = phi i32 [ %add84.us.us, %for.body.70.us.us ]
  br label %for.inc.88.us.us

for.inc.88.us.us:                                 ; preds = %for.inc.88.us.us.loopexit, %middle.block
  %add84.us.us.lcssa = phi i32 [ %89, %middle.block ], [ %add84.us.us.lcssa261, %for.inc.88.us.us.loopexit ]
  store i32 %add84.us.us.lcssa, i32* %arrayidx83.us.us, align 4, !tbaa !1
  %indvars.iv.next222 = add nuw nsw i64 %indvars.iv221, 1
  %lftr.wideiv223 = trunc i64 %indvars.iv.next222 to i32
  %exitcond224 = icmp eq i32 %lftr.wideiv223, %.pr
  br i1 %exitcond224, label %for.inc.91.us.loopexit, label %for.body.70.lr.ph.us.us

for.body.70.lr.ph.us.us:                          ; preds = %for.body.70.lr.ph.us.us.preheader, %for.inc.88.us.us
  %indvars.iv221 = phi i64 [ %indvars.iv.next222, %for.inc.88.us.us ], [ 0, %for.body.70.lr.ph.us.us.preheader ]
  %arrayidx83.us.us = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %result, i64 0, i64 %indvars.iv225, i64 %indvars.iv221
  %arrayidx83.promoted.us.us = load i32, i32* %arrayidx83.us.us, align 4, !tbaa !1
  %48 = insertelement <4 x i32> <i32 undef, i32 0, i32 0, i32 0>, i32 %arrayidx83.promoted.us.us, i32 0
  br i1 %cmp.zero, label %middle.block, label %vector.body.preheader

vector.body.preheader:                            ; preds = %for.body.70.lr.ph.us.us
  br label %vector.body

vector.body:                                      ; preds = %vector.body.preheader, %vector.body
  %index = phi i64 [ %index.next, %vector.body ], [ 0, %vector.body.preheader ]
  %vec.phi = phi <4 x i32> [ %86, %vector.body ], [ %48, %vector.body.preheader ]
  %vec.phi246 = phi <4 x i32> [ %87, %vector.body ], [ zeroinitializer, %vector.body.preheader ]
  %broadcast.splatinsert = insertelement <4 x i64> undef, i64 %index, i32 0
  %broadcast.splat = shufflevector <4 x i64> %broadcast.splatinsert, <4 x i64> undef, <4 x i32> zeroinitializer
  %induction = add <4 x i64> %broadcast.splat, <i64 0, i64 1, i64 2, i64 3>
  %induction245 = add <4 x i64> %broadcast.splat, <i64 4, i64 5, i64 6, i64 7>
  %49 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %a, i64 0, i64 %indvars.iv225, i64 %index
  %50 = extractelement <4 x i64> %induction, i32 1
  %51 = extractelement <4 x i64> %induction, i32 2
  %52 = extractelement <4 x i64> %induction, i32 3
  %53 = extractelement <4 x i64> %induction245, i32 0
  %54 = extractelement <4 x i64> %induction245, i32 1
  %55 = extractelement <4 x i64> %induction245, i32 2
  %56 = extractelement <4 x i64> %induction245, i32 3
  %57 = bitcast i32* %49 to <4 x i32>*
  %wide.load = load <4 x i32>, <4 x i32>* %57, align 8, !tbaa !1
  %58 = getelementptr i32, i32* %49, i64 4
  %59 = bitcast i32* %58 to <4 x i32>*
  %wide.load247 = load <4 x i32>, <4 x i32>* %59, align 8, !tbaa !1
  %60 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %b, i64 0, i64 %index, i64 %indvars.iv221
  %61 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %b, i64 0, i64 %50, i64 %indvars.iv221
  %62 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %b, i64 0, i64 %51, i64 %indvars.iv221
  %63 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %b, i64 0, i64 %52, i64 %indvars.iv221
  %64 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %b, i64 0, i64 %53, i64 %indvars.iv221
  %65 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %b, i64 0, i64 %54, i64 %indvars.iv221
  %66 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %b, i64 0, i64 %55, i64 %indvars.iv221
  %67 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %b, i64 0, i64 %56, i64 %indvars.iv221
  %68 = load i32, i32* %60, align 4, !tbaa !1
  %69 = insertelement <4 x i32> undef, i32 %68, i32 0
  %70 = load i32, i32* %61, align 4, !tbaa !1
  %71 = insertelement <4 x i32> %69, i32 %70, i32 1
  %72 = load i32, i32* %62, align 4, !tbaa !1
  %73 = insertelement <4 x i32> %71, i32 %72, i32 2
  %74 = load i32, i32* %63, align 4, !tbaa !1
  %75 = insertelement <4 x i32> %73, i32 %74, i32 3
  %76 = load i32, i32* %64, align 4, !tbaa !1
  %77 = insertelement <4 x i32> undef, i32 %76, i32 0
  %78 = load i32, i32* %65, align 4, !tbaa !1
  %79 = insertelement <4 x i32> %77, i32 %78, i32 1
  %80 = load i32, i32* %66, align 4, !tbaa !1
  %81 = insertelement <4 x i32> %79, i32 %80, i32 2
  %82 = load i32, i32* %67, align 4, !tbaa !1
  %83 = insertelement <4 x i32> %81, i32 %82, i32 3
  %84 = mul nsw <4 x i32> %75, %wide.load
  %85 = mul nsw <4 x i32> %83, %wide.load247
  %86 = add nsw <4 x i32> %vec.phi, %84
  %87 = add nsw <4 x i32> %vec.phi246, %85
  %index.next = add i64 %index, 8
  %88 = icmp eq i64 %index.next, %n.vec
  br i1 %88, label %middle.block.loopexit, label %vector.body, !llvm.loop !7

middle.block.loopexit:                            ; preds = %vector.body
  %.lcssa260 = phi <4 x i32> [ %87, %vector.body ]
  %.lcssa259 = phi <4 x i32> [ %86, %vector.body ]
  br label %middle.block

middle.block:                                     ; preds = %middle.block.loopexit, %for.body.70.lr.ph.us.us
  %resume.val = phi i64 [ 0, %for.body.70.lr.ph.us.us ], [ %n.vec, %middle.block.loopexit ]
  %rdx.vec.exit.phi = phi <4 x i32> [ %48, %for.body.70.lr.ph.us.us ], [ %.lcssa259, %middle.block.loopexit ]
  %rdx.vec.exit.phi250 = phi <4 x i32> [ zeroinitializer, %for.body.70.lr.ph.us.us ], [ %.lcssa260, %middle.block.loopexit ]
  %bin.rdx = add <4 x i32> %rdx.vec.exit.phi250, %rdx.vec.exit.phi
  %rdx.shuf = shufflevector <4 x i32> %bin.rdx, <4 x i32> undef, <4 x i32> <i32 2, i32 3, i32 undef, i32 undef>
  %bin.rdx251 = add <4 x i32> %bin.rdx, %rdx.shuf
  %rdx.shuf252 = shufflevector <4 x i32> %bin.rdx251, <4 x i32> undef, <4 x i32> <i32 1, i32 undef, i32 undef, i32 undef>
  %bin.rdx253 = add <4 x i32> %bin.rdx251, %rdx.shuf252
  %89 = extractelement <4 x i32> %bin.rdx253, i32 0
  %cmp.n = icmp eq i64 %end.idx, %resume.val
  br i1 %cmp.n, label %for.inc.88.us.us, label %for.body.70.us.us.preheader

for.body.70.us.us.preheader:                      ; preds = %middle.block
  br label %for.body.70.us.us

for.body.70.us.us:                                ; preds = %for.body.70.us.us.preheader, %for.body.70.us.us
  %indvars.iv219 = phi i64 [ %indvars.iv.next220, %for.body.70.us.us ], [ %resume.val, %for.body.70.us.us.preheader ]
  %90 = phi i32 [ %add84.us.us, %for.body.70.us.us ], [ %89, %for.body.70.us.us.preheader ]
  %arrayidx74.us.us = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %a, i64 0, i64 %indvars.iv225, i64 %indvars.iv219
  %91 = load i32, i32* %arrayidx74.us.us, align 4, !tbaa !1
  %arrayidx78.us.us = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %b, i64 0, i64 %indvars.iv219, i64 %indvars.iv221
  %92 = load i32, i32* %arrayidx78.us.us, align 4, !tbaa !1
  %mul79.us.us = mul nsw i32 %92, %91
  %add84.us.us = add nsw i32 %90, %mul79.us.us
  %indvars.iv.next220 = add nuw nsw i64 %indvars.iv219, 1
  %lftr.wideiv = trunc i64 %indvars.iv.next220 to i32
  %exitcond = icmp eq i32 %lftr.wideiv, %43
  br i1 %exitcond, label %for.inc.88.us.us.loopexit, label %for.body.70.us.us, !llvm.loop !10

for.end.93.loopexit:                              ; preds = %for.inc.91.us
  br label %for.end.93

for.end.93:                                       ; preds = %for.end.93.loopexit, %for.cond.49.preheader.lr.ph, %for.cond.46.preheader, %for.cond.65.preheader.lr.ph, %for.cond.62.preheader
  %puts158 = call i32 @puts(i8* getelementptr inbounds ([16 x i8], [16 x i8]* @str.12, i64 0, i64 0))
  %93 = load i32, i32* %r1, align 4, !tbaa !1
  %cmp96.163 = icmp sgt i32 %93, 0
  br i1 %cmp96.163, label %for.cond.98.preheader.lr.ph, label %for.end.113

for.cond.98.preheader.lr.ph:                      ; preds = %for.end.93
  %.pre235 = load i32, i32* %c2, align 4, !tbaa !1
  br label %for.cond.98.preheader

for.cond.98.preheader:                            ; preds = %for.cond.98.preheader.lr.ph, %for.inc.111
  %94 = phi i32 [ %93, %for.cond.98.preheader.lr.ph ], [ %101, %for.inc.111 ]
  %95 = phi i32 [ %.pre235, %for.cond.98.preheader.lr.ph ], [ %102, %for.inc.111 ]
  %indvars.iv207 = phi i64 [ 0, %for.cond.98.preheader.lr.ph ], [ %indvars.iv.next208, %for.inc.111 ]
  %cmp99.161 = icmp sgt i32 %95, 0
  br i1 %cmp99.161, label %for.body.100.preheader, label %for.inc.111

for.body.100.preheader:                           ; preds = %for.cond.98.preheader
  br label %for.body.100

for.body.100:                                     ; preds = %for.body.100.preheader, %for.inc.108
  %indvars.iv = phi i64 [ %indvars.iv.next, %for.inc.108 ], [ 0, %for.body.100.preheader ]
  %arrayidx104 = getelementptr inbounds [10 x [10 x i32]], [10 x [10 x i32]]* %result, i64 0, i64 %indvars.iv207, i64 %indvars.iv
  %96 = load i32, i32* %arrayidx104, align 4, !tbaa !1
  %call105 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.9, i64 0, i64 0), i32 %96) #1
  %97 = load i32, i32* %c2, align 4, !tbaa !1
  %sub = add nsw i32 %97, -1
  %98 = trunc i64 %indvars.iv to i32
  %cmp106 = icmp eq i32 %98, %sub
  br i1 %cmp106, label %if.then, label %for.inc.108

if.then:                                          ; preds = %for.body.100
  %puts159 = call i32 @puts(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @str.13, i64 0, i64 0))
  %.pre236 = load i32, i32* %c2, align 4, !tbaa !1
  br label %for.inc.108

for.inc.108:                                      ; preds = %for.body.100, %if.then
  %99 = phi i32 [ %97, %for.body.100 ], [ %.pre236, %if.then ]
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %100 = sext i32 %99 to i64
  %cmp99 = icmp slt i64 %indvars.iv.next, %100
  br i1 %cmp99, label %for.body.100, label %for.cond.98.for.inc.111_crit_edge

for.cond.98.for.inc.111_crit_edge:                ; preds = %for.inc.108
  %.lcssa = phi i32 [ %99, %for.inc.108 ]
  %.pre237 = load i32, i32* %r1, align 4, !tbaa !1
  br label %for.inc.111

for.inc.111:                                      ; preds = %for.cond.98.for.inc.111_crit_edge, %for.cond.98.preheader
  %101 = phi i32 [ %.pre237, %for.cond.98.for.inc.111_crit_edge ], [ %94, %for.cond.98.preheader ]
  %102 = phi i32 [ %.lcssa, %for.cond.98.for.inc.111_crit_edge ], [ %95, %for.cond.98.preheader ]
  %indvars.iv.next208 = add nuw nsw i64 %indvars.iv207, 1
  %103 = sext i32 %101 to i64
  %cmp96 = icmp slt i64 %indvars.iv.next208, %103
  br i1 %cmp96, label %for.cond.98.preheader, label %for.end.113.loopexit

for.end.113.loopexit:                             ; preds = %for.inc.111
  br label %for.end.113

for.end.113:                                      ; preds = %for.end.113.loopexit, %for.end.93
  call void @llvm.lifetime.end(i64 4, i8* %6) #1
  call void @llvm.lifetime.end(i64 4, i8* %5) #1
  call void @llvm.lifetime.end(i64 4, i8* %4) #1
  call void @llvm.lifetime.end(i64 4, i8* %3) #1
  call void @llvm.lifetime.end(i64 400, i8* %2) #1
  call void @llvm.lifetime.end(i64 400, i8* %1) #1
  call void @llvm.lifetime.end(i64 400, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @puts(i8* nocapture readonly) #1

; Function Attrs: nounwind
declare void @llvm.memset.p0i8.i64(i8* nocapture, i8, i64, i32, i1) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = distinct !{!5, !6}
!6 = !{!"llvm.loop.unroll.disable"}
!7 = distinct !{!7, !8, !9}
!8 = !{!"llvm.loop.vectorize.width", i32 1}
!9 = !{!"llvm.loop.interleave.count", i32 1}
!10 = distinct !{!10, !11, !8, !9}
!11 = !{!"llvm.loop.unroll.runtime.disable"}
