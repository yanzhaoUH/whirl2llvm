; ModuleID = 'test12_O.bc'

@.str = private constant [32 x i8] c"Enter coefficients a, b and c: \00", align 1
@.str.1 = private constant [12 x i8] c"%lf %lf %lf\00", align 1
@.str.2 = private constant [32 x i8] c"root1 = %.2lf and root2 = %.2lf\00", align 1
@.str.3 = private constant [23 x i8] c"root1 = root2 = %.2lf;\00", align 1
@.str.4 = private constant [44 x i8] c"root1 = %.2lf+%.2lfi and root2 = %.2f-%.2fi\00", align 1
@_LIB_VERSION_63 = common global i32 0, align 4
@signgam_64 = common global i32 0, align 4

define i32 @main() {
entry:
  %.preg_F8_11_57 = alloca double
  %.preg_F8_11_70 = alloca double
  %.preg_F8_11_60 = alloca double
  %.preg_F8_11_59 = alloca double
  %.preg_F8_11_61 = alloca double
  %.preg_F8_11_66 = alloca double
  %.preg_F8_11_54 = alloca double
  %.preg_F8_11_65 = alloca double
  %.preg_F8_11_52 = alloca double
  %.preg_F8_11_75 = alloca double
  %.preg_F8_11_64 = alloca double
  %.preg_F8_11_53 = alloca double
  %.preg_F8_11_74 = alloca double
  %.preg_F8_11_62 = alloca double
  %.preg_F8_11_72 = alloca double
  %.preg_F8_11_73 = alloca double
  %_temp___save_sqrt3_15 = alloca double, align 8
  %_temp___save_sqrt5_17 = alloca double, align 8
  %_temp___save_sqrt9_21 = alloca double, align 8
  %_temp___sqrt_arg2_14 = alloca double, align 8
  %_temp___sqrt_arg4_16 = alloca double, align 8
  %_temp___sqrt_arg8_20 = alloca double, align 8
  %_temp_dummy10_12 = alloca i32, align 4
  %_temp_dummy110_22 = alloca i32, align 4
  %_temp_dummy11_13 = alloca i32, align 4
  %_temp_dummy16_18 = alloca i32, align 4
  %_temp_dummy17_19 = alloca i32, align 4
  %_temp_ehpit0_23 = alloca i32, align 4
  %a_4 = alloca double, align 8
  %b_5 = alloca double, align 8
  %c_6 = alloca double, align 8
  %determinant_7 = alloca double, align 8
  %imaginaryPart_11 = alloca double, align 8
  %old_frame_pointer_28.addr = alloca i64, align 8
  %realPart_10 = alloca double, align 8
  %return_address_29.addr = alloca i64, align 8
  %root1_8 = alloca double, align 8
  %root2_9 = alloca double, align 8
  %0 = bitcast i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast double* %a_4 to double*
  %3 = bitcast double* %b_5 to double*
  %4 = bitcast double* %c_6 to double*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, double* %2, double* %3, double* %4)
  %5 = load double, double* %b_5, align 8
  store double %5, double* %.preg_F8_11_73, align 8
  %6 = load double, double* %a_4, align 8
  store double %6, double* %.preg_F8_11_72, align 8
  %7 = load double, double* %.preg_F8_11_73
  %8 = load double, double* %.preg_F8_11_73
  %mul = fmul double %7, %8
  %9 = load double, double* %c_6, align 8
  %10 = load double, double* %.preg_F8_11_72
  %mul.1 = fmul double %10, -4.000000e+00
  %mul.2 = fmul double %9, %mul.1
  %add = fadd double %mul, %mul.2
  store double %add, double* %.preg_F8_11_62, align 8
  store double 0.000000e+00, double* %.preg_F8_11_74, align 8
  %11 = load double, double* %.preg_F8_11_62
  %12 = load double, double* %.preg_F8_11_74
  %cmp1 = fcmp ogt double %11, %12
  br i1 %cmp1, label %tb, label %L5378

tb:                                               ; preds = %entry
  %13 = load double, double* %.preg_F8_11_72
  %call7 = call double @sqrt(double %13)
  store double %call7, double* %.preg_F8_11_53, align 8
  %14 = load double, double* %.preg_F8_11_53
  store double %14, double* %.preg_F8_11_64, align 8
  %15 = load double, double* %.preg_F8_11_53
  %16 = load double, double* %.preg_F8_11_53
  %cmp2 = fcmp one double %15, %16
  br i1 %cmp2, label %tb3, label %L6658

L5378:                                            ; preds = %entry
  %17 = load double, double* %.preg_F8_11_73
  %18 = load double, double* %.preg_F8_11_72
  %mul.11 = fmul double %18, -2.000000e+00
  %SDiv.12 = fdiv double %17, %mul.11
  store double %SDiv.12, double* %.preg_F8_11_61, align 8
  %19 = load double, double* %.preg_F8_11_62
  %20 = load double, double* %.preg_F8_11_74
  %cmp4 = fcmp oeq double %19, %20
  br i1 %cmp4, label %tb13, label %L6146

tb3:                                              ; preds = %tb
  %21 = load double, double* %.preg_F8_11_72
  %call3 = call double @sqrt(double %21)
  store double %call3, double* %.preg_F8_11_64, align 8
  %22 = load double, double* %a_4, align 8
  store double %22, double* %.preg_F8_11_72, align 8
  %23 = load double, double* %b_5, align 8
  store double %23, double* %.preg_F8_11_73, align 8
  br label %L6658

L6658:                                            ; preds = %tb3, %tb
  store double 2.000000e+00, double* %.preg_F8_11_75, align 8
  %24 = load double, double* %.preg_F8_11_72
  %25 = load double, double* %.preg_F8_11_75
  %mul.4 = fmul double %24, %25
  store double %mul.4, double* %.preg_F8_11_52, align 8
  %26 = load double, double* %.preg_F8_11_64
  %27 = load double, double* %.preg_F8_11_73
  %add.5 = fsub double %26, %27
  %28 = load double, double* %.preg_F8_11_52
  %SDiv = fdiv double %add.5, %28
  store double %SDiv, double* %.preg_F8_11_65, align 8
  %29 = load double, double* %.preg_F8_11_62
  %call76 = call double @sqrt(double %29)
  store double %call76, double* %.preg_F8_11_54, align 8
  %30 = load double, double* %.preg_F8_11_54
  store double %30, double* %.preg_F8_11_66, align 8
  %31 = load double, double* %.preg_F8_11_54
  %32 = load double, double* %.preg_F8_11_54
  %cmp3 = fcmp one double %31, %32
  br i1 %cmp3, label %tb7, label %L6914

tb7:                                              ; preds = %L6658
  %33 = load double, double* %.preg_F8_11_62
  %call4 = call double @sqrt(double %33)
  store double %call4, double* %.preg_F8_11_66, align 8
  %34 = load double, double* %a_4, align 8
  %35 = load double, double* %.preg_F8_11_75
  %mul.8 = fmul double %34, %35
  store double %mul.8, double* %.preg_F8_11_52, align 8
  %36 = load double, double* %b_5, align 8
  store double %36, double* %.preg_F8_11_73, align 8
  br label %L6914

L6914:                                            ; preds = %tb7, %L6658
  %37 = bitcast i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.2, i32 0, i32 0) to i8*
  %38 = load double, double* %.preg_F8_11_65
  %39 = load double, double* %.preg_F8_11_66
  %40 = load double, double* %.preg_F8_11_73
  %add.9 = fadd double %39, %40
  %sub = fsub double -0.000000e+00, %add.9
  %41 = load double, double* %.preg_F8_11_52
  %SDiv.10 = fdiv double %sub, %41
  %call5 = call i32 (i8*, ...) @printf(i8* %37, double %38, double %SDiv.10)
  br label %L7170

L7170:                                            ; preds = %L7426, %L6914
  ret i32 0

tb13:                                             ; preds = %L5378
  %42 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.3, i32 0, i32 0) to i8*
  %43 = load double, double* %.preg_F8_11_61
  %call6 = call i32 (i8*, ...) @printf(i8* %42, double %43)
  br label %L7426

L6146:                                            ; preds = %L5378
  %44 = load double, double* %.preg_F8_11_62
  %sub.14 = fsub double -0.000000e+00, %44
  store double %sub.14, double* %.preg_F8_11_59, align 8
  %45 = load double, double* %.preg_F8_11_59
  %call715 = call double @sqrt(double %45)
  store double %call715, double* %.preg_F8_11_60, align 8
  %46 = load double, double* %.preg_F8_11_60
  store double %46, double* %.preg_F8_11_70, align 8
  %47 = load double, double* %.preg_F8_11_60
  %48 = load double, double* %.preg_F8_11_60
  %cmp5 = fcmp one double %47, %48
  br i1 %cmp5, label %tb16, label %L7682

L7426:                                            ; preds = %L7682, %tb13
  br label %L7170

tb16:                                             ; preds = %L6146
  %49 = load double, double* %.preg_F8_11_59
  %call717 = call double @sqrt(double %49)
  store double %call717, double* %.preg_F8_11_70, align 8
  %50 = load double, double* %a_4, align 8
  store double %50, double* %.preg_F8_11_72, align 8
  br label %L7682

L7682:                                            ; preds = %tb16, %L6146
  %51 = load double, double* %.preg_F8_11_70
  %52 = load double, double* %.preg_F8_11_72
  %mul.18 = fmul double %52, 2.000000e+00
  %SDiv.19 = fdiv double %51, %mul.18
  store double %SDiv.19, double* %.preg_F8_11_57, align 8
  %53 = bitcast i8* getelementptr inbounds ([44 x i8], [44 x i8]* @.str.4, i32 0, i32 0) to i8*
  %54 = load double, double* %.preg_F8_11_61
  %55 = load double, double* %.preg_F8_11_57
  %56 = load double, double* %.preg_F8_11_61
  %57 = load double, double* %.preg_F8_11_57
  %call8 = call i32 (i8*, ...) @printf(i8* %53, double %54, double %55, double %56, double %57)
  br label %L7426
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

declare double @sqrt(double)
