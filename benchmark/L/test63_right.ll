; ModuleID = 'test63.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.Distance = type { i32, float }

@.str = private unnamed_addr constant [36 x i8] c"Enter information for 1st distance\0A\00", align 1
@.str.1 = private unnamed_addr constant [13 x i8] c"Enter feet: \00", align 1
@.str.2 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@d1 = common global %struct.Distance zeroinitializer, align 4
@.str.3 = private unnamed_addr constant [13 x i8] c"Enter inch: \00", align 1
@.str.4 = private unnamed_addr constant [3 x i8] c"%f\00", align 1
@.str.5 = private unnamed_addr constant [37 x i8] c"\0AEnter information for 2nd distance\0A\00", align 1
@d2 = common global %struct.Distance zeroinitializer, align 4
@sumOfDistances = common global %struct.Distance zeroinitializer, align 4
@.str.6 = private unnamed_addr constant [30 x i8] c"\0ASum of distances = %d'-%.1f\22\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str, i32 0, i32 0))
  %call1 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.1, i32 0, i32 0))
  %call2 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0), i32* getelementptr inbounds (%struct.Distance, %struct.Distance* @d1, i32 0, i32 0))
  %call3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.3, i32 0, i32 0))
  %call4 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.4, i32 0, i32 0), float* getelementptr inbounds (%struct.Distance, %struct.Distance* @d1, i32 0, i32 1))
  %call5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.5, i32 0, i32 0))
  %call6 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.1, i32 0, i32 0))
  %call7 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0), i32* getelementptr inbounds (%struct.Distance, %struct.Distance* @d2, i32 0, i32 0))
  %call8 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.3, i32 0, i32 0))
  %call9 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.4, i32 0, i32 0), float* getelementptr inbounds (%struct.Distance, %struct.Distance* @d2, i32 0, i32 1))
  %0 = load i32, i32* getelementptr inbounds (%struct.Distance, %struct.Distance* @d1, i32 0, i32 0), align 4
  %1 = load i32, i32* getelementptr inbounds (%struct.Distance, %struct.Distance* @d2, i32 0, i32 0), align 4
  %add = add nsw i32 %0, %1
  store i32 %add, i32* getelementptr inbounds (%struct.Distance, %struct.Distance* @sumOfDistances, i32 0, i32 0), align 4
  %2 = load float, float* getelementptr inbounds (%struct.Distance, %struct.Distance* @d1, i32 0, i32 1), align 4
  %3 = load float, float* getelementptr inbounds (%struct.Distance, %struct.Distance* @d2, i32 0, i32 1), align 4
  %add10 = fadd float %2, %3
  store float %add10, float* getelementptr inbounds (%struct.Distance, %struct.Distance* @sumOfDistances, i32 0, i32 1), align 4
  %4 = load float, float* getelementptr inbounds (%struct.Distance, %struct.Distance* @sumOfDistances, i32 0, i32 1), align 4
  %conv = fpext float %4 to double
  %cmp = fcmp ogt double %conv, 1.200000e+01
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load float, float* getelementptr inbounds (%struct.Distance, %struct.Distance* @sumOfDistances, i32 0, i32 1), align 4
  %conv12 = fpext float %5 to double
  %sub = fsub double %conv12, 1.200000e+01
  %conv13 = fptrunc double %sub to float
  store float %conv13, float* getelementptr inbounds (%struct.Distance, %struct.Distance* @sumOfDistances, i32 0, i32 1), align 4
  %6 = load i32, i32* getelementptr inbounds (%struct.Distance, %struct.Distance* @sumOfDistances, i32 0, i32 0), align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* getelementptr inbounds (%struct.Distance, %struct.Distance* @sumOfDistances, i32 0, i32 0), align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = load i32, i32* getelementptr inbounds (%struct.Distance, %struct.Distance* @sumOfDistances, i32 0, i32 0), align 4
  %8 = load float, float* getelementptr inbounds (%struct.Distance, %struct.Distance* @sumOfDistances, i32 0, i32 1), align 4
  %conv14 = fpext float %8 to double
  %call15 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.6, i32 0, i32 0), i32 %7, double %conv14)
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @__isoc99_scanf(i8*, ...) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
