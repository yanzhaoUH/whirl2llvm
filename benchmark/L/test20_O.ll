; ModuleID = 'test20_O.bc'

@.str = private constant [21 x i8] c"Enter two integers: \00", align 1
@.str.1 = private constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private constant [25 x i8] c"G.C.D of %d and %d is %d\00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_51 = alloca i32
  %.preg_I4_4_50 = alloca i32
  %.preg_I4_4_53 = alloca i32
  %.preg_I4_4_52 = alloca i32
  %.preg_I4_4_49 = alloca i32
  %_temp_dummy10_8 = alloca i32, align 4
  %_temp_dummy11_9 = alloca i32, align 4
  %_temp_dummy12_10 = alloca i32, align 4
  %_temp_ehpit0_11 = alloca i32, align 4
  %gcd_7 = alloca i32, align 4
  %i_6 = alloca i32, align 4
  %n1_4 = alloca i32, align 4
  %n2_5 = alloca i32, align 4
  %old_frame_pointer_16.addr = alloca i64, align 8
  %return_address_17.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n1_4 to i32*
  %3 = bitcast i32* %n2_5 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2, i32* %3)
  store i32 0, i32* %.preg_I4_4_49, align 8
  %4 = load i32, i32* %n2_5, align 4
  store i32 %4, i32* %.preg_I4_4_52, align 8
  %5 = load i32, i32* %n1_4, align 4
  store i32 %5, i32* %.preg_I4_4_53, align 8
  %6 = load i32, i32* %.preg_I4_4_53
  %cmp1 = icmp sgt i32 %6, 0
  br i1 %cmp1, label %tb, label %L6402

tb:                                               ; preds = %entry
  %7 = load i32, i32* %.preg_I4_4_52
  %cmp2 = icmp sgt i32 %7, 0
  br i1 %cmp2, label %tb1, label %L6658

L6402:                                            ; preds = %entry
  %8 = load i32, i32* %gcd_7, align 4
  store i32 %8, i32* %.preg_I4_4_51, align 8
  br label %L4610

tb1:                                              ; preds = %tb
  store i32 1, i32* %.preg_I4_4_50, align 8
  %9 = load i32, i32* %gcd_7, align 4
  store i32 %9, i32* %.preg_I4_4_51, align 8
  br label %L4354

L6658:                                            ; preds = %tb
  %10 = load i32, i32* %gcd_7, align 4
  store i32 %10, i32* %.preg_I4_4_51, align 8
  br label %L4610

L4354:                                            ; preds = %tb6, %tb1
  %11 = load i32, i32* %.preg_I4_4_53
  %12 = load i32, i32* %.preg_I4_4_50
  %srem = srem i32 %11, %12
  %cmp3 = icmp eq i32 %srem, 0
  br i1 %cmp3, label %tb2, label %L6146

tb2:                                              ; preds = %L4354
  %13 = load i32, i32* %.preg_I4_4_52
  %14 = load i32, i32* %.preg_I4_4_50
  %srem.3 = srem i32 %13, %14
  %cmp4 = icmp eq i32 %srem.3, 0
  br i1 %cmp4, label %tb4, label %L5890

L6146:                                            ; preds = %L5890, %L4354
  %15 = load i32, i32* %.preg_I4_4_49
  %add = add i32 %15, 1
  store i32 %add, i32* %.preg_I4_4_49, align 8
  %16 = load i32, i32* %.preg_I4_4_50
  %add.5 = add i32 %16, 1
  store i32 %add.5, i32* %.preg_I4_4_50, align 8
  %17 = load i32, i32* %.preg_I4_4_49
  %18 = load i32, i32* %.preg_I4_4_53
  %cmp5 = icmp slt i32 %17, %18
  br i1 %cmp5, label %tb6, label %L5122

tb4:                                              ; preds = %tb2
  %19 = load i32, i32* %.preg_I4_4_50
  store i32 %19, i32* %.preg_I4_4_51, align 8
  br label %L5890

L5890:                                            ; preds = %tb4, %tb2
  br label %L6146

tb6:                                              ; preds = %L6146
  %20 = load i32, i32* %.preg_I4_4_49
  %21 = load i32, i32* %.preg_I4_4_52
  %cmp6 = icmp slt i32 %20, %21
  br i1 %cmp6, label %L4354, label %fb

L5122:                                            ; preds = %fb, %L6146
  br label %L4610

fb:                                               ; preds = %tb6
  br label %L5122

L4610:                                            ; preds = %L5122, %L6658, %L6402
  %22 = bitcast i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.2, i32 0, i32 0) to i8*
  %23 = load i32, i32* %.preg_I4_4_53
  %24 = load i32, i32* %.preg_I4_4_52
  %25 = load i32, i32* %.preg_I4_4_51
  %call3 = call i32 (i8*, ...) @printf(i8* %22, i32 %23, i32 %24, i32 %25)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
