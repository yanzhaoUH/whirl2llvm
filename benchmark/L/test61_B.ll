; ModuleID = 'test61.bc'

@.str = private constant [17 x i8] c"Enter 10 words:\0A\00", align 1
@.str.1 = private constant [7 x i8] c"%s[^\0A]\00", align 1
@.str.2 = private constant [29 x i8] c"\0AIn lexicographical order: \0A\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_8 = alloca i32, align 4
  %_temp_dummy11_9 = alloca i32, align 4
  %_temp_dummy12_10 = alloca i32, align 4
  %_temp_dummy13_11 = alloca i32, align 4
  %i_4 = alloca i32, align 4
  %j_5 = alloca i32, align 4
  %str_6 = alloca [10 x [50 x i8]], align 1
  %temp_7 = alloca [50 x i8], align 1
  %0 = bitcast i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  store i32 0, i32* %i_4, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32, i32* %i_4, align 4
  %cmp1 = icmp sle i32 %1, 2
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = bitcast i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.1, i32 0, i32 0) to i8*
  %3 = load i32, i32* %i_4, align 4
  %conv3 = sext i32 %3 to i64
  %arrayidx = getelementptr [10 x [50 x i8]], [10 x [50 x i8]]* %str_6, i32 0, i64 %conv3
  %4 = bitcast [50 x i8]* %arrayidx to i8*
  %call2 = call i32 (i8*, ...) @scanf(i8* %2, i8* %4)
  %5 = load i32, i32* %i_4, align 4
  %add = add i32 %5, 1
  store i32 %add, i32* %i_4, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 0, i32* %i_4, align 4
  br label %while.cond1

while.cond1:                                      ; preds = %while.end7, %while.end
  %6 = load i32, i32* %i_4, align 4
  %cmp2 = icmp sle i32 %6, 1
  br i1 %cmp2, label %while.body2, label %while.end3

while.body2:                                      ; preds = %while.cond1
  %7 = load i32, i32* %i_4, align 4
  %add.4 = add i32 %7, 1
  store i32 %add.4, i32* %j_5, align 4
  br label %while.cond5

while.end3:                                       ; preds = %while.cond1
  %8 = bitcast i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call7 = call i32 (i8*, ...) @printf(i8* %8)
  store i32 0, i32* %i_4, align 4
  br label %while.cond23

while.cond5:                                      ; preds = %if.end, %while.body2
  %9 = load i32, i32* %j_5, align 4
  %cmp3 = icmp sle i32 %9, 2
  br i1 %cmp3, label %while.body6, label %while.end7

while.body6:                                      ; preds = %while.cond5
  %10 = load i32, i32* %i_4, align 4
  %conv38 = sext i32 %10 to i64
  %arrayidx9 = getelementptr [10 x [50 x i8]], [10 x [50 x i8]]* %str_6, i32 0, i64 %conv38
  %11 = bitcast [50 x i8]* %arrayidx9 to i8*
  %12 = load i32, i32* %j_5, align 4
  %conv310 = sext i32 %12 to i64
  %arrayidx11 = getelementptr [10 x [50 x i8]], [10 x [50 x i8]]* %str_6, i32 0, i64 %conv310
  %13 = bitcast [50 x i8]* %arrayidx11 to i8*
  %call3 = call i32 @strcmp(i8* %11, i8* %13)
  %cmp4 = icmp sgt i32 %call3, 0
  br i1 %cmp4, label %if.then, label %if.else

while.end7:                                       ; preds = %while.cond5
  %14 = load i32, i32* %i_4, align 4
  %add.22 = add i32 %14, 1
  store i32 %add.22, i32* %i_4, align 4
  br label %while.cond1

if.then:                                          ; preds = %while.body6
  %arrayAddress = getelementptr [50 x i8], [50 x i8]* %temp_7, i32 0, i32 0
  %15 = bitcast i8* %arrayAddress to i8*
  %16 = load i32, i32* %i_4, align 4
  %conv312 = sext i32 %16 to i64
  %arrayidx13 = getelementptr [10 x [50 x i8]], [10 x [50 x i8]]* %str_6, i32 0, i64 %conv312
  %17 = bitcast [50 x i8]* %arrayidx13 to i8*
  %call4 = call i8* @strcpy(i8* %15, i8* %17)
  %18 = load i32, i32* %i_4, align 4
  %conv314 = sext i32 %18 to i64
  %arrayidx15 = getelementptr [10 x [50 x i8]], [10 x [50 x i8]]* %str_6, i32 0, i64 %conv314
  %19 = bitcast [50 x i8]* %arrayidx15 to i8*
  %20 = load i32, i32* %j_5, align 4
  %conv316 = sext i32 %20 to i64
  %arrayidx17 = getelementptr [10 x [50 x i8]], [10 x [50 x i8]]* %str_6, i32 0, i64 %conv316
  %21 = bitcast [50 x i8]* %arrayidx17 to i8*
  %call5 = call i8* @strcpy(i8* %19, i8* %21)
  %22 = load i32, i32* %j_5, align 4
  %conv318 = sext i32 %22 to i64
  %arrayidx19 = getelementptr [10 x [50 x i8]], [10 x [50 x i8]]* %str_6, i32 0, i64 %conv318
  %23 = bitcast [50 x i8]* %arrayidx19 to i8*
  %arrayAddress20 = getelementptr [50 x i8], [50 x i8]* %temp_7, i32 0, i32 0
  %24 = bitcast i8* %arrayAddress20 to i8*
  %call6 = call i8* @strcpy(i8* %23, i8* %24)
  br label %if.end

if.else:                                          ; preds = %while.body6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %25 = load i32, i32* %j_5, align 4
  %add.21 = add i32 %25, 1
  store i32 %add.21, i32* %j_5, align 4
  br label %while.cond5

while.cond23:                                     ; preds = %while.body24, %while.end3
  %26 = load i32, i32* %i_4, align 4
  %cmp5 = icmp sle i32 %26, 2
  br i1 %cmp5, label %while.body24, label %while.end25

while.body24:                                     ; preds = %while.cond23
  %27 = load i32, i32* %i_4, align 4
  %conv326 = sext i32 %27 to i64
  %arrayidx27 = getelementptr [10 x [50 x i8]], [10 x [50 x i8]]* %str_6, i32 0, i64 %conv326
  %28 = bitcast [50 x i8]* %arrayidx27 to i8*
  %call8 = call i32 @puts(i8* %28)
  %29 = load i32, i32* %i_4, align 4
  %add.28 = add i32 %29, 1
  store i32 %add.28, i32* %i_4, align 4
  br label %while.cond23

while.end25:                                      ; preds = %while.cond23
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

declare i32 @puts(i8*)

; Function Attrs: nounwind readonly
declare i32 @strcmp(i8*, i8*) #0

; Function Attrs: nounwind
declare i8* @strcpy(i8*, i8*) #1

attributes #0 = { nounwind readonly }
attributes #1 = { nounwind }
