; ModuleID = 'test36.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [27 x i8] c"Enter a positive integer: \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.2 = private unnamed_addr constant [14 x i8] c"%d = %d + %d\0A\00", align 1
@.str.3 = private unnamed_addr constant [55 x i8] c"%d can't be expressed as the sum of two prime numbers.\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %n = alloca i32, align 4
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start(i64 4, i8* %0) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %n) #1
  %1 = load i32, i32* %n, align 4, !tbaa !1
  %cmp.36 = icmp slt i32 %1, 4
  br i1 %cmp.36, label %if.then.11, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.inc
  %2 = phi i32 [ %3, %for.inc ], [ %1, %for.body.preheader ]
  %flag.040 = phi i32 [ %flag.1, %for.inc ], [ 0, %for.body.preheader ]
  %i.037 = phi i32 [ %inc, %for.inc ], [ 2, %for.body.preheader ]
  %div.i = sdiv i32 %i.037, 2
  %cmp.7.i = icmp slt i32 %i.037, 4
  br i1 %cmp.7.i, label %if.then, label %for.body.i.preheader

for.body.i.preheader:                             ; preds = %for.body
  br label %for.body.i

for.body.i:                                       ; preds = %for.body.i.preheader, %for.body.i
  %flag.09.i = phi i32 [ %.flag.0.i, %for.body.i ], [ 1, %for.body.i.preheader ]
  %i.08.i = phi i32 [ %inc.i, %for.body.i ], [ 2, %for.body.i.preheader ]
  %rem.i = srem i32 %i.037, %i.08.i
  %cmp1.i = icmp eq i32 %rem.i, 0
  %.flag.0.i = select i1 %cmp1.i, i32 0, i32 %flag.09.i
  %inc.i = add nuw nsw i32 %i.08.i, 1
  %cmp.i = icmp slt i32 %i.08.i, %div.i
  br i1 %cmp.i, label %for.body.i, label %checkPrime.exit

checkPrime.exit:                                  ; preds = %for.body.i
  %.flag.0.i.lcssa = phi i32 [ %.flag.0.i, %for.body.i ]
  %cmp3 = icmp eq i32 %.flag.0.i.lcssa, 1
  br i1 %cmp3, label %if.then, label %for.inc

if.then:                                          ; preds = %for.body, %checkPrime.exit
  %sub = sub nsw i32 %2, %i.037
  %div.i.21 = sdiv i32 %sub, 2
  %cmp.7.i.22 = icmp slt i32 %sub, 4
  br i1 %cmp.7.i.22, label %if.then.6, label %for.body.i.30.preheader

for.body.i.30.preheader:                          ; preds = %if.then
  br label %for.body.i.30

for.body.i.30:                                    ; preds = %for.body.i.30.preheader, %for.body.i.30
  %flag.09.i.23 = phi i32 [ %.flag.0.i.27, %for.body.i.30 ], [ 1, %for.body.i.30.preheader ]
  %i.08.i.24 = phi i32 [ %inc.i.28, %for.body.i.30 ], [ 2, %for.body.i.30.preheader ]
  %rem.i.25 = srem i32 %sub, %i.08.i.24
  %cmp1.i.26 = icmp eq i32 %rem.i.25, 0
  %.flag.0.i.27 = select i1 %cmp1.i.26, i32 0, i32 %flag.09.i.23
  %inc.i.28 = add nuw nsw i32 %i.08.i.24, 1
  %cmp.i.29 = icmp slt i32 %i.08.i.24, %div.i.21
  br i1 %cmp.i.29, label %for.body.i.30, label %checkPrime.exit32

checkPrime.exit32:                                ; preds = %for.body.i.30
  %.flag.0.i.27.lcssa = phi i32 [ %.flag.0.i.27, %for.body.i.30 ]
  %cmp5 = icmp eq i32 %.flag.0.i.27.lcssa, 1
  br i1 %cmp5, label %if.then.6, label %for.inc

if.then.6:                                        ; preds = %if.then, %checkPrime.exit32
  %call8 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.2, i64 0, i64 0), i32 %2, i32 %i.037, i32 %sub) #1
  %.pre = load i32, i32* %n, align 4, !tbaa !1
  br label %for.inc

for.inc:                                          ; preds = %checkPrime.exit, %if.then.6, %checkPrime.exit32
  %3 = phi i32 [ %.pre, %if.then.6 ], [ %2, %checkPrime.exit32 ], [ %2, %checkPrime.exit ]
  %flag.1 = phi i32 [ 1, %if.then.6 ], [ %flag.040, %checkPrime.exit32 ], [ %flag.040, %checkPrime.exit ]
  %inc = add nuw nsw i32 %i.037, 1
  %div = sdiv i32 %3, 2
  %cmp = icmp slt i32 %i.037, %div
  br i1 %cmp, label %for.body, label %for.end

for.end:                                          ; preds = %for.inc
  %flag.1.lcssa = phi i32 [ %flag.1, %for.inc ]
  %.lcssa = phi i32 [ %3, %for.inc ]
  %cmp10 = icmp eq i32 %flag.1.lcssa, 0
  br i1 %cmp10, label %if.then.11, label %if.end.13

if.then.11:                                       ; preds = %entry, %for.end
  %.lcssa45 = phi i32 [ %.lcssa, %for.end ], [ %1, %entry ]
  %call12 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([55 x i8], [55 x i8]* @.str.3, i64 0, i64 0), i32 %.lcssa45) #1
  br label %if.end.13

if.end.13:                                        ; preds = %if.then.11, %for.end
  call void @llvm.lifetime.end(i64 4, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind readnone uwtable
define i32 @checkPrime(i32 %n) #3 {
entry:
  %div = sdiv i32 %n, 2
  %cmp.7 = icmp slt i32 %n, 4
  br i1 %cmp.7, label %for.end, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.body
  %flag.09 = phi i32 [ %.flag.0, %for.body ], [ 1, %for.body.preheader ]
  %i.08 = phi i32 [ %inc, %for.body ], [ 2, %for.body.preheader ]
  %rem = srem i32 %n, %i.08
  %cmp1 = icmp eq i32 %rem, 0
  %.flag.0 = select i1 %cmp1, i32 0, i32 %flag.09
  %inc = add nuw nsw i32 %i.08, 1
  %cmp = icmp slt i32 %i.08, %div
  br i1 %cmp, label %for.body, label %for.end.loopexit

for.end.loopexit:                                 ; preds = %for.body
  %.flag.0.lcssa = phi i32 [ %.flag.0, %for.body ]
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  %flag.0.lcssa = phi i32 [ 1, %entry ], [ %.flag.0.lcssa, %for.end.loopexit ]
  ret i32 %flag.0.lcssa
}

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readnone uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
