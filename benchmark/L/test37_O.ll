; ModuleID = 'test37_O.bc'

%struct._temp_dummy13 = type {}

@.str = private constant [27 x i8] c"Enter a positive integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [9 x i8] c"Sum = %d\00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_49 = alloca i32
  %.preg_I4_4_50 = alloca i32
  %.preg_I4_4_53 = alloca i32
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_dummy11_6 = alloca i32, align 4
  %_temp_dummy12_7 = alloca i32, align 4
  %_temp_dummy13_12 = alloca i32, align 4
  %_temp_dummy13_14 = alloca %struct._temp_dummy13, align 4
  %_temp_ehpit0_15 = alloca i32, align 4
  %n_11.addr = alloca i32, align 4
  %n_13 = alloca i32, align 4
  %n_4 = alloca i32, align 4
  %old_frame_pointer_20.addr = alloca i64, align 8
  %return_address_21.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = load i32, i32* %n_4, align 4
  store i32 %3, i32* %.preg_I4_4_53, align 8
  %4 = load i32, i32* %.preg_I4_4_53
  %cmp1 = icmp ne i32 %4, 0
  br i1 %cmp1, label %tb, label %L1794

tb:                                               ; preds = %entry
  %5 = load i32, i32* %.preg_I4_4_53
  %add = add i32 %5, -1
  %call3 = call i32 @_Z10addNumbersi(i32 %add)
  store i32 %call3, i32* %.preg_I4_4_50, align 8
  %6 = load i32, i32* %.preg_I4_4_50
  %7 = load i32, i32* %.preg_I4_4_53
  %add.1 = add i32 %6, %7
  store i32 %add.1, i32* %.preg_I4_4_49, align 8
  br label %L258

L1794:                                            ; preds = %entry
  %8 = load i32, i32* %.preg_I4_4_53
  store i32 %8, i32* %.preg_I4_4_49, align 8
  br label %L258

L258:                                             ; preds = %L1794, %tb
  %9 = bitcast i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.2, i32 0, i32 0) to i8*
  %10 = load i32, i32* %.preg_I4_4_49
  %call4 = call i32 (i8*, ...) @printf(i8* %9, i32 %10)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define i32 @_Z10addNumbersi(i32 %n_4) {
entry:
  %.preg_I4_4_49 = alloca i32
  %.preg_I4_4_50 = alloca i32
  %_temp_dummy13_5 = alloca i32, align 4
  %_temp_ehpit1_6 = alloca i32, align 4
  %n_4.addr = alloca i32, align 4
  %old_frame_pointer_11.addr = alloca i64, align 8
  %return_address_12.addr = alloca i64, align 8
  store i32 %n_4, i32* %n_4.addr, align 4
  store i32 %n_4, i32* %.preg_I4_4_50, align 8
  %0 = load i32, i32* %.preg_I4_4_50
  %cmp2 = icmp ne i32 %0, 0
  br i1 %cmp2, label %tb, label %L770

tb:                                               ; preds = %entry
  %1 = load i32, i32* %.preg_I4_4_50
  %add = add i32 %1, -1
  %call5 = call i32 @_Z10addNumbersi(i32 %add)
  store i32 %call5, i32* %.preg_I4_4_49, align 8
  %2 = load i32, i32* %.preg_I4_4_50
  %3 = load i32, i32* %.preg_I4_4_49
  %add.1 = add i32 %2, %3
  ret i32 %add.1

L770:                                             ; preds = %entry
  %4 = load i32, i32* %.preg_I4_4_50
  ret i32 %4
}
