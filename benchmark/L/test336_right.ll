; ModuleID = 'test336.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [21 x i8] c"%d %d %d %d %d %d %d\00", align 1
@.str.1 = private unnamed_addr constant [6 x i8] c"%f %f\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %c1 = alloca i8, align 1
  %c2 = alloca i8, align 1
  %c3 = alloca i8, align 1
  %s1 = alloca i16, align 2
  %s2 = alloca i16, align 2
  %s3 = alloca i16, align 2
  %us1 = alloca i16, align 2
  %us2 = alloca i16, align 2
  %us3 = alloca i16, align 2
  %i1 = alloca i32, align 4
  %i2 = alloca i32, align 4
  %i3 = alloca i32, align 4
  %l1 = alloca i64, align 8
  %l2 = alloca i64, align 8
  %l3 = alloca i64, align 8
  %ll1 = alloca i64, align 8
  %ll2 = alloca i64, align 8
  %ll3 = alloca i64, align 8
  %ui1 = alloca i32, align 4
  %ui2 = alloca i32, align 4
  %ui3 = alloca i32, align 4
  %ul1 = alloca i64, align 8
  %ul2 = alloca i64, align 8
  %ul3 = alloca i64, align 8
  %f1 = alloca float, align 4
  %f2 = alloca float, align 4
  %f3 = alloca float, align 4
  %d1 = alloca double, align 8
  %d2 = alloca double, align 8
  %d3 = alloca double, align 8
  store i32 0, i32* %retval
  store i8 1, i8* %c1, align 1
  store i8 2, i8* %c2, align 1
  %0 = load i8, i8* %c1, align 1
  %conv = sext i8 %0 to i32
  %1 = load i8, i8* %c2, align 1
  %conv1 = sext i8 %1 to i32
  %mul = mul nsw i32 %conv, %conv1
  %conv2 = trunc i32 %mul to i8
  store i8 %conv2, i8* %c3, align 1
  store i16 1, i16* %s1, align 2
  store i16 2, i16* %s2, align 2
  %2 = load i16, i16* %s1, align 2
  %conv3 = sext i16 %2 to i32
  %3 = load i16, i16* %s2, align 2
  %conv4 = sext i16 %3 to i32
  %mul5 = mul nsw i32 %conv3, %conv4
  %conv6 = trunc i32 %mul5 to i16
  store i16 %conv6, i16* %s3, align 2
  store i16 1, i16* %us1, align 2
  store i16 2, i16* %us2, align 2
  %4 = load i16, i16* %us1, align 2
  %conv7 = zext i16 %4 to i32
  %5 = load i16, i16* %us2, align 2
  %conv8 = zext i16 %5 to i32
  %mul9 = mul nsw i32 %conv7, %conv8
  %conv10 = trunc i32 %mul9 to i16
  store i16 %conv10, i16* %us3, align 2
  store i32 1, i32* %i1, align 4
  store i32 2, i32* %i2, align 4
  %6 = load i32, i32* %i1, align 4
  %7 = load i32, i32* %i2, align 4
  %mul11 = mul nsw i32 %6, %7
  store i32 %mul11, i32* %i3, align 4
  store i64 1, i64* %l1, align 8
  store i64 2, i64* %l2, align 8
  %8 = load i64, i64* %l1, align 8
  %9 = load i64, i64* %l2, align 8
  %mul12 = mul nsw i64 %8, %9
  store i64 %mul12, i64* %l3, align 8
  store i64 1, i64* %ll1, align 8
  store i64 2, i64* %ll2, align 8
  %10 = load i64, i64* %ll1, align 8
  %11 = load i64, i64* %ll2, align 8
  %mul13 = mul nsw i64 %10, %11
  store i64 %mul13, i64* %ll3, align 8
  store i32 1, i32* %ui1, align 4
  store i32 1, i32* %ui2, align 4
  %12 = load i32, i32* %ui1, align 4
  %13 = load i32, i32* %ui2, align 4
  %mul14 = mul i32 %12, %13
  store i32 %mul14, i32* %ui3, align 4
  store i64 1, i64* %ul1, align 8
  store i64 1, i64* %ul2, align 8
  %14 = load i64, i64* %ul1, align 8
  %15 = load i64, i64* %ul2, align 8
  %mul15 = mul i64 %14, %15
  store i64 %mul15, i64* %ul3, align 8
  store float 1.000000e+00, float* %f1, align 4
  store float 2.000000e+00, float* %f2, align 4
  %16 = load float, float* %f1, align 4
  %17 = load float, float* %f2, align 4
  %mul16 = fmul float %16, %17
  store float %mul16, float* %f3, align 4
  store double 1.000000e+00, double* %d1, align 8
  store double 2.000000e+00, double* %d2, align 8
  %18 = load double, double* %d1, align 8
  %19 = load double, double* %d2, align 8
  %mul17 = fmul double %18, %19
  store double %mul17, double* %d3, align 8
  %20 = load i8, i8* %c3, align 1
  %conv18 = sext i8 %20 to i32
  %21 = load i16, i16* %s3, align 2
  %conv19 = sext i16 %21 to i32
  %22 = load i32, i32* %i3, align 4
  %23 = load i64, i64* %l3, align 8
  %24 = load i64, i64* %ll3, align 8
  %25 = load i32, i32* %ui3, align 4
  %26 = load i64, i64* %ul3, align 8
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str, i32 0, i32 0), i32 %conv18, i32 %conv19, i32 %22, i64 %23, i64 %24, i32 %25, i64 %26)
  %27 = load float, float* %f3, align 4
  %conv20 = fpext float %27 to double
  %28 = load double, double* %d3, align 8
  %call21 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0), double %conv20, double %28)
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
