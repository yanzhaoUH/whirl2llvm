; ModuleID = 'test42_O.bc'

@.str = private constant [15 x i8] c"Instructions:\0A\00", align 1
@.str.1 = private constant [51 x i8] c"1. Enter alphabet 'o' to convert binary to octal.\0A\00", align 1
@.str.2 = private constant [51 x i8] c"2. Enter alphabet 'b' to convert octal to binary.\0A\00", align 1
@.str.3 = private constant [3 x i8] c"%c\00", align 1
@.str.4 = private constant [24 x i8] c"Enter a binary number: \00", align 1
@.str.5 = private constant [3 x i8] c"%d\00", align 1
@.str.6 = private constant [27 x i8] c"%d in binary = %d in octal\00", align 1
@.str.7 = private constant [23 x i8] c"Enter a octal number: \00", align 1
@.str.8 = private constant [27 x i8] c"%d in octal = %d in binary\00", align 1
@_LIB_VERSION_67 = common global i32 0, align 4
@signgam_68 = common global i32 0, align 4

define i32 @main() {
entry:
  %.preg_I4_4_61 = alloca i32
  %.preg_I4_4_62 = alloca i32
  %.preg_I4_4_58 = alloca i32
  %.preg_F8_11_52 = alloca double
  %.preg_F8_11_70 = alloca double
  %.preg_I4_4_63 = alloca i32
  %.preg_I4_4_55 = alloca i32
  %.preg_I4_4_60 = alloca i32
  %.preg_I4_4_64 = alloca i32
  %.preg_I4_4_66 = alloca i32
  %.preg_I4_4_57 = alloca i32
  %.preg_F8_11_51 = alloca double
  %.preg_F8_11_69 = alloca double
  %.preg_I4_4_65 = alloca i32
  %.preg_I4_4_53 = alloca i32
  %.preg_I4_4_59 = alloca i32
  %.preg_I4_4_67 = alloca i32
  %.preg_I4_4_68 = alloca i32
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %_temp_dummy13_9 = alloca i32, align 4
  %_temp_dummy14_10 = alloca i32, align 4
  %_temp_dummy15_11 = alloca i32, align 4
  %_temp_dummy16_12 = alloca i32, align 4
  %_temp_dummy17_13 = alloca i32, align 4
  %_temp_dummy18_14 = alloca i32, align 4
  %_temp_dummy19_15 = alloca i32, align 4
  %_temp_ehpit0_32 = alloca i32, align 4
  %binary_21 = alloca i32, align 4
  %c_5 = alloca i8, align 1
  %decimal_20 = alloca i32, align 4
  %decimal_29 = alloca i32, align 4
  %i_22 = alloca i32, align 4
  %i_30 = alloca i32, align 4
  %n_19.addr = alloca i32, align 4
  %n_23 = alloca i32, align 4
  %n_27.addr = alloca i32, align 4
  %n_31 = alloca i32, align 4
  %n_4 = alloca i32, align 4
  %octal_28 = alloca i32, align 4
  %old_frame_pointer_37.addr = alloca i64, align 8
  %return_address_38.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([51 x i8], [51 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call2 = call i32 (i8*, ...) @printf(i8* %1)
  %2 = bitcast i8* getelementptr inbounds ([51 x i8], [51 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %2)
  %3 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.3, i32 0, i32 0) to i8*
  %4 = bitcast i8* %c_5 to i8*
  %call4 = call i32 (i8*, ...) @scanf(i8* %3, i8* %4)
  %5 = load i8, i8* %c_5, align 1
  %conv3 = sext i8 %5 to i32
  store i32 %conv3, i32* %.preg_I4_4_68, align 8
  %6 = load i32, i32* %.preg_I4_4_68
  %cmp1 = icmp eq i32 %6, 111
  br i1 %cmp1, label %L10242, label %fb

L10242:                                           ; preds = %tb, %entry
  %7 = bitcast i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.4, i32 0, i32 0) to i8*
  %call5 = call i32 (i8*, ...) @printf(i8* %7)
  %8 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.5, i32 0, i32 0) to i8*
  %9 = bitcast i32* %n_4 to i32*
  %call6 = call i32 (i8*, ...) @scanf(i8* %8, i32* %9)
  %10 = load i32, i32* %n_4, align 4
  store i32 %10, i32* %.preg_I4_4_67, align 8
  %11 = load i32, i32* %.preg_I4_4_67
  store i32 %11, i32* %.preg_I4_4_59, align 8
  store i32 0, i32* %.preg_I4_4_53, align 8
  %12 = load i32, i32* %.preg_I4_4_67
  %cmp3 = icmp ne i32 %12, 0
  br i1 %cmp3, label %tb1, label %L15618

fb:                                               ; preds = %entry
  %13 = load i32, i32* %.preg_I4_4_68
  %cmp2 = icmp eq i32 %13, 79
  br i1 %cmp2, label %tb, label %L9986

tb:                                               ; preds = %fb
  br label %L10242

L9986:                                            ; preds = %L12034, %fb
  %14 = load i32, i32* %.preg_I4_4_68
  %cmp7 = icmp eq i32 %14, 98
  br i1 %cmp7, label %L10754, label %fb14

tb1:                                              ; preds = %L10242
  store i32 0, i32* %.preg_I4_4_65, align 8
  store double 2.000000e+00, double* %.preg_F8_11_69, align 8
  br label %L11522

L15618:                                           ; preds = %L10242
  store i32 0, i32* %.preg_I4_4_66, align 8
  br label %L12034

L11522:                                           ; preds = %L11522, %tb1
  %15 = load double, double* %.preg_F8_11_69
  %16 = load i32, i32* %.preg_I4_4_53
  %conv9 = sitofp i32 %16 to double
  %call7 = call double @pow(double %15, double %conv9)
  store double %call7, double* %.preg_F8_11_51, align 8
  %17 = load i32, i32* %.preg_I4_4_59
  %lowPart_4 = alloca i32, align 4
  %div = sdiv i32 %17, 10
  %srem = srem i32 %17, 10
  store i32 %div, i32* %lowPart_4, align 4
  store i32 %srem, i32* %.preg_I4_4_57, align 8
  %18 = load i32, i32* %.preg_I4_4_65
  %conv92 = sitofp i32 %18 to double
  %19 = load i32, i32* %.preg_I4_4_57
  %conv93 = sitofp i32 %19 to double
  %20 = load double, double* %.preg_F8_11_51
  %mul = fmul double %conv93, %20
  %add = fadd double %conv92, %mul
  %fp2int = fptosi double %add to i32
  store i32 %fp2int, i32* %.preg_I4_4_65, align 8
  %21 = load i32, i32* %lowPart_4
  store i32 %21, i32* %.preg_I4_4_59, align 8
  %22 = load i32, i32* %.preg_I4_4_53
  %add.4 = add i32 %22, 1
  store i32 %add.4, i32* %.preg_I4_4_53, align 8
  %23 = load i32, i32* %.preg_I4_4_59
  %cmp4 = icmp ne i32 %23, 0
  br i1 %cmp4, label %L11522, label %fb5

fb5:                                              ; preds = %L11522
  br label %L15874

L15874:                                           ; preds = %fb5
  %24 = load i32, i32* %.preg_I4_4_65
  %cmp5 = icmp ne i32 %24, 0
  br i1 %cmp5, label %tb6, label %L17666

L12034:                                           ; preds = %fb12, %L17666, %L15618
  %25 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.6, i32 0, i32 0) to i8*
  %26 = load i32, i32* %.preg_I4_4_67
  %27 = load i32, i32* %.preg_I4_4_66
  %call8 = call i32 (i8*, ...) @printf(i8* %25, i32 %26, i32 %27)
  %28 = load i8, i8* %c_5, align 1
  %conv313 = sext i8 %28 to i32
  store i32 %conv313, i32* %.preg_I4_4_68, align 8
  br label %L9986

tb6:                                              ; preds = %L15874
  store i32 1, i32* %.preg_I4_4_64, align 8
  store i32 0, i32* %.preg_I4_4_66, align 8
  br label %L12546

L17666:                                           ; preds = %L15874
  store i32 0, i32* %.preg_I4_4_66, align 8
  %29 = load i32, i32* %n_4, align 4
  store i32 %29, i32* %.preg_I4_4_67, align 8
  br label %L12034

L12546:                                           ; preds = %L12546, %tb6
  %30 = load i32, i32* %.preg_I4_4_66
  %31 = load i32, i32* %.preg_I4_4_64
  %32 = load i32, i32* %.preg_I4_4_65
  %srem.7 = srem i32 %32, 8
  %mul.8 = mul i32 %31, %srem.7
  %add.9 = add i32 %30, %mul.8
  store i32 %add.9, i32* %.preg_I4_4_66, align 8
  %33 = load i32, i32* %.preg_I4_4_65
  %div.10 = sdiv i32 %33, 8
  store i32 %div.10, i32* %.preg_I4_4_65, align 8
  %34 = load i32, i32* %.preg_I4_4_64
  %mul.11 = mul i32 %34, 10
  store i32 %mul.11, i32* %.preg_I4_4_64, align 8
  %35 = load i32, i32* %.preg_I4_4_65
  %cmp6 = icmp ne i32 %35, 0
  br i1 %cmp6, label %L12546, label %fb12

fb12:                                             ; preds = %L12546
  %36 = load i32, i32* %n_4, align 4
  store i32 %36, i32* %.preg_I4_4_67, align 8
  br label %L12034

L10754:                                           ; preds = %tb15, %L9986
  %37 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.7, i32 0, i32 0) to i8*
  %call9 = call i32 (i8*, ...) @printf(i8* %37)
  %38 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.5, i32 0, i32 0) to i8*
  %39 = bitcast i32* %n_4 to i32*
  %call10 = call i32 (i8*, ...) @scanf(i8* %38, i32* %39)
  %40 = load i32, i32* %n_4, align 4
  store i32 %40, i32* %.preg_I4_4_67, align 8
  %41 = load i32, i32* %.preg_I4_4_67
  store i32 %41, i32* %.preg_I4_4_60, align 8
  store i32 0, i32* %.preg_I4_4_55, align 8
  %42 = load i32, i32* %.preg_I4_4_67
  %cmp9 = icmp ne i32 %42, 0
  br i1 %cmp9, label %tb16, label %L16386

fb14:                                             ; preds = %L9986
  %43 = load i32, i32* %.preg_I4_4_68
  %cmp8 = icmp eq i32 %43, 66
  br i1 %cmp8, label %tb15, label %L10498

tb15:                                             ; preds = %fb14
  br label %L10754

L10498:                                           ; preds = %L14082, %fb14
  ret i32 0

tb16:                                             ; preds = %L10754
  store i32 0, i32* %.preg_I4_4_63, align 8
  store double 8.000000e+00, double* %.preg_F8_11_70, align 8
  br label %L13570

L16386:                                           ; preds = %L10754
  store i32 0, i32* %.preg_I4_4_62, align 8
  br label %L14082

L13570:                                           ; preds = %L13570, %tb16
  %44 = load double, double* %.preg_F8_11_70
  %45 = load i32, i32* %.preg_I4_4_55
  %conv917 = sitofp i32 %45 to double
  %call11 = call double @pow(double %44, double %conv917)
  store double %call11, double* %.preg_F8_11_52, align 8
  %46 = load i32, i32* %.preg_I4_4_60
  %lowPart_418 = alloca i32, align 4
  %div.19 = sdiv i32 %46, 10
  %srem.20 = srem i32 %46, 10
  store i32 %div.19, i32* %lowPart_418, align 4
  store i32 %srem.20, i32* %.preg_I4_4_58, align 8
  %47 = load i32, i32* %.preg_I4_4_63
  %conv921 = sitofp i32 %47 to double
  %48 = load i32, i32* %.preg_I4_4_58
  %conv922 = sitofp i32 %48 to double
  %49 = load double, double* %.preg_F8_11_52
  %mul.23 = fmul double %conv922, %49
  %add.24 = fadd double %conv921, %mul.23
  %fp2int25 = fptosi double %add.24 to i32
  store i32 %fp2int25, i32* %.preg_I4_4_63, align 8
  %50 = load i32, i32* %lowPart_418
  store i32 %50, i32* %.preg_I4_4_60, align 8
  %51 = load i32, i32* %.preg_I4_4_55
  %add.26 = add i32 %51, 1
  store i32 %add.26, i32* %.preg_I4_4_55, align 8
  %52 = load i32, i32* %.preg_I4_4_60
  %cmp10 = icmp ne i32 %52, 0
  br i1 %cmp10, label %L13570, label %fb27

fb27:                                             ; preds = %L13570
  br label %L16642

L16642:                                           ; preds = %fb27
  %53 = load i32, i32* %.preg_I4_4_63
  %cmp11 = icmp ne i32 %53, 0
  br i1 %cmp11, label %tb28, label %L18434

L14082:                                           ; preds = %fb34, %L18434, %L16386
  %54 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.8, i32 0, i32 0) to i8*
  %55 = load i32, i32* %.preg_I4_4_67
  %56 = load i32, i32* %.preg_I4_4_62
  %call12 = call i32 (i8*, ...) @printf(i8* %54, i32 %55, i32 %56)
  br label %L10498

tb28:                                             ; preds = %L16642
  store i32 1, i32* %.preg_I4_4_61, align 8
  store i32 0, i32* %.preg_I4_4_62, align 8
  br label %L14594

L18434:                                           ; preds = %L16642
  store i32 0, i32* %.preg_I4_4_62, align 8
  %57 = load i32, i32* %n_4, align 4
  store i32 %57, i32* %.preg_I4_4_67, align 8
  br label %L14082

L14594:                                           ; preds = %L14594, %tb28
  %58 = load i32, i32* %.preg_I4_4_62
  %59 = load i32, i32* %.preg_I4_4_61
  %60 = load i32, i32* %.preg_I4_4_63
  %srem.29 = srem i32 %60, 2
  %mul.30 = mul i32 %59, %srem.29
  %add.31 = add i32 %58, %mul.30
  store i32 %add.31, i32* %.preg_I4_4_62, align 8
  %61 = load i32, i32* %.preg_I4_4_63
  %div.32 = sdiv i32 %61, 2
  store i32 %div.32, i32* %.preg_I4_4_63, align 8
  %62 = load i32, i32* %.preg_I4_4_61
  %mul.33 = mul i32 %62, 10
  store i32 %mul.33, i32* %.preg_I4_4_61, align 8
  %63 = load i32, i32* %.preg_I4_4_63
  %cmp12 = icmp ne i32 %63, 0
  br i1 %cmp12, label %L14594, label %fb34

fb34:                                             ; preds = %L14594
  %64 = load i32, i32* %n_4, align 4
  store i32 %64, i32* %.preg_I4_4_67, align 8
  br label %L14082
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define i32 @_Z12binary_octali(i32 %n_4) {
entry:
  %.preg_I4_4_56 = alloca i32
  %.preg_I4_4_54 = alloca i32
  %.preg_I4_4_52 = alloca i32
  %.preg_F8_11_49 = alloca double
  %.preg_F8_11_57 = alloca double
  %.preg_I4_4_55 = alloca i32
  %.preg_I4_4_50 = alloca i32
  %.preg_I4_4_53 = alloca i32
  %decimal_6 = alloca i32, align 4
  %i_7 = alloca i32, align 4
  %n_4.addr = alloca i32, align 4
  %octal_5 = alloca i32, align 4
  %old_frame_pointer_12.addr = alloca i64, align 8
  %return_address_13.addr = alloca i64, align 8
  store i32 %n_4, i32* %n_4.addr, align 4
  store i32 %n_4, i32* %.preg_I4_4_53, align 8
  store i32 0, i32* %.preg_I4_4_50, align 8
  %0 = load i32, i32* %.preg_I4_4_53
  %cmp17 = icmp ne i32 %0, 0
  br i1 %cmp17, label %tb, label %L5634

tb:                                               ; preds = %entry
  store i32 0, i32* %.preg_I4_4_55, align 8
  store double 2.000000e+00, double* %.preg_F8_11_57, align 8
  br label %L3842

L5634:                                            ; preds = %entry
  store i32 0, i32* %.preg_I4_4_56, align 8
  br label %L4354

L3842:                                            ; preds = %L3842, %tb
  %1 = load double, double* %.preg_F8_11_57
  %2 = load i32, i32* %.preg_I4_4_50
  %conv9 = sitofp i32 %2 to double
  %call14 = call double @pow(double %1, double %conv9)
  store double %call14, double* %.preg_F8_11_49, align 8
  %3 = load i32, i32* %.preg_I4_4_53
  %lowPart_4 = alloca i32, align 4
  %div = sdiv i32 %3, 10
  %srem = srem i32 %3, 10
  store i32 %div, i32* %lowPart_4, align 4
  store i32 %srem, i32* %.preg_I4_4_52, align 8
  %4 = load i32, i32* %.preg_I4_4_55
  %conv91 = sitofp i32 %4 to double
  %5 = load i32, i32* %.preg_I4_4_52
  %conv92 = sitofp i32 %5 to double
  %6 = load double, double* %.preg_F8_11_49
  %mul = fmul double %conv92, %6
  %add = fadd double %conv91, %mul
  %fp2int = fptosi double %add to i32
  store i32 %fp2int, i32* %.preg_I4_4_55, align 8
  %7 = load i32, i32* %lowPart_4
  store i32 %7, i32* %.preg_I4_4_53, align 8
  %8 = load i32, i32* %.preg_I4_4_50
  %add.3 = add i32 %8, 1
  store i32 %add.3, i32* %.preg_I4_4_50, align 8
  %9 = load i32, i32* %.preg_I4_4_53
  %cmp18 = icmp ne i32 %9, 0
  br i1 %cmp18, label %L3842, label %fb

fb:                                               ; preds = %L3842
  %10 = load i32, i32* %.preg_I4_4_55
  %cmp19 = icmp ne i32 %10, 0
  br i1 %cmp19, label %tb4, label %L5890

tb4:                                              ; preds = %fb
  store i32 1, i32* %.preg_I4_4_54, align 8
  store i32 0, i32* %.preg_I4_4_56, align 8
  br label %L4866

L5890:                                            ; preds = %fb
  store i32 0, i32* %.preg_I4_4_56, align 8
  br label %L4354

L4866:                                            ; preds = %L4866, %tb4
  %11 = load i32, i32* %.preg_I4_4_56
  %12 = load i32, i32* %.preg_I4_4_54
  %13 = load i32, i32* %.preg_I4_4_55
  %srem.5 = srem i32 %13, 8
  %mul.6 = mul i32 %12, %srem.5
  %add.7 = add i32 %11, %mul.6
  store i32 %add.7, i32* %.preg_I4_4_56, align 8
  %14 = load i32, i32* %.preg_I4_4_55
  %div.8 = sdiv i32 %14, 8
  store i32 %div.8, i32* %.preg_I4_4_55, align 8
  %15 = load i32, i32* %.preg_I4_4_54
  %mul.9 = mul i32 %15, 10
  store i32 %mul.9, i32* %.preg_I4_4_54, align 8
  %16 = load i32, i32* %.preg_I4_4_55
  %cmp20 = icmp ne i32 %16, 0
  br i1 %cmp20, label %L4866, label %fb10

fb10:                                             ; preds = %L4866
  br label %L4354

L4354:                                            ; preds = %fb10, %L5890, %L5634
  %17 = load i32, i32* %.preg_I4_4_56
  ret i32 %17
}

define i32 @_Z12octal_binaryi(i32 %n_4) {
entry:
  %.preg_I4_4_55 = alloca i32
  %.preg_I4_4_54 = alloca i32
  %.preg_I4_4_52 = alloca i32
  %.preg_F8_11_49 = alloca double
  %.preg_F8_11_57 = alloca double
  %.preg_I4_4_56 = alloca i32
  %.preg_I4_4_50 = alloca i32
  %.preg_I4_4_53 = alloca i32
  %binary_6 = alloca i32, align 4
  %decimal_5 = alloca i32, align 4
  %i_7 = alloca i32, align 4
  %n_4.addr = alloca i32, align 4
  %old_frame_pointer_12.addr = alloca i64, align 8
  %return_address_13.addr = alloca i64, align 8
  store i32 %n_4, i32* %n_4.addr, align 4
  store i32 %n_4, i32* %.preg_I4_4_53, align 8
  store i32 0, i32* %.preg_I4_4_50, align 8
  %0 = load i32, i32* %.preg_I4_4_53
  %cmp13 = icmp ne i32 %0, 0
  br i1 %cmp13, label %tb, label %L5634

tb:                                               ; preds = %entry
  store i32 0, i32* %.preg_I4_4_56, align 8
  store double 8.000000e+00, double* %.preg_F8_11_57, align 8
  br label %L3842

L5634:                                            ; preds = %entry
  store i32 0, i32* %.preg_I4_4_55, align 8
  br label %L4354

L3842:                                            ; preds = %L3842, %tb
  %1 = load double, double* %.preg_F8_11_57
  %2 = load i32, i32* %.preg_I4_4_50
  %conv9 = sitofp i32 %2 to double
  %call13 = call double @pow(double %1, double %conv9)
  store double %call13, double* %.preg_F8_11_49, align 8
  %3 = load i32, i32* %.preg_I4_4_53
  %lowPart_4 = alloca i32, align 4
  %div = sdiv i32 %3, 10
  %srem = srem i32 %3, 10
  store i32 %div, i32* %lowPart_4, align 4
  store i32 %srem, i32* %.preg_I4_4_52, align 8
  %4 = load i32, i32* %.preg_I4_4_56
  %conv91 = sitofp i32 %4 to double
  %5 = load i32, i32* %.preg_I4_4_52
  %conv92 = sitofp i32 %5 to double
  %6 = load double, double* %.preg_F8_11_49
  %mul = fmul double %conv92, %6
  %add = fadd double %conv91, %mul
  %fp2int = fptosi double %add to i32
  store i32 %fp2int, i32* %.preg_I4_4_56, align 8
  %7 = load i32, i32* %lowPart_4
  store i32 %7, i32* %.preg_I4_4_53, align 8
  %8 = load i32, i32* %.preg_I4_4_50
  %add.3 = add i32 %8, 1
  store i32 %add.3, i32* %.preg_I4_4_50, align 8
  %9 = load i32, i32* %.preg_I4_4_53
  %cmp14 = icmp ne i32 %9, 0
  br i1 %cmp14, label %L3842, label %fb

fb:                                               ; preds = %L3842
  %10 = load i32, i32* %.preg_I4_4_56
  %cmp15 = icmp ne i32 %10, 0
  br i1 %cmp15, label %tb4, label %L5890

tb4:                                              ; preds = %fb
  store i32 1, i32* %.preg_I4_4_54, align 8
  store i32 0, i32* %.preg_I4_4_55, align 8
  br label %L4866

L5890:                                            ; preds = %fb
  store i32 0, i32* %.preg_I4_4_55, align 8
  br label %L4354

L4866:                                            ; preds = %L4866, %tb4
  %11 = load i32, i32* %.preg_I4_4_55
  %12 = load i32, i32* %.preg_I4_4_54
  %13 = load i32, i32* %.preg_I4_4_56
  %srem.5 = srem i32 %13, 2
  %mul.6 = mul i32 %12, %srem.5
  %add.7 = add i32 %11, %mul.6
  store i32 %add.7, i32* %.preg_I4_4_55, align 8
  %14 = load i32, i32* %.preg_I4_4_56
  %div.8 = sdiv i32 %14, 2
  store i32 %div.8, i32* %.preg_I4_4_56, align 8
  %15 = load i32, i32* %.preg_I4_4_54
  %mul.9 = mul i32 %15, 10
  store i32 %mul.9, i32* %.preg_I4_4_54, align 8
  %16 = load i32, i32* %.preg_I4_4_56
  %cmp16 = icmp ne i32 %16, 0
  br i1 %cmp16, label %L4866, label %fb10

fb10:                                             ; preds = %L4866
  br label %L4354

L4354:                                            ; preds = %fb10, %L5890, %L5634
  %17 = load i32, i32* %.preg_I4_4_55
  ret i32 %17
}

declare double @pow(double, double)
