; ModuleID = 'test16.bc'

@.str = private constant [27 x i8] c"Enter a positive integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [9 x i8] c"Sum = %d\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %i_5 = alloca i32, align 4
  %n_4 = alloca i32, align 4
  %sum_6 = alloca i32, align 4
  store i32 0, i32* %sum_6, align 4
  %0 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  store i32 1, i32* %i_5, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %3 = load i32, i32* %i_5, align 4
  %4 = load i32, i32* %n_4, align 4
  %cmp1 = icmp sle i32 %3, %4
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = load i32, i32* %sum_6, align 4
  %6 = load i32, i32* %i_5, align 4
  %add = add i32 %5, %6
  store i32 %add, i32* %sum_6, align 4
  %7 = load i32, i32* %i_5, align 4
  %add.1 = add i32 %7, 1
  store i32 %add.1, i32* %i_5, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %8 = bitcast i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.2, i32 0, i32 0) to i8*
  %9 = load i32, i32* %sum_6, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %8, i32 %9)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
