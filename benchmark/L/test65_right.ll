; ModuleID = 'test65.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.TIME = type { i32, i32, i32 }

@.str = private unnamed_addr constant [20 x i8] c"Enter start time: \0A\00", align 1
@.str.1 = private unnamed_addr constant [48 x i8] c"Enter hours, minutes and seconds respectively: \00", align 1
@.str.2 = private unnamed_addr constant [9 x i8] c"%d %d %d\00", align 1
@.str.3 = private unnamed_addr constant [19 x i8] c"Enter stop time: \0A\00", align 1
@.str.4 = private unnamed_addr constant [30 x i8] c"\0ATIME DIFFERENCE: %d:%d:%d - \00", align 1
@.str.5 = private unnamed_addr constant [10 x i8] c"%d:%d:%d \00", align 1
@.str.6 = private unnamed_addr constant [12 x i8] c"= %d:%d:%d\0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %startTime = alloca %struct.TIME, align 4
  %stopTime = alloca %struct.TIME, align 4
  %diff = alloca %struct.TIME, align 4
  %startTime.coerce = alloca { i64, i32 }
  %stopTime.coerce = alloca { i64, i32 }
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0))
  %call1 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.1, i32 0, i32 0))
  %hours = getelementptr inbounds %struct.TIME, %struct.TIME* %startTime, i32 0, i32 2
  %minutes = getelementptr inbounds %struct.TIME, %struct.TIME* %startTime, i32 0, i32 1
  %seconds = getelementptr inbounds %struct.TIME, %struct.TIME* %startTime, i32 0, i32 0
  %call2 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.2, i32 0, i32 0), i32* %hours, i32* %minutes, i32* %seconds)
  %call3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.3, i32 0, i32 0))
  %call4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.1, i32 0, i32 0))
  %hours5 = getelementptr inbounds %struct.TIME, %struct.TIME* %stopTime, i32 0, i32 2
  %minutes6 = getelementptr inbounds %struct.TIME, %struct.TIME* %stopTime, i32 0, i32 1
  %seconds7 = getelementptr inbounds %struct.TIME, %struct.TIME* %stopTime, i32 0, i32 0
  %call8 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.2, i32 0, i32 0), i32* %hours5, i32* %minutes6, i32* %seconds7)
  %0 = bitcast { i64, i32 }* %startTime.coerce to i8*
  %1 = bitcast %struct.TIME* %startTime to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %0, i8* %1, i64 12, i32 0, i1 false)
  %2 = getelementptr { i64, i32 }, { i64, i32 }* %startTime.coerce, i32 0, i32 0
  %3 = load i64, i64* %2, align 1
  %4 = getelementptr { i64, i32 }, { i64, i32 }* %startTime.coerce, i32 0, i32 1
  %5 = load i32, i32* %4, align 1
  %6 = bitcast { i64, i32 }* %stopTime.coerce to i8*
  %7 = bitcast %struct.TIME* %stopTime to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %6, i8* %7, i64 12, i32 0, i1 false)
  %8 = getelementptr { i64, i32 }, { i64, i32 }* %stopTime.coerce, i32 0, i32 0
  %9 = load i64, i64* %8, align 1
  %10 = getelementptr { i64, i32 }, { i64, i32 }* %stopTime.coerce, i32 0, i32 1
  %11 = load i32, i32* %10, align 1
  call void @differenceBetweenTimePeriod(i64 %3, i32 %5, i64 %9, i32 %11, %struct.TIME* %diff)
  %hours9 = getelementptr inbounds %struct.TIME, %struct.TIME* %startTime, i32 0, i32 2
  %12 = load i32, i32* %hours9, align 4
  %minutes10 = getelementptr inbounds %struct.TIME, %struct.TIME* %startTime, i32 0, i32 1
  %13 = load i32, i32* %minutes10, align 4
  %seconds11 = getelementptr inbounds %struct.TIME, %struct.TIME* %startTime, i32 0, i32 0
  %14 = load i32, i32* %seconds11, align 4
  %call12 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.4, i32 0, i32 0), i32 %12, i32 %13, i32 %14)
  %hours13 = getelementptr inbounds %struct.TIME, %struct.TIME* %stopTime, i32 0, i32 2
  %15 = load i32, i32* %hours13, align 4
  %minutes14 = getelementptr inbounds %struct.TIME, %struct.TIME* %stopTime, i32 0, i32 1
  %16 = load i32, i32* %minutes14, align 4
  %seconds15 = getelementptr inbounds %struct.TIME, %struct.TIME* %stopTime, i32 0, i32 0
  %17 = load i32, i32* %seconds15, align 4
  %call16 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.5, i32 0, i32 0), i32 %15, i32 %16, i32 %17)
  %hours17 = getelementptr inbounds %struct.TIME, %struct.TIME* %diff, i32 0, i32 2
  %18 = load i32, i32* %hours17, align 4
  %minutes18 = getelementptr inbounds %struct.TIME, %struct.TIME* %diff, i32 0, i32 1
  %19 = load i32, i32* %minutes18, align 4
  %seconds19 = getelementptr inbounds %struct.TIME, %struct.TIME* %diff, i32 0, i32 0
  %20 = load i32, i32* %seconds19, align 4
  %call20 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.6, i32 0, i32 0), i32 %18, i32 %19, i32 %20)
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @__isoc99_scanf(i8*, ...) #1

; Function Attrs: nounwind uwtable
define void @differenceBetweenTimePeriod(i64 %start.coerce0, i32 %start.coerce1, i64 %stop.coerce0, i32 %stop.coerce1, %struct.TIME* %diff) #0 {
entry:
  %start = alloca %struct.TIME, align 8
  %coerce = alloca { i64, i32 }, align 8
  %stop = alloca %struct.TIME, align 8
  %coerce1 = alloca { i64, i32 }, align 8
  %diff.addr = alloca %struct.TIME*, align 8
  %0 = getelementptr { i64, i32 }, { i64, i32 }* %coerce, i32 0, i32 0
  store i64 %start.coerce0, i64* %0
  %1 = getelementptr { i64, i32 }, { i64, i32 }* %coerce, i32 0, i32 1
  store i32 %start.coerce1, i32* %1
  %2 = bitcast %struct.TIME* %start to i8*
  %3 = bitcast { i64, i32 }* %coerce to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %2, i8* %3, i64 12, i32 8, i1 false)
  %4 = getelementptr { i64, i32 }, { i64, i32 }* %coerce1, i32 0, i32 0
  store i64 %stop.coerce0, i64* %4
  %5 = getelementptr { i64, i32 }, { i64, i32 }* %coerce1, i32 0, i32 1
  store i32 %stop.coerce1, i32* %5
  %6 = bitcast %struct.TIME* %stop to i8*
  %7 = bitcast { i64, i32 }* %coerce1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %6, i8* %7, i64 12, i32 8, i1 false)
  store %struct.TIME* %diff, %struct.TIME** %diff.addr, align 8
  %seconds = getelementptr inbounds %struct.TIME, %struct.TIME* %stop, i32 0, i32 0
  %8 = load i32, i32* %seconds, align 4
  %seconds2 = getelementptr inbounds %struct.TIME, %struct.TIME* %start, i32 0, i32 0
  %9 = load i32, i32* %seconds2, align 4
  %cmp = icmp sgt i32 %8, %9
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %minutes = getelementptr inbounds %struct.TIME, %struct.TIME* %start, i32 0, i32 1
  %10 = load i32, i32* %minutes, align 4
  %dec = add nsw i32 %10, -1
  store i32 %dec, i32* %minutes, align 4
  %seconds3 = getelementptr inbounds %struct.TIME, %struct.TIME* %start, i32 0, i32 0
  %11 = load i32, i32* %seconds3, align 4
  %add = add nsw i32 %11, 60
  store i32 %add, i32* %seconds3, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %seconds4 = getelementptr inbounds %struct.TIME, %struct.TIME* %start, i32 0, i32 0
  %12 = load i32, i32* %seconds4, align 4
  %seconds5 = getelementptr inbounds %struct.TIME, %struct.TIME* %stop, i32 0, i32 0
  %13 = load i32, i32* %seconds5, align 4
  %sub = sub nsw i32 %12, %13
  %14 = load %struct.TIME*, %struct.TIME** %diff.addr, align 8
  %seconds6 = getelementptr inbounds %struct.TIME, %struct.TIME* %14, i32 0, i32 0
  store i32 %sub, i32* %seconds6, align 4
  %minutes7 = getelementptr inbounds %struct.TIME, %struct.TIME* %stop, i32 0, i32 1
  %15 = load i32, i32* %minutes7, align 4
  %minutes8 = getelementptr inbounds %struct.TIME, %struct.TIME* %start, i32 0, i32 1
  %16 = load i32, i32* %minutes8, align 4
  %cmp9 = icmp sgt i32 %15, %16
  br i1 %cmp9, label %if.then.10, label %if.end.14

if.then.10:                                       ; preds = %if.end
  %hours = getelementptr inbounds %struct.TIME, %struct.TIME* %start, i32 0, i32 2
  %17 = load i32, i32* %hours, align 4
  %dec11 = add nsw i32 %17, -1
  store i32 %dec11, i32* %hours, align 4
  %minutes12 = getelementptr inbounds %struct.TIME, %struct.TIME* %start, i32 0, i32 1
  %18 = load i32, i32* %minutes12, align 4
  %add13 = add nsw i32 %18, 60
  store i32 %add13, i32* %minutes12, align 4
  br label %if.end.14

if.end.14:                                        ; preds = %if.then.10, %if.end
  %minutes15 = getelementptr inbounds %struct.TIME, %struct.TIME* %start, i32 0, i32 1
  %19 = load i32, i32* %minutes15, align 4
  %minutes16 = getelementptr inbounds %struct.TIME, %struct.TIME* %stop, i32 0, i32 1
  %20 = load i32, i32* %minutes16, align 4
  %sub17 = sub nsw i32 %19, %20
  %21 = load %struct.TIME*, %struct.TIME** %diff.addr, align 8
  %minutes18 = getelementptr inbounds %struct.TIME, %struct.TIME* %21, i32 0, i32 1
  store i32 %sub17, i32* %minutes18, align 4
  %hours19 = getelementptr inbounds %struct.TIME, %struct.TIME* %start, i32 0, i32 2
  %22 = load i32, i32* %hours19, align 4
  %hours20 = getelementptr inbounds %struct.TIME, %struct.TIME* %stop, i32 0, i32 2
  %23 = load i32, i32* %hours20, align 4
  %sub21 = sub nsw i32 %22, %23
  %24 = load %struct.TIME*, %struct.TIME** %diff.addr, align 8
  %hours22 = getelementptr inbounds %struct.TIME, %struct.TIME* %24, i32 0, i32 2
  store i32 %sub21, i32* %hours22, align 4
  ret void
}

; Function Attrs: nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture, i8* nocapture readonly, i64, i32, i1) #2

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
