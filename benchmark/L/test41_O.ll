; ModuleID = 'test41_O.bc'

@.str = private constant [15 x i8] c"Instructions:\0A\00", align 1
@.str.1 = private constant [52 x i8] c"1. Enter alphabet 'o' to convert decimal to octal.\0A\00", align 1
@.str.2 = private constant [52 x i8] c"2. Enter alphabet 'd' to convert octal to decimal.\0A\00", align 1
@.str.3 = private constant [3 x i8] c"%c\00", align 1
@.str.4 = private constant [24 x i8] c"Enter an octal number: \00", align 1
@.str.5 = private constant [3 x i8] c"%d\00", align 1
@.str.6 = private constant [28 x i8] c"%d in octal = %d in decimal\00", align 1
@.str.7 = private constant [25 x i8] c"Enter a decimal number: \00", align 1
@.str.8 = private constant [28 x i8] c"%d in decimal = %d in octal\00", align 1
@_LIB_VERSION_66 = common global i32 0, align 4
@signgam_67 = common global i32 0, align 4

define i32 @main() {
entry:
  %.preg_I4_4_58 = alloca i32
  %.preg_I4_4_60 = alloca i32
  %.preg_I4_4_59 = alloca i32
  %.preg_I4_4_57 = alloca i32
  %.preg_F8_11_51 = alloca double
  %.preg_I4_4_56 = alloca i32
  %.preg_I4_4_54 = alloca i32
  %.preg_F8_11_64 = alloca double
  %.preg_I4_4_61 = alloca i32
  %.preg_I4_4_52 = alloca i32
  %.preg_I4_4_55 = alloca i32
  %.preg_I4_4_62 = alloca i32
  %.preg_I4_4_63 = alloca i32
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %_temp_dummy13_9 = alloca i32, align 4
  %_temp_dummy14_10 = alloca i32, align 4
  %_temp_dummy15_11 = alloca i32, align 4
  %_temp_dummy16_12 = alloca i32, align 4
  %_temp_dummy17_13 = alloca i32, align 4
  %_temp_dummy18_14 = alloca i32, align 4
  %_temp_dummy19_15 = alloca i32, align 4
  %_temp_ehpit0_32 = alloca i32, align 4
  %c_5 = alloca i8, align 1
  %decimal_28 = alloca i32, align 4
  %i_21 = alloca i32, align 4
  %i_29 = alloca i32, align 4
  %n_19.addr = alloca i32, align 4
  %n_23 = alloca i32, align 4
  %n_27.addr = alloca i32, align 4
  %n_31 = alloca i32, align 4
  %n_4 = alloca i32, align 4
  %octal_22 = alloca i32, align 4
  %old_frame_pointer_37.addr = alloca i64, align 8
  %rem_20 = alloca i32, align 4
  %rem_30 = alloca i32, align 4
  %return_address_38.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([52 x i8], [52 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call2 = call i32 (i8*, ...) @printf(i8* %1)
  %2 = bitcast i8* getelementptr inbounds ([52 x i8], [52 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %2)
  %3 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.3, i32 0, i32 0) to i8*
  %4 = bitcast i8* %c_5 to i8*
  %call4 = call i32 (i8*, ...) @scanf(i8* %3, i8* %4)
  %5 = load i8, i8* %c_5, align 1
  %conv3 = sext i8 %5 to i32
  store i32 %conv3, i32* %.preg_I4_4_63, align 8
  %6 = load i32, i32* %.preg_I4_4_63
  %cmp1 = icmp eq i32 %6, 100
  br i1 %cmp1, label %L7170, label %fb

L7170:                                            ; preds = %tb, %entry
  %7 = bitcast i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.4, i32 0, i32 0) to i8*
  %call5 = call i32 (i8*, ...) @printf(i8* %7)
  %8 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.5, i32 0, i32 0) to i8*
  %9 = bitcast i32* %n_4 to i32*
  %call6 = call i32 (i8*, ...) @scanf(i8* %8, i32* %9)
  %10 = load i32, i32* %n_4, align 4
  store i32 %10, i32* %.preg_I4_4_62, align 8
  %11 = load i32, i32* %.preg_I4_4_62
  store i32 %11, i32* %.preg_I4_4_55, align 8
  store i32 0, i32* %.preg_I4_4_52, align 8
  %12 = load i32, i32* %.preg_I4_4_62
  %cmp3 = icmp ne i32 %12, 0
  br i1 %cmp3, label %tb1, label %L10754

fb:                                               ; preds = %entry
  %13 = load i32, i32* %.preg_I4_4_63
  %cmp2 = icmp eq i32 %13, 68
  br i1 %cmp2, label %tb, label %L6914

tb:                                               ; preds = %fb
  br label %L7170

L6914:                                            ; preds = %L7938, %fb
  %14 = load i32, i32* %.preg_I4_4_63
  %cmp5 = icmp eq i32 %14, 111
  br i1 %cmp5, label %L7682, label %fb7

tb1:                                              ; preds = %L7170
  store i32 0, i32* %.preg_I4_4_61, align 8
  store double 8.000000e+00, double* %.preg_F8_11_64, align 8
  br label %L8450

L10754:                                           ; preds = %L7170
  store i32 0, i32* %.preg_I4_4_61, align 8
  br label %L7938

L8450:                                            ; preds = %L8450, %tb1
  %15 = load i32, i32* %.preg_I4_4_55
  %lowPart_4 = alloca i32, align 4
  %div = sdiv i32 %15, 10
  %srem = srem i32 %15, 10
  store i32 %div, i32* %lowPart_4, align 4
  store i32 %srem, i32* %.preg_I4_4_54, align 8
  %16 = load i32, i32* %.preg_I4_4_54
  store i32 %16, i32* %.preg_I4_4_56, align 8
  %17 = load i32, i32* %lowPart_4
  store i32 %17, i32* %.preg_I4_4_55, align 8
  %18 = load double, double* %.preg_F8_11_64
  %19 = load i32, i32* %.preg_I4_4_52
  %conv9 = sitofp i32 %19 to double
  %call7 = call double @pow(double %18, double %conv9)
  store double %call7, double* %.preg_F8_11_51, align 8
  %20 = load i32, i32* %.preg_I4_4_61
  %conv92 = sitofp i32 %20 to double
  %21 = load i32, i32* %.preg_I4_4_56
  %conv93 = sitofp i32 %21 to double
  %22 = load double, double* %.preg_F8_11_51
  %mul = fmul double %conv93, %22
  %add = fadd double %conv92, %mul
  %fp2int = fptosi double %add to i32
  store i32 %fp2int, i32* %.preg_I4_4_61, align 8
  %23 = load i32, i32* %.preg_I4_4_52
  %add.4 = add i32 %23, 1
  store i32 %add.4, i32* %.preg_I4_4_52, align 8
  %24 = load i32, i32* %.preg_I4_4_55
  %cmp4 = icmp ne i32 %24, 0
  br i1 %cmp4, label %L8450, label %fb5

fb5:                                              ; preds = %L8450
  %25 = load i32, i32* %n_4, align 4
  store i32 %25, i32* %.preg_I4_4_62, align 8
  br label %L7938

L7938:                                            ; preds = %fb5, %L10754
  %26 = bitcast i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.6, i32 0, i32 0) to i8*
  %27 = load i32, i32* %.preg_I4_4_62
  %28 = load i32, i32* %.preg_I4_4_61
  %call8 = call i32 (i8*, ...) @printf(i8* %26, i32 %27, i32 %28)
  %29 = load i8, i8* %c_5, align 1
  %conv36 = sext i8 %29 to i32
  store i32 %conv36, i32* %.preg_I4_4_63, align 8
  br label %L6914

L7682:                                            ; preds = %tb8, %L6914
  %30 = bitcast i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.7, i32 0, i32 0) to i8*
  %call9 = call i32 (i8*, ...) @printf(i8* %30)
  %31 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.5, i32 0, i32 0) to i8*
  %32 = bitcast i32* %n_4 to i32*
  %call10 = call i32 (i8*, ...) @scanf(i8* %31, i32* %32)
  %33 = load i32, i32* %n_4, align 4
  store i32 %33, i32* %.preg_I4_4_62, align 8
  %34 = load i32, i32* %.preg_I4_4_62
  store i32 %34, i32* %.preg_I4_4_57, align 8
  %35 = load i32, i32* %.preg_I4_4_62
  %cmp7 = icmp ne i32 %35, 0
  br i1 %cmp7, label %tb9, label %L11522

fb7:                                              ; preds = %L6914
  %36 = load i32, i32* %.preg_I4_4_63
  %cmp6 = icmp eq i32 %36, 79
  br i1 %cmp6, label %tb8, label %L7426

tb8:                                              ; preds = %fb7
  br label %L7682

L7426:                                            ; preds = %L8962, %fb7
  ret i32 0

tb9:                                              ; preds = %L7682
  store i32 0, i32* %.preg_I4_4_59, align 8
  store i32 1, i32* %.preg_I4_4_60, align 8
  br label %L9474

L11522:                                           ; preds = %L7682
  store i32 0, i32* %.preg_I4_4_59, align 8
  br label %L8962

L9474:                                            ; preds = %L9474, %tb9
  %37 = load i32, i32* %.preg_I4_4_57
  %srem.10 = srem i32 %37, 8
  store i32 %srem.10, i32* %.preg_I4_4_58, align 8
  %38 = load i32, i32* %.preg_I4_4_57
  %div.11 = sdiv i32 %38, 8
  store i32 %div.11, i32* %.preg_I4_4_57, align 8
  %39 = load i32, i32* %.preg_I4_4_59
  %40 = load i32, i32* %.preg_I4_4_58
  %41 = load i32, i32* %.preg_I4_4_60
  %mul.12 = mul i32 %40, %41
  %add.13 = add i32 %39, %mul.12
  store i32 %add.13, i32* %.preg_I4_4_59, align 8
  %42 = load i32, i32* %.preg_I4_4_60
  %mul.14 = mul i32 %42, 10
  store i32 %mul.14, i32* %.preg_I4_4_60, align 8
  %43 = load i32, i32* %.preg_I4_4_57
  %cmp8 = icmp ne i32 %43, 0
  br i1 %cmp8, label %L9474, label %fb15

fb15:                                             ; preds = %L9474
  br label %L8962

L8962:                                            ; preds = %fb15, %L11522
  %44 = bitcast i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.8, i32 0, i32 0) to i8*
  %45 = load i32, i32* %.preg_I4_4_62
  %46 = load i32, i32* %.preg_I4_4_59
  %call11 = call i32 (i8*, ...) @printf(i8* %44, i32 %45, i32 %46)
  br label %L7426
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define i32 @_Z13octal_decimali(i32 %n_4) {
entry:
  %.preg_F8_11_49 = alloca double
  %.preg_I4_4_53 = alloca i32
  %.preg_I4_4_51 = alloca i32
  %.preg_F8_11_55 = alloca double
  %.preg_I4_4_54 = alloca i32
  %.preg_I4_4_50 = alloca i32
  %.preg_I4_4_52 = alloca i32
  %decimal_5 = alloca i32, align 4
  %i_6 = alloca i32, align 4
  %n_4.addr = alloca i32, align 4
  %old_frame_pointer_12.addr = alloca i64, align 8
  %rem_7 = alloca i32, align 4
  %return_address_13.addr = alloca i64, align 8
  store i32 %n_4, i32* %n_4.addr, align 4
  store i32 %n_4, i32* %.preg_I4_4_52, align 8
  store i32 0, i32* %.preg_I4_4_50, align 8
  %0 = load i32, i32* %.preg_I4_4_52
  %cmp9 = icmp ne i32 %0, 0
  br i1 %cmp9, label %tb, label %L3074

tb:                                               ; preds = %entry
  store i32 0, i32* %.preg_I4_4_54, align 8
  store double 8.000000e+00, double* %.preg_F8_11_55, align 8
  br label %L2306

L3074:                                            ; preds = %entry
  store i32 0, i32* %.preg_I4_4_54, align 8
  br label %L1794

L2306:                                            ; preds = %L2306, %tb
  %1 = load i32, i32* %.preg_I4_4_52
  %lowPart_4 = alloca i32, align 4
  %div = sdiv i32 %1, 10
  %srem = srem i32 %1, 10
  store i32 %div, i32* %lowPart_4, align 4
  store i32 %srem, i32* %.preg_I4_4_51, align 8
  %2 = load i32, i32* %.preg_I4_4_51
  store i32 %2, i32* %.preg_I4_4_53, align 8
  %3 = load i32, i32* %lowPart_4
  store i32 %3, i32* %.preg_I4_4_52, align 8
  %4 = load double, double* %.preg_F8_11_55
  %5 = load i32, i32* %.preg_I4_4_50
  %conv9 = sitofp i32 %5 to double
  %call12 = call double @pow(double %4, double %conv9)
  store double %call12, double* %.preg_F8_11_49, align 8
  %6 = load i32, i32* %.preg_I4_4_54
  %conv91 = sitofp i32 %6 to double
  %7 = load i32, i32* %.preg_I4_4_53
  %conv92 = sitofp i32 %7 to double
  %8 = load double, double* %.preg_F8_11_49
  %mul = fmul double %conv92, %8
  %add = fadd double %conv91, %mul
  %fp2int = fptosi double %add to i32
  store i32 %fp2int, i32* %.preg_I4_4_54, align 8
  %9 = load i32, i32* %.preg_I4_4_50
  %add.3 = add i32 %9, 1
  store i32 %add.3, i32* %.preg_I4_4_50, align 8
  %10 = load i32, i32* %.preg_I4_4_52
  %cmp10 = icmp ne i32 %10, 0
  br i1 %cmp10, label %L2306, label %fb

fb:                                               ; preds = %L2306
  br label %L1794

L1794:                                            ; preds = %fb, %L3074
  %11 = load i32, i32* %.preg_I4_4_54
  ret i32 %11
}

define i32 @_Z13decimal_octali(i32 %n_4) {
entry:
  %.preg_I4_4_51 = alloca i32
  %.preg_I4_4_53 = alloca i32
  %.preg_I4_4_52 = alloca i32
  %.preg_I4_4_50 = alloca i32
  %i_6 = alloca i32, align 4
  %n_4.addr = alloca i32, align 4
  %octal_7 = alloca i32, align 4
  %old_frame_pointer_12.addr = alloca i64, align 8
  %rem_5 = alloca i32, align 4
  %return_address_13.addr = alloca i64, align 8
  store i32 %n_4, i32* %n_4.addr, align 4
  store i32 %n_4, i32* %.preg_I4_4_50, align 8
  %0 = load i32, i32* %.preg_I4_4_50
  %cmp11 = icmp ne i32 %0, 0
  br i1 %cmp11, label %tb, label %L2818

tb:                                               ; preds = %entry
  store i32 0, i32* %.preg_I4_4_52, align 8
  store i32 1, i32* %.preg_I4_4_53, align 8
  br label %L2306

L2818:                                            ; preds = %entry
  store i32 0, i32* %.preg_I4_4_52, align 8
  br label %L1794

L2306:                                            ; preds = %L2306, %tb
  %1 = load i32, i32* %.preg_I4_4_50
  %srem = srem i32 %1, 8
  store i32 %srem, i32* %.preg_I4_4_51, align 8
  %2 = load i32, i32* %.preg_I4_4_50
  %div = sdiv i32 %2, 8
  store i32 %div, i32* %.preg_I4_4_50, align 8
  %3 = load i32, i32* %.preg_I4_4_52
  %4 = load i32, i32* %.preg_I4_4_51
  %5 = load i32, i32* %.preg_I4_4_53
  %mul = mul i32 %4, %5
  %add = add i32 %3, %mul
  store i32 %add, i32* %.preg_I4_4_52, align 8
  %6 = load i32, i32* %.preg_I4_4_53
  %mul.1 = mul i32 %6, 10
  store i32 %mul.1, i32* %.preg_I4_4_53, align 8
  %7 = load i32, i32* %.preg_I4_4_50
  %cmp12 = icmp ne i32 %7, 0
  br i1 %cmp12, label %L2306, label %fb

fb:                                               ; preds = %L2306
  br label %L1794

L1794:                                            ; preds = %fb, %L2818
  %8 = load i32, i32* %.preg_I4_4_52
  ret i32 %8
}

declare double @pow(double, double)
