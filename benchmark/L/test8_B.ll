; ModuleID = 'test8.bc'

@.str = private constant [21 x i8] c"Enter first number: \00", align 1
@.str.1 = private constant [4 x i8] c"%lf\00", align 1
@.str.2 = private constant [22 x i8] c"Enter second number: \00", align 1
@.str.3 = private constant [38 x i8] c"\0AAfter swapping, firstNumber = %.2lf\0A\00", align 1
@.str.4 = private constant [37 x i8] c"After swapping, secondNumber = %.2lf\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %_temp_dummy14_11 = alloca i32, align 4
  %_temp_dummy15_12 = alloca i32, align 4
  %firstNumber_4 = alloca double, align 8
  %secondNumber_5 = alloca double, align 8
  %temporaryVariable_6 = alloca double, align 8
  %0 = bitcast i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast double* %firstNumber_4 to double*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, double* %2)
  %3 = bitcast i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %3)
  %4 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.1, i32 0, i32 0) to i8*
  %5 = bitcast double* %secondNumber_5 to double*
  %call4 = call i32 (i8*, ...) @scanf(i8* %4, double* %5)
  %6 = load double, double* %firstNumber_4, align 8
  store double %6, double* %temporaryVariable_6, align 8
  %7 = load double, double* %secondNumber_5, align 8
  store double %7, double* %firstNumber_4, align 8
  %8 = load double, double* %temporaryVariable_6, align 8
  store double %8, double* %secondNumber_5, align 8
  %9 = bitcast i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.3, i32 0, i32 0) to i8*
  %10 = load double, double* %firstNumber_4, align 8
  %call5 = call i32 (i8*, ...) @printf(i8* %9, double %10)
  %11 = bitcast i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.4, i32 0, i32 0) to i8*
  %12 = load double, double* %secondNumber_5, align 8
  %call6 = call i32 (i8*, ...) @printf(i8* %11, double %12)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
