; ModuleID = 'test52.bc'

@.str = private constant [17 x i8] c"Enter elements: \00", align 1
@.str.1 = private constant [15 x i8] c"You entered: \0A\00", align 1
@.str.2 = private constant [4 x i8] c"%d\0A\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %data_4 = alloca [5 x i32], align 4
  %i_5 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  store i32 0, i32* %i_5, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32, i32* %i_5, align 4
  %cmp1 = icmp sle i32 %1, 4
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %arrayAddress = getelementptr [5 x i32], [5 x i32]* %data_4, i32 0, i32 0
  %2 = load i32, i32* %i_5, align 4
  %conv3 = sext i32 %2 to i64
  %mul = mul i64 %conv3, 4
  %3 = ptrtoint i32* %arrayAddress to i64
  %add = add i64 %3, %mul
  %4 = load i32, i32* %i_5, align 4
  %5 = inttoptr i64 %add to i32*
  store i32 %4, i32* %5, align 4
  %6 = load i32, i32* %i_5, align 4
  %add.1 = add i32 %6, 1
  store i32 %add.1, i32* %i_5, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %7 = bitcast i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call2 = call i32 (i8*, ...) @printf(i8* %7)
  store i32 0, i32* %i_5, align 4
  br label %while.cond2

while.cond2:                                      ; preds = %while.body3, %while.end
  %8 = load i32, i32* %i_5, align 4
  %cmp2 = icmp sle i32 %8, 4
  br i1 %cmp2, label %while.body3, label %while.end4

while.body3:                                      ; preds = %while.cond2
  %9 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.2, i32 0, i32 0) to i8*
  %arrayAddress5 = getelementptr [5 x i32], [5 x i32]* %data_4, i32 0, i32 0
  %10 = load i32, i32* %i_5, align 4
  %conv36 = sext i32 %10 to i64
  %mul.7 = mul i64 %conv36, 4
  %11 = ptrtoint i32* %arrayAddress5 to i64
  %add.8 = add i64 %11, %mul.7
  %12 = inttoptr i64 %add.8 to i32*
  %13 = load i32, i32* %12, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %9, i32 %13)
  %14 = load i32, i32* %i_5, align 4
  %add.9 = add i32 %14, 1
  store i32 %add.9, i32* %i_5, align 4
  br label %while.cond2

while.end4:                                       ; preds = %while.cond2
  ret i32 0
}

declare i32 @printf(i8*, ...)
