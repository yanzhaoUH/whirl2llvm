; ModuleID = 'test48.bc'

@.str = private constant [43 x i8] c"Enter number of rows (between 1 and 100): \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [46 x i8] c"Enter number of columns (between 1 and 100): \00", align 1
@.str.3 = private constant [32 x i8] c"\0AEnter elements of 1st matrix:\0A\00", align 1
@.str.4 = private constant [22 x i8] c"Enter element a%d%d: \00", align 1
@.str.5 = private constant [31 x i8] c"Enter elements of 2nd matrix:\0A\00", align 1
@.str.6 = private constant [26 x i8] c"\0ASum of two matrix is: \0A\0A\00", align 1
@.str.7 = private constant [6 x i8] c"%d   \00", align 1
@.str.8 = private constant [3 x i8] c"\0A\0A\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_11 = alloca i32, align 4
  %_temp_dummy110_21 = alloca i32, align 4
  %_temp_dummy11_12 = alloca i32, align 4
  %_temp_dummy12_13 = alloca i32, align 4
  %_temp_dummy13_14 = alloca i32, align 4
  %_temp_dummy14_15 = alloca i32, align 4
  %_temp_dummy15_16 = alloca i32, align 4
  %_temp_dummy16_17 = alloca i32, align 4
  %_temp_dummy17_18 = alloca i32, align 4
  %_temp_dummy18_19 = alloca i32, align 4
  %_temp_dummy19_20 = alloca i32, align 4
  %a_6 = alloca [100 x [100 x i32]], align 4
  %b_7 = alloca [100 x [100 x i32]], align 4
  %c_5 = alloca i32, align 4
  %i_9 = alloca i32, align 4
  %j_10 = alloca i32, align 4
  %r_4 = alloca i32, align 4
  %sum_8 = alloca [100 x [100 x i32]], align 4
  %0 = bitcast i8* getelementptr inbounds ([43 x i8], [43 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %r_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = bitcast i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %3)
  %4 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %5 = bitcast i32* %c_5 to i32*
  %call4 = call i32 (i8*, ...) @scanf(i8* %4, i32* %5)
  %6 = bitcast i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.3, i32 0, i32 0) to i8*
  %call5 = call i32 (i8*, ...) @printf(i8* %6)
  store i32 0, i32* %i_9, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.end3, %entry
  %7 = load i32, i32* %i_9, align 4
  %8 = load i32, i32* %r_4, align 4
  %cmp1 = icmp slt i32 %7, %8
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  store i32 0, i32* %j_10, align 4
  br label %while.cond1

while.end:                                        ; preds = %while.cond
  %9 = bitcast i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.5, i32 0, i32 0) to i8*
  %call7 = call i32 (i8*, ...) @printf(i8* %9)
  store i32 0, i32* %i_9, align 4
  br label %while.cond11

while.cond1:                                      ; preds = %while.body2, %while.body
  %10 = load i32, i32* %j_10, align 4
  %11 = load i32, i32* %c_5, align 4
  %cmp2 = icmp slt i32 %10, %11
  br i1 %cmp2, label %while.body2, label %while.end3

while.body2:                                      ; preds = %while.cond1
  %12 = bitcast i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.4, i32 0, i32 0) to i8*
  %13 = load i32, i32* %i_9, align 4
  %add = add i32 %13, 1
  %14 = load i32, i32* %j_10, align 4
  %add.4 = add i32 %14, 1
  %call6 = call i32 (i8*, ...) @printf(i8* %12, i32 %add, i32 %add.4)
  %15 = load i32, i32* %i_9, align 4
  %conv3 = sext i32 %15 to i64
  %arrayidx = getelementptr [100 x [100 x i32]], [100 x [100 x i32]]* %a_6, i32 0, i64 %conv3
  %16 = load i32, i32* %j_10, align 4
  %conv35 = sext i32 %16 to i64
  %arrayidx6 = getelementptr [100 x i32], [100 x i32]* %arrayidx, i32 0, i64 %conv35
  %17 = load i32, i32* %i_9, align 4
  %add.7 = add i32 %17, 1
  %18 = load i32, i32* %j_10, align 4
  %add.8 = add i32 %18, 1
  %mul = mul i32 %add.7, %add.8
  store i32 %mul, i32* %arrayidx6, align 4
  %19 = load i32, i32* %j_10, align 4
  %add.9 = add i32 %19, 1
  store i32 %add.9, i32* %j_10, align 4
  br label %while.cond1

while.end3:                                       ; preds = %while.cond1
  %20 = load i32, i32* %i_9, align 4
  %add.10 = add i32 %20, 1
  store i32 %add.10, i32* %i_9, align 4
  br label %while.cond

while.cond11:                                     ; preds = %while.end16, %while.end
  %21 = load i32, i32* %i_9, align 4
  %22 = load i32, i32* %r_4, align 4
  %cmp3 = icmp slt i32 %21, %22
  br i1 %cmp3, label %while.body12, label %while.end13

while.body12:                                     ; preds = %while.cond11
  store i32 0, i32* %j_10, align 4
  br label %while.cond14

while.end13:                                      ; preds = %while.cond11
  store i32 0, i32* %i_9, align 4
  br label %while.cond28

while.cond14:                                     ; preds = %while.body15, %while.body12
  %23 = load i32, i32* %j_10, align 4
  %24 = load i32, i32* %c_5, align 4
  %cmp4 = icmp slt i32 %23, %24
  br i1 %cmp4, label %while.body15, label %while.end16

while.body15:                                     ; preds = %while.cond14
  %25 = bitcast i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.4, i32 0, i32 0) to i8*
  %26 = load i32, i32* %i_9, align 4
  %add.17 = add i32 %26, 1
  %27 = load i32, i32* %j_10, align 4
  %add.18 = add i32 %27, 1
  %call8 = call i32 (i8*, ...) @printf(i8* %25, i32 %add.17, i32 %add.18)
  %28 = load i32, i32* %i_9, align 4
  %conv319 = sext i32 %28 to i64
  %arrayidx20 = getelementptr [100 x [100 x i32]], [100 x [100 x i32]]* %b_7, i32 0, i64 %conv319
  %29 = load i32, i32* %j_10, align 4
  %conv321 = sext i32 %29 to i64
  %arrayidx22 = getelementptr [100 x i32], [100 x i32]* %arrayidx20, i32 0, i64 %conv321
  %30 = load i32, i32* %i_9, align 4
  %add.23 = add i32 %30, 1
  %31 = load i32, i32* %j_10, align 4
  %add.24 = add i32 %31, 1
  %mul.25 = mul i32 %add.23, %add.24
  store i32 %mul.25, i32* %arrayidx22, align 4
  %32 = load i32, i32* %j_10, align 4
  %add.26 = add i32 %32, 1
  store i32 %add.26, i32* %j_10, align 4
  br label %while.cond14

while.end16:                                      ; preds = %while.cond14
  %33 = load i32, i32* %i_9, align 4
  %add.27 = add i32 %33, 1
  store i32 %add.27, i32* %i_9, align 4
  br label %while.cond11

while.cond28:                                     ; preds = %while.end33, %while.end13
  %34 = load i32, i32* %i_9, align 4
  %35 = load i32, i32* %r_4, align 4
  %cmp5 = icmp slt i32 %34, %35
  br i1 %cmp5, label %while.body29, label %while.end30

while.body29:                                     ; preds = %while.cond28
  store i32 0, i32* %j_10, align 4
  br label %while.cond31

while.end30:                                      ; preds = %while.cond28
  %36 = bitcast i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.6, i32 0, i32 0) to i8*
  %call9 = call i32 (i8*, ...) @printf(i8* %36)
  store i32 0, i32* %i_9, align 4
  br label %while.cond49

while.cond31:                                     ; preds = %while.body32, %while.body29
  %37 = load i32, i32* %j_10, align 4
  %38 = load i32, i32* %c_5, align 4
  %cmp6 = icmp slt i32 %37, %38
  br i1 %cmp6, label %while.body32, label %while.end33

while.body32:                                     ; preds = %while.cond31
  %39 = load i32, i32* %i_9, align 4
  %conv334 = sext i32 %39 to i64
  %arrayidx35 = getelementptr [100 x [100 x i32]], [100 x [100 x i32]]* %sum_8, i32 0, i64 %conv334
  %40 = load i32, i32* %j_10, align 4
  %conv336 = sext i32 %40 to i64
  %arrayidx37 = getelementptr [100 x i32], [100 x i32]* %arrayidx35, i32 0, i64 %conv336
  %41 = load i32, i32* %i_9, align 4
  %conv338 = sext i32 %41 to i64
  %arrayidx39 = getelementptr [100 x [100 x i32]], [100 x [100 x i32]]* %a_6, i32 0, i64 %conv338
  %42 = load i32, i32* %j_10, align 4
  %conv340 = sext i32 %42 to i64
  %arrayidx41 = getelementptr [100 x i32], [100 x i32]* %arrayidx39, i32 0, i64 %conv340
  %43 = load i32, i32* %arrayidx41, align 4
  %44 = load i32, i32* %i_9, align 4
  %conv342 = sext i32 %44 to i64
  %arrayidx43 = getelementptr [100 x [100 x i32]], [100 x [100 x i32]]* %b_7, i32 0, i64 %conv342
  %45 = load i32, i32* %j_10, align 4
  %conv344 = sext i32 %45 to i64
  %arrayidx45 = getelementptr [100 x i32], [100 x i32]* %arrayidx43, i32 0, i64 %conv344
  %46 = load i32, i32* %arrayidx45, align 4
  %add.46 = add i32 %43, %46
  store i32 %add.46, i32* %arrayidx37, align 4
  %47 = load i32, i32* %j_10, align 4
  %add.47 = add i32 %47, 1
  store i32 %add.47, i32* %j_10, align 4
  br label %while.cond31

while.end33:                                      ; preds = %while.cond31
  %48 = load i32, i32* %i_9, align 4
  %add.48 = add i32 %48, 1
  store i32 %add.48, i32* %i_9, align 4
  br label %while.cond28

while.cond49:                                     ; preds = %while.end54, %while.end30
  %49 = load i32, i32* %i_9, align 4
  %50 = load i32, i32* %r_4, align 4
  %cmp7 = icmp slt i32 %49, %50
  br i1 %cmp7, label %while.body50, label %while.end51

while.body50:                                     ; preds = %while.cond49
  store i32 0, i32* %j_10, align 4
  br label %while.cond52

while.end51:                                      ; preds = %while.cond49
  ret i32 0

while.cond52:                                     ; preds = %if.end, %while.body50
  %51 = load i32, i32* %j_10, align 4
  %52 = load i32, i32* %c_5, align 4
  %cmp8 = icmp slt i32 %51, %52
  br i1 %cmp8, label %while.body53, label %while.end54

while.body53:                                     ; preds = %while.cond52
  %53 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.7, i32 0, i32 0) to i8*
  %54 = load i32, i32* %i_9, align 4
  %conv355 = sext i32 %54 to i64
  %arrayidx56 = getelementptr [100 x [100 x i32]], [100 x [100 x i32]]* %sum_8, i32 0, i64 %conv355
  %55 = load i32, i32* %j_10, align 4
  %conv357 = sext i32 %55 to i64
  %arrayidx58 = getelementptr [100 x i32], [100 x i32]* %arrayidx56, i32 0, i64 %conv357
  %56 = load i32, i32* %arrayidx58, align 4
  %call10 = call i32 (i8*, ...) @printf(i8* %53, i32 %56)
  %57 = load i32, i32* %c_5, align 4
  %add.59 = sub i32 %57, 1
  %58 = load i32, i32* %j_10, align 4
  %cmp9 = icmp eq i32 %add.59, %58
  br i1 %cmp9, label %if.then, label %if.else

while.end54:                                      ; preds = %while.cond52
  %59 = load i32, i32* %i_9, align 4
  %add.61 = add i32 %59, 1
  store i32 %add.61, i32* %i_9, align 4
  br label %while.cond49

if.then:                                          ; preds = %while.body53
  %60 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.8, i32 0, i32 0) to i8*
  %call11 = call i32 (i8*, ...) @printf(i8* %60)
  br label %if.end

if.else:                                          ; preds = %while.body53
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %61 = load i32, i32* %j_10, align 4
  %add.60 = add i32 %61, 1
  store i32 %add.60, i32* %j_10, align 4
  br label %while.cond52
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
