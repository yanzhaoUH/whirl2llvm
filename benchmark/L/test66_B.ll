; ModuleID = 'test66.bc'

%struct.student = type { [50 x i8], i32, float }

@.str = private constant [32 x i8] c"Enter information of students:\0A\00", align 1
@.str.1 = private constant [21 x i8] c"\0AFor roll number%d,\0A\00", align 1
@.str.2 = private constant [13 x i8] c"Enter name: \00", align 1
@.str.3 = private constant [3 x i8] c"%s\00", align 1
@.str.4 = private constant [14 x i8] c"Enter marks: \00", align 1
@.str.5 = private constant [3 x i8] c"%f\00", align 1
@.str.6 = private constant [2 x i8] c"\0A\00", align 1
@.str.7 = private constant [26 x i8] c"Displaying Information:\0A\0A\00", align 1
@.str.8 = private constant [18 x i8] c"\0ARoll number: %d\0A\00", align 1
@.str.9 = private constant [7 x i8] c"Name: \00", align 1
@.str.10 = private constant [12 x i8] c"Marks: %.1f\00", align 1
@s_53 = common global [10 x %struct.student] zeroinitializer, align 4

define i32 @main() {
entry:
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_dummy110_15 = alloca i32, align 4
  %_temp_dummy111_16 = alloca i32, align 4
  %_temp_dummy112_17 = alloca i32, align 4
  %_temp_dummy11_6 = alloca i32, align 4
  %_temp_dummy12_7 = alloca i32, align 4
  %_temp_dummy13_8 = alloca i32, align 4
  %_temp_dummy14_9 = alloca i32, align 4
  %_temp_dummy15_10 = alloca i32, align 4
  %_temp_dummy16_11 = alloca i32, align 4
  %_temp_dummy17_12 = alloca i32, align 4
  %_temp_dummy18_13 = alloca i32, align 4
  %_temp_dummy19_14 = alloca i32, align 4
  %i_4 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  store i32 0, i32* %i_4, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32, i32* %i_4, align 4
  %cmp1 = icmp sle i32 %1, 1
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load i32, i32* %i_4, align 4
  %conv3 = sext i32 %2 to i64
  %arrayidx = getelementptr [10 x %struct.student], [10 x %struct.student]* @s_53, i32 0, i64 %conv3
  %3 = load i32, i32* %i_4, align 4
  %add = add i32 %3, 1
  %name = getelementptr %struct.student, %struct.student* %arrayidx, i32 0, i32 0
  %roll = getelementptr %struct.student, %struct.student* %arrayidx, i32 0, i32 1
  %marks = getelementptr %struct.student, %struct.student* %arrayidx, i32 0, i32 2
  store i32 %add, i32* %roll, align 4
  %4 = bitcast i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.1, i32 0, i32 0) to i8*
  %5 = load i32, i32* %i_4, align 4
  %conv31 = sext i32 %5 to i64
  %arrayidx2 = getelementptr [10 x %struct.student], [10 x %struct.student]* @s_53, i32 0, i64 %conv31
  %name3 = getelementptr %struct.student, %struct.student* %arrayidx2, i32 0, i32 0
  %roll4 = getelementptr %struct.student, %struct.student* %arrayidx2, i32 0, i32 1
  %marks5 = getelementptr %struct.student, %struct.student* %arrayidx2, i32 0, i32 2
  %6 = load i32, i32* %roll4, align 4
  %call2 = call i32 (i8*, ...) @printf(i8* %4, i32 %6)
  %7 = bitcast i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %7)
  %8 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.3, i32 0, i32 0) to i8*
  %9 = load i32, i32* %i_4, align 4
  %conv36 = sext i32 %9 to i64
  %arrayidx7 = getelementptr [10 x %struct.student], [10 x %struct.student]* @s_53, i32 0, i64 %conv36
  %10 = ptrtoint %struct.student* %arrayidx7 to i64
  %add.8 = add i64 %10, 0
  %11 = inttoptr i64 %add.8 to i8*
  %call4 = call i32 (i8*, ...) @scanf(i8* %8, i8* %11)
  %12 = bitcast i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.4, i32 0, i32 0) to i8*
  %call5 = call i32 (i8*, ...) @printf(i8* %12)
  %13 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.5, i32 0, i32 0) to i8*
  %14 = load i32, i32* %i_4, align 4
  %conv39 = sext i32 %14 to i64
  %arrayidx10 = getelementptr [10 x %struct.student], [10 x %struct.student]* @s_53, i32 0, i64 %conv39
  %15 = ptrtoint %struct.student* %arrayidx10 to i64
  %add.11 = add i64 %15, 56
  %16 = inttoptr i64 %add.11 to float*
  %call6 = call i32 (i8*, ...) @scanf(i8* %13, float* %16)
  %17 = bitcast i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.6, i32 0, i32 0) to i8*
  %call7 = call i32 (i8*, ...) @printf(i8* %17)
  %18 = load i32, i32* %i_4, align 4
  %add.12 = add i32 %18, 1
  store i32 %add.12, i32* %i_4, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %19 = bitcast i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.7, i32 0, i32 0) to i8*
  %call8 = call i32 (i8*, ...) @printf(i8* %19)
  store i32 0, i32* %i_4, align 4
  br label %while.cond13

while.cond13:                                     ; preds = %while.body14, %while.end
  %20 = load i32, i32* %i_4, align 4
  %cmp2 = icmp sle i32 %20, 1
  br i1 %cmp2, label %while.body14, label %while.end15

while.body14:                                     ; preds = %while.cond13
  %21 = bitcast i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.8, i32 0, i32 0) to i8*
  %22 = load i32, i32* %i_4, align 4
  %add.16 = add i32 %22, 1
  %call9 = call i32 (i8*, ...) @printf(i8* %21, i32 %add.16)
  %23 = bitcast i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.9, i32 0, i32 0) to i8*
  %call10 = call i32 (i8*, ...) @printf(i8* %23)
  %24 = load i32, i32* %i_4, align 4
  %conv317 = sext i32 %24 to i64
  %arrayidx18 = getelementptr [10 x %struct.student], [10 x %struct.student]* @s_53, i32 0, i64 %conv317
  %25 = ptrtoint %struct.student* %arrayidx18 to i64
  %add.19 = add i64 %25, 0
  %26 = inttoptr i64 %add.19 to i8*
  %call11 = call i32 @puts(i8* %26)
  %27 = bitcast i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.10, i32 0, i32 0) to i8*
  %28 = load i32, i32* %i_4, align 4
  %conv320 = sext i32 %28 to i64
  %arrayidx21 = getelementptr [10 x %struct.student], [10 x %struct.student]* @s_53, i32 0, i64 %conv320
  %name22 = getelementptr %struct.student, %struct.student* %arrayidx21, i32 0, i32 0
  %roll23 = getelementptr %struct.student, %struct.student* %arrayidx21, i32 0, i32 1
  %marks24 = getelementptr %struct.student, %struct.student* %arrayidx21, i32 0, i32 2
  %29 = load float, float* %marks24, align 4
  %convDouble = fpext float %29 to double
  %call12 = call i32 (i8*, ...) @printf(i8* %27, double %convDouble)
  %30 = bitcast i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.6, i32 0, i32 0) to i8*
  %call13 = call i32 (i8*, ...) @printf(i8* %30)
  %31 = load i32, i32* %i_4, align 4
  %add.25 = add i32 %31, 1
  store i32 %add.25, i32* %i_4, align 4
  br label %while.cond13

while.end15:                                      ; preds = %while.cond13
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

declare i32 @puts(i8*)
