; ModuleID = 'test336_O.bc'

@.str = private constant [21 x i8] c"%d %d %d %d %d %d %d\00", align 1
@.str.1 = private constant [6 x i8] c"%f %f\00", align 1
@_LIB_VERSION_58 = common global i32 0, align 4
@signgam_59 = common global i32 0, align 4

define i32 @main() {
entry:
  %.preg_F8_11_49 = alloca double
  %_temp_dummy10_34 = alloca i32, align 4
  %_temp_dummy11_35 = alloca i32, align 4
  %_temp_ehpit0_36 = alloca i32, align 4
  %c1_4 = alloca i8, align 1
  %c2_5 = alloca i8, align 1
  %c3_6 = alloca i8, align 1
  %d1_31 = alloca double, align 8
  %d2_32 = alloca double, align 8
  %d3_33 = alloca double, align 8
  %f1_28 = alloca float, align 4
  %f2_29 = alloca float, align 4
  %f3_30 = alloca float, align 4
  %i1_13 = alloca i32, align 4
  %i2_14 = alloca i32, align 4
  %i3_15 = alloca i32, align 4
  %l1_16 = alloca i64, align 8
  %l2_17 = alloca i64, align 8
  %l3_18 = alloca i64, align 8
  %ll1_19 = alloca i64, align 8
  %ll2_20 = alloca i64, align 8
  %ll3_21 = alloca i64, align 8
  %old_frame_pointer_41.addr = alloca i64, align 8
  %return_address_42.addr = alloca i64, align 8
  %s1_7 = alloca i16, align 2
  %s2_8 = alloca i16, align 2
  %s3_9 = alloca i16, align 2
  %ui1_22 = alloca i32, align 4
  %ui2_23 = alloca i32, align 4
  %ui3_24 = alloca i32, align 4
  %ul1_25 = alloca i64, align 8
  %ul2_26 = alloca i64, align 8
  %ul3_27 = alloca i64, align 8
  %us1_10 = alloca i16, align 2
  %us2_11 = alloca i16, align 2
  %us3_12 = alloca i16, align 2
  %0 = bitcast i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0, i32 2, i32 2, i32 2, i32 2, i32 2, i32 1, i32 1)
  store double 2.000000e+00, double* %.preg_F8_11_49, align 8
  %1 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = load double, double* %.preg_F8_11_49
  %3 = load double, double* %.preg_F8_11_49
  %call2 = call i32 (i8*, ...) @printf(i8* %1, double %2, double %3)
  ret i32 0
}

declare i32 @printf(i8*, ...)
