; ModuleID = 'test12.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [32 x i8] c"Enter coefficients a, b and c: \00", align 1
@.str.1 = private unnamed_addr constant [12 x i8] c"%lf %lf %lf\00", align 1
@.str.2 = private unnamed_addr constant [32 x i8] c"root1 = %.2lf and root2 = %.2lf\00", align 1
@.str.3 = private unnamed_addr constant [23 x i8] c"root1 = root2 = %.2lf;\00", align 1
@.str.4 = private unnamed_addr constant [44 x i8] c"root1 = %.2lf+%.2lfi and root2 = %.2f-%.2fi\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %a = alloca double, align 8
  %b = alloca double, align 8
  %c = alloca double, align 8
  %determinant = alloca double, align 8
  %root1 = alloca double, align 8
  %root2 = alloca double, align 8
  %realPart = alloca double, align 8
  %imaginaryPart = alloca double, align 8
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str, i32 0, i32 0))
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i32 0, i32 0), double* %a, double* %b, double* %c)
  %0 = load double, double* %b, align 8
  %1 = load double, double* %b, align 8
  %mul = fmul double %0, %1
  %2 = load double, double* %a, align 8
  %mul2 = fmul double 4.000000e+00, %2
  %3 = load double, double* %c, align 8
  %mul3 = fmul double %mul2, %3
  %sub = fsub double %mul, %mul3
  store double %sub, double* %determinant, align 8
  %4 = load double, double* %determinant, align 8
  %cmp = fcmp ogt double %4, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load double, double* %b, align 8
  %sub4 = fsub double -0.000000e+00, %5
  %6 = load double, double* %a, align 8
  %call5 = call double @sqrt(double %6) #3
  %add = fadd double %sub4, %call5
  %7 = load double, double* %a, align 8
  %mul6 = fmul double 2.000000e+00, %7
  %div = fdiv double %add, %mul6
  store double %div, double* %root1, align 8
  %8 = load double, double* %b, align 8
  %sub7 = fsub double -0.000000e+00, %8
  %9 = load double, double* %determinant, align 8
  %call8 = call double @sqrt(double %9) #3
  %sub9 = fsub double %sub7, %call8
  %10 = load double, double* %a, align 8
  %mul10 = fmul double 2.000000e+00, %10
  %div11 = fdiv double %sub9, %mul10
  store double %div11, double* %root2, align 8
  %11 = load double, double* %root1, align 8
  %12 = load double, double* %root2, align 8
  %call12 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.2, i32 0, i32 0), double %11, double %12)
  br label %if.end.28

if.else:                                          ; preds = %entry
  %13 = load double, double* %determinant, align 8
  %cmp13 = fcmp oeq double %13, 0.000000e+00
  br i1 %cmp13, label %if.then.14, label %if.else.19

if.then.14:                                       ; preds = %if.else
  %14 = load double, double* %b, align 8
  %sub15 = fsub double -0.000000e+00, %14
  %15 = load double, double* %a, align 8
  %mul16 = fmul double 2.000000e+00, %15
  %div17 = fdiv double %sub15, %mul16
  store double %div17, double* %root2, align 8
  store double %div17, double* %root1, align 8
  %16 = load double, double* %root1, align 8
  %call18 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.3, i32 0, i32 0), double %16)
  br label %if.end

if.else.19:                                       ; preds = %if.else
  %17 = load double, double* %b, align 8
  %sub20 = fsub double -0.000000e+00, %17
  %18 = load double, double* %a, align 8
  %mul21 = fmul double 2.000000e+00, %18
  %div22 = fdiv double %sub20, %mul21
  store double %div22, double* %realPart, align 8
  %19 = load double, double* %determinant, align 8
  %sub23 = fsub double -0.000000e+00, %19
  %call24 = call double @sqrt(double %sub23) #3
  %20 = load double, double* %a, align 8
  %mul25 = fmul double 2.000000e+00, %20
  %div26 = fdiv double %call24, %mul25
  store double %div26, double* %imaginaryPart, align 8
  %21 = load double, double* %realPart, align 8
  %22 = load double, double* %imaginaryPart, align 8
  %23 = load double, double* %realPart, align 8
  %24 = load double, double* %imaginaryPart, align 8
  %call27 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([44 x i8], [44 x i8]* @.str.4, i32 0, i32 0), double %21, double %22, double %23, double %24)
  br label %if.end

if.end:                                           ; preds = %if.else.19, %if.then.14
  br label %if.end.28

if.end.28:                                        ; preds = %if.end, %if.then
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @__isoc99_scanf(i8*, ...) #1

; Function Attrs: nounwind
declare double @sqrt(double) #2

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
