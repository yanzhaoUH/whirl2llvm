; ModuleID = 'test3.bc'

@.str = private constant [20 x i8] c"Enter two numbers: \00", align 1
@.str.1 = private constant [6 x i8] c"%f %f\00", align 1
@.str.2 = private constant [16 x i8] c"Product = %.2lf\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %firstNumber_4 = alloca float, align 4
  %productOfTwoNumbers_6 = alloca float, align 4
  %secondNumber_5 = alloca float, align 4
  %0 = bitcast i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast float* %firstNumber_4 to float*
  %3 = bitcast float* %secondNumber_5 to float*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, float* %2, float* %3)
  %4 = load float, float* %firstNumber_4, align 4
  %5 = load float, float* %secondNumber_5, align 4
  %mul = fmul float %4, %5
  store float %mul, float* %productOfTwoNumbers_6, align 4
  %6 = bitcast i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.2, i32 0, i32 0) to i8*
  %7 = load float, float* %productOfTwoNumbers_6, align 4
  %convDouble = fpext float %7 to double
  %call3 = call i32 (i8*, ...) @printf(i8* %6, double %convDouble)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
