; ModuleID = 'test66.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.student = type { [50 x i8], i32, float }

@.str = private unnamed_addr constant [32 x i8] c"Enter information of students:\0A\00", align 1
@s = common global [10 x %struct.student] zeroinitializer, align 16
@.str.1 = private unnamed_addr constant [21 x i8] c"\0AFor roll number%d,\0A\00", align 1
@.str.2 = private unnamed_addr constant [13 x i8] c"Enter name: \00", align 1
@.str.3 = private unnamed_addr constant [3 x i8] c"%s\00", align 1
@.str.4 = private unnamed_addr constant [14 x i8] c"Enter marks: \00", align 1
@.str.5 = private unnamed_addr constant [3 x i8] c"%f\00", align 1
@.str.6 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.7 = private unnamed_addr constant [26 x i8] c"Displaying Information:\0A\0A\00", align 1
@.str.8 = private unnamed_addr constant [18 x i8] c"\0ARoll number: %d\0A\00", align 1
@.str.9 = private unnamed_addr constant [7 x i8] c"Name: \00", align 1
@.str.10 = private unnamed_addr constant [12 x i8] c"Marks: %.1f\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %i = alloca i32, align 4
  store i32 0, i32* %retval
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %add = add nsw i32 %1, 1
  %2 = load i32, i32* %i, align 4
  %idxprom = sext i32 %2 to i64
  %arrayidx = getelementptr inbounds [10 x %struct.student], [10 x %struct.student]* @s, i32 0, i64 %idxprom
  %roll = getelementptr inbounds %struct.student, %struct.student* %arrayidx, i32 0, i32 1
  store i32 %add, i32* %roll, align 4
  %3 = load i32, i32* %i, align 4
  %idxprom1 = sext i32 %3 to i64
  %arrayidx2 = getelementptr inbounds [10 x %struct.student], [10 x %struct.student]* @s, i32 0, i64 %idxprom1
  %roll3 = getelementptr inbounds %struct.student, %struct.student* %arrayidx2, i32 0, i32 1
  %4 = load i32, i32* %roll3, align 4
  %call4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.1, i32 0, i32 0), i32 %4)
  %call5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.2, i32 0, i32 0))
  %5 = load i32, i32* %i, align 4
  %idxprom6 = sext i32 %5 to i64
  %arrayidx7 = getelementptr inbounds [10 x %struct.student], [10 x %struct.student]* @s, i32 0, i64 %idxprom6
  %name = getelementptr inbounds %struct.student, %struct.student* %arrayidx7, i32 0, i32 0
  %arraydecay = getelementptr inbounds [50 x i8], [50 x i8]* %name, i32 0, i32 0
  %call8 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.3, i32 0, i32 0), i8* %arraydecay)
  %call9 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.4, i32 0, i32 0))
  %6 = load i32, i32* %i, align 4
  %idxprom10 = sext i32 %6 to i64
  %arrayidx11 = getelementptr inbounds [10 x %struct.student], [10 x %struct.student]* @s, i32 0, i64 %idxprom10
  %marks = getelementptr inbounds %struct.student, %struct.student* %arrayidx11, i32 0, i32 2
  %call12 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.5, i32 0, i32 0), float* %marks)
  %call13 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.6, i32 0, i32 0))
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call14 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.7, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond.15

for.cond.15:                                      ; preds = %for.inc.31, %for.end
  %8 = load i32, i32* %i, align 4
  %cmp16 = icmp slt i32 %8, 2
  br i1 %cmp16, label %for.body.17, label %for.end.33

for.body.17:                                      ; preds = %for.cond.15
  %9 = load i32, i32* %i, align 4
  %add18 = add nsw i32 %9, 1
  %call19 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.8, i32 0, i32 0), i32 %add18)
  %call20 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.9, i32 0, i32 0))
  %10 = load i32, i32* %i, align 4
  %idxprom21 = sext i32 %10 to i64
  %arrayidx22 = getelementptr inbounds [10 x %struct.student], [10 x %struct.student]* @s, i32 0, i64 %idxprom21
  %name23 = getelementptr inbounds %struct.student, %struct.student* %arrayidx22, i32 0, i32 0
  %arraydecay24 = getelementptr inbounds [50 x i8], [50 x i8]* %name23, i32 0, i32 0
  %call25 = call i32 @puts(i8* %arraydecay24)
  %11 = load i32, i32* %i, align 4
  %idxprom26 = sext i32 %11 to i64
  %arrayidx27 = getelementptr inbounds [10 x %struct.student], [10 x %struct.student]* @s, i32 0, i64 %idxprom26
  %marks28 = getelementptr inbounds %struct.student, %struct.student* %arrayidx27, i32 0, i32 2
  %12 = load float, float* %marks28, align 4
  %conv = fpext float %12 to double
  %call29 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.10, i32 0, i32 0), double %conv)
  %call30 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.6, i32 0, i32 0))
  br label %for.inc.31

for.inc.31:                                       ; preds = %for.body.17
  %13 = load i32, i32* %i, align 4
  %inc32 = add nsw i32 %13, 1
  store i32 %inc32, i32* %i, align 4
  br label %for.cond.15

for.end.33:                                       ; preds = %for.cond.15
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @__isoc99_scanf(i8*, ...) #1

declare i32 @puts(i8*) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
