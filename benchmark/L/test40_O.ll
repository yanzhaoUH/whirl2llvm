; ModuleID = 'test40_O.bc'

@.str = private constant [24 x i8] c"Enter a binary number: \00", align 1
@.str.1 = private constant [5 x i8] c"%lld\00", align 1
@.str.2 = private constant [31 x i8] c"%lld in binary = %d in decimal\00", align 1
@_LIB_VERSION_59 = common global i32 0, align 4
@signgam_60 = common global i32 0, align 4

define i32 @main() {
entry:
  %.preg_F8_11_50 = alloca double
  %.preg_I4_4_54 = alloca i32
  %.preg_I8_5_52 = alloca i64
  %.preg_F8_11_58 = alloca double
  %.preg_I4_4_56 = alloca i32
  %.preg_I4_4_55 = alloca i32
  %.preg_I8_5_53 = alloca i64
  %.preg_I8_5_57 = alloca i64
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_dummy11_6 = alloca i32, align 4
  %_temp_dummy12_7 = alloca i32, align 4
  %_temp_ehpit0_16 = alloca i32, align 4
  %decimalNumber_12 = alloca i32, align 4
  %i_13 = alloca i32, align 4
  %n_11.addr = alloca i64, align 8
  %n_15 = alloca i64, align 8
  %n_4 = alloca i64, align 8
  %old_frame_pointer_21.addr = alloca i64, align 8
  %remainder_14 = alloca i32, align 4
  %return_address_22.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i64* %n_4 to i64*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i64* %2)
  %3 = load i64, i64* %n_4, align 8
  store i64 %3, i64* %.preg_I8_5_57, align 8
  %4 = load i64, i64* %.preg_I8_5_57
  store i64 %4, i64* %.preg_I8_5_53, align 8
  %5 = load i64, i64* %.preg_I8_5_57
  %conv3 = zext i32 0 to i64
  %cmp1 = icmp ne i64 %5, %conv3
  br i1 %cmp1, label %tb, label %L2818

tb:                                               ; preds = %entry
  store i32 0, i32* %.preg_I4_4_55, align 8
  store i32 0, i32* %.preg_I4_4_56, align 8
  store double 2.000000e+00, double* %.preg_F8_11_58, align 8
  br label %L2306

L2818:                                            ; preds = %entry
  store i32 0, i32* %.preg_I4_4_56, align 8
  br label %L1794

L2306:                                            ; preds = %L2306, %tb
  %6 = load i64, i64* %.preg_I8_5_53
  %conv31 = zext i32 10 to i64
  %lowPart_5 = alloca i64, align 8
  %div = sdiv i64 %6, %conv31
  %srem = srem i64 %6, %conv31
  store i64 %div, i64* %lowPart_5, align 8
  store i64 %srem, i64* %.preg_I8_5_52, align 8
  %7 = load i64, i64* %.preg_I8_5_52
  %conv = trunc i64 %7 to i32
  store i32 %conv, i32* %.preg_I4_4_54, align 8
  %8 = load i64, i64* %lowPart_5
  store i64 %8, i64* %.preg_I8_5_53, align 8
  %9 = load double, double* %.preg_F8_11_58
  %10 = load i32, i32* %.preg_I4_4_55
  %conv9 = sitofp i32 %10 to double
  %call3 = call double @pow(double %9, double %conv9)
  store double %call3, double* %.preg_F8_11_50, align 8
  %11 = load i32, i32* %.preg_I4_4_56
  %conv92 = sitofp i32 %11 to double
  %12 = load i32, i32* %.preg_I4_4_54
  %conv93 = sitofp i32 %12 to double
  %13 = load double, double* %.preg_F8_11_50
  %mul = fmul double %conv93, %13
  %add = fadd double %conv92, %mul
  %fp2int = fptosi double %add to i32
  store i32 %fp2int, i32* %.preg_I4_4_56, align 8
  %14 = load i32, i32* %.preg_I4_4_55
  %add.4 = add i32 %14, 1
  store i32 %add.4, i32* %.preg_I4_4_55, align 8
  %15 = load i64, i64* %.preg_I8_5_53
  %conv35 = zext i32 0 to i64
  %cmp2 = icmp ne i64 %15, %conv35
  br i1 %cmp2, label %L2306, label %fb

fb:                                               ; preds = %L2306
  %16 = load i64, i64* %n_4, align 8
  store i64 %16, i64* %.preg_I8_5_57, align 8
  br label %L1794

L1794:                                            ; preds = %fb, %L2818
  %17 = bitcast i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.2, i32 0, i32 0) to i8*
  %18 = load i64, i64* %.preg_I8_5_57
  %19 = load i32, i32* %.preg_I4_4_56
  %call4 = call i32 (i8*, ...) @printf(i8* %17, i64 %18, i32 %19)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define i32 @_Z22ConvertBinaryToDecimalx(i64 %n_4) {
entry:
  %.preg_F8_11_49 = alloca double
  %.preg_I4_4_53 = alloca i32
  %.preg_I8_5_51 = alloca i64
  %.preg_F8_11_56 = alloca double
  %.preg_I4_4_55 = alloca i32
  %.preg_I4_4_54 = alloca i32
  %.preg_I8_5_52 = alloca i64
  %decimalNumber_5 = alloca i32, align 4
  %i_6 = alloca i32, align 4
  %n_4.addr = alloca i64, align 8
  %old_frame_pointer_12.addr = alloca i64, align 8
  %remainder_7 = alloca i32, align 4
  %return_address_13.addr = alloca i64, align 8
  store i64 %n_4, i64* %n_4.addr, align 8
  store i64 %n_4, i64* %.preg_I8_5_52, align 8
  %0 = load i64, i64* %.preg_I8_5_52
  %conv3 = zext i32 0 to i64
  %cmp3 = icmp ne i64 %0, %conv3
  br i1 %cmp3, label %tb, label %L2818

tb:                                               ; preds = %entry
  store i32 0, i32* %.preg_I4_4_54, align 8
  store i32 0, i32* %.preg_I4_4_55, align 8
  store double 2.000000e+00, double* %.preg_F8_11_56, align 8
  br label %L2306

L2818:                                            ; preds = %entry
  store i32 0, i32* %.preg_I4_4_55, align 8
  br label %L1794

L2306:                                            ; preds = %L2306, %tb
  %1 = load i64, i64* %.preg_I8_5_52
  %conv31 = zext i32 10 to i64
  %lowPart_5 = alloca i64, align 8
  %div = sdiv i64 %1, %conv31
  %srem = srem i64 %1, %conv31
  store i64 %div, i64* %lowPart_5, align 8
  store i64 %srem, i64* %.preg_I8_5_51, align 8
  %2 = load i64, i64* %.preg_I8_5_51
  %conv = trunc i64 %2 to i32
  store i32 %conv, i32* %.preg_I4_4_53, align 8
  %3 = load i64, i64* %lowPart_5
  store i64 %3, i64* %.preg_I8_5_52, align 8
  %4 = load double, double* %.preg_F8_11_56
  %5 = load i32, i32* %.preg_I4_4_54
  %conv9 = sitofp i32 %5 to double
  %call5 = call double @pow(double %4, double %conv9)
  store double %call5, double* %.preg_F8_11_49, align 8
  %6 = load i32, i32* %.preg_I4_4_55
  %conv92 = sitofp i32 %6 to double
  %7 = load i32, i32* %.preg_I4_4_53
  %conv93 = sitofp i32 %7 to double
  %8 = load double, double* %.preg_F8_11_49
  %mul = fmul double %conv93, %8
  %add = fadd double %conv92, %mul
  %fp2int = fptosi double %add to i32
  store i32 %fp2int, i32* %.preg_I4_4_55, align 8
  %9 = load i32, i32* %.preg_I4_4_54
  %add.4 = add i32 %9, 1
  store i32 %add.4, i32* %.preg_I4_4_54, align 8
  %10 = load i64, i64* %.preg_I8_5_52
  %conv35 = zext i32 0 to i64
  %cmp4 = icmp ne i64 %10, %conv35
  br i1 %cmp4, label %L2306, label %fb

fb:                                               ; preds = %L2306
  br label %L1794

L1794:                                            ; preds = %fb, %L2818
  %11 = load i32, i32* %.preg_I4_4_55
  ret i32 %11
}

declare double @pow(double, double)
