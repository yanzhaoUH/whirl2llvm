; ModuleID = 'test44_O.bc'

%struct._temp_dummy15 = type {}

@.str = private constant [20 x i8] c"Enter base number: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [39 x i8] c"Enter power number(positive integer): \00", align 1
@.str.3 = private constant [11 x i8] c"%d^%d = %d\00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_49 = alloca i32
  %.preg_I4_4_50 = alloca i32
  %.preg_I4_4_55 = alloca i32
  %.preg_I4_4_53 = alloca i32
  %.preg_I4_4_56 = alloca i32
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %_temp_dummy13_9 = alloca i32, align 4
  %_temp_dummy14_10 = alloca i32, align 4
  %_temp_dummy15_16 = alloca i32, align 4
  %_temp_dummy15_19 = alloca %struct._temp_dummy15, align 4
  %_temp_ehpit0_20 = alloca i32, align 4
  %base_14.addr = alloca i32, align 4
  %base_17 = alloca i32, align 4
  %base_4 = alloca i32, align 4
  %exp_15.addr = alloca i32, align 4
  %exp_18 = alloca i32, align 4
  %exp_5 = alloca i32, align 4
  %old_frame_pointer_25.addr = alloca i64, align 8
  %return_address_26.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %base_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = bitcast i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %3)
  %4 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %5 = bitcast i32* %exp_5 to i32*
  %call4 = call i32 (i8*, ...) @scanf(i8* %4, i32* %5)
  %6 = load i32, i32* %base_4, align 4
  store i32 %6, i32* %.preg_I4_4_56, align 8
  %7 = load i32, i32* %.preg_I4_4_56
  store i32 %7, i32* %.preg_I4_4_53, align 8
  %8 = load i32, i32* %exp_5, align 4
  store i32 %8, i32* %.preg_I4_4_55, align 8
  %9 = load i32, i32* %.preg_I4_4_55
  %cmp1 = icmp ne i32 %9, 1
  br i1 %cmp1, label %tb, label %L1794

tb:                                               ; preds = %entry
  %10 = load i32, i32* %.preg_I4_4_53
  %11 = load i32, i32* %.preg_I4_4_55
  %add = add i32 %11, -1
  %call5 = call i32 @_Z5powerii(i32 %10, i32 %add)
  store i32 %call5, i32* %.preg_I4_4_50, align 8
  %12 = load i32, i32* %.preg_I4_4_50
  %13 = load i32, i32* %.preg_I4_4_53
  %mul = mul i32 %12, %13
  store i32 %mul, i32* %.preg_I4_4_49, align 8
  %14 = load i32, i32* %exp_5, align 4
  store i32 %14, i32* %.preg_I4_4_55, align 8
  %15 = load i32, i32* %base_4, align 4
  store i32 %15, i32* %.preg_I4_4_56, align 8
  br label %L258

L1794:                                            ; preds = %entry
  %16 = load i32, i32* %.preg_I4_4_53
  store i32 %16, i32* %.preg_I4_4_49, align 8
  br label %L258

L258:                                             ; preds = %L1794, %tb
  %17 = bitcast i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.3, i32 0, i32 0) to i8*
  %18 = load i32, i32* %.preg_I4_4_56
  %19 = load i32, i32* %.preg_I4_4_55
  %20 = load i32, i32* %.preg_I4_4_49
  %call6 = call i32 (i8*, ...) @printf(i8* %17, i32 %18, i32 %19, i32 %20)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define i32 @_Z5powerii(i32 %base_4, i32 %exp_5) {
entry:
  %.preg_I4_4_49 = alloca i32
  %.preg_I4_4_51 = alloca i32
  %.preg_I4_4_50 = alloca i32
  %_temp_dummy15_6 = alloca i32, align 4
  %_temp_ehpit1_7 = alloca i32, align 4
  %base_4.addr = alloca i32, align 4
  %exp_5.addr = alloca i32, align 4
  %old_frame_pointer_12.addr = alloca i64, align 8
  %return_address_13.addr = alloca i64, align 8
  store i32 %base_4, i32* %base_4.addr, align 4
  store i32 %exp_5, i32* %exp_5.addr, align 4
  store i32 %base_4, i32* %.preg_I4_4_50, align 8
  store i32 %exp_5, i32* %.preg_I4_4_51, align 8
  %0 = load i32, i32* %.preg_I4_4_51
  %cmp2 = icmp ne i32 %0, 1
  br i1 %cmp2, label %tb, label %L770

tb:                                               ; preds = %entry
  %1 = load i32, i32* %.preg_I4_4_50
  %2 = load i32, i32* %.preg_I4_4_51
  %add = add i32 %2, -1
  %call7 = call i32 @_Z5powerii(i32 %1, i32 %add)
  store i32 %call7, i32* %.preg_I4_4_49, align 8
  %3 = load i32, i32* %.preg_I4_4_50
  %4 = load i32, i32* %.preg_I4_4_49
  %mul = mul i32 %3, %4
  ret i32 %mul

L770:                                             ; preds = %entry
  %5 = load i32, i32* %.preg_I4_4_50
  ret i32 %5
}
