; ModuleID = 'test19.bc'

@.str = private constant [28 x i8] c"Enter the number of terms: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [27 x i8] c"Fibonacci Series: %d, %d, \00", align 1
@.str.3 = private constant [5 x i8] c"%d, \00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_9 = alloca i32, align 4
  %_temp_dummy11_10 = alloca i32, align 4
  %_temp_dummy12_11 = alloca i32, align 4
  %_temp_dummy13_12 = alloca i32, align 4
  %i_4 = alloca i32, align 4
  %n_5 = alloca i32, align 4
  %nextTerm_8 = alloca i32, align 4
  %t1_6 = alloca i32, align 4
  %t2_7 = alloca i32, align 4
  store i32 0, i32* %t1_6, align 4
  store i32 1, i32* %t2_7, align 4
  store i32 0, i32* %nextTerm_8, align 4
  %0 = bitcast i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_5 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = bitcast i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.2, i32 0, i32 0) to i8*
  %4 = load i32, i32* %t1_6, align 4
  %5 = load i32, i32* %t2_7, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %3, i32 %4, i32 %5)
  store i32 3, i32* %i_4, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %6 = load i32, i32* %i_4, align 4
  %7 = load i32, i32* %n_5, align 4
  %cmp1 = icmp sle i32 %6, %7
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %8 = load i32, i32* %t1_6, align 4
  %9 = load i32, i32* %t2_7, align 4
  %add = add i32 %8, %9
  store i32 %add, i32* %nextTerm_8, align 4
  %10 = load i32, i32* %t2_7, align 4
  store i32 %10, i32* %t1_6, align 4
  %11 = load i32, i32* %nextTerm_8, align 4
  store i32 %11, i32* %t2_7, align 4
  %12 = bitcast i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.3, i32 0, i32 0) to i8*
  %13 = load i32, i32* %nextTerm_8, align 4
  %call4 = call i32 (i8*, ...) @printf(i8* %12, i32 %13)
  %14 = load i32, i32* %i_4, align 4
  %add.1 = add i32 %14, 1
  store i32 %add.1, i32* %i_4, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
