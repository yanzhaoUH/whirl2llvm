; ModuleID = 'test45.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [28 x i8] c"Enter the numbers of data: \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.3 = private unnamed_addr constant [25 x i8] c"Enter the number again: \00", align 1
@.str.4 = private unnamed_addr constant [19 x i8] c"%d. Enter number: \00", align 1
@.str.5 = private unnamed_addr constant [15 x i8] c"Average = %.2f\00", align 1
@str = private unnamed_addr constant [45 x i8] c"Error! number should in range of (1 to 100).\00"

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %n = alloca i32, align 4
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start(i64 4, i8* %0) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %n) #1
  %1 = load i32, i32* %n, align 4, !tbaa !1
  %.off.27 = add i32 %1, -1
  %2 = icmp ugt i32 %.off.27, 99
  br i1 %2, label %while.body.preheader, label %for.cond.preheader

while.body.preheader:                             ; preds = %entry
  br label %while.body

for.cond.preheader.loopexit:                      ; preds = %while.body
  %.lcssa36 = phi i32 [ %4, %while.body ]
  br label %for.cond.preheader

for.cond.preheader:                               ; preds = %for.cond.preheader.loopexit, %entry
  %3 = phi i32 [ %1, %entry ], [ %.lcssa36, %for.cond.preheader.loopexit ]
  %cmp6.23 = icmp sgt i32 %3, 0
  br i1 %cmp6.23, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %for.cond.preheader
  br label %for.body

while.body:                                       ; preds = %while.body.preheader, %while.body
  %puts = call i32 @puts(i8* getelementptr inbounds ([45 x i8], [45 x i8]* @str, i64 0, i64 0))
  %call4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.3, i64 0, i64 0)) #1
  %call5 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %n) #1
  %4 = load i32, i32* %n, align 4, !tbaa !1
  %.off = add i32 %4, -1
  %5 = icmp ugt i32 %.off, 99
  br i1 %5, label %while.body, label %for.cond.preheader.loopexit

for.body:                                         ; preds = %for.body.preheader, %for.body
  %i.025 = phi i32 [ %add, %for.body ], [ 0, %for.body.preheader ]
  %sum.024 = phi float [ %add10, %for.body ], [ 0.000000e+00, %for.body.preheader ]
  %add = add nuw nsw i32 %i.025, 1
  %call7 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.4, i64 0, i64 0), i32 %add) #1
  %conv = sitofp i32 %i.025 to float
  %add10 = fadd float %sum.024, %conv
  %6 = load i32, i32* %n, align 4, !tbaa !1
  %cmp6 = icmp slt i32 %add, %6
  br i1 %cmp6, label %for.body, label %for.end.loopexit

for.end.loopexit:                                 ; preds = %for.body
  %.lcssa35 = phi i32 [ %6, %for.body ]
  %add10.lcssa = phi float [ %add10, %for.body ]
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %for.cond.preheader
  %.lcssa = phi i32 [ %3, %for.cond.preheader ], [ %.lcssa35, %for.end.loopexit ]
  %sum.0.lcssa = phi float [ 0.000000e+00, %for.cond.preheader ], [ %add10.lcssa, %for.end.loopexit ]
  %conv11 = sitofp i32 %.lcssa to float
  %div = fdiv float %sum.0.lcssa, %conv11
  %conv12 = fpext float %div to double
  %call13 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.5, i64 0, i64 0), double %conv12) #1
  call void @llvm.lifetime.end(i64 4, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @puts(i8* nocapture readonly) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
