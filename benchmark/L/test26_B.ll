; ModuleID = 'test26.bc'

@.str = private constant [19 x i8] c"Enter an integer: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [20 x i8] c"%d is a palindrome.\00", align 1
@.str.3 = private constant [24 x i8] c"%d is not a palindrome.\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_8 = alloca i32, align 4
  %_temp_dummy11_9 = alloca i32, align 4
  %_temp_dummy12_10 = alloca i32, align 4
  %_temp_dummy13_11 = alloca i32, align 4
  %n_4 = alloca i32, align 4
  %originalInteger_7 = alloca i32, align 4
  %remainder_6 = alloca i32, align 4
  %reversedInteger_5 = alloca i32, align 4
  store i32 0, i32* %reversedInteger_5, align 4
  %0 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = load i32, i32* %n_4, align 4
  store i32 %3, i32* %originalInteger_7, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %4 = load i32, i32* %n_4, align 4
  %cmp1 = icmp ne i32 %4, 0
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = load i32, i32* %n_4, align 4
  %srem = srem i32 %5, 10
  store i32 %srem, i32* %remainder_6, align 4
  %6 = load i32, i32* %reversedInteger_5, align 4
  %mul = mul i32 %6, 10
  %7 = load i32, i32* %remainder_6, align 4
  %add = add i32 %mul, %7
  store i32 %add, i32* %reversedInteger_5, align 4
  %8 = load i32, i32* %n_4, align 4
  %div = sdiv i32 %8, 10
  store i32 %div, i32* %n_4, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %9 = load i32, i32* %originalInteger_7, align 4
  %10 = load i32, i32* %reversedInteger_5, align 4
  %cmp2 = icmp eq i32 %9, %10
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %while.end
  %11 = bitcast i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.2, i32 0, i32 0) to i8*
  %12 = load i32, i32* %originalInteger_7, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %11, i32 %12)
  br label %if.end

if.else:                                          ; preds = %while.end
  %13 = bitcast i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.3, i32 0, i32 0) to i8*
  %14 = load i32, i32* %originalInteger_7, align 4
  %call4 = call i32 (i8*, ...) @printf(i8* %13, i32 %14)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
