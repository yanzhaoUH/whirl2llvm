; ModuleID = 'test334.bc'

@.str = private constant [20 x i8] c"%c %d %ld %f %lf %u\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_11 = alloca i32, align 4
  %bi_6 = alloca i32, align 4
  %cf_9 = alloca float, align 4
  %ch_4 = alloca i8, align 1
  %dd_10 = alloca double, align 8
  %eui_8 = alloca i32, align 4
  %longi_7 = alloca i64, align 8
  %shorti_5 = alloca i16, align 2
  %conv = trunc i32 97 to i8
  store i8 %conv, i8* %ch_4, align 1
  %conv1 = trunc i32 2 to i16
  store i16 %conv1, i16* %shorti_5, align 2
  store i32 4, i32* %bi_6, align 4
  store i64 100, i64* %longi_7, align 8
  store i32 4, i32* %eui_8, align 4
  store float 1.000000e+00, float* %cf_9, align 4
  store double 2.000000e+00, double* %dd_10, align 8
  %0 = bitcast i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0) to i8*
  %1 = load i8, i8* %ch_4, align 1
  %conv3 = sext i8 %1 to i32
  %2 = load i32, i32* %bi_6, align 4
  %3 = load i64, i64* %longi_7, align 8
  %4 = load float, float* %cf_9, align 4
  %convDouble = fpext float %4 to double
  %5 = load double, double* %dd_10, align 8
  %6 = load i32, i32* %eui_8, align 4
  %call1 = call i32 (i8*, ...) @printf(i8* %0, i32 %conv3, i32 %2, i64 %3, double %convDouble, double %5, i32 %6)
  ret i32 0
}

declare i32 @printf(i8*, ...)
