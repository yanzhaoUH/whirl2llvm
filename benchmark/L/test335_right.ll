; ModuleID = 'test335.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.sTest = type { i32, float }

@.str = private unnamed_addr constant [4 x i8] c"%d\0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %a = alloca [4 x i32], align 16
  %s1 = alloca %struct.sTest, align 4
  %p = alloca i32*, align 8
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* %a, i32 0, i64 2
  store i32 26, i32* %arrayidx, align 4
  %arrayidx1 = getelementptr inbounds [4 x i32], [4 x i32]* %a, i32 0, i64 2
  %0 = load i32, i32* %arrayidx1, align 4
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0), i32 %0)
  %mI = getelementptr inbounds %struct.sTest, %struct.sTest* %s1, i32 0, i32 0
  store i32 10, i32* %mI, align 4
  %mI2 = getelementptr inbounds %struct.sTest, %struct.sTest* %s1, i32 0, i32 0
  %1 = load i32, i32* %mI2, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0), i32 %1)
  %arraydecay = getelementptr inbounds [4 x i32], [4 x i32]* %a, i32 0, i32 0
  store i32* %arraydecay, i32** %p, align 8
  %2 = load i32*, i32** %p, align 8
  store i32 66, i32* %2, align 4
  %arrayidx4 = getelementptr inbounds [4 x i32], [4 x i32]* %a, i32 0, i64 0
  %3 = load i32, i32* %arrayidx4, align 4
  %call5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0), i32 %3)
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
