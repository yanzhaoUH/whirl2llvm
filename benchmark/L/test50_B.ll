; ModuleID = 'test50.bc'

@.str = private constant [35 x i8] c"Enter rows and columns of matrix: \00", align 1
@.str.1 = private constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private constant [28 x i8] c"\0AEnter elements of matrix:\0A\00", align 1
@.str.3 = private constant [22 x i8] c"Enter element a%d%d: \00", align 1
@.str.4 = private constant [19 x i8] c"\0AEntered Matrix: \0A\00", align 1
@.str.5 = private constant [5 x i8] c"%d  \00", align 1
@.str.6 = private constant [3 x i8] c"\0A\0A\00", align 1
@.str.7 = private constant [23 x i8] c"\0ATranspose of Matrix:\0A\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_10 = alloca i32, align 4
  %_temp_dummy11_11 = alloca i32, align 4
  %_temp_dummy12_12 = alloca i32, align 4
  %_temp_dummy13_13 = alloca i32, align 4
  %_temp_dummy14_14 = alloca i32, align 4
  %_temp_dummy15_15 = alloca i32, align 4
  %_temp_dummy16_16 = alloca i32, align 4
  %_temp_dummy17_17 = alloca i32, align 4
  %_temp_dummy18_18 = alloca i32, align 4
  %_temp_dummy19_19 = alloca i32, align 4
  %a_4 = alloca [10 x [10 x i32]], align 4
  %c_7 = alloca i32, align 4
  %i_8 = alloca i32, align 4
  %j_9 = alloca i32, align 4
  %r_6 = alloca i32, align 4
  %transpose_5 = alloca [10 x [10 x i32]], align 4
  %0 = bitcast i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %r_6 to i32*
  %3 = bitcast i32* %c_7 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2, i32* %3)
  %4 = bitcast i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %4)
  store i32 0, i32* %i_8, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.end3, %entry
  %5 = load i32, i32* %i_8, align 4
  %6 = load i32, i32* %r_6, align 4
  %cmp1 = icmp slt i32 %5, %6
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  store i32 0, i32* %j_9, align 4
  br label %while.cond1

while.end:                                        ; preds = %while.cond
  %7 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.4, i32 0, i32 0) to i8*
  %call5 = call i32 (i8*, ...) @printf(i8* %7)
  store i32 0, i32* %i_8, align 4
  br label %while.cond11

while.cond1:                                      ; preds = %while.body2, %while.body
  %8 = load i32, i32* %j_9, align 4
  %9 = load i32, i32* %c_7, align 4
  %cmp2 = icmp slt i32 %8, %9
  br i1 %cmp2, label %while.body2, label %while.end3

while.body2:                                      ; preds = %while.cond1
  %10 = bitcast i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.3, i32 0, i32 0) to i8*
  %11 = load i32, i32* %i_8, align 4
  %add = add i32 %11, 1
  %12 = load i32, i32* %j_9, align 4
  %add.4 = add i32 %12, 1
  %call4 = call i32 (i8*, ...) @printf(i8* %10, i32 %add, i32 %add.4)
  %13 = load i32, i32* %i_8, align 4
  %conv3 = sext i32 %13 to i64
  %arrayidx = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %a_4, i32 0, i64 %conv3
  %14 = load i32, i32* %j_9, align 4
  %conv35 = sext i32 %14 to i64
  %arrayidx6 = getelementptr [10 x i32], [10 x i32]* %arrayidx, i32 0, i64 %conv35
  %15 = load i32, i32* %i_8, align 4
  %add.7 = add i32 %15, 1
  %16 = load i32, i32* %j_9, align 4
  %add.8 = add i32 %16, 1
  %mul = mul i32 %add.7, %add.8
  store i32 %mul, i32* %arrayidx6, align 4
  %17 = load i32, i32* %j_9, align 4
  %add.9 = add i32 %17, 1
  store i32 %add.9, i32* %j_9, align 4
  br label %while.cond1

while.end3:                                       ; preds = %while.cond1
  %18 = load i32, i32* %i_8, align 4
  %add.10 = add i32 %18, 1
  store i32 %add.10, i32* %i_8, align 4
  br label %while.cond

while.cond11:                                     ; preds = %while.end16, %while.end
  %19 = load i32, i32* %i_8, align 4
  %20 = load i32, i32* %r_6, align 4
  %cmp3 = icmp slt i32 %19, %20
  br i1 %cmp3, label %while.body12, label %while.end13

while.body12:                                     ; preds = %while.cond11
  store i32 0, i32* %j_9, align 4
  br label %while.cond14

while.end13:                                      ; preds = %while.cond11
  store i32 0, i32* %i_8, align 4
  br label %while.cond24

while.cond14:                                     ; preds = %if.end, %while.body12
  %21 = load i32, i32* %j_9, align 4
  %22 = load i32, i32* %c_7, align 4
  %cmp4 = icmp slt i32 %21, %22
  br i1 %cmp4, label %while.body15, label %while.end16

while.body15:                                     ; preds = %while.cond14
  %23 = bitcast i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.5, i32 0, i32 0) to i8*
  %24 = load i32, i32* %i_8, align 4
  %conv317 = sext i32 %24 to i64
  %arrayidx18 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %a_4, i32 0, i64 %conv317
  %25 = load i32, i32* %j_9, align 4
  %conv319 = sext i32 %25 to i64
  %arrayidx20 = getelementptr [10 x i32], [10 x i32]* %arrayidx18, i32 0, i64 %conv319
  %26 = load i32, i32* %arrayidx20, align 4
  %call6 = call i32 (i8*, ...) @printf(i8* %23, i32 %26)
  %27 = load i32, i32* %c_7, align 4
  %add.21 = sub i32 %27, 1
  %28 = load i32, i32* %j_9, align 4
  %cmp5 = icmp eq i32 %add.21, %28
  br i1 %cmp5, label %if.then, label %if.else

while.end16:                                      ; preds = %while.cond14
  %29 = load i32, i32* %i_8, align 4
  %add.23 = add i32 %29, 1
  store i32 %add.23, i32* %i_8, align 4
  br label %while.cond11

if.then:                                          ; preds = %while.body15
  %30 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.6, i32 0, i32 0) to i8*
  %call7 = call i32 (i8*, ...) @printf(i8* %30)
  br label %if.end

if.else:                                          ; preds = %while.body15
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %31 = load i32, i32* %j_9, align 4
  %add.22 = add i32 %31, 1
  store i32 %add.22, i32* %j_9, align 4
  br label %while.cond14

while.cond24:                                     ; preds = %while.end29, %while.end13
  %32 = load i32, i32* %i_8, align 4
  %33 = load i32, i32* %r_6, align 4
  %cmp6 = icmp slt i32 %32, %33
  br i1 %cmp6, label %while.body25, label %while.end26

while.body25:                                     ; preds = %while.cond24
  store i32 0, i32* %j_9, align 4
  br label %while.cond27

while.end26:                                      ; preds = %while.cond24
  %34 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.7, i32 0, i32 0) to i8*
  %call8 = call i32 (i8*, ...) @printf(i8* %34)
  store i32 0, i32* %i_8, align 4
  br label %while.cond40

while.cond27:                                     ; preds = %while.body28, %while.body25
  %35 = load i32, i32* %j_9, align 4
  %36 = load i32, i32* %c_7, align 4
  %cmp7 = icmp slt i32 %35, %36
  br i1 %cmp7, label %while.body28, label %while.end29

while.body28:                                     ; preds = %while.cond27
  %37 = load i32, i32* %j_9, align 4
  %conv330 = sext i32 %37 to i64
  %arrayidx31 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %transpose_5, i32 0, i64 %conv330
  %38 = load i32, i32* %i_8, align 4
  %conv332 = sext i32 %38 to i64
  %arrayidx33 = getelementptr [10 x i32], [10 x i32]* %arrayidx31, i32 0, i64 %conv332
  %39 = load i32, i32* %i_8, align 4
  %conv334 = sext i32 %39 to i64
  %arrayidx35 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %a_4, i32 0, i64 %conv334
  %40 = load i32, i32* %j_9, align 4
  %conv336 = sext i32 %40 to i64
  %arrayidx37 = getelementptr [10 x i32], [10 x i32]* %arrayidx35, i32 0, i64 %conv336
  %41 = load i32, i32* %arrayidx37, align 4
  store i32 %41, i32* %arrayidx33, align 4
  %42 = load i32, i32* %j_9, align 4
  %add.38 = add i32 %42, 1
  store i32 %add.38, i32* %j_9, align 4
  br label %while.cond27

while.end29:                                      ; preds = %while.cond27
  %43 = load i32, i32* %i_8, align 4
  %add.39 = add i32 %43, 1
  store i32 %add.39, i32* %i_8, align 4
  br label %while.cond24

while.cond40:                                     ; preds = %while.end45, %while.end26
  %44 = load i32, i32* %i_8, align 4
  %45 = load i32, i32* %c_7, align 4
  %cmp8 = icmp slt i32 %44, %45
  br i1 %cmp8, label %while.body41, label %while.end42

while.body41:                                     ; preds = %while.cond40
  store i32 0, i32* %j_9, align 4
  br label %while.cond43

while.end42:                                      ; preds = %while.cond40
  ret i32 0

while.cond43:                                     ; preds = %if.end53, %while.body41
  %46 = load i32, i32* %j_9, align 4
  %47 = load i32, i32* %r_6, align 4
  %cmp9 = icmp slt i32 %46, %47
  br i1 %cmp9, label %while.body44, label %while.end45

while.body44:                                     ; preds = %while.cond43
  %48 = bitcast i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.5, i32 0, i32 0) to i8*
  %49 = load i32, i32* %i_8, align 4
  %conv346 = sext i32 %49 to i64
  %arrayidx47 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* %transpose_5, i32 0, i64 %conv346
  %50 = load i32, i32* %j_9, align 4
  %conv348 = sext i32 %50 to i64
  %arrayidx49 = getelementptr [10 x i32], [10 x i32]* %arrayidx47, i32 0, i64 %conv348
  %51 = load i32, i32* %arrayidx49, align 4
  %call9 = call i32 (i8*, ...) @printf(i8* %48, i32 %51)
  %52 = load i32, i32* %r_6, align 4
  %add.52 = sub i32 %52, 1
  %53 = load i32, i32* %j_9, align 4
  %cmp10 = icmp eq i32 %add.52, %53
  br i1 %cmp10, label %if.then50, label %if.else51

while.end45:                                      ; preds = %while.cond43
  %54 = load i32, i32* %i_8, align 4
  %add.55 = add i32 %54, 1
  store i32 %add.55, i32* %i_8, align 4
  br label %while.cond40

if.then50:                                        ; preds = %while.body44
  %55 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.6, i32 0, i32 0) to i8*
  %call10 = call i32 (i8*, ...) @printf(i8* %55)
  br label %if.end53

if.else51:                                        ; preds = %while.body44
  br label %if.end53

if.end53:                                         ; preds = %if.else51, %if.then50
  %56 = load i32, i32* %j_9, align 4
  %add.54 = add i32 %56, 1
  store i32 %add.54, i32* %j_9, align 4
  br label %while.cond43
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
