; ModuleID = 'test40.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [24 x i8] c"Enter a binary number: \00", align 1
@.str.1 = private unnamed_addr constant [5 x i8] c"%lld\00", align 1
@.str.2 = private unnamed_addr constant [31 x i8] c"%lld in binary = %d in decimal\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %n = alloca i64, align 8
  %0 = bitcast i64* %n to i8*
  call void @llvm.lifetime.start(i64 8, i8* %0) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.1, i64 0, i64 0), i64* nonnull %n) #1
  %1 = load i64, i64* %n, align 8, !tbaa !1
  %cmp.12.i = icmp eq i64 %1, 0
  br i1 %cmp.12.i, label %ConvertBinaryToDecimal.exit, label %while.body.i.preheader

while.body.i.preheader:                           ; preds = %entry
  br label %while.body.i

while.body.i:                                     ; preds = %while.body.i.preheader, %while.body.i
  %n.addr.015.i = phi i64 [ %div.i, %while.body.i ], [ %1, %while.body.i.preheader ]
  %i.014.i = phi i32 [ %inc.i, %while.body.i ], [ 0, %while.body.i.preheader ]
  %decimalNumber.013.i = phi i32 [ %conv4.i, %while.body.i ], [ 0, %while.body.i.preheader ]
  %rem.i = srem i64 %n.addr.015.i, 10
  %conv.i = trunc i64 %rem.i to i32
  %div.i = sdiv i64 %n.addr.015.i, 10
  %conv1.i = sitofp i32 %conv.i to double
  %2 = call double @ldexp(double 1.000000e+00, i32 %i.014.i) #1
  %mul.i = fmul double %conv1.i, %2
  %conv3.i = sitofp i32 %decimalNumber.013.i to double
  %add.i = fadd double %conv3.i, %mul.i
  %conv4.i = fptosi double %add.i to i32
  %inc.i = add nuw nsw i32 %i.014.i, 1
  %n.addr.015.off.i = add i64 %n.addr.015.i, 9
  %3 = icmp ult i64 %n.addr.015.off.i, 19
  br i1 %3, label %ConvertBinaryToDecimal.exit.loopexit, label %while.body.i

ConvertBinaryToDecimal.exit.loopexit:             ; preds = %while.body.i
  %conv4.i.lcssa = phi i32 [ %conv4.i, %while.body.i ]
  br label %ConvertBinaryToDecimal.exit

ConvertBinaryToDecimal.exit:                      ; preds = %ConvertBinaryToDecimal.exit.loopexit, %entry
  %decimalNumber.0.lcssa.i = phi i32 [ 0, %entry ], [ %conv4.i.lcssa, %ConvertBinaryToDecimal.exit.loopexit ]
  %call3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.2, i64 0, i64 0), i64 %1, i32 %decimalNumber.0.lcssa.i) #1
  call void @llvm.lifetime.end(i64 8, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind uwtable
define i32 @ConvertBinaryToDecimal(i64 %n) #0 {
entry:
  %cmp.12 = icmp eq i64 %n, 0
  br i1 %cmp.12, label %while.end, label %while.body.preheader

while.body.preheader:                             ; preds = %entry
  br label %while.body

while.body:                                       ; preds = %while.body.preheader, %while.body
  %n.addr.015 = phi i64 [ %div, %while.body ], [ %n, %while.body.preheader ]
  %i.014 = phi i32 [ %inc, %while.body ], [ 0, %while.body.preheader ]
  %decimalNumber.013 = phi i32 [ %conv4, %while.body ], [ 0, %while.body.preheader ]
  %rem = srem i64 %n.addr.015, 10
  %conv = trunc i64 %rem to i32
  %div = sdiv i64 %n.addr.015, 10
  %conv1 = sitofp i32 %conv to double
  %0 = tail call double @ldexp(double 1.000000e+00, i32 %i.014) #1
  %mul = fmul double %conv1, %0
  %conv3 = sitofp i32 %decimalNumber.013 to double
  %add = fadd double %conv3, %mul
  %conv4 = fptosi double %add to i32
  %inc = add nuw nsw i32 %i.014, 1
  %n.addr.015.off = add i64 %n.addr.015, 9
  %1 = icmp ult i64 %n.addr.015.off, 19
  br i1 %1, label %while.end.loopexit, label %while.body

while.end.loopexit:                               ; preds = %while.body
  %conv4.lcssa = phi i32 [ %conv4, %while.body ]
  br label %while.end

while.end:                                        ; preds = %while.end.loopexit, %entry
  %decimalNumber.0.lcssa = phi i32 [ 0, %entry ], [ %conv4.lcssa, %while.end.loopexit ]
  ret i32 %decimalNumber.0.lcssa
}

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

declare double @ldexp(double, i32)

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"long long", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
