; ModuleID = 'test21.bc'

@.str = private constant [30 x i8] c"Enter two positive integers: \00", align 1
@.str.1 = private constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private constant [28 x i8] c"The LCM of %d and %d is %d.\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %minMultiple_6 = alloca i32, align 4
  %n1_4 = alloca i32, align 4
  %n2_5 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n1_4 to i32*
  %3 = bitcast i32* %n2_5 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2, i32* %3)
  %4 = load i32, i32* %n1_4, align 4
  %5 = load i32, i32* %n2_5, align 4
  %cmp1 = icmp sgt i32 %4, %5
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = load i32, i32* %n1_4, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %7 = load i32, i32* %n2_5, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %8 = phi i32 [ %6, %if.then ], [ %7, %if.else ]
  store i32 %8, i32* %minMultiple_6, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end4, %if.end
  %icmp = icmp ne i32 1, 0
  br i1 %icmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load i32, i32* %minMultiple_6, align 4
  %10 = load i32, i32* %n1_4, align 4
  %srem = srem i32 %9, %10
  %cmp2 = icmp eq i32 %srem, 0
  br i1 %cmp2, label %land.rhs, label %land.end

while.end:                                        ; preds = %if.then1, %while.cond
  br label %LABEL_258

if.then1:                                         ; preds = %land.end
  %11 = bitcast i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.2, i32 0, i32 0) to i8*
  %12 = load i32, i32* %n1_4, align 4
  %13 = load i32, i32* %n2_5, align 4
  %14 = load i32, i32* %minMultiple_6, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %11, i32 %12, i32 %13, i32 %14)
  br label %while.end

if.else2:                                         ; preds = %land.end
  br label %if.end4

land.rhs:                                         ; preds = %while.body
  %15 = load i32, i32* %minMultiple_6, align 4
  %16 = load i32, i32* %n2_5, align 4
  %srem.3 = srem i32 %15, %16
  %cmp3 = icmp eq i32 %srem.3, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.body
  %17 = phi i1 [ false, %while.body ], [ %cmp3, %land.rhs ]
  br i1 %17, label %if.then1, label %if.else2

if.end4:                                          ; preds = %if.else2
  %18 = load i32, i32* %minMultiple_6, align 4
  %add = add i32 %18, 1
  store i32 %add, i32* %minMultiple_6, align 4
  br label %while.cond

LABEL_258:                                        ; preds = %while.end
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
