; ModuleID = 'test2.bc'

@.str = private constant [21 x i8] c"Enter two integers: \00", align 1
@.str.1 = private constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private constant [13 x i8] c"%d + %d = %d\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %firstNumber_4 = alloca i32, align 4
  %secondNumber_5 = alloca i32, align 4
  %sumOfTwoNumbers_6 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %firstNumber_4 to i32*
  %3 = bitcast i32* %secondNumber_5 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2, i32* %3)
  %4 = load i32, i32* %firstNumber_4, align 4
  %5 = load i32, i32* %secondNumber_5, align 4
  %add = add i32 %4, %5
  store i32 %add, i32* %sumOfTwoNumbers_6, align 4
  %6 = bitcast i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.2, i32 0, i32 0) to i8*
  %7 = load i32, i32* %firstNumber_4, align 4
  %8 = load i32, i32* %secondNumber_5, align 4
  %9 = load i32, i32* %sumOfTwoNumbers_6, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %6, i32 %7, i32 %8, i32 %9)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
