; ModuleID = 'test28_O.bc'

@.str = private constant [30 x i8] c"Enter two numbers(intevals): \00", align 1
@.str.1 = private constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private constant [38 x i8] c"Prime numbers between %d and %d are: \00", align 1
@.str.3 = private constant [4 x i8] c"%d \00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_53 = alloca i32
  %.preg_I4_4_57 = alloca i32
  %.preg_I4_4_54 = alloca i32
  %.preg_I4_4_52 = alloca i32
  %.preg_I4_4_49 = alloca i32
  %.preg_I4_4_56 = alloca i32
  %.preg_I4_4_55 = alloca i32
  %_temp_dummy10_8 = alloca i32, align 4
  %_temp_dummy11_9 = alloca i32, align 4
  %_temp_dummy12_10 = alloca i32, align 4
  %_temp_dummy13_11 = alloca i32, align 4
  %_temp_ehpit0_12 = alloca i32, align 4
  %flag_7 = alloca i32, align 4
  %i_6 = alloca i32, align 4
  %n1_4 = alloca i32, align 4
  %n2_5 = alloca i32, align 4
  %old_frame_pointer_17.addr = alloca i64, align 8
  %return_address_18.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n1_4 to i32*
  %3 = bitcast i32* %n2_5 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2, i32* %3)
  %4 = bitcast i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.2, i32 0, i32 0) to i8*
  %5 = load i32, i32* %n1_4, align 4
  %6 = load i32, i32* %n2_5, align 4
  %call3 = call i32 (i8*, ...) @printf(i8* %4, i32 %5, i32 %6)
  %7 = load i32, i32* %n2_5, align 4
  store i32 %7, i32* %.preg_I4_4_55, align 8
  %8 = load i32, i32* %n1_4, align 4
  store i32 %8, i32* %.preg_I4_4_56, align 8
  %9 = load i32, i32* %.preg_I4_4_56
  %10 = load i32, i32* %.preg_I4_4_55
  %cmp1 = icmp slt i32 %9, %10
  br i1 %cmp1, label %tb, label %L5634

tb:                                               ; preds = %entry
  br label %L6146

L5634:                                            ; preds = %fb7, %entry
  ret i32 0

L6146:                                            ; preds = %L8450, %tb
  store i32 0, i32* %.preg_I4_4_49, align 8
  %11 = load i32, i32* %.preg_I4_4_56
  %div = sdiv i32 %11, 2
  store i32 %div, i32* %.preg_I4_4_52, align 8
  %12 = load i32, i32* %.preg_I4_4_52
  %add = add i32 %12, -2
  store i32 %add, i32* %.preg_I4_4_54, align 8
  %13 = load i32, i32* %.preg_I4_4_54
  %cmp2 = icmp sge i32 %13, 0
  br i1 %cmp2, label %tb1, label %L6402

tb1:                                              ; preds = %L6146
  %14 = load i32, i32* %.preg_I4_4_52
  %add.2 = add i32 %14, -1
  store i32 %add.2, i32* %.preg_I4_4_57, align 8
  store i32 2, i32* %.preg_I4_4_53, align 8
  br label %L6914

L6402:                                            ; preds = %fb, %L6146
  %15 = bitcast i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.3, i32 0, i32 0) to i8*
  %16 = load i32, i32* %.preg_I4_4_56
  %call4 = call i32 (i8*, ...) @printf(i8* %15, i32 %16)
  %17 = load i32, i32* %n2_5, align 4
  store i32 %17, i32* %.preg_I4_4_55, align 8
  %18 = load i32, i32* %n1_4, align 4
  store i32 %18, i32* %.preg_I4_4_56, align 8
  br label %L8450

L6914:                                            ; preds = %L7170, %tb1
  %19 = load i32, i32* %.preg_I4_4_56
  %20 = load i32, i32* %.preg_I4_4_53
  %srem = srem i32 %19, %20
  %cmp3 = icmp eq i32 %srem, 0
  br i1 %cmp3, label %tb3, label %L7170

tb3:                                              ; preds = %L6914
  br label %L8450

L7170:                                            ; preds = %L6914
  %21 = load i32, i32* %.preg_I4_4_49
  %add.4 = add i32 %21, 1
  store i32 %add.4, i32* %.preg_I4_4_49, align 8
  %22 = load i32, i32* %.preg_I4_4_53
  %add.5 = add i32 %22, 1
  store i32 %add.5, i32* %.preg_I4_4_53, align 8
  %23 = load i32, i32* %.preg_I4_4_49
  %24 = load i32, i32* %.preg_I4_4_54
  %cmp4 = icmp sle i32 %23, %24
  br i1 %cmp4, label %L6914, label %fb

L8450:                                            ; preds = %tb3, %L6402
  %25 = load i32, i32* %.preg_I4_4_56
  %add.6 = add i32 %25, 1
  store i32 %add.6, i32* %.preg_I4_4_56, align 8
  %26 = load i32, i32* %.preg_I4_4_56
  store i32 %26, i32* %n1_4, align 4
  %27 = load i32, i32* %.preg_I4_4_56
  %28 = load i32, i32* %.preg_I4_4_55
  %cmp5 = icmp slt i32 %27, %28
  br i1 %cmp5, label %L6146, label %fb7

fb:                                               ; preds = %L7170
  br label %L6402

fb7:                                              ; preds = %L8450
  br label %L5634
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
