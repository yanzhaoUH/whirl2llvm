; ModuleID = 'test340_O.bc'

@.str = private constant [3 x i8] c"%d\00", align 1
@.str.1 = private constant [6 x i8] c"hello\00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_51 = alloca i32
  %.preg_I4_4_57 = alloca i32
  %.preg_I4_4_50 = alloca i32
  %.preg_I4_4_54 = alloca i32
  %.preg_I4_4_55 = alloca i32
  %_temp_dummy10_5 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %_temp_ehpit0_9 = alloca i32, align 4
  %i_4 = alloca i32, align 4
  %j_6 = alloca i32, align 4
  %old_frame_pointer_14.addr = alloca i64, align 8
  %return_address_15.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str, i32 0, i32 0) to i8*
  %1 = bitcast i32* %i_4 to i32*
  %call1 = call i32 (i8*, ...) @scanf(i8* %0, i32* %1)
  %2 = load i32, i32* %i_4, align 4
  store i32 %2, i32* %.preg_I4_4_55, align 8
  %3 = load i32, i32* %.preg_I4_4_55
  %add = sub i32 9, %3
  store i32 %add, i32* %.preg_I4_4_54, align 8
  %4 = load i32, i32* %.preg_I4_4_54
  %cmp1 = icmp sge i32 %4, 0
  br i1 %cmp1, label %tb, label %L2818

tb:                                               ; preds = %entry
  store i32 0, i32* %.preg_I4_4_50, align 8
  %5 = load i32, i32* %.preg_I4_4_55
  %add.1 = sub i32 10, %5
  store i32 %add.1, i32* %.preg_I4_4_57, align 8
  %6 = load i32, i32* %.preg_I4_4_55
  store i32 %6, i32* %.preg_I4_4_51, align 8
  br label %L3586

L2818:                                            ; preds = %entry
  br label %L4866

L3586:                                            ; preds = %L3586, %tb
  %7 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str, i32 0, i32 0) to i8*
  %8 = load i32, i32* %.preg_I4_4_51
  %call2 = call i32 (i8*, ...) @printf(i8* %7, i32 %8)
  %9 = load i32, i32* %.preg_I4_4_50
  %add.2 = add i32 %9, 1
  store i32 %add.2, i32* %.preg_I4_4_50, align 8
  %10 = load i32, i32* %.preg_I4_4_51
  %add.3 = add i32 %10, 1
  store i32 %add.3, i32* %.preg_I4_4_51, align 8
  %11 = load i32, i32* %.preg_I4_4_50
  %12 = load i32, i32* %.preg_I4_4_54
  %cmp2 = icmp sle i32 %11, %12
  br i1 %cmp2, label %L3586, label %fb

fb:                                               ; preds = %L3586
  br label %L4354

L4354:                                            ; preds = %fb
  %conv = trunc i32 0 to i1
  br i1 %conv, label %tb4, label %L5378

L4866:                                            ; preds = %L2818
  %13 = load i32, i32* %.preg_I4_4_55
  %add.5 = add i32 %13, 10
  %cmp3 = icmp sgt i32 %add.5, 100
  br i1 %cmp3, label %tb6, label %L4098

tb4:                                              ; preds = %L4354
  br label %L5122

L5378:                                            ; preds = %L4354
  br label %L4098

L5122:                                            ; preds = %tb6, %tb4
  %14 = bitcast i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %14)
  br label %L4610

L4610:                                            ; preds = %L4098, %L5122
  ret i32 0

tb6:                                              ; preds = %L4866
  br label %L5122

L4098:                                            ; preds = %L5378, %L4866
  br label %L4610
}

declare i32 @scanf(i8*, ...)

declare i32 @printf(i8*, ...)
