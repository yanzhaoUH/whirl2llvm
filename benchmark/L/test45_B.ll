; ModuleID = 'test45.bc'

@.str = private constant [28 x i8] c"Enter the numbers of data: \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [46 x i8] c"Error! number should in range of (1 to 100).\0A\00", align 1
@.str.3 = private constant [25 x i8] c"Enter the number again: \00", align 1
@.str.4 = private constant [19 x i8] c"%d. Enter number: \00", align 1
@.str.5 = private constant [15 x i8] c"Average = %.2f\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_9 = alloca i32, align 4
  %_temp_dummy11_10 = alloca i32, align 4
  %_temp_dummy12_11 = alloca i32, align 4
  %_temp_dummy13_12 = alloca i32, align 4
  %_temp_dummy14_13 = alloca i32, align 4
  %_temp_dummy15_14 = alloca i32, align 4
  %_temp_dummy16_15 = alloca i32, align 4
  %average_8 = alloca float, align 4
  %i_5 = alloca i32, align 4
  %n_4 = alloca i32, align 4
  %num_6 = alloca [100 x float], align 4
  %sum_7 = alloca float, align 4
  store float 0.000000e+00, float* %sum_7, align 4
  %0 = bitcast i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_4 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %3 = load i32, i32* %n_4, align 4
  %add = sub i32 %3, 1
  %cmp1 = icmp ugt i32 %add, 99
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = bitcast i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %4)
  %5 = bitcast i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.3, i32 0, i32 0) to i8*
  %call4 = call i32 (i8*, ...) @printf(i8* %5)
  %6 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %7 = bitcast i32* %n_4 to i32*
  %call5 = call i32 (i8*, ...) @scanf(i8* %6, i32* %7)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 0, i32* %i_5, align 4
  br label %while.cond1

while.cond1:                                      ; preds = %while.body2, %while.end
  %8 = load i32, i32* %i_5, align 4
  %9 = load i32, i32* %n_4, align 4
  %cmp2 = icmp slt i32 %8, %9
  br i1 %cmp2, label %while.body2, label %while.end3

while.body2:                                      ; preds = %while.cond1
  %10 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.4, i32 0, i32 0) to i8*
  %11 = load i32, i32* %i_5, align 4
  %add.4 = add i32 %11, 1
  %call6 = call i32 (i8*, ...) @printf(i8* %10, i32 %add.4)
  %12 = load i32, i32* %i_5, align 4
  %conv3 = sext i32 %12 to i64
  %arrayidx = getelementptr [100 x float], [100 x float]* %num_6, i32 0, i64 %conv3
  %13 = load i32, i32* %i_5, align 4
  %conv9 = sitofp i32 %13 to float
  store float %conv9, float* %arrayidx, align 4
  %14 = load i32, i32* %i_5, align 4
  %conv35 = sext i32 %14 to i64
  %arrayidx6 = getelementptr [100 x float], [100 x float]* %num_6, i32 0, i64 %conv35
  %15 = load float, float* %arrayidx6, align 4
  %16 = load float, float* %sum_7, align 4
  %add.7 = fadd float %15, %16
  store float %add.7, float* %sum_7, align 4
  %17 = load i32, i32* %i_5, align 4
  %add.8 = add i32 %17, 1
  store i32 %add.8, i32* %i_5, align 4
  br label %while.cond1

while.end3:                                       ; preds = %while.cond1
  %18 = load float, float* %sum_7, align 4
  %19 = load i32, i32* %n_4, align 4
  %conv99 = sitofp i32 %19 to float
  %SDiv = fdiv float %18, %conv99
  store float %SDiv, float* %average_8, align 4
  %20 = bitcast i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.5, i32 0, i32 0) to i8*
  %21 = load float, float* %average_8, align 4
  %convDouble = fpext float %21 to double
  %call7 = call i32 (i8*, ...) @printf(i8* %20, double %convDouble)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
