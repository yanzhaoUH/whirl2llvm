; ModuleID = 'test23_O.bc'

@.str = private constant [19 x i8] c"Enter an integer: \00", align 1
@.str.1 = private constant [5 x i8] c"%lld\00", align 1
@.str.2 = private constant [21 x i8] c"Number of digits: %d\00", align 1

define i32 @main() {
entry:
  %.preg_I4_4_51 = alloca i32
  %.preg_I8_5_50 = alloca i64
  %_temp_dummy10_6 = alloca i32, align 4
  %_temp_dummy11_7 = alloca i32, align 4
  %_temp_dummy12_8 = alloca i32, align 4
  %_temp_ehpit0_9 = alloca i32, align 4
  %count_5 = alloca i32, align 4
  %n_4 = alloca i64, align 8
  %old_frame_pointer_14.addr = alloca i64, align 8
  %return_address_15.addr = alloca i64, align 8
  %0 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i64* %n_4 to i64*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i64* %2)
  %3 = load i64, i64* %n_4, align 8
  store i64 %3, i64* %.preg_I8_5_50, align 8
  %4 = load i64, i64* %.preg_I8_5_50
  %conv3 = zext i32 0 to i64
  %cmp1 = icmp ne i64 %4, %conv3
  br i1 %cmp1, label %tb, label %L2818

tb:                                               ; preds = %entry
  store i32 0, i32* %.preg_I4_4_51, align 8
  br label %L2306

L2818:                                            ; preds = %entry
  store i32 0, i32* %.preg_I4_4_51, align 8
  br label %L1794

L2306:                                            ; preds = %L2306, %tb
  %5 = load i64, i64* %.preg_I8_5_50
  %conv31 = zext i32 10 to i64
  %div = sdiv i64 %5, %conv31
  store i64 %div, i64* %.preg_I8_5_50, align 8
  %6 = load i32, i32* %.preg_I4_4_51
  %add = add i32 %6, 1
  store i32 %add, i32* %.preg_I4_4_51, align 8
  %7 = load i64, i64* %.preg_I8_5_50
  %conv32 = zext i32 0 to i64
  %cmp2 = icmp ne i64 %7, %conv32
  br i1 %cmp2, label %L2306, label %fb

fb:                                               ; preds = %L2306
  %8 = load i64, i64* %.preg_I8_5_50
  store i64 %8, i64* %n_4, align 8
  br label %L1794

L1794:                                            ; preds = %fb, %L2818
  %9 = bitcast i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.2, i32 0, i32 0) to i8*
  %10 = load i32, i32* %.preg_I4_4_51
  %call3 = call i32 (i8*, ...) @printf(i8* %9, i32 %10)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
