; ModuleID = 'test46.bc'

@.str = private constant [43 x i8] c"Enter total number of elements(1 to 100): \00", align 1
@.str.1 = private constant [3 x i8] c"%d\00", align 1
@.str.2 = private constant [2 x i8] c"\0A\00", align 1
@.str.3 = private constant [18 x i8] c"Enter Number %d: \00", align 1
@.str.4 = private constant [23 x i8] c"Largest element = %.2f\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %_temp_dummy14_11 = alloca i32, align 4
  %arr_6 = alloca [100 x float], align 4
  %i_4 = alloca i32, align 4
  %n_5 = alloca i32, align 4
  %0 = bitcast i8* getelementptr inbounds ([43 x i8], [43 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0) to i8*
  %2 = bitcast i32* %n_5 to i32*
  %call2 = call i32 (i8*, ...) @scanf(i8* %1, i32* %2)
  %3 = bitcast i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.2, i32 0, i32 0) to i8*
  %call3 = call i32 (i8*, ...) @printf(i8* %3)
  store i32 0, i32* %i_4, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %4 = load i32, i32* %i_4, align 4
  %5 = load i32, i32* %n_5, align 4
  %cmp1 = icmp slt i32 %4, %5
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %6 = bitcast i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.3, i32 0, i32 0) to i8*
  %7 = load i32, i32* %i_4, align 4
  %add = add i32 %7, 1
  %call4 = call i32 (i8*, ...) @printf(i8* %6, i32 %add)
  %8 = load i32, i32* %i_4, align 4
  %conv3 = sext i32 %8 to i64
  %arrayidx = getelementptr [100 x float], [100 x float]* %arr_6, i32 0, i64 %conv3
  %9 = load i32, i32* %i_4, align 4
  %conv9 = sitofp i32 %9 to float
  store float %conv9, float* %arrayidx, align 4
  %10 = load i32, i32* %i_4, align 4
  %add.1 = add i32 %10, 1
  store i32 %add.1, i32* %i_4, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 1, i32* %i_4, align 4
  br label %while.cond2

while.cond2:                                      ; preds = %if.end, %while.end
  %11 = load i32, i32* %i_4, align 4
  %12 = load i32, i32* %n_5, align 4
  %cmp2 = icmp slt i32 %11, %12
  br i1 %cmp2, label %while.body3, label %while.end4

while.body3:                                      ; preds = %while.cond2
  %arrayidx5 = getelementptr [100 x float], [100 x float]* %arr_6, i32 0, i32 0
  %13 = load float, float* %arrayidx5, align 4
  %14 = load i32, i32* %i_4, align 4
  %conv36 = sext i32 %14 to i64
  %arrayidx7 = getelementptr [100 x float], [100 x float]* %arr_6, i32 0, i64 %conv36
  %15 = load float, float* %arrayidx7, align 4
  %cmp3 = fcmp olt float %13, %15
  br i1 %cmp3, label %if.then, label %if.else

while.end4:                                       ; preds = %while.cond2
  %16 = bitcast i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.4, i32 0, i32 0) to i8*
  %arrayidx12 = getelementptr [100 x float], [100 x float]* %arr_6, i32 0, i32 0
  %17 = load float, float* %arrayidx12, align 4
  %convDouble = fpext float %17 to double
  %call5 = call i32 (i8*, ...) @printf(i8* %16, double %convDouble)
  ret i32 0

if.then:                                          ; preds = %while.body3
  %arrayidx8 = getelementptr [100 x float], [100 x float]* %arr_6, i32 0, i32 0
  %18 = load i32, i32* %i_4, align 4
  %conv39 = sext i32 %18 to i64
  %arrayidx10 = getelementptr [100 x float], [100 x float]* %arr_6, i32 0, i64 %conv39
  %19 = load float, float* %arrayidx10, align 4
  store float %19, float* %arrayidx8, align 4
  br label %if.end

if.else:                                          ; preds = %while.body3
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %20 = load i32, i32* %i_4, align 4
  %add.11 = add i32 %20, 1
  store i32 %add.11, i32* %i_4, align 4
  br label %while.cond2
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)
