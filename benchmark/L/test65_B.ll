; ModuleID = 'test65.bc'

%struct.TIME = type { i32, i32, i32 }

@.str = private constant [20 x i8] c"Enter start time: \0A\00", align 1
@.str.1 = private constant [48 x i8] c"Enter hours, minutes and seconds respectively: \00", align 1
@.str.2 = private constant [9 x i8] c"%d %d %d\00", align 1
@.str.3 = private constant [19 x i8] c"Enter stop time: \0A\00", align 1
@.str.4 = private constant [30 x i8] c"\0ATIME DIFFERENCE: %d:%d:%d - \00", align 1
@.str.5 = private constant [10 x i8] c"%d:%d:%d \00", align 1
@.str.6 = private constant [12 x i8] c"= %d:%d:%d\0A\00", align 1

define i32 @main() {
entry:
  %_temp_dummy10_7 = alloca i32, align 4
  %_temp_dummy11_8 = alloca i32, align 4
  %_temp_dummy12_9 = alloca i32, align 4
  %_temp_dummy13_10 = alloca i32, align 4
  %_temp_dummy14_11 = alloca i32, align 4
  %_temp_dummy15_12 = alloca i32, align 4
  %_temp_dummy16_13 = alloca i32, align 4
  %_temp_dummy17_14 = alloca i32, align 4
  %_temp_dummy18_15 = alloca i32, align 4
  %diff_6 = alloca %struct.TIME, align 4
  %startTime_4 = alloca %struct.TIME, align 4
  %stopTime_5 = alloca %struct.TIME, align 4
  %0 = bitcast i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0) to i8*
  %call1 = call i32 (i8*, ...) @printf(i8* %0)
  %1 = bitcast i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call2 = call i32 (i8*, ...) @printf(i8* %1)
  %2 = bitcast i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.2, i32 0, i32 0) to i8*
  %seconds = getelementptr %struct.TIME, %struct.TIME* %startTime_4, i32 0, i32 0
  %minutes = getelementptr %struct.TIME, %struct.TIME* %startTime_4, i32 0, i32 1
  %hours = getelementptr %struct.TIME, %struct.TIME* %startTime_4, i32 0, i32 2
  %3 = bitcast i32* %hours to i32*
  %seconds1 = getelementptr %struct.TIME, %struct.TIME* %startTime_4, i32 0, i32 0
  %minutes2 = getelementptr %struct.TIME, %struct.TIME* %startTime_4, i32 0, i32 1
  %hours3 = getelementptr %struct.TIME, %struct.TIME* %startTime_4, i32 0, i32 2
  %4 = bitcast i32* %minutes2 to i32*
  %seconds4 = getelementptr %struct.TIME, %struct.TIME* %startTime_4, i32 0, i32 0
  %minutes5 = getelementptr %struct.TIME, %struct.TIME* %startTime_4, i32 0, i32 1
  %hours6 = getelementptr %struct.TIME, %struct.TIME* %startTime_4, i32 0, i32 2
  %5 = bitcast i32* %seconds4 to i32*
  %call3 = call i32 (i8*, ...) @scanf(i8* %2, i32* %3, i32* %4, i32* %5)
  %6 = bitcast i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.3, i32 0, i32 0) to i8*
  %call4 = call i32 (i8*, ...) @printf(i8* %6)
  %7 = bitcast i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.1, i32 0, i32 0) to i8*
  %call5 = call i32 (i8*, ...) @printf(i8* %7)
  %8 = bitcast i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.2, i32 0, i32 0) to i8*
  %seconds7 = getelementptr %struct.TIME, %struct.TIME* %stopTime_5, i32 0, i32 0
  %minutes8 = getelementptr %struct.TIME, %struct.TIME* %stopTime_5, i32 0, i32 1
  %hours9 = getelementptr %struct.TIME, %struct.TIME* %stopTime_5, i32 0, i32 2
  %9 = bitcast i32* %hours9 to i32*
  %seconds10 = getelementptr %struct.TIME, %struct.TIME* %stopTime_5, i32 0, i32 0
  %minutes11 = getelementptr %struct.TIME, %struct.TIME* %stopTime_5, i32 0, i32 1
  %hours12 = getelementptr %struct.TIME, %struct.TIME* %stopTime_5, i32 0, i32 2
  %10 = bitcast i32* %minutes11 to i32*
  %seconds13 = getelementptr %struct.TIME, %struct.TIME* %stopTime_5, i32 0, i32 0
  %minutes14 = getelementptr %struct.TIME, %struct.TIME* %stopTime_5, i32 0, i32 1
  %hours15 = getelementptr %struct.TIME, %struct.TIME* %stopTime_5, i32 0, i32 2
  %11 = bitcast i32* %seconds13 to i32*
  %call6 = call i32 (i8*, ...) @scanf(i8* %8, i32* %9, i32* %10, i32* %11)
  %12 = load %struct.TIME, %struct.TIME* %startTime_4
  %13 = load %struct.TIME, %struct.TIME* %stopTime_5
  %14 = bitcast %struct.TIME* %diff_6 to %struct.TIME*
  call void @_Z27differenceBetweenTimePeriod4TIMES_PS_(%struct.TIME %12, %struct.TIME %13, %struct.TIME* %14)
  %15 = bitcast i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.4, i32 0, i32 0) to i8*
  %seconds16 = getelementptr %struct.TIME, %struct.TIME* %startTime_4, i32 0, i32 0
  %minutes17 = getelementptr %struct.TIME, %struct.TIME* %startTime_4, i32 0, i32 1
  %hours18 = getelementptr %struct.TIME, %struct.TIME* %startTime_4, i32 0, i32 2
  %16 = load i32, i32* %hours18, align 4
  %seconds19 = getelementptr %struct.TIME, %struct.TIME* %startTime_4, i32 0, i32 0
  %minutes20 = getelementptr %struct.TIME, %struct.TIME* %startTime_4, i32 0, i32 1
  %hours21 = getelementptr %struct.TIME, %struct.TIME* %startTime_4, i32 0, i32 2
  %17 = load i32, i32* %minutes20, align 4
  %seconds22 = getelementptr %struct.TIME, %struct.TIME* %startTime_4, i32 0, i32 0
  %minutes23 = getelementptr %struct.TIME, %struct.TIME* %startTime_4, i32 0, i32 1
  %hours24 = getelementptr %struct.TIME, %struct.TIME* %startTime_4, i32 0, i32 2
  %18 = load i32, i32* %seconds22, align 4
  %call8 = call i32 (i8*, ...) @printf(i8* %15, i32 %16, i32 %17, i32 %18)
  %19 = bitcast i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.5, i32 0, i32 0) to i8*
  %seconds25 = getelementptr %struct.TIME, %struct.TIME* %stopTime_5, i32 0, i32 0
  %minutes26 = getelementptr %struct.TIME, %struct.TIME* %stopTime_5, i32 0, i32 1
  %hours27 = getelementptr %struct.TIME, %struct.TIME* %stopTime_5, i32 0, i32 2
  %20 = load i32, i32* %hours27, align 4
  %seconds28 = getelementptr %struct.TIME, %struct.TIME* %stopTime_5, i32 0, i32 0
  %minutes29 = getelementptr %struct.TIME, %struct.TIME* %stopTime_5, i32 0, i32 1
  %hours30 = getelementptr %struct.TIME, %struct.TIME* %stopTime_5, i32 0, i32 2
  %21 = load i32, i32* %minutes29, align 4
  %seconds31 = getelementptr %struct.TIME, %struct.TIME* %stopTime_5, i32 0, i32 0
  %minutes32 = getelementptr %struct.TIME, %struct.TIME* %stopTime_5, i32 0, i32 1
  %hours33 = getelementptr %struct.TIME, %struct.TIME* %stopTime_5, i32 0, i32 2
  %22 = load i32, i32* %seconds31, align 4
  %call9 = call i32 (i8*, ...) @printf(i8* %19, i32 %20, i32 %21, i32 %22)
  %23 = bitcast i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.6, i32 0, i32 0) to i8*
  %seconds34 = getelementptr %struct.TIME, %struct.TIME* %diff_6, i32 0, i32 0
  %minutes35 = getelementptr %struct.TIME, %struct.TIME* %diff_6, i32 0, i32 1
  %hours36 = getelementptr %struct.TIME, %struct.TIME* %diff_6, i32 0, i32 2
  %24 = load i32, i32* %hours36, align 4
  %seconds37 = getelementptr %struct.TIME, %struct.TIME* %diff_6, i32 0, i32 0
  %minutes38 = getelementptr %struct.TIME, %struct.TIME* %diff_6, i32 0, i32 1
  %hours39 = getelementptr %struct.TIME, %struct.TIME* %diff_6, i32 0, i32 2
  %25 = load i32, i32* %minutes38, align 4
  %seconds40 = getelementptr %struct.TIME, %struct.TIME* %diff_6, i32 0, i32 0
  %minutes41 = getelementptr %struct.TIME, %struct.TIME* %diff_6, i32 0, i32 1
  %hours42 = getelementptr %struct.TIME, %struct.TIME* %diff_6, i32 0, i32 2
  %26 = load i32, i32* %seconds40, align 4
  %call10 = call i32 (i8*, ...) @printf(i8* %23, i32 %24, i32 %25, i32 %26)
  ret i32 0
}

declare i32 @printf(i8*, ...)

declare i32 @scanf(i8*, ...)

define void @_Z27differenceBetweenTimePeriod4TIMES_PS_(%struct.TIME %start_4, %struct.TIME %stop_5, %struct.TIME* %diff_6) {
entry:
  %diff_6.addr = alloca %struct.TIME*, align 4
  %start_4.addr = alloca %struct.TIME, align 4
  %stop_5.addr = alloca %struct.TIME, align 4
  store %struct.TIME %start_4, %struct.TIME* %start_4.addr, align 4
  store %struct.TIME %stop_5, %struct.TIME* %stop_5.addr, align 4
  store %struct.TIME* %diff_6, %struct.TIME** %diff_6.addr, align 4
  %seconds = getelementptr %struct.TIME, %struct.TIME* %stop_5.addr, i32 0, i32 0
  %minutes = getelementptr %struct.TIME, %struct.TIME* %stop_5.addr, i32 0, i32 1
  %hours = getelementptr %struct.TIME, %struct.TIME* %stop_5.addr, i32 0, i32 2
  %0 = load i32, i32* %seconds, align 4
  %seconds1 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 0
  %minutes2 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 1
  %hours3 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 2
  %1 = load i32, i32* %seconds1, align 4
  %cmp1 = icmp sgt i32 %0, %1
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %seconds4 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 0
  %minutes5 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 1
  %hours6 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 2
  %2 = load i32, i32* %minutes5, align 4
  %add = sub i32 %2, 1
  %seconds7 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 0
  %minutes8 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 1
  %hours9 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 2
  store i32 %add, i32* %minutes8, align 4
  %seconds10 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 0
  %minutes11 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 1
  %hours12 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 2
  %3 = load i32, i32* %seconds10, align 4
  %add.13 = add i32 %3, 60
  %seconds14 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 0
  %minutes15 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 1
  %hours16 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 2
  store i32 %add.13, i32* %seconds14, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %4 = load %struct.TIME*, %struct.TIME** %diff_6.addr, align 8
  %seconds17 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 0
  %minutes18 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 1
  %hours19 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 2
  %5 = load i32, i32* %seconds17, align 4
  %seconds20 = getelementptr %struct.TIME, %struct.TIME* %stop_5.addr, i32 0, i32 0
  %minutes21 = getelementptr %struct.TIME, %struct.TIME* %stop_5.addr, i32 0, i32 1
  %hours22 = getelementptr %struct.TIME, %struct.TIME* %stop_5.addr, i32 0, i32 2
  %6 = load i32, i32* %seconds20, align 4
  %add.23 = sub i32 %5, %6
  %seconds24 = getelementptr %struct.TIME, %struct.TIME* %4, i32 0, i32 0
  %minutes25 = getelementptr %struct.TIME, %struct.TIME* %4, i32 0, i32 1
  %hours26 = getelementptr %struct.TIME, %struct.TIME* %4, i32 0, i32 2
  store i32 %add.23, i32* %seconds24, align 4
  %seconds29 = getelementptr %struct.TIME, %struct.TIME* %stop_5.addr, i32 0, i32 0
  %minutes30 = getelementptr %struct.TIME, %struct.TIME* %stop_5.addr, i32 0, i32 1
  %hours31 = getelementptr %struct.TIME, %struct.TIME* %stop_5.addr, i32 0, i32 2
  %7 = load i32, i32* %minutes30, align 4
  %seconds32 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 0
  %minutes33 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 1
  %hours34 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 2
  %8 = load i32, i32* %minutes33, align 4
  %cmp2 = icmp sgt i32 %7, %8
  br i1 %cmp2, label %if.then27, label %if.else28

if.then27:                                        ; preds = %if.end
  %seconds35 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 0
  %minutes36 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 1
  %hours37 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 2
  %9 = load i32, i32* %hours37, align 4
  %add.38 = sub i32 %9, 1
  %seconds39 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 0
  %minutes40 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 1
  %hours41 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 2
  store i32 %add.38, i32* %hours41, align 4
  %seconds42 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 0
  %minutes43 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 1
  %hours44 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 2
  %10 = load i32, i32* %minutes43, align 4
  %add.45 = add i32 %10, 60
  %seconds46 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 0
  %minutes47 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 1
  %hours48 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 2
  store i32 %add.45, i32* %minutes47, align 4
  br label %if.end49

if.else28:                                        ; preds = %if.end
  br label %if.end49

if.end49:                                         ; preds = %if.else28, %if.then27
  %11 = load %struct.TIME*, %struct.TIME** %diff_6.addr, align 8
  %seconds50 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 0
  %minutes51 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 1
  %hours52 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 2
  %12 = load i32, i32* %minutes51, align 4
  %seconds53 = getelementptr %struct.TIME, %struct.TIME* %stop_5.addr, i32 0, i32 0
  %minutes54 = getelementptr %struct.TIME, %struct.TIME* %stop_5.addr, i32 0, i32 1
  %hours55 = getelementptr %struct.TIME, %struct.TIME* %stop_5.addr, i32 0, i32 2
  %13 = load i32, i32* %minutes54, align 4
  %add.56 = sub i32 %12, %13
  %seconds57 = getelementptr %struct.TIME, %struct.TIME* %11, i32 0, i32 0
  %minutes58 = getelementptr %struct.TIME, %struct.TIME* %11, i32 0, i32 1
  %hours59 = getelementptr %struct.TIME, %struct.TIME* %11, i32 0, i32 2
  store i32 %add.56, i32* %minutes58, align 4
  %14 = load %struct.TIME*, %struct.TIME** %diff_6.addr, align 8
  %seconds60 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 0
  %minutes61 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 1
  %hours62 = getelementptr %struct.TIME, %struct.TIME* %start_4.addr, i32 0, i32 2
  %15 = load i32, i32* %hours62, align 4
  %seconds63 = getelementptr %struct.TIME, %struct.TIME* %stop_5.addr, i32 0, i32 0
  %minutes64 = getelementptr %struct.TIME, %struct.TIME* %stop_5.addr, i32 0, i32 1
  %hours65 = getelementptr %struct.TIME, %struct.TIME* %stop_5.addr, i32 0, i32 2
  %16 = load i32, i32* %hours65, align 4
  %add.66 = sub i32 %15, %16
  %seconds67 = getelementptr %struct.TIME, %struct.TIME* %14, i32 0, i32 0
  %minutes68 = getelementptr %struct.TIME, %struct.TIME* %14, i32 0, i32 1
  %hours69 = getelementptr %struct.TIME, %struct.TIME* %14, i32 0, i32 2
  store i32 %add.66, i32* %hours69, align 4
  ret void
}
