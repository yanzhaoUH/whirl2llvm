; ModuleID = 'test28.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [30 x i8] c"Enter two numbers(intevals): \00", align 1
@.str.1 = private unnamed_addr constant [6 x i8] c"%d %d\00", align 1
@.str.2 = private unnamed_addr constant [38 x i8] c"Prime numbers between %d and %d are: \00", align 1
@.str.3 = private unnamed_addr constant [4 x i8] c"%d \00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %n1 = alloca i32, align 4
  %n2 = alloca i32, align 4
  %0 = bitcast i32* %n1 to i8*
  call void @llvm.lifetime.start(i64 4, i8* %0) #1
  %1 = bitcast i32* %n2 to i8*
  call void @llvm.lifetime.start(i64 4, i8* %1) #1
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i64 0, i64 0), i32* nonnull %n1, i32* nonnull %n2) #1
  %2 = load i32, i32* %n1, align 4, !tbaa !1
  %3 = load i32, i32* %n2, align 4, !tbaa !1
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.2, i64 0, i64 0), i32 %2, i32 %3) #1
  %4 = load i32, i32* %n1, align 4, !tbaa !1
  %5 = load i32, i32* %n2, align 4, !tbaa !1
  %cmp.18 = icmp slt i32 %4, %5
  br i1 %cmp.18, label %for.cond.preheader.preheader, label %while.end

for.cond.preheader.preheader:                     ; preds = %entry
  br label %for.cond.preheader

for.cond.preheader:                               ; preds = %for.cond.preheader.preheader, %if.end.8
  %6 = phi i32 [ %8, %if.end.8 ], [ %5, %for.cond.preheader.preheader ]
  %7 = phi i32 [ %inc9, %if.end.8 ], [ %4, %for.cond.preheader.preheader ]
  %cmp3.16 = icmp slt i32 %7, 4
  br i1 %cmp3.16, label %if.then.6.critedge, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %for.cond.preheader
  %div = sdiv i32 %7, 2
  br label %for.body

for.cond:                                         ; preds = %for.body
  %inc = add nuw nsw i32 %i.017, 1
  %cmp3 = icmp slt i32 %i.017, %div
  br i1 %cmp3, label %for.body, label %if.then.6.critedge.loopexit

for.body:                                         ; preds = %for.cond, %for.body.lr.ph
  %i.017 = phi i32 [ 2, %for.body.lr.ph ], [ %inc, %for.cond ]
  %rem = srem i32 %7, %i.017
  %cmp4 = icmp eq i32 %rem, 0
  br i1 %cmp4, label %if.end.8.loopexit, label %for.cond

if.then.6.critedge.loopexit:                      ; preds = %for.cond
  br label %if.then.6.critedge

if.then.6.critedge:                               ; preds = %if.then.6.critedge.loopexit, %for.cond.preheader
  %call7 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.3, i64 0, i64 0), i32 %7) #1
  %.pre = load i32, i32* %n1, align 4, !tbaa !1
  %.pre19 = load i32, i32* %n2, align 4, !tbaa !1
  br label %if.end.8

if.end.8.loopexit:                                ; preds = %for.body
  br label %if.end.8

if.end.8:                                         ; preds = %if.end.8.loopexit, %if.then.6.critedge
  %8 = phi i32 [ %.pre19, %if.then.6.critedge ], [ %6, %if.end.8.loopexit ]
  %9 = phi i32 [ %.pre, %if.then.6.critedge ], [ %7, %if.end.8.loopexit ]
  %inc9 = add nsw i32 %9, 1
  store i32 %inc9, i32* %n1, align 4, !tbaa !1
  %cmp = icmp slt i32 %inc9, %8
  br i1 %cmp, label %for.cond.preheader, label %while.end.loopexit

while.end.loopexit:                               ; preds = %if.end.8
  br label %while.end

while.end:                                        ; preds = %while.end.loopexit, %entry
  call void @llvm.lifetime.end(i64 4, i8* %1) #1
  call void @llvm.lifetime.end(i64 4, i8* %0) #1
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
