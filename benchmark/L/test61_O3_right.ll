; ModuleID = 'test61.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str.1 = private unnamed_addr constant [7 x i8] c"%s[^\0A]\00", align 1
@str = private unnamed_addr constant [16 x i8] c"Enter 10 words:\00"
@str.3 = private unnamed_addr constant [28 x i8] c"\0AIn lexicographical order: \00"

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
for.body.7.lr.ph:
  %str = alloca [10 x [50 x i8]], align 16
  %temp = alloca [50 x i8], align 16
  %0 = getelementptr inbounds [10 x [50 x i8]], [10 x [50 x i8]]* %str, i64 0, i64 0, i64 0
  call void @llvm.lifetime.start(i64 500, i8* %0) #1
  %1 = getelementptr inbounds [50 x i8], [50 x i8]* %temp, i64 0, i64 0
  call void @llvm.lifetime.start(i64 50, i8* %1) #1
  %puts = tail call i32 @puts(i8* getelementptr inbounds ([16 x i8], [16 x i8]* @str, i64 0, i64 0)) #1
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.1, i64 0, i64 0), i8* %0) #1
  %arraydecay.1 = getelementptr inbounds [10 x [50 x i8]], [10 x [50 x i8]]* %str, i64 0, i64 1, i64 0
  %call1.1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.1, i64 0, i64 0), i8* %arraydecay.1) #1
  %arraydecay.2 = getelementptr inbounds [10 x [50 x i8]], [10 x [50 x i8]]* %str, i64 0, i64 2, i64 0
  %call1.2 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.1, i64 0, i64 0), i8* %arraydecay.2) #1
  br label %for.body.7

for.body.7:                                       ; preds = %for.body.7.lr.ph
  %arraydecay13 = getelementptr inbounds [10 x [50 x i8]], [10 x [50 x i8]]* %str, i64 0, i64 1, i64 0
  %call14 = call i32 @strcmp(i8* %0, i8* %arraydecay13) #1
  %cmp15 = icmp sgt i32 %call14, 0
  br i1 %cmp15, label %if.then, label %for.inc.33

if.then:                                          ; preds = %for.body.7
  %call20 = call i8* @strcpy(i8* %1, i8* %0) #1
  %call27 = call i8* @strcpy(i8* %0, i8* %arraydecay13) #1
  %call32 = call i8* @strcpy(i8* %arraydecay13, i8* %1) #1
  br label %for.inc.33

for.inc.33:                                       ; preds = %for.body.7, %if.then
  %arraydecay13.1.81 = getelementptr inbounds [10 x [50 x i8]], [10 x [50 x i8]]* %str, i64 0, i64 2, i64 0
  %call14.1.82 = call i32 @strcmp(i8* %0, i8* %arraydecay13.1.81) #1
  %cmp15.1.83 = icmp sgt i32 %call14.1.82, 0
  br i1 %cmp15.1.83, label %if.then.1.88, label %for.inc.33.1

if.then.1:                                        ; preds = %for.inc.33.1
  %call20.1 = call i8* @strcpy(i8* %1, i8* %arraydecay.1) #1
  %call27.1 = call i8* @strcpy(i8* %arraydecay.1, i8* %arraydecay13.1) #1
  %call32.1 = call i8* @strcpy(i8* %arraydecay13.1, i8* %1) #1
  br label %for.cond.2.loopexit.1

for.cond.2.loopexit.1:                            ; preds = %if.then.1, %for.inc.33.1
  %puts69 = call i32 @puts(i8* getelementptr inbounds ([28 x i8], [28 x i8]* @str.3, i64 0, i64 0)) #1
  %call46 = call i32 @puts(i8* %0) #1
  %call46.1 = call i32 @puts(i8* %arraydecay.1) #1
  %call46.2 = call i32 @puts(i8* %arraydecay.2) #1
  call void @llvm.lifetime.end(i64 50, i8* %1) #1
  call void @llvm.lifetime.end(i64 500, i8* %0) #1
  ret i32 0

if.then.1.88:                                     ; preds = %for.inc.33
  %call20.1.85 = call i8* @strcpy(i8* %1, i8* %0) #1
  %call27.1.86 = call i8* @strcpy(i8* %0, i8* %arraydecay13.1.81) #1
  %call32.1.87 = call i8* @strcpy(i8* %arraydecay13.1.81, i8* %1) #1
  br label %for.inc.33.1

for.inc.33.1:                                     ; preds = %if.then.1.88, %for.inc.33
  %arraydecay13.1 = getelementptr inbounds [10 x [50 x i8]], [10 x [50 x i8]]* %str, i64 0, i64 2, i64 0
  %call14.1 = call i32 @strcmp(i8* %arraydecay.1, i8* %arraydecay13.1) #1
  %cmp15.1 = icmp sgt i32 %call14.1, 0
  br i1 %cmp15.1, label %if.then.1, label %for.cond.2.loopexit.1
}

; Function Attrs: nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #2

; Function Attrs: nounwind readonly
declare i32 @strcmp(i8* nocapture, i8* nocapture) #3

; Function Attrs: nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #1

; Function Attrs: nounwind
declare i8* @strcpy(i8*, i8* nocapture readonly) #2

; Function Attrs: nounwind
declare i32 @puts(i8* nocapture readonly) #2

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readonly "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
