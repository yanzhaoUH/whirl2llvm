; ModuleID = 'test56.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [25 x i8] c"Enter a line of string: \00", align 1
@.str.1 = private unnamed_addr constant [6 x i8] c"%[^\0A]\00", align 1
@.str.2 = private unnamed_addr constant [11 x i8] c"Vowels: %d\00", align 1
@.str.3 = private unnamed_addr constant [16 x i8] c"\0AConsonants: %d\00", align 1
@.str.4 = private unnamed_addr constant [12 x i8] c"\0ADigits: %d\00", align 1
@.str.5 = private unnamed_addr constant [18 x i8] c"\0AWhite spaces: %d\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %line = alloca [150 x i8], align 16
  %i = alloca i32, align 4
  %vowels = alloca i32, align 4
  %consonants = alloca i32, align 4
  %digits = alloca i32, align 4
  %spaces = alloca i32, align 4
  store i32 0, i32* %retval
  store i32 0, i32* %spaces, align 4
  store i32 0, i32* %digits, align 4
  store i32 0, i32* %consonants, align 4
  store i32 0, i32* %vowels, align 4
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str, i32 0, i32 0))
  %arraydecay = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i32 0
  %call1 = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0), i8* %arraydecay)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %idxprom = sext i32 %0 to i64
  %arrayidx = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom
  %1 = load i8, i8* %arrayidx, align 1
  %conv = sext i8 %1 to i32
  %cmp = icmp ne i32 %conv, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %i, align 4
  %idxprom3 = sext i32 %2 to i64
  %arrayidx4 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom3
  %3 = load i8, i8* %arrayidx4, align 1
  %conv5 = sext i8 %3 to i32
  %cmp6 = icmp eq i32 %conv5, 97
  br i1 %cmp6, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %idxprom8 = sext i32 %4 to i64
  %arrayidx9 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom8
  %5 = load i8, i8* %arrayidx9, align 1
  %conv10 = sext i8 %5 to i32
  %cmp11 = icmp eq i32 %conv10, 101
  br i1 %cmp11, label %if.then, label %lor.lhs.false.13

lor.lhs.false.13:                                 ; preds = %lor.lhs.false
  %6 = load i32, i32* %i, align 4
  %idxprom14 = sext i32 %6 to i64
  %arrayidx15 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom14
  %7 = load i8, i8* %arrayidx15, align 1
  %conv16 = sext i8 %7 to i32
  %cmp17 = icmp eq i32 %conv16, 105
  br i1 %cmp17, label %if.then, label %lor.lhs.false.19

lor.lhs.false.19:                                 ; preds = %lor.lhs.false.13
  %8 = load i32, i32* %i, align 4
  %idxprom20 = sext i32 %8 to i64
  %arrayidx21 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom20
  %9 = load i8, i8* %arrayidx21, align 1
  %conv22 = sext i8 %9 to i32
  %cmp23 = icmp eq i32 %conv22, 111
  br i1 %cmp23, label %if.then, label %lor.lhs.false.25

lor.lhs.false.25:                                 ; preds = %lor.lhs.false.19
  %10 = load i32, i32* %i, align 4
  %idxprom26 = sext i32 %10 to i64
  %arrayidx27 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom26
  %11 = load i8, i8* %arrayidx27, align 1
  %conv28 = sext i8 %11 to i32
  %cmp29 = icmp eq i32 %conv28, 117
  br i1 %cmp29, label %if.then, label %lor.lhs.false.31

lor.lhs.false.31:                                 ; preds = %lor.lhs.false.25
  %12 = load i32, i32* %i, align 4
  %idxprom32 = sext i32 %12 to i64
  %arrayidx33 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom32
  %13 = load i8, i8* %arrayidx33, align 1
  %conv34 = sext i8 %13 to i32
  %cmp35 = icmp eq i32 %conv34, 65
  br i1 %cmp35, label %if.then, label %lor.lhs.false.37

lor.lhs.false.37:                                 ; preds = %lor.lhs.false.31
  %14 = load i32, i32* %i, align 4
  %idxprom38 = sext i32 %14 to i64
  %arrayidx39 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom38
  %15 = load i8, i8* %arrayidx39, align 1
  %conv40 = sext i8 %15 to i32
  %cmp41 = icmp eq i32 %conv40, 69
  br i1 %cmp41, label %if.then, label %lor.lhs.false.43

lor.lhs.false.43:                                 ; preds = %lor.lhs.false.37
  %16 = load i32, i32* %i, align 4
  %idxprom44 = sext i32 %16 to i64
  %arrayidx45 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom44
  %17 = load i8, i8* %arrayidx45, align 1
  %conv46 = sext i8 %17 to i32
  %cmp47 = icmp eq i32 %conv46, 73
  br i1 %cmp47, label %if.then, label %lor.lhs.false.49

lor.lhs.false.49:                                 ; preds = %lor.lhs.false.43
  %18 = load i32, i32* %i, align 4
  %idxprom50 = sext i32 %18 to i64
  %arrayidx51 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom50
  %19 = load i8, i8* %arrayidx51, align 1
  %conv52 = sext i8 %19 to i32
  %cmp53 = icmp eq i32 %conv52, 79
  br i1 %cmp53, label %if.then, label %lor.lhs.false.55

lor.lhs.false.55:                                 ; preds = %lor.lhs.false.49
  %20 = load i32, i32* %i, align 4
  %idxprom56 = sext i32 %20 to i64
  %arrayidx57 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom56
  %21 = load i8, i8* %arrayidx57, align 1
  %conv58 = sext i8 %21 to i32
  %cmp59 = icmp eq i32 %conv58, 85
  br i1 %cmp59, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false.55, %lor.lhs.false.49, %lor.lhs.false.43, %lor.lhs.false.37, %lor.lhs.false.31, %lor.lhs.false.25, %lor.lhs.false.19, %lor.lhs.false.13, %lor.lhs.false, %for.body
  %22 = load i32, i32* %vowels, align 4
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %vowels, align 4
  br label %if.end.109

if.else:                                          ; preds = %lor.lhs.false.55
  %23 = load i32, i32* %i, align 4
  %idxprom61 = sext i32 %23 to i64
  %arrayidx62 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom61
  %24 = load i8, i8* %arrayidx62, align 1
  %conv63 = sext i8 %24 to i32
  %cmp64 = icmp sge i32 %conv63, 97
  br i1 %cmp64, label %land.lhs.true, label %lor.lhs.false.71

land.lhs.true:                                    ; preds = %if.else
  %25 = load i32, i32* %i, align 4
  %idxprom66 = sext i32 %25 to i64
  %arrayidx67 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom66
  %26 = load i8, i8* %arrayidx67, align 1
  %conv68 = sext i8 %26 to i32
  %cmp69 = icmp sle i32 %conv68, 122
  br i1 %cmp69, label %if.then.83, label %lor.lhs.false.71

lor.lhs.false.71:                                 ; preds = %land.lhs.true, %if.else
  %27 = load i32, i32* %i, align 4
  %idxprom72 = sext i32 %27 to i64
  %arrayidx73 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom72
  %28 = load i8, i8* %arrayidx73, align 1
  %conv74 = sext i8 %28 to i32
  %cmp75 = icmp sge i32 %conv74, 65
  br i1 %cmp75, label %land.lhs.true.77, label %if.else.85

land.lhs.true.77:                                 ; preds = %lor.lhs.false.71
  %29 = load i32, i32* %i, align 4
  %idxprom78 = sext i32 %29 to i64
  %arrayidx79 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom78
  %30 = load i8, i8* %arrayidx79, align 1
  %conv80 = sext i8 %30 to i32
  %cmp81 = icmp sle i32 %conv80, 90
  br i1 %cmp81, label %if.then.83, label %if.else.85

if.then.83:                                       ; preds = %land.lhs.true.77, %land.lhs.true
  %31 = load i32, i32* %consonants, align 4
  %inc84 = add nsw i32 %31, 1
  store i32 %inc84, i32* %consonants, align 4
  br label %if.end.108

if.else.85:                                       ; preds = %land.lhs.true.77, %lor.lhs.false.71
  %32 = load i32, i32* %i, align 4
  %idxprom86 = sext i32 %32 to i64
  %arrayidx87 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom86
  %33 = load i8, i8* %arrayidx87, align 1
  %conv88 = sext i8 %33 to i32
  %cmp89 = icmp sge i32 %conv88, 48
  br i1 %cmp89, label %land.lhs.true.91, label %if.else.99

land.lhs.true.91:                                 ; preds = %if.else.85
  %34 = load i32, i32* %i, align 4
  %idxprom92 = sext i32 %34 to i64
  %arrayidx93 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom92
  %35 = load i8, i8* %arrayidx93, align 1
  %conv94 = sext i8 %35 to i32
  %cmp95 = icmp sle i32 %conv94, 57
  br i1 %cmp95, label %if.then.97, label %if.else.99

if.then.97:                                       ; preds = %land.lhs.true.91
  %36 = load i32, i32* %digits, align 4
  %inc98 = add nsw i32 %36, 1
  store i32 %inc98, i32* %digits, align 4
  br label %if.end.107

if.else.99:                                       ; preds = %land.lhs.true.91, %if.else.85
  %37 = load i32, i32* %i, align 4
  %idxprom100 = sext i32 %37 to i64
  %arrayidx101 = getelementptr inbounds [150 x i8], [150 x i8]* %line, i32 0, i64 %idxprom100
  %38 = load i8, i8* %arrayidx101, align 1
  %conv102 = sext i8 %38 to i32
  %cmp103 = icmp eq i32 %conv102, 32
  br i1 %cmp103, label %if.then.105, label %if.end

if.then.105:                                      ; preds = %if.else.99
  %39 = load i32, i32* %spaces, align 4
  %inc106 = add nsw i32 %39, 1
  store i32 %inc106, i32* %spaces, align 4
  br label %if.end

if.end:                                           ; preds = %if.then.105, %if.else.99
  br label %if.end.107

if.end.107:                                       ; preds = %if.end, %if.then.97
  br label %if.end.108

if.end.108:                                       ; preds = %if.end.107, %if.then.83
  br label %if.end.109

if.end.109:                                       ; preds = %if.end.108, %if.then
  br label %for.inc

for.inc:                                          ; preds = %if.end.109
  %40 = load i32, i32* %i, align 4
  %inc110 = add nsw i32 %40, 1
  store i32 %inc110, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %41 = load i32, i32* %vowels, align 4
  %call111 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.2, i32 0, i32 0), i32 %41)
  %42 = load i32, i32* %consonants, align 4
  %call112 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.3, i32 0, i32 0), i32 %42)
  %43 = load i32, i32* %digits, align 4
  %call113 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.4, i32 0, i32 0), i32 %43)
  %44 = load i32, i32* %spaces, align 4
  %call114 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.5, i32 0, i32 0), i32 %44)
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

declare i32 @__isoc99_scanf(i8*, ...) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
