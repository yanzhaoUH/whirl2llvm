	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test31.c (test31.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test31.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# number = 0
	# i = 4
	# _temp_gra_spill1 = 8
	.loc	1	2	0
 #   1  #include <stdio.h>
 #   2  int main()
.LBB1_main:
.LEH_adjustsp_main:
.LEH_csr_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
	movq %rbx,8(%rsp)             	# [0] _temp_gra_spill1
.L_0_4098:
	xorl %eax,%eax                	# [0] 
	.loc	1	6	0
 #   3  {
 #   4      int number,i;
 #   5  
 #   6      printf("Enter a positive integer: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	7	0
 #   7      scanf("%d",&number);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	9	0
 #   8      //number = 60;
 #   9      printf("Factors of %d are: ", number);
	movl 0(%rsp),%esi             	# [1] number
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call printf                   	# [1] printf
.LBB5_main:
	movl 0(%rsp),%esi             	# [0] number
	cmpl $1,%esi                  	# [3] 
	jl .Lt_0_1282                 	# [4] 
.LBB6_main:
	movl $1,%ebx                  	# [0] 
.Lt_0_2818:
.Lt_0_3330:
 #<loop> Loop body line 13
	.loc	1	13	0
 #  10  
 #  11      for(i=1; i <= number; ++i)
 #  12      {
 #  13          if (number%i == 0)
	movl %esi,%eax                	# [0] 
	cltd                          	# [1] 
	idivl %ebx                    	# [2] 
	testl %edx,%edx               	# [24] 
	je .LBB13_main                	# [25] 
.Lt_0_3074:
 #<loop> Part of loop body line 13, head labeled .Lt_0_2818
	.loc	1	15	0
 #  14          {
 #  15              printf("%d ",i);
	addl $1,%ebx                  	# [0] 
	cmpl %esi,%ebx                	# [1] 
	jle .Lt_0_2818                	# [2] 
.Lt_0_1282:
	xorq %rax,%rax                	# [0] 
	.loc	1	19	0
 #  16          }
 #  17      }
 #  18  
 #  19      return 0;
	movq 8(%rsp),%rbx             	# [1] _temp_gra_spill1
	addq $24,%rsp                 	# [1] 
	ret                           	# [1] 
.LBB13_main:
 #<loop> Part of loop body line 13, head labeled .Lt_0_2818
	xorl %eax,%eax                	# [0] 
	.loc	1	15	0
	movl %ebx,%esi                	# [1] 
	movq $(.rodata+80),%rdi       	# [1] .rodata+80
	call printf                   	# [1] printf
.LBB14_main:
 #<loop> Part of loop body line 13, head labeled .Lt_0_2818
	movl 0(%rsp),%esi             	# [0] number
	addl $1,%ebx                  	# [2] 
	cmpl %esi,%ebx                	# [3] 
	jle .Lt_0_2818                	# [4] 
.LBB22_main:
	jmp .Lt_0_1282                	# [0] 
.L_0_4354:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter a positive integer: "
	.org 0x20
	.align	0
	# offset 32
	.string "%d"
	.org 0x30
	.align	0
	# offset 48
	.string "Factors of %d are: "
	.org 0x50
	.align	0
	# offset 80
	.string "%d "

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_4098-main
	.uleb128	.L_0_4354-.L_0_4098
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20, 0x04
	.4byte	.LEH_csr_main - .LEH_adjustsp_main
	.byte	0x83, 0x03
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test31.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

