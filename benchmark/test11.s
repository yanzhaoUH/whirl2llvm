	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test11.c (test11.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test11.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 40, %rsp
	# n1 = 0
	# n2 = 8
	# n3 = 16
	.loc	1	2	0
 #   1  #include <stdio.h>
 #   2  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-40,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_11010:
	xorl %eax,%eax                	# [0] 
	.loc	1	6	0
 #   3  {
 #   4      double n1, n2, n3;
 #   5      //printf("Enter three numbers: ");
 #   6      scanf("%lf %lf %lf", &n1, &n2, &n3);
	leaq 16(%rsp),%rcx            	# [0] n3
	leaq 8(%rsp),%rdx             	# [0] n2
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata),%rdi          	# [1] .rodata
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB3_main:
	.loc	1	8	0
 #   7  
 #   8      if( n1>=n2 && n1>=n3 )
	movsd 8(%rsp),%xmm4           	# [0] n2
	movsd 0(%rsp),%xmm1           	# [1] n1
	comisd %xmm4,%xmm1            	# [4] 
	movsd 16(%rsp),%xmm3          	# [8] n3
	jb .L_0_4098                  	# [8] 
.LBB4_main:
	comisd %xmm3,%xmm1            	# [0] 
	jae .Lt_0_7938                	# [4] 
.L_0_4098:
	.loc	1	11	0
 #   9          printf("%.2f is the largest number.", n1);
 #  10  
 #  11      if( n2>=n1 && n2>=n3 )
	comisd %xmm1,%xmm4            	# [0] 
	jb .L_0_5122                  	# [4] 
.LBB10_main:
	comisd %xmm3,%xmm4            	# [0] 
	jae .Lt_0_8706                	# [4] 
.L_0_5122:
	.loc	1	14	0
 #  12          printf("%.2f is the largest number.", n2);
 #  13  
 #  14      if( n3>=n1 && n3>=n2 )
	comisd %xmm1,%xmm3            	# [0] 
	jb .L_0_6146                  	# [4] 
.LBB16_main:
	movsd 8(%rsp),%xmm0           	# [0] n2
	comisd %xmm0,%xmm3            	# [3] 
	jae .Lt_0_9218                	# [7] 
.L_0_6146:
	xorq %rax,%rax                	# [0] 
	.loc	1	18	0
 #  15          printf("%.2f is the largest number.", n3);
 #  16  
 #  17     
 #  18      return 0;
	addq $40,%rsp                 	# [0] 
	ret                           	# [0] 
.Lt_0_9218:
	.loc	1	15	0
	movl $1,%eax                  	# [0] 
	movaps %xmm3,%xmm0            	# [1] 
	movq $(.rodata+16),%rdi       	# [1] .rodata+16
	.globl	printf
	call printf                   	# [1] printf
.LBB23_main:
	jmp .L_0_6146                 	# [0] 
.Lt_0_7938:
	.loc	1	9	0
	movl $1,%eax                  	# [0] 
	movaps %xmm1,%xmm0            	# [1] 
	movq $(.rodata+16),%rdi       	# [1] .rodata+16
	call printf                   	# [1] printf
.LBB8_main:
	movsd 0(%rsp),%xmm1           	# [0] n1
	movsd 8(%rsp),%xmm4           	# [1] n2
	movsd 16(%rsp),%xmm3          	# [2] n3
	jmp .L_0_4098                 	# [2] 
.Lt_0_8706:
	.loc	1	12	0
	movl $1,%eax                  	# [0] 
	movaps %xmm4,%xmm0            	# [1] 
	movq $(.rodata+16),%rdi       	# [1] .rodata+16
	call printf                   	# [1] printf
.LBB14_main:
	movsd 0(%rsp),%xmm1           	# [0] n1
	movsd 16(%rsp),%xmm3          	# [1] n3
	jmp .L_0_5122                 	# [1] 
.L_0_11266:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "%lf %lf %lf"
	.org 0x10
	.align	0
	# offset 16
	.string "%.2f is the largest number."

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_11010-main
	.uleb128	.L_0_11266-.L_0_11010
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x30
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test11.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

