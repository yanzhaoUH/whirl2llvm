	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test62.c (test62.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test62.c"


	.text
	.align	2

	.section .bss, "wa",@nobits

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits

	.section .bss
	.org 0x0
	.align	4
.globl	s
	.type	s, @object
	.size	s, 60
s:	# 0x0
	.skip 60
	.org 0x3c
	.align	4
.globl	g_i
	.type	g_i, @object
	.size	g_i, 4
g_i:	# 0x3c
	.skip 4
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 8, %rsp
	.loc	1	14	0
 #  10  int g_i;
 #  11  
 #  12  
 #  13  
 #  14  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-8,%rsp                 	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_258:
	xorl %eax,%eax                	# [0] 
	.loc	1	21	0
 #  17  
 #  18     
 #  19  
 #  20  
 #  21  	    printf("Enter information:\n");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	23	0
 #  22  
 #  23  	    printf("Enter name: ");
	movq $(.rodata+32),%rdi       	# [0] .rodata+32
	call printf                   	# [0] printf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	24	0
 #  24  	    scanf("%s", s.name);
	movq $(.bss),%rsi             	# [1] .bss
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	28	0
 #  25  
 #  26             //strcpy(s.name, "hello");
 #  27  
 #  28  	    printf("Enter roll number: ");
	movq $(.rodata+64),%rdi       	# [0] .rodata+64
	call printf                   	# [0] printf
.LBB6_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	29	0
 #  29  	    scanf("%d", &s.roll);
	movq $(.bss+52),%rsi          	# [1] .bss+52
	movq $(.rodata+96),%rdi       	# [1] .rodata+96
	call scanf                    	# [1] scanf
.LBB7_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	32	0
 #  30              //s.roll = 1;
 #  31  
 #  32  	    printf("Enter marks: ");
	movq $(.rodata+112),%rdi      	# [0] .rodata+112
	call printf                   	# [0] printf
.LBB8_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	33	0
 #  33  	    scanf("%f", &s.marks);
	movq $(.bss+56),%rsi          	# [1] .bss+56
	movq $(.rodata+128),%rdi      	# [1] .rodata+128
	call scanf                    	# [1] scanf
.LBB9_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	36	0
 #  34              //s.marks = 1.0f;
 #  35  
 #  36  	    printf("Displaying Information:\n");
	movq $(.rodata+144),%rdi      	# [0] .rodata+144
	call printf                   	# [0] printf
.LBB10_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	39	0
 #  37  
 #  38  	   
 #  39              printf("Name: ");
	movq $(.rodata+176),%rdi      	# [0] .rodata+176
	call printf                   	# [0] printf
.LBB11_main:
	.loc	1	40	0
 #  40  	    puts(s.name);
	movq $(.bss),%rdi             	# [0] .bss
	.globl	puts
	call puts                     	# [0] puts
.LBB12_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	42	0
 #  41  
 #  42  	    printf("Roll number: %d\n",s.roll);
	movl .bss+52(%rip),%esi       	# [1] id:5 s+0x34
	movq $(.rodata+192),%rdi      	# [1] .rodata+192
	call printf                   	# [1] printf
.LBB13_main:
	.loc	1	43	0
 #  43               printf("Marks: %.1f\n", s.marks);
	movss .bss+56(%rip),%xmm0     	# [0] id:6 s+0x38
	movl $1,%eax                  	# [2] 
	movq $(.rodata+224),%rdi      	# [3] .rodata+224
	cvtss2sd %xmm0,%xmm0          	# [3] 
	call printf                   	# [3] printf
.LBB14_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	46	0
 #  44     
 #  45  
 #  46      return 0;
	addq $8,%rsp                  	# [0] 
	ret                           	# [0] 
.L_0_514:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.byte	0x45, 0x6e, 0x74, 0x65, 0x72, 0x20, 0x69, 0x6e	# Enter in
	.byte	0x66, 0x6f, 0x72, 0x6d, 0x61, 0x74, 0x69, 0x6f	# formatio
	.byte	0x6e, 0x3a, 0xa, 0x0	# n:\n\000
	.org 0x20
	.align	0
	# offset 32
	.string "Enter name: "
	.org 0x30
	.align	0
	# offset 48
	.string "%s"
	.org 0x40
	.align	0
	# offset 64
	.string "Enter roll number: "
	.org 0x60
	.align	0
	# offset 96
	.string "%d"
	.org 0x70
	.align	0
	# offset 112
	.string "Enter marks: "
	.org 0x80
	.align	0
	# offset 128
	.string "%f"
	.org 0x90
	.align	0
	# offset 144
	.byte	0x44, 0x69, 0x73, 0x70, 0x6c, 0x61, 0x79, 0x69	# Displayi
	.byte	0x6e, 0x67, 0x20, 0x49, 0x6e, 0x66, 0x6f, 0x72	# ng Infor
	.byte	0x6d, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x3a, 0xa	# mation:\n
	.byte	0x0	# \000
	.org 0xb0
	.align	0
	# offset 176
	.string "Name: "
	.org 0xc0
	.align	0
	# offset 192
	.byte	0x52, 0x6f, 0x6c, 0x6c, 0x20, 0x6e, 0x75, 0x6d	# Roll num
	.byte	0x62, 0x65, 0x72, 0x3a, 0x20, 0x25, 0x64, 0xa	# ber: %d\n
	.byte	0x0	# \000
	.org 0xe0
	.align	0
	# offset 224
	.byte	0x4d, 0x61, 0x72, 0x6b, 0x73, 0x3a, 0x20, 0x25	# Marks: %
	.byte	0x2e, 0x31, 0x66, 0xa, 0x0	# .1f\n\000

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_258-main
	.uleb128	.L_0_514-.L_0_258
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .bss
	.align	16
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x10
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test62.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

