	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test27.c (test27.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test27.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# n = 0
	# i = 4
	.loc	1	2	0
 #   1  #include <stdio.h>
 #   2  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_4866:
	xorl %eax,%eax                	# [0] 
	.loc	1	6	0
 #   3  {
 #   4      int n, i, flag = 0;
 #   5  
 #   6      printf("Enter a positive integer: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	7	0
 #   7      scanf("%d",&n);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	movl 0(%rsp),%esi             	# [0] n
	movl %esi,%r8d                	# [3] 
	sarl $31,%r8d                 	# [4] 
	andl $1,%r8d                  	# [5] 
	addl %esi,%r8d                	# [6] 
	sarl $1,%r8d                  	# [7] 
	cmpl $2,%r8d                  	# [8] 
	jl .Lt_0_2050                 	# [9] 
.LBB5_main:
	movl $2,%ecx                  	# [0] 
.Lt_0_2562:
 #<loop> Loop body line 7, nesting depth: 1, estimated iterations: 100
	.loc	1	13	0
 #   9  
 #  10      for(i=2; i<=n/2; ++i)
 #  11      {
 #  12          // condition for nonprime number
 #  13          if(n%i==0)
	movl %esi,%eax                	# [0] 
	cltd                          	# [1] 
	idivl %ecx                    	# [2] 
	testl %edx,%edx               	# [24] 
	je .Lt_0_3330                 	# [25] 
.Lt_0_2818:
 #<loop> Part of loop body line 7, head labeled .Lt_0_2562
	.loc	1	16	0
 #  14          {
 #  15              flag=1;
 #  16              break;
	addl $1,%ecx                  	# [0] 
	cmpl %ecx,%r8d                	# [1] 
	jge .Lt_0_2562                	# [2] 
.Lt_0_2050:
	xorl %eax,%eax                	# [0] 
	.loc	1	21	0
 #  17          }
 #  18      }
 #  19  
 #  20      if (flag==0)
 #  21          printf("%d is a prime number.",n);
	movl %esi,%esi                	# [1] 
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call printf                   	# [1] printf
.LBB16_main:
	jmp .Lt_0_3586                	# [0] 
.Lt_0_3330:
	xorl %eax,%eax                	# [0] 
	.loc	1	23	0
 #  22      else
 #  23          printf("%d is not a prime number.",n);
	movl %esi,%esi                	# [1] 
	movq $(.rodata+80),%rdi       	# [1] .rodata+80
	call printf                   	# [1] printf
.LBB20_main:
.Lt_0_3586:
	xorq %rax,%rax                	# [0] 
	.loc	1	25	0
 #  24      
 #  25      return 0;
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.L_0_5122:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter a positive integer: "
	.org 0x20
	.align	0
	# offset 32
	.string "%d"
	.org 0x30
	.align	0
	# offset 48
	.string "%d is a prime number."
	.org 0x50
	.align	0
	# offset 80
	.string "%d is not a prime number."

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_4866-main
	.uleb128	.L_0_5122-.L_0_4866
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test27.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

