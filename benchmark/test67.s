	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test67.c (test67.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test67.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 56, %rsp
	# i = 4
	# noOfRecords = 0
	# _temp_gra_spill2 = 8
	# _temp_gra_spill3 = 16
	# _temp_gra_spill4 = 24
	# _temp_gra_spill5 = 32
	.loc	1	11	0
 #   7     int marks;
 #   8     char subject[30];
 #   9  };
 #  10  
 #  11  int main()
.LBB1_main:
.LEH_adjustsp_main:
.LEH_csr_main:
	addq $-56,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
	movq %rbx,32(%rsp)            	# [0] _temp_gra_spill5
	movq %rbp,16(%rsp)            	# [0] _temp_gra_spill3
	movq %r12,8(%rsp)             	# [0] _temp_gra_spill2
	movq %r13,24(%rsp)            	# [0] _temp_gra_spill4
.L_0_3842:
	xorl %eax,%eax                	# [0] 
	.loc	1	15	0
 #  12  {
 #  13     struct course *ptr;
 #  14     int i, noOfRecords;
 #  15     printf("Enter number of records: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	16	0
 #  16     scanf("%d", &noOfRecords);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	.loc	1	21	0
 #  17     
 #  18     // noOfRecords = 3;
 #  19  
 #  20     // Allocates the memory for noOfRecords structures with pointer ptr pointing to the base address.
 #  21     ptr = (struct course*) malloc (noOfRecords * sizeof(struct course));
	movslq 0(%rsp),%rdi           	# [0] noOfRecords
	imulq $36,%rdi                	# [3] 
	.globl	malloc
	call malloc                   	# [3] malloc
.LBB5_main:
	movq %rax,%r13                	# [0] 
	movl 0(%rsp),%eax             	# [0] noOfRecords
	cmpl $0,%eax                  	# [3] 
	jle .Lt_0_1282                	# [4] 
.LBB6_main:
	movq %r13,%rbp                	# [0] 
	xorl %r12d,%r12d              	# [0] 
	leaq 4(%r13),%rbx             	# [0] 
.Lt_0_1794:
 #<loop> Loop body line 21, nesting depth: 1, estimated iterations: 100
	xorl %eax,%eax                	# [0] 
	.loc	1	25	0
 #  22  
 #  23     for(i = 0; i < noOfRecords; ++i)
 #  24     {
 #  25         printf("Enter name of the subject and marks respectively:\n");
	movq $(.rodata+48),%rdi       	# [0] .rodata+48
	call printf                   	# [0] printf
.LBB8_main:
 #<loop> Part of loop body line 21, head labeled .Lt_0_1794
	xorl %eax,%eax                	# [0] 
	.loc	1	26	0
 #  26         scanf("%s %d", &(ptr+i)->subject, &(ptr+i)->marks);
	movq %rbp,%rdx                	# [0] 
	movq %rbx,%rsi                	# [1] 
	movq $(.rodata+112),%rdi      	# [1] .rodata+112
	call scanf                    	# [1] scanf
.LBB9_main:
 #<loop> Part of loop body line 21, head labeled .Lt_0_1794
	movl 0(%rsp),%eax             	# [0] noOfRecords
	addl $1,%r12d                 	# [2] 
	addq $36,%rbx                 	# [3] 
	addq $36,%rbp                 	# [3] 
	cmpl %r12d,%eax               	# [3] 
	jg .Lt_0_1794                 	# [4] 
.Lt_0_1282:
	xorl %eax,%eax                	# [0] 
	.loc	1	31	0
 #  27         //strcpy((ptr+i)->subject, "math");
 #  28         //(ptr+i)->marks = 10;
 #  29     }
 #  30  
 #  31     printf("Displaying Information:\n");
	movq $(.rodata+128),%rdi      	# [0] .rodata+128
	call printf                   	# [0] printf
.LBB11_main:
	movl 0(%rsp),%eax             	# [0] noOfRecords
	cmpl $0,%eax                  	# [3] 
	jle .Lt_0_2306                	# [4] 
.LBB12_main:
	movq %r13,%rbp                	# [0] 
	xorl %r12d,%r12d              	# [0] 
	leaq 4(%r13),%rbx             	# [0] 
.Lt_0_2818:
 #<loop> Loop body line 31, nesting depth: 1, estimated iterations: 100
	xorl %eax,%eax                	# [0] 
	.loc	1	34	0
 #  32  
 #  33     for(i = 0; i < noOfRecords ; ++i)
 #  34         printf("%s\t%d\n", (ptr+i)->subject, (ptr+i)->marks);
	movl 0(%rbp),%edx             	# [0] id:30
	movq %rbx,%rsi                	# [1] 
	movq $(.rodata+160),%rdi      	# [1] .rodata+160
	call printf                   	# [1] printf
.LBB14_main:
 #<loop> Part of loop body line 31, head labeled .Lt_0_2818
	movl 0(%rsp),%eax             	# [0] noOfRecords
	addl $1,%r12d                 	# [2] 
	addq $36,%rbx                 	# [3] 
	addq $36,%rbp                 	# [3] 
	cmpl %r12d,%eax               	# [3] 
	jg .Lt_0_2818                 	# [4] 
.Lt_0_2306:
	xorq %rax,%rax                	# [0] 
	.loc	1	36	0
 #  35  
 #  36     return 0;
	movq 32(%rsp),%rbx            	# [1] _temp_gra_spill5
	movq 16(%rsp),%rbp            	# [1] _temp_gra_spill3
	movq 8(%rsp),%r12             	# [1] _temp_gra_spill2
	movq 24(%rsp),%r13            	# [2] _temp_gra_spill4
	addq $56,%rsp                 	# [2] 
	ret                           	# [2] 
.L_0_4098:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter number of records: "
	.org 0x20
	.align	0
	# offset 32
	.string "%d"
	.org 0x30
	.align	0
	# offset 48
	.byte	0x45, 0x6e, 0x74, 0x65, 0x72, 0x20, 0x6e, 0x61	# Enter na
	.byte	0x6d, 0x65, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68	# me of th
	.byte	0x65, 0x20, 0x73, 0x75, 0x62, 0x6a, 0x65, 0x63	# e subjec
	.byte	0x74, 0x20, 0x61, 0x6e, 0x64, 0x20, 0x6d, 0x61	# t and ma
	.byte	0x72, 0x6b, 0x73, 0x20, 0x72, 0x65, 0x73, 0x70	# rks resp
	.byte	0x65, 0x63, 0x74, 0x69, 0x76, 0x65, 0x6c, 0x79	# ectively
	.byte	0x3a, 0xa, 0x0	# :\n\000
	.org 0x70
	.align	0
	# offset 112
	.string "%s %d"
	.org 0x80
	.align	0
	# offset 128
	.byte	0x44, 0x69, 0x73, 0x70, 0x6c, 0x61, 0x79, 0x69	# Displayi
	.byte	0x6e, 0x67, 0x20, 0x49, 0x6e, 0x66, 0x6f, 0x72	# ng Infor
	.byte	0x6d, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x3a, 0xa	# mation:\n
	.byte	0x0	# \000
	.org 0xa0
	.align	0
	# offset 160
	.byte	0x25, 0x73, 0x9, 0x25, 0x64, 0xa, 0x0	# %s\t%d\n\000

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_3842-main
	.uleb128	.L_0_4098-.L_0_3842
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x40, 0x04
	.4byte	.LEH_csr_main - .LEH_adjustsp_main
	.byte	0x8c, 0x07, 0x86, 0x06, 0x8d, 0x05, 0x83, 0x04
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test67.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

