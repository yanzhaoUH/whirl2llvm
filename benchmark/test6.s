	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test6.c (test6.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test6.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 8, %rsp
	.loc	1	2	0
 #   1  #include <stdio.h>
 #   2  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-8,%rsp                 	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_258:
	xorl %eax,%eax                	# [0] 
	.loc	1	10	0
 #   6      double doubleType;
 #   7      char charType;
 #   8  
 #   9      // Sizeof operator is used to evaluate the size of a variable
 #  10      printf("Size of int: %ld bytes\n",sizeof(integerType));
	movq $4,%rsi                  	# [1] 
	movq $(.rodata),%rdi          	# [1] .rodata
	.globl	printf
	call printf                   	# [1] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	11	0
 #  11      printf("Size of float: %ld bytes\n",sizeof(floatType));
	movq $4,%rsi                  	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	call printf                   	# [1] printf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	12	0
 #  12      printf("Size of double: %ld bytes\n",sizeof(doubleType));
	movq $8,%rsi                  	# [1] 
	movq $(.rodata+64),%rdi       	# [1] .rodata+64
	call printf                   	# [1] printf
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	13	0
 #  13      printf("Size of char: %ld byte\n",sizeof(charType));
	movq $1,%rsi                  	# [1] 
	movq $(.rodata+96),%rdi       	# [1] .rodata+96
	call printf                   	# [1] printf
.LBB6_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	15	0
 #  14  
 #  15      return 0;
	addq $8,%rsp                  	# [0] 
	ret                           	# [0] 
.L_0_514:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.byte	0x53, 0x69, 0x7a, 0x65, 0x20, 0x6f, 0x66, 0x20	# Size of 
	.byte	0x69, 0x6e, 0x74, 0x3a, 0x20, 0x25, 0x6c, 0x64	# int: %ld
	.byte	0x20, 0x62, 0x79, 0x74, 0x65, 0x73, 0xa, 0x0	#  bytes\n\000
	# 
	.org 0x20
	.align	0
	# offset 32
	.byte	0x53, 0x69, 0x7a, 0x65, 0x20, 0x6f, 0x66, 0x20	# Size of 
	.byte	0x66, 0x6c, 0x6f, 0x61, 0x74, 0x3a, 0x20, 0x25	# float: %
	.byte	0x6c, 0x64, 0x20, 0x62, 0x79, 0x74, 0x65, 0x73	# ld bytes
	.byte	0xa, 0x0	# \n\000
	.org 0x40
	.align	0
	# offset 64
	.byte	0x53, 0x69, 0x7a, 0x65, 0x20, 0x6f, 0x66, 0x20	# Size of 
	.byte	0x64, 0x6f, 0x75, 0x62, 0x6c, 0x65, 0x3a, 0x20	# double: 
	.byte	0x25, 0x6c, 0x64, 0x20, 0x62, 0x79, 0x74, 0x65	# %ld byte
	.byte	0x73, 0xa, 0x0	# s\n\000
	.org 0x60
	.align	0
	# offset 96
	.byte	0x53, 0x69, 0x7a, 0x65, 0x20, 0x6f, 0x66, 0x20	# Size of 
	.byte	0x63, 0x68, 0x61, 0x72, 0x3a, 0x20, 0x25, 0x6c	# char: %l
	.byte	0x64, 0x20, 0x62, 0x79, 0x74, 0x65, 0xa, 0x0	# d byte\n\000
	# 

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_258-main
	.uleb128	.L_0_514-.L_0_258
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x10
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test6.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

