	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test47.c (test47.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test47.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 56, %rsp
	# i = 40
	# data = 0
	.loc	1	6	0
 #   2  #include <math.h>
 #   3  
 #   4  float calculateSD(float data[]);
 #   5  
 #   6  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-56,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_1794:
	xorl %eax,%eax                	# [0] 
	.loc	1	11	0
 #   7  {
 #   8      int i;
 #   9      float data[10];
 #  10  
 #  11      printf("Enter elements: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %edx,%edx                	# [0] 
	movq %rsp,%rax                	# [0] 
	movl $2,%esi                  	# [0] 
.LBB14_main:
	.loc	1	14	0
 #  12      for(i=0; i < 10; ++i)
 #  13         // scanf("%f", &data[i]);
 #  14          data[i] = i+2;
	cvtsi2ss %esi,%xmm1           	# [0] 
	movl $3,%esi                  	# [0] 
	cvtsi2ss %esi,%xmm0           	# [1] 
	addq $8,%rax                  	# [3] 
	incl %edx                     	# [4] 
	movss %xmm1,-8(%rax)          	# [4] id:11 data+0x0
	incl %esi                     	# [5] 
	incl %edx                     	# [5] 
	movss %xmm0,-4(%rax)          	# [5] id:11 data+0x0
.LBB13_main:
.LBB12_main:
 #<loop> Loop body line 11, nesting depth: 1, iterations: 2
 #<loop> unrolled 4 times
	leal 1(%rsi),%edi             	# [0] 
	cvtsi2ss %esi,%xmm3           	# [2] 
	leal 1(%rdi),%esi             	# [2] 
	cvtsi2ss %edi,%xmm2           	# [3] 
	cvtsi2ss %esi,%xmm1           	# [4] 
	incl %esi                     	# [4] 
	addq $16,%rax                 	# [5] 
	cvtsi2ss %esi,%xmm0           	# [5] 
	movss %xmm3,-16(%rax)         	# [6] id:11 data+0x0
	addl $4,%edx                  	# [7] 
	movss %xmm2,-12(%rax)         	# [7] id:11 data+0x0
	incl %esi                     	# [8] 
	cmpl $9,%edx                  	# [8] 
	movss %xmm1,-8(%rax)          	# [8] id:11 data+0x0
	movss %xmm0,-4(%rax)          	# [9] id:11 data+0x0
	jle .LBB12_main               	# [9] 
.LBB11_main:
	.loc	1	16	0
 #  15  
 #  16      printf("\nStandard Deviation = %.6f", calculateSD(data));
	movq %rsp,%rdi                	# [0] 
	call _Z11calculateSDPf        	# [0] _Z11calculateSDPf
.LBB6_main:
	movl $1,%eax                  	# [0] 
	movaps %xmm0,%xmm0            	# [0] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	cvtss2sd %xmm0,%xmm0          	# [1] 
	call printf                   	# [1] printf
.LBB7_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	18	0
 #  17  
 #  18      return 0;
	addq $56,%rsp                 	# [0] 
	ret                           	# [0] 
.L_0_2050:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter elements: "
	.org 0x20
	.align	0
	# offset 32
	.byte	0xa, 0x53, 0x74, 0x61, 0x6e, 0x64, 0x61, 0x72	# \nStandar
	.byte	0x64, 0x20, 0x44, 0x65, 0x76, 0x69, 0x61, 0x74	# d Deviat
	.byte	0x69, 0x6f, 0x6e, 0x20, 0x3d, 0x20, 0x25, 0x2e	# ion = %.
	.byte	0x36, 0x66, 0x0	# 6f\000

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_1794-main
	.uleb128	.L_0_2050-.L_0_1794
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.p2align 5,,

	# Program Unit: _Z11calculateSDPf
.globl	_Z11calculateSDPf
	.type	_Z11calculateSDPf, @function
_Z11calculateSDPf:	# 0xa8
	# .frame	%rsp, 72, %rsp
	# sum = 0
	# standardDeviation = 4
	# i = 8
	# _temp_gra_spill1 = 16
	# _temp_gra_spill2 = 24
	# _temp_gra_spill3 = 32
	.loc	1	21	0
 #  19  }
 #  20  
 #  21  float calculateSD(float data[])
.LBB1__Z11calculateSDPf:
	addq $-72,%rsp                	# [0] 
	xorps %xmm0,%xmm0             	# [0] 
	movq %rbx,24(%rsp)            	# [1] _temp_gra_spill2
	movq %rbp,16(%rsp)            	# [1] _temp_gra_spill1
	xorl %ebp,%ebp                	# [1] 
.LBB17__Z11calculateSDPf:
	.loc	1	29	0
 #  25      int i;
 #  26  
 #  27      for(i=0; i<10; ++i)
 #  28      {
 #  29          sum += data[i];
	movss 0(%rdi),%xmm2           	# [0] id:39
	addss %xmm0,%xmm2             	# [3] 
	movss 4(%rdi),%xmm1           	# [4] id:39
	incl %ebp                     	# [6] 
	movaps %xmm2,%xmm0            	# [6] 
	leaq 8(%rdi),%rbx             	# [7] 
	incl %ebp                     	# [7] 
	addss %xmm1,%xmm0             	# [7] 
.LBB16__Z11calculateSDPf:
.LBB15__Z11calculateSDPf:
 #<loop> Loop body line 21, nesting depth: 1, iterations: 2
 #<loop> unrolled 4 times
	movss 0(%rbx),%xmm4           	# [0] id:39
	addss %xmm0,%xmm4             	# [3] 
	movss 4(%rbx),%xmm3           	# [4] id:39
	movss 8(%rbx),%xmm2           	# [5] id:39
	movaps %xmm4,%xmm0            	# [6] 
	addss %xmm3,%xmm0             	# [7] 
	movss 12(%rbx),%xmm1          	# [9] id:39
	addss %xmm2,%xmm0             	# [10] 
	addl $4,%ebp                  	# [11] 
	addq $16,%rbx                 	# [12] 
	cmpl $9,%ebp                  	# [12] 
	addss %xmm1,%xmm0             	# [13] 
	jle .LBB15__Z11calculateSDPf  	# [13] 
.LBB14__Z11calculateSDPf:
	movss .rodata+64(%rip),%xmm2  	# [0] 
	movaps %xmm0,%xmm1            	# [2] 
	divss %xmm2,%xmm1             	# [3] 
	xorps %xmm3,%xmm3             	# [19] 
	movss %xmm3,4(%rsp)           	# [20] standardDeviation
	xorl %ebp,%ebp                	# [21] 
	movq %rdi,%rbx                	# [21] 
	movsd %xmm1,32(%rsp)          	# [21] _temp_gra_spill3
.Lt_1_3330:
 #<loop> Loop body line 29, nesting depth: 1, iterations: 10
	.loc	1	37	0
 #  33      
 #  34      mean = sum/10;
 #  35  
 #  36      for(i=0; i<10; ++i)
 #  37          standardDeviation += pow(data[i] - mean, 2);
	movsd 32(%rsp),%xmm2          	# [0] _temp_gra_spill3
	movss 0(%rbx),%xmm0           	# [1] id:40
	subss %xmm2,%xmm0             	# [4] 
	movsd .rodata+72(%rip),%xmm1  	# [7] 
	cvtss2sd %xmm0,%xmm0          	# [7] 
	.globl	pow
	call pow                      	# [7] pow
.LBB5__Z11calculateSDPf:
 #<loop> Part of loop body line 29, head labeled .Lt_1_3330
	movss 4(%rsp),%xmm1           	# [0] standardDeviation
	cvtss2sd %xmm1,%xmm2          	# [3] 
	addsd %xmm2,%xmm0             	# [4] 
	cvtsd2ss %xmm0,%xmm1          	# [7] 
	addl $1,%ebp                  	# [9] 
	addq $4,%rbx                  	# [10] 
	cmpl $9,%ebp                  	# [10] 
	movss %xmm1,4(%rsp)           	# [11] standardDeviation
	jle .Lt_1_3330                	# [11] 
.LBB19__Z11calculateSDPf:
	.loc	1	39	0
 #  38     
 #  39      return sqrt(standardDeviation/10);
	movss .rodata+64(%rip),%xmm0  	# [0] 
	movaps %xmm1,%xmm3            	# [2] 
	divss %xmm0,%xmm3             	# [3] 
	sqrtss %xmm3,%xmm1            	# [21] 
	comiss %xmm1,%xmm1            	# [50] 
	jne .LBB9__Z11calculateSDPf   	# [54] 
.LBB7__Z11calculateSDPf:
	comiss %xmm1,%xmm1            	# [0] 
	jp .LBB9__Z11calculateSDPf    	# [4] 
.Lt_1_4098:
	movaps %xmm1,%xmm0            	# [0] 
	movq 24(%rsp),%rbx            	# [0] _temp_gra_spill2
	movq 16(%rsp),%rbp            	# [1] _temp_gra_spill1
	addq $72,%rsp                 	# [1] 
	ret                           	# [1] 
.LBB9__Z11calculateSDPf:
	movaps %xmm3,%xmm0            	# [0] 
	.globl	sqrtf
	call sqrtf                    	# [0] sqrtf
.LBB10__Z11calculateSDPf:
	movq 24(%rsp),%rbx            	# [0] _temp_gra_spill2
	movq 16(%rsp),%rbp            	# [0] _temp_gra_spill1
	movaps %xmm0,%xmm0            	# [0] 
	movaps %xmm0,%xmm0            	# [1] 
	addq $72,%rsp                 	# [1] 
	ret                           	# [1] 
.LDWend__Z11calculateSDPf:
	.size _Z11calculateSDPf, .LDWend__Z11calculateSDPf-_Z11calculateSDPf

	.section .rodata
	.org 0x3c
	.align	0
	# offset 60
	.4byte	0
	# float 0.00000
	.org 0x40
	.align	0
	# offset 64
	.4byte	1092616192
	# float 10.0000
	.org 0x48
	.align	0
	# offset 72
	.4byte	0
	.4byte	1073741824
	# double 2.00000
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x40
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test47.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

