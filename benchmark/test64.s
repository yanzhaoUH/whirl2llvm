	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test64.c (test64.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test64.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 56, %rsp
	# n1 = 0
	# n2 = 16
	# .anon_1 = 32
	.loc	1	9	0
 #   5      float imag;
 #   6  } complex;
 #   7  complex add(complex n1,complex n2);
 #   8  
 #   9  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-56,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_258:
	xorl %eax,%eax                	# [0] 
	.loc	1	13	0
 #  10  {
 #  11      complex n1, n2, temp;
 #  12  
 #  13      printf("For 1st complex number \n");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	14	0
 #  14      printf("Enter real and imaginary part respectively:\n");
	movq $(.rodata+32),%rdi       	# [0] .rodata+32
	call printf                   	# [0] printf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	15	0
 #  15      scanf("%f %f", &n1.real, &n1.imag);
	leaq 4(%rsp),%rdx             	# [0] n1+4
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+80),%rdi       	# [1] .rodata+80
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	19	0
 #  16     //n1.real = 1.0f;
 #  17     //n1.imag  = 2.0f;
 #  18   
 #  19      printf("\nFor 2nd complex number \n");
	movq $(.rodata+96),%rdi       	# [0] .rodata+96
	call printf                   	# [0] printf
.LBB6_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	20	0
 #  20      printf("Enter real and imaginary part respectively:\n");
	movq $(.rodata+32),%rdi       	# [0] .rodata+32
	call printf                   	# [0] printf
.LBB7_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	21	0
 #  21      scanf("%f %f", &n2.real, &n2.imag);
	leaq 20(%rsp),%rdx            	# [0] n2+4
	leaq 16(%rsp),%rsi            	# [1] n2
	movq $(.rodata+80),%rdi       	# [1] .rodata+80
	call scanf                    	# [1] scanf
.LBB8_main:
	.loc	1	26	0
 #  22  
 #  23     //n2.real = 2.0f;
 #  24     //n2.imag  = 3.0f;
 #  25  
 #  26      temp = add(n1, n2);
	movsd 16(%rsp),%xmm1          	# [0] n2
	movsd 0(%rsp),%xmm0           	# [1] n1
	call _Z3add7complexS_         	# [1] _Z3add7complexS_
.LBB9_main:
	movsd %xmm0,32(%rsp)          	# [0] .anon_1
	.loc	1	27	0
 #  27      printf("Sum = %.1f + %.1fi", temp.real, temp.imag);
	movss 36(%rsp),%xmm1          	# [0] .anon_1+4
	movss 32(%rsp),%xmm0          	# [1] .anon_1
	movl $2,%eax                  	# [3] 
	cvtss2sd %xmm1,%xmm1          	# [3] 
	movq $(.rodata+128),%rdi      	# [4] .rodata+128
	cvtss2sd %xmm0,%xmm0          	# [4] 
	call printf                   	# [4] printf
.LBB10_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	29	0
 #  28  
 #  29      return 0;
	addq $56,%rsp                 	# [0] 
	ret                           	# [0] 
.L_0_514:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.byte	0x46, 0x6f, 0x72, 0x20, 0x31, 0x73, 0x74, 0x20	# For 1st 
	.byte	0x63, 0x6f, 0x6d, 0x70, 0x6c, 0x65, 0x78, 0x20	# complex 
	.byte	0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x20, 0xa	# number \n
	.byte	0x0	# \000
	.org 0x20
	.align	0
	# offset 32
	.byte	0x45, 0x6e, 0x74, 0x65, 0x72, 0x20, 0x72, 0x65	# Enter re
	.byte	0x61, 0x6c, 0x20, 0x61, 0x6e, 0x64, 0x20, 0x69	# al and i
	.byte	0x6d, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x72, 0x79	# maginary
	.byte	0x20, 0x70, 0x61, 0x72, 0x74, 0x20, 0x72, 0x65	#  part re
	.byte	0x73, 0x70, 0x65, 0x63, 0x74, 0x69, 0x76, 0x65	# spective
	.byte	0x6c, 0x79, 0x3a, 0xa, 0x0	# ly:\n\000
	.org 0x50
	.align	0
	# offset 80
	.string "%f %f"
	.org 0x60
	.align	0
	# offset 96
	.byte	0xa, 0x46, 0x6f, 0x72, 0x20, 0x32, 0x6e, 0x64	# \nFor 2nd
	.byte	0x20, 0x63, 0x6f, 0x6d, 0x70, 0x6c, 0x65, 0x78	#  complex
	.byte	0x20, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x20	#  number 
	.byte	0xa, 0x0	# \n\000
	.org 0x80
	.align	0
	# offset 128
	.string "Sum = %.1f + %.1fi"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_258-main
	.uleb128	.L_0_514-.L_0_258
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.p2align 5,,

	# Program Unit: _Z3add7complexS_
.globl	_Z3add7complexS_
	.type	_Z3add7complexS_, @function
_Z3add7complexS_:	# 0x94
	# .frame	%rsp, 40, %rsp
	# temp = 0
	.loc	1	32	0
 #  30  }
 #  31  
 #  32  complex add(complex n1, complex n2)
.LBB1__Z3add7complexS_:
	addq $-40,%rsp                	# [0] 
	movsd %xmm0,16(%rsp)          	# [1] n1
	.loc	1	36	0
 #  33  {
 #  34        complex temp;
 #  35  
 #  36        temp.real = n1.real + n2.real;
	movss 16(%rsp),%xmm4          	# [1] n1
	.loc	1	37	0
 #  37        temp.imag = n1.imag + n2.imag;
	movss 20(%rsp),%xmm3          	# [2] n1+4
	.loc	1	32	0
	movsd %xmm1,24(%rsp)          	# [3] n2
	.loc	1	37	0
	movss 28(%rsp),%xmm1          	# [3] n2+4
	.loc	1	36	0
	movss 24(%rsp),%xmm2          	# [4] n2
	.loc	1	37	0
	addss %xmm3,%xmm1             	# [6] 
	.loc	1	36	0
	addss %xmm4,%xmm2             	# [7] 
	.loc	1	37	0
	movss %xmm1,4(%rsp)           	# [9] temp+4
	.loc	1	36	0
	movss %xmm2,0(%rsp)           	# [10] temp
	.loc	1	39	0
 #  38  
 #  39        return(temp);
	movsd 0(%rsp),%xmm0           	# [11] temp
	addq $40,%rsp                 	# [11] 
	ret                           	# [11] 
.LDWend__Z3add7complexS_:
	.size _Z3add7complexS_, .LDWend__Z3add7complexS_-_Z3add7complexS_
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x40
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test64.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

