#include <gtest/gtest.h>
#include <string>
#include <cstdlib>

using namespace std;



int performOptiTestWithString(int number, string input){
	string name = "printf \"";
	name += input;
	name += "\" | ";
	name += "lli test";
	name += to_string(number);
	name +="_O.bc >my_result";
	
	
	string name1 = "printf \"";
	name1 += input;
	name1 += "\" | ";
	name1 += "lli ./L/test";
	name1 += to_string(number);
	name1 += "_right.ll >right_result";
	
	system(name.c_str() );
	system(name1.c_str() );
	
	string name2 = "diff my_result right_result > /dev/null";
	int result =  system(name2.c_str() );
	
	if(result != 0){
		string str_num = to_string(number);
		string command1 = "mv my_result my";
		command1+="_";
		command1+=str_num;
		system(command1.c_str());
		
		string command2 = "mv right_result right";
		command2+="_";
		command2+=str_num;
		system(command2.c_str());	
	}	
	return result;
}


int performTestWithString(int number, string input){
	string name = "printf \"";
	name += input;
	name += "\" | ";
	name += "lli test";
	name += to_string(number);
	name +="_B.bc >my_result";
	
	
	string name1 = "printf \"";
	name1 += input;
	name1 += "\" | ";
	name1 += "lli ./L/test";
	name1 += to_string(number);
	name1 += "_right.ll >right_result";
	
	system(name.c_str() );
	system(name1.c_str() );
	
	string name2 = "diff my_result right_result > /dev/null";
	int result =  system(name2.c_str() );
	
	if(result != 0){
		string str_num = to_string(number);
		string command1 = "mv my_result my";
		command1+="_";
		command1+=str_num;
		system(command1.c_str());
		
		string command2 = "mv right_result right";
		command2+="_";
		command2+=str_num;
		system(command2.c_str());	
	}	
	return result;
}

int performOptiTest(int number, string input ){
	string name = "echo ";
	name += input;
	name += " | ";
	name += "lli test";
	name += to_string(number);
	name +="_O.bc >my_result";

	
	string name1 = "echo ";
	name1 += input;
	name1 += " | ";
	name1 += "lli ./L/test";
	name1 += to_string(number);
	name1 += "_right.ll >right_result";
	
	system(name.c_str() );
	system(name1.c_str() );
	
	string name2 = "diff my_result right_result > /dev/null";
	int result =  system(name2.c_str() );
	
	if(result != 0){
		string str_num = to_string(number);
		string command1 = "mv my_result my";
		command1+="_";
		command1+=str_num;
		system(command1.c_str());
		
		string command2 = "mv right_result right";
		command2+="_";
		command2+=str_num;
		system(command2.c_str());	
	}	
	return result;
}


int performTest(int number, string input ){
	string name = "echo ";
	name += input;
	name += " | ";
	name += "lli test";
	name += to_string(number);
	name +="_B.bc >my_result";

	
	string name1 = "echo ";
	name1 += input;
	name1 += " | ";
	name1 += "lli ./L/test";
	name1 += to_string(number);
	name1 += "_right.ll >right_result";
	
	system(name.c_str() );
	system(name1.c_str() );
	
	string name2 = "diff my_result right_result > /dev/null";
	int result =  system(name2.c_str() );
	
	if(result != 0){
		string str_num = to_string(number);
		string command1 = "mv my_result my";
		command1+="_";
		command1+=str_num;
		system(command1.c_str());
		
		string command2 = "mv right_result right";
		command2+="_";
		command2+=str_num;
		system(command2.c_str());	
	}	
	return result;
}


TEST (YanOptitest, test1) { 
    EXPECT_EQ (0, performOptiTest(1,"1"));
	EXPECT_EQ (0, performOptiTest(1, "123"));
}


TEST (YanOptitest, test2) { 
    EXPECT_EQ (0, performOptiTest(2,"1 2"));
	EXPECT_EQ (0, performOptiTest(2, "123 123"));
}


TEST (YanOptitest, test3) { 
    EXPECT_EQ (0, performOptiTest(3,"1.0 2.2"));
	EXPECT_EQ (0, performOptiTest(3, "123.0 123.1"));
}


TEST (YanOptitest, test4) { 
    EXPECT_EQ (0, performOptiTest(4,"a"));
	EXPECT_EQ (0, performOptiTest(4, "A"));
	EXPECT_EQ (0, performOptiTest(4, "?"));
}

TEST (YanOptitest, test5) { 
    EXPECT_EQ (0, performOptiTest(5,"5 2"));
	EXPECT_EQ (0, performOptiTest(5, "9 3"));
}
TEST (YanOptitest, test6) { 
    EXPECT_EQ (0, performOptiTest(6,""));
}
TEST (YanOptitest, test7) { 
    EXPECT_EQ (0, performOptiTest(7,""));
}

TEST (YanOptitest, test8) { 
    EXPECT_EQ (0, performOptiTest(8,"111.11 222.22"));
	EXPECT_EQ (0, performOptiTest(8, "-1.2 30.1"));
}
TEST (YanOptitest, test9) { 
    EXPECT_EQ (0, performOptiTest(9,"4"));
	EXPECT_EQ (0, performOptiTest(9, "123"));
}

TEST (YanOptitest, test10) { 
    EXPECT_EQ (0, performOptiTest(10,"a"));
	EXPECT_EQ (0, performOptiTest(10, "E"));
	EXPECT_EQ (0, performOptiTest(10, "s"));
}

TEST (YanOptitest, test11) { 
    EXPECT_EQ (0, performOptiTest(11," 4 1.2 3"));
	EXPECT_EQ (0, performOptiTest(11, "100.1 -1.0 3.3"));
}
TEST (YanOptitest, test12) { 
    EXPECT_EQ (0, performOptiTest(12," 1 4 4"));
	EXPECT_EQ (0, performOptiTest(12, "1.0 3.0 2.2"));
	EXPECT_EQ (0, performOptiTest(12, "2.1 3.0 2.0"));
}
TEST (YanOptitest, test13) { 
    EXPECT_EQ (0, performOptiTest(13,"1616"));
	EXPECT_EQ (0, performOptiTest(13, "1400"));
	EXPECT_EQ (0, performOptiTest(13,"800"));
	EXPECT_EQ (0, performOptiTest(13, "1411"));
}
TEST (YanOptitest, test14) { 
    EXPECT_EQ (0, performOptiTest(14,"2.3"));
	EXPECT_EQ (0, performOptiTest(14, "-1.3"));
	EXPECT_EQ (0, performOptiTest(14, "0.0"));
}
TEST (YanOptitest, test15) { 
    EXPECT_EQ (0, performOptiTest(15,"c"));
	EXPECT_EQ (0, performOptiTest(15, "*"));
}
TEST (YanOptitest, test16) { 
    EXPECT_EQ (0, performOptiTest(16,"10"));
	EXPECT_EQ (0, performOptiTest(16, "3"));
}
TEST (YanOptitest, test17) { 
    EXPECT_EQ (0, performOptiTest(17,"-3"));
	EXPECT_EQ (0, performOptiTest(17, "10"));
}
TEST (YanOptitest, test18) { 
    EXPECT_EQ (0, performOptiTest(18,"9"));
	EXPECT_EQ (0, performOptiTest(18, "6"));
}
TEST (YanOptitest, test19) { 
    EXPECT_EQ (0, performOptiTest(19,"9"));
	EXPECT_EQ (0, performOptiTest(19, "6"));
}
TEST (YanOptitest, test20) { 
    EXPECT_EQ (0, performOptiTest(20,"24 8"));
	EXPECT_EQ (0, performOptiTest(20, "36 9"));
	EXPECT_EQ (0, performOptiTest(20, "13 7"));
	
}
TEST (YanOptitest, test21) { 
    EXPECT_EQ (0, performOptiTest(21,"3 4"));
	EXPECT_EQ (0, performOptiTest(21, "4 6"));
}
TEST (YanOptitest, test22) { 
    EXPECT_EQ (0, performOptiTest(22,""));
}
TEST (YanOptitest, test23) { 
    EXPECT_EQ (0, performOptiTest(23,"1234"));
	EXPECT_EQ (0, performOptiTest(23, "4389"));
}
TEST (YanOptitest, test24) { 
    EXPECT_EQ (0, performOptiTest(24,"1234"));
	EXPECT_EQ (0, performOptiTest(24, "5678"));
}
TEST (YanOptitest, test25) { 
    EXPECT_EQ (0, performOptiTest(25,"3 4"));
	EXPECT_EQ (0, performOptiTest(25, "2 5"));
}
TEST (YanOptitest, test26) { 
    EXPECT_EQ (0, performOptiTest(26,"53235"));
	EXPECT_EQ (0, performOptiTest(26, "12345"));
}
TEST (YanOptitest, test27) { 
    EXPECT_EQ (0, performOptiTest(27,"37"));
	EXPECT_EQ (0, performOptiTest(27, "46"));
}
TEST (YanOptitest, test28) { 
    EXPECT_EQ (0, performOptiTest(28,"3 100"));
	EXPECT_EQ (0, performOptiTest(28, "18 99"));
}
TEST (YanOptitest, test29) { 
    EXPECT_EQ (0, performOptiTest(29,"153"));
	EXPECT_EQ (0, performOptiTest(29, "154"));
}
TEST (YanOptitest, test30) { 
    EXPECT_EQ (0, performOptiTest(30,"100 300"));
	EXPECT_EQ (0, performOptiTest(30, "999 9999"));
}
TEST (YanOptitest, test31) { 
    EXPECT_EQ (0, performOptiTest(31,"60"));
	EXPECT_EQ (0, performOptiTest(31, "90"));
}
TEST (YanOptitest, test32) { 
    EXPECT_EQ (0, performOptiTest(32,"6"));
	EXPECT_EQ (0, performOptiTest(32, "9"));
}
TEST (YanOptitest, test33) { 
    EXPECT_EQ (0, performOptiTest(33,"* 3 4"));
	EXPECT_EQ (0, performOptiTest(33, "+ 4.1 6.2"));
	EXPECT_EQ (0, performOptiTest(33,"- 5.3 1.4"));
	EXPECT_EQ (0, performOptiTest(33, "/ 4.4 1.6"));
}
TEST (YanOptitest, test34) { 
    EXPECT_EQ (0, performOptiTest(34,"1 40"));
	EXPECT_EQ (0, performOptiTest(34, "4 96"));
}
TEST (YanOptitest, test35) { 
    EXPECT_EQ (0, performOptiTest(35,"31"));
	EXPECT_EQ (0, performOptiTest(35, "153"));
}
TEST (YanOptitest, test36) { 
    EXPECT_EQ (0, performOptiTest(36,"37"));
	EXPECT_EQ (0, performOptiTest(36, "46"));
}
TEST (YanOptitest, test37) { 
    EXPECT_EQ (0, performOptiTest(37,"9"));
	EXPECT_EQ (0, performOptiTest(37, "16"));
}
TEST (YanOptitest, test38) { 
    EXPECT_EQ (0, performOptiTest(38,"4"));
	EXPECT_EQ (0, performOptiTest(38, "9"));
}
TEST (YanOptiOptitest, test39) { 
    EXPECT_EQ (0, performOptiTest(39,"28 12"));
	EXPECT_EQ (0, performOptiTest(39, "24 12"));
}
TEST (YanOptitest, test40) { 
    EXPECT_EQ (0, performOptiTest(40,"11111"));
	EXPECT_EQ (0, performOptiTest(40, "10010"));
}

TEST (YanOptitest, test41) { 
    EXPECT_EQ (0, performOptiTest(41,"d 11"));
	EXPECT_EQ (0, performOptiTest(41, "o 10"));
}

TEST (YanOptitest, test42) { 
    EXPECT_EQ (0, performOptiTest(42,"o 11111"));
	EXPECT_EQ (0, performOptiTest(42, "b 32"));
}

TEST (YanOptitest, test43) { 
    EXPECT_EQ (0, performOptiTest(43,"hello world"));
	EXPECT_EQ (0, performOptiTest(43, "zhaoyan"));
}


TEST (YanOptitest, test44) { 
    EXPECT_EQ (0, performOptiTest(44,"10 2"));
	EXPECT_EQ (0, performOptiTest(44, "5 3"));
}
TEST (YanOptitest, test45) { 
    EXPECT_EQ (0, performOptiTest(45,"111 10"));
	EXPECT_EQ (0, performOptiTest(45, "15"));
}




TEST (Yantest, test1) { 
    EXPECT_EQ (0, performTest(1,"1"));
	EXPECT_EQ (0, performTest(1, "123"));
}

TEST (Yantest, test2) { 
    EXPECT_EQ (0, performTest(2,"1 2"));
	EXPECT_EQ (0, performTest(2, "123 123"));
}

TEST (Yantest, test3) { 
    EXPECT_EQ (0, performTest(3,"1.0 2.2"));
	EXPECT_EQ (0, performTest(3, "123.0 123.1"));
}


TEST (Yantest, test4) { 
    EXPECT_EQ (0, performTest(4,"a"));
	EXPECT_EQ (0, performTest(4, "A"));
	EXPECT_EQ (0, performTest(4, "?"));
}

TEST (Yantest, test5) { 
    EXPECT_EQ (0, performTest(5,"5 2"));
	EXPECT_EQ (0, performTest(5, "9 3"));
}
TEST (Yantest, test6) { 
    EXPECT_EQ (0, performTest(6,""));
}
TEST (Yantest, test7) { 
    EXPECT_EQ (0, performTest(7,""));
}
TEST (Yantest, test8) { 
    EXPECT_EQ (0, performTest(8,"111.11 222.22"));
	EXPECT_EQ (0, performTest(8, "-1.2 30.1"));
}
TEST (Yantest, test9) { 
    EXPECT_EQ (0, performTest(9,"4"));
	EXPECT_EQ (0, performTest(9, "123"));
}

TEST (Yantest, test10) { 
    EXPECT_EQ (0, performTest(10,"a"));
	EXPECT_EQ (0, performTest(10, "E"));
	EXPECT_EQ (0, performTest(10, "s"));
}

TEST (Yantest, test11) { 
    EXPECT_EQ (0, performTest(11," 4 1.2 3"));
	EXPECT_EQ (0, performTest(11, "100.1 -1.0 3.3"));
}
TEST (Yantest, test12) { 
    EXPECT_EQ (0, performTest(12," 1 4 4"));
	EXPECT_EQ (0, performTest(12, "1.0 3.0 2.2"));
	EXPECT_EQ (0, performTest(12, "2.1 3.0 2.0"));
}
TEST (Yantest, test13) { 
    EXPECT_EQ (0, performTest(13,"1616"));
	EXPECT_EQ (0, performTest(13, "1400"));
	EXPECT_EQ (0, performTest(13,"800"));
	EXPECT_EQ (0, performTest(13, "1411"));
}
TEST (Yantest, test14) { 
    EXPECT_EQ (0, performTest(14,"2.3"));
	EXPECT_EQ (0, performTest(14, "-1.3"));
	EXPECT_EQ (0, performTest(14, "0.0"));
}
TEST (Yantest, test15) { 
    EXPECT_EQ (0, performTest(15,"c"));
	EXPECT_EQ (0, performTest(15, "*"));
}
TEST (Yantest, test16) { 
    EXPECT_EQ (0, performTest(16,"10"));
	EXPECT_EQ (0, performTest(16, "3"));
}
TEST (Yantest, test17) { 
    EXPECT_EQ (0, performTest(17,"-3"));
	EXPECT_EQ (0, performTest(17, "10"));
}
TEST (Yantest, test18) { 
    EXPECT_EQ (0, performTest(18,"9"));
	EXPECT_EQ (0, performTest(18, "6"));
}
TEST (Yantest, test19) { 
    EXPECT_EQ (0, performTest(19,"9"));
	EXPECT_EQ (0, performTest(19, "6"));
}
TEST (Yantest, test20) { 
    EXPECT_EQ (0, performTest(20,"24 8"));
	EXPECT_EQ (0, performTest(20, "36 9"));
	EXPECT_EQ (0, performTest(20, "13 7"));
	
}
TEST (Yantest, test21) { 
    EXPECT_EQ (0, performTest(21,"3 4"));
	EXPECT_EQ (0, performTest(21, "4 6"));
}
TEST (Yantest, test22) { 
    EXPECT_EQ (0, performTest(22,""));
}
TEST (Yantest, test23) { 
    EXPECT_EQ (0, performTest(23,"1234"));
	EXPECT_EQ (0, performTest(23, "4389"));
}
TEST (Yantest, test24) { 
    EXPECT_EQ (0, performTest(24,"1234"));
	EXPECT_EQ (0, performTest(24, "5678"));
}
TEST (Yantest, test25) { 
    EXPECT_EQ (0, performTest(25,"3 4"));
	EXPECT_EQ (0, performTest(25, "2 5"));
}
TEST (Yantest, test26) { 
    EXPECT_EQ (0, performTest(26,"53235"));
	EXPECT_EQ (0, performTest(26, "12345"));
}
TEST (Yantest, test27) { 
    EXPECT_EQ (0, performTest(27,"37"));
	EXPECT_EQ (0, performTest(27, "46"));
}
TEST (Yantest, test28) { 
    EXPECT_EQ (0, performTest(28,"3 100"));
	EXPECT_EQ (0, performTest(28, "18 99"));
}
TEST (Yantest, test29) { 
    EXPECT_EQ (0, performTest(29,"153"));
	EXPECT_EQ (0, performTest(29, "154"));
}
TEST (Yantest, test30) { 
    EXPECT_EQ (0, performTest(30,"100 300"));
	EXPECT_EQ (0, performTest(30, "999 9999"));
}
TEST (Yantest, test31) { 
    EXPECT_EQ (0, performTest(31,"60"));
	EXPECT_EQ (0, performTest(31, "90"));
}
TEST (Yantest, test32) { 
    EXPECT_EQ (0, performTest(32,"6"));
	EXPECT_EQ (0, performTest(32, "9"));
}
TEST (Yantest, test33) { 
    EXPECT_EQ (0, performTest(33,"* 3 4"));
	EXPECT_EQ (0, performTest(33, "+ 4.1 6.2"));
	EXPECT_EQ (0, performTest(33,"- 5.3 1.4"));
	EXPECT_EQ (0, performTest(33, "/ 4.4 1.6"));
}
TEST (Yantest, test34) { 
    EXPECT_EQ (0, performTest(34,"1 40"));
	EXPECT_EQ (0, performTest(34, "4 96"));
}
TEST (Yantest, test35) { 
    EXPECT_EQ (0, performTest(35,"31"));
	EXPECT_EQ (0, performTest(35, "153"));
}
TEST (Yantest, test36) { 
    EXPECT_EQ (0, performTest(36,"37"));
	EXPECT_EQ (0, performTest(36, "46"));
}
TEST (Yantest, test37) { 
    EXPECT_EQ (0, performTest(37,"9"));
	EXPECT_EQ (0, performTest(37, "16"));
}
TEST (Yantest, test38) { 
    EXPECT_EQ (0, performTest(38,"4"));
	EXPECT_EQ (0, performTest(38, "9"));
}
TEST (Yantest, test39) { 
    EXPECT_EQ (0, performTest(39,"28 12"));
	EXPECT_EQ (0, performTest(39, "24 12"));
}
TEST (Yantest, test40) { 
    EXPECT_EQ (0, performTest(40,"11111"));
	EXPECT_EQ (0, performTest(40, "10010"));
}

TEST (Yantest, test41) { 
    EXPECT_EQ (0, performTest(41,"d 11"));
	EXPECT_EQ (0, performTest(41, "o 10"));
}

TEST (Yantest, test42) { 
    EXPECT_EQ (0, performTest(42,"o 11111"));
	EXPECT_EQ (0, performTest(42, "b 32"));
}

TEST (Yantest, test43) { 
    EXPECT_EQ (0, performTest(43,"hello world"));
	EXPECT_EQ (0, performTest(43, "zhaoyan"));
}


TEST (Yantest, test44) { 
    EXPECT_EQ (0, performTest(44,"10 2"));
	EXPECT_EQ (0, performTest(44, "5 3"));
}
TEST (Yantest, test45) { 
    EXPECT_EQ (0, performTest(45,"111 10"));
	EXPECT_EQ (0, performTest(45, "15"));
}
TEST (Yantest, test46) { 
    EXPECT_EQ (0, performTest(46,"11"));
}
TEST (Yantest, test47) { 
    EXPECT_EQ (0, performTest(47,""));
}
TEST (Yantest, test48) { 
    EXPECT_EQ (0, performTest(48,"2 3"));
	EXPECT_EQ (0, performTest(48, "4 5"));
}
TEST (Yantest, test49) { 
    EXPECT_EQ (0, performTest(49,"2 3 3 2"));
}
TEST (Yantest, test50) { 
    EXPECT_EQ (0, performTest(50,"3 4"));
}

TEST (Yantest, test51) { 
	EXPECT_EQ (0, performTest(50,"3 4 5 3 2 3 3 2"));
    EXPECT_EQ (0, performTest(50,"3 4 4 3"));
}

TEST (Yantest, test52) { 
    EXPECT_EQ (0, performTest(52,""));
}

TEST (Yantest, test53) { 
    EXPECT_EQ (0, performTest(53,"3 5 7"));
}
TEST (Yantest, test54) { 
    EXPECT_EQ (0, performTest(54,"5"));
}
TEST (Yantest, test55) { 
    EXPECT_EQ (0, performTestWithString(55,"hello world\\n l"));
}
TEST (Yantest, test56) { 
    EXPECT_EQ (0, performTest(56,"hello world"));
}
TEST (Yantest, test57) { 
    EXPECT_EQ (0, performTestWithString(57,"hello ?# world 123\n"));
}
TEST (Yantest, test58) { 
    EXPECT_EQ (0, performTest(58,"helloworld"));
}
TEST (Yantest, test59) { 
    EXPECT_EQ (0, performTestWithString(59,"hello\nworld\n"));
}

TEST (Yantest, test60) { 
    EXPECT_EQ (0, performTest(60,"helloworld"));
}

TEST (Yantest, test61) { 
    EXPECT_EQ (0, performTestWithString(61,"hello\\nworld\\nYan\\n"));
}
TEST (Yantest, test62) { 
    EXPECT_EQ (0, performTestWithString(62,"hello\\n23\\n2.3\\n"));
}
TEST (Yantest, test63) { 
    EXPECT_EQ (0, performTest(63,"1 2.0 2 3.0"));
}
TEST (Yantest, test64) { 
    EXPECT_EQ (0, performTest(64,"1.0 2.0 2.0 3.0"));
}
TEST (Yantest, test65) { 
    EXPECT_EQ (0, performTest(65,"9 30 30 10 20 20"));
}
TEST (Yantest, test66) { 
    EXPECT_EQ (0, performTest(66,"zhao 1.0 yan 2.0"));
}
TEST (Yantest, test67) { 
    EXPECT_EQ (0, performTestWithString(67,"2 math 98 english 50"));
}
TEST (Yantest, test68) { 
    EXPECT_EQ (0, performTestWithString(68,"hello world\\n"));
}
TEST (Yantest, test69) { 
    EXPECT_EQ (0, performTest(69,""));
}
TEST (Yantest, test70) { 
    EXPECT_EQ (0, performTest(70,"6"));
}
TEST (Yantest, test71) { 
    EXPECT_EQ (0, performTest(71,"6"));
	 EXPECT_EQ (0, performTest(71,"5"));
}

TEST (Yantest, test72) { 
    EXPECT_EQ (0, performTest(72,"6"));
	EXPECT_EQ (0, performTest(72,"15"));
}

TEST (Yantest, test73) { 
    EXPECT_EQ (0, performTest(73,"6"));
	EXPECT_EQ (0, performTest(73,"0"));
	EXPECT_EQ (0, performTest(73,"-23"));
}

int main(int argc, char **argv) {
        //if(strcpy(argv[1], "-high"){
	::testing::InitGoogleTest(&argc, argv);
	//}
	//::testing::GTEST_FLAG(filter) = "YanOptitest*";
       //::testing::GTEST_FLAG(filter) = "Yantest*";
	return RUN_ALL_TESTS();
}
