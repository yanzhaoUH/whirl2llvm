	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test49.c (test49.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test49.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 1320, %rsp
	# a = 16
	# b = 416
	# result = 816
	# r1 = 0
	# c1 = 4
	# r2 = 8
	# c2 = 12
	# i = 1220
	# j = 1216
	# k = 1224
	# _temp_gra_spill1 = 1232
	# _temp_gra_spill2 = 1240
	# _temp_gra_spill3 = 1248
	# _temp_gra_spill4 = 1256
	# _temp_gra_spill5 = 1264
	# _temp_gra_spill6 = 1272
	# _temp_gra_spill7 = 1280
	# _temp_gra_spill8 = 1288
	# _temp_gra_spill9 = 1296
	.loc	1	3	0
 #   1  #include <stdio.h>
 #   2  
 #   3  int main()
.LBB1_main:
.LEH_adjustsp_main:
.LEH_csr_main:
	addq $-1320,%rsp              	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
	movq %rbx,1272(%rsp)          	# [0] _temp_gra_spill6
	movq %rbp,1256(%rsp)          	# [0] _temp_gra_spill4
	movq %r12,1248(%rsp)          	# [0] _temp_gra_spill3
	movq %r13,1264(%rsp)          	# [0] _temp_gra_spill5
	movq %r14,1240(%rsp)          	# [0] _temp_gra_spill2
	movq %r15,1232(%rsp)          	# [0] _temp_gra_spill1
.L_0_22786:
	xorl %eax,%eax                	# [0] 
	.loc	1	7	0
 #   4  {
 #   5      int a[10][10], b[10][10], result[10][10], r1, c1, r2, c2, i, j, k;
 #   6  
 #   7      printf("Enter rows and column for first matrix: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	8	0
 #   8      scanf("%d %d", &r1, &c1); 
	leaq 4(%rsp),%rdx             	# [0] c1
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	12	0
 #   9    //  r1 = 2;
 #  10    //  c1 = 3;
 #  11  
 #  12      printf("Enter rows and column for second matrix: ");
	movq $(.rodata+64),%rdi       	# [0] .rodata+64
	call printf                   	# [0] printf
.LBB5_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	13	0
 #  13      scanf("%d %d",&r2, &c2);
	leaq 12(%rsp),%rdx            	# [0] c2
	leaq 8(%rsp),%rsi             	# [1] r2
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call scanf                    	# [1] scanf
.LBB6_main:
	movl 8(%rsp),%edi             	# [0] r2
	movl 4(%rsp),%eax             	# [0] c1
	cmpl %edi,%eax                	# [3] 
	je .Lt_0_6914                 	# [4] 
.Lt_0_7426:
 #<loop> Loop body line 21
	xorl %eax,%eax                	# [0] 
	.loc	1	21	0
 #  17  
 #  18      // Column of first matrix should be equal to column of second matrix and
 #  19      while (c1 != r2)
 #  20      {
 #  21          printf("Error! column of first matrix not equal to row of second.\n\n");
	movq $(.rodata+112),%rdi      	# [0] .rodata+112
	call printf                   	# [0] printf
.LBB8_main:
 #<loop> Part of loop body line 21, head labeled .Lt_0_7426
	xorl %eax,%eax                	# [0] 
	.loc	1	22	0
 #  22          printf("Enter rows and column for first matrix: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	call printf                   	# [0] printf
.LBB9_main:
 #<loop> Part of loop body line 21, head labeled .Lt_0_7426
	xorl %eax,%eax                	# [0] 
	.loc	1	23	0
 #  23          scanf("%d %d", &r1, &c1);
	leaq 4(%rsp),%rdx             	# [0] c1
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call scanf                    	# [1] scanf
.LBB10_main:
 #<loop> Part of loop body line 21, head labeled .Lt_0_7426
	xorl %eax,%eax                	# [0] 
	.loc	1	24	0
 #  24          printf("Enter rows and column for second matrix: ");
	movq $(.rodata+64),%rdi       	# [0] .rodata+64
	call printf                   	# [0] printf
.LBB11_main:
 #<loop> Part of loop body line 21, head labeled .Lt_0_7426
	xorl %eax,%eax                	# [0] 
	.loc	1	25	0
 #  25          scanf("%d %d",&r2, &c2);
	leaq 12(%rsp),%rdx            	# [0] c2
	leaq 8(%rsp),%rsi             	# [1] r2
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call scanf                    	# [1] scanf
.LBB12_main:
 #<loop> Part of loop body line 21, head labeled .Lt_0_7426
	movl 8(%rsp),%edi             	# [0] r2
	movl 4(%rsp),%eax             	# [0] c1
	cmpl %edi,%eax                	# [3] 
	jne .Lt_0_7426                	# [4] 
.Lt_0_6914:
	xorl %eax,%eax                	# [0] 
	.loc	1	29	0
 #  26      }
 #  27  
 #  28      // Storing elements of first matrix.
 #  29      printf("\nEnter elements of matrix 1:\n");
	movq $(.rodata+176),%rdi      	# [0] .rodata+176
	call printf                   	# [0] printf
.LBB14_main:
	movl 0(%rsp),%eax             	# [0] r1
	testl %eax,%eax               	# [3] 
	jle .Lt_0_7938                	# [4] 
.LBB15_main:
	movl 4(%rsp),%r12d            	# [0] c1
	xorl %edi,%edi                	# [0] 
	leaq 16(%rsp),%r15            	# [1] a
	movl $1,%r14d                 	# [1] 
	movl %edi,1220(%rsp)          	# [1] i
.Lt_0_8450:
 #<loop> Loop body line 29, nesting depth: 1, estimated iterations: 100
	testl %r12d,%r12d             	# [0] 
	jle .Lt_0_8706                	# [1] 
.LBB17_main:
 #<loop> Part of loop body line 29, head labeled .Lt_0_8450
	movq %r15,%r13                	# [0] 
	xorl %eax,%eax                	# [0] 
	movl %r14d,%ebp               	# [1] 
	movl $1,%ebx                  	# [1] 
	movl %eax,1216(%rsp)          	# [1] j
.Lt_0_9218:
 #<loop> Loop body line 33
	xorl %eax,%eax                	# [0] 
	.loc	1	33	0
 #  30      for(i=0; i<r1; ++i)
 #  31          for(j=0; j<c1; ++j)
 #  32          {
 #  33              printf("Enter elements a%d%d: ",i+1, j+1);
	movl %ebx,%edx                	# [0] 
	movl %r14d,%esi               	# [1] 
	movq $(.rodata+208),%rdi      	# [1] .rodata+208
	call printf                   	# [1] printf
.LBB19_main:
 #<loop> Part of loop body line 33, head labeled .Lt_0_9218
	.loc	1	35	0
 #  34              //scanf("%d", &a[i][j]);
 #  35              a[i][j] = (i+1)*(j+1);
	movl 1216(%rsp),%eax          	# [0] j
	movl 4(%rsp),%r12d            	# [2] c1
	incl %eax                     	# [3] 
	addq $4,%r13                  	# [4] 
	addl $1,%ebx                  	# [4] 
	movl %eax,1216(%rsp)          	# [4] j
	movl %ebp,-4(%r13)            	# [5] id:101 a+0x0
	addl %r14d,%ebp               	# [5] 
	cmpl %r12d,%eax               	# [5] 
	jl .Lt_0_9218                 	# [6] 
.LBB20_main:
 #<loop> Part of loop body line 29, head labeled .Lt_0_8450
	movl 0(%rsp),%eax             	# [0] r1
.Lt_0_8706:
 #<loop> Part of loop body line 29, head labeled .Lt_0_8450
	movl 1220(%rsp),%edi          	# [0] i
	incl %edi                     	# [3] 
	addq $40,%r15                 	# [4] 
	addl $1,%r14d                 	# [4] 
	cmpl %edi,%eax                	# [4] 
	movl %edi,1220(%rsp)          	# [5] i
	jg .Lt_0_8450                 	# [5] 
.Lt_0_7938:
	xorl %eax,%eax                	# [0] 
	.loc	1	39	0
 #  36          }
 #  37  
 #  38      // Storing elements of second matrix.
 #  39      printf("\nEnter elements of matrix 2:\n");
	movq $(.rodata+240),%rdi      	# [0] .rodata+240
	call printf                   	# [0] printf
.LBB23_main:
	movl 8(%rsp),%eax             	# [0] r2
	testl %eax,%eax               	# [3] 
	jle .Lt_0_9986                	# [4] 
.LBB24_main:
	xorl %edi,%edi                	# [0] 
	leaq 416(%rsp),%r13           	# [1] b
	movl $1,%r14d                 	# [1] 
	movl %edi,1220(%rsp)          	# [1] i
.Lt_0_10498:
 #<loop> Loop body line 39, nesting depth: 1, estimated iterations: 100
	movl 12(%rsp),%edi            	# [0] c2
	cmpl $0,%edi                  	# [3] 
	jle .Lt_0_10754               	# [4] 
.LBB26_main:
 #<loop> Part of loop body line 39, head labeled .Lt_0_10498
	movq %r13,%r12                	# [0] 
	xorl %eax,%eax                	# [0] 
	movl %r14d,%ebp               	# [1] 
	movl $1,%ebx                  	# [1] 
	movl %eax,1216(%rsp)          	# [1] j
.Lt_0_11266:
 #<loop> Loop body line 43
	xorl %eax,%eax                	# [0] 
	.loc	1	43	0
 #  40      for(i=0; i<r2; ++i)
 #  41          for(j=0; j<c2; ++j)
 #  42          {
 #  43              printf("Enter elements b%d%d: ",i+1, j+1);
	movl %ebx,%edx                	# [0] 
	movl %r14d,%esi               	# [1] 
	movq $(.rodata+272),%rdi      	# [1] .rodata+272
	call printf                   	# [1] printf
.LBB28_main:
 #<loop> Part of loop body line 43, head labeled .Lt_0_11266
	.loc	1	45	0
 #  44              //scanf("%d",&b[i][j]);
 #  45              b[i][j] = (i+1)*(j+1);
	movl 1216(%rsp),%edi          	# [0] j
	incl %edi                     	# [3] 
	movl %edi,1216(%rsp)          	# [4] j
	movl %ebp,0(%r12)             	# [4] id:102 b+0x0
	movl 12(%rsp),%eax            	# [4] c2
	addq $4,%r12                  	# [6] 
	addl $1,%ebx                  	# [7] 
	addl %r14d,%ebp               	# [7] 
	cmpl %edi,%eax                	# [7] 
	jg .Lt_0_11266                	# [8] 
.LBB29_main:
 #<loop> Part of loop body line 39, head labeled .Lt_0_10498
	movl 8(%rsp),%eax             	# [0] r2
.Lt_0_10754:
 #<loop> Part of loop body line 39, head labeled .Lt_0_10498
	movl 1220(%rsp),%edi          	# [0] i
	incl %edi                     	# [3] 
	addq $40,%r13                 	# [4] 
	addl $1,%r14d                 	# [4] 
	cmpl %eax,%edi                	# [4] 
	movl %edi,1220(%rsp)          	# [5] i
	jl .Lt_0_10498                	# [5] 
.Lt_0_9986:
	movl 0(%rsp),%r8d             	# [0] r1
	testl %r8d,%r8d               	# [3] 
	jle .Lt_0_12034               	# [4] 
.LBB32_main:
	xorl %eax,%eax                	# [0] 
	leaq 816(%rsp),%rdx           	# [1] result
	movl %eax,1220(%rsp)          	# [1] i
	jmp .Lt_0_12546               	# [1] 
.LBB69_main:
 #<loop> Part of loop body line 45, head labeled .Lt_0_12546
	movl %esi,%eax                	# [0] 
	sarl $2,%eax                  	# [1] 
	testl %eax,%eax               	# [2] 
	je .Lt_0_12802                	# [3] 
.LBB74_main:
 #<loop> Part of loop body line 45, head labeled .Lt_0_12546
.LBB68_main:
 #<loop> Loop body line 45, nesting depth: 2, estimated iterations: 25
 #<loop> unrolled 4 times
	.loc	1	52	0
 #  48      // Initializing all elements of result matrix to 0
 #  49      for(i=0; i<r1; ++i)
 #  50          for(j=0; j<c2; ++j)
 #  51          {
 #  52              result[i][j] = 0;
	movl 1216(%rsp),%ecx          	# [0] j
	leal 1(%rcx),%r10d            	# [3] 
	incl %r10d                    	# [5] 
	incl %r10d                    	# [6] 
	leal 1(%r10),%ecx             	# [7] 
	xorl %edi,%edi                	# [9] 
	movl %ecx,1216(%rsp)          	# [9] j
	movl %edi,4(%rbp)             	# [10] id:103 result+0x0
	movl %edi,0(%rbp)             	# [10] id:103 result+0x0
	movl 12(%rsp),%esi            	# [10] c2
	addq $16,%rbp                 	# [13] 
	cmpl %ecx,%esi                	# [13] 
	movl %edi,-4(%rbp)            	# [14] id:103 result+0x0
	movl %edi,-8(%rbp)            	# [14] id:103 result+0x0
	jg .LBB68_main                	# [14] 
.Lt_0_12802:
 #<loop> Part of loop body line 45, head labeled .Lt_0_12546
	movl 1220(%rsp),%eax          	# [0] i
	incl %eax                     	# [3] 
	addq $40,%rdx                 	# [4] 
	cmpl %eax,%r8d                	# [4] 
	movl %eax,1220(%rsp)          	# [5] i
	jle .Lt_0_12034               	# [5] 
.Lt_0_12546:
 #<loop> Loop body line 45, nesting depth: 1, estimated iterations: 100
	.loc	1	45	0
	movl 12(%rsp),%eax            	# [0] c2
	cmpl $0,%eax                  	# [3] 
	jle .Lt_0_12802               	# [4] 
.LBB35_main:
 #<loop> Part of loop body line 45, head labeled .Lt_0_12546
	movl %eax,%esi                	# [0] 
	andl $3,%eax                  	# [1] 
	xorl %edi,%edi                	# [1] 
	testl $3,%esi                 	# [1] 
	movq %rdx,%rbp                	# [2] 
	movl %edi,1216(%rsp)          	# [2] j
	je .LBB69_main                	# [2] 
.LBB70_main:
 #<loop> Part of loop body line 45, head labeled .Lt_0_12546
	.loc	1	52	0
	leaq 4(%rdx),%rbp             	# [0] 
	decl %eax                     	# [0] 
	movl %edi,%ecx                	# [1] 
	incl %edi                     	# [1] 
	testl %eax,%eax               	# [1] 
	movl %edi,1216(%rsp)          	# [2] j
	movl %ecx,0(%rdx)             	# [2] id:103 result+0x0
	je .LBB69_main                	# [2] 
.LBB71_main:
 #<loop> Part of loop body line 45, head labeled .Lt_0_12546
	movl 1216(%rsp),%r10d         	# [0] j
	movl %eax,%edi                	# [1] 
	decl %edi                     	# [2] 
	addq $4,%rbp                  	# [3] 
	incl %r10d                    	# [3] 
	testl %edi,%edi               	# [3] 
	movl %r10d,1216(%rsp)         	# [4] j
	movl %ecx,4(%rdx)             	# [4] id:103 result+0x0
	je .LBB69_main                	# [4] 
.LBB72_main:
 #<loop> Part of loop body line 45, head labeled .Lt_0_12546
	xorl %edi,%edi                	# [0] 
	movl %edi,0(%rbp)             	# [1] id:103 result+0x0
	movl 1216(%rsp),%eax          	# [1] j
	incl %eax                     	# [4] 
	addq $4,%rbp                  	# [5] 
	movl %eax,1216(%rsp)          	# [5] j
	jmp .LBB69_main               	# [5] 
.Lt_0_12034:
	testl %r8d,%r8d               	# [0] 
	jle .Lt_0_14082               	# [1] 
.LBB40_main:
	leaq 16(%rsp),%r15            	# [0] a
	movl %r8d,%eax                	# [0] 
	leaq 816(%rsp),%rdi           	# [0] result
	movl 4(%rsp),%r12d            	# [1] c1
	xorl %esi,%esi                	# [1] 
	movq %rax,1280(%rsp)          	# [1] _temp_gra_spill7
	movl %esi,1220(%rsp)          	# [2] i
	movq %rdi,1296(%rsp)          	# [2] _temp_gra_spill9
	jmp .Lt_0_14594               	# [2] 
.Lt_0_14850:
 #<loop> Part of loop body line 52, head labeled .Lt_0_14594
	.loc	1	61	0
 #  57      for(i=0; i<r1; ++i)
 #  58          for(j=0; j<c2; ++j)
 #  59              for(k=0; k<c1; ++k)
 #  60              {
 #  61                  result[i][j]+=a[i][k]*b[k][j];
	movl 1220(%rsp),%edi          	# [0] i
	movl 0(%rsp),%eax             	# [1] r1
	movq 1296(%rsp),%rsi          	# [1] _temp_gra_spill9
	incl %edi                     	# [3] 
	addq $40,%r15                 	# [4] 
	addq $40,%rsi                 	# [4] 
	cmpl %edi,%eax                	# [4] 
	movq %rsi,1296(%rsp)          	# [5] _temp_gra_spill9
	movl %edi,1220(%rsp)          	# [5] i
	jle .Lt_0_14082               	# [5] 
.Lt_0_14594:
 #<loop> Loop body line 52, nesting depth: 1, estimated iterations: 100
	.loc	1	52	0
	movl 12(%rsp),%eax            	# [0] c2
	cmpl $0,%eax                  	# [3] 
	jle .Lt_0_14850               	# [4] 
.LBB43_main:
 #<loop> Part of loop body line 52, head labeled .Lt_0_14594
	movq %rax,1288(%rsp)          	# [0] _temp_gra_spill8
	leaq 416(%rsp),%r13           	# [0] b
	xorl %edi,%edi                	# [0] 
	movq 1296(%rsp),%rbp          	# [1] _temp_gra_spill9
	movl %edi,1216(%rsp)          	# [1] j
	jmp .Lt_0_15362               	# [1] 
.LBB79_main:
 #<loop> Part of loop body line 52, head labeled .Lt_0_15362
	movl %esi,%r14d               	# [0] 
	sarl $2,%r14d                 	# [1] 
	testl %r14d,%r14d             	# [2] 
	je .Lt_0_15618                	# [3] 
.LBB84_main:
 #<loop> Part of loop body line 52, head labeled .Lt_0_15362
.LBB78_main:
 #<loop> Loop body line 52, nesting depth: 3, estimated iterations: 25
 #<loop> unrolled 4 times
	.loc	1	61	0
	movl 0(%r10),%esi             	# [0] id:105 a+0x0
	movl 0(%r11),%r9d             	# [0] id:106 b+0x0
	movl 4(%r10),%eax             	# [1] id:105 a+0x0
	movl 40(%r11),%ecx            	# [1] id:106 b+0x0
	movl 8(%r10),%r8d             	# [3] id:105 a+0x0
	movl 80(%r11),%edi            	# [3] id:106 b+0x0
	imull %esi,%r9d               	# [3] 
	movl 12(%r10),%esi            	# [4] id:105 a+0x0
	imull %eax,%ecx               	# [4] 
	movl 120(%r11),%eax           	# [4] id:106 b+0x0
	imull %r8d,%edi               	# [6] 
	imull %esi,%eax               	# [7] 
	addl %r9d,%edx                	# [7] 
	addl %ecx,%edx                	# [8] 
	addq $160,%r11                	# [9] 
	addl $4,%ebx                  	# [9] 
	addl %edi,%edx                	# [9] 
	addq $16,%r10                 	# [10] 
	addl %eax,%edx                	# [10] 
	cmpl %r12d,%ebx               	# [10] 
	movl %edx,0(%rbp)             	# [11] id:107 result+0x0
	jl .LBB78_main                	# [11] 
.Lt_0_15618:
 #<loop> Part of loop body line 52, head labeled .Lt_0_15362
	movl 1216(%rsp),%edi          	# [0] j
	movl 12(%rsp),%eax            	# [1] c2
	incl %edi                     	# [3] 
	addq $4,%rbp                  	# [4] 
	addq $4,%r13                  	# [4] 
	cmpl %edi,%eax                	# [4] 
	movl %edi,1216(%rsp)          	# [5] j
	jle .Lt_0_14850               	# [5] 
.Lt_0_15362:
 #<loop> Loop body line 52, nesting depth: 2, estimated iterations: 100
	.loc	1	52	0
	testl %r12d,%r12d             	# [0] 
	jle .Lt_0_15618               	# [1] 
.LBB46_main:
 #<loop> Part of loop body line 52, head labeled .Lt_0_15362
	movl 0(%rbp),%edx             	# [0] id:104 result+0x0
	movq %r13,%r11                	# [0] 
	movl %r12d,%eax               	# [0] 
	xorl %ebx,%ebx                	# [1] 
	andl $3,%eax                  	# [1] 
	testl $3,%r12d                	# [1] 
	movq %r15,%r10                	# [2] 
	movl %r12d,%esi               	# [2] 
	je .LBB79_main                	# [2] 
.LBB80_main:
 #<loop> Part of loop body line 52, head labeled .Lt_0_15362
	.loc	1	61	0
	movl 0(%r15),%ecx             	# [0] id:105 a+0x0
	movl 0(%r13),%edi             	# [0] id:106 b+0x0
	imull %ecx,%edi               	# [3] 
	leaq 40(%r13),%r11            	# [5] 
	leaq 4(%r15),%r10             	# [5] 
	decl %eax                     	# [5] 
	incl %ebx                     	# [6] 
	addl %edi,%edx                	# [6] 
	testl %eax,%eax               	# [6] 
	movl %edx,0(%rbp)             	# [7] id:107 result+0x0
	je .LBB79_main                	# [7] 
.LBB81_main:
 #<loop> Part of loop body line 52, head labeled .Lt_0_15362
	movl 4(%r15),%r8d             	# [0] id:105 a+0x0
	movl 40(%r13),%ecx            	# [0] id:106 b+0x0
	imull %r8d,%ecx               	# [3] 
	movl %eax,%edi                	# [4] 
	addq $40,%r11                 	# [5] 
	addq $4,%r10                  	# [5] 
	decl %edi                     	# [5] 
	incl %ebx                     	# [6] 
	addl %ecx,%edx                	# [6] 
	testl %edi,%edi               	# [6] 
	movl %edx,0(%rbp)             	# [7] id:107 result+0x0
	je .LBB79_main                	# [7] 
.LBB82_main:
 #<loop> Part of loop body line 52, head labeled .Lt_0_15362
	movl 0(%r10),%edi             	# [0] id:105 a+0x0
	movl 0(%r11),%eax             	# [0] id:106 b+0x0
	imull %edi,%eax               	# [3] 
	addq $40,%r11                 	# [6] 
	addq $4,%r10                  	# [6] 
	addl %eax,%edx                	# [6] 
	incl %ebx                     	# [7] 
	movl %edx,0(%rbp)             	# [7] id:107 result+0x0
	jmp .LBB79_main               	# [7] 
.Lt_0_14082:
	xorl %eax,%eax                	# [0] 
	.loc	1	65	0
 #  62              }
 #  63  
 #  64      // Displaying the result
 #  65      printf("\nOutput Matrix:\n");
	movq $(.rodata+304),%rdi      	# [0] .rodata+304
	call printf                   	# [0] printf
.LBB52_main:
	movl 0(%rsp),%eax             	# [0] r1
	testl %eax,%eax               	# [3] 
	jle .Lt_0_17154               	# [4] 
.LBB53_main:
	xorl %r12d,%r12d              	# [0] 
	leaq 816(%rsp),%rbx           	# [0] result
	jmp .Lt_0_17666               	# [0] 
.LBB61_main:
 #<loop> Part of loop body line 65, head labeled .Lt_0_17666
	.loc	1	71	0
 #  67          for(j=0; j<c2; ++j)
 #  68          {
 #  69              printf("%d  ", result[i][j]);
 #  70              if(j == c2-1)
 #  71                  printf("\n\n");
	movl 0(%rsp),%eax             	# [0] r1
.Lt_0_17922:
 #<loop> Part of loop body line 65, head labeled .Lt_0_17666
	addl $1,%r12d                 	# [0] 
	addq $40,%rbx                 	# [1] 
	cmpl %r12d,%eax               	# [1] 
	jle .Lt_0_17154               	# [2] 
.Lt_0_17666:
 #<loop> Loop body line 65, nesting depth: 1, estimated iterations: 100
	.loc	1	65	0
	movl 12(%rsp),%edi            	# [0] c2
	cmpl $0,%edi                  	# [3] 
	jle .Lt_0_17922               	# [4] 
.LBB55_main:
 #<loop> Part of loop body line 65, head labeled .Lt_0_17666
	xorl %eax,%eax                	# [0] 
	movq %rbx,%rbp                	# [1] 
	movl %eax,1216(%rsp)          	# [1] j
	jmp .Lt_0_18434               	# [1] 
.Lt_0_19458:
 #<loop> Part of loop body line 69, head labeled .Lt_0_18434
	.loc	1	71	0
	movl 1216(%rsp),%edi          	# [0] j
	movl 12(%rsp),%eax            	# [1] c2
	incl %edi                     	# [3] 
	addq $4,%rbp                  	# [4] 
	cmpl %edi,%eax                	# [4] 
	movl %edi,1216(%rsp)          	# [5] j
	jle .LBB61_main               	# [5] 
.Lt_0_18434:
 #<loop> Loop body line 69
	xorl %eax,%eax                	# [0] 
	.loc	1	69	0
	movl 0(%rbp),%esi             	# [1] id:108 result+0x0
	movq $(.rodata+336),%rdi      	# [1] .rodata+336
	call printf                   	# [1] printf
.LBB57_main:
 #<loop> Part of loop body line 69, head labeled .Lt_0_18434
	.loc	1	70	0
	movl 12(%rsp),%edi            	# [0] c2
	movl 1216(%rsp),%eax          	# [1] j
	decl %edi                     	# [3] 
	cmpl %edi,%eax                	# [4] 
	jne .Lt_0_19458               	# [5] 
.LBB58_main:
 #<loop> Part of loop body line 69, head labeled .Lt_0_18434
	xorl %eax,%eax                	# [0] 
	.loc	1	71	0
	movq $(.rodata+352),%rdi      	# [0] .rodata+352
	call printf                   	# [0] printf
.LBB59_main:
 #<loop> Part of loop body line 69, head labeled .Lt_0_18434
	jmp .Lt_0_19458               	# [0] 
.Lt_0_17154:
	xorq %rax,%rax                	# [0] 
	.loc	1	73	0
 #  72          }
 #  73      return 0;
	movq 1272(%rsp),%rbx          	# [0] _temp_gra_spill6
	movq 1256(%rsp),%rbp          	# [0] _temp_gra_spill4
	movq 1248(%rsp),%r12          	# [1] _temp_gra_spill3
	movq 1264(%rsp),%r13          	# [1] _temp_gra_spill5
	movq 1240(%rsp),%r14          	# [1] _temp_gra_spill2
	movq 1232(%rsp),%r15          	# [2] _temp_gra_spill1
	addq $1320,%rsp               	# [2] 
	ret                           	# [2] 
.L_0_23042:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter rows and column for first matrix: "
	.org 0x30
	.align	0
	# offset 48
	.string "%d %d"
	.org 0x40
	.align	0
	# offset 64
	.string "Enter rows and column for second matrix: "
	.org 0x70
	.align	0
	# offset 112
	.byte	0x45, 0x72, 0x72, 0x6f, 0x72, 0x21, 0x20, 0x63	# Error! c
	.byte	0x6f, 0x6c, 0x75, 0x6d, 0x6e, 0x20, 0x6f, 0x66	# olumn of
	.byte	0x20, 0x66, 0x69, 0x72, 0x73, 0x74, 0x20, 0x6d	#  first m
	.byte	0x61, 0x74, 0x72, 0x69, 0x78, 0x20, 0x6e, 0x6f	# atrix no
	.byte	0x74, 0x20, 0x65, 0x71, 0x75, 0x61, 0x6c, 0x20	# t equal 
	.byte	0x74, 0x6f, 0x20, 0x72, 0x6f, 0x77, 0x20, 0x6f	# to row o
	.byte	0x66, 0x20, 0x73, 0x65, 0x63, 0x6f, 0x6e, 0x64	# f second
	.byte	0x2e, 0xa, 0xa, 0x0	# .\n\n\000
	.org 0xb0
	.align	0
	# offset 176
	.byte	0xa, 0x45, 0x6e, 0x74, 0x65, 0x72, 0x20, 0x65	# \nEnter e
	.byte	0x6c, 0x65, 0x6d, 0x65, 0x6e, 0x74, 0x73, 0x20	# lements 
	.byte	0x6f, 0x66, 0x20, 0x6d, 0x61, 0x74, 0x72, 0x69	# of matri
	.byte	0x78, 0x20, 0x31, 0x3a, 0xa, 0x0	# x 1:\n\000
	.org 0xd0
	.align	0
	# offset 208
	.string "Enter elements a%d%d: "
	.org 0xf0
	.align	0
	# offset 240
	.byte	0xa, 0x45, 0x6e, 0x74, 0x65, 0x72, 0x20, 0x65	# \nEnter e
	.byte	0x6c, 0x65, 0x6d, 0x65, 0x6e, 0x74, 0x73, 0x20	# lements 
	.byte	0x6f, 0x66, 0x20, 0x6d, 0x61, 0x74, 0x72, 0x69	# of matri
	.byte	0x78, 0x20, 0x32, 0x3a, 0xa, 0x0	# x 2:\n\000
	.org 0x110
	.align	0
	# offset 272
	.string "Enter elements b%d%d: "
	.org 0x130
	.align	0
	# offset 304
	.byte	0xa, 0x4f, 0x75, 0x74, 0x70, 0x75, 0x74, 0x20	# \nOutput 
	.byte	0x4d, 0x61, 0x74, 0x72, 0x69, 0x78, 0x3a, 0xa	# Matrix:\n
	.byte	0x0	# \000
	.org 0x150
	.align	0
	# offset 336
	.string "%d  "
	.org 0x160
	.align	0
	# offset 352
	.byte	0xa, 0xa, 0x0	# \n\n\000

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_22786-main
	.uleb128	.L_0_23042-.L_0_22786
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0xb0, 0x0a, 0x04
	.4byte	.LEH_csr_main - .LEH_adjustsp_main
	.byte	0x8f, 0x0c, 0x8e, 0x0b, 0x8c, 0x0a, 0x86, 0x09
	.byte	0x8d, 0x08, 0x83, 0x07
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test49.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

