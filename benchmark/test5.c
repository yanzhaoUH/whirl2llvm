#include <stdio.h>
int main(){

    int dividend, divisor, quotient, remainder;

    printf("Enter dividend: ");
    scanf("%d", &dividend);
//dividend = 5;
    printf("Enter divisor: ");
    scanf("%d", &divisor);
//divisor = 2;
    // Computes quotient
    quotient = dividend/divisor;

    // Computes remainder
    remainder = dividend%divisor;

    printf("Quotient = %d\n",quotient);
    printf("Remainder = %d",remainder);

    return 0;
}
