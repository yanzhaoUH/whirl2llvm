	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test34.c (test34.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test34.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# n1 = 0
	# n2 = 4
	# _temp_gra_spill1 = 8
	.loc	1	21	0
 #  17      }
 #  18      return flag;
 #  19  }
 #  20  
 #  21  int main()
.LBB1_main:
.LEH_adjustsp_main:
.LEH_csr_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
	movq %rbx,8(%rsp)             	# [0] _temp_gra_spill1
.L_0_3074:
	xorl %eax,%eax                	# [0] 
	.loc	1	25	0
 #  22  {
 #  23      int n1, n2, i, flag;
 #  24  
 #  25      printf("Enter two positive integers: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	26	0
 #  26      scanf("%d %d", &n1, &n2);
	leaq 4(%rsp),%rdx             	# [0] n2
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	29	0
 #  27      //n1 = 1;
 #  28      //n2 = 30;
 #  29      printf("Prime numberbers between %d and %d are: ", n1, n2);
	movl 4(%rsp),%edx             	# [0] n2
	movl 0(%rsp),%esi             	# [1] n1
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	call printf                   	# [1] printf
.LBB5_main:
	.loc	1	31	0
 #  30  
 #  31      for(i=n1+1; i<n2; ++i)
	movl 0(%rsp),%ebx             	# [0] n1
	movl 4(%rsp),%eax             	# [1] n2
	incl %ebx                     	# [3] 
	cmpl %ebx,%eax                	# [4] 
	jle .Lt_0_1282                	# [5] 
.Lt_0_1794:
 #<loop> Loop body line 34
	.loc	1	34	0
 #  32      {
 #  33          // i is a prime number, flag will be equal to 1
 #  34          flag = checkPrimeNumber(i);
	movl %ebx,%edi                	# [0] 
	call _Z16checkPrimeNumberi    	# [0] _Z16checkPrimeNumberi
.LBB7_main:
 #<loop> Part of loop body line 34, head labeled .Lt_0_1794
	movl %eax,%eax                	# [0] 
	.loc	1	36	0
 #  35  
 #  36          if(flag == 1)
	cmpl $1,%eax                  	# [1] 
	je .LBB8_main                 	# [2] 
.Lt_0_2562:
 #<loop> Part of loop body line 34, head labeled .Lt_0_1794
	.loc	1	37	0
 #  37              printf("%d ",i);
	movl 4(%rsp),%eax             	# [0] n2
	addl $1,%ebx                  	# [2] 
	cmpl %ebx,%eax                	# [3] 
	jg .Lt_0_1794                 	# [4] 
.Lt_0_1282:
	xorq %rax,%rax                	# [0] 
	.loc	1	39	0
 #  38      }
 #  39      return 0;
	movq 8(%rsp),%rbx             	# [1] _temp_gra_spill1
	addq $24,%rsp                 	# [1] 
	ret                           	# [1] 
.LBB8_main:
 #<loop> Part of loop body line 34, head labeled .Lt_0_1794
	xorl %eax,%eax                	# [0] 
	.loc	1	37	0
	movl %ebx,%esi                	# [1] 
	movq $(.rodata+96),%rdi       	# [1] .rodata+96
	call printf                   	# [1] printf
.LBB13_main:
 #<loop> Part of loop body line 34, head labeled .Lt_0_1794
	jmp .Lt_0_2562                	# [0] 
.L_0_3330:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter two positive integers: "
	.org 0x20
	.align	0
	# offset 32
	.string "%d %d"
	.org 0x30
	.align	0
	# offset 48
	.string "Prime numberbers between %d and %d are: "
	.org 0x60
	.align	0
	# offset 96
	.string "%d "

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_3074-main
	.uleb128	.L_0_3330-.L_0_3074
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.p2align 5,,

	# Program Unit: _Z16checkPrimeNumberi
.globl	_Z16checkPrimeNumberi
	.type	_Z16checkPrimeNumberi, @function
_Z16checkPrimeNumberi:	# 0x98
	# .frame	%rsp, 40, %rsp
	# j = 0
	.loc	1	6	0
.LBB1__Z16checkPrimeNumberi:
	addq $-40,%rsp                	# [0] 
	movl %edi,%r8d                	# [0] 
	sarl $31,%r8d                 	# [1] 
	andl $1,%r8d                  	# [2] 
	addl %edi,%r8d                	# [3] 
	sarl $1,%r8d                  	# [4] 
	cmpl $2,%r8d                  	# [5] 
	jl .Lt_1_1538                 	# [6] 
.LBB2__Z16checkPrimeNumberi:
	movl $2,%ecx                  	# [0] 
.Lt_1_2050:
 #<loop> Loop body line 6, nesting depth: 1, estimated iterations: 100
	.loc	1	12	0
	movl %edi,%eax                	# [0] 
	cltd                          	# [1] 
	idivl %ecx                    	# [2] 
	testl %edx,%edx               	# [24] 
	je .LBB5__Z16checkPrimeNumberi	# [25] 
.Lt_1_2306:
 #<loop> Part of loop body line 6, head labeled .Lt_1_2050
	.loc	1	15	0
	addl $1,%ecx                  	# [0] 
	cmpl %ecx,%r8d                	# [1] 
	jge .Lt_1_2050                	# [2] 
.Lt_1_1538:
	movl $1,%eax                  	# [0] 
	.loc	1	18	0
	movl %eax,%eax                	# [1] 
	addq $40,%rsp                 	# [1] 
	ret                           	# [1] 
.LBB5__Z16checkPrimeNumberi:
	xorl %eax,%eax                	# [0] 
	movl %eax,%eax                	# [1] 
	addq $40,%rsp                 	# [1] 
	ret                           	# [1] 
.LDWend__Z16checkPrimeNumberi:
	.size _Z16checkPrimeNumberi, .LDWend__Z16checkPrimeNumberi-_Z16checkPrimeNumberi
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20, 0x04
	.4byte	.LEH_csr_main - .LEH_adjustsp_main
	.byte	0x83, 0x03
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test34.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

