	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test54.c (test54.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test54.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 56, %rsp
	# i = 4
	# num = 0
	# _temp_gra_spill2 = 8
	# _temp_gra_spill3 = 16
	# _temp_gra_spill4 = 24
	# _temp_gra_spill5 = 32
	.loc	1	4	0
 #   1  #include <stdio.h>
 #   2  #include <stdlib.h>
 #   3  
 #   4  int main()
.LBB1_main:
.LEH_adjustsp_main:
.LEH_csr_main:
	addq $-56,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
	movq %rbx,32(%rsp)            	# [0] _temp_gra_spill5
	movq %rbp,16(%rsp)            	# [0] _temp_gra_spill3
	movq %r12,8(%rsp)             	# [0] _temp_gra_spill2
	movq %r13,24(%rsp)            	# [0] _temp_gra_spill4
.L_0_5634:
	xorl %eax,%eax                	# [0] 
	.loc	1	9	0
 #   5  {
 #   6      int i, num;
 #   7      float *data;
 #   8  
 #   9      printf("Enter total number of elements(1 to 100): ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	10	0
 #  10      scanf("%d", &num);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+48),%rdi       	# [1] .rodata+48
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	.loc	1	14	0
 #  11      //num = 3;
 #  12  
 #  13      // Allocates the memory for 'num' elements.
 #  14      data = (float*) calloc(num, sizeof(float));
	movq $4,%rsi                  	# [0] 
	movslq 0(%rsp),%rdi           	# [0] num
	.globl	calloc
	call calloc                   	# [0] calloc
.LBB5_main:
	.loc	1	16	0
 #  15  
 #  16      if(data == NULL)
	testq %rax,%rax               	# [0] 
	.loc	1	14	0
	movq %rax,%r13                	# [1] 
	.loc	1	16	0
	je .LBB6_main                 	# [1] 
.Lt_0_2306:
	xorl %eax,%eax                	# [0] 
	.loc	1	22	0
 #  18          printf("Error!!! memory not allocated.");
 #  19          exit(0);
 #  20      }
 #  21  
 #  22      printf("\n");
	movq $(.rodata+96),%rdi       	# [0] .rodata+96
	call printf                   	# [0] printf
.LBB10_main:
	movslq 0(%rsp),%rsi           	# [0] num
	testl %esi,%esi               	# [3] 
	jle .Lt_0_2562                	# [4] 
.LBB11_main:
	xorl %r12d,%r12d              	# [0] 
	movq %r13,%rbp                	# [0] 
	movl $1,%ebx                  	# [0] 
.Lt_0_3074:
 #<loop> Loop body line 22, nesting depth: 1, estimated iterations: 100
	xorl %eax,%eax                	# [0] 
	.loc	1	27	0
 #  23  
 #  24      // Stores the number entered by the user.
 #  25      for(i = 0; i < num; ++i)
 #  26      {
 #  27         printf("Enter Number %d: ", i + 1);
	movl %ebx,%esi                	# [1] 
	movq $(.rodata+112),%rdi      	# [1] .rodata+112
	call printf                   	# [1] printf
.LBB13_main:
 #<loop> Part of loop body line 22, head labeled .Lt_0_3074
	.loc	1	29	0
 #  28         //scanf("%f", data + i);
 #  29         *(data+i) = i;
	movslq 0(%rsp),%rsi           	# [0] num
	cvtsi2ss %r12d,%xmm0          	# [0] 
	addl $1,%r12d                 	# [2] 
	addq $4,%rbp                  	# [3] 
	addl $1,%ebx                  	# [3] 
	cmpl %esi,%r12d               	# [3] 
	movss %xmm0,-4(%rbp)          	# [4] id:39
	jl .Lt_0_3074                 	# [4] 
.Lt_0_2562:
	cmpl $1,%esi                  	# [0] 
	jle .Lt_0_5378                	# [1] 
.LBB15_main:
	movss 0(%r13),%xmm1           	# [0] id:40
	leaq 4(%r13),%rbp             	# [1] 
	movl $1,%r12d                 	# [1] 
	jmp .Lt_0_4098                	# [1] 
.Lt_0_4866:
 #<loop> Part of loop body line 29, head labeled .Lt_0_4098
	.loc	1	38	0
 #  34      for(i = 1; i < num; ++i)
 #  35      {
 #  36         // Change < to > if you want to find the smallest number
 #  37         if(*data < *(data + i))
 #  38             *data = *(data + i);
	addl $1,%r12d                 	# [0] 
	addq $4,%rbp                  	# [1] 
	cmpl %esi,%r12d               	# [1] 
	jge .Lt_0_3586                	# [2] 
.Lt_0_4098:
 #<loop> Loop body line 29, nesting depth: 1, estimated iterations: 100
	.loc	1	37	0
	movss 0(%rbp),%xmm0           	# [0] id:41
	comiss %xmm1,%xmm0            	# [3] 
	jbe .Lt_0_4866                	# [7] 
.LBB18_main:
 #<loop> Part of loop body line 29, head labeled .Lt_0_4098
	.loc	1	38	0
	movss %xmm0,0(%r13)           	# [0] id:42
	movaps %xmm0,%xmm1            	# [0] 
	jmp .Lt_0_4866                	# [0] 
.Lt_0_3586:
	.loc	1	42	0
 #  39      }
 #  40  
 #  41  
 #  42      printf("Largest element = %.2f", *data);
	movl $1,%eax                  	# [0] 
	cvtss2sd %xmm1,%xmm0          	# [1] 
	movq $(.rodata+144),%rdi      	# [1] .rodata+144
	call printf                   	# [1] printf
.LBB23_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	44	0
 #  43  
 #  44      return 0;
	movq 32(%rsp),%rbx            	# [1] _temp_gra_spill5
	movq 16(%rsp),%rbp            	# [1] _temp_gra_spill3
	movq 8(%rsp),%r12             	# [1] _temp_gra_spill2
	movq 24(%rsp),%r13            	# [2] _temp_gra_spill4
	addq $56,%rsp                 	# [2] 
	ret                           	# [2] 
.LBB6_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	18	0
	movq $(.rodata+64),%rdi       	# [0] .rodata+64
	call printf                   	# [0] printf
.LBB7_main:
	xorq %rdi,%rdi                	# [0] 
	.loc	1	19	0
	.globl	exit
	call exit                     	# [0] exit
.LBB8_main:
	movq 32(%rsp),%rbx            	# [0] _temp_gra_spill5
	movq 16(%rsp),%rbp            	# [0] _temp_gra_spill3
	movq 8(%rsp),%r12             	# [0] _temp_gra_spill2
	movq 24(%rsp),%r13            	# [1] _temp_gra_spill4
	addq $56,%rsp                 	# [1] 
	ret                           	# [1] 
.Lt_0_5378:
	.loc	1	38	0
	movss 0(%r13),%xmm1           	# [0] id:40
	jmp .Lt_0_3586                	# [0] 
.L_0_5890:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter total number of elements(1 to 100): "
	.org 0x30
	.align	0
	# offset 48
	.string "%d"
	.org 0x40
	.align	0
	# offset 64
	.string "Error!!! memory not allocated."
	.org 0x60
	.align	0
	# offset 96
	.byte	0xa, 0x0	# \n\000
	.org 0x70
	.align	0
	# offset 112
	.string "Enter Number %d: "
	.org 0x90
	.align	0
	# offset 144
	.string "Largest element = %.2f"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_5634-main
	.uleb128	.L_0_5890-.L_0_5634
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x40, 0x04
	.4byte	.LEH_csr_main - .LEH_adjustsp_main
	.byte	0x8c, 0x07, 0x86, 0x06, 0x8d, 0x05, 0x83, 0x04
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test54.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

