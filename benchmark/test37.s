	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test37.c (test37.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test37.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 24, %rsp
	# n = 0
	.loc	1	4	0
 #   1  #include <stdio.h>
 #   2  int addNumbers(int n);
 #   3  
 #   4  int main()
.LBB1_main:
.LEH_adjustsp_main:
	addq $-24,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
.L_0_258:
	xorl %eax,%eax                	# [0] 
	.loc	1	7	0
 #   5  {
 #   6      int n;
 #   7      printf("Enter a positive integer: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	8	0
 #   8      scanf("%d", &n);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	.loc	1	10	0
 #   9      //n = 9;
 #  10      printf("Sum = %d",addNumbers(n));
	movl 0(%rsp),%edi             	# [0] n
	call _Z10addNumbersi          	# [0] _Z10addNumbersi
.LBB5_main:
	movq $(.rodata+48),%rdi       	# [0] .rodata+48
	movl %eax,%eax                	# [0] 
	movl %eax,%esi                	# [1] 
	xorl %eax,%eax                	# [1] 
	call printf                   	# [1] printf
.LBB6_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	11	0
 #  11      return 0;
	addq $24,%rsp                 	# [0] 
	ret                           	# [0] 
.L_0_514:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter a positive integer: "
	.org 0x20
	.align	0
	# offset 32
	.string "%d"
	.org 0x30
	.align	0
	# offset 48
	.string "Sum = %d"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_258-main
	.uleb128	.L_0_514-.L_0_258
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.p2align 5,,

	# Program Unit: _Z10addNumbersi
.globl	_Z10addNumbersi
	.type	_Z10addNumbersi, @function
_Z10addNumbersi:	# 0x48
	# .frame	%rsp, 40, %rsp
	# _temp_gra_spill2 = 0
	.loc	1	13	0
 #  12  }
 #  13  int addNumbers(int n)
.LBB1__Z10addNumbersi:
.LEH_adjustsp__Z10addNumbersi:
.LEH_csr__Z10addNumbersi:
	addq $-40,%rsp                	# [0] 
	movq %rbx,0(%rsp)             	# [1] _temp_gra_spill2
	movl %edi,%ebx                	# [1] 
.L_1_1026:
	.loc	1	15	0
 #  14  {
 #  15      if(n!=0){
	testl %edi,%edi               	# [0] 
	je .Lt_1_770                  	# [1] 
.LBB3__Z10addNumbersi:
	.loc	1	16	0
 #  16          return n+addNumbers(n-1);
	addl $-1,%edi                 	# [0] 
	call _Z10addNumbersi          	# [0] _Z10addNumbersi
.LBB4__Z10addNumbersi:
	movl %eax,%eax                	# [0] 
	addl %ebx,%eax                	# [1] 
	movq 0(%rsp),%rbx             	# [2] _temp_gra_spill2
	addq $40,%rsp                 	# [2] 
	ret                           	# [2] 
.Lt_1_770:
	.loc	1	19	0
 #  17      }
 #  18      else{
 #  19         return n;
	movl %edi,%eax                	# [0] 
	movq 0(%rsp),%rbx             	# [1] _temp_gra_spill2
	addq $40,%rsp                 	# [1] 
	ret                           	# [1] 
.L_1_1282:
.LDWend__Z10addNumbersi:
	.size _Z10addNumbersi, .LDWend__Z10addNumbersi-_Z10addNumbersi

	.section .except_table
	.align	0
	.type	.range_table._Z10addNumbersi, @object
.range_table._Z10addNumbersi:	# 0x8
	# offset 8
	.byte	255
	# offset 9
	.byte	0
	.uleb128	.LSDATTYPEB2-.LSDATTYPED2
.LSDATTYPED2:
	# offset 14
	.byte	1
	.uleb128	.LSDACSE2-.LSDACSB2
.LSDACSB2:
	.uleb128	.L_1_1026-_Z10addNumbersi
	.uleb128	.L_1_1282-.L_1_1026
	# offset 25
	.uleb128	0
	# offset 29
	.uleb128	0
.LSDACSE2:
	# offset 33
	.sleb128	0
	# offset 37
	.sleb128	0
.LSDATTYPEB2:
	# end of initialization for .range_table._Z10addNumbersi
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x20
	.align 8
.LFDE1_end:
	.4byte	.LFDE2_end - .LFDE2_begin
.LFDE2_begin:
	.4byte	.LFDE2_begin - .LEHCIE
	.quad	.LBB1__Z10addNumbersi
	.quad	.LDWend__Z10addNumbersi - .LBB1__Z10addNumbersi
	.byte	0x08
	.quad	.range_table._Z10addNumbersi
	.byte	0x04
	.4byte	.LEH_adjustsp__Z10addNumbersi - .LBB1__Z10addNumbersi
	.byte	0x0e, 0x30, 0x04
	.4byte	.LEH_csr__Z10addNumbersi - .LEH_adjustsp__Z10addNumbersi
	.byte	0x83, 0x06
	.align 8
.LFDE2_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test37.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

