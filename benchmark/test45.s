	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test45.c (test45.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test45.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 40, %rsp
	# n = 0
	# i = 8
	# sum = 4
	# _temp_gra_spill1 = 16
	# _temp_gra_spill2 = 24
	.loc	1	2	0
 #   1  #include <stdio.h>
 #   2  int main(){
.LBB1_main:
.LEH_adjustsp_main:
.LEH_csr_main:
	addq $-40,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
	movq %rbx,24(%rsp)            	# [0] _temp_gra_spill2
	movq %rbp,16(%rsp)            	# [0] _temp_gra_spill1
.L_0_3842:
	xorl %eax,%eax                	# [0] 
	.loc	1	5	0
 #   3      int n, i;
 #   4      float num[100], sum=0.0, average;
 #   5      printf("Enter the numbers of data: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	6	0
 #   6      scanf("%d",&n);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	movl 0(%rsp),%esi             	# [0] n
	leal -1(%rsi),%eax            	# [3] 
	cmpl $99,%eax                 	# [5] 
	jbe .Lt_0_1282                	# [6] 
.Lt_0_1794:
 #<loop> Loop body line 10
	xorl %eax,%eax                	# [0] 
	.loc	1	10	0
 #   7      //n = 101;
 #   8      while (n>100 || n<=0)
 #   9      {
 #  10          printf("Error! number should in range of (1 to 100).\n");
	movq $(.rodata+48),%rdi       	# [0] .rodata+48
	call printf                   	# [0] printf
.LBB6_main:
 #<loop> Part of loop body line 10, head labeled .Lt_0_1794
	xorl %eax,%eax                	# [0] 
	.loc	1	11	0
 #  11          printf("Enter the number again: ");
	movq $(.rodata+96),%rdi       	# [0] .rodata+96
	call printf                   	# [0] printf
.LBB7_main:
 #<loop> Part of loop body line 10, head labeled .Lt_0_1794
	xorl %eax,%eax                	# [0] 
	.loc	1	12	0
 #  12          scanf("%d",&n);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	call scanf                    	# [1] scanf
.LBB8_main:
 #<loop> Part of loop body line 10, head labeled .Lt_0_1794
	movl 0(%rsp),%esi             	# [0] n
	leal -1(%rsi),%eax            	# [3] 
	cmpl $99,%eax                 	# [5] 
	ja .Lt_0_1794                 	# [6] 
.Lt_0_1282:
	testl %esi,%esi               	# [0] 
	jle .Lt_0_3586                	# [1] 
.LBB10_main:
	xorps %xmm0,%xmm0             	# [0] 
	xorl %ebp,%ebp                	# [1] 
	movl $1,%ebx                  	# [1] 
	movss %xmm0,4(%rsp)           	# [1] sum
.Lt_0_2818:
 #<loop> Loop body line 17
	xorl %eax,%eax                	# [0] 
	.loc	1	17	0
 #  13          //n = 10;
 #  14      }
 #  15     for(i=0; i<n; ++i)
 #  16     {
 #  17        printf("%d. Enter number: ",i+1);
	movl %ebx,%esi                	# [1] 
	movq $(.rodata+128),%rdi      	# [1] .rodata+128
	call printf                   	# [1] printf
.LBB12_main:
 #<loop> Part of loop body line 17, head labeled .Lt_0_2818
	.loc	1	20	0
 #  18        //scanf("%f",&num[i]);
 #  19        num[i] = i;
 #  20        sum+=num[i];
	cvtsi2ss %ebp,%xmm0           	# [0] 
	movss 4(%rsp),%xmm1           	# [1] sum
	movl 0(%rsp),%esi             	# [3] n
	addss %xmm0,%xmm1             	# [4] 
	addl $1,%ebp                  	# [5] 
	addl $1,%ebx                  	# [6] 
	cmpl %esi,%ebp                	# [6] 
	movss %xmm1,4(%rsp)           	# [7] sum
	jl .Lt_0_2818                 	# [7] 
.Lt_0_2306:
	.loc	1	24	0
 #  21     }
 #  22  
 #  23     average=sum/n;
 #  24     printf("Average = %.2f",average);
	cvtsi2ss %esi,%xmm2           	# [0] 
	divss %xmm2,%xmm1             	# [4] 
	movl $1,%eax                  	# [21] 
	movq $(.rodata+160),%rdi      	# [22] .rodata+160
	cvtss2sd %xmm1,%xmm0          	# [22] 
	call printf                   	# [22] printf
.LBB16_main:
	xorq %rax,%rax                	# [0] 
	.loc	1	25	0
 #  25     return 0;
	movq 24(%rsp),%rbx            	# [0] _temp_gra_spill2
	movq 16(%rsp),%rbp            	# [1] _temp_gra_spill1
	addq $40,%rsp                 	# [1] 
	ret                           	# [1] 
.Lt_0_3586:
	.loc	1	20	0
	xorps %xmm1,%xmm1             	# [0] 
	jmp .Lt_0_2306                	# [0] 
.L_0_4098:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter the numbers of data: "
	.org 0x20
	.align	0
	# offset 32
	.string "%d"
	.org 0x30
	.align	0
	# offset 48
	.byte	0x45, 0x72, 0x72, 0x6f, 0x72, 0x21, 0x20, 0x6e	# Error! n
	.byte	0x75, 0x6d, 0x62, 0x65, 0x72, 0x20, 0x73, 0x68	# umber sh
	.byte	0x6f, 0x75, 0x6c, 0x64, 0x20, 0x69, 0x6e, 0x20	# ould in 
	.byte	0x72, 0x61, 0x6e, 0x67, 0x65, 0x20, 0x6f, 0x66	# range of
	.byte	0x20, 0x28, 0x31, 0x20, 0x74, 0x6f, 0x20, 0x31	#  (1 to 1
	.byte	0x30, 0x30, 0x29, 0x2e, 0xa, 0x0	# 00).\n\000
	.org 0x60
	.align	0
	# offset 96
	.string "Enter the number again: "
	.org 0x7c
	.align	0
	# offset 124
	.4byte	0
	# float 0.00000
	.org 0x80
	.align	0
	# offset 128
	.string "%d. Enter number: "
	.org 0xa0
	.align	0
	# offset 160
	.string "Average = %.2f"

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_3842-main
	.uleb128	.L_0_4098-.L_0_3842
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x30, 0x04
	.4byte	.LEH_csr_main - .LEH_adjustsp_main
	.byte	0x86, 0x04, 0x83, 0x03
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test45.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

