	#  /home/yan/openuh64-build/lib/gcc-lib/x86_64-open64-linux/5.0/be::5.0

	#-----------------------------------------------------------
	# Compiling test36.c (test36.B)
	#-----------------------------------------------------------

	#-----------------------------------------------------------
	# Options:
	#-----------------------------------------------------------
	#  Target:Core, ISA:ISA_1, Endian:little, Pointer Size:64
	#  -O2	(Optimization level)
	#  -g0	(Debug level)
	#  -m2	(Report advisories)
	#-----------------------------------------------------------

	.file	1	"/home/yan/myllvm/writeBit/whirl2llvm/benchmark/test36.c"


	.text
	.align	2

	.section .rodata, "a",@progbits

	.section .except_table_supp, "a",@progbits

	.section .except_table, "a",@progbits
	.section .text
	.p2align 5,,

	# Program Unit: main
.globl	main
	.type	main, @function
main:	# 0x0
	# .frame	%rsp, 40, %rsp
	# n = 0
	# i = 4
	# flag = 8
	# _temp_gra_spill1 = 16
	# _temp_gra_spill2 = 24
	.loc	1	3	0
 #   1  #include <stdio.h>
 #   2  int checkPrime(int n);
 #   3  int main()
.LBB1_main:
.LEH_adjustsp_main:
.LEH_csr_main:
	addq $-40,%rsp                	# [0] 
	addq    $-8,%rsp
	fnstcw  (%rsp)
	andw    $0xfcff,(%rsp)
	orw     $768,(%rsp)
	fldcw   (%rsp)
	addq    $8,%rsp
	movq %rbx,24(%rsp)            	# [0] _temp_gra_spill2
	movq %rbp,16(%rsp)            	# [0] _temp_gra_spill1
.L_0_5378:
	xorl %eax,%eax                	# [0] 
	.loc	1	7	0
 #   4  {
 #   5      int n, i, flag = 0;
 #   6  
 #   7      printf("Enter a positive integer: ");
	movq $(.rodata),%rdi          	# [0] .rodata
	.globl	printf
	call printf                   	# [0] printf
.LBB3_main:
	xorl %eax,%eax                	# [0] 
	.loc	1	8	0
 #   8      scanf("%d", &n);
	movq %rsp,%rsi                	# [1] 
	movq $(.rodata+32),%rdi       	# [1] .rodata+32
	.globl	scanf
	call scanf                    	# [1] scanf
.LBB4_main:
	movl 0(%rsp),%esi             	# [0] n
	movl %esi,%eax                	# [3] 
	sarl $31,%eax                 	# [4] 
	andl $1,%eax                  	# [5] 
	addl %esi,%eax                	# [6] 
	sarl $1,%eax                  	# [7] 
	cmpl $2,%eax                  	# [8] 
	jl .Lt_0_5122                 	# [9] 
.LBB5_main:
	xorl %ebp,%ebp                	# [0] 
	movl $2,%ebx                  	# [0] 
.Lt_0_2818:
 #<loop> Loop body line 15
	.loc	1	15	0
 #  11  
 #  12      for(i=2; i<=n/2; ++i)
 #  13      {
 #  14          // condition for i to be a prime number
 #  15          if (checkPrime(i) == 1)
	movl %ebx,%edi                	# [0] 
	call _Z10checkPrimei          	# [0] _Z10checkPrimei
.LBB7_main:
 #<loop> Part of loop body line 15, head labeled .Lt_0_2818
	movl %eax,%eax                	# [0] 
	cmpl $1,%eax                  	# [1] 
	je .LBB8_main                 	# [2] 
.Lt_0_4098:
.Lt_0_4354:
 #<loop> Part of loop body line 15, head labeled .Lt_0_2818
	.loc	1	22	0
 #  18              if (checkPrime(n-i) == 1)
 #  19              {
 #  20          // n = primeNumber1 + primeNumber2
 #  21                  printf("%d = %d + %d\n", n, i, n-i);
 #  22                  flag = 1;
	movl 0(%rsp),%esi             	# [0] n
	movl %esi,%eax                	# [3] 
	sarl $31,%eax                 	# [4] 
	andl $1,%eax                  	# [5] 
	addl %esi,%eax                	# [6] 
	addl $1,%ebx                  	# [7] 
	sarl $1,%eax                  	# [7] 
	cmpl %eax,%ebx                	# [8] 
	jle .Lt_0_2818                	# [9] 
.LBB13_main:
	.loc	1	27	0
 #  23              }
 #  24  
 #  25          }
 #  26      }
 #  27      if (flag==0)
	testl %ebp,%ebp               	# [0] 
	je .Lt_0_5122                 	# [1] 
.Lt_0_4610:
	xorq %rax,%rax                	# [0] 
	.loc	1	29	0
 #  28          printf("%d can't be expressed as the sum of two prime numbers.", n);
 #  29      return 0;
	movq 24(%rsp),%rbx            	# [0] _temp_gra_spill2
	movq 16(%rsp),%rbp            	# [1] _temp_gra_spill1
	addq $40,%rsp                 	# [1] 
	ret                           	# [1] 
.LBB8_main:
 #<loop> Part of loop body line 15, head labeled .Lt_0_2818
	.loc	1	18	0
	movl 0(%rsp),%edi             	# [0] n
	movl %edi,%edi                	# [3] 
	subl %ebx,%edi                	# [4] 
	call _Z10checkPrimei          	# [4] _Z10checkPrimei
.LBB9_main:
 #<loop> Part of loop body line 15, head labeled .Lt_0_2818
	movl %eax,%eax                	# [0] 
	cmpl $1,%eax                  	# [1] 
	jne .Lt_0_4098                	# [2] 
.LBB10_main:
 #<loop> Part of loop body line 15, head labeled .Lt_0_2818
	.loc	1	21	0
	movl 0(%rsp),%ecx             	# [0] n
	xorl %eax,%eax                	# [2] 
	movl %ebx,%edx                	# [3] 
	movl %ecx,%esi                	# [3] 
	movl %ecx,%ecx                	# [3] 
	movq $(.rodata+48),%rdi       	# [4] .rodata+48
	subl %ebx,%ecx                	# [4] 
	call printf                   	# [4] printf
.LBB11_main:
 #<loop> Part of loop body line 15, head labeled .Lt_0_2818
	movl $1,%ebp                  	# [0] 
	jmp .Lt_0_4098                	# [0] 
.Lt_0_5122:
	xorl %eax,%eax                	# [0] 
	.loc	1	28	0
	movl %esi,%esi                	# [1] 
	movq $(.rodata+64),%rdi       	# [1] .rodata+64
	call printf                   	# [1] printf
.LBB20_main:
	jmp .Lt_0_4610                	# [0] 
.L_0_5634:
.LDWend_main:
	.size main, .LDWend_main-main

	.section .rodata
	.org 0x0
	.align	0
	# offset 0
	.string "Enter a positive integer: "
	.org 0x20
	.align	0
	# offset 32
	.string "%d"
	.org 0x30
	.align	0
	# offset 48
	.byte	0x25, 0x64, 0x20, 0x3d, 0x20, 0x25, 0x64, 0x20	# %d = %d 
	.byte	0x2b, 0x20, 0x25, 0x64, 0xa, 0x0	# + %d\n\000
	.org 0x40
	.align	0
	# offset 64
	.string "%d can't be expressed as the sum of two prime numbers."

	.section .except_table
	.align	0
	.type	.range_table.main, @object
.range_table.main:	# 0x0
	# offset 0
	.byte	255
	# offset 1
	.byte	0
	.uleb128	.LSDATTYPEB1-.LSDATTYPED1
.LSDATTYPED1:
	# offset 6
	.byte	1
	.uleb128	.LSDACSE1-.LSDACSB1
.LSDACSB1:
	.uleb128	.L_0_5378-main
	.uleb128	.L_0_5634-.L_0_5378
	# offset 17
	.uleb128	0
	# offset 21
	.uleb128	0
.LSDACSE1:
	# offset 25
	.sleb128	0
	# offset 29
	.sleb128	0
.LSDATTYPEB1:
	# end of initialization for .range_table.main
	.section .text
	.p2align 5,,

	# Program Unit: _Z10checkPrimei
.globl	_Z10checkPrimei
	.type	_Z10checkPrimei, @function
_Z10checkPrimei:	# 0xfc
	# .frame	%rsp, 40, %rsp
	# i = 0
	# flag = 4
	.loc	1	33	0
 #  30  }
 #  31  
 #  32  // Function to check prime number
 #  33  int checkPrime(int n)
.LBB1__Z10checkPrimei:
	addq $-40,%rsp                	# [0] 
	movl %edi,%r8d                	# [0] 
	sarl $31,%r8d                 	# [1] 
	andl $1,%r8d                  	# [2] 
	addl %edi,%r8d                	# [3] 
	sarl $1,%r8d                  	# [4] 
	cmpl $2,%r8d                  	# [5] 
	jl .Lt_1_2818                 	# [6] 
.LBB2__Z10checkPrimei:
	movl $2,%ecx                  	# [0] 
	movl $1,%r9d                  	# [0] 
	jmp .Lt_1_1794                	# [0] 
.Lt_1_2562:
 #<loop> Part of loop body line 33, head labeled .Lt_1_1794
	.loc	1	40	0
 #  36  
 #  37      for(i=2; i<=n/2; ++i)
 #  38      {
 #  39          if(n%i==0)
 #  40              flag=0;
	addl $1,%ecx                  	# [0] 
	cmpl %ecx,%r8d                	# [1] 
	jl .Lt_1_1282                 	# [2] 
.Lt_1_1794:
 #<loop> Loop body line 33, nesting depth: 1, estimated iterations: 100
	.loc	1	39	0
	movl %edi,%eax                	# [0] 
	cltd                          	# [1] 
	idivl %ecx                    	# [2] 
	testl %edx,%edx               	# [24] 
	jne .Lt_1_2562                	# [25] 
.LBB5__Z10checkPrimei:
 #<loop> Part of loop body line 33, head labeled .Lt_1_1794
	xorl %r9d,%r9d                	# [0] 
	jmp .Lt_1_2562                	# [0] 
.Lt_1_1282:
	.loc	1	43	0
 #  41      }
 #  42  
 #  43      return flag;
	movl %r9d,%eax                	# [0] 
	addq $40,%rsp                 	# [0] 
	ret                           	# [0] 
.Lt_1_2818:
	.loc	1	40	0
	movl $1,%eax                  	# [0] 
	.loc	1	43	0
	movl %eax,%eax                	# [1] 
	addq $40,%rsp                 	# [1] 
	ret                           	# [1] 
.LDWend__Z10checkPrimei:
	.size _Z10checkPrimei, .LDWend__Z10checkPrimei-_Z10checkPrimei
	.section .text
	.align	4
	.section .rodata
	.align	16
	.section .except_table_supp
	.align	4
	.section .except_table
	.align	8

	.section .eh_frame, "a",@progbits
.LEHCIE:
	.4byte	.LEHCIE_end - .LEHCIE_begin
.LEHCIE_begin:
	.4byte 0x0
	.byte	0x01, 0x7a, 0x50, 0x4c, 0x00, 0x01, 0x78, 0x10
	.byte	0x0a, 0x00
	.quad	__gxx_personality_v0
	.byte	0x00, 0x0c, 0x07, 0x08, 0x90, 0x01
	.align 8
.LEHCIE_end:
	.4byte	.LFDE1_end - .LFDE1_begin
.LFDE1_begin:
	.4byte	.LFDE1_begin - .LEHCIE
	.quad	.LBB1_main
	.quad	.LDWend_main - .LBB1_main
	.byte	0x08
	.quad	.range_table.main
	.byte	0x04
	.4byte	.LEH_adjustsp_main - .LBB1_main
	.byte	0x0e, 0x30, 0x04
	.4byte	.LEH_csr_main - .LEH_adjustsp_main
	.byte	0x86, 0x04, 0x83, 0x03
	.align 8
.LFDE1_end:

	.section .debug_line, ""
	.section	.note.GNU-stack,"",@progbits
	.ident	"#Open64 Compiler Version 5.0 : test36.c compiled with : -O2 -march=core -msse2 -msse3 -mno-3dnow -mno-sse4a -mno-ssse3 -mno-sse41 -mno-sse42 -mno-aes -mno-pclmul -mno-avx -mno-xop -mno-fma -mno-fma4 -m64"

